/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    // carga dinamicamente municipio
    var idprovincia = $('#form_provincia option:selected').val();
    $valor_municipio = $('#form_municipio').val();
//$('#form_municipio').load(Routing.generate('admin_lugaresfiltramunicipio', {idprovincia: idprovincia, obligatorio: 0}));
//$('#form_municipio').val($valor_municipio);

    $('#form_lugar').html('');
    $('#form_provincia').change(function(event) {
        var idprovincia = $('#form_provincia option:selected').val();
        $('#form_municipio').load(Routing.generate('admin_lugaresfiltramunicipio', {idprovincia: idprovincia, obligatorio: 0}));
    });
// carga dinamicamente zona        
    $('#form_municipio').change(function(event) {
        var idprovincia = $('#form_provincia option:selected').val();
        var idmunicipio = $('#form_municipio option:selected').val();
        $('#form_lugar').load(Routing.generate('admin_lugaresfiltralugares', {idprovincia: idprovincia, idmunicipio: idmunicipio, obligatorio: 0}));
    });
    jQuery(document).ready(function() {
        TaskList.initTaskWidget();
    });
    $("#sortable").sortable({
        stop: function(event, ui) {
            $('#imagenessubidasinputs').html('')
            $('.list-primary').each(function(indice, elemento) {
                $('#imagenessubidasinputs').append('<input type="text" name="imageneshotel[]" class="borraelemento_' + $(elemento).attr('data-id') + '" value="' + $(elemento).attr('data-id') + '"><br>')
            });
            //return false
        }
    });
    $("#sortable").disableSelection();
});

var doneTypingInterval = 750;  //time in ms, 2 second for example
var typingTimer;                //timer identifier

$(document).ready(function() {
    //setup before functions

    $('.nombre-hotel').keyup(function(evt) {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTypingName, doneTypingInterval, evt.target);
    });

    $('.nombre-hotel').keydown(function(evt) {
        clearTimeout(typingTimer);
    });

    //on keyup, start the countdown
    $('.direccionmapa').keyup(function() {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTypingStreet, doneTypingInterval);
    });
    //on keydown, clear the countdown 
    $('.direccionmapa').keydown(function() {
        clearTimeout(typingTimer);
    });
    //user is "finished typing," do something
    function doneTypingStreet() {
        situadireccion();
    }
    function doneTypingName(target) {
        $valor_base = $(target).val();
        /* Remove unwanted characters, only accept alphanumeric and space */
        var url_final = $valor_base.replace(/[^A-Za-z0-9 ]/g, '');

        /* Replace multi spaces with a single space */
        url_final = url_final.replace(/\s{2,}/g, ' ');

        /* Replace space with a '-' symbol */
        url_final = url_final.replace(/\s/g, "-");
        $('.url-hotel').val(url_final);
    }
});
var map;
function resizemap() {
    map = new GMaps({
        div: '#mapaiframe',
        lat: -12.043333,
        lng: -77.028333
    });
    if ($('#hoteles_backendbundle_hoteltype_ubicacion').val() != '') {
        situadireccion();
    }
}

function situadireccion() {
    GMaps.geocode({
        address: $('.direccionmapa').val().trim(),
        callback: function(results, status) {
            if (status == 'OK') {
                var latlng = results[0].geometry.location;
                map.setCenter(latlng.lat(), latlng.lng());
                map.addMarker({
                    lat: latlng.lat(),
                    lng: latlng.lng()
                });
                $('#form_latitud').val(latlng.lat());
                $('#form_longitud').val(latlng.lng());
            }
        }
    });
}

function deleteElement(id) {
    console.log(id)
    $('.borraelemento_' + id).remove()
    return false
}

function cargadatosempresa(idEmpresa) {
    $('#hoteles_backendbundle_hoteltype_nombreempresa').val('aaaaaa1' + idEmpresa)
    $('#hoteles_backendbundle_hoteltype_cif').val('aaaaaa2' + idEmpresa)
    $('#hoteles_backendbundle_hoteltype_direccionfacturacion').val('aaaaaa3' + idEmpresa)
    $('#hoteles_backendbundle_hoteltype_telefono').val('aaaaaa4' + idEmpresa)
    $('#hoteles_backendbundle_hoteltype_numerocuenta').val('aaaaaa5' + idEmpresa)
}

$(document).ready(function() {

    $('.direccionmapa').change(function(event) {
        event.preventDefault();
        situadireccion()
    });
    $('#hoteles_backendbundle_hoteltype_empresa').change(function(event) {
        var r = confirm('¿Quiere usted cargar los datos de esa empresa?')
        if (r == true) {
            cargadatosempresa($("#hoteles_backendbundle_hoteltype_empresa option:selected").val())
        }
    });


});
