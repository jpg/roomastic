/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var map_array = {};
$(document).ready(function() {
    function initialize() {
        $('.mapa').each(function(index, mapa_elem) {
            mapa_elem_long = mapa_elem.dataset.longitud;
            mapa_elem_lat = mapa_elem.dataset.latitud;
            mapa_elem_title = mapa_elem.dataset.titulo;
            hotel_id = mapa_elem.dataset.hotelId;
            latlng = new google.maps.LatLng(mapa_elem_lat, mapa_elem_long);
            var mapOptions = {
                zoom: 15,
                center: latlng //center: new google.maps.LatLng(mapa_elem_lat, mapa_elem_long)
            };
            map = new google.maps.Map(mapa_elem,
                    mapOptions);
            map_array['mapa_hotel_' + hotel_id ] = map;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: mapa_elem_title
            });
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    $(window).scroll(function() {
        var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
        var scrolltrigger = 0.80;
        if ((wintop / (docheight - winheight)) > scrolltrigger) {
            if (typeof last_page !== 'undefined' && last_page === false && is_processing === false) {
                addMoreActivities();
            }
        }
    });
    mapa = $('.single_hotel .abrir_map').closest('.single_hotel').find('.desplegable_mapa').first();

    detalles = $('.single_hotel .detalle').closest('.single_hotel').find('.desplegable').first();

    $('.wrapper_hoteles').on('click', '.detalle', function(evt) {

        var detalles = $(this).closest('.single_hotel').find('.desplegable');

        if (detalles.is(':visible')) {

            detalles.toggle(400);
            mapa.hide(400);

        } else {

            $('.desplegable').hide(400);
            $('.desplegable_mapa').hide(400);
            detalles.toggle(400);

        }
        evt.preventDefault();

    });

    $('.wrapper_hoteles').on('click', '.abrir_map', function(evt) {
        var mapa = $(this).closest('.single_hotel').find('.desplegable_mapa');
        if (mapa.is(':visible')) {
            mapa.toggle(400);
        } else {
            $('.desplegable').hide(400);
            $('.desplegable_mapa').hide(400);

            mapa.toggle(400, function() {
                $dataset = mapa.find('.mapa').data();
                google_map = map_array['mapa_hotel_' + $dataset.hotelId];
                var center = google_map.getCenter();
                google.maps.event.trigger(google_map, 'resize');
                google_map.setCenter(center);

            });
        }
        evt.preventDefault();

    });
    $('.selectize-input input').blur();

    $('.ajaxupdate').change(function(event) {
        confirm("¿Desea cambiar su lugar de busqueda?");
        var lugar = $(".selec_ciudad option:selected").val();
        var fecha = $(".select_fecha").val();
        if (lugar !== '' && fecha !== '') {
            fecha = fecha.replace(new RegExp("/", "g"), "-");
            var res = fecha.split(" - ");
            var url = '/resultados/' + lugar + '/' + res[0] + '/' + res[1];
            //var url = Routing.generate('HotelesFrontendBundle_listado', { lugar: lugar, fechain: res[0], fechaout: res[1] });
            window.location.href = url;
        }
        event.preventDefault();
    });

    $('.ajaxupdate').on('ifChecked', function(event) {
        cargarDatosExtra();
    });

    $('.ajaxupdate').on('ifUnchecked', function(event) {
        cargarDatosExtra();
    });

});

var is_processing = false;
function addMoreActivities() {
    is_processing = true;
    $.ajax({
        type: "GET",
        url: Routing.generate('uah_gestoractividades_default_ajaxactivities', {page: page}),
        success: function(data) {
            if (data.html.length > 0) {
                $('.activities').append(data.html);
                $('.activity').not(filter).hide();
                $(filter).show();
                page = page + 1;
                last_page = data.last_page;
            } else {
                last_page = true;
            }
            is_processing = false;
        },
        error: function(data) {
            is_processing = false;
        }
    });
}
function cargarDatosExtra() {
    var url = $('#formajax').attr('action'); // El script a dónde se realizará la petición.
    //datos_formulario = $("#formajax").serializeArray();

    numero_pagina = $('#infinite-scroll-ajax').data().lastPage + 1;
    datos = [];
    datos.push({name: "numero_pagina", value: numero_pagina});

    $.ajax({
        type: "POST",
        url: url,
        data: $.param(datos), // Adjuntar los campos del formulario enviado.
        success: function(data) {
            //Actualizo el numero de pagina que he pedido
            $('#infinite-scroll-ajax').data().lastPage = $('#infinite-scroll-ajax').data().lastPage + 1;
            $(".pikame_listado", data).PikaChoose({buildFinished: a});
            $('.mapa', data).each(function(index, mapa_elem) {
                //Cargo el mapa de cada uno de los nuevos elementos
                mapa_elem_long = mapa_elem.dataset.longitud;
                mapa_elem_lat = mapa_elem.dataset.latitud;
                mapa_elem_title = mapa_elem.dataset.titulo;
                hotel_id = mapa_elem.dataset.hotelId;
                latlng = new google.maps.LatLng(mapa_elem_lat, mapa_elem_long);
                var mapOptions = {
                    zoom: 15,
                    center: latlng //center: new google.maps.LatLng(mapa_elem_lat, mapa_elem_long)
                };
                map = new google.maps.Map(mapa_elem,
                        mapOptions);
                map_array['mapa_hotel_' + hotel_id ] = map;
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    title: mapa_elem_title
                });
            });

            //Adjunto los nuevos hoteles al mapa
            $(".wrapper_hoteles").append(data); // Mostrar la respuestas del script PHP.

            $("input.timbre").iCheck(
                    timbreCheckboxConfig);
            configEventsTimbre();
        }
    });

    return false;

}
