$(document).ready(function() {

    $(document).ajaxStart(function() {
        $('#modal').show();
        $('#fade').show();

    });
    $(document).ajaxStop(function() {
        $('#modal').hide();
        $('#fade').hide();
    });

});