if (typeof ROOMASTIC === 'undefined') {
    ROOMASTIC = {};
}

timbreCheckboxConfig = {
    checkboxClass: 'icheckbox_timbre',
    radioClass: 'iradio_minimal',
    //increaseArea: '20%' // optional
};
checkboxLateralConfig = {
    checkboxClass: 'icheckbox_minimal',
    radioClass: 'iradio_minimal'

}

var configEventsTimbre = function() {

};

$(function() {
    //Elimino la cache para las llamadas ajax
    $.ajaxSetup({
        cache: false
    });
    

    $('#hacer-oferta').on('click', function(evt) {
        //Pillo las id's de los hoteles
        //$hoteles_seleccionados = $('.timbre:checked');/
        $hoteles_seleccionados = $.makeArray($('.timbre:checked')).map(
                function(hotel) {
                    return hotel.dataset.hotelId;
                }).join();

        //Pillo las fechas
        $fecha_inicio = $('#reservation').data().daterangepicker.startDate.format('DD/MM/YY');
        $fecha_fin = $('#reservation').data().daterangepicker.endDate.format('DD/MM/YY');
        location.href = Routing.generate('HotelesFrontendBundle_oferta', {hoteles: $hoteles_seleccionados, fecha_inicio: $fecha_inicio, fecha_fin: $fecha_fin});

    });
    ROOMASTIC.daterangepicker = $('#reservation').daterangepicker({});

    $(document).on('click', 'td', function(evt) {
        //Usar esto $('#reservation').data().daterangepicker.startDate para sacar la fecha de inicio y final
        $input_fecha_element = $('#reservation');
        if ($input_fecha_element.length === 0) {
            $input_fecha_element = $('#hoteles_backendbundle_ofertatype_fecha');
        }
        $fecha_inicio = $input_fecha_element.data().daterangepicker.startDate.format('DD/MM/YY');
        $fecha_fin = $input_fecha_element.data().daterangepicker.endDate.format('DD/MM/YY');

        if ($('#hoteles_backendbundle_ofertatype_fecha').length !== 0) {
            $('#hoteles_backendbundle_ofertatype_numnoches').val(dateDiff($input_fecha_element.data().daterangepicker.startDate.format('MM/DD/YY'), $input_fecha_element.data().daterangepicker.endDate.format('MM/DD/YY')).d);
            calcularPrecio();
        }
        $input_fecha_element.val($fecha_inicio + ' - ' + $fecha_fin);
    });
    $('#reservation').on('apply.daterangepicker', function(ev, picker) {

        var anoInicio = picker.startDate.format('YYYY');
        var mesInicio = picker.startDate.format('MM')
        var diaInicio = picker.startDate.format('DD')

        var anoFinal = picker.endDate.format('YYYY');
        var mesFinal = picker.endDate.format('MM');
        var diaFinal = picker.endDate.format('DD');

        var b = moment([anoInicio, mesInicio, diaInicio]);
        var a = moment([anoFinal, mesFinal, diaFinal]);
        a.diff(b, 'days'); // 1

        var difDias = a.diff(b, 'days');

        $('.caja_noches input').val(difDias);

        var valor = $('.caja_noches input').val(difDias);

        // Actualizar Total

        var maxPrice = Number($('input[name=precio]').val());
        var nNoches = valor.val();
        var nPersonas = Number($('input[name=adultos]').val());
        var nHabs = Number($('input[name=habs]').val());

        var valor = maxPrice * nNoches * nPersonas * nHabs;

        $('input[name=precio_total]').val(valor);

    });


    $('#hoteles_backendbundle_ofertatype_fecha').on('apply.daterangepicker', function(ev, picker) {

        var anoInicio = picker.startDate.format('YYYY');
        var mesInicio = picker.startDate.format('MM')
        var diaInicio = picker.startDate.format('DD')

        var anoFinal = picker.endDate.format('YYYY');
        var mesFinal = picker.endDate.format('MM');
        var diaFinal = picker.endDate.format('DD');

        /*console.log(anoInicio+mesInicio+diaInicio);
         console.log(anoFinal+mesFinal+diaFinal);*/

        var b = moment([anoInicio, mesInicio, diaInicio]);
        var a = moment([anoFinal, mesFinal, diaFinal]);
        a.diff(b, 'days'); // 1

        var difDias = a.diff(b, 'days');

        $('.caja_noches input').val(difDias);

        var valor = $('.caja_noches input').val(difDias);

        // Actualizar Total

        var maxPrice = Number($('input[name=precio]').val());
        var nNoches = valor.val();
        var nPersonas = Number($('input[name=adultos]').val());
        var nHabs = Number($('input[name=habs]').val());

        var valor = maxPrice * nNoches * nPersonas * nHabs;

        $('input[name=precio_total]').val(valor);

    });


// Calcular precio Final

    var maxPrice = Number($('input[name=precio]').val());
    var nNoches = Number($('input[name=n_noches]').val());
    var nPersonas = Number($('input[name=adultos]').val());
    var nHabs = Number($('input[name=habs]').val());


    $('input[name=precio], input[name=n_noches], input[name=adultos], input[name=habs]').change(function() {

        var maxPrice = Number($('input[name=precio]').val());
        var nNoches = Number($('input[name=n_noches]').val());
        var nPersonas = Number($('input[name=adultos]').val());
        var nHabs = Number($('input[name=habs]').val());


        var valor = maxPrice * nNoches * nPersonas * nHabs;

        $('input[name=precio_total]').val(valor);
    });


    ROOMASTIC.selectize_options = {
        create: false,
        openOnFocus: false,
        maxOptions: 5000,
        sortField: {
            field: 'text',
            direction: 'asc'
        },
        dropdownParent: 'body'
    };

    ROOMASTIC.select_ciudad = $('.selec_ciudad').selectize(ROOMASTIC.selectize_options);

    $('.selectize-input input').delay(1700).focus();



    var viewportHeight = $(window).height();
    var footerHeight = $('#footer').height();

    if ((viewportHeight) < 792) {

        var viewportHeight = $(window).height();

        $('.col_izq').css('min-height', viewportHeight - footerHeight - 5);

    } else {

        var viewportHeight = $('.col_der').height();

        $('.col_izq').css('min-height', viewportHeight);

    }
    ;

    $(window).resize(function() {
        if ((viewportHeight) < 792) {
            var viewportHeight = $(window).height();
            $('.col_izq').css('min-height', viewportHeight - footerHeight - 5);
        } else {
            var viewportHeight = $('.col_der').height();
            $('.col_izq').css('min-height', viewportHeight);
        }
        ;
    });



    $('input.checkbox-lateral').iCheck(checkboxLateralConfig);
    checkboxOfertaConfig = checkboxLateralConfig;
    $('input.checkbox-oferta').iCheck(checkboxOfertaConfig);
    $('input.timbre').iCheck(timbreCheckboxConfig);

    function ocultarLlamarTimbre() {
        $('.hacer_oferta').hide(400);
        $('.menu_int').css('right', '0px');
        $('.lineatop').css({
            position: 'relative',
            minWidth: '1020px',
            marginBottom: '0px',
        });
        $('.wrapper').css({
            marginTop: 'auto',
        });
    }
    function mostrarLlamarTimbre() {
        if (!$('.hacer_oferta').is(':visible')) {
            $('.hacer_oferta').show(400);
            $('.menu_int').css('right', '160px');
            $('.hacer_oferta').addClass('posFixed');
            $('.lineatop').css({
                position: 'fixed',
                width: '100%',
                top: '0',
            });
            $('.wrapper').css({
                marginTop: '5px',
            });
        }
    }

    $(document).on('ifChecked', 'input.timbre', function(evt) {
        mostrarLlamarTimbre();
    });
    $(document).on('ifUnchecked', 'input.timbre', function(evt) {
        //No se usa el 0, porque la clase checked esta todavía puesta en este evento
        if ($('.icheckbox_timbre.checked').length === 1) {
            ocultarLlamarTimbre();
        }
    });

    $('.hotel-selected input[type=checkbox]').on('ifUnchecked', function(evt) {
        $li_padre = $(this).closest('.hotel-selected');
        $li_padre.hide();
        $hotelId = $li_padre.data().hotelId;
        $('#hotel-summary-' + $hotelId + ' .timbre').iCheck('uncheck');
    });

    //Este código hay que hacerlo inmune a los cambios que hay por AJAX
    //Hay que reestructurar el código o la manera en la que se hace lo del ajax
    $('.wrapper_hoteles').on('ifClicked', '.timbre', function(evt) {
        $hotel_id = evt.target.dataset.hotelId;
        // Busco el checkbox que tengo que mostrar
        $('#hotel-checkbox-' + $hotel_id).show();
        $('#hotel-checkbox-' + $hotel_id + ' input[type=checkbox]').iCheck('check');

    });

    $('.wrapper_hoteles').on('ifUnchecked', '.timbre', function(evt) {
        $hotel_id = evt.target.dataset.hotelId;
        // Busco el checkbox que tengo que mostrar
        $('#hotel-checkbox-' + $hotel_id).hide();
        $('#hotel-checkbox-' + $hotel_id + ' input[type=checkbox]').iCheck('uncheck');

    });

    //fancybox - vídeohome
    if ($("#fancyhome").length > 0) {
        $("#fancyhome").fancybox();
    }

    //cookies
    $.cookieBar({});

}); // Fin Document Ready


if (typeof ROOMASTIC === 'undefined') {
    ROOMASTIC = {};
}

function login_fb() {
    $.ajax({
        url: Routing.generate('TooltypAuthFaceBundle_login'),
        type: "GET",
        success: function(data) {
            ROOMASTIC.user_data = data.user_data;
            if (ROOMASTIC.user_data['canOffer'] === 'si') {
                hacerOferta();
            } else if (ROOMASTIC.user_data['canOffer'] === 'faltan_datos') {
                for (var propertyName in ROOMASTIC.user_data) {
                    $('#formregisterextra .' + propertyName).val(ROOMASTIC.user_data[propertyName]);
                    if (propertyName === 'email' && ROOMASTIC.user_data[propertyName].length > 0) {
                        $('#formregisterextra .' + propertyName).prop('readonly', true);
                    }
                }
                showRegisterExtra();
            } else if (ROOMASTIC.user_data['canOffer'] === 'oferta_realizada_pendiente') {
                swal('Tu usuario tiene una oferta pendiente');
            }
        },
        error: function(data) {
            console.log(data);
            swal('Vaya, ha habido un error. Inténtalo de nuevo tras recargar la página');
        }
    });
}

$(document).ready(function() {
    function facebookReady() {
        FB.init({
            appId: ROOMASTIC.facebookAppId,
            status: true,
            cookie: true,
            xfbml: true,
            oauth: true
        });
        $(document).trigger("facebook:ready");
    }
    if (window.FB) {
        facebookReady();
    } else {
        window.fbAsyncInit = facebookReady;
    }

});

$(document).on('facebook:ready', function() {
    function checkPermisos(scopes) {
        if (scopes.indexOf("email") === -1 || scopes.indexOf("contact_email") === -1) {
            return false;
        } else {
            return true;
        }
    }
    var number_of_fb_auths = 0;
    function try_login_fb(response) {
        number_of_fb_auths++;
        if (response.authResponse) {
            grantedScopes = response.authResponse.grantedScopes;
            scopes = grantedScopes.split(",");
            if (checkPermisos(scopes)) {
                login_fb();
            } else {
                swal('Vaya, necesitamos tu e-mail para poder crear tu usuario en roomastic');
                if (number_of_fb_auths >= 3) {
                    swal('Bueno, siempre puedes crear una cuenta de otra manera :(');
                    number_of_fb_auths = 0;
                } else {
                    FB.login(try_login_fb, {
                        scope: 'email',
                        auth_type: 'rerequest',
                        return_scopes: true
                    });
                }
            }

        } else {
            swal('No nos has autorizado en Facebook :(');
        }
    }
    $('.login-rrss.facebook').on('click', function(evt) {
        evt.preventDefault();
        FB.login(try_login_fb, {scope: 'email', return_scopes: true});
    });
    /*
     FB.Event.subscribe('auth.login', function(response) {
     //window.location.reload();
     //alert('login')
     console.log('facebook login lanzado');
     login_fb();
     
     });
     FB.Event.subscribe('auth.logout', function(response) {
     //window.location.reload();
     //alert('logout')
     });
     */
});
if (typeof ROOMASTIC === 'undefined') {
    ROOMASTIC = {};
}

if (typeof ROOMASTIC.googleID !== 'undefined') {
    function googleReady() {
        ROOMASTIC.google_scopes = '';
        //ROOMASTIC.google_scopes = 'https://www.googleapis.com/auth/userinfo.profile';
        //ROOMASTIC.google_scopes += ' https://www.googleapis.com/auth/userinfo.email ';
        //ROOMASTIC.google_scopes += 'https://www.googleapis.com/auth/plus.me ';
        //ROOMASTIC.google_scopes += ' https://www.googleapis.com/auth/plus.login ';
        //ROOMASTIC.google_scopes += ' https://www.googleapis.com/auth/plus.profile.emails.read';
        ROOMASTIC.google_scopes += ' https://www.googleapis.com/auth/userinfo.email ';

        gapi.client.setApiKey(ROOMASTIC.googleapikey);
        //gapi.client.load('plus', 'v1', function() {});
    }
    function checkPermisos(scopes) {
        if (scopes.indexOf("email") === -1 || scopes.indexOf("contact_email") === -1) {
            return false;
        } else {
            return true;
        }
    }
    ROOMASTIC.response = [];
    ROOMASTIC.try_login_google_error = false;
    function try_login_google(response) {
        var data = {};
        //Compruebo que no haya errores en la respuesta
        if (typeof response.error === 'undefined') {
            if (response.code) {
                data.code = response.code;
                data.state = ROOMASTIC.state;
            }
            $.ajax({
                url: Routing.generate('TooltypAuthGoogleBundle_login'),
                type: "POST",
                data: JSON.stringify(data),
                success: function(response) {
                    ROOMASTIC.try_login_google_error = false;
                    ROOMASTIC.user_data = response.user_data;
                    if (ROOMASTIC.user_data['canOffer'] === 'si') {
                        hacerOferta();
                    } else if (ROOMASTIC.user_data['canOffer'] === 'faltan_datos') {
                        for (var propertyName in ROOMASTIC.user_data) {
                            $('#formregisterextra .' + propertyName).val(ROOMASTIC.user_data[propertyName]);
                            if (propertyName === 'email' && ROOMASTIC.user_data[propertyName].length > 0) {
                                $('#formregisterextra .' + propertyName).prop('readonly', true);
                            }
                        }
                        showRegisterExtra();
                    } else if (ROOMASTIC.user_data['canOffer'] === 'oferta_realizada_pendiente') {
                        swal('Tu usuario tiene una oferta pendiente');
                    }
                },
                error: function(data) {
                    if (false === ROOMASTIC.try_login_google_error) {
                        swal('Vaya, ha habido un error. Inténtalo de nuevo tras recargar la página');
                        ROOMASTIC.try_login_google_error = true;
                    }
                }
            });
        } else {
            if (false === ROOMASTIC.try_login_google_error) {
                swal('Vaya, parece que ha habido un error al autorizarnos :(. \n Intentalo de nuevo tras recargar la página');
                ROOMASTIC.try_login_google_error = true;
            }
        }
    }
    $(document).ready(function() {

        $('.login-rrss.google').on('click', function(evt) {
            var parametros = {
                'clientid': ROOMASTIC.googleID, //You need to set client id
                'cookiepolicy': 'single_host_origin',
                'callback': 'try_login_google', //callback function
                'approvalprompt': 'force',
                'scope': ROOMASTIC.google_scopes //'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read https://www.googleapis.com/auth/userinfo.email'
            };
            gapi.auth.signIn(parametros);

        });
        if (window.gapi && window.gapi.client) {
            googleReady();
            //$('.login-rrss.google').click();
        }
    });
}
if (typeof ROOMASTIC === 'undefined') {
    ROOMASTIC = {};
}

ROOMASTIC.urlSalir = Routing.generate('fos_user_security_logout');

function dateDiff(str1, str2) {
    var diff = Date.parse(str2) - Date.parse(str1);
    return isNaN(diff) ? NaN : {
        diff: diff,
        ms: Math.floor(diff % 1000),
        s: Math.floor(diff / 1000 % 60),
        m: Math.floor(diff / 60000 % 60),
        h: Math.floor(diff / 3600000 % 24),
        d: Math.floor(diff / 86400000)
    };
}
function validarOferta() {
    $form_oferta = $('#formoferta');
    if ($form_oferta.valid()) {
        return true;
    } else {
        $(ROOMASTIC.form_oferta.invalidElements()[0]).focus();
        if ($('#acepto1').valid() === false) {
            sweetAlert("Vaya...", "Tienes que aceptar nuestras condiciones antes de continuar!", "error");
        }
        return false;
    }
}
function showRegisterExtra() {
    $('.body').hide();
    $('.bodyregisterextra').show();
    $('.modalregister').show('fast');
}
function showLogin() {
    $('.bodyregister').hide();
    $('.body').show();
    $('.modalregister').show('fast');
    $('.clearForm').val('');
}
function showRegisterWeb() {
    $('.body').hide();
    $('.bodyregister').show();
    $('.modalregister').show('fast');
}
function hideRegLogModal() {
    $('.modalregister').hide();
    $('.bodyregister').hide();
    $('.body').show();
    $('.clearForm').val('');
}
function canMakeOffer() {
    if (typeof ROOMASTIC.user_data === 'undefined') {
        showLogin();
        return false;
    } else {
        if (ROOMASTIC.user_data['canOffer'] === 'si') {
            //Cambio el estilo del boton de enviar oferta
            //Intento hacer la oferta
            $('.btn_oferta').attr('title', 'Ya puedes hacer tu oferta');
            ////html('<a class="btn_oferta" href="#" id="submitoffer" title="" style="background:red!important" >hacer oferta</a>');
            return true;

        } else if (ROOMASTIC.user_data['canOffer'] === 'faltan_datos') {
            //Si no puedo muestro el modal de registro extra al que le precargo los datos que puedo
            showRegisterExtra();
            return false;

        } else if (ROOMASTIC.user_data['canOffer'] === 'no') {
            swal('Tu usuario no puede hacer ofertas :S');
            return false;
        } else if (ROOMASTIC.user_data['canOffer'] === 'oferta_realizada_pendiente') {
            swal('Tu usuario tiene una oferta pendiente');
            return false;
        }
    }
}

function hacerOferta() {
    if (validarOferta() && canMakeOffer()) {
        $form_oferta.submit();
        hideRegLogModal();
    } else {
        console.log('Oferta NO VALIDA');
    }
}
function logout() {
    $.ajax({
        url: Routing.generate('HotelesFrontendBundle_logout'),
        type: "GET",
        success: function(data) {
            console.log('Logout');
        },
        error: function(data) {
            console.log('Vaya, ha habido un error :S');
        }
    });
}
function calcularPrecio() {

    if (/^(\d+)$/.test($('#hoteles_backendbundle_ofertatype_preciohabitacion').val()) === false) {
        $('#hoteles_backendbundle_ofertatype_preciototaloferta').val(0);
        return;
    }

    var precio_por_noche = parseFloat($('#hoteles_backendbundle_ofertatype_preciohabitacion').val().replace('.', '').replace(' ', '').replace(',', '.'));

    if (precio_por_noche > 0 && $('#hoteles_backendbundle_ofertatype_numnoches').val() > 0) {
        $resultado = precio_por_noche * $('#hoteles_backendbundle_ofertatype_numnoches').val() * (
                parseInt($('#hoteles_backendbundle_ofertatype_numadultos').val()) +
                parseInt($('#hoteles_backendbundle_ofertatype_numninos').val())) *
                (parseInt($('#hoteles_backendbundle_ofertatype_numhabitaciones').val()));
        if (isNaN($resultado)) {
            $('#hoteles_backendbundle_ofertatype_preciototaloferta').val(0);
        } else {

            $resultado = $resultado.toLocaleString('es', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            });
            $('#hoteles_backendbundle_ofertatype_preciototaloferta').val($resultado);
        }
    } else {
        $('#hoteles_backendbundle_ofertatype_preciototaloferta').val(0);
    }
}
$(document).ready(function() {

    $('#hoteles_backendbundle_ofertatype_preciohabitacion').on('keyup', calcularPrecio);
    $('#hoteles_backendbundle_ofertatype_numninos').on('keyup', calcularPrecio);
    $('#hoteles_backendbundle_ofertatype_numadultos').on('keyup', calcularPrecio);
    $('#hoteles_backendbundle_ofertatype_numhabitaciones').on('keyup', calcularPrecio);
    $('#hoteles_backendbundle_ofertatype_fecha').daterangepicker({});
    $(document).on('click', 'td', function(evt) {
        //Usar esto $('#reservation').data().daterangepicker.startDate para sacar la fecha de inicio y final
        $input_fecha_element = $('#reservation');
        if ($input_fecha_element.length === 0) {
            $input_fecha_element = $('#hoteles_backendbundle_ofertatype_fecha');
        }
        $fecha_inicio = $input_fecha_element.data().daterangepicker.startDate.format('DD/MM/YY');
        $fecha_fin = $input_fecha_element.data().daterangepicker.endDate.format('DD/MM/YY');

        if ($('#hoteles_backendbundle_ofertatype_fecha').length !== 0) {
            //console.log(dateDiff($input_fecha_element.data().daterangepicker.startDate.format('MM/DD/YY'), $input_fecha_element.data().daterangepicker.endDate.format('MM/DD/YY')));
            $('#hoteles_backendbundle_ofertatype_numnoches').val(dateDiff($input_fecha_element.data().daterangepicker.startDate.format('MM/DD/YY'), $input_fecha_element.data().daterangepicker.endDate.format('MM/DD/YY')).d);
            calcularPrecio();
        }
        $input_fecha_element.val($fecha_inicio + ' - ' + $fecha_fin);
    });

});

jQuery.validator.addMethod("notEqual", function(value, element, param) {
    return this.optional(element) || value != param;
}, "");
jQuery.validator.addMethod("numeroEntero", function(value, element) {
    return this.optional(element) || /^(\d+)$/.test(value);
}, "");
jQuery.validator.addMethod("atLeastChecked", function(value, element, param) {
    return $(element).closest('form').find('input[type=checkbox]:checked').length > param;
});

$(document).ready(function() {
    ROOMASTIC.form_oferta = $('#formoferta').validate({
        onkeyup: false,
        focusCleanup: true,
        errorClass: "error",
        validClass: "valid",
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).addClass(validClass).removeClass(errorClass);
        },
        rules: {
            "hoteles_backendbundle_ofertatype[preciohabitacion]": {
                required: true,
                numeroEntero: true
            },
            "hoteles_backendbundle_ofertatype[numnoches]": {
                required: true,
                min: 1
            },
            "hoteles_backendbundle_ofertatype[numadultos]": {
                required: true,
                min: 1
            },
            "hoteles_backendbundle_ofertatype[numhabitaciones]": {
                required: true,
                min: 1
            },
            "hoteles_backendbundle_ofertatype[preciototaloferta]": {
                required: true,
                notEqual: "0"
            },
            "acepto_condiciones": {
                required: true
            },
            "hoteles[id][]": {
                atLeastChecked: 1
            }
        },
        messages: {
            'hoteles_backendbundle_ofertatype[preciohabitacion]': {
                required: '',
                min: ''
            },
            'hoteles_backendbundle_ofertatype[numnoches]': {
                required: '',
                min: ''
            },
            'hoteles_backendbundle_ofertatype[numadultos]': {
                required: '',
                min: ''
            },
            'hoteles_backendbundle_ofertatype[numhabitaciones]': {
                required: '',
                min: ''
            },
            'hoteles_backendbundle_ofertatype[preciototaloferta]': {
                required: '',
                min: ''
            },
            "hoteles[id][]": {
                atLeastChecked: ''
            },
            "acepto_condiciones": {
                required: ''
            }
        }
    });

    $('.btn_oferta').on('click', function(evt) {
        evt.preventDefault();
        if (validarOferta()) {
            $.ajax({
                url: Routing.generate('HotelesFrontendBundle_compruebaloginuserrss'),
                type: "GET",
                success: function(data) {
                    ROOMASTIC.user_data = data.user_data;
                    hacerOferta();
                },
                error: function(data) {
                    //$('.body').hide();
                    //$('.bodyregisterextra').show();
                    $('.modalregister').show('fast');
                }
            });
        }
    });

    $('.showregister').click(function(evt) {
        evt.preventDefault();
        showRegisterWeb();
    });
    $('.showlogin').click(function(evt) {
        evt.preventDefault();
        showLogin();
    });
    $('.close').click(function(evt) {
        evt.preventDefault();
        hideRegLogModal();
    });

    $('#formlogin').submit(function(evt) {
        evt.preventDefault();
        var url = $('#formlogin').attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: $("#formlogin").serialize(),
            success: function(data) {
                //Hago una llamada para ver si puedo hacer oferta, si puedo intento hacer el submit
                if (data.hasOwnProperty('success') && data.success === true) {
                    if (data.hasOwnProperty('oferta_form_token')) {
                        $('#hoteles_backendbundle_ofertatype__token_oferta').val(data.oferta_form_token);
                    }
                    ROOMASTIC.user_data = data.user_data;
                    hacerOferta();
                }
            },
            error: function(data) {
                $('.errorlogin').html('<p style="position:absolute; background-color:#fff; top:16px;padding: 3px 10px; border-radius: 3px; color: #000;">Usuario o pass incorrectas</p>');
            }
        });
        return false;
    });

    $('#formregister').submit(function(evt) {
        evt.preventDefault();
        $.ajax({
            type: "POST",
            url: Routing.generate('HotelesFrontendBundle_registroajax'),
            data: $("#formregister").serialize(),
            success: function(data) {
                if (data.hasOwnProperty('registertype__token')) {
                    $('#hoteles_backendbundle_registertype__token').val(data.registertype__token);
                }
                ROOMASTIC.user_data = data.user_data;
                hacerOferta();
            },
            error: function(data) {
                var message = '';
                if (data.responseJSON.hasOwnProperty('email')) {
                    message += 'Vaya, parece que ese e-mail esta ya en uso';
                }
                if (data.responseJSON.hasOwnProperty('dni')) {
                    message += '\n Vaya, parece que ya hay alguien registrado con tus datos';
                }
                if (data.responseJSON.hasOwnProperty('dni_invalid')) {
                    message += '\n Vaya, el DNI introducido es incorrecto';
                }
                if (data.responseJSON.hasOwnProperty('telefono_invalid')) {
                    message += '\n Vaya, el teléfono introducido debe tener 9 dígitos';
                }
                message === '' ? swal(data) : swal(message);
            }
        });
    });

    $('#formregisterextra').submit(function(evt) {
        evt.preventDefault();
        $.ajax({
            type: "POST",
            url: Routing.generate('HotelesFrontendBundle_registroextraajax'),
            data: $("#formregisterextra").serialize(),
            success: function(data) {
                ROOMASTIC.user_data = data.user_data;
                hacerOferta();
            },
            error: function(data) {
                var message = '';
                if (data.responseJSON.hasOwnProperty('email')) {
                    message += 'Vaya, parece que ese e-mail esta ya en uso';
                }
                if (data.responseJSON.hasOwnProperty('dni')) {
                    message += '\n Vaya, parece que ya hay alguien registrado con tus datos';
                }
                if (data.responseJSON.hasOwnProperty('dni_invalid')) {
                    message += '\n Vaya, el DNI introducido es incorrecto';
                }
                if (data.responseJSON.hasOwnProperty('telefono_invalid')) {
                    message += '\n Vaya, el teléfono introducido debe tener 9 dígitos';
                }
                swal(message);
            }
        });
    });

});