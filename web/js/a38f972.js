/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


//Google map api --> mapa del detalle del hotel
$(document).ready(function () {
    var map;
    function initialize() {
        var mapa_elem = document.getElementById('mapa_det');
        mapa_elem_long = mapa_elem.dataset.longitud;
        mapa_elem_lat = mapa_elem.dataset.latitud;
        mapa_elem_title = mapa_elem.dataset.titulo;
        latlng = new google.maps.LatLng(mapa_elem_lat, mapa_elem_long);
        var mapOptions = {
            zoom: 15,
            center: latlng //center: new google.maps.LatLng(mapa_elem_lat, mapa_elem_long)
        };
        map = new google.maps.Map(mapa_elem,
                mapOptions);
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: mapa_elem_title
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    if ($('.owl-carousel').length) {
        $('.owl-carousel').owlCarousel({
            loop: true,
            nav: false,
            autoplay: true,
            dots: false,
            //center: true,
            items: 1,
            slideSpeed: 200
        });
    }
});
