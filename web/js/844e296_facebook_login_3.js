if (typeof ROOMASTIC === 'undefined') {
    ROOMASTIC = {};
}

function login_fb() {
    $.ajax({
        url: Routing.generate('TooltypAuthFaceBundle_login'),
        type: "GET",
        success: function(data) {
            ROOMASTIC.user_data = data.user_data;
            if (ROOMASTIC.user_data['canOffer'] === 'si') {
                hacerOferta();
            } else if (ROOMASTIC.user_data['canOffer'] === 'faltan_datos') {
                for (var propertyName in ROOMASTIC.user_data) {
                    $('#formregisterextra .' + propertyName).val(ROOMASTIC.user_data[propertyName]);
                    if (propertyName === 'email' && ROOMASTIC.user_data[propertyName].length > 0) {
                        $('#formregisterextra .' + propertyName).prop('readonly', true);
                    }
                }
                showRegisterExtra();
            } else if (ROOMASTIC.user_data['canOffer'] === 'oferta_realizada_pendiente') {
                swal('Tu usuario tiene una oferta pendiente');
            }
        },
        error: function(data) {
            console.log(data);
            swal('Vaya, ha habido un error. Inténtalo de nuevo tras recargar la página');
        }
    });
}

$(document).ready(function() {
    function facebookReady() {
        FB.init({
            appId: ROOMASTIC.facebookAppId,
            status: true,
            cookie: true,
            xfbml: true,
            oauth: true
        });
        $(document).trigger("facebook:ready");
    }
    if (window.FB) {
        facebookReady();
    } else {
        window.fbAsyncInit = facebookReady;
    }

});

$(document).on('facebook:ready', function() {
    function checkPermisos(scopes) {
        if (scopes.indexOf("email") === -1 || scopes.indexOf("contact_email") === -1) {
            return false;
        } else {
            return true;
        }
    }
    var number_of_fb_auths = 0;
    function try_login_fb(response) {
        number_of_fb_auths++;
        if (response.authResponse) {
            grantedScopes = response.authResponse.grantedScopes;
            scopes = grantedScopes.split(",");
            if (checkPermisos(scopes)) {
                login_fb();
            } else {
                swal('Vaya, necesitamos tu e-mail para poder crear tu usuario en roomastic');
                if (number_of_fb_auths >= 3) {
                    swal('Bueno, siempre puedes crear una cuenta de otra manera :(');
                    number_of_fb_auths = 0;
                } else {
                    FB.login(try_login_fb, {
                        scope: 'email',
                        auth_type: 'rerequest',
                        return_scopes: true
                    });
                }
            }

        } else {
            swal('No nos has autorizado en Facebook :(');
        }
    }
    $('.login-rrss.facebook').on('click', function(evt) {
        evt.preventDefault();
        FB.login(try_login_fb, {scope: 'email', return_scopes: true});
    });
    /*
     FB.Event.subscribe('auth.login', function(response) {
     //window.location.reload();
     //alert('login')
     console.log('facebook login lanzado');
     login_fb();
     
     });
     FB.Event.subscribe('auth.logout', function(response) {
     //window.location.reload();
     //alert('logout')
     });
     */
});