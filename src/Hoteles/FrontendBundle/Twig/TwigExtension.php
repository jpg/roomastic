<?php

namespace Hoteles\FrontendBundle\Twig;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Hoteles\BackendBundle\Entity\Contacto;
use Hoteles\BackendBundle\Entity\Incidencia;

class TwigExtension extends \Twig_Extension
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Request
     */
    protected $request;

    /**
     * Constructor
     * 
     * @param ContainerInterface $container
     */
    public function __construct($container)
    {

        $this->container = $container;

        if ($this->container->isScopeActive('request')) {
            $this->request = $this->container->get('request');
        }
    }

    public function getFilters()
    {
        return array(
                //new \Twig_SimpleFilter('price', array($this, 'priceFilter')),
        );
    }

    public function getFunctions()
    {
        return array(
            'active_menu' => new \Twig_Function_Method($this, 'active_menu'),
            'incidencias_backend' => new \Twig_Function_Method($this, 'incidencias_backend'),
            'error' => new \Twig_Function_Method($this, 'error'),
            'contactos_backend' => new \Twig_Function_Method($this, 'contactos_backend'),
            'select' => new \Twig_Function_Method($this, 'select'),
            'errorgroup' => new \Twig_Function_Method($this, 'errorgroup'),
            'getRouteParam' => new \Twig_Function_Method($this, 'routerParams'),
            'getCurrentRouteParams' => new \Twig_Function_Method($this, 'getCurrentRouteParams'),
            'getRRSS' => new \Twig_Function_Method($this, 'getRRSS'),
        );
    }

    public function errorgroup($arrayerrores)
    {
        $error = '';
        foreach ($arrayerrores as $key => $value) {
            if ($value) {
                $error = 'errortab';
            }
        }
        return $error;
    }

    public function select($uno, $dos)
    {
        if ($uno == $dos) {
            return 'selected="selected';
        } else {
            return '';
        }
    }

    public function error($error)
    {
        if ($error) {
            return 'has-error';
        } else {
            return '';
        }
    }

    public function active_menu($category, $route, $format)
    {
        $status = '';
        if ($format == 'single') {
            if ($category == $route) {
                $status = 'active';
            }
        } elseif ($format == 'complex') {
            if ($category == $route ||
                    $category == $route . '_show' ||
                    $category == $route . '_new' ||
                    $category == $route . '_edit' ||
                    $category == $route . '_create' ||
                    $category == $route . '_update' ||
                    $category == $route . '_contraoferta') {
                $status = 'active';
            }
        }
        return $status;
    }

    public function incidencias_backend($user)
    {

        $resultado = array();
        foreach ($this->getIncidenciasForUser($user) as $key => $incidencia) {
            $resultado[] = array(
                'imagen' => $incidencia->getUsuariofrom()->getAvatarImage(),
                'userfrom' => $incidencia->getUsuariofrom()->getPublicUsername(),
                'fecha' => $incidencia->getFecha(),
                'asunto' => $incidencia->getAsunto(),
                'id' => $incidencia->getId()
            );
        }
        return $resultado;
    }

    public function contactos_backend()
    {

        $repository = $this->container->get('doctrine')->getRepository('HotelesBackendBundle:Contacto');
        $query = $repository->createQueryBuilder('c')
                ->andWhere('c.atendido = :no_atendido')
                ->setParameter('no_atendido', Contacto::NO_ATENDIDO)
                ->orderBy('c.fecha', 'ASC')
                ->getQuery();
        $resultado = $query->getResult();
        return $resultado;
    }

    public function getIncidenciasForUser($user)
    {

        $repository = $this->container->get('doctrine')->getRepository('HotelesBackendBundle:Incidencia');
        $query = $repository->createQueryBuilder('i')
                ->where('i.usuarioto = :usuarioto')
                ->andWhere('i.atendido = :no_atendido')
                ->setParameter('usuarioto', $user)
                ->setParameter('no_atendido', Incidencia::NO_ATENDIDO)
                ->orderBy('i.fecha', 'ASC')
                ->getQuery();
        return $query->getResult();
    }

    public function getCurrentRouteParams()
    {
        $router = $this->container->get('router');
        $request = $this->container->get('request');

        $routeName = $request->attributes->get('_route');
        $routeParams = $request->query->all();
        foreach ($router->getRouteCollection()->get($routeName)->compile()->getVariables() as $variable) {
            $routeParams[$variable] = $request->attributes->get($variable);
        }

        return $routeParams;
    }

    public function routerParams($param)
    {
        $router = $this->container->get('router');
        $request = $this->container->get('request');

        $routeName = $request->attributes->get('_route');
        $routeParams = $request->query->all();
        foreach ($router->getRouteCollection()->get($routeName)->compile()->getVariables() as $variable) {
            $routeParams[$variable] = $request->attributes->get($variable);
        }
        return isset($routeParams[$param]) ? $routeParams[$param] : null;
    }

    public function getRRSS()
    {
        $configuracion = $this->container->get('hoteles_backend.load_configuracion')->loadConfiguracion();
        $respuesta = array();
        $respuesta['twitter'] = $configuracion->getNombreTwitter();
        $respuesta['facebook'] = $configuracion->getNombreFacebook();
        return $respuesta;
    }

    public function getName()
    {
        return 'twig_extension';
    }

}
