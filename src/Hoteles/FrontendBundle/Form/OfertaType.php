<?php

namespace Hoteles\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class OfertaType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('preciohabitacion', 'integer')
                ->add('fecha')
                ->add('numnoches', 'integer')
                ->add('numadultos', 'integer')
                ->add('numninos', 'integer')
                ->add('numhabitaciones', 'integer')
                ->add('preciototaloferta')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_ofertatype';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'csrf_protection' => true,
            'csrf_field_name' => '_token_oferta',
            // Si esto se cambia tiene que verse reflejado en el AuthenticationHandler
            'intention' => 'oferta_type',
        );
    }

}
