<?php

namespace Hoteles\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class LoginType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('_username')
                ->add('_password')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_login';
    }

}
