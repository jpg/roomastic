<?php

namespace Hoteles\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class RegisterextraType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('email')
                ->add('nombre')
                ->add('apellidos')
                ->add('dni')
                ->add('direccioncompleta','text', array(
                    'required' => false                    
                ))
                ->add('telefono')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_registertype';
    }

}
