if (typeof ROOMASTIC === 'undefined') {
    ROOMASTIC = {};
}


var map_array = {};
var a;
$(document).ready(function () {
    a = function (self) {
        if (self.anchor) {
            self.anchor.fancybox();
        }
    };
    $(".pikame_listado").each(function () {
        $(this).PikaChoose({
            buildFinished: a,
            autoPlay: false});
    });
    if (ROOMASTIC.version_mv === true) {
        $('#tabsWithStyle').tabs();
        // setter
        $("#tabsWithStyle").tabs("option", "active", 0);
        $('#nav-options-open').on('click', function (evt) {
            $('.content_listado').hide();
            $('.opciones').toggle("drop");
        });
        $('.btn_volver').on('click', function (evt) {
            $('.opciones').toggle();
            $('.content_listado').show(500);
        });
        $('#fromDate').datepicker('setDate', ROOMASTIC.fecha_in);
        $('#toDate').datepicker('setDate', ROOMASTIC.fecha_out);
        $('input').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
        $("input.timbre").iCheck(timbreCheckboxConfig);

    }
    function initialize() {
        $('.mapa').each(function (index, mapa_elem) {
            mapa_elem_long = mapa_elem.dataset.longitud;
            mapa_elem_lat = mapa_elem.dataset.latitud;
            mapa_elem_title = mapa_elem.dataset.titulo;
            hotel_id = mapa_elem.dataset.hotelId;
            latlng = new google.maps.LatLng(mapa_elem_lat, mapa_elem_long);
            var mapOptions = {
                zoom: 15,
                center: latlng //center: new google.maps.LatLng(mapa_elem_lat, mapa_elem_long)
            };
            map = new google.maps.Map(mapa_elem,
                    mapOptions);
            map_array['mapa_hotel_' + hotel_id ] = map;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: mapa_elem_title
            });
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    mapa = $('.singlehotel .abrir_map').closest('.singlehotel').find('.desplegable_mapa').first();
    detalles = $('.singlehotel .detalle').closest('.singlehotel').find('.desplegable').first();
    $('.wrapper_hoteles').on('click', '.detalle', function (evt) {

        var detalles = $(this).closest('.singlehotel').find('.desplegable');
        if (detalles.is(':visible')) {

            detalles.toggle(400);
            mapa.hide(400);
        } else {

            $('.desplegable').hide(400);
            $('.desplegable_mapa').hide(400);
            detalles.toggle(400);
        }
        evt.preventDefault();
    });
    /*
     * Esta funcion se encarga de cambiar la imagen cuando haces click en una de ellas en el listado
     * Esto es debido a que pikachoose no funciona del todo bien con multiples instancias 
     * en la misma página
     * 
     */
    $('.wrapper_hoteles').on('click', '.thumbnail-pikachoose', function (evt) {
        var pikameId, photoIndex;
        pikameId = evt.target.dataset.pikameId;
        photoIndex = evt.target.dataset.photoIndex;
        $('#' + pikameId).data('pikachoose').GoTo(photoIndex);
        evt.preventDefault();
    });
    $('.wrapper_hoteles').on('click', '.abrir_map', function (evt) {
        var mapa = $(this).closest('.singlehotel').find('.desplegable_mapa');
        if (mapa.is(':visible')) {
            mapa.toggle(400);
        } else {
            $('.desplegable').hide(400);
            $('.desplegable_mapa').hide(400);
            mapa.toggle(400, function () {
                $dataset = mapa.find('.mapa').data();
                google_map = map_array['mapa_hotel_' + $dataset.hotelId];
                var center = google_map.getCenter();
                google.maps.event.trigger(google_map, 'resize');
                google_map.setCenter(center);
            });
        }
        evt.preventDefault();
    });
    $('.selectize-input input').blur();
    $('.checkbox-servicios').on('ifUnchecked', function (event) {
        actualizarFiltroServicios(event.target, 'del');
        applyFilters();
    });
    $('.checkbox-servicios').on('ifChecked', function (event) {
        actualizarFiltroServicios(event.target, 'add');
        applyFilters();
    });
    $('.checkbox-servicios').each(function (index, elem) {
        actualizarFiltroServicios(elem, 'add');
    }).promise().done(applyFilters);
    $('.checkbox-stars').on('ifUnchecked', function (event) {
        actualizarFiltroEstrellas(event.target, 'del');
        applyFilters();
    });
    $('.checkbox-stars').on('ifChecked', function (event) {
        actualizarFiltroEstrellas(event.target, 'add');
        applyFilters();
    });
    $('.checkbox-stars').each(function (index, elem) {
        actualizarFiltroEstrellas(elem, 'add');
    }).promise().done(applyFilters);
});
function unique(element, index, array) {
    return array.indexOf(element) === index;
}
var filtro_servicios = '';
var filtro_servicios_array = [];
function actualizarFiltroServicios(elem, tipo) {
    nuevo_filtro = elem.dataset.filter;
    if ('add' === tipo) {
        filtro_servicios_array.push('.' + nuevo_filtro);
        filtro_servicios_array = filtro_servicios_array.filter(unique);
    } else if ('del' === tipo) {
        index = filtro_servicios_array.indexOf('.' + nuevo_filtro);
        if (index > -1) {
            filtro_servicios_array.splice(index, 1);
        }
    }
    filtro_servicios = filtro_servicios_array.length > 0 ? filtro_servicios_array.join(",") : '';
}

var filtro_estrellas = '';
var filtro_estrellas_array = [];
function actualizarFiltroEstrellas(elem, tipo) {
    nuevo_filtro = elem.dataset.filter;
    if ('add' === tipo) {
        filtro_estrellas_array.push('.' + nuevo_filtro);
        filtro_estrellas_array = filtro_estrellas_array.filter(unique);
    } else if ('del' === tipo) {
        index = filtro_estrellas_array.indexOf('.' + nuevo_filtro);
        if (index > -1) {
            filtro_estrellas_array.splice(index, 1);
        }
    }
    filtro_estrellas = filtro_estrellas_array.length > 0 ? filtro_estrellas_array.join(",") : '';
}

function applyFilters() {

    $('.singlehotel').show();
    $('.singlehotel').not(filtro_estrellas).hide();
    $('.singlehotel').not(filtro_servicios).hide();
}

var procesando = false;
//Este evento lo uso para hacer la llamada al servidor
$(window).scroll(function () {
    var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
    var scrolltrigger = 0.80;
    if ((wintop / (docheight - winheight)) > scrolltrigger) {
        if (typeof last_page !== 'undefined' && last_page === false && procesando === false) {
            cargarHotelesAjax();
        }
    }
});
procesando = false;
last_page = false;
ultima_pagina = false;
function cargarHotelesAjax() {
    if(ultima_pagina === false){

            var url = $('#formajax').attr('action'); // El script a dónde se realizará la petición.
            //datos_formulario = $("#formajax").serializeArray();

            numero_pagina = $('#infinite-scroll-ajax').data().lastPage + 1;
            datos = [];
            datos.push({name: "numero_pagina", value: numero_pagina});
            //Le pido al servidor los nuevos hoteles
            if (false === procesando) {
                procesando = true;
                $.ajax({
                    type: "POST",
                    url: url,
                    data: $.param(datos), // Adjuntar los campos del formulario enviado.
                    success: function (data) {
                        
                            //Actualizo el numero de pagina que he pedido
                            $('#infinite-scroll-ajax').data().lastPage = $('#infinite-scroll-ajax').data().lastPage + 1;
                            //Adjunto los nuevos hoteles al listado
                            $html_listado = $(data.html);
                            //$html_listado.find('input.timbre').iCheck(timbreCheckboxConfig);
                            $html_listado.not(filtro_estrellas).hide();
                            $html_listado.not(filtro_servicios).hide();
                            //Aniado mapas y los centro en ese punto
                            $html_listado.find('.mapa').each(function (index, mapa_elem) {
                                //Cargo el mapa de cada uno de los nuevos elementos
                                mapa_elem_long = mapa_elem.dataset.longitud === "" ? 0 : mapa_elem.dataset.longitud;
                                mapa_elem_lat = mapa_elem.dataset.latitud === "" ? 0 : mapa_elem.dataset.latitud;
                                mapa_elem_title = mapa_elem.dataset.titulo;
                                hotel_id = mapa_elem.dataset.hotelId;
                                latlng = new google.maps.LatLng(mapa_elem_lat, mapa_elem_long);
                                var mapOptions = {
                                    zoom: 15,
                                    center: latlng //center: new google.maps.LatLng(mapa_elem_lat, mapa_elem_long)
                                };
                                map = new google.maps.Map(mapa_elem,
                                        mapOptions);
                                map_array['mapa_hotel_' + hotel_id ] = map;
                                var marker = new google.maps.Marker({
                                    position: latlng,
                                    map: map,
                                    title: mapa_elem_title
                                });
                            });
                            //Fancy box 
                            $html_listado.find(".pikame_listado").each(function () {
                                $(this).PikaChoose({
                                    buildFinished: a,
                                    autoPlay: false});
                            });
                            $html_lateral = $(data.html_lateral);
                            $html_lateral.find('input.checkbox-lateral').iCheck(checkboxLateralConfig);
                            $(".wrapper_hoteles").append($html_listado.html());
                            $("#listado-lateral-hoteles").append($html_lateral.html());
                            //applyFilters();
                            $("input.timbre").iCheck(timbreCheckboxConfig);
                            //$('input.checkbox-lateral').iCheck(checkboxLateralConfig);
                            configEventsTimbre();
                            procesando = false;
                        if (data.ultima_pagina !== false) {
                            ultima_pagina = true;
                        }
                    }, error: function (data) {
                        procesando = false;
                    }
                });
            }
    }

    return false;
}

