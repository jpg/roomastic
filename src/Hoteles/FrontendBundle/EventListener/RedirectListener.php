<?php

namespace Hoteles\FrontendBundle\EventListener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
//use Symfony\Component\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcher;

class RedirectListener
{

    protected $router;
    protected $security;
    protected $dispatcher;
    protected $container;

    public function __construct(Router $router, SecurityContext $security, EventDispatcher $dispatcher, $container)
    {
        //$this->container = $container;
        $this->router = $router;
        $this->security = $security;
        $this->dispatcher = $dispatcher;
        $this->container = $container;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        //No uso el user para nada
        $user = $event->getAuthenticationToken()->getUser();
        $request = $event->getRequest();
        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_ADMIN')) {
            $request->request->set('_target_path', $this->router->generate('admin_index'));
        } elseif ($securityContext->isGranted('ROLE_TIENDA')) {
            $request->request->set('_target_path', $this->router->generate('admin_tienda'));
        } elseif ($securityContext->isGranted('ROLE_USER')) {
            $request->request->set('_target_path', $this->router->generate('transat_i_bfront_carro'));
        }
    }

}
