<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\FrontendBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Hoteles\FrontendBundle\EventListener\MobileDetect;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Description of MobileDetectListener
 *
 * @author joaquin
 */
class MobileDetectListener
{

    const VERSION_FULL_VERSION = 'full';
    const VERSION_MOBILE_VERSION = 'mobile';
    const VERSION_DEFAULT_VERSION = 'default';

//put your code here
    public function onControllerDetected(FilterControllerEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST === $event->getRequestType()) {
            $request = $event->getRequest();
            $version = $request->query->get('version', self::VERSION_DEFAULT_VERSION);
            $version_cookie = $request->cookies->get('version', self::VERSION_DEFAULT_VERSION);

            if ($version !== 'default') {
                $request->cookies->set('version', $version);
                if ($version === self::VERSION_MOBILE_VERSION) {
                    $request->headers->set('ROOMASTIC-MV', TRUE);
                } else if ($version === self::VERSION_FULL_VERSION) {
                    $request->headers->set('ROOMASTIC-MV', FALSE);
                }
            } else {
                if ($version_cookie === self::VERSION_MOBILE_VERSION) {
                    $request->headers->set('ROOMASTIC-MV', TRUE);
                } else if ($version_cookie === self::VERSION_FULL_VERSION) {
                    $request->headers->set('ROOMASTIC-MV', FALSE);
                } else {
                    $headers = $request->headers->all();
                    $user_agent = $request->headers->get('User-Agent');
                    $mobileDetect = new MobileDetect($headers, $user_agent);
                    $isMobile = $mobileDetect->isMobile() || $mobileDetect->isTablet();
                    $request->headers->set('ROOMASTIC-MV', $isMobile);
                }
            }
        }
        return;
    }

    public function onResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();
        $request = $event->getRequest();
        $version = $request->query->get('version', self::VERSION_DEFAULT_VERSION);
        if($version === self::VERSION_FULL_VERSION || $version === self::VERSION_MOBILE_VERSION){
            $cookie = new Cookie('version', $version);
            $response->headers->setCookie($cookie);
        }
        return;
    }

}
