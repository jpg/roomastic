<?php

namespace Hoteles\FrontendBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsDni extends Constraint
{
    public $message = 'El DNI no es válido.';
}