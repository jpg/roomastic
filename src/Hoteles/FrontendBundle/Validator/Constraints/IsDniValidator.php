<?php

namespace Hoteles\FrontendBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsDniValidator extends ConstraintValidator {
    /* public function validate($value, Constraint $constraint)
      {
      $this->context->addViolation(
      $constraint->message,
      array('%string%' => $value)
      );
      } */

    public function isValid($value, Constraint $constraint) {
        
        if ($value==null) return true;
        
        if(!$this->is_valid_dni_nie($value)){
            $this->setMessage($constraint->message, array(
                
            ));

            return false;
        }
        return true;

        
    }

    private function is_valid_dni_nie($string) {
        if (strlen($string) != 9 ||
                preg_match('/^[XYZ]?([0-9]{7,8})([A-Z])$/i', $string, $matches) !== 1) {

            return false;
        }

        $map = 'TRWAGMYFPDXBNJZSQVHLCKE';

        list(, $number, $letter) = $matches;

        return strtoupper($letter) === $map[((int) $number) % 23];
    }

}
