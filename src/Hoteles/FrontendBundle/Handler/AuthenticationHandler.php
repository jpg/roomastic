<?php

namespace Hoteles\FrontendBundle\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class AuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{

    protected $router;
    protected $security;
    protected $userManager;
    protected $service_container;

    public function __construct(RouterInterface $router, SecurityContext $security, $userManager, $service_container)
    {
        $this->router = $router;
        $this->security = $security;
        $this->userManager = $userManager;
        $this->service_container = $service_container;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if ($request->isXmlHttpRequest()) {
            $result = array('success' => true);
            $user_data = ($token->getUser()->getTypeOf() === 'basico') ? $token->getUser()->getUserData($this->service_container->getParameter('limite_ofertas')) : '';
            $result['user_data'] = $user_data;
            //Cualquier cambio en oferta_type tiene que verse reflejado aqui
            $result['oferta_form_token'] = $this->service_container->get('form.csrf_provider')->generateCsrfToken('oferta_type');
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            // Create a flash message with the authentication error message
            //$request->getSession()->getFlashBag()->set('error', $exception->getMessage());
            $url = ($token->getUser()->getTypeOf() === 'basico')? $request->headers->get('referer') : $this->router->generate('admin_landing');

            return new RedirectResponse($url);
        }
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {

        if ($request->isXmlHttpRequest()) {
            $result = array('success' => false, 'message' => $exception->getMessage());
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            $response->setStatusCode(400);
            return $response;
        } else {
            $url = $this->router->generate('fos_user_security_login');
            return new RedirectResponse($url, 302);
        }
        return new Response();
    }

}
