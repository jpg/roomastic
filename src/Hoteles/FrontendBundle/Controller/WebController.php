<?php

namespace Hoteles\FrontendBundle\Controller;

use Hoteles\BackendBundle\Entity\Contacto;
use Hoteles\BackendBundle\Form\ContactowebType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Hoteles\BackendBundle\Entity\Hotel;
use Hoteles\FrontendBundle\EventListener\MobileDetectListener;
use Hoteles\FrontendBundle\Form\RegisterNewsletterType;


class WebController extends Controller
{

    public function indexAction(Request $request)
    {

        $formRegister = $this->createForm(new RegisterNewsletterType(), null);
        
        $entorno = $this->get('hoteles_backend.load_configuracion')
                ->loadConfiguracion()
                ->getEntorno();
        $home_repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Home');
        $imageneshome = $home_repository->createQueryBuilder('h')
                        ->where('h.status = :status')
                        ->setParameter('status', 1)
                        ->orderBy('h.posicion', 'ASC')
                        ->getQuery()->getResult();

        if ($entorno == 'demo') {
            return $this->render('HotelesFrontendBundle:Temporal:index.html.twig', array(
                        'imageneshome' => $imageneshome,
                        'seo' => $this->seo('Home'),
            ));
        }

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Hotel');
        $hoteles_query = $repository->createQueryBuilder('h');
        $hoteles_query->where('h.status = :status')->setParameter('status', Hotel::HOTEL_STATUS_VISIBLE);
        $hoteles = $hoteles_query->getQuery()->getResult();
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';

        return $this->render('HotelesFrontendBundle:Frontend:index.' . $format . '.twig', array(
                    'imageneshome' => $imageneshome,
                    'seo' => $this->seo('Home'),
                    'hoteles' => $hoteles,
                    'formRegister' => $formRegister->createView(),
        ));
    }
    
    public function quienesSomosAction(Request $request)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $entities = $em->getRepository('HotelesBackendBundle:Quienessomos')->findAll();
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';

        return $this->render('HotelesFrontendBundle:Frontend:quienesSomos.' . $format . '.twig', array(
                    'content' => (isset($entities[0])) ? $entities[0] : '',
                    'seo' => $this->seo('Quienes Somos'),
        ));
    }

    public function condicionesLegalesAction(Request $request)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $entities = $em->getRepository('HotelesBackendBundle:Condicioneslegales')->findAll();
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';

        return $this->render('HotelesFrontendBundle:Frontend:condicionesLegales.' . $format . '.twig', array(
                    'content' => (isset($entities[0])) ? $entities[0] : '',
                    'seo' => $this->seo('Condiciones Legales'),
        ));
    }

    public function privacidadAction(Request $request)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $entities = $em->getRepository('HotelesBackendBundle:Privacidad')->findAll();
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';

        return $this->render('HotelesFrontendBundle:Frontend:privacidad.' . $format . '.twig', array(
                    'content' => (isset($entities[0])) ? $entities[0] : '',
                    'seo' => $this->seo('Privacidad'),
        ));
    }

    public function cookiesAction()
    {

        $em = $this->getDoctrine()->getEntityManager();
        $entities = $em->getRepository('HotelesBackendBundle:Cookies')->findAll();

        return $this->render('HotelesFrontendBundle:Frontend:cookies.html.twig', array(
                    'content' => (isset($entities[0])) ? $entities[0] : '',
                    'seo' => $this->seo('Cookies'),
        ));
    }

    public function faqsAction()
    {

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Faqs');

        $query = $repository->createQueryBuilder('f')
                ->orderBy('f.posicion', 'ASC')
                ->getQuery();

        $faqs = $query->getResult();

        return $this->render('HotelesFrontendBundle:Frontend:faqs.html.twig', array(
                    'faqs' => $faqs,
                    'seo' => $this->seo('Faqs'),
        ));
    }

    public function publicaTuHotelAction(Request $request)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $contenido = $em->getRepository('HotelesBackendBundle:Publicatuhotel')->findAll();

        $nuevo_contacto = new Contacto();
        $form = $this->createForm(new ContactowebType(), $nuevo_contacto);
        if ($request->getMethod() === 'POST') {
            $form->bindRequest($request);

            if ($form->isValid()) {
                $nuevo_contacto->setAtendido(false);
                $nuevo_contacto->setFecha(new \Datetime());
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($nuevo_contacto);
                $em->flush();
                $this->get('session')->setFlash('notice', 'Mensaje enviado');

                return $this->redirect($this->generateUrl('HotelesFrontendBundle_publicaTuHotel'));
            }
        }
        return $this->render('HotelesFrontendBundle:Frontend:publicaTuHotel.html.twig', array(
                    'content' => (isset($contenido[0])) ? $contenido[0] : '',
                    'seo' => $this->seo('Publica Tu Hotel'),
                    'form' => $form->createView(),
        ));
    }

    public function contactoAction(Request $request)
    {

        $nuevo_contacto = new Contacto();
        $form = $this->createForm(new ContactowebType(), $nuevo_contacto);
        //$request = $this->getRequest();
        //$form    = $this->createForm(new ContactoType(), $entity);
        if ($request->getMethod() === 'POST') {
            $form->bindRequest($request);

            if ($form->isValid()) {
                $nuevo_contacto->setAtendido(false);
                $nuevo_contacto->setFecha(new \Datetime());
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($nuevo_contacto);
                $em->flush();
                $this->get('session')->setFlash('notice', 'Mensaje enviado');

                return $this->redirect($this->generateUrl('HotelesFrontendBundle_contacto'));
            }
        }
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';

        return $this->render('HotelesFrontendBundle:Frontend:contacto.' . $format . '.twig', array(
                    'form' => $form->createView(),
                    'seo' => $this->seo('Contacto'),
        ));
    }

    public function mapawebAction()
    {
        return $this->render('HotelesFrontendBundle:Frontend:mapaweb.html.twig', array(
                    'seo' => $this->seo('Mapa web'),
        ));
    }

    public function seo($seccionName)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $seo = $em->getRepository('HotelesBackendBundle:Seo')->findOneBy(array('seccion' => $seccionName));
        return $seo;
    }

    public function renderAction($templateName)
    {
        //return $this->render('HotelesFrontendBundle:Frontend:'.$templateName.'.html.twig');
        //return $this->render('HotelesBackendBundle:Mail:' . $templateName . '.html.twig');
    }
}
