<?php

namespace Hoteles\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Hoteles\BackendBundle\Entity\Contacto;
use Hoteles\BackendBundle\Entity\UserBasico;
use Hoteles\BackendBundle\Entity\User;
use Hoteles\BackendBundle\Entity\Hotel;
use Hoteles\BackendBundle\Form\ContactowebType;
use Hoteles\BackendBundle\Entity\Oferta;
use Hoteles\FrontendBundle\Form\OfertaType;
use Hoteles\FrontendBundle\Form\RegisterType;
use Hoteles\FrontendBundle\Form\RegisterextraType;
use Hoteles\FrontendBundle\Form\RegisterNewsletterType;
use Tooltyp\AuthFaceBundle\Facebook\Facebook;
use Hoteles\BackendBundle\Interfaces\UserCanOfferInterface;
use Hoteles\FrontendBundle\Services\ForceLogin;
use Hoteles\BackendBundle\Entity\Newsletter;;

class FrontendController extends Controller
{

    public function detallehotelAction($nombrehotel, Request $request)
    {

        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';

        $em = $this->getDoctrine()->getEntityManager();
        $hotel = $em->getRepository('HotelesBackendBundle:Hotel')->findoneBy(array('url' => $nombrehotel));

        return $this->render('HotelesFrontendBundle:Frontend:hotel.' . $format . '.twig', array(
                    'hotel' => $hotel,
        ));
    }

    public function compruebaloginAction()
    {
        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($this->container->get('security.context')->getToken()->getUser());
        if ($user) {
            $response = array('loggedin' => true);
            return $this->JSONResponse($response);
        } else {
            $response = array('loggedin' => false);
            return $this->JSONResponse($response, 400);
        }
    }

    public function logoutAction()
    {
        $this->get('security.context')->setToken(null);
        $this->get('request')->getSession()->invalidate();
        return new Response('1');
    }

    public function JSONResponse($response, $code = 200)
    {
        $respuesta_json = new Response(json_encode($response), $code);
        $respuesta_json->headers->set('Content-Type', 'application/json');
        return $respuesta_json;
    }

    public function compruebaloginuserrssAction()
    {

        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($this->container->get('security.context')->getToken()->getUser());
        $limite_ofertas = $this->container->getParameter('limite_ofertas');
        if ($user && $user->canOffer($limite_ofertas) === UserCanOfferInterface::PUEDE_OFERTAR) {
            $response = array();
            $response['valid'] = User::USER_EXTRA_INFORMATION_VALID;
            $response['user_data'] = $user instanceof UserBasico ? $user->getUserData($limite_ofertas) : 'undefined';
            return $this->JSONResponse($response);
        } else {
            $response = array();
            $response['valid'] = User::USER_EXTRA_INFORMATION_INVALID;
            return $this->JSONResponse($response, 400);
        }
    }

    public function getUserDataAction()
    {
        $limite_ofertas = $this->container->getParameter('limite_ofertas');

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user instanceof \Hoteles\BackendBundle\Entity\UserBasico) {
            $response['user_data'] = $user->getUserData($limite_ofertas);
        } else {
            $response['user_data'] = array();
        }
        $respuesta_json = new Response(json_encode($response), 200);
        $respuesta_json->headers->set('Content-Type', 'application/json');
        return $respuesta_json;
    }

    public function ofertaAction(Request $request)
    {    
        $oferta_form = $this->createForm(new OfertaType(), null);
        $em = $this->getDoctrine()->getEntityManager();
        if ($request->getMethod() == 'GET') {
            $all_params = $request->query;
            $id_hoteles = preg_split("/,/", $request->get('hoteles', -1));
            $start_date = \DateTime::createFromFormat('d/m/y', $request->get('fecha_inicio', -1));
            $end_date = \DateTime::createFromFormat('d/m/y', $request->get('fecha_fin', -1));

            $valid_data = true;
            if (is_array($id_hoteles)) {
                foreach ($id_hoteles as $id) {
                    //Compruebo que todos son números
                    if (!is_numeric($id)) {
                        //No es un entero, salgo
                        $valid_data = false;
                        break;
                    }
                }
            } else if (is_numeric($id_hoteles)) {
                //Es un id, asi que lanzo la query
                $valid_data = true;
            } else {
                $valid_data = false;
            }

            if (!$valid_data) {
                throw $this->createNotFoundException('Error con los datos');
                //Redirijo al inicio
                $this->get('session')->setFlash('error', 'Hay un error con la Id de los hoteles.');
                error_log('Hay un error con la Id de los hoteles.');
                return $this->redirect($this->generateUrl('HotelesFrontendBundle_homepage'));
            }

            //Compruebo que la fecha de comienzo es 48 horas posterior a la del día actual
            //Mirando horas, minutos y segundos
            //Meterlo en un parámetro
            /* $fecha_limite = new \DateTime();
              $fecha_limite->setTime(0, 0, 0);
              $fecha_limite->modify('+53 hours');
              if ($start_date === FALSE || $end_date === FALSE ||
              $fecha_limite > $start_date) {
              throw $this->createNotFoundException('Error con los datos');
              //Redirijo al inicio
              $this->get('session')->setFlash('error', 'Esa fecha ya no es válida.');
              return $this->redirect($this->generateUrl('HotelesFrontendBundle_homepage'));
              } */

            if ($start_date >= $end_date) {
                //Redirijo al inicio
                $this->get('session')->setFlash('error', 'Fecha de inicio posterior a fecha de fin.');
                error_log('Fecha de inicio posterior a fecha de fin.');
                return $this->redirect($this->generateUrl('HotelesFrontendBundle_homepage'));
            }
            //Miro para cargar los hoteles
            $repo = $em->getRepository('HotelesBackendBundle:Hotel');
            $hoteles = $repo->getByIdArray($id_hoteles);
            if ($valid_data && $hoteles) {                ;
                if(sizeof($hoteles) > 1){
                    $precio_hotel_min = min(array_map(function($hotel) {
                                return $hotel['pvpoficialenteros'] + $hotel['pvpoficialenteros'] / 100;
                            }, $hoteles));
                    $precio_hotel_max = max(array_map(function($hotel) {
                                return $hotel['pvpoficialenteros'] + $hotel['pvpoficialenteros'] / 100;
                            }, $hoteles));   
                }
                else { // Si solo se selecciono un hotel los precios maximo y minimo dependen de un porcentaje configurado
                    $porcentaje = $this->get('hoteles_backend.load_configuracion')
                                       ->loadConfiguracion()
                                       ->getPorcentajePrecioHotel();
                   $precio_hotel_min = round($hoteles[0]['pvpoficialenteros']-(($hoteles[0]['pvpoficialenteros']*$porcentaje) / 100), 2);
                   $precio_hotel_max = round($hoteles[0]['pvpoficialenteros']+(($hoteles[0]['pvpoficialenteros']*$porcentaje) / 100), 2);
                }
                
                $num_noches = $start_date->diff($end_date)->format('%a');
            } else {
                //throw $this->createNotFoundException('Error con los datos');
                //$this->get('session')->setFlash('error', 'Ha habido un problema al cargar los hoteles.');
                error_log('Problema al cargar los hoteles.');
                return $this->redirect($this->generateUrl('HotelesFrontendBundle_homepage'));
            }
        }
        if ($request->getMethod() == 'POST') {
            $securityContext = $this->container->get('security.context');
            $user = $securityContext->getToken()->getUser();
            //Compruebo si el usuario esta baneado o suspendido

            if (($user->isBanned() || $user->isSuspended()) && $this->container->getParameter('baneo_usuarios') === 'si') {
                $this->redirect($this->generateUrl('HotelesFrontendBundle_usuariobaneado'));
            }
            if ($user->hasOfferMade() && $this->container->getParameter('limite_ofertas') === 'si') {
                //Alguien ha llegado hasta aquí y no debería asi que lo mando a la pagina de error
                return $this->createNotFoundException();
            }
            $oferta_form->bindRequest($request);
            if ($oferta_form->isValid()) {  
                //Este es el id del grupo de ofertas que se van a generar
                $id_grupo_ofertas = sha1(uniqid(mt_rand(), true));

                // hay que comprobar que viajen hoteles

                $arrayhoteles = $request->request->get("hoteles[id]", null, true);
                $fechainout = explode(' - ', $oferta_form["fecha"]->getData());
                $fecha_entrada = \DateTime::createFromFormat('j/m/y', $fechainout[0]);
                $fecha_salida = \DateTime::createFromFormat('j/m/y', $fechainout[1]);

                $fechaOferta = new \Datetime();
                foreach ($arrayhoteles as $hotel_id) {

                    $hotel = $this->getHotel($hotel_id);
                    //ladybug_dump($hotel);
                    $oferta_manager = $this->get('hoteles_backend.oferta_manager');
                    $oferta = $oferta_manager->createOffer(
                            $user, $hotel, $fechaOferta, $oferta_form["preciohabitacion"]->getData(), $fecha_entrada, $fecha_salida, $oferta_form["numadultos"]->getData(), $oferta_form["numninos"]->getData(), $oferta_form["numhabitaciones"]->getData(), $id_grupo_ofertas
                    );

                    $em->persist($oferta);
                    //Si la oferta es valida, notifico con un e-mail al hotel
                    if ($oferta->getStatus() === Oferta::OFERTA_VALIDA) {
                        $this->get('oferta.notice_mailer')->notifyHotelOffer($oferta);
                    }
                }

                //Marco el usuario como que tiene una oferta pendiente
                $user->offerMade();
                $em->persist($user);
                if ($request->request->get('acepto_suscribirme', false)) {
                    $newsletter_manager = $this->get('roomastic.newsletter_manager');
                    $newsletter_manager->addUser($user);
                }
                $em->flush();
                return $this->redirect($this->generateUrl('HotelesFrontendBundle_ofertarealizada'));
            } else {
                $csrf_valido = $this->container->get('form.csrf_provider')->generateCsrfToken('oferta_type');
                $csrf_invalido = $oferta_form['_token_oferta']->getData();
                $errors = $oferta_form->getErrors();
            }
        }

        $formRegister = $this->createForm(new RegisterType(), null);
        $formRegisterextra = $this->createForm(new RegisterextraType(), null);

        $csrfToken = $this->container->has('form.csrf_provider') ? $this->container->get('form.csrf_provider')->generateCsrfToken('authenticate') : null;

        $configuracion = $em->getRepository('HotelesBackendBundle:Configuracion')->findAll();
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';

        return $this->render('HotelesFrontendBundle:Frontend:oferta.' . $format . '.twig', array(
                    'fecha_inicio' => $start_date,
                    'fecha_fin' => $end_date,
                    'precio_hotel_min' => $precio_hotel_min,
                    'precio_hotel_max' => $precio_hotel_max,
                    'num_noches' => $num_noches,
                    'form' => $oferta_form->createView(),
                    'formRegister' => $formRegister->createView(),
                    'formRegisterextra' => $formRegisterextra->createView(),
                    'hoteles' => $hoteles,
                    'csrf_token' => $csrfToken,
                    'configuracion' => $configuracion[0],
        ));
    }

    public function registroajaxAction(Request $request)
    {

        $em = $this->getDoctrine()->getEntityManager();

        $request = $this->getRequest();
        $user = new \Hoteles\BackendBundle\Entity\UserBasico();
        $registro_form = $this->createForm(new RegisterType(), $user);
        $registro_form->bindRequest($request);
        
        $entities = $em->getRepository('HotelesBackendBundle:User')
                ->findBy(array('email' => $registro_form["email"]->getData(),
                               'status' =>\Hoteles\BackendBundle\Entity\User::USER_STATUS_ONLY_NEWSLETTER));
        
        if (count($entities) > 0) {
            // El usuario existe como usuario de newsletter, pero hay que darlo de alta como usuario normal
            
            $id = $entities[0]->getId();
            //die(strval($id));
            $user = $em->getRepository('HotelesBackendBundle:User')->find(strval($id));

            if (!$user) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $registro_form = $this->createForm(new RegisterType(), $user);
  
            $registro_form->bindRequest($request);

        }        
        
        if ($registro_form->isValid()) {

            //ladybug_dump($user); exit;
            $user->setUsername($registro_form["email"]->getData());
            $user->setEmail($registro_form["email"]->getData());
            $user->setRoles(array('ROLE_WEB'));
            $user->setLastLogin(new \DateTime('now'));

            $user->setNombre($registro_form["nombre"]->getData());
            $user->setApellidos($registro_form["apellidos"]->getData());
            $user->setDni($registro_form["dni"]->getData());
            $user->setDireccioncompleta($registro_form["direccioncompleta"]->getData());
            $user->setTelefono($registro_form["telefono"]->getData());
            $user->setPlainPassword($registro_form["password"]["first"]->getData());

            $token = sha1(uniqid(mt_rand(), true));
            $user->setConfirmationToken($token);
            $em->persist($user);
            $validator = $this->get('validator');
            $errors = $validator->validate($user);
            if (count($errors) > 0) {
               $response = array();
                foreach ($errors as $error){
                        $response[$error->getPropertyPath()."_invalid"][0]=$error->getMessage();
                }
                return $this->JSONResponse($response, 400);
            } else {
                $a = $this->container;
                $force_login = $this->get('user.force_login');
                $logger = $this->get('tpv.logger');
                $mailer = $this->get('hoteles_backend.mailerfosuser_global');
                try {
                    $em->flush();
                } catch (\Exception $e) {
                    $response = array('Error' => 'Error desconocido');
                    return $this->JSONResponse($response, 400);
                }
                //Logueamos al usuario
                $force_login->forceUserLogin($this->getRequest(), $user);
                // mandamos mail de bienvenida
                $mailer->sendWelcomeEmailMessage($user);
                $response = array();
                $limite_ofertas = $this->container->getParameter('limite_ofertas');

                $response ['user_data'] = $user->getUserData($limite_ofertas);
                $response['canOffer'] = $user->canOffer();
                return $this->JSONResponse($response);
            }
        } else {
            $response = $this->getErrorMessages($registro_form);
            return $this->JSONResponse($response, 400);
        }
    }
    
    public function registroextraajaxAction(Request $request)
    {

        $securityContext = $this->container->get('security.context');
        $token = $securityContext->getToken();
        $user = $token->getUser();
        $request = $this->getRequest();
        $form = $this->createForm(new RegisterextraType(), null);
        $form->bindRequest($request);

        if ($form->isValid()) {

            $user->setEmail($form["email"]->getData());
            $user->setNombre($form["nombre"]->getData());
            $user->setApellidos($form["apellidos"]->getData());
            $user->setDni($form["dni"]->getData());
            $user->setDireccioncompleta($form["direccioncompleta"]->getData());
            $user->setTelefono($form["telefono"]->getData());

            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($user);
            try {
                $em->flush();
            } catch (PDOException $e) {
                
            }
            // mandamos mail de confirmacion
            /* $mailer = $this->container->get('fos_user.mailer');
              $mailer->sendConfirmationEmailMessage($user); */
            $response = array();
            $limite_ofertas = $this->container->getParameter('limite_ofertas');

            $response ['user_data'] = $user->getUserData($limite_ofertas);
            $response['canOffer'] = $user->canOffer();
            return $this->JSONResponse($response);
        } else {
            $response = $this->getErrorMessages($form);
            return $this->JSONResponse($response, 400);
        }


        exit;
        return $this->render('HotelesFrontendBundle:Frontend:ofertarealizada.html.twig', array(
        ));
    }

    public function registronewsletterajaxAction(Request $request)
    {
        $request = $this->getRequest();
        
        $user = new \Hoteles\BackendBundle\Entity\UserBasico();
        $registro_form = $this->createForm(new RegisterNewsletterType(), $user);
        $registro_form->bindRequest($request);

        if ($registro_form->isValid()) {


            //ladybug_dump($user); exit;
            $user->setUsername($registro_form["email"]->getData());
            $user->setEmail($registro_form["email"]->getData());
            $user->setRoles(array('ROLE_WEB'));

            $user->setNombre(null);
            $user->setApellidos(null);
            $user->setDni(null);
            $user->setDireccioncompleta(null);
            $user->setTelefono(null);
            $user->setPlainPassword($registro_form["email"]->getData());
            $user->setStatus(\Hoteles\BackendBundle\Entity\User::USER_STATUS_ONLY_NEWSLETTER);

            $em = $this->getDoctrine()->getEntityManager();

            $token = sha1(uniqid(mt_rand(), true));
            $user->setConfirmationToken($token);
            
            $validator = $this->get('validator');
            
            $em->persist($user);
            $errors = $validator->validate($user);

           
            try {
                    $em->flush();
                } catch (\Exception $e) {
                    $response = array('Error' => 'La dirección de email ya está en uso');
                    return $this->JSONResponse($response, 400);
                }
            //ladybug_dump($user); exit;

            $newsletter_manager = $this->get('roomastic.newsletter_manager');
            $newsletter_manager->addUser($user);
            
            $response = array();
            return $this->JSONResponse($response);
  
        } else {
            $response = $this->getErrorMessages($registro_form);
            return $this->JSONResponse($response, 400);
        }
    }
    
    private function getErrorMessages(\Symfony\Component\Form\Form $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $key => $error) {
            $template = $error->getMessageTemplate();
            $parameters = $error->getMessageParameters();

            foreach ($parameters as $var => $value) {
                $template = str_replace($var, $value, $template);
            }

            $errors[$key] = $template;
        }
        if ($form->hasChildren()) {
            foreach ($form->getChildren() as $child) {
                if (!$child->isValid()) {
                    $errors[$child->getName()] = $this->getErrorMessages($child);
                }
            }
        }

        return $errors;
    }

    public function ofertarealizadaAction(Request $request)
    {
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';

        return $this->render('HotelesFrontendBundle:Frontend:ofertarealizada.' . $format . '.twig');
    }

    public function usuarioBaneadoAction(Request $request)
    {
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';
        return $this->render('HotelesFrontendBundle:Frontend:baneo.' . $format . '.twig');
    }

    public function getHotel($idHotel)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $hotel = $em->getRepository('HotelesBackendBundle:Hotel')->find($idHotel);
        return $hotel;
    }
  
}
