<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Condicioneslegales;
use Hoteles\BackendBundle\Form\CondicioneslegalesType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Condicioneslegales controller.
 *
 * @Route("/admin/export")
 */
class ExportController extends Controller
{

    /**
     * Lists all Condicioneslegales entities.
     *
     * @Route("/newsletter")
     */
    public function exportNewsletter()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $usuarios_newsletter = $em->getRepository('HotelesBackendBundle:Newsletter')->findBy(array('activo' => true));
        $resultado = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><title></title></head><body><table width="200">';
        foreach ($usuarios_newsletter as $usuario_newsletter) {
            $resultado .= $usuario_newsletter->exportToExcel();
        }
        $resultado .= '</table></body></html>';

        $response = new Response();
        $response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $response->headers->set('Expires', '0');
        $response->headers->set('Content-type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', sprintf('attachment;filename="%s";', 'Newsletter.xls'));
        $response->headers->set('Content-length', strlen($resultado));
        $response->prepare();
        $response->sendHeaders();
        $response->setContent($resultado);
        return $response;
    }

}
