<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Imagenhotel;
use Hoteles\BackendBundle\Form\ImagenhotelType;

/**
 * Imagenhotel controller.
 *
 * @Route("/admin/imagenhotel")
 */
class ImagenhotelController extends Controller
{

    /**
     * Lists all Imagenhotel entities.
     *
     * @Route("/", name="admin_imagenhotel")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:Imagenhotel')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Imagenhotel entity.
     *
     * @Route("/{id}/show", name="admin_imagenhotel_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Imagenhotel')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Imagenhotel entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Imagenhotel entity.
     *
     * @Route("/new", name="admin_imagenhotel_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Imagenhotel();
        $form = $this->createForm(new ImagenhotelType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Imagenhotel entity.
     *
     * @Route("/create", name="admin_imagenhotel_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Imagenhotel:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Imagenhotel();
        $request = $this->getRequest();
        $form = $this->createForm(new ImagenhotelType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_imagenhotel_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Imagenhotel entity.
     *
     * @Route("/{id}/edit", name="admin_imagenhotel_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Imagenhotel')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Imagenhotel entity.');
        }

        $editForm = $this->createForm(new ImagenhotelType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Imagenhotel entity.
     *
     * @Route("/{id}/update", name="admin_imagenhotel_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Imagenhotel:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Imagenhotel')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Imagenhotel entity.');
        }

        $editForm = $this->createForm(new ImagenhotelType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_imagenhotel_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Imagenhotel entity.
     *
     * @Route("/{id}/delete", name="admin_imagenhotel_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Imagenhotel')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Imagenhotel entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_imagenhotel'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm();
    }

}
