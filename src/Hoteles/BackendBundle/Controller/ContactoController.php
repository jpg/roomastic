<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Contacto;
use Hoteles\BackendBundle\Form\ContactoType;

/**
 * Contacto controller.
 *
 * @Route("/admin/contactoweb")
 */
class ContactoController extends Controller
{

    /**
     * Lists all Contacto entities.
     *
     * @Route("/", name="admin_contactoweb")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:Contacto')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Contacto entity.
     *
     * @Route("/{id}/show", name="admin_contactoweb_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Contacto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contacto entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Contacto entity.
     *
     * @Route("/new", name="admin_contactoweb_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Contacto();
        $form = $this->createForm(new ContactoType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Contacto entity.
     *
     * @Route("/create", name="admin_contactoweb_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Contacto:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Contacto();
        $request = $this->getRequest();
        $form = $this->createForm(new ContactoType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_contactoweb_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Contacto entity.
     *
     * @Route("/{id}/edit", name="admin_contactoweb_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $contacto = $em->getRepository('HotelesBackendBundle:Contacto')->find($id);

        if (!$contacto) {
            throw $this->createNotFoundException('Unable to find Contacto entity.');
        }

        $editForm = $this->createForm(new ContactoType(), $contacto);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'contacto' => $contacto,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Contacto entity.
     *
     * @Route("/{id}/update", name="admin_contactoweb_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Contacto:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $contacto = $em->getRepository('HotelesBackendBundle:Contacto')->find($id);
        if (!$contacto) {
            throw $this->createNotFoundException('Unable to find Contacto entity.');
        }

        $editForm = $this->createForm(new ContactoType(), $contacto);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);
        if ($editForm->isValid()) {
            $em->persist($contacto);
            $em->flush();
            //if ($contacto->getAtendido() !== $contacto_atendido_old) {
                return $this->redirect($this->generateUrl('admin_contactoweb'));
           /* } else {
                return $this->redirect($this->generateUrl('admin_contactoweb_edit', array('id' => $id)));
            }*/
        }

        return array(
            'entity' => $contacto,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Contacto entity.
     *
     * @Route("/{id}/delete", name="admin_contactoweb_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Contacto')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Contacto entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_contactoweb'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
