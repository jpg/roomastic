<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Oferta;
use Hoteles\BackendBundle\Entity\Sale;
use Hoteles\BackendBundle\Factories\SaleFactory;
use Hoteles\BackendBundle\Form\OfertaType;
use Hoteles\BackendBundle\Form\OfertacontraofertaType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Oferta controller.
 *
 * @Route("/admin/ofertas")
 */
class OfertaController extends Controller
{

    /**
     * Lists all Oferta entities.
     *
     * @Route("/", name="admin_ofertas", defaults={"filtro" = "todos", "id_hotel" = null}))
     * @Route("/filtro/{filtro}/{id_hotel}", name="admin_ofertas_filtradas",options={"expose"=true}, defaults={"filtro" = "todos", "id_hotel" = null}))
     * @Template()
     */
    public function indexAction($filtro, $id_hotel, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $securityContext = $this->container->get('security.context');
        $oferta_repository = $em->getRepository('HotelesBackendBundle:Oferta');
        if ($securityContext->isGranted('ROLE_EMPRESA')) {

            if ($id_hotel === null) {
//Si no paso un id_hotel y es una empresa dame todos
                $hoteles_empresa = $securityContext->getToken()->getUser()->getEmpresa()->getHoteles()->toArray();
                $ofertas = $oferta_repository->getOfertasFiltered($filtro, false, null, $hoteles_empresa);
            } else {
//Si paso un id de hotel compruebo que sea la misma empresa
                $hotel = $em->getRepository('HotelesBackendBundle:Oferta')->findBy(
                        array('id' => $id_hotel, 'empresa' => $securityContext->getToken()->getUser()->getEmpresa()));
                if ($hotel) {
//Si encuentro un hotel, significa que el hotel es de la empresa
                    $ofertas = $oferta_repository->getOfertasFiltered($filtro, false, $id_hotel);
                } else {
//Si no lo encuentro, no es tu hotel
                    $this->get('session')->setFlash('error', 'No tienes permiso para ver este hotel');
                    return $this->redirect($this->generateUrl('admin_landing'));
                }
            }
            $hoteles = $hoteles_empresa;
        } elseif ($securityContext->isGranted('ROLE_HOTEL')) {
            $idhotel = $securityContext->getToken()->getUser()->getHotel()->getId();
            $is_admin = $securityContext->getToken()->getUser()->isAdmin();
            $ofertas = $oferta_repository->getOfertasFiltered($filtro, $is_admin, $idhotel);
            $this->get('hoteles_backend.notificacion_manager')->resetNotificaciones($securityContext->getToken()->getUser()->getHotel(), $filtro);
            $hoteles = null;
        } else {
//Usuario que es ADMIN
            $ofertas = $oferta_repository->getOfertasFiltered($filtro, true, $id_hotel);
            $hoteles = $em->getRepository('HotelesBackendBundle:Hotel')->findAll();
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $ofertas,
                /* Numero de pagina */ $this->get('request')->query->get('page', 1),
                /* resultados por pagina */ $this->get('request')->query->get('show', $this->container->getParameter('paginacionpordefecto'))
        );

        return array(
            'entities' => $ofertas,
            'hoteles' => $hoteles,
            'pagination' => $pagination,
        );
    }

    /**
     * Finds and displays a Oferta entity.
     *
     * @Route("/{id}/show", name="admin_ofertas_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Oferta')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Oferta entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Oferta entity.
     *
     * @Route("/new", name="admin_ofertas_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Oferta();
        $form = $this->createForm(new OfertaType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Oferta entity.
     *
     * @Route("/create", name="admin_ofertas_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Oferta:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Oferta();
        $request = $this->getRequest();
        $form = $this->createForm(new OfertaType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_ofertas_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Oferta entity.
     *
     * @Route("/{id}/edit", name="admin_ofertas_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $oferta = $em->getRepository('HotelesBackendBundle:Oferta')->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException('Unable to find Oferta entity.');
        }

        $editForm = $this->createForm(new OfertaType(), $oferta);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $oferta,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Oferta entity.
     *
     * @Route("/{id}/update", name="admin_ofertas_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Oferta:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $oferta = $em->getRepository('HotelesBackendBundle:Oferta')->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException('Unable to find Oferta entity.');
        }

        $editForm = $this->createForm(new OfertaType(), $oferta);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($oferta);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_ofertas_edit', array('id' => $id)));
        }

        return array(
            'entity' => $oferta,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Oferta entity.
     *
     * @Route("/{id}/delete", name="admin_ofertas_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $oferta = $em->getRepository('HotelesBackendBundle:Oferta')->find($id);

            if (!$oferta) {
                throw $this->createNotFoundException('Unable to find Oferta entity.');
            }

            $em->remove($oferta);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_ofertas'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

    /**
     * Acepta oferta
     *
     * @Route("/aceptaoferta/{id}", name="acepta_oferta", options={"expose"=true})
     * @Template()
     */
    public function aceptaofertaAction($id, Request $request)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $oferta = $em->getRepository('HotelesBackendBundle:Oferta')->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException('Unable to find Oferta entity.');
        }
        $ofertaManager = $this->get('hoteles_backend.oferta_manager');
        $estado = $this->checkOfertaDates($oferta);
        if (true === $estado) {
//Lanzo la transicion acepta_oferta
            $user = $this->get('security.context')->getToken()->getUser();
            $resultado = $ofertaManager->updateOfferState('acepta_oferta', $oferta, $user);
//Compruebo que me ha devuelto una oferta valida
            if ($resultado) {
//Oferta valida                
//Creo Sale asociada a la oferta
                $sale = SaleFactory::createOfferSale($oferta);
                $em->persist($sale);
                $em->persist($oferta);
                $em->flush();
//Envio email
                $this->enviaEmailUser($oferta);
            } else {
                $this->get('session')->setFlash('error', 'Parece que hay un error con esta oferta');
            }
        } else {
            $this->get('session')->setFlash('error', 'Parece que esa oferta ya ha caducado');
        }
        $referer = $request->headers->get('referer');
        if (is_null($referer)) {
            return $this->redirect($this->generateUrl('admin_ofertas'));
        } else {
            return new RedirectResponse($referer);
        }
    }

    /**
     * Acepta oferta
     *
     * @Route("/acepta-oferta/{idofertaunico}", options={"expose"=true})
     */
    public function aceptarOfertaDesdeEmailAction($idofertaunico, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $oferta = $em->getRepository('HotelesBackendBundle:Oferta')->findOneBy(
                array('idofertaunico' => $idofertaunico));

        if (!$oferta) {
            throw $this->createNotFoundException('Oferta no encontrada :S');
        }
        $ofertaManager = $this->get('hoteles_backend.oferta_manager');
        $estado = $this->checkOfertaDates($oferta);
        if (true === $estado) {
//Lanzo la transicion acepta_oferta
            $user = $this->get('security.context')->getToken()->getUser();
            $resultado = $ofertaManager->updateOfferState('acepta_oferta', $oferta, $user);
//Compruebo que me ha devuelto una oferta valida
            if ($resultado) {
//Oferta valida                
//Creo Sale asociada a la oferta
                $sale = SaleFactory::createOfferSale($oferta);
                $em->persist($sale);
                $em->persist($oferta);
                $em->flush();
//Envio email
                $this->enviaEmailUser($oferta);
            } else {
                $this->get('session')->setFlash('error', 'Parece que hay un error con esta oferta');
            }
        } else {
            $this->get('session')->setFlash('error', 'Parece que esa oferta ya ha caducado');
        }
        return $this->redirect($this->generateUrl('admin_ofertas'));
    }

    /**
     * Rechaza oferta
     *
     * @Route("/rechazar-oferta/{idofertaunico}", options={"expose"=true})
     * @Template()
     */
    public function rechazarOfertaDesdeEmailAction($idofertaunico, Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $oferta = $em->getRepository('HotelesBackendBundle:Oferta')->findOneBy(
                array('idofertaunico' => $idofertaunico));

        if (!$oferta) {
            throw $this->createNotFoundException('Oferta no encontrada :S');
        }
        $ofertaManager = $this->get('hoteles_backend.oferta_manager');
        $estado = $this->checkOfertaDates($oferta);
        if (true === $estado) {
//Lanzo la transicion acepta_oferta
            $user = $this->get('security.context')->getToken()->getUser();
            $resultado = $ofertaManager->updateOfferState('rechaza_oferta', $oferta, $user);
//Compruebo que me ha devuelto una oferta valida
            if ($resultado) {
                $this->get('session')->setFlash('success', 'Has rechazado esta oferta satisfactoriamente');
                $em->flush();
            } else {
                $this->get('session')->setFlash('error', 'Parece que hay un error con esta oferta');
            }
        } else {
            $this->get('session')->setFlash('error', 'Parece que esa oferta ya ha caducado');
        }
        return $this->redirect($this->generateUrl('admin_ofertas'));
    }

    public function enviaEmailUser(Oferta $oferta)
    {

        $usuario = $oferta->getUsuario();
        $message = \Swift_Message::newInstance()
                ->setSubject('Oferta aceptada')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($usuario->getEmail())
                ->setBody(
                $this->renderView(
                        'HotelesBackendBundle:Mail:avisoUsuarioOfertaAceptada.html.twig', array(
                    'oferta' => $oferta,
                    'hotel' => $oferta->getHotel(),
                    'sale' => $oferta->getSale())), 'text/html');
        $this->get('mailer')->send($message);
        $this->get('session')->setFlash('success', 'Enviada confirmacion de oferta correctamente');

        return '1';
    }

    /**
     * Le paso el id del grupo de ofertas que se han generado.
     * Compruebo el grupo de ofertas 
     */
    public function checkOfertaDates(Oferta $oferta)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $configuracion = $em->getRepository('HotelesBackendBundle:Configuracion')->findAll();
        $tiempoHotel = $configuracion[0]->getTiempohotel();
//Compruebo que la oferta esté dentro del rango de horas de validez
        $fechaActual = new \Datetime();
        $fechaValidezOferta = $oferta->getFecha();
        $fechaValidezOferta->modify('+' . $tiempoHotel . ' hour');
        if ($fechaActual > $fechaValidezOferta) {
//Ha caducado la oferta
            $ofertaManager = $this->get('hoteles_backend.oferta_manager');
            $resultado = $ofertaManager->updateOfferState('caduca_oferta', $oferta);
            if ($resultado) {
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($oferta);
                $em->flush();
            } else {
                $this->get('logger')->err('Error al comprobar las fechas de '
                        . 'la oferta:' + $oferta->getId());
            }
            return false;
        } else {
            return true;
        }
    }

    /**
     * Declina oferta
     *
     * @Route("/declinaoferta/{id}", name="declina_oferta", options={"expose"=true})
     * @Template()
     */
    public function declinaofertaAction($id)
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getEntityManager();
        $oferta = $em->getRepository('HotelesBackendBundle:Oferta')->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException('Unable to find Oferta entity.');
        }

        $ofertaManager = $this->get('hoteles_backend.oferta_manager');

        $user = $this->get('security.context')->getToken()->getUser();
        $resultado = $ofertaManager->updateOfferState('rechaza_oferta', $oferta, $user);
        if ($resultado) {
            $em->persist($oferta);
            $em->flush();
            $this->get('session')->setFlash('success', 'Oferta declinada correctamente.');
        } else {
            $this->get('session')->setFlash('error', 'Problema al rechazar esta oferta.');
        }
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    /**
     * Hacer contraoferta
     *
     * @Route("/contraoferta/{id}", name="admin_ofertas_contraoferta", options={"expose"=true})
     * @Template()
     */
    public function contraofertaAction($id)
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getEntityManager();

        $oferta = $em->getRepository('HotelesBackendBundle:Oferta')->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException('Unable to find Oferta entity.');
        }

        $resultado_check_fechas = $this->checkOfertaDates($oferta);
        if ($resultado_check_fechas === FALSE) {
            //Recargo la pagina
            return $this->redirect($this->generateUrl('admin_ofertas'));
        }

        $editForm = $this->createForm(new OfertacontraofertaType(), $oferta);

        if ($request->getMethod() === 'POST') {

            $editForm->bindRequest($request);
            $contra_oferta = $request->request->get('hoteles_backendbundle_ofertatype');
            $precio_total_contraoferta = $contra_oferta['preciototalcontraoferta'];
            if ($precio_total_contraoferta == '') {
                $error = new FormError("No puede estar vacio");
                $editForm->get('preciototalcontraoferta')->addError($error);
            } else {
                if ($precio_total_contraoferta < $oferta->getPreciototaloferta()) {
                    $error = new FormError("La contraoferta no puede tener un precio por debajo del precio ofrecido por el cliente");
                    $editForm->get('preciototalcontraoferta')->addError($error);
                } else if ($oferta->getPrecioPorHabitacion($precio_total_contraoferta) > $oferta->getHotel()->getPVP()) {
                    $error = new FormError("La contraoferta no puede tener un precio superior al PVP");
                    $editForm->get('preciototalcontraoferta')->addError($error);
                }
            }

            if ($editForm->isValid()) {
                $ofertaManager = $this->get('hoteles_backend.oferta_manager');
                $user = $this->get('security.context')->getToken()->getUser();
                $resultado = $ofertaManager->updateOfferState('realiza_contraoferta', $oferta, $user);
                //Compruebo que me ha devuelto una oferta valida
                if ($resultado) {
                    $sale = SaleFactory::createCounterOfferSale($oferta);
                    $em->persist($oferta);
                    $em->persist($sale);
                    $em->flush();
                } else {
                    $this->get('session')->setFlash('error', 'Parece que hay un error con esta oferta');
                }
                return $this->redirect($this->generateUrl('admin_ofertas'));
            } else {
                $errors = $editForm->getErrors();
            }
        }

        return array(
            'entity' => $oferta,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * 
     * @Route("/oferta-details/{id}", options={"expose"=true})
     *      
     */
    public function ofertaDetailsAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $oferta = $em->getRepository('HotelesBackendBundle:Oferta')->find($id);
        if (is_null($oferta)) {
            throw $this->createNotFoundException('Oferta no encontrada');
        } else if (is_null($oferta->getSale()) !== false && $oferta->getSale()->isPayed() === false) {
            //throw $this->createNotFoundException('Oferta no encontrada');
        }
        $securityContext = $this->get('security.context');
        $user = $securityContext->getToken()->getUser();
        if ($securityContext->isGranted('ROLE_EMPRESA') &&
                $user->getEmpresa()->getId() !== $oferta->getHotel()->getEmpresa()->getId()) {
            throw $this->createNotFoundException('Oferta no encontrada');
        } elseif ($securityContext->isGranted('ROLE_HOTEL') &&
                $user->getHotel()->getId() !== $oferta->getHotel()->getId()) {
            throw $this->createNotFoundException('Oferta no encontrada');
        }
        $respuesta = $oferta->getUsuario()->getPublicOfferData();
        return $this->get("backend_utilities.json_response")->JSONResponse($respuesta);
    }

}
