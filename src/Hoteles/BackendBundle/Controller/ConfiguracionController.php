<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Configuracion;
use Hoteles\BackendBundle\Form\ConfiguracionType;

/**
 * Configuracion controller.
 *
 * @Route("/admin/configuracion")
 */
class ConfiguracionController extends Controller
{

    /**
     * Lists all Configuracion entities.
     *
     * @Route("/", name="admin_configuracion")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:Configuracion')->findAll();

        if ($entities) {
            return $this->redirect($this->generateUrl('admin_configuracion_edit', array('id' => $entities[0]->getId())));
        } else {
            return $this->redirect($this->generateUrl('admin_configuracion_new'));
        }

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Configuracion entity.
     *
     * @Route("/{id}/show", name="admin_configuracion_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Configuracion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Configuracion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Configuracion entity.
     *
     * @Route("/new", name="admin_configuracion_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Configuracion();
        $form = $this->createForm(new ConfiguracionType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Configuracion entity.
     *
     * @Route("/create", name="admin_configuracion_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Configuracion:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Configuracion();
        $request = $this->getRequest();
        $form = $this->createForm(new ConfiguracionType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_configuracion_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Configuracion entity.
     *
     * @Route("/{id}/edit", name="admin_configuracion_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Configuracion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Configuracion entity.');
        }

        $editForm = $this->createForm(new ConfiguracionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Configuracion entity.
     *
     * @Route("/{id}/update", name="admin_configuracion_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Configuracion:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $configuracion = $em->getRepository('HotelesBackendBundle:Configuracion')->find($id);

        if (!$configuracion) {
            throw $this->createNotFoundException('Unable to find Configuracion entity.');
        }

        $editForm = $this->createForm(new ConfiguracionType(), $configuracion);
        $deleteForm = $this->createDeleteForm($id);
        $request = $this->getRequest();
        $editForm->bindRequest($request);
        if ($editForm->isValid()) {
            $em->persist($configuracion);
            $em->flush();
            return $this->redirect($this->generateUrl('admin_configuracion_edit', array('id' => $id)));
        } else {
            $errors = $editForm->getErrors();
        }

        return array(
            'entity' => $configuracion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Configuracion entity.
     *
     * @Route("/{id}/delete", name="admin_configuracion_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Configuracion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Configuracion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_configuracion'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
