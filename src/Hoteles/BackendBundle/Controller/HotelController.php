<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Hotel;
use Hoteles\BackendBundle\Entity\UserHotel;
use Hoteles\BackendBundle\Form\HotelType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Hoteles\BackendBundle\Form\EventListener\DependentHotelDataSubscriber;

/**
 * Hotel controller.
 *
 * @Route("/admin/hotel")
 */
class HotelController extends Controller
{

    /**
     * Lists all Hotel entities.
     *
     * @Route("/", name="admin_hotel")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $securityContext = $this->container->get('security.context');

        $filtro = array();
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $filtro = array('empresa' => $this->obtieneEmpresa($em, $securityContext->getToken()->getUser()->getId()));
        } elseif ($securityContext->isGranted('ROLE_HOTEL')) {
            $filtro = array('id' => $this->obtieneHotel($em, $securityContext->getToken()->getUser()->getId()));
        }
        if ($securityContext->isGranted('ROLE_EMPRESA') ||
                $securityContext->isGranted('ROLE_ADMIN') || (
                $securityContext->isGranted('ROLE_HOTEL') &&
                $securityContext->getToken()->getUser()->isAdmin()
                )) {
            $show_admin = true;
        } else {
            $show_admin = false;
        }
        $entities = $em->getRepository('HotelesBackendBundle:Hotel')->findBy($filtro);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $this->get('request')->query->get('page', 1)/* page number */, $this->get('request')->query->get('show', $this->container->getParameter('paginacionpordefecto'))/* limit per page */
        );

        return array(
            'entities' => $entities,
            'pagination' => $pagination,
            'show_admin' => $show_admin
        );
    }

    /**
     * Finds and displays a Hotel entity.
     *
     * @Route("/{id}/show", name="admin_hotel_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Hotel')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Hotel entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Hotel entity.
     *
     * @Route("/new", name="admin_hotel_new")
     * @Template()
     */
    public function newAction()
    {
        $new_hotel = new Hotel();
        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $new_hotel->loadDatosEmpresa($securityContext->getToken()->getUser()->getEmpresa());
        }
        $form = $this->generaFormularioDinamico($new_hotel)->getForm();

        return array(
            'entity' => $new_hotel,
            'form' => $form->createView(),
            'imagenes' => isset($_POST["imageneshotel"]) ? $filtro->arrayImagenes($_POST["imageneshotel"]) : '',
        );
    }

    /**
     * Creates a new Hotel entity.
     *
     * @Route("/create", name="admin_hotel_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Hotel:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $nuevo_hotel = new Hotel();
        $request = $this->getRequest();
        $form = $this->generaFormularioDinamico($nuevo_hotel,'create')->getForm();
        $form->bindRequest($request);

        $filtro = $this->get('hoteles_backend.funcionalidades');

        /*if ($filtro->compruebaEmail($form['emailpersonacontacto']->getData())) {
            $error = new formerror("Ese email está en uso");
            $form->get('emailpersonacontacto')->addError($error);
        }

        if ($filtro->compruebaUsername($form['emailpersonacontacto']->getData())) {
            $error = new formerror("Ese username está en uso");
            $form->get('emailpersonacontacto')->addError($error);
        }*/

        $numeroImagenes = 2;
        if (isset($_POST["imageneshotel"])) {
            if (count($_POST["imageneshotel"]) < $numeroImagenes) {
                $error = new formerror("Debe meter minimo " . $numeroImagenes . " imagenes");
                $form->get('imagenes')->addError($error);
            } else {
                $nuevo_hotel->setImagenes(count($_POST["imageneshotel"]));
            }
        } else {
            $error = new formerror("Debe meter minimo " . $numeroImagenes . " imagenes");
            $form->get('imagenes')->addError($error);
        }

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getEntityManager();
            $securityContext = $this->container->get('security.context');
            if ($securityContext->isGranted('ROLE_EMPRESA')) {
                $empresa = $securityContext->getToken()->getUser()->getEmpresa();
                $nuevo_hotel->setComisionHotelEntero($empresa->getComisionEmpresaEntero());
                $nuevo_hotel->setComisionHotelDecimal($empresa->getComisionEmpresaDecimal());
            }

            $em->persist($nuevo_hotel);
            $em->flush();

            $cont = 1;

            foreach ($_POST["imageneshotel"] as $value) {
                $imagenHotel = $em->getRepository('HotelesBackendBundle:Imagenhotel')->find($value);
                $imagenHotel->setHotel($nuevo_hotel);
                $imagenHotel->setOrden($cont);
                $em->persist($imagenHotel);
                $cont = $cont + 1;
            }
            $em->flush();

//Creo el primero usuario para administrar el hotel
            $administrador = $request->request->get('administrador', false);
            $this->creaPrimerUsuario($nuevo_hotel, $em, $administrador);



            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_hotel_edit', array('id' => $nuevo_hotel->getId())));
        } else {
            $errors = $form->getErrors();
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }

        return array(
            'entity' => $nuevo_hotel,
            'form' => $form->createView(),
            'imagenes' => isset($_POST["imageneshotel"]) ? $filtro->arrayImagenes($_POST["imageneshotel"]) : '',
        );
    }

    /**
     * Displays a form to edit an existing Hotel entity.
     *
     * @Route("/{id}/edit", name="admin_hotel_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
//Compruebo los permisos del usuario
        $securityContext = $this->container->get('security.context');
        if (($securityContext->isGranted('ROLE_EMPRESA') ||
                $securityContext->isGranted('ROLE_ADMIN') || (
                $securityContext->isGranted('ROLE_HOTEL') &&
                $securityContext->getToken()->getUser()->isAdmin()
                )) === FALSE) {
//No tiene permiso para editar campos del hotel, le redirijo al listado
            $this->get('session')->setFlash('nosuccess', 'No tienes permiso para realizar esa accion.');
            return $this->redirect($this->generateUrl("admin_hotel"));
        }
        $hotel = $em->getRepository('HotelesBackendBundle:Hotel')->find($id);

        if (!$hotel) {
            $this->get('session')->setFlash('nosuccess', 'Hotel no encontrado.');
            return $this->redirect($this->generateUrl("admin_hotel"));
        }
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
//Es una empresa, luego tengo que comprobar si es la empresa del hotel
            if ($hotel->getEmpresa() !== $securityContext->getToken()->getUser()->getEmpresa()) {
//No es la empresa, redirijo al admin list y muestro un warning
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para realizar esa accion.');
                return $this->redirect($this->generateUrl("admin_hotel"));
            }
        } else if ($securityContext->isGranted('ROLE_HOTEL') &&
                $securityContext->getToken()->getUser()->isAdmin()) {
            if ($hotel !== $securityContext->getToken()->getUser()->getHotel()) {
//No es la empresa, redirijo al admin list y muestro un warning
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para realizar esa accion.');
                return $this->redirect($this->generateUrl("admin_hotel"));
            }
        }

        $imagenHotel = $em->getRepository('HotelesBackendBundle:Imagenhotel')->findBy(
                array('hotel' => $hotel->getId()), array('orden' => 'ASC'));

        $editForm = $this->generaFormularioDinamico($hotel,'edit')->getForm();
        $deleteForm = $this->createDeleteForm($id);
        $reactivateForm = $this->createReactivateForm($id);
        $res =  array(
            'entity' => $hotel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'reactivate_form' => $reactivateForm->createView(),
            'imagenes' => $imagenHotel,
        );
        return $res;
    }

    /**
     * Edits an existing Hotel entity.
     *
     * @Route("/{id}/update", name="admin_hotel_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Hotel:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $hotel = $em->getRepository('HotelesBackendBundle:Hotel')->find($id);

        if (!$hotel) {
            $this->get('session')->setFlash('nosuccess', 'Hotel no encontrado.');
            return $this->forward('HotelesBackendBundle:Hotel:index');
        }
//Compruebo los permisos del usuario
        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
//Es una empresa, luego tengo que comprobar si es la empresa del hotel
            if ($hotel->getEmpresa() !== $securityContext->getToken()->getUser()->getEmpresa()) {
//No es la empresa, redirijo al admin list y muestro un warning
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para realizar esa accion.');
                return $this->redirect($this->generateUrl("admin_hotel"));
            }
        } else if ($securityContext->isGranted('ROLE_HOTEL') &&
                $securityContext->getToken()->getUser()->isAdmin()) {
            $hotel_de_user = $securityContext->getToken()->getUser()->getHotel();
            if ($hotel !== $hotel_de_user) {
//No es la empresa, redirijo al admin list y muestro un warning
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para realizar esa accion.');
                return $this->redirect($this->generateUrl("admin_hotel"));
            }
        }
        $imagenHotel = $em->getRepository('HotelesBackendBundle:Imagenhotel')->findBy(array('hotel' => $hotel->getId()), array('orden' => 'ASC'));

        $editForm = $this->generaFormularioDinamico($hotel,'update')->getForm();
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        $filtro = $this->get('hoteles_backend.funcionalidades');

        $numeroImagenes = 2;
        if (isset($_POST["imageneshotel"])) {
            if (count($_POST["imageneshotel"]) < $numeroImagenes) {
                $error = new formerror("Debe meter minimo " . $numeroImagenes . " imagenes");
                $editForm->get('imagenes')->addError($error);
            } else {
                $hotel->setImagenes(count($_POST["imageneshotel"]));
            }
        } else {
            $error = new formerror("Debe meter minimo " . $numeroImagenes . " imagenes");
            $editForm->get('imagenes')->addError($error);
        }

        if ($editForm->isValid()) {
            $em->persist($hotel);
            $em->flush();

            $em = $this->getDoctrine()->getEntityManager();
            $cont = 1;

            $imagenHotel = $em->getRepository('HotelesBackendBundle:Imagenhotel')->findBy(array('hotel' => $hotel->getId()));
            foreach ($imagenHotel as $key => $value) {
                $value->setHotel(null);
                $value->setOrden(null);
            }
            $em->flush();


            foreach ($_POST["imageneshotel"] as $value) {
                $imagenHotel = $em->getRepository('HotelesBackendBundle:Imagenhotel')->find($value);
                $imagenHotel->setHotel($hotel);
                $imagenHotel->setOrden($cont);
                $em->persist($imagenHotel);
                $cont = $cont + 1;
            }
            $em->flush();

            return $this->redirect($this->generateUrl('admin_hotel_edit', array('id' => $id)));
        }
        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_EMPRESA') ||
                $securityContext->isGranted('ROLE_ADMIN') || (
                $securityContext->isGranted('ROLE_HOTEL') &&
                $securityContext->getToken()->getUser()->isAdmin()
                )) {
            $show_admin = true;
        } else {
            $show_admin = false;
        }       

        return array(
            'entity' => $hotel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'imagenes' => $imagenHotel,
            'show_admin' => $show_admin
        );
    }

    /**
     * Deletes a Hotel entity.
     *
     * @Route("/{id}/delete", name="admin_hotel_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $hotel = $em->getRepository('HotelesBackendBundle:Hotel')->find($id);

            if (!$hotel) {
                $this->get('session')->setFlash('nosuccess', 'Empresa no encontrada.');
                return $this->forward('HotelesBackendBundle:Empresa:index');
            }
            $securityContext = $this->container->get('security.context');
            if ($securityContext->isGranted('ROLE_HOTEL')) {
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para realizar esta accion.');
                return $this->forward('HotelesBackendBundle:Hotel:index');
            }

            if ($securityContext->isGranted('ROLE_ADMIN') ||
                    ($securityContext->isGranted('ROLE_EMPRESA') &&
                    $securityContext->getToken()->getUser()->getEmpresa() === $hotel->getEmpresa())) {
                foreach ($hotel->getUserHotel() as $user_hotel) {
//Suspendo a todos los usuarios hotel de este hotel
                    $user_hotel->suspendUser();
                    $em->persist($user_hotel);
                }
//Suspendo a este hotel
                $oferta_manager = $this->get('hoteles_backend.oferta_manager');
                $ofertas_pendientes = $em->getRepository('HotelesBackendBundle:Oferta')->getOffersPendingUserAction($hotel);
                foreach ($ofertas_pendientes as $oferta_pendiente) {
                    $oferta_manager->updateOfferState('hotel_suspenso', $oferta_pendiente);
                    $em->persist($oferta_pendiente);
                }
                $hotel->suspendHotel();
                $em->persist($hotel);
            } else {
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para realizar esta accion.');
                return $this->forward('HotelesBackendBundle:Hotel:index');
            }



            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_hotel'));
    }

    /**
     * Deletes a Hotel entity.
     *
     * @Route("/{id}/reactivate", name="admin_hotel_reactivate")
     * @Method("post")
     */
    public function reactivateHotelAction($id)
    {

        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $hotel = $em->getRepository('HotelesBackendBundle:Hotel')->find($id);

            if (!$hotel) {
                $this->get('session')->setFlash('nosuccess', 'Hotel no encontrado.');
                return $this->forward('HotelesBackendBundle:Empresa:index');
            }
            $securityContext = $this->container->get('security.context');
            if ($securityContext->isGranted('ROLE_HOTEL')) {
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para realizar esta accion.');
                return $this->forward('HotelesBackendBundle:Hotel:index');
            }
            //Hay que definir que acción queremos para esto
            if ($securityContext->isGranted('ROLE_ADMIN') ||
                    ($securityContext->isGranted('ROLE_EMPRESA') &&
                    $securityContext->getToken()->getUser()->getEmpresa() === $hotel->getEmpresa())) {
                foreach ($hotel->getUserHotel() as $user_hotel) {
                    //Suspendo a todos los usuarios hotel de este hotel
                    $user_hotel->unsuspendUser();
                    $em->persist($user_hotel);
                }
                //Suspendo a este hotel
                $hotel->unsuspendHotel();
                $em->persist($hotel);
            } else {
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para realizar esta accion.');
                return $this->forward('HotelesBackendBundle:Hotel:index');
            }
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_hotel'));
    }

/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm();
    }

    private function createReactivateForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

    /**
     * Checks that a slug is unique. If yes, returns the same slug, if not it generates one.
     *
     * @Route("/check-hotel-slug/{slug}", name="admin_hotel_check_slug", options={"expose"=true})
     * @Method("get")
     */
    public function checkHotelSlugAction($slug)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $index = 1;
        $hotel = $em->getRepository('HotelesBackendBundle:Hotel')->findoneBy(array('url' => $slug));
        $new_slug = $slug;
        while ($hotel) {
//Existe el hotel, le aniado al slug un -$index y devuelvo el valor
            $new_slug = $slug . '-' . $index;
            $hotel = $em->getRepository('HotelesBackendBundle:Hotel')->findoneBy(array('url' => $new_slug));
            $index++;
        }
        $respuesta = array('slug' => strtolower($new_slug));
        return $this->get("backend_utilities.json_response")->JSONResponse($respuesta);
    }

    public function compruebaEmailParaPrimerUsuario($objeto)
    {

// comprobar que no exista usuario para el email ese 
// si existe pasar un error al formulario para el emailpersonacontacto

        return true;
    }

    public function creaPrimerUsuario(Hotel $hotel, $em, $administrador = false)
    {

        $nuevo_usuario_hotel = new UserHotel();
        $nuevo_usuario_hotel->setUsername($hotel->getEmailpersonacontacto());
        $nuevo_usuario_hotel->setEmail($hotel->getEmailpersonacontacto());
        $nuevo_usuario_hotel->setNombreCompleto($hotel->getNombrepersonacontacto() + " " + $hotel->getApellidospersonacontacto());
        $nuevo_usuario_hotel->setPassword(md5(microtime()));
        $nuevo_usuario_hotel->setRoles(array('ROLE_HOTEL'));
        $nuevo_usuario_hotel->setHotel($hotel);
        $nuevo_usuario_hotel->setAdministrador($administrador);
        $em->persist($nuevo_usuario_hotel);
        try{
            $em->flush();
        } catch (\PDOException $e) {
            //No hacer nada
        }
        
// enviar mail registro
        $this->get('hoteles_backend.mailerfosuser')->sendConfirmationEmailMessage($nuevo_usuario_hotel->getId());

        return true;
    }

    public function generaFormularioDinamico($hotel,$method='edit')
    {

        $em = $this->getDoctrine()->getEntityManager();
        $securityContext = $this->container->get('security.context');

        $form = $this->createFormBuilder($hotel);
        $idUser = $securityContext->getToken()->getUser()->getId();

        $query_user = $this->getDoctrine()->getRepository('HotelesBackendBundle:Empresa')->createQueryBuilder('u');

        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $user = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($idUser);
            $idEmpresa = $user->getEmpresa()->getId();
            $query_user->where('u.id = :idEmpresa')->setParameter('idEmpresa', $idEmpresa);
        } elseif ($securityContext->isGranted('ROLE_HOTEL')) {
            $user = $em->getRepository('HotelesBackendBundle:UserHotel')->find($idUser);
            $idEmpresa = $user->getHotel()->getEmpresa()->getId();
            $query_user->where('u.id = :idEmpresa')->setParameter('idEmpresa', $idEmpresa);
        }

        $query_provincias = $this->getDoctrine()->getRepository('HotelesBackendBundle:Provincia')
                        ->createQueryBuilder('p')->orderBy('p.nombre', 'ASC');
        
        $form->addEventSubscriber(new DependentHotelDataSubscriber($form->getFormFactory(),$em,$method));

        $form->add('empresa', 'entity', array(
                    'class' => 'HotelesBackendBundle:Empresa',
                    'query_builder' => $query_user,
                ))
                ->add('email', 'text', array(
                    'required' => false,
                ))
                ->add('url', 'text', array(
                    'required' => false,
                ))
                ->add('nombreempresa', 'text', array(
                    'required' => false,
                ))
                ->add('cif', 'text', array(
                    'required' => false,
                ))
                ->add('direccionfacturacion', 'text', array(
                    'required' => false,
                ))
                ->add('telefono', 'text', array(
                    'required' => false,
                ))
                ->add('numerocuenta', 'text', array(
                    'required' => false,
                ))
                ->add('nombrepersonacontacto', 'text', array(
                    'required' => false,
                ))
                ->add('apellidospersonacontacto', 'text', array(
                    'required' => false,
                ))
                ->add('cargopersonacontacto', 'text', array(
                    'required' => false,
                ))
                ->add('emailpersonacontacto', 'text', array(
                    'required' => false,
                ))
                ->add('telefonopersonacontacto', 'text', array(
                    'required' => false,
                ))
                ->add('nombrehotel', 'text', array(
                    'required' => false,
                ))
//->add('nombrehotel')
                ->add('calificacion', 'choice', array(
                    'choices' => array(
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                    ),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                ))
                ->add('descripcion', 'textarea', array(
                    'required' => false,
                    'attr' => array('rows' => '5'),
                ))
                ->add('caracteristicas', 'textarea', array(
                    'required' => false,
                    'attr' => array('rows' => '5'),
                ))
                ->add('ubicacion', 'text', array(
                    'required' => false,
                ))
                ->add('pvpoficialenteros', 'text', array(
                    'required' => false,
                ))
                ->add('pvpoficialdecimales', 'text', array(
                    'required' => false,
                ))
                ->add('cantidadminimaaceptadaenteros', 'text', array(
                    'required' => false,
                ))
                ->add('cantidadminimaaceptadadecimales', 'text', array(
                    'required' => false,
                ))
                ->add('imagenes', 'text', array(
                    'required' => false,
                ))
                ->add('piscina', 'choice', array(
                    'choices' => array('1' => 'Si', '0' => 'No'),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                ))
                ->add('spa', 'choice', array(
                    'choices' => array('1' => 'Si', '0' => 'No'),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                ))
                ->add('wi_fi', 'choice', array(
                    'choices' => array('1' => 'Si', '0' => 'No'),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                ))
                ->add('acceso_adaptado', 'choice', array(
                    'choices' => array('1' => 'Si', '0' => 'No'),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                ))
                ->add('aceptan_perros', 'choice', array(
                    'choices' => array('1' => 'Si', '0' => 'No'),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                ))
                ->add('aparcamiento', 'choice', array(
                    'choices' => array('1' => 'Si', '0' => 'No'),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                ))
                ->add('business_center', 'choice', array(
                    'choices' => array('1' => 'Si', '0' => 'No'),
                    'required' => true,
                    'multiple' => false,
                    'expanded' => false,
                ))
//Un usuario que envíe una petición con un 2 aquí,
//banearía a ese hotel
                ->add('status', 'choice', array(
                    'choices' => array('1' => 'Publicado', '0' => 'No publicado'),
                    'required' => true,
                ))
                ->add('seotitulo', 'text', array(
                    'required' => false,
                ))
                ->add('seodescripcion', 'text', array(
                    'required' => false,
                ))
                ->add('seokeywords', 'text', array(
                    'required' => false,
                ))
                ->add('provincia', 'entity', array(
                    'class' => 'HotelesBackendBundle:Provincia',
                    'query_builder' => $query_provincias,
                ))
                ->add('latitud', 'hidden', array(
                    'data' => 'abcdef',
                ))->add('longitud', 'hidden', array(
            'data' => 'abcdef',
        ));
        if ($securityContext->isGranted('ROLE_ADMIN')) {
            $form->add('comisionhotelentero')
                    ->add('comisionhoteldecimal');
        }

        return $form;
    }

    public function obtieneEmpresa($em, $userId)
    {

        $userEmpresa = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($userId);
        return $userEmpresa->getEmpresa()->getId();
    }

    public function obtieneHotel($em, $userId)
    {

        $userHotel = $em->getRepository('HotelesBackendBundle:UserHotel')->find($userId);
        return $userHotel->getHotel()->getId();
    }

}
