<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Cookies;
use Hoteles\BackendBundle\Form\CookiesType;

/**
 * Cookies controller.
 *
 * @Route("/admin/cookies")
 */
class CookiesController extends Controller
{

    /**
     * Lists all Cookies entities.
     *
     * @Route("/", name="admin_cookies")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:Cookies')->findAll();

        if ($entities) {
            return $this->redirect($this->generateUrl('admin_cookies_edit', array('id' => $entities[0]->getId())));
        } else {
            return $this->redirect($this->generateUrl('admin_cookies_new'));
        }

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Cookies entity.
     *
     * @Route("/{id}/show", name="admin_cookies_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Cookies')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cookies entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Cookies entity.
     *
     * @Route("/new", name="admin_cookies_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Cookies();
        $form = $this->createForm(new CookiesType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Cookies entity.
     *
     * @Route("/create", name="admin_cookies_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Cookies:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Cookies();
        $request = $this->getRequest();
        $form = $this->createForm(new CookiesType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_cookies_edit', array('id' => $entity->getId())));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Cookies entity.
     *
     * @Route("/{id}/edit", name="admin_cookies_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Cookies')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cookies entity.');
        }

        $editForm = $this->createForm(new CookiesType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Cookies entity.
     *
     * @Route("/{id}/update", name="admin_cookies_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Cookies:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Cookies')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Cookies entity.');
        }

        $editForm = $this->createForm(new CookiesType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Editado correctamente.');

            return $this->redirect($this->generateUrl('admin_cookies_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo editar.');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Cookies entity.
     *
     * @Route("/{id}/delete", name="admin_cookies_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Cookies')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Cookies entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Borrado correctamente.');
        }

        return $this->redirect($this->generateUrl('admin_cookies'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
