<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\UserAdministrador;
use Hoteles\BackendBundle\Form\UserAdministradorType;
use Symfony\Component\Form\FormError;

/**
 * UserAdministrador controller.
 *
 * @Route("/admin/useradministrador")
 */
class UserAdministradorController extends Controller
{

    /**
     * Lists all UserAdministrador entities.
     *
     * @Route("/", name="admin_useradministrador")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:UserAdministrador')->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $this->get('request')->query->get('page', 1)/* page number */, $this->get('request')->query->get('show', $this->container->getParameter('paginacionpordefecto'))/* limit per page */
        );

        return array(
            'entities' => $entities,
            'pagination' => $pagination,
        );
    }

    /**
     * Finds and displays a UserAdministrador entity.
     *
     * @Route("/{id}/show", name="admin_useradministrador_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:UserAdministrador')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserAdministrador entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new UserAdministrador entity.
     *
     * @Route("/new", name="admin_useradministrador_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new UserAdministrador();
        $form = $this->createForm(new UserAdministradorType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'imagendevuelta' => '',
        );
    }

    /**
     * Creates a new UserAdministrador entity.
     *
     * @Route("/create", name="admin_useradministrador_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:UserAdministrador:new.html.twig")
     */
    public function createAction()
    {
        $entity = new UserAdministrador();
        $request = $this->getRequest();
        $form = $this->createForm(new UserAdministradorType(), $entity);
        $form->bindRequest($request);

        $filtro = $this->get('hoteles_backend.funcionalidades');

        if ($filtro->compruebaEmail($form['email']->getData())) {
            $error = new FormError("Ese email está en uso");
            $form->get('email')->addError($error);
        }

        if ($filtro->compruebaUsername($form['username']->getData())) {
            $error = new FormError("Ese username está en uso");
            $form->get('username')->addError($error);
        }

        if ($form->isValid()) {

            $entity->setImagen($_POST["imagensubidanombre"]);
            $entity->setPassword(md5(microtime()));
            $entity->setRoles(array('ROLE_ADMIN'));
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            $mailerfosuser = $this->get('hoteles_backend.mailerfosuser')->sendWelcomeEmailMessage($entity->getId());

            return $this->redirect($this->generateUrl('admin_useradministrador_edit', array('id' => $entity->getId())));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'imagendevuelta' => $_POST["imagensubidanombre"],
        );
    }

    /**
     * Displays a form to edit an existing UserAdministrador entity.
     *
     * @Route("/{id}/edit", name="admin_useradministrador_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:UserAdministrador')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserAdministrador entity.');
        }

        $editForm = $this->createForm(new UserAdministradorType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'imagendevuelta' => $entity->getImagen(),
        );
    }

    /**
     * Edits an existing UserAdministrador entity.
     *
     * @Route("/{id}/update", name="admin_useradministrador_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:UserAdministrador:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:UserAdministrador')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserAdministrador entity.');
        }

        $editForm = $this->createForm(new UserAdministradorType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);


        $filtro = $this->get('hoteles_backend.funcionalidades');

        if ($filtro->compruebaEmail($editForm['email']->getData(), $entity)) {
            $error = new formerror("Ese email está en uso");
            $editForm->get('email')->addError($error);
        }

        if ($filtro->compruebaUsername($editForm['username']->getData(), $entity)) {
            $error = new formerror("Ese username está en uso");
            $editForm->get('username')->addError($error);
        }

        if ($editForm->isValid()) {

            $entity->setImagen($_POST["imagensubidanombre"]);
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Editado correctamente.');

            return $this->redirect($this->generateUrl('admin_useradministrador_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo editar.');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'imagendevuelta' => $_POST["imagensubidanombre"],
        );
    }

    /**
     * Deletes a UserAdministrador entity.
     *
     * @Route("/{id}/delete", name="admin_useradministrador_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:UserAdministrador')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find UserAdministrador entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_useradministrador'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
