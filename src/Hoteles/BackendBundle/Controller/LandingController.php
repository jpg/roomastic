<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class LandingController extends Controller
{

    /**
     * @Route("/admin/dashboard/", name="admin_landing")
     * @Template()
     */
    public function indexAction()
    {
        $log = $this->get('logger');

        $em = $this->getDoctrine()->getEntityManager();
        $securityContext = $this->get('security.context');
        if ($securityContext->isGranted('ROLE_HOTEL')) {
            $type_of_user = 'hotel';
        } else if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $type_of_user = 'empresa';
        } else {
            $type_of_user = 'admin';
        }
        $user_id = $securityContext->getToken()->getUser()->getId();
        $numero_de_dias = 30;

        $ofertaRepository = $em->getRepository('HotelesBackendBundle:Oferta');
        $num_ofertas_aceptadas = $ofertaRepository->getOfferCountFromLastNDays($numero_de_dias, $type_of_user, 'aceptadas', $user_id);
        $num_ofertas_recibidas = $ofertaRepository->getOfferCountFromLastNDays($numero_de_dias, $type_of_user, 'recibidas', $user_id);
        $num_reservas_efectuadas = $ofertaRepository->getOfferCountFromLastNDays($numero_de_dias, $type_of_user, 'pagadas', $user_id);
        $num_ofertas_debajo_minimo = $ofertaRepository->getOfferCountFromLastNDays($numero_de_dias, $type_of_user, 'debajo-minimo', $user_id);
        //Informe de facturacion diaria por periodo
        $dias_mes = 30;
        $daily_ofertas_ultimo_mes = array_map(
                function($elem) {
            return floatval($elem['suma']);
        }, $ofertaRepository->getOfferDailySumLastNDays(
                        $dias_mes, $type_of_user, $user_id));
        $sum_ofertas_ultimo_mes = array_sum($daily_ofertas_ultimo_mes);
        $dias_semana = 7;
        $daily_ofertas_ultima_semana = array_slice($daily_ofertas_ultimo_mes, count($daily_ofertas_ultimo_mes) - 7, $dias_semana);
        $sum_ofertas_ultima_semana = array_sum($daily_ofertas_ultima_semana);

        //Informe de facturacion mensual por año
        $monthly_ofertas_yearly = array_map(
                function($elem) {
            return floatval($elem['suma']);
        }, $ofertaRepository->getOfferMonthlySales(
                        $type_of_user, $user_id));

        //Numero de ofertas recibidas en el último año
        $num_ofertas_anio_mes = array_map(
                function($elem) {
            return floatval($elem['suma']);
        }, $ofertaRepository->getNumberOfOffersByMonth($type_of_user, $user_id)); // [0, 1, 1, 2, 3, 20, 24, 56, 78, 20, 56, 12];
        $nombre_meses_temp = preg_split("/,/", "ENE,FEB,MAR,ABR,MAY,JUN,JUL,AGO,SEP,OCT,NOV,DIC");
        $fecha_actual = new \DateTime();
        $curr_month = $fecha_actual->format('m');

        $nombre_meses = array_merge_recursive(array_splice($nombre_meses_temp, $curr_month, 12 - $curr_month), array_splice($nombre_meses_temp, 0, $curr_month));
        $y_axis_step = max($num_ofertas_anio_mes) / 5;
        $rango_y = range(max($num_ofertas_anio_mes), 0, -1 * $y_axis_step);
        $ranking = $ofertaRepository->getHotelRankings($type_of_user, $user_id);

        //Ofertas gestionadas por el usuario
        $num_ofertas_gestionadas = $ofertaRepository->getOffersManagedByUser($user_id);
        //Ofertas pendientes del hotel
        $num_ofertas_pendientes = $ofertaRepository->getNumOfertasPendientes($user_id);
        $log->info('Vamos a pintar el dashboard');
        return array('ofertas_recibidas_30_dias' => $num_ofertas_recibidas,
            'ofertas_aceptadas_30_dias' => $num_ofertas_aceptadas,
            'reservas_efectuadas_30_dias' => $num_reservas_efectuadas,
            'ofertas_debajo_minimo' => $num_ofertas_debajo_minimo,
            'daily_sum_ofertas_mes' => json_encode($daily_ofertas_ultimo_mes),
            'daily_sum_ofertas_semana' => json_encode($daily_ofertas_ultima_semana),
            'sum_ofertas_mes' => $sum_ofertas_ultimo_mes,
            'sum_ofertas_semana' => $sum_ofertas_ultima_semana,
            'monthly_ofertas_yearly' => json_encode($monthly_ofertas_yearly),
            'sum_monthly_ofertas_yearly' => array_sum($monthly_ofertas_yearly),
            'nombre_meses' => $nombre_meses,
            'rango_y' => $rango_y,
            'num_ofertas_anio_mes' => $num_ofertas_anio_mes,
            'num_ofertas_anio_mes_max' => max($num_ofertas_anio_mes),
            'ranking' => $ranking,
            'num_ofertas_gestionadas' => $num_ofertas_gestionadas,
            'num_ofertas_pendientes' => $num_ofertas_pendientes
        );
    }

}
