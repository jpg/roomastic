<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Description of PasswordResetController
 *
 * @author joaquin
 */
class PasswordResetController extends Controller
{

    /**
     * 
     * @Route("/password-cambiada")
     */
    public function afterResetAction(Request $request)
    {
        //Si el usuario es hotel, empresa o admin los mando al backend si no los mando al aviso
        $securityContext = $this->get('security.context');
        if ($securityContext->isGranted('ROLE_HOTEL') ||
                $securityContext->isGranted('ROLE_EMPRESA') ||
                $securityContext->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('admin_landing'));
        }
        return $this->render('HotelesBackendBundle:PasswordReset:passwordReset.html.twig');
    }

}
