<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\UserEmpresa;
use Hoteles\BackendBundle\Form\UserEmpresaType;
use Symfony\Component\Form\FormError;

/**
 * UserEmpresa controller.
 *
 * @Route("/admin/userempresa")
 */
class UserEmpresaController extends Controller
{

    /**
     * Lists all UserEmpresa entities.
     *
     * @Route("/", name="admin_userempresa")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $securityContext = $this->container->get('security.context');
        //$entities = $em->getRepository('HotelesBackendBundle:UserEmpresa')->findAll();

        $filtro = array();
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $filtro = array('empresa' => $this->obtieneIdEmpresa($em, $securityContext->getToken()->getUser()->getId()));
        }
        $entities = $em->getRepository('HotelesBackendBundle:UserEmpresa')->findBy($filtro);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $this->get('request')->query
                        /* page number */->get('page', 1)
                , /* resultados por pagina */ $this->get('request')->query->get('show', $this->container->getParameter('paginacionpordefecto'))
        );

        return array(
            'entities' => $entities,
            'pagination' => $pagination,
        );
    }

    public function obtieneIdEmpresa($em, $userId)
    {

        $userEmpresa = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($userId);
        return $userEmpresa->getEmpresa()->getId();
    }

    /**
     * Finds and displays a UserEmpresa entity.
     *
     * @Route("/{id}/show", name="admin_userempresa_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $user_empresa = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($id);

        if (!$user_empresa) {
            $this->get('session')->setFlash('nosuccess', 'Usuario empresa no encontrado.');
            return $this->redirect($this->generateUrl("admin_hotel"));
        }
        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $empresa_user_actual = $securityContext->getToken()->getUser()->getEmpresa();
            if ($empresa_user_actual !== $user_empresa->getEmpresa()) {
                //Un usuario empresa esta intentando ver el perfil de un usuario empresa de otra empresa
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->redirect($this->generateUrl("admin_hotel"));
            }
        }
        $deleteForm = $this->createDeleteForm($id);
        return array(
            'entity' => $user_empresa,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new UserEmpresa entity.
     *
     * @Route("/new", name="admin_userempresa_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new UserEmpresa();
        //$form   = $this->createForm(new UserEmpresaType(), $entity);
        $form = $this->generaFormularioDinamico($entity)->getForm();

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'imagendevuelta' => '',
        );
    }

    /**
     * Creates a new UserEmpresa entity.
     *
     * @Route("/create", name="admin_userempresa_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:UserEmpresa:new.html.twig")
     */
    public function createAction()
    {
        $new_user_empresa = new UserEmpresa();
        $request = $this->getRequest();
        //$form    = $this->createForm(new UserEmpresaType(), $entity);
        $form = $this->generaFormularioDinamico($new_user_empresa)->getForm();
        $form->bindRequest($request);

        $filtro = $this->get('hoteles_backend.funcionalidades');

        if ($filtro->compruebaEmail($form['email']->getData())) {
            $error = new formerror("Ese email está en uso");
            $form->get('email')->addError($error);
        }

        if ($filtro->compruebaUsername($form['email']->getData())) {
            $error = new formerror("Ese username está en uso");
            $form->get('username')->addError($error);
        }

        if ($form->isValid()) {

            $new_user_empresa->setImagen($_POST["imagensubidanombre"]);
            $new_user_empresa->setUsername($new_user_empresa->getEmail());
            $new_user_empresa->setEmail($new_user_empresa->getEmail());
            $new_user_empresa->setPassword(md5(microtime()));
            $new_user_empresa->setRoles(array('ROLE_EMPRESA'));
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($new_user_empresa);
            $em->flush();

            $mailerfosuser = $this->get('hoteles_backend.mailerfosuser')->sendWelcomeEmailMessage($new_user_empresa->getId());

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_userempresa_edit', array('id' => $new_user_empresa->getId())));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }

        return array(
            'entity' => $new_user_empresa,
            'form' => $form->createView(),
            'imagendevuelta' => $_POST["imagensubidanombre"],
        );
    }

    /**
     * Displays a form to edit an existing UserEmpresa entity.
     *
     * @Route("/{id}/edit", name="admin_userempresa_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $user_empresa = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($id);

        if (!$user_empresa) {
            $this->get('session')->setFlash('nosuccess', 'Usuario empresa no encontrado.');
            return $this->redirect($this->generateUrl("admin_hotel"));
        }

        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $empresa_user_actual = $securityContext->getToken()->getUser()->getEmpresa();
            if ($empresa_user_actual !== $user_empresa->getEmpresa()) {
                //Un usuario empresa esta intentando editar el perfil de un usuario empresa de otra empresa
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->redirect($this->generateUrl("admin_hotel"));
            }
        }
        $editForm = $this->generaFormularioDinamico($user_empresa)->getForm();
        $deleteForm = $this->createDeleteForm($id);
        $reactivateForm = $this->createReactivateForm($id);
        
        return array(
            'entity' => $user_empresa,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'reactivate_form' => $reactivateForm->createView(),
            'imagendevuelta' => $user_empresa->getImagen(),
        );
    }

    /**
     * Edits an existing UserEmpresa entity.
     *
     * @Route("/{id}/update", name="admin_userempresa_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:UserEmpresa:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $user_empresa = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($id);

        if (!$user_empresa) {
            $this->get('session')->setFlash('nosuccess', 'Usuario empresa no encontrado.');
            return $this->forward('HotelesBackendBundle:Hotel:index');
        }

        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $empresa_user_actual = $securityContext->getToken()->getUser()->getEmpresa();
            if ($empresa_user_actual !== $user_empresa->getEmpresa()) {
                //Un usuario empresa esta intentando actualizar el perfil de un usuario empresa de otra empresa
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->forward('HotelesBackendBundle:Hotel:index');
            }
        }
        //$editForm   = $this->createForm(new UserEmpresaType(), $entity);
        $editForm = $this->generaFormularioDinamico($user_empresa)->getForm();
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        $filtro = $this->get('hoteles_backend.funcionalidades');

        if ($filtro->compruebaEmail($editForm['email']->getData(), $user_empresa)) {
            $error = new formerror("Ese email está en uso");
            $editForm->get('email')->addError($error);
        }

        if ($filtro->compruebaUsername($editForm['email']->getData(), $user_empresa)) {
            $error = new formerror("Ese username está en uso");
            $editForm->get('username')->addError($error);
        }

        if ($editForm->isValid()) {

            $user_empresa->setImagen($_POST["imagensubidanombre"]);
            $em->persist($user_empresa);
            $em->flush();

            $this->get('session')->setFlash('success', 'Editado correctamente.');

            return $this->redirect($this->generateUrl('admin_userempresa_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo editar.');
        }

        return array(
            'entity' => $user_empresa,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a UserEmpresa entity.
     *
     * @Route("/{id}/delete", name="admin_userempresa_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $user_empresa = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($id);

            if (!$user_empresa) {
                $this->get('session')->setFlash('nosuccess', 'Usuario empresa no encontrado.');
                return $this->forward('HotelesBackendBundle:Hotel:index');
            }
            //Compruebo los permisos
            $securityContext = $this->container->get('security.context');
            if ($securityContext->isGranted('ROLE_EMPRESA')) {
                $empresa_user_actual = $securityContext->getToken()->getUser()->getEmpresa();
                if ($empresa_user_actual !== $user_empresa->getEmpresa()) {
                    //Un usuario empresa esta intentando suspender el perfil de un usuario empresa de otra empresa
                    $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                    return $this->forward('HotelesBackendBundle:Hotel:index');
                }
            }
            $user_empresa->suspendUser();
            $em->persist($user_empresa);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_userempresa'));
    }

    /**
     * Deletes a UserHotel entity.
     *
     * @Route("/{id}/reactivate", name="admin_userempresa_reactivate")
     * @Method("post")
     */
    public function reactivateUserAction($id)
    {
        $form = $this->createReactivateForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $user_empresa = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($id);

            if (!$user_empresa) {
                throw $this->createNotFoundException('Unable to find UserEmpresa entity.');
            }
            //Compruebo los permisos
            $securityContext = $this->container->get('security.context');
            if ($securityContext->isGranted('ROLE_ADMIN')) {
                $user_empresa->unsuspendUser();
                $em->persist($user_empresa);
                $em->flush();
            } else {
                throw $this->createNotFoundException('No tienes permiso...');
            }
        }
        return $this->redirect($this->generateUrl('admin_userempresa'));
    }
    
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm();
    }

    private function createReactivateForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }
    
    public function generaFormularioDinamico($user_empresa)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $securityContext = $this->container->get('security.context');

        $form = $this->createFormBuilder($user_empresa);
        $idUser = $securityContext->getToken()->getUser()->getId();

        $query_user = $this->getDoctrine()->getRepository('HotelesBackendBundle:Empresa')->createQueryBuilder('u');

        //Filtro para que un usuario vea solamente sus empresas
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $user = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($idUser);
            $idEmpresa = $user->getEmpresa()->getId();
            $query_user->where('u.id = :idEmpresa')->setParameter('idEmpresa', $idEmpresa);
        }

        $form->add('empresa', 'entity', array(
                    'class' => 'HotelesBackendBundle:Empresa',
                    'query_builder' => $query_user,
                ))
                ->add('email')
                ->add('nombrecompleto');

        return $form;
    }

}
