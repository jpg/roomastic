<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Lugar;
use Hoteles\BackendBundle\Form\LugarType;

/**
 * Lugar controller.
 *
 * @Route("/admin/lugares")
 */
class LugarController extends Controller
{

    /**
     * Lists all Lugar entities.
     *
     * @Route("/", name="admin_lugares")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:Lugar')->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $this->get('request')->query->get('page', 1)/* page number */, $this->get('request')->query->get('show', $this->container->getParameter('paginacionpordefecto'))/* limit per page */
        );

        return array(
            'entities' => $entities,
            'pagination' => $pagination,
        );
    }

    /**
     * Lists all Lugar entities.
     *
     * @Route("/filtramunicipios/{idprovincia}/{obligatorio}", name="admin_lugaresfiltramunicipio", options={"expose"=true})
     * @Template()
     */
    public function sacamunicipiosAction($idprovincia, $obligatorio)
    {

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Municipio');
        $query_municipios = $repository->createQueryBuilder('m')
                ->where('m.provincia = :provincia')
                ->setParameter('provincia', $idprovincia)
                ->orderBy('m.nombre', 'ASC');
        $municipios = $query_municipios->getQuery()->getResult();

        return array(
            'obligatorio' => $obligatorio,
            'municipios' => $municipios,
        );
    }

    /**
     * Lists all zonas entities.
     *
     * @Route("/filtrazonas/{idprovincia}/{idmunicipio}/{obligatorio}", name="admin_lugaresfiltralugares", options={"expose"=true})
     * @Template()
     */
    public function sacalugaresAction($idprovincia, $idmunicipio, $obligatorio)
    {

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Lugar');
        $query_lugares = $repository->createQueryBuilder('m')
                ->where('m.provincia = :provincia')
                ->setParameter('provincia', $idprovincia)
                ->andWhere('m.municipio = :municipio')
                ->setParameter('municipio', $idmunicipio)
                ->orderBy('m.zona', 'ASC');
        $lugares = $query_lugares->getQuery()->getResult();

        return array(
            'obligatorio' => $obligatorio,
            'lugares' => $lugares,
        );
    }

    /**
     * Finds and displays a Lugar entity.
     *
     * @Route("/{id}/show", name="admin_lugares_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Lugar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lugar entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Lugar entity.
     *
     * @Route("/new", name="admin_lugares_new")
     * @Template()
     */
    public function newAction()
    {

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Provincia');
        $query_provincias = $repository->createQueryBuilder('p')->orderBy('p.nombre', 'ASC');

        $provincias = $query_provincias->getQuery()->getResult();

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Municipio');
        $query_municipios = $repository->createQueryBuilder('m')
                ->orderBy('m.nombre', 'ASC');

        //ladybug_dump($provincias); exit;

        $entity = new Lugar();
        $form = $this->createForm(new LugarType(), $entity, array("attr" => array("provincias" => $query_provincias, "municipios" => $query_municipios)));

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Lugar entity.
     *
     * @Route("/create", name="admin_lugares_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Lugar:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Lugar();

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Provincia');
        $query_provincias = $repository->createQueryBuilder('p')->orderBy('p.nombre', 'ASC');

        $provincias = $query_provincias->getQuery()->getResult();

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Municipio');
        $query_municipios = $repository->createQueryBuilder('m')
                ->orderBy('m.nombre', 'ASC');

        //ladybug_dump($provincias); exit;

        $request = $this->getRequest();
        $form = $this->createForm(new LugarType(), $entity, array("attr" => array("provincias" => $query_provincias, "municipios" => $query_municipios)));
        //$form    = $this->createForm(new LugarType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_lugares_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Lugar entity.
     *
     * @Route("/{id}/edit", name="admin_lugares_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Lugar')->find($id);



        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lugar entity.');
        }


        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Provincia');
        $query_provincias = $repository->createQueryBuilder('p')->orderBy('p.nombre', 'ASC');

        $provincias = $query_provincias->getQuery()->getResult();

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Municipio');
        $query_municipios = $repository->createQueryBuilder('m')
                ->orderBy('m.nombre', 'ASC');


        $editForm = $this->createForm(new LugarType(), $entity, array("attr" => array("provincias" => $query_provincias, "municipios" => $query_municipios)));

        //$editForm = $this->createForm(new LugarType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Lugar entity.
     *
     * @Route("/{id}/update", name="admin_lugares_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Lugar:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Lugar')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lugar entity.');
        }

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Provincia');
        $query_provincias = $repository->createQueryBuilder('p')->orderBy('p.nombre', 'ASC');

        $provincias = $query_provincias->getQuery()->getResult();

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Municipio');
        $query_municipios = $repository->createQueryBuilder('m')
                ->orderBy('m.nombre', 'ASC');


        $editForm = $this->createForm(new LugarType(), $entity, array("attr" => array("provincias" => $query_provincias, "municipios" => $query_municipios)));

        //$editForm   = $this->createForm(new LugarType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_lugares_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Lugar entity.
     *
     * @Route("/{id}/delete", name="admin_lugares_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Lugar')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Lugar entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_lugares'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
