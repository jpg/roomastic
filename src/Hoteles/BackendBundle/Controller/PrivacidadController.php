<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Privacidad;
use Hoteles\BackendBundle\Form\PrivacidadType;

/**
 * Privacidad controller.
 *
 * @Route("/admin/privacidad")
 */
class PrivacidadController extends Controller
{

    /**
     * Lists all Privacidad entities.
     *
     * @Route("/", name="admin_privacidad")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:Privacidad')->findAll();

        if ($entities) {
            return $this->redirect($this->generateUrl('admin_privacidad_edit', array('id' => $entities[0]->getId())));
        } else {
            return $this->redirect($this->generateUrl('admin_privacidad_new'));
        }

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Privacidad entity.
     *
     * @Route("/{id}/show", name="admin_privacidad_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Privacidad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Privacidad entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Privacidad entity.
     *
     * @Route("/new", name="admin_privacidad_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Privacidad();
        $form = $this->createForm(new PrivacidadType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Privacidad entity.
     *
     * @Route("/create", name="admin_privacidad_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Privacidad:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Privacidad();
        $request = $this->getRequest();
        $form = $this->createForm(new PrivacidadType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_privacidad_edit', array('id' => $entity->getId())));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Privacidad entity.
     *
     * @Route("/{id}/edit", name="admin_privacidad_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Privacidad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Privacidad entity.');
        }

        $editForm = $this->createForm(new PrivacidadType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Privacidad entity.
     *
     * @Route("/{id}/update", name="admin_privacidad_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Privacidad:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Privacidad')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Privacidad entity.');
        }

        $editForm = $this->createForm(new PrivacidadType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Editado correctamente.');

            return $this->redirect($this->generateUrl('admin_privacidad_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo editar.');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Privacidad entity.
     *
     * @Route("/{id}/delete", name="admin_privacidad_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Privacidad')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Privacidad entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Borrado correctamente.');
        }

        return $this->redirect($this->generateUrl('admin_privacidad'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
