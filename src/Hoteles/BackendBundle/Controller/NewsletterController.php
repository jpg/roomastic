<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Newsletter;
use Hoteles\BackendBundle\Form\NewsletterType;

/**
 * Newsletter controller.
 *
 * @Route("/admin/newsletter")
 */
class NewsletterController extends Controller
{

    /**
     * Lists all Newsletter entities.
     *
     * @Route("/{email}", name="admin_newsletter", defaults={"email"= null},options={"expose"=true})
     * @Template()
     */
    public function indexAction($email)
    {
        $em = $this->getDoctrine()->getEntityManager();

        if (is_null($email)) {
            $usuarios_newsletter = $em->getRepository('HotelesBackendBundle:Newsletter')->findAll();
        } else {
            //Hay que refactorizarlo para que haga una unica consulta JOIN
            $user = $em->getRepository('HotelesBackendBundle:User')->findOneBy(array('email' => $email));
            if (is_null($user)) {
                $this->get('session')->setFlash('nosuccess', 'No se encontro ningun usuario con ese e-mail.');
                $usuarios_newsletter = $em->getRepository('HotelesBackendBundle:Newsletter')->findAll();
            } else {
                $usuarios_newsletter = $em->getRepository('HotelesBackendBundle:Newsletter')->findBy(array('usuario' => $user->getId()));
            }
        }

        return array('entities' => $usuarios_newsletter);
    }

    /**
     * Finds and displays a Newsletter entity.
     *
     * @Route("/{id}/show", name="admin_newsletter_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Newsletter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Newsletter entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Newsletter entity.
     *
     * @Route("/new", name="admin_newsletter_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Newsletter();
        $form = $this->createForm(new NewsletterType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Newsletter entity.
     *
     * @Route("/create", name="admin_newsletter_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Newsletter:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Newsletter();
        $request = $this->getRequest();
        $form = $this->createForm(new NewsletterType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_newsletter_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Newsletter entity.
     *
     * @Route("/{id}/edit", name="admin_newsletter_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Newsletter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Newsletter entity.');
        }

        $editForm = $this->createForm(new NewsletterType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Newsletter entity.
     *
     * @Route("/{id}/update", name="admin_newsletter_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Newsletter:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Newsletter')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Newsletter entity.');
        }

        $editForm = $this->createForm(new NewsletterType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_newsletter_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Newsletter entity.
     *
     * @Route("/{id}/delete", name="admin_newsletter_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $newsletter = $em->getRepository('HotelesBackendBundle:Newsletter')->find($id);

        if (!$newsletter) {
            $respuesta = array();
            $respuesta['type'] = 'error';
            $respuesta['message'] = 'Newsletter no encontrado.';
            return $this->get("backend_utilities.json_response")->JSONResponse($respuesta, 400);
        } else {

            $newsletter->makeNotActivo();

            $em->persist($newsletter);
            $em->flush();
            $respuesta = array();
            $respuesta['type'] = 'success';
            $respuesta['message'] = 'Newsletter borrado.';
            return $this->get("backend_utilities.json_response")->JSONResponse($respuesta, 200);
        }
    }

    /**
     * Deletes a Newsletter entity.
     *
     * @Route("/{id}/activate", name="admin_newsletter_activate")
     * @Method("post")
     */
    public function activateAction($id)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $newsletter = $em->getRepository('HotelesBackendBundle:Newsletter')->find($id);

        if (!$newsletter) {
            $respuesta = array();
            $respuesta['type'] = 'error';
            $respuesta['message'] = 'Newsletter no encontrado.';
            return $this->get("backend_utilities.json_response")->JSONResponse($respuesta, 400);
        } else {

            $newsletter->makeActivo();
            $em->persist($newsletter);
            $em->flush();
            $respuesta = array();
            $respuesta['type'] = 'success';
            $respuesta['message'] = 'Newsletter activado.';
            return $this->get("backend_utilities.json_response")->JSONResponse($respuesta, 200);
        }
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
