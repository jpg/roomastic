<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Home;
use Hoteles\BackendBundle\Form\HomeType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Home controller.
 *
 * @Route("/admin/homeconfiguracion")
 */
class HomeController extends Controller
{

    /**
     * Lists all Home entities.
     *
     * @Route("/", name="admin_homeconfiguracion")
     * @Template()
     */
    public function indexAction()
    {

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Home');
        $query = $repository->createQueryBuilder('f')
                ->orderBy('f.posicion', 'ASC')
                ->getQuery();
        $entities = $query->getResult();

        return array('entities' => $entities);
    }

    /**
     * reordena lista de faqs
     *
     * @Route("/ordena/{orden}", name="admin_ordenahome", options={"expose"=true})
     * @Template()
     */
    public function ordenaAction($orden)
    {
        $elementos = explode('_', $orden);
        unset($elementos[0]);

        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('HotelesBackendBundle:Home')->findAll();

        $cont = 1;
        foreach ($elementos as $orden) {
            foreach ($entity as $faq) {
                if ($orden == $faq->getId()) {
                    $faq->setPosicion($cont);
                    $em->persist($faq);
                }
            }
            $cont = $cont + 1;
        }
        $em->flush();

        echo "1";
        exit;

        return array();
    }

    /**
     * Finds and displays a Home entity.
     *
     * @Route("/{id}/show", name="admin_homeconfiguracion_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Home')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Home entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Home entity.
     *
     * @Route("/new", name="admin_homeconfiguracion_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Home();
        $form = $this->createForm(new HomeType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Home entity.
     *
     * @Route("/create", name="admin_homeconfiguracion_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Home:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Home();
        $request = $this->getRequest();
        $form = $this->createForm(new HomeType(), $entity);
        $form->bindRequest($request);

        if ($_FILES["hoteles_backendbundle_hometype"]["name"]["imagen"] == '') {
            $error = new formerror("Debe de incluir una imagen");
            $form->get('imagen')->addError($error);
        }

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getEntityManager();
            $totalFaqs = $em->getRepository('HotelesBackendBundle:Faqs')->findAll();

            $entity->setPosicion(count($totalFaqs) + 1);

            //if ($form['imagen']->getData() !== null ) {
            $someNewFilename = md5(uniqid(microtime(), true)) . '.png';
            $form['imagen']->getData()->move($this->get('kernel')->getRootDir() . "/../web/uploads/home", $someNewFilename);
            $entity->setImagen($someNewFilename);
            //}

            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_homeconfiguracion'));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Home entity.
     *
     * @Route("/{id}/edit", name="admin_homeconfiguracion_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Home')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Home entity.');
        }

        $editForm = $this->createForm(new HomeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Home entity.
     *
     * @Route("/{id}/update", name="admin_homeconfiguracion_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Home:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Home')->find($id);

        $image = $entity->getImagen();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Home entity.');
        }

        $editForm = $this->createForm(new HomeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);


        if ($editForm->isValid()) {

            if ($editForm['imagen']->getData() == null) {
                $entity->setImagen($image);
            } else {
                $someNewFilename1 = md5(uniqid(microtime(), true)) . '.png';
                $editForm['imagen']->getData()->move($this->get('kernel')->getRootDir() . "/../web/uploads/home", $someNewFilename1);
                $entity->setImagen($someNewFilename1);
            }

            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Editado correctamente.');

            return $this->redirect($this->generateUrl('admin_homeconfiguracion_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo editar.');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Home entity.
     *
     * @Route("/{id}/delete", name="admin_homeconfiguracion_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Home')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Home entity.');
            }

            $em->remove($entity);
            $em->flush();
            $this->get('session')->setFlash('success', 'Borrado correctamente.');
        }

        return $this->redirect($this->generateUrl('admin_homeconfiguracion'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}/delete", name="admin_homeconfiguracion_deleteget")
     * @Method("get")
     */
    public function deletegetAction($id)
    {

        $request = $this->getRequest();

        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('HotelesBackendBundle:Home')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Home entity.');
        }

        $em->remove($entity);
        $em->flush();

        $this->get('session')->setFlash('success', 'Borrado correctamente.');

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

}
