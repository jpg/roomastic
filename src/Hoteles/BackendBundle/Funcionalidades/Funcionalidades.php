<?php

namespace Hoteles\BackendBundle\Funcionalidades;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 */
class Funcionalidades
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     * 
     * @param ContainerInterface $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Comprueba si existe un usuario con ese email que no sea el mismo
     *
     */
    public function compruebaEmail($email, $entityactual = null)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $entity = $em->getRepository('HotelesBackendBundle:User')->findOneBy(array('email' => $email));
        if ($entity) {
            if ($entityactual != null) {
                if ($entity == $entityactual) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Comprueba si existe un usuario con ese username que no sea el mismo
     *
     */
    public function compruebaUsername($username, $entityactual = null)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $entity = $em->getRepository('HotelesBackendBundle:User')->findOneBy(array('username' => $username));
        if ($entity) {
            if ($entityactual != null) {
                if ($entity == $entityactual) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Comprueba si existe un usuario con ese email y devuelve ese usuario
     *
     */
    public function sacaUserDesdeMail($email)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $entity = $em->getRepository('HotelesBackendBundle:User')->findOneBy(array('email' => $email));
        return $entity;
    }

    public function arrayImagenes($idImagenes)
    {
        $arrayImagenes = array();
        foreach ($idImagenes as $key => $value) {
            $em = $this->container->get('doctrine')->getEntityManager();
            $imagenHotel = $em->getRepository('HotelesBackendBundle:Imagenhotel')->find($value);
            $arrayImagenes[] = $imagenHotel;
        }
        return $arrayImagenes;
    }

    public function sacaProvinciaPorNombre($nombre)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $entity = $em->getRepository('HotelesBackendBundle:Provincia')->findOneBy(array('nombre' => $nombre));
        return $entity;
    }

    public function sacaMunicipioPorNombreYProvincia($nombre, $idProvincia)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $entity = $em->getRepository('HotelesBackendBundle:Municipio')->findOneBy(array(
            'nombre' => $nombre,
            'provincia' => $idProvincia
        ));
        return $entity;
    }

    public function sacaZonaPorNombreProvinciaMunicipio($nombre, $idProvincia, $idMunicipio)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $entity = $em->getRepository('HotelesBackendBundle:Lugar')->findOneBy(array(
            'zona' => $nombre,
            'provincia' => $idProvincia,
            'municipio' => $idMunicipio,
        ));
        return $entity;
    }

}
