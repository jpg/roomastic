<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Hoteles\BackendBundle\Interfaces\UserCanOfferInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Hoteles\FrontendBundle\Validator\Constraints as UserBasicoAssert;
/**
 * Hoteles\BackendBundle\Entity\UserWeb
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Hoteles\BackendBundle\Repository\UserBasicoRepository")
 * @UniqueEntity(fields="dni",message="user.dni.unique",groups={"registration"})
 */
class UserBasico extends \Hoteles\BackendBundle\Entity\User
{

    const USER_STATUS_OFFER_PENDING = 3;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255,nullable=true)
     */
    private $nombre;

    /**
     * @var string $apellidos
     *
     * @ORM\Column(name="apellidos", type="string", length=255,nullable=true)
     */
    private $apellidos;

    /**
     * @var string $dni
     *
     * @ORM\Column(name="dni", type="string", length=255,nullable=true,unique=true)
     * @UserBasicoAssert\IsDni
     */
    private $dni;

    /**
     * @var string $direccioncompleta
     *
     * @ORM\Column(name="direccioncompleta", type="string", length=255,nullable=true)
     */
    private $direccioncompleta;

    /**
     * @var string $telefono
     *
     * @ORM\Column(name="telefono", type="string", length=255,nullable=true)
     * @Assert\MinLength(
     *      limit = 9
     * )
     * @Assert\MaxLength(
     *      limit = 9
     * )
     */
    private $telefono;

    /**
     * @var string $idfacebook
     *
     * @ORM\Column(name="idfacebook", type="string", length=25,nullable=true)
     */
    private $facebookID;

    /**
     * @var string $twitterID
     *
     * @ORM\Column(name="idtwitter", type="string", length=255,nullable=true)
     */
    private $twitterID;

    /**
     * @var string $googleID
     *
     * @ORM\Column(name="idgoogle", type="string", length=255,nullable=true)
     */
    private $googleID;

    /**
     * @var string $googleID
     * Valores: facebook, twitter, web
     * @ORM\Column(name="origin", type="string", length=255,nullable=true)
     */
    private $origin = "web";

    public function getOrigin()
    {
        return $this->origin;
    }

    public function setOrigin($origin)
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set dni
     *
     * @param string $dni
     */
    public function setDni($dni)
    {
        $this->dni = $dni;
    }

    /**
     * Get dni
     *
     * @return string 
     */
    public function getDni()
    {
        return $this->dni;
    }

    /**
     * Set direccioncompleta
     *
     * @param string $direccioncompleta
     */
    public function setDireccioncompleta($direccioncompleta)
    {
        $this->direccioncompleta = $direccioncompleta;
    }

    /**
     * Get direccioncompleta
     *
     * @return string 
     */
    public function getDireccioncompleta()
    {
        return $this->direccioncompleta;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    public function setFacebookID($facebookID)
    {
        $this->facebookID = $facebookID;
        return $this;
    }

    public function getFacebookID()
    {
        return $this->facebookID;
    }

    public function getTwitterID()
    {
        return $this->twitterID;
    }

    public function setTwitterID($twitterID)
    {
        $this->twitterID = $twitterID;
        return $this;
    }

    public function getGoogleID()
    {
        return $this->googleID;
    }

    public function setGoogleID($googleID)
    {
        $this->googleID = $googleID;
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    public function getUserData($limite_ofertas = 'si')
    {
        $data = array();
        $data['nombre'] = $this->getNombre();
        $data['apellidos'] = $this->getApellidos();
        $data['dni'] = $this->getDni();
        $data['direccion'] = $this->getDireccioncompleta();
        $data['telefono'] = $this->getTelefono();
        $data['email'] = $this->getEmail();
        $data['canOffer'] = $this->canOffer($limite_ofertas);
        return $data;
    }

    public function canOffer($limite_ofertas = 'si')
    {
        if ($this->hasOfferMade() && $limite_ofertas === 'si') {
            return UserCanOfferInterface::OFERTA_PENDIENTE;
        }
        if ($this->isBanned()) {
            return UserCanOfferInterface::USUARIO_BANEADO;
        } elseif ($this->isSuspended()) {
            return UserCanOfferInterface::NO_PUEDE_OFERTAR;
        }
        $resultado = true;
        $resultado &= strlen($this->getNombre()) > 0;
        $resultado &= strlen($this->getApellidos()) > 0;
        $resultado &= strlen($this->getDni()) > 0;
        $resultado &= strlen($this->getApellidos()) > 0;
        $resultado &= strlen($this->getTelefono()) > 0;
        $resultado &= strlen($this->getEmail()) > 0;

        if ($resultado) {
            return UserCanOfferInterface::PUEDE_OFERTAR;
        } else {
            return UserCanOfferInterface::FALTAN_DATOS;
        }
    }

    public function setFBData($fbdata)
    {
        //Esta funcion actualiza los datos de un usuario básico con los de facebook
        if (isset($fbdata['id'])) {
            $this->setFacebookID($fbdata['id']);
        }
        if (isset($fbdata['first_name'])) {
            $this->setNombre($fbdata['first_name']);
        }
        if (isset($fbdata['last_name'])) {
            $this->setApellidos($fbdata['last_name']);
        }
        if (isset($fbdata['email']) && $this->getEmail() == NULL) {
            $this->setEmail($fbdata['email']);
        } else {
//            $this->setEmail(sha1(uniqid(mt_rand(), true)) .
//                    '@' . sha1(uniqid(mt_rand(), true) .
//                            'roomastic.com'));
        }
    }

    public function setGoogleData($google_user_data)
    {
        if (isset($google_user_data['id'])) {
            $this->setGoogleID($google_user_data['id']);
        }
        if (isset($google_user_data['given_name'])) {
            $this->setNombre(filter_var($google_user_data['given_name'], FILTER_SANITIZE_SPECIAL_CHARS));
        }
        if (isset($google_user_data['family_name'])) {
            $this->setApellidos(filter_var($google_user_data['family_name'], FILTER_SANITIZE_SPECIAL_CHARS));
        }
        if (isset($google_user_data['email']) && $this->getEmail() == NULL) {
            $this->setEmail(filter_var($google_user_data['email'], FILTER_SANITIZE_EMAIL));
        } else {
//            $this->setEmail(sha1(uniqid(mt_rand(), true)) .
//                    '@' . sha1(uniqid(mt_rand(), true) .
//                            'roomastic.com'));
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getNombreCompleto()
    {
        return $this->nombre . " " . $this->apellidos;
    }

    public function getPublicUsername()
    {
        $resultado = $this->getNombreCompleto() . "(basico)";
        return $resultado;
    }

    public function offerMade()
    {
        $this->setStatus(self::USER_STATUS_OFFER_PENDING);
    }

    public function hasOfferMade()
    {
        return $this->getStatus() === self::USER_STATUS_OFFER_PENDING;
    }

    public function removeOfferMade()
    {
        error_log('Removiendo oferta del usuario...');
        if ($this->getStatus() === self::USER_STATUS_OFFER_PENDING) {
            $this->setStatus(self::USER_STATUS_ENABLED);
            error_log('Oferta removida correctamente');
        } else {
            $fecha = date('Y-m-d H:i:s');
            error_log('Error: Fecha: ' . $fecha . ' Usuario (' . $this->getId() . ') con estado diferente al esperado: (' . $this->getStatus() . ')');
        }
    }

    public function getTypeOf()
    {
        return 'basico';
    }

    public function getPublicOfferData()
    {
        $resultado = array();
        $resultado['email'] = $this->getEmail();
        $resultado['nombre_completo'] = $this->getNombreCompleto();
        $resultado['dni'] = $this->getDni();
        $resultado['direccion'] = $this->getDireccioncompleta();
        $resultado['telefono'] = $this->getTelefono();
        return $resultado;
    }

}
