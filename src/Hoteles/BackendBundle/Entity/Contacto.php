<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hoteles\BackendBundle\Entity\Contacto
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Contacto
{

    const ATENDIDO = 1;
    const NO_ATENDIDO = 0;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string $apellidos
     *
     * @ORM\Column(name="apellidos", type="string", length=255)
     */
    private $apellidos;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var text $texto
     *
     * @ORM\Column(name="texto", type="text")
     */
    private $texto;

    /**
     * @var boolean $atendido
     *
     * @ORM\Column(name="atendido", type="boolean")
     */
    private $atendido;

    /**
     * @var datetime $fecha
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var datetime $fecha
     *
     * @ORM\Column(name="fecha_atendido", type="datetime",nullable=true)
     */
    private $fecha_atendido;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    /**
     * Get apellidos
     *
     * @return string 
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set texto
     *
     * @param text $texto
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;
    }

    /**
     * Get texto
     *
     * @return text 
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set atendido
     *
     * @param boolean $atendido
     */
    public function setAtendido($atendido)
    {
        $this->atendido = $atendido;
    }

    /**
     * Get atendido
     *
     * @return boolean 
     */
    public function getAtendido()
    {
        return $this->atendido;
    }

    /**
     * Set fecha
     *
     * @param datetime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * Get fecha
     *
     * @return datetime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    public function markAsAtendido()
    {
        $this->setAtendido(Contacto::ATENDIDO);
        $this->setFechaAtendido(new \DateTime());
    }

    /**
     * Set fecha_atendido
     *
     * @param datetime $fechaAtendido
     */
    public function setFechaAtendido($fechaAtendido)
    {
        $this->fecha_atendido = $fechaAtendido;
    }

    /**
     * Get fecha_atendido
     *
     * @return datetime 
     */
    public function getFechaAtendido()
    {
        return $this->fecha_atendido;
    }

    public function getNombreCompleto()
    {
        return $this->nombre . " " . $this->apellidos;
    }

}