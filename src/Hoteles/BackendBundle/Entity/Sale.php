<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Hoteles\BackendBundle\Entity\Sale
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Sale
{
    /*
     * Venta realizada con éxito. Tiene asociada una transacción con TPV_OK 
     *      
     */

    const VENTA_REALIZADA = 0;

    /*
     *  Este valor es el por defecto, cuando se crea la venta tras aceptar una oferta y enviar el e-mail con el link.
     * Hasta que no se realice una transacción con TPV_OK este valor no se actualiza
     *      
     */
    const VENTA_PENDIENTE = 1;

    /*
     * Este valor se usa cuando se caduca una oferta
     *      
     */
    const VENTA_CADUCADA = 2;

    /**
     * Este valor se usa cuando hay un error en la venta. Por ejemplo la oferta esta en un estado no válido
     *      
     */
    const VENTA_ERROR = 3;

    /**
     * Este valor se usa cuando esta venta ya no es accesible porque se ha pagado otra contraoferta del grupo de contraofertas
     */
    const VENTA_CONTRAOFERTA_RECHAZADA = 4;

    /**
     *  @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Oferta")
     * @ORM\JoinColumn(name="oferta_id",referencedColumnName="id",onDelete="SET NULL")
     */
    private $oferta;

    /**
     * @ORM\Column(name="cantidad", type="float")
     *      */
    private $amount;

    /**
     * @var integer $aceptada
     *
     * @ORM\Column(name="status_venta", type="integer")
     */
    private $status;

    /**
     * @var string $signature
     *
     * @ORM\Column(name="firma_transaccion", type="string", length=255)
     */
    private $firma;

    /**
     * @var string $order_number FORMATO: ####XXXXXXXXXXXX 
     * (4 numeros seguidos de letras en minuscula hasta 12)
     * @ORM\Column(name="order_number", type="string", length=12, nullable="true")
     */
    private $order_number;

    /**
     * @var datetime $fecha_creacion
     *
     * @ORM\Column(name="fecha_creacion", type="datetime")
     */
    private $fecha_creacion;

    /**
     * @var datetime $fecha_pago
     *
     * @ORM\Column(name="fecha_pago", type="datetime", nullable="true")
     */
    private $fecha_pago;

    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="sale")
     * */
    private $transactions;

    public function __construct()
    {
        $this->setFechaCreacion(new \DateTime());
        $this->transactions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set status
     *
     * @param integer $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set firma
     *
     * @param string $firma
     */
    public function setFirma($firma)
    {
        $this->firma = $firma;
    }

    /**
     * Get firma
     *
     * @return string 
     */
    public function getFirma()
    {
        return $this->firma;
    }

    /**
     * Set order_number
     *
     * @param string $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->order_number = $orderNumber;
    }

    /**
     * Get order_number
     *
     * @return string 
     */
    public function getOrderNumber()
    {
        return $this->order_number;
    }

    /**
     * Set fecha_creacion
     *
     * @param datetime $fechaCreacion
     */
    public function setFechaCreacion($fechaCreacion)
    {
        $this->fecha_creacion = $fechaCreacion;
    }

    /**
     * Get fecha_creacion
     *
     * @return datetime 
     */
    public function getFechaCreacion()
    {
        return $this->fecha_creacion;
    }

    /**
     * Set fecha_pago
     *
     * @param datetime $fechaPago
     */
    public function setFechaPago($fechaPago)
    {
        $this->fecha_pago = $fechaPago;
    }

    /**
     * Get fecha_pago
     *
     * @return datetime 
     */
    public function getFechaPago()
    {
        return $this->fecha_pago;
    }

    /**
     * Set oferta
     *
     * @param Hoteles\BackendBundle\Entity\Oferta $oferta
     */
    public function setOferta(\Hoteles\BackendBundle\Entity\Oferta $oferta)
    {
        $this->oferta = $oferta;
    }

    /**
     * Get oferta
     *
     * @return Hoteles\BackendBundle\Entity\Oferta 
     */
    public function getOferta()
    {
        return $this->oferta;
    }

    private function updateStatus($status)
    {
        $this->setStatus($status);
    }

    public function markAsPayed()
    {
        $this->updateStatus(Sale::VENTA_REALIZADA);
        $this->setFechaPago(new \DateTime());
    }

    public function markAsExpired()
    {
        $this->updateStatus(Sale::VENTA_CADUCADA);
    }

    public function markAsPending()
    {
        $this->updateStatus(Sale::VENTA_PENDIENTE);
    }

    public function markAsError()
    {
        $this->updateStatus(Sale::VENTA_ERROR);
    }

    public function markAsCounterOfferRejected()
    {
        $this->updateStatus(Sale::VENTA_CONTRAOFERTA_RECHAZADA);
    }

    /**
     * Add transactions
     *
     * @param Hoteles\BackendBundle\Entity\Transaction $transaction
     */
    public function addTransaction(\Hoteles\BackendBundle\Entity\Transaction $transaction)
    {
        $this->transactions[] = $transaction;
    }

    /**
     * Get transactions
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    public function canBePayed()
    {

        return ($this->getStatus() === Sale::VENTA_PENDIENTE ||
                $this->getStatus() === Sale::VENTA_ERROR) && $this->getOferta()->canBePayed();
    }

    public function isPayed()
    {
        return $this->getStatus() === Sale::VENTA_REALIZADA;
    }

}
