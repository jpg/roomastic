<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Hoteles\BackendBundle\Entity\Lugar
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Lugar
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \provincia
     *
     * @ORM\ManyToOne(targetEntity="Provincia")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="provincia", referencedColumnName="id")
     * })
     */
    private $provincia;

    /**
     * @var \municipio
     *
     * @ORM\ManyToOne(targetEntity="Municipio")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="municipio", referencedColumnName="id")
     * })
     */
    private $municipio;

    /**
     * @var string $zona
     *
     * @ORM\Column(name="zona", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $zona;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get provincia
     *
     * @return \Hoteles\BackendBundle\Entity\Provincia
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set provincia
     *
     * @param \Hoteles\BackendBundle\Entity\Provincia $provincia
     * @return Event
     */
    public function setProvincia(\Hoteles\BackendBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Set municipio
     *
     * @param \Hoteles\BackendBundle\Entity\Municipio $Municipio
     * @return Event
     */
    public function setMunicipio(\Hoteles\BackendBundle\Entity\Municipio $municipio = null)
    {
        $this->municipio = $municipio;
    }

    /**
     * Get municipio
     *
     * @return \Hoteles\BackendBundle\Entity\Municipio
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set zona
     *
     * @param string $zona
     */
    public function setZona($zona)
    {
        $this->zona = $zona;
    }

    /**
     * Get zona
     *
     * @return string 
     */
    public function getZona()
    {
        return $this->zona;
    }

    public function __toString()
    {
        return $this->getZona();
    }

}