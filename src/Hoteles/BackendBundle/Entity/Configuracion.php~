<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;

/**
 * Hoteles\BackendBundle\Entity\Configuracion
 *
 * @ORM\Table()
 * @ORM\Entity
 * @Assert\Callback(methods={"sonHorasValidas"})
 */
class Configuracion
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer $descuentoentero
     *
     * @ORM\Column(name="descuentoentero", type="integer")
     * @Assert\NotBlank(message="Este valor (descuento entero) no puede estar en blanco")
     * 
     */
    private $descuentoentero;

    /**
     * @var integer $descuentodecimal
     *
     * @ORM\Column(name="descuentodecimal", type="integer")
     * @Assert\NotBlank(message="Este valor (descuento decimal) no puede estar en blanco")
     * 
     */
    private $descuentodecimal;

    /**
     * @var integer $tiempohotel
     *
     * @ORM\Column(name="tiempohotel", type="integer")
     * @Assert\NotBlank(message="Este valor (tiempo hotel) no puede estar en blanco")
     * 
     */
    private $tiempohotel;

    /**
     * @var integer $tiempousuariosoferta
     *
     * @ORM\Column(name="tiempousuariosoferta", type="integer")
     * @Assert\NotBlank(message="Este valor (tiempo usuarios oferta) no puede estar en blanco")
     * 
     */
    private $tiempousuariosoferta;

    /**
     * @var integer $tiempousuarioscontraoferta
     *
     * @ORM\Column(name="tiempousuarioscontraoferta", type="integer")
     * @Assert\NotBlank(message="Este valor (tiempo usuarios contraoferta) no puede estar en blanco")
     * 
     */
    private $tiempousuarioscontraoferta;

    /**
     * @var string $fbappid
     *
     * @ORM\Column(name="fbappid", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (facebook app id) no puede estar en blanco")
     * 
     */
    private $fbAppId;

    /**
     * @var string $fbappsecret
     *
     * @ORM\Column(name="fbappsecret", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (facebook app secret) no puede estar en blanco")
     * 
     */
    private $fbAppSecret;

    /**
     * @var string $fbappsecret
     *
     * @ORM\Column(name="googappid", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (google app id) no puede estar en blanco")
     * 
     */
    private $googAppId;

    /**
     * @var string $fbappsecret
     *
     * @ORM\Column(name="googappsecret", type="string", length=255)
     * 
     */
    private $googAppSecret;

    /**
     * @var string $googapikey
     *
     * @ORM\Column(name="googapikey", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (google api key) no puede estar en blanco")
     * 
     */
    private $googApiKey;

    /**
     * @var string $fbappsecret
     *
     * @ORM\Column(name="twitterappid", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (twitter id) no puede estar en blanco")
     * 
     */
    private $twitterAppId;

    /**
     * @var string $fbappsecret
     *
     * @ORM\Column(name="twitterappsecret", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (twitter secret) no puede estar en blanco")
     * 
     */
    private $twitterAppSecret;

    /**
     * @var string $fbappsecret
     *
     * @ORM\Column(name="tiempoRecordatorioPago", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (recordatorio pago) no puede estar en blanco")
     * 
     */
    private $tiempoRecordatorioPago;

    /**
     * @var string $fbappsecret
     *
     * @ORM\Column(name="tiempoAlertaPago", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (alerta pago) no puede estar en blanco")
     * 
     */
    private $tiempoAlertaPago;

    /**
     * @var string $entorno
     *
     * @ORM\Column(name="entorno", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (entorno) no puede estar en blanco")
     * 
     */
    private $entorno;

    /**
     * @var string $entorno
     *
     * @ORM\Column(name="nombre_facebook", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (nombre_facebook) no puede estar en blanco")
     * 
     */
    private $nombre_facebook;

    /**
     * @var string $entorno
     *
     * @ORM\Column(name="nombre_twitter", type="string", length=255)
     * @Assert\NotBlank(message="Este valor (nombre_twitter) no puede estar en blanco")
     * 
     */
    private $nombre_twitter;
    
    /**
     * @var integer $porcentajePrecioHotel
     *
     * @ORM\Column(name="porcentajePrecioHotel", type="integer")
     * @Assert\NotBlank(message="Este valor (porcentajePrecioHotel) no puede estar en blanco")
     * 
     */
    private $porcentajePrecioHotel;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descuentoentero
     *
     * @param integer $descuentoentero
     */
    public function setDescuentoentero($descuentoentero)
    {
        $this->descuentoentero = $descuentoentero;
    }

    /**
     * Get descuentoentero
     *
     * @return integer 
     */
    public function getDescuentoentero()
    {
        return $this->descuentoentero;
    }

    /**
     * Set descuentodecimal
     *
     * @param integer $descuentodecimal
     */
    public function setDescuentodecimal($descuentodecimal)
    {
        $this->descuentodecimal = $descuentodecimal;
    }

    /**
     * Get descuentodecimal
     *
     * @return integer 
     */
    public function getDescuentodecimal()
    {
        return $this->descuentodecimal;
    }

    /**
     * Set tiempohotel
     *
     * @param integer $tiempohotel
     */
    public function setTiempohotel($tiempohotel)
    {
        $this->tiempohotel = $tiempohotel;
    }

    /**
     * Get tiempohotel
     *
     * @return integer 
     */
    public function getTiempohotel()
    {
        return $this->tiempohotel;
    }

    /**
     * Set tiempousuariosoferta
     *
     * @param integer $tiempousuariosoferta
     */
    public function setTiempousuariosoferta($tiempousuariosoferta)
    {
        $this->tiempousuariosoferta = $tiempousuariosoferta;
    }

    /**
     * Get tiempousuariosoferta
     *
     * @return integer 
     */
    public function getTiempousuariosoferta()
    {
        return $this->tiempousuariosoferta;
    }

    /**
     * Set tiempousuarioscontraoferta
     *
     * @param integer $tiempousuarioscontraoferta
     */
    public function setTiempousuarioscontraoferta($tiempousuarioscontraoferta)
    {
        $this->tiempousuarioscontraoferta = $tiempousuarioscontraoferta;
    }

    /**
     * Get tiempousuarioscontraoferta
     *
     * @return integer 
     */
    public function getTiempousuarioscontraoferta()
    {
        return $this->tiempousuarioscontraoferta;
    }

    /**
     * Set fbappid
     *
     * @param string $googAppId
     */
    public function setGoogAppId($googAppId)
    {
        $this->googAppId = $googAppId;
        return $this;
    }

    /**
     * Get fbappid
     *
     * @return string 
     */
    public function getGoogAppId()
    {
        return $this->googAppId;
    }

    /**
     * Set fbappsecret
     *
     * @param string $googAppSecret
     */
    public function setGoogAppSecret($googAppSecret)
    {
        $this->googAppSecret = $googAppSecret;
    }

    /**
     * Get fbappsecret
     *
     * @return string 
     */
    public function getGoogAppSecret()
    {
        return $this->googAppSecret;
    }

    /**
     * Set fbappid
     *
     * @param string $twitterAppId
     */
    public function setTwitterAppId($twitterAppId)
    {
        $this->twitterAppId = $twitterAppId;
    }

    /**
     * Get fbappid
     *
     * @return string 
     */
    public function getTwitterAppId()
    {
        return $this->twitterAppId;
    }

    /**
     * Set fbappsecret
     *
     * @param string $twitterAppSecret
     */
    public function setTwitterAppSecret($twitterAppSecret)
    {
        $this->twitterAppSecret = $twitterAppSecret;
    }

    /**
     * Get fbappsecret
     *
     * @return string 
     */
    public function getTwitterAppSecret()
    {
        return $this->twitterAppSecret;
    }

    public function getGoogApiKey()
    {
        return $this->googApiKey;
    }

    public function setGoogApiKey($googApiKey)
    {
        $this->googApiKey = $googApiKey;
    }

    /**
     * Set fbAppId
     *
     * @param string $fbAppId
     */
    public function setFbAppId($fbAppId)
    {
        $this->fbAppId = $fbAppId;
    }

    /**
     * Get fbAppId
     *
     * @return string 
     */
    public function getFbAppId()
    {
        return $this->fbAppId;
    }

    /**
     * Set fbAppSecret
     *
     * @param string $fbAppSecret
     */
    public function setFbAppSecret($fbAppSecret)
    {
        $this->fbAppSecret = $fbAppSecret;
    }

    /**
     * Get fbAppSecret
     *
     * @return string 
     */
    public function getFbAppSecret()
    {
        return $this->fbAppSecret;
    }

    /**
     * Set tiempoRecordatorioPago
     *
     * @param string $tiempoRecordatorioPago
     */
    public function setTiempoRecordatorioPago($tiempoRecordatorioPago)
    {
        $this->tiempoRecordatorioPago = $tiempoRecordatorioPago;
    }

    /**
     * Get tiempoRecordatorioPago
     *
     * @return string 
     */
    public function getTiempoRecordatorioPago()
    {
        return $this->tiempoRecordatorioPago;
    }

    /**
     * Set tiempoAlertaPago
     *
     * @param string $tiempoAlertaPago
     */
    public function setTiempoAlertaPago($tiempoAlertaPago)
    {
        $this->tiempoAlertaPago = $tiempoAlertaPago;
    }

    /**
     * Get tiempoAlertaPago
     *
     * @return string 
     */
    public function getTiempoAlertaPago()
    {
        return $this->tiempoAlertaPago;
    }

    public function sonHorasValidas(ExecutionContext $context)
    {
        if ($this->getTiempousuariosoferta() < $this->getTiempoRecordatorioPago() ||
                $this->getTiempousuariosoferta() < $this->getTiempoAlertaPago()
        ) {
            $propertyPath = $context->getPropertyPath() . '.tiempousuariosoferta';
            $context->setPropertyPath($propertyPath);
            $context->addViolation('Este valor tiene que ser mayor que los recordatorios', array(), null);
        }
        if ($this->getTiempoRecordatorioPago() > $this->getTiempoAlertaPago()) {
            $propertyPath = $context->getPropertyPath() . '.tiempoalertapago';
            $context->setPropertyPath($propertyPath);
            $context->addViolation('Este valor tiene que ser mayor que el primer recordatorio', array(), null);
        }
    }

    /**
     * Set entorno
     *
     * @param string $entorno
     */
    public function setEntorno($entorno)
    {
        $this->entorno = $entorno;
    }

    /**
     * Get entorno
     *
     * @return string 
     */
    public function getEntorno()
    {
        return $this->entorno;
    }

    /**
     * Set nombre_facebook
     *
     * @param string $nombreFacebook
     */
    public function setNombreFacebook($nombreFacebook)
    {
        $this->nombre_facebook = $nombreFacebook;
    }

    /**
     * Get nombre_facebook
     *
     * @return string 
     */
    public function getNombreFacebook()
    {
        return $this->nombre_facebook;
    }

    /**
     * Set nombre_twitter
     *
     * @param string $nombreTwitter
     */
    public function setNombreTwitter($nombreTwitter)
    {
        $this->nombre_twitter = $nombreTwitter;
    }

    /**
     * Get nombre_twitter
     *
     * @return string 
     */
    public function getNombreTwitter()
    {
        return $this->nombre_twitter;
    }
    
    /**
     * Set porcentajePrecioHotel
     *
     * @param integer porcentajePrecioHotel
     */
    public function setPorcentajePrecioHotel($porcentajePrecioHotel)
    {
        $this->porcentajePrecioHotel = $porcentajePrecioHotel;
    }

    /**
     * Get porcentajePrecioHotel
     *
     * @return integer 
     */
    public function getPorcentajePrecioHotel()
    {
        return $this->porcentajePrecioHotel;
    }

}