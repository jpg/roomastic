<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Hoteles\BackendBundle\Entity\User;

/**
 * Hoteles\BackendBundle\Entity\UserHotel
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class UserHotel extends User
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \hotel
     *
     * @ORM\ManyToOne(targetEntity="Hotel")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="hotel", referencedColumnName="id")
     * })
     */
    private $hotel;

    /**
     * @var boolean $administrador
     *
     * @ORM\Column(name="administrador", type="boolean", nullable="true")
     */
    private $administrador;

    /**
     * @var string $imagen
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable="true")
     */
    private $imagen;

    /**
     * @var string $nombrecompleto
     *
     * @ORM\Column(name="nombre_completo", type="string", length=255, nullable="true")
     */
    private $nombreCompleto;

    /**
     * @var string $cargo
     *
     * @ORM\Column(name="cargo", type="string", length=255, nullable="true")
     */
    private $cargo;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get hotel
     *
     * @return \Hoteles\BackendBundle\Entity\Hotel
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set hotel
     *
     * @param \Hoteles\BackendBundle\Entity\Hotel $hotel
     * @return Event
     */
    public function setHotel(\Hoteles\BackendBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Set administrador
     *
     * @param boolean $administrador
     */
    public function setAdministrador($administrador)
    {
        $this->administrador = $administrador;
    }

    /**
     * Get administrador
     *
     * @return boolean 
     */
    public function getAdministrador()
    {
        return $this->administrador;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    public function inrole($role)
    {
        return in_array($role, $this->getRoles());
    }

    public function isAdmin()
    {
        return $this->getAdministrador() === true;
    }

    public function getPublicUsername()
    {
        $resultado = $this->getNombreCompleto() . ", hotel (" . $this->getHotel()->getNombrehotel() . ")";
        return $resultado;
    }

    /**
     * Set nombreCompleto
     *
     * @param string $nombreCompleto
     */
    public function setNombreCompleto($nombreCompleto)
    {
        $this->nombreCompleto = $nombreCompleto;
    }

    /**
     * Get nombreCompleto
     *
     * @return string 
     */
    public function getNombreCompleto()
    {
        return $this->nombreCompleto;
    }

    public function getTypeOf()
    {
        return 'hotel';
    }

    public function getAvatarImage()
    {
        return $this->getImagen() == "" ? parent::getAvatarImage() : "/uploads/user/" . $this->getImagen();
    }


    /**
     * Set cargo
     *
     * @param string $cargo
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;
    }

    /**
     * Get cargo
     *
     * @return string 
     */
    public function getCargo()
    {
        return $this->cargo;
    }
}