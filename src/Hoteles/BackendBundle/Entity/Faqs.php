<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Hoteles\BackendBundle\Entity\Faqs
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Faqs
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $pregunta
     *
     * @ORM\Column(name="pregunta", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $pregunta;

    /**
     * @var text $respuesta
     *
     * @ORM\Column(name="respuesta", type="text")
     * @Assert\NotBlank()
     * 
     */
    private $respuesta;

    /**
     * @var text $posicion
     *
     * @ORM\Column(name="posicion", type="integer")
     */
    private $posicion;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pregunta
     *
     * @param string $pregunta
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;
    }

    /**
     * Get pregunta
     *
     * @return string 
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

    /**
     * Set respuesta
     *
     * @param text $respuesta
     */
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;
    }

    /**
     * Get respuesta
     *
     * @return text 
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * Set posicion
     *
     * @param text $posicion
     */
    public function setPosicion($posicion)
    {
        $this->posicion = $posicion;
    }

    /**
     * Get posicion
     *
     * @return text 
     */
    public function getPosicion()
    {
        return $this->posicion;
    }

}