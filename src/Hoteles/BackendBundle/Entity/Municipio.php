<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hoteles\BackendBundle\Entity\Municipio
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Municipio
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \provincia
     *
     * @ORM\ManyToOne(targetEntity="Provincia")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="provincia", referencedColumnName="id")
     * })
     */
    private $provincia;

    /**
     * @var string $codmunicipio
     *
     * @ORM\Column(name="codmunicipio", type="string", length=255)
     */
    private $codmunicipio;

    /**
     * @var string $dc
     *
     * @ORM\Column(name="dc", type="string", length=255)
     */
    private $dc;

    /**
     * @var string $nombre
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get provincia
     *
     * @return \Hoteles\BackendBundle\Entity\Provincia
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set provincia
     *
     * @param \Hoteles\BackendBundle\Entity\Provincia $provincia
     * @return Event
     */
    public function setProvincia(\Hoteles\BackendBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Set codmunicipio
     *
     * @param string $codmunicipio
     */
    public function setCodmunicipio($codmunicipio)
    {
        $this->codmunicipio = $codmunicipio;
    }

    /**
     * Get codmunicipio
     *
     * @return string 
     */
    public function getCodmunicipio()
    {
        return $this->codmunicipio;
    }

    /**
     * Set dc
     *
     * @param string $dc
     */
    public function setDc($dc)
    {
        $this->dc = $dc;
    }

    /**
     * Get dc
     *
     * @return string 
     */
    public function getDc()
    {
        return $this->dc;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    public function __toString()
    {
        return $this->getNombre();
    }

}