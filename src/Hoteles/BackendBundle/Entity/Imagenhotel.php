<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hoteles\BackendBundle\Entity\Imagenhotel
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Imagenhotel
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $imagen
     *
     * @ORM\Column(name="imagen", type="string", length=255)
     */
    private $imagen;

    /**
     * @var \hotel
     *
     * @ORM\ManyToOne(targetEntity="Hotel")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="hotel", referencedColumnName="id", nullable="true")
     * })
     */
    private $hotel;

    /**
     * @var integer $orden
     *
     * @ORM\Column(name="orden", type="integer", nullable="true")
     */
    private $orden;

    /**
     * @var string $alt
     *
     * @ORM\Column(name="alt", type="string", length=255, nullable="true")
     */
    private $alt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Get hotel
     *
     * @return \Hoteles\BackendBundle\Entity\Hotel
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set hotel
     *
     * @param \Hoteles\BackendBundle\Entity\Hotel $hotel
     * @return Event
     */
    public function setHotel(\Hoteles\BackendBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set alt
     *
     * @param string $alt
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    /**
     * Get alt
     *
     * @return string 
     */
    public function getAlt()
    {
        return $this->alt;
    }

}