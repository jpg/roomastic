<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hoteles\BackendBundle\Entity\UserAdministrador
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class UserAdministrador extends \Hoteles\BackendBundle\Entity\User
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $imagen
     *
     * @ORM\Column(name="imagen", type="string", length=255)
     */
    private $imagen;

    /**
     * Set imagen
     *
     * @param string $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    public function inrole($role)
    {
        return in_array($role, $this->getRoles());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getPublicUsername()
    {
        $resultado = $this->getEmail() . "(ADMIN)";
        return $resultado;
    }

    public function getTypeOf()
    {
        return 'admin';
    }

    public function getNombreCompleto()
    {
        return $this->getUsernameCanonical();
    }

    public function getAvatarImage()
    {
        return $this->getImagen() == "" ? parent::getAvatarImage() : "/uploads/user/" . $this->getImagen();
    }

}