<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hoteles\BackendBundle\Entity\Incidencia
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Incidencia
{

    const ATENDIDO = 1;
    const NO_ATENDIDO = 0;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \usuariofrom
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="usuariofrom", referencedColumnName="id")
     * })
     */
    protected $usuariofrom;

    /**
     * @var \usuariofrom
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="usuarioto", referencedColumnName="id")
     * })
     */
    private $usuarioto;

    /**
     * @var datetime $fecha
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var string $asunto
     *
     * @ORM\Column(name="asunto", type="string", length=255)
     */
    private $asunto;

    /**
     * @var text $mensaje
     *
     * @ORM\Column(name="mensaje", type="text")
     */
    private $mensaje;

    /**
     * @var boolean $atendido
     *
     * @ORM\Column(name="atendido", type="boolean", nullable=true)
     */
    private $atendido;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get usuariofrom
     *
     * @return \Hoteles\BackendBundle\Entity\User
     */
    public function getUsuariofrom()
    {
        return $this->usuariofrom;
    }

    /**
     * Set usuariofrom
     *
     * @param \Hoteles\BackendBundle\Entity\UserPromoter $usuariofrom
     * @return Event
     */
    public function setUsuariofrom(\Hoteles\BackendBundle\Entity\User $usuariofrom = null)
    {
        $this->usuariofrom = $usuariofrom;

        return $this;
    }

    /**
     * Get usuarioto
     *
     * @return \Hoteles\BackendBundle\Entity\User
     */
    public function getUsuarioto()
    {
        return $this->usuarioto;
    }

    /**
     * Set usuarioto
     *
     * @param \Hoteles\BackendBundle\Entity\UserPromoter $usuarioto
     * @return Event
     */
    public function setUsuarioto(\Hoteles\BackendBundle\Entity\User $usuarioto = null)
    {
        $this->usuarioto = $usuarioto;

        return $this;
    }

    /**
     * Set fecha
     *
     * @param datetime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * Get fecha
     *
     * @return datetime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set asunto
     *
     * @param string $asunto
     */
    public function setAsunto($asunto)
    {
        $this->asunto = $asunto;
    }

    /**
     * Get asunto
     *
     * @return string 
     */
    public function getAsunto()
    {
        return $this->asunto;
    }

    /**
     * Set mensaje
     *
     * @param text $mensaje
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;
    }

    /**
     * Get mensaje
     *
     * @return text 
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set atendido
     *
     * @param boolean $atendido
     */
    public function setAtendido($atendido)
    {
        $this->atendido = $atendido;
    }

    /**
     * Get atendido
     *
     * @return boolean 
     */
    public function getAtendido()
    {
        return $this->atendido;
    }

    public function markAsAtendido()
    {
        $this->setAtendido(self::ATENDIDO);
    }

    public function markAsNoAtendido()
    {
        $this->setAtendido(self::NO_ATENDIDO);
    }

    public function isAtendido()
    {
        return $this->getAtendido() === self::ATENDIDO;
    }

    public function isNoAtendido()
    {
        return $this->getAtendido() === self::NO_ATENDIDO;
    }

}