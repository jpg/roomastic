<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Hoteles\BackendBundle\Entity\Hotel
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Seo
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $seccion
     *
     * @ORM\Column(name="seccion", type="string", length=255)
     */
    private $seccion;

    /**
     * @var string $seotitulo
     *
     * @ORM\Column(name="seotitulo", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $seotitulo;

    /**
     * @var string $seodescripcion
     *
     * @ORM\Column(name="seodescripcion", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $seodescripcion;

    /**
     * @var string $seokeywords
     *
     * @ORM\Column(name="seokeywords", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $seokeywords;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seccion
     *
     * @param string $seccion
     */
    public function setSeccion($seccion)
    {
        $this->seccion = $seccion;
    }

    /**
     * Get seccion
     *
     * @return string 
     */
    public function getSeccion()
    {
        return $this->seccion;
    }

    /**
     * Set seotitulo
     *
     * @param string $seotitulo
     */
    public function setSeotitulo($seotitulo)
    {
        $this->seotitulo = $seotitulo;
    }

    /**
     * Get seotitulo
     *
     * @return string 
     */
    public function getSeotitulo()
    {
        return $this->seotitulo;
    }

    /**
     * Set seodescripcion
     *
     * @param string $seodescripcion
     */
    public function setSeodescripcion($seodescripcion)
    {
        $this->seodescripcion = $seodescripcion;
    }

    /**
     * Get seodescripcion
     *
     * @return string 
     */
    public function getSeodescripcion()
    {
        return $this->seodescripcion;
    }

    /**
     * Set seokeywords
     *
     * @param string $seokeywords
     */
    public function setSeokeywords($seokeywords)
    {
        $this->seokeywords = $seokeywords;
    }

    /**
     * Get seokeywords
     *
     * @return string 
     */
    public function getSeokeywords()
    {
        return $this->seokeywords;
    }

    public function __toString()
    {
        return $this->getNombreempresa();
    }

}