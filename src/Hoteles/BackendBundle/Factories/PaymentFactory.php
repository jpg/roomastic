<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Factories;

use Hoteles\BackendBundle\Entity\Payment;

class PaymentFactory
{

    protected $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function createPayment($amount, $order, $hash_order, $descripcion, $titular)
    {
        $payment_creado = clone $this->payment;
        $payment_creado->setImporte($amount);
        $payment_creado->setPedido($order);
        $payment_creado->setDescripcion($descripcion);
        $payment_creado->setTitular($titular);
        $payment_creado->generateUrls($hash_order);
        $payment_creado->config_entorno();
        return $payment_creado;
    }

}
