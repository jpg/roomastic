$(document).ready(function() {

    $('.unsuscribe-newsletter').on('click', function(evt) {
        var resultado;
        resultado = confirm('¿Seguro que quiere dar de baja a este usuario?');
        if (true === resultado) {
            $.post(evt.target.href).done(function(data) {
                $(evt.target).closest('tr').find('td.status i').removeClass('fa-check').addClass('fa-times');
                $(evt.target).closest('td').html('');
            }).fail(function(data) {
                alert('error al borrar');
            });
        }
        evt.preventDefault();

    });
    $('.suscribe-newsletter').on('click', function(evt) {
        var resultado;
        resultado = confirm('¿Seguro que quiere dar de alta a este usuario?');
        if (true === resultado) {
            $.post(evt.target.href).done(function(data) {
                $(evt.target).closest('tr').find('td.status i').removeClass('fa-times').addClass('fa-check');
                $(evt.target).closest('td').html('');
            }).fail(function(data) {
                alert('error al dar de alta');
            });
        }
        evt.preventDefault();

    });
    $('#form-buscar-newsletter').on('submit', function(evt) {
        var email;
        email = $(this).find('input').val();
        window.location.href = Routing.generate('admin_newsletter', {email: email});
        evt.preventDefault();
    });
});