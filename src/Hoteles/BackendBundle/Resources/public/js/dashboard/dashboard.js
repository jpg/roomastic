/**
 * Esta función la utilizo para cargar los datos que se ven en la cabecera
 * Recibe 2 parámetros:
 * - Un objeto jquery para seleccionar el elemento donde se va a actuar
 * - La cuenta a la que queremos llegar
 * 
 * */
function countUp($jquery_object, count) {
    var div_by = 100,
            speed = Math.round(count / div_by),
            $display = $jquery_object,
            run_count = 1,
            int_speed = 24;

    var int = setInterval(function () {
        if (run_count < div_by) {
            $display.text(speed * run_count);
            run_count++;
        } else if (parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}

function actualizar_header() {
    var header_vars = ROOMASTIC.dashboard.header;
    for (var propName in header_vars) {
        countUp($('#' + propName), header_vars[propName]);
    }
}
function actualizar_ventasdiarias_periodo() {
    function actualizarSparkLine(datos) {
        $(".sparkline").each(function () {
            var $data = $(this).data();

            $data.valueSpots = {'0:': $data.spotColor};

            $(this).sparkline(datos || "html", $data,
                    {
                        tooltipFormat: '<span style="display:block; padding:0px 10px 12px 0px;">' +
                                '<span style="color: {{color}}">&#9679;</span> {{offset:names}} ({{percent.1}}%)</span>'
                    });
        });
    }
    $('.facturacion-ultimo-mes').on('click', function (evt) {
        var datos = $('.grafico-facturacion').data().dataMes;
        actualizarSparkLine(datos);
        $('#sum-ofertas-periodo').text($('.grafico-facturacion').data().sumOfertasMes + '€');
        $('.facturacion-ultimo-mes').removeClass('active');
        $('.facturacion-ultima-semana').addClass('active');
        $('#titulo-periodo').text($('#titulo-periodo').data().tituloMes);
        evt.preventDefault();
    });
    $('.facturacion-ultima-semana').on('click', function (evt) {
        var datos = $('.grafico-facturacion').data().dataSemana;
        actualizarSparkLine(datos);
        $('#sum-ofertas-periodo').text($('.grafico-facturacion').data().sumOfertasSemana + '€');
        $('.facturacion-ultimo-mes').addClass('active');
        $('.facturacion-ultima-semana').removeClass('active');
        $('#titulo-periodo').text($('#titulo-periodo').data().tituloSemana);
        evt.preventDefault();
    });
    $('.facturacion-ultimo-mes').trigger('click');
}
function actualizar_ventasmensuales_anio() {
    var $data = $('#barchart').data().data, max;
    $("#barchart").sparkline($data, {
        type: 'bar',
        height: '200',
        barWidth: 8,
        barSpacing: 5,
        barColor: '#fff'
//        tooltipFormat: '<span style="display:block; padding:0px 10px 12px 0px;">' +
//            '<span style="color: {{color}}">&#9679;</span> {{offset:names}} ({{percent.1}}%)</span>'
    });
    if ($(".custom-bar-chart")) {
        max = $('#custom-bar-chart').data().maxValue;

        $(".bar").each(function () {
            var i = $(this).find(".value").html(), altura;
            var altura = 100 * (i / max);
            $(this).find(".value").html("");
            $(this).find(".value").animate({
                height: altura + '%'
            }, 2000)
        })
    }
}
$(document).ready(function () {
    actualizar_header();
    actualizar_ventasdiarias_periodo();
    actualizar_ventasmensuales_anio();
});