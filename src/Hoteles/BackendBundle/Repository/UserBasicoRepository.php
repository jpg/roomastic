<?php

namespace Hoteles\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;

class UserBasicoRepository extends EntityRepository
{

    public function findUserByEmailFacebookEmailGoogleTwitter($user_id, $email = null)
    {
        $em = $this->getEntityManager();
        $dql = "SELECT u FROM HotelesBackendBundle:User u where u.email = :email or u.username = :username";
        $consulta_fosuser = $em->createQuery($dql);
        if (!is_null($email)) {
            $consulta_fosuser->setParameter("email", $email);
        } else {
            $consulta_fosuser->setParameter("email", $user_id);
        }
        $consulta_fosuser->setParameter("username", $user_id);
        try {
            $fos_user = $consulta_fosuser->getSingleResult();
            return $fos_user;
        } catch (NoResultException $e) {
            //Hago la consulta de esta manera por si hay una colision entre los id de los usuarios
            // de fb, twitter y google no tendría problemas
            $dql = "SELECT u from HotelesBackendBundle:UserBasico u where " .
                    " (u.facebookID = :user_id and u.origin = :fb_origin) or " .
                    " (u.twitterID = :user_id and u.origin = :tw_origin) or " .
                    " (u.googleID = :user_id and u.origin = :goog_origin)";
            $consulta_user_basico = $em->createQuery($dql);
            $consulta_user_basico->setParameter('fb_origin', "facebook");
            $consulta_user_basico->setParameter('tw_origin', "twitter");
            $consulta_user_basico->setParameter('goog_origin', "google");
            $consulta_user_basico->setParameter("user_id", $user_id);
            try {
                $user_basico = $consulta_user_basico->getSingleResult();
                return $user_basico;
            } catch (NoResultException $e) {
                return null;
            }
        }
    }

    public function findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
    {
        if (count($criteria) === 1 && isset($criteria['email'])) {
            return $this
                            ->getEntityManager()
                            ->createQuery("SELECT u FROM HotelesBackendBundle:User u WHERE u.email = :email")
                            ->setParameters($criteria)
                            ->getResult();
        } else {
            return parent::findBy($criteria, $orderBy, $limit, $offset);
        }
    }

}
