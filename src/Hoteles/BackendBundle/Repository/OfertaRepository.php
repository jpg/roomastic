<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Hoteles\BackendBundle\Entity\Oferta;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query;

class OfertaRepository extends EntityRepository
{

    public function checkValidOffer($idGrupoOfertas)
    {

        $dql = "SELECT count(o) FROM HotelesBackendBundle:Oferta o WHERE " .
                " o.idoferta = :id_grupo_ofertas and " .
                " o.status NOT IN (:estados_no_modificables)";
        $em = $this->getEntityManager();
        $consulta = $em->createQuery($dql);
        $consulta->setParameter("id_grupo_ofertas", $idGrupoOfertas);
        $consulta->setParameter("estados_no_modificables", $array_de_estados);
        $resultado = $consulta->getSingleScalarResult();
        if ($resultado > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Solamente una oferta de cada grupo tiene este estado
     */
    public function getPendingPaymentOffers()
    {
        $dql = "SELECT o  FROM HotelesBackendBundle:Oferta o WHERE " .
                " o.status = :status_oferta_aceptada";
        $em = $this->getEntityManager();
        $consulta = $em->createQuery($dql);
        $consulta->setParameter("status_oferta_aceptada", Oferta::OFERTA_ACEPTADA);
        $resultado = $consulta->getResult();
        return $resultado;
    }

    /*
     * Nos devuelve los grupos de ofertas NO atendidas y los grupos de ofertas
     * que tienen un estado de CONTRAOFERTA realizada
     */

    public function getPendingOfferGroup($hours_ago)
    {
        $dql = "SELECT DISTINCT o.idoferta  FROM HotelesBackendBundle:Oferta o WHERE " .
                " o.group_status IN (:group_status_array) and " .
                " o.fecha < :fecha_caducada";
        $em = $this->getEntityManager();
        $consulta = $em->createQuery($dql);
        $consulta->setParameter("group_status_array", array(
            Oferta::GRUPO_OFERTA_ESTADO_INICIAL,
            Oferta::GRUPO_OFERTA_CONTRAOFERTA_REALIZADA));
//$consulta->setParameter("group_status_contraofertado", );
        $fecha_limite = new \DateTime();
        $fecha_limite->modify('- ' . $hours_ago . ' hours');
        $consulta->setParameter("fecha_caducada", $fecha_limite);
        $resultado = $consulta->getScalarResult();
        return $resultado;
    }

    /*
     * Me devuelve los grupos de ofertas con una contraoferta validada y no notificada
     * 
     */

    public function getPendingCounterOfferGroup()
    {
        $dql = " SELECT DISTINCT o.idoferta  FROM HotelesBackendBundle:Oferta o WHERE " .
                " o.group_status = :group_status_contraoferta and " .
//Esta línea no la podemos quitar ya que solamente se marcan como notificadas las contraofertas
                " o.status = :status_contraoferta_realizada and " .
                " o.notificacion_contraoferta = :valor_notificacion";
        $em = $this->getEntityManager();
        $consulta = $em->createQuery($dql);
        $consulta->setParameter("group_status_contraoferta", Oferta::GRUPO_OFERTA_CONTRAOFERTA_VALIDA);
        $consulta->setParameter("status_contraoferta_realizada", Oferta::OFERTA_CONTRAOFERTA_REALIZADA);
        $consulta->setParameter("valor_notificacion", FALSE);
//$sql = $consulta->getSQL();
        $resultado = $consulta->getScalarResult();
        return $resultado;
    }

    /**
     * 
     * 
     */
    public function getExpiredCounterOfferGroup($hours_ago)
    {
        $dql = "SELECT DISTINCT o.idoferta  FROM HotelesBackendBundle:Oferta o WHERE " .
                " o.group_status = :group_contraoferta_valida and " .
                " o.notificacion_contraoferta = :valor_notificacion and " .
                " o.fechacontraoferta < :fecha_limite_contraoferta";
        $em = $this->getEntityManager();
        $consulta = $em->createQuery($dql);
        $consulta->setParameter("group_contraoferta_valida", Oferta::GRUPO_OFERTA_CONTRAOFERTA_VALIDA);
        $consulta->setParameter("valor_notificacion", TRUE);
        $fecha_limite = new \DateTime();
        $fecha_limite->modify('- ' . $hours_ago . ' hours');
        $consulta->setParameter("fecha_limite_contraoferta", $fecha_limite);
        $resultado = $consulta->getScalarResult();
        return $resultado;
    }

    public function getOffersFromGroup($id_oferta)
    {
        $dql = "SELECT o FROM HotelesBackendBundle:Oferta o WHERE " .
                " o.idoferta=:idoferta";
        $em = $this->getEntityManager();
        $consulta = $em->createQuery($dql);
        $consulta->setParameter("idoferta", $id_oferta);
        $resultado = $consulta->getResult();
        return $resultado;
    }

    /**
     * Me devuelve todas las contraofertas que tenga un grupo de ofertas
     * @param type $id_grupo_contraoferta
     * @return type     /
     */
    public function getCounterOffersFromGroup($id_grupo_contraoferta)
    {
        $dql = "SELECT o FROM HotelesBackendBundle:Oferta o WHERE " .
                " o.idoferta=:id_grupo_contraoferta ";
        $em = $this->getEntityManager();
        $consulta = $em->createQuery($dql);
        $consulta->setParameter("id_grupo_contraoferta", $id_grupo_contraoferta);
        $resultado = $consulta->getResult();
        return $resultado;
    }

    public function getFirstOfferFromGroup($id_oferta)
    {
        $dql = "SELECT o FROM HotelesBackendBundle:Oferta o WHERE " .
                " o.idoferta=:id_oferta";
        $em = $this->getEntityManager();
        $consulta = $em->createQuery($dql);
        $consulta->setParameter("id_oferta", $id_oferta);
        $consulta->setMaxResults(1);
        $resultado = $consulta->getSingleResult();
        return $resultado;
    }

    public function getOfertasFiltered($filtro = 'todos', $is_admin = false, $id_hotel = null, array $hoteles_empresa = null)
    {

        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('o')
                ->from('HotelesBackendBundle:Oferta', 'o');
//$dql = "SELECT o FROM HotelesBackendBundle:Oferta o ";
        if (false === is_null($id_hotel)) {
            $qb->andWhere("o.hotel=:id_hotel");
            $qb->setParameter('id_hotel', $id_hotel);
        }
        if (false === is_null($hoteles_empresa)) {
            $qb->andWhere("o.hotel in (:hoteles_empresa)");
            $qb->setParameter("hoteles_empresa", $hoteles_empresa);
        }
        switch ($filtro) {
            case "contraofertas":
                $estados_validos = array(Oferta::OFERTA_CONTRAOFERTA_REALIZADA);
                break;
            case "pendientes":
                $estados_validos = array(Oferta::OFERTA_VALIDA);
                break;
            case "aceptadas":
                $estados_validos = array(Oferta::OFERTA_ACEPTADA);
                break;
            case "pagadas":
                $estados_validos = array(Oferta::OFERTA_PAGADA, Oferta::OFERTA_CONTRAOFERTA_PAGADA);
                break;
            case "no-pagadas":
                $estados_validos = array(Oferta::OFERTA_NO_PAGADA);
                break;
            case "contraofertas-rechazadas":
                $estados_validos = array(Oferta::OFERTA_CONTRAOFERTA_RECHAZADA);
                break;
            case "inferior-minimo":
                $estados_validos = array(Oferta::OFERTA_INFERIOR_MINIMO);
                break;
            case "contraoferta-caducada":
                $estados_validos = array(Oferta::OFERTA_CONTRAOFERTA_CADUCADA);
                break;
            case "superior-pvp":
                $estados_validos = array(Oferta::OFERTA_SUPERIOR_PVP);
                break;
            case "rechazadas":
                $estados_validos = array(Oferta::OFERTA_RECHAZADA);
                break;
            case "todos":
            default:
                if ($is_admin) {
                    $estados_validos = array(
                        Oferta::OFERTA_SUPERIOR_PVP,
                        Oferta::OFERTA_INFERIOR_MINIMO,
                        Oferta::OFERTA_CONTRAOFERTA_REALIZADA,
                        Oferta::OFERTA_CONTRAOFERTA_RECHAZADA,
                        Oferta::OFERTA_CONTRAOFERTA_CADUCADA,
                        Oferta::OFERTA_CONTRAOFERTA_PAGADA,
                        Oferta::OFERTA_CONTRAOFERTA_ANULADA,
                        Oferta::OFERTA_PAGADA,
                        Oferta::OFERTA_NO_PAGADA,
                        Oferta::OFERTA_RECHAZADA,
                        Oferta::OFERTA_CADUCADA,
                        Oferta::OFERTA_ACEPTADA,
                        Oferta::OFERTA_HOTEL_ACEPTADA,
                        Oferta::OFERTA_VALIDA,
                    );
                } else {
                    $estados_validos = array(
                        Oferta::OFERTA_CONTRAOFERTA_REALIZADA,
                        Oferta::OFERTA_VALIDA,
                        Oferta::OFERTA_ACEPTADA,
                        Oferta::OFERTA_NO_PAGADA,
                        Oferta::OFERTA_PAGADA,
                        Oferta::OFERTA_CONTRAOFERTA_RECHAZADA);
                    break;
                }
        }
        $qb->andWhere($qb->expr()->in('o.status', $estados_validos));
        $qb->orderBy('o.fecha', 'DESC');
        $resultado = $qb->getQuery()->execute();
        return $resultado;
    }

    public function getOfferCountFromLastNDays($numero_de_dias, $type_of_user, $tipo_de_oferta = null, $user_id = null)
    {
        $em = $this->getEntityManager();
        $fecha_limite = new \DateTime();
        $fecha_limite->modify('-' . $numero_de_dias . ' day');
        if ($tipo_de_oferta === 'recibidas') {
            $statuses = array(
                Oferta::OFERTA_ACEPTADA,
                Oferta::OFERTA_NO_PAGADA,
                Oferta::OFERTA_PAGADA,
                Oferta::OFERTA_VALIDA,
                Oferta::OFERTA_CONTRAOFERTA_ANULADA,
                Oferta::OFERTA_CONTRAOFERTA_CADUCADA,
                Oferta::OFERTA_CONTRAOFERTA_PAGADA,
                Oferta::OFERTA_CONTRAOFERTA_REALIZADA,
                Oferta::OFERTA_CONTRAOFERTA_RECHAZADA,
                Oferta::OFERTA_RECHAZADA);
        } elseif ($tipo_de_oferta === 'aceptadas') {
            $statuses = array(
                Oferta::OFERTA_ACEPTADA,
                Oferta::OFERTA_NO_PAGADA,
                Oferta::OFERTA_PAGADA
            );
        } elseif ($tipo_de_oferta === 'pagadas') {
            $statuses = array(
                Oferta::OFERTA_CONTRAOFERTA_PAGADA,
                Oferta::OFERTA_PAGADA
            );
        } elseif ($tipo_de_oferta === 'debajo-minimo') {
            $statuses = array(
                Oferta::OFERTA_INFERIOR_MINIMO);
        }
        if ($type_of_user === 'admin') {
            $dql = "SELECT COUNT(o) FROM HotelesBackendBundle:Oferta o " .
                    " WHERE o.fecha > :fecha_limite and o.status in (:statuses)";

            $consulta = $em->createQuery($dql);
            $consulta->setParameter("fecha_limite", $fecha_limite);
            $consulta->setParameter("statuses", $statuses);
            $resultado = $consulta->getSingleScalarResult();
        } elseif ($type_of_user === 'empresa') {
            $dql = "SELECT COUNT(ue) FROM HotelesBackendBundle:Oferta o " .
                    " LEFT JOIN o.hotel h" .
                    " LEFT JOIN h.empresa e " .
                    " LEFT JOIN e.user_empresa ue " .
                    " WHERE ue.id = :user_empresa_id " .
                    " AND o.fecha > :fecha_limite " .
                    " AND o.status IN (:statuses) " .
                    " GROUP BY ue.id";
            $consulta = $em->createQuery($dql);
            $consulta->setParameter("fecha_limite", $fecha_limite);
            $consulta->setParameter("user_empresa_id", $user_id);
            $consulta->setParameter("statuses", $statuses);
            try {
                $resultado = $consulta->getSingleScalarResult();
            } catch (\Doctrine\ORM\NoResultException $e) {
                $resultado = 0;
            }
        } elseif ($type_of_user === 'hotel') {
            $dql = "SELECT COUNT (uh) FROM HotelesBackendBundle:Oferta o " .
                    " LEFT JOIN o.hotel h" .
                    " LEFT JOIN h.user_hotel uh " .
                    " WHERE uh.id = :user_hotel_id" .
                    " AND o.fecha > :fecha_limite " .
                    " AND o.status IN (:statuses) " .
                    " GROUP BY h.id";
            $consulta = $em->createQuery($dql);
            $consulta->setParameter("fecha_limite", $fecha_limite);
            $consulta->setParameter("user_hotel_id", $user_id);
            $consulta->setParameter("statuses", $statuses);

            try {
                $resultado = $consulta->getSingleScalarResult();
            } catch (\Doctrine\ORM\NoResultException $e) {
                $resultado = 0;
            }
        }
        return $resultado;
    }

    public function getOfferDailySumLastNDays($numero_de_dias, $type_of_user, $user_id)
    {
        /*
         * - Busco todas las ofertas y contraofertas pagadas de un hotel/empresa/todos
         * - Calculo la suma de acuerdo al periodo (día, mes) durante los ultimos n dias
         */
        $em = $this->getEntityManager();
        $statuses = implode(',', array(Oferta::OFERTA_CONTRAOFERTA_PAGADA, Oferta::OFERTA_PAGADA));
        $fecha_limite = new \DateTime();
        $fecha_limite->modify('- ' . $numero_de_dias . ' day');
        if ($type_of_user === 'admin') {
            $native_sql = "SELECT /*! SQL_CACHE */ IFNULL( SUM(o.neto_hotel_pagado), 1 ) suma_periodo, DAY(o.fecha) as periodo " .
                    " FROM Oferta o  WHERE o.fecha > :fecha_limite and o.status IN (" . $statuses . ") GROUP BY periodo";
            $stmt = $em->getConnection()->prepare($native_sql);
            $stmt->bindValue('fecha_limite', $fecha_limite->format('Y-m-d H:i:s'));
            $stmt->execute();
            $resultado_consulta = $stmt->fetchAll();
        } elseif ($type_of_user === 'empresa') {
            $native_sql = "SELECT /*! SQL_CACHE */ IFNULL( SUM(o.neto_hotel_pagado), 0 ) suma_periodo, DAY(o.fecha) as periodo " .
                    " FROM Oferta o " .
                    " LEFT JOIN Hotel h on o.hotel = h.id " .
                    " LEFT JOIN UserEmpresa ue on ue.empresa = h.empresa " .
                    " WHERE o.fecha > :fecha_limite " .
                    " and o.status IN (" . $statuses . ") " .
                    " and ue.id = :user_empresa_id " .
                    " GROUP BY periodo";
            $stmt = $em->getConnection()->prepare($native_sql);
            $stmt->bindValue('fecha_limite', $fecha_limite->format('Y-m-d H:i:s'));
            $stmt->bindValue('user_empresa_id', $user_id);
            $stmt->execute();
            $resultado_consulta = $stmt->fetchAll();
        } elseif ($type_of_user === 'hotel') {
//TODO: Refactorizar todo para que use una unica JOIN
            $native_sql = "SELECT /*! SQL_CACHE */ IFNULL( SUM(o.neto_hotel_pagado), 0 ) suma_periodo, DAY(o.fecha) as periodo " .
                    " FROM Oferta o " .
                    " LEFT JOIN UserHotel uh on uh.hotel = o.hotel" .
                    " WHERE o.fecha > :fecha_limite " .
                    " and o.status IN (" . $statuses . ") " .
                    " and uh.id = :user_hotel_id " .
                    " GROUP BY periodo";
            $stmt = $em->getConnection()->prepare($native_sql);
            $stmt->bindValue('fecha_limite', $fecha_limite->format('Y-m-d H:i:s'));
            $stmt->bindValue('user_hotel_id', $user_id);
            $stmt->execute();
            $resultado_consulta = $stmt->fetchAll();
        }

        $resultado = array_fill(0, $numero_de_dias, array('dia' => 0, 'suma' => 0));
        for ($i = 0; $i <= $numero_de_dias - 1; $i++) {
            $resultado[$i]['dia'] = $fecha_limite->modify('+1 day')->format('d');
        }
        foreach ($resultado_consulta as $fila) {
            $this->loadRowIntoResult($fila, $resultado);
        }
        return $resultado;
    }

    private function loadRowIntoResult($fila, &$resultado, $periodo = 'dia')
    {
        $dia_resultado_mysql_en_entero = intval($fila['periodo']);
        foreach ($resultado as $k => $r) {
            $dia_resultado_en_entero = intval($r[$periodo]);
            if ($dia_resultado_en_entero === $dia_resultado_mysql_en_entero) {
                $resultado[$k]['suma'] = $fila['suma_periodo'];
                return;
            }
        }
    }

    public function getOfferMonthlySales($type_of_user, $user_id)
    {
        $em = $this->getEntityManager();
        $statuses = implode(',', array(Oferta::OFERTA_CONTRAOFERTA_PAGADA, Oferta::OFERTA_PAGADA));

        $fecha_limite = new \DateTime();
        $fecha_limite->modify('- 1 year'); // . $numero_de_dias . ' day');
        if ($type_of_user === 'admin') {
            $native_sql = "SELECT /*! SQL_CACHE */ IFNULL( SUM(o.neto_hotel_pagado), 0 ) suma_periodo, MONTH(o.fecha) as periodo " .
                    " FROM Oferta o  WHERE o.fecha > :fecha_limite and o.status IN (" . $statuses . ") GROUP BY periodo";
            $stmt = $em->getConnection()->prepare($native_sql);
            $stmt->bindValue('fecha_limite', $fecha_limite->format('Y-m-d H:i:s'));
            $stmt->execute();
            $resultado_consulta = $stmt->fetchAll();
        } elseif ($type_of_user === 'empresa') {
            $native_sql = "SELECT /*! SQL_CACHE */ IFNULL( SUM(o.neto_hotel_pagado), 0 ) suma_periodo, MONTH(o.fecha) as periodo " .
                    " FROM Oferta o " .
                    " LEFT JOIN Hotel h on o.hotel = h.id " .
                    " LEFT JOIN UserEmpresa ue on ue.empresa = h.empresa " .
                    " WHERE o.fecha > :fecha_limite " .
                    " and o.status IN (" . $statuses . ") " .
                    " and ue.id = :user_empresa_id " .
                    " GROUP BY periodo";
            $stmt = $em->getConnection()->prepare($native_sql);
            $stmt->bindValue('fecha_limite', $fecha_limite->format('Y-m-d H:i:s'));
            $stmt->bindValue('user_empresa_id', $user_id);
            $stmt->execute();
            $resultado_consulta = $stmt->fetchAll();
        } elseif ($type_of_user === 'hotel') {
//TODO: Refactorizar todo para que use una unica JOIN
            $native_sql = "SELECT /*! SQL_CACHE */ IFNULL( SUM(o.neto_hotel_pagado), 0 ) suma_periodo, MONTH(o.fecha) as periodo " .
                    " FROM Oferta o " .
                    " LEFT JOIN UserHotel uh on uh.hotel = o.hotel" .
                    " WHERE o.fecha > :fecha_limite " .
                    " and o.status IN (" . $statuses . ") " .
                    " and uh.id = :user_hotel_id " .
                    " GROUP BY periodo";
            $stmt = $em->getConnection()->prepare($native_sql);
            $stmt->bindValue('fecha_limite', $fecha_limite->format('Y-m-d H:i:s'));
            $stmt->bindValue('user_hotel_id', $user_id);
            $stmt->execute();
            $resultado_consulta = $stmt->fetchAll();
        }
        $numero_de_meses = 12;
        $resultado = array_fill(0, $numero_de_meses, array('mes' => 0, 'suma' => 0));
        for ($i = 0; $i <= $numero_de_meses - 1; $i++) {
            $resultado[$i]['mes'] = $fecha_limite->modify('+1 month')->format('m');
        }
        foreach ($resultado_consulta as $fila) {
            $this->loadRowIntoResult($fila, $resultado, 'mes');
        }
        return $resultado;
    }

    public function getNumberOfOffersByMonth($type_of_user, $user_id)
    {
        $em = $this->getEntityManager();
        $statuses = implode(',', array(
            Oferta::OFERTA_ACEPTADA,
            Oferta::OFERTA_NO_PAGADA,
            Oferta::OFERTA_PAGADA,
            Oferta::OFERTA_VALIDA,
            Oferta::OFERTA_CONTRAOFERTA_ANULADA,
            Oferta::OFERTA_CONTRAOFERTA_CADUCADA,
            Oferta::OFERTA_CONTRAOFERTA_PAGADA,
            Oferta::OFERTA_CONTRAOFERTA_REALIZADA,
            Oferta::OFERTA_CONTRAOFERTA_RECHAZADA,
            Oferta::OFERTA_RECHAZADA));

        $fecha_limite = new \DateTime();
        $fecha_limite->modify('- 1 year');

        if ($type_of_user === 'admin') {
            $native_sql = "SELECT /*! SQL_CACHE */  COUNT(o.id) as suma_periodo, MONTH(o.fecha) as periodo " .
                    " FROM Oferta o  WHERE o.fecha > :fecha_limite and o.status IN (" . $statuses . ") GROUP BY periodo";
            $stmt = $em->getConnection()->prepare($native_sql);
            $stmt->bindValue('fecha_limite', $fecha_limite->format('Y-m-d H:i:s'));
            $stmt->execute();
            $resultado_consulta = $stmt->fetchAll();
        } elseif ($type_of_user === 'empresa') {
            $native_sql = "SELECT /*! SQL_CACHE */ COUNT(o.id) as suma_periodo, MONTH(o.fecha) as periodo " .
                    " FROM Oferta o " .
                    " LEFT JOIN Hotel h on o.hotel = h.id " .
                    " LEFT JOIN UserEmpresa ue on ue.empresa = h.empresa " .
                    " WHERE o.fecha > :fecha_limite " .
                    " and o.status IN (" . $statuses . ") " .
                    " and ue.id = :user_empresa_id " .
                    " GROUP BY periodo";
            $stmt = $em->getConnection()->prepare($native_sql);
            $stmt->bindValue('fecha_limite', $fecha_limite->format('Y-m-d H:i:s'));
            $stmt->bindValue('user_empresa_id', $user_id);
            $stmt->execute();
            $resultado_consulta = $stmt->fetchAll();
        } elseif ($type_of_user === 'hotel') {
//TODO: Refactorizar todo para que use una unica JOIN
            $native_sql = "SELECT /*! SQL_CACHE */ COUNT(o.id) as suma_periodo, MONTH(o.fecha) as periodo " .
                    " FROM Oferta o " .
                    " LEFT JOIN UserHotel uh on uh.hotel = o.hotel" .
                    " WHERE o.fecha > :fecha_limite " .
                    " and o.status IN (" . $statuses . ") " .
                    " and uh.id = :user_hotel_id " .
                    " GROUP BY periodo";
            $stmt = $em->getConnection()->prepare($native_sql);
            $stmt->bindValue('fecha_limite', $fecha_limite->format('Y-m-d H:i:s'));
            $stmt->bindValue('user_hotel_id', $user_id);
            $stmt->execute();
            $resultado_consulta = $stmt->fetchAll();
        }
        $numero_de_meses = 12;
        $resultado = array_fill(0, $numero_de_meses, array('mes' => 0, 'suma' => 0));
        for ($i = 0; $i <= $numero_de_meses - 1; $i++) {
            $resultado[$i]['mes'] = $fecha_limite->modify('+1 month')->format('m');
        }
        foreach ($resultado_consulta as $fila) {
            $this->loadRowIntoResult($fila, $resultado, 'mes');
        }
        return $resultado;
    }

    public function getHotelRankings($type_of_user, $user_id, $rank_size = 50)
    {
        $em = $this->getEntityManager();
        $statuses = array(
            Oferta::OFERTA_ACEPTADA,
            Oferta::OFERTA_NO_PAGADA,
            Oferta::OFERTA_PAGADA,
            Oferta::OFERTA_CONTRAOFERTA_ANULADA,
            Oferta::OFERTA_CONTRAOFERTA_CADUCADA,
            Oferta::OFERTA_CONTRAOFERTA_PAGADA,
            Oferta::OFERTA_CONTRAOFERTA_REALIZADA,
            Oferta::OFERTA_CONTRAOFERTA_RECHAZADA,
            Oferta::OFERTA_RECHAZADA);

        $statuses_todos = array(
            Oferta::OFERTA_ACEPTADA,
            Oferta::OFERTA_NO_PAGADA,
            Oferta::OFERTA_PAGADA,
            Oferta::OFERTA_VALIDA,
            Oferta::OFERTA_CADUCADA,
            Oferta::OFERTA_CONTRAOFERTA_ANULADA,
            Oferta::OFERTA_CONTRAOFERTA_CADUCADA,
            Oferta::OFERTA_CONTRAOFERTA_PAGADA,
            Oferta::OFERTA_CONTRAOFERTA_REALIZADA,
            Oferta::OFERTA_CONTRAOFERTA_RECHAZADA,
            Oferta::OFERTA_RECHAZADA);
        $resultado = array();
        if ($type_of_user === 'admin') {
            $dql = " SELECT COUNT(o) total_ofertas_respondidas, h.nombrehotel, " .
                    " (SELECT COUNT(o2) FROM HotelesBackendBundle:Oferta o2 " .
                    " where o2.status in (:statuses_todos) and o2.hotel = o.hotel GROUP BY o2.hotel) total_ofertas " .
                    " FROM HotelesBackendBundle:Oferta o " .
                    " JOIN o.hotel h " .
                    " WHERE o.status in (:statuses) " .
                    " GROUP BY o.hotel " .
                    " ORDER BY total_ofertas_respondidas DESC ";
            $consulta = $em->createQuery($dql);
            $consulta->setParameter("statuses", $statuses);
            $consulta->setParameter("statuses_todos", $statuses_todos);
            $consulta->setMaxResults($rank_size);
            try {
                $res = $consulta->getScalarResult();
            } catch (Doctrine\ORM\NoResultException $e) {
                $res = 0;
            }
            foreach ($res as $r) {
                $resultado[$r['nombrehotel']] = floatval(100 * $r['total_ofertas_respondidas'] / $r['total_ofertas']);
            }
        } elseif ($type_of_user === 'empresa') {
            $dql = " SELECT COUNT(o) total_ofertas_respondidas, h.nombrehotel, " .
                    " (SELECT COUNT(o2) " .
                    " FROM HotelesBackendBundle:Oferta o2 " .
                    " WHERE o2.status in (:statuses_todos) " .
                    " AND o2.hotel = o.hotel GROUP BY o2.hotel) total_ofertas " .
                    " FROM HotelesBackendBundle:Oferta o " .
                    " JOIN o.hotel h " .
                    " JOIN h.empresa e " .
                    " JOIN e.user_empresa ue " .
                    " WHERE o.status in (:statuses) " .
                    " AND ue.id = :user_empresa_id " .
                    " GROUP BY o.hotel " .
                    " ORDER BY total_ofertas_respondidas DESC ";
            $consulta = $em->createQuery($dql);
            $consulta->setParameter("statuses", $statuses);
            $consulta->setParameter("statuses_todos", $statuses_todos);
            $consulta->setParameter("user_empresa_id", $user_id);
            $consulta->setMaxResults($rank_size);
            try {
                $res = $consulta->getScalarResult();
            } catch (Doctrine\ORM\NoResultException $e) {
                $res = 0;
            }
            if ($res !== 0) {
                foreach ($res as $r) {
                    $resultado[$r['nombrehotel']] = floatval(100 * $r['total_ofertas_respondidas'] / $r['total_ofertas']);
                }
            }
        }
        arsort($resultado);
        return $resultado;
    }

    public function getOffersManagedByUser($user_id)
    {
        $em = $this->getEntityManager();
        $statuses = array(
            Oferta::OFERTA_ACEPTADA,
            Oferta::OFERTA_NO_PAGADA,
            Oferta::OFERTA_PAGADA,
            Oferta::OFERTA_CONTRAOFERTA_ANULADA,
            Oferta::OFERTA_CONTRAOFERTA_CADUCADA,
            Oferta::OFERTA_CONTRAOFERTA_PAGADA,
            Oferta::OFERTA_CONTRAOFERTA_REALIZADA,
            Oferta::OFERTA_CONTRAOFERTA_RECHAZADA,
            Oferta::OFERTA_RECHAZADA);
        $dql = " SELECT COUNT(o) total_ofertas_gestionadas " .
                " FROM HotelesBackendBundle:Oferta o " .
                " WHERE o.status in (:statuses) " .
                " AND o.usuario_hotel = :user_hotel_id ";
        $consulta = $em->createQuery($dql);
        $consulta->setParameter("statuses", $statuses);
        $consulta->setParameter("user_hotel_id", $user_id);
        try {
            $resultado = $consulta->getSingleScalarResult();
        } catch (Doctrine\ORM\NoResultException $e) {
            $resultado = 0;
        }
        return $resultado;
    }

    public function getNumOfertasPendientes($user_id)
    {
        $em = $this->getEntityManager();
        $statuses = array(
            Oferta::OFERTA_VALIDA);
        $dql = " SELECT COUNT(o) total_ofertas_pendientes " .
                " FROM HotelesBackendBundle:Oferta o " .
                " JOIN o.hotel h " .
                " JOIN h.user_hotel uh " .
                " WHERE o.status in (:statuses) " .
                " AND uh.id = :user_hotel_id ";
        $consulta = $em->createQuery($dql);
        $consulta->setParameter("statuses", $statuses);
        $consulta->setParameter("user_hotel_id", $user_id);
        try {
            $resultado = $consulta->getSingleScalarResult();
        } catch (Doctrine\ORM\NoResultException $e) {
            $resultado = 0;
        }
        return $resultado;
    }

    /**
     * Me devuelve aquellas ofertas que tengan pendiente una acción de un usuario
     * Lo utilizo para saber que ofertas puedo suspender de un usuario
     * @param type $user_id
     */
    public function getOffersPendingUserAction(\Hoteles\BackendBundle\Entity\Hotel $hotel)
    {
        $statuses = array(Oferta::OFERTA_VALIDA, Oferta::OFERTA_ACEPTADA, Oferta::OFERTA_CONTRAOFERTA_REALIZADA);
        $dql = "SELECT o from HotelesBackendBundle:Oferta o " .
                " WHERE o.status in (:valid_statuses) AND "
                . " o.hotel = :hotel";
        $em = $this->getEntityManager();
        $consulta = $em->createQuery($dql);
        $consulta->setParameter('valid_statuses', $statuses);
        $consulta->setParameter('hotel', $hotel);
        try {
            $resultado = $consulta->getResult();
        } catch (Exception $ex) {
            $resultado = null;
        }
        return $resultado;
    }

}
