<?php

namespace Hoteles\BackendBundle\Command;

use Hoteles\BackendBundle\Entity\UserAdministrador;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Finder\Finder;

class RoomasticInstallCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
                ->setName('roomastic:install')
                ->setDescription('Instala y configura un usuario básico admin')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln('<info>Instalando Roomastic</info>');
        $this->setupRoomastic($input, $output);
        $output->writeln('<info>Roomastic instalado!</info>');
    }

    protected function setupRoomastic(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Configurando BD.</info>');
        $this->setupRoomasticDatabase($input, $output);

        $output->writeln('<info>Configurando los directorios</info>');
        $this->setupRoomasticDirs($input, $output);

        $output->writeln('<info>Configurando SuperAdmin.</info>');
        $this->setupRoomasticAdmin($output);

        $output->writeln('<info>Cargando datos por defecto</info>');
        $this->loadRoomasticFixtures($input, $output);

        $output->writeln('<info>Roomastic configurada</info>');

        return $this;
    }

    protected function setupRoomasticDatabase(InputInterface $input, OutputInterface $output)
    {
        if ($this->runCommand('doctrine:database:create', $input, $output)) {
            $output->writeln('<error>Error al crear la base de datos</error>');
        }
        if ($this->runCommand('doctrine:schema:create', $input, $output)) {
            $output->writeln('<error>Error al crear el esquema de la base de datos</error>');
        }
        if ($this->runCommand('assets:install web --symlink', $input, $output)) {
            $output->writeln('<error>Error al instalar los assets</error>');
        }
        if ($this->runCommand('assetic:dump', $input, $output)) {
            $output->writeln('<error>Error al crear el esquema de la base de datos</error>');
        }
    }

    protected function setupRoomasticDirs(InputInterface $input, OutputInterface $output)
    {
        $dirs = array();
        $dirs[] = 'uploads';
        $dirs[] = 'uploads/temporal';
        $dirs[] = 'uploads/hoteles';
        $rootDir = $this->getContainer->get('kernel')->getRootDir() . '/../web/';
        foreach ($dirs as $dir) {
            $process = new Process('mkdir -p ' . $rootDir . $dir);
            $process->setTimeout(5);
            $process->run();
            if ($process->isSuccessful() === false) {
                throw new \RuntimeException($process->getErrorOutput());
            } else {
                $output->writeln(sprintf('<info>Creado el directorio: %s</info>', $dir));
            }
        }
    }

    protected function setupRoomasticAdmin(OutputInterface $output)
    {
        $dialog = $this->getHelperSet()->get('dialog');
        $user = new UserAdministrador();
        $user->setUsername($dialog->ask($output, '<question>Usuario:</question>'));
        $user->setPlainPassword($dialog->ask($output, '<question>Contraseña:</question>'));
        $user->setEmail($dialog->ask($output, '<question>Email:</question>'));
        $user->setImagen('roomastic.jpeg');
        $user->setEnabled(true);
        $user->addRole('ROLE_ADMIN');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($user);
        $em->flush();
    }

    protected function loadRoomasticFixtures(InputInterface $input, OutputInterface $output)
    {
        $finder = new Finder();
        $finder->in('../sql');
        $finder->name('listadoMunicipiosYProvincias.sql');
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        foreach ($finder as $file) {
            $content = $file->getContents();
            $stmt = $em->getConnection()->prepare($content);
            $stmt->execute();
        }
        $em->flush();
    }

    protected function runCommand($command, InputInterface $input, OutputInterface $output)
    {
        return $this->getApplication()
                        ->find($command)
                        ->run($input, $output);
    }

}
