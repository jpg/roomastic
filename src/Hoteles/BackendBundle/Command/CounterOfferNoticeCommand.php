<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Input\InputOption;
use Hoteles\BackendBundle\Entity\Oferta;
use Doctrine\ORM\Query;
use Hoteles\BackendBundle\Interfaces\ConfigureCommandEnviromentInterface;

/**
 * Notifica al usuario de que su oferta no ha sido aceptada y que tiene n contraofertas
 * -Busco las contraofertas validadas que no hayan enviado notificacion
 *
 * @author joaquin
 */
class CounterOfferNoticeCommand extends ContainerAwareCommand implements ConfigureCommandEnviromentInterface
{

    //put your code here
    protected function configure()
    {
        $this->setName('hoteles:counteroffer:notice')
                ->setDescription('Envía notificacion de estado de contraofertas')
                ->addOption('mock', null, InputOption::VALUE_NONE, 'No guarda cambios en la BD');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->configureCommandEnviroment();
        $em = $this->getContainer()
                ->get('doctrine.orm.entity_manager');
        //Busco los grupos de ofertas que:
        // - Estén validadas
        // - No estén notificadas
        $id_contraofertas_grupo_a_notificar = $em->getRepository('HotelesBackendBundle:Oferta')
                ->getPendingCounterOfferGroup();
        $oferta_mailer = $this->getContainer()->get('oferta.notice_mailer');
        $output->writeln(sprintf('Encontradas <info>%s</info> contraofertas para notificar', count($id_contraofertas_grupo_a_notificar)));
        $verbose = $input->getOption('verbose');

        if ($verbose) {
            $fecha_actual = new \DateTime();
            $output->writeln(sprintf('Hora de ejecucion: %s', $fecha_actual->format('d-m-Y H:i')));
        }
        foreach ($id_contraofertas_grupo_a_notificar as $id_contraoferta) {
            //Pillo solamente las contraofertas del grupo
            $ofertas_del_grupo = $em->getRepository('HotelesBackendBundle:Oferta')
                    ->getCounterOffersFromGroup($id_contraoferta['idoferta']);
            if ($verbose) {
                $output->writeln(sprintf('Encontrada la oferta (%s)', $id_contraoferta['idoferta']));
            }
            foreach ($ofertas_del_grupo as $oferta) {
                if ($oferta->isContraoferta()) {
                    $oferta->markCounterOfferNoticeSent();
                }
                $em->persist($oferta);
            }
            $oferta_mailer->sendOfferCounterOfferedSummary($ofertas_del_grupo);
        }
        if (false === $input->getOption('mock')) {
            $em->flush();
        } else {
            $output->writeln("Cambios NO guardados");
        }
    }

    public function configureCommandEnviroment()
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost('www.roomastic.com');
        $context->setScheme('https');
    }

}
