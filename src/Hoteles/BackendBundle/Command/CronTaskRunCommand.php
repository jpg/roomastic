<?php

namespace Hoteles\BackendBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\StringInput;

class CronTaskRunCommand extends ContainerAwareCommand
{

    private $output;

    protected function configure()
    {
        $this->setName('crontasks:run')
                ->setDescription('Runs Cron Tasks if needed');
    }

    /*
     * Referencia: http://inuits.eu/blog/creating-automated-interval-based-cron-tasks-symfony2
     * Esta tarea es la que vamos a ejecutar periodicamente para lanzar el resto de crons de symfony2
     * Hay que ejecutar la siguiente sentencia para instalarla en el crontab ( */

    //(crontab -l ; echo " */1 * * * * php ~/roomastic/app/console crontasks:run >> ~/logs/cron_logs/avisos_cron_job.log 2>&1") | crontab -
    /**/

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Running Cron Tasks...</comment>');

        $this->output = $output;
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $crontasks = $em->getRepository('HotelesBackendBundle:CronTask')->findAll();
        $verbose = $input->getOption('verbose');
        foreach ($crontasks as $crontask) {
            // Get the last run time of this task, and calculate when it should run next
            $lastrun = $crontask->getLastRun() ? $crontask->getLastRun()->format('U') : 0;
            $nextrun = $lastrun + $crontask->getInterval();

            // We must run this task if:
            // * time() is larger or equal to $nextrun
            $run = (time() >= $nextrun);

            if ($run) {
                if ($verbose) {
                    $output->writeln(sprintf('Running Cron Task <info>%s</info>', $crontask));
                }

                // Set $lastrun for this crontask
                $crontask->setLastRun(new \DateTime());

                try {
                    $commands = $crontask->getCommands();
                    foreach ($commands as $command) {
                        if ($verbose) {
                            $output->writeln(sprintf('Executing command <comment>%s</comment>...', $command));
                        }
                        // Run the command
                        $this->runCommand($command);
                    }
                    if ($verbose) {
                        $output->writeln('<info>SUCCESS</info>');
                    }
                } catch (\Exception $e) {
                    $output->writeln(var_export($e, true));
                    $output->writeln('<error>ERROR</error>');
                }

                // Persist crontask
                $em->persist($crontask);
            } else {
                if ($verbose) {
                    $output->writeln(sprintf('Skipping Cron Task <info>%s</info>', $crontask));
                }
            }
        }

        // Flush database changes
        $em->flush();

        if ($verbose) {
            $output->writeln('<comment>Done!</comment>');
        }
    }

    private function runCommand($string)
    {
        // Split namespace and arguments
        $array_respuesta = preg_split('/ /', $string);
        $namespace = $array_respuesta[0];

        // Set input
        $command = $this->getApplication()->find($namespace);
        $input = new StringInput($string);

        // Send all output to the console
        $returnCode = $command->run($input, $this->output);

        return $returnCode != 0;
    }

}
