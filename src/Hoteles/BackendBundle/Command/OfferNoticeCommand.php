<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Input\InputOption;
use Hoteles\BackendBundle\Entity\Oferta;
use Doctrine\ORM\Query;
use Hoteles\BackendBundle\Interfaces\ConfigureCommandEnviromentInterface;

/**
 * Description of AcceptedOfferNoticeCommand
 *
 * @author joaquin
 */
class OfferNoticeCommand extends ContainerAwareCommand implements ConfigureCommandEnviromentInterface
{

    protected function configure()
    {
        $this->setName('hoteles:offer:notice')
                ->setDescription('Envía todas las notificaciones de ofertas aceptadas y NO pagadas')
                ->addOption('mock', null, InputOption::VALUE_NONE, 'No guarda cambios en la BD');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->configureCommandEnviroment();
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $configuracion = $this->getContainer()->get('hoteles_backend.load_configuracion')->loadConfiguracion();

        //Cargo la parte de la configuracion que voy a usar
        $tiempoRecordatorio = $configuracion->getTiempoRecordatorioPago();
        $tiempoAlerta = $configuracion->getTiempoAlertaPago();
        $tiempoBaneo = $configuracion->getTiempousuariosoferta();

        $ofertas_pendientes_de_pago = $em->getRepository('HotelesBackendBundle:Oferta')->getPendingPaymentOffers();
        $fecha_actual = new \DateTime();
        //Helpers
        $oferta_mailer = $this->getContainer()->get('oferta.notice_mailer');
        $oferta_manager = $this->getContainer()->get('hoteles_backend.oferta_manager');
        //Escribo información por pantalla
        $output->writeln(sprintf('Encontradas <info>%s</info> ofertas para notificar', count($ofertas_pendientes_de_pago)));
        $verbose = $input->getOption('verbose');
        if ($verbose) {
            $output->writeln(sprintf('Hora de ejecucion: %s', $fecha_actual->format('d-m-Y H:i')));
        }
        foreach ($ofertas_pendientes_de_pago as $oferta) {
            if ($verbose) {
                $output->writeln(sprintf('Encontrada la oferta (%s)', $oferta->getId()));
            }

            $fecha_primer_recordatorio = clone $oferta->getFechaaceptadaoferta();
            $fecha_primer_recordatorio->modify('+ ' . $tiempoRecordatorio . ' hour');

            $fecha_segundo_recordatorio = clone $oferta->getFechaaceptadaoferta();
            $fecha_segundo_recordatorio->modify('+ ' . $tiempoAlerta . ' hour');

            $fecha_tercer_recordatorio = clone $oferta->getFechaaceptadaoferta();
            $fecha_tercer_recordatorio->modify('+ ' . $tiempoBaneo . ' hour');

            if ($verbose) {
                $fecha_actual = new \DateTime();
                $output->writeln(sprintf('Fecha primer recordatorio: %s', $fecha_primer_recordatorio->format('Y-m-d H:i:s')));
                $output->writeln(sprintf('Fecha segundo recordatorio: %s', $fecha_segundo_recordatorio->format('Y-m-d H:i:s')));
                $output->writeln(sprintf('Fecha tercer recordatorio: %s', $fecha_tercer_recordatorio->format('Y-m-d H:i:s')));
                $output->writeln(sprintf('Fecha actual: %s', $fecha_actual->format('Y-m-d H:i:s')));
                $output->writeln(sprintf('Oferta %s', $oferta->debug()));
                $output->writeln(sprintf('Horas primer recordatorio: %s', $tiempoRecordatorio));
                $output->writeln(sprintf('Horas primer alerta: %s', $tiempoAlerta));
                $output->writeln(sprintf('Horas primer baneo: %s', $tiempoBaneo));
            }
            if ($fecha_actual > $fecha_primer_recordatorio && $oferta->isFirstNoticeSent() === false) {
                if ($verbose) {
                    $output->writeln(sprintf('Enviando primera notificacion', $oferta->getId()));
                }
                //Envío el primer aviso y lo marco
                $oferta->markFirstNoticeSent();
                $oferta_mailer->sendFirstNotice($oferta);
                $em->persist($oferta);
            } else if ($fecha_actual > $fecha_segundo_recordatorio && $oferta->isSecondNoticeSent() === false) {
                if ($verbose) {
                    $output->writeln(sprintf('Enviando segunda notificacion', $oferta->getId()));
                }
                //Envío el segundo aviso y lo marco
                $oferta->markSecondNoticeSent();
                $oferta_mailer->sendSecondNotice($oferta);
                $em->persist($oferta);
            } else if ($fecha_actual > $fecha_tercer_recordatorio && $oferta->isThirdNoticeSent() === false) {
                if ($verbose) {
                    $output->writeln(sprintf('Enviando tercera notificacion', $oferta->getId()));
                }
                //Envío el tercer aviso y lo marco. También baneo al usuario
                $oferta->markThirdNoticeSent();
                $usuario = $oferta->getUsuario();
                $usuario->banUser();
                $resultado = $oferta_manager->updateOfferState('no_paga_oferta', $oferta);
                if ($resultado) {
                    $oferta_mailer->sendThirdNotice($oferta);
                    $em->persist($oferta);
                    $em->persist($usuario);
                } else {
                    $logger = $this->getContainer()->get('logger');
                    $logger->err('Error al procesar la oferta(' . $oferta->getId() + ')');
                    $output->writeln(sprintf('<error>ERROR</error> Oferta($d) no procesada. Transicion: $s', $oferta->getId(), 'no_paga_oferta'));
                }
            } else {
                if ($verbose) {
                    $output->writeln(sprintf('<info>INFO</info>: Oferta(%s) no procesada. Todavía no hace falta', $oferta->getId()));
                }
            }
        }
        if (false === $input->getOption('mock')) {
            $em->flush();
        } else {
            $output->writeln("Cambios NO guardados");
        }
    }

    public function configureCommandEnviroment()
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost('www.roomastic.com');
        $context->setScheme('https');
    }

}
