<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Input\InputOption;
use Hoteles\BackendBundle\Entity\Oferta;
use Doctrine\ORM\Query;
use Hoteles\BackendBundle\Interfaces\ConfigureCommandEnviromentInterface;

/**
 * Caduca las contraofertas NO pagadas
 *
 * @author joaquin
 */
class CounterOfferExpireCommand extends ContainerAwareCommand implements ConfigureCommandEnviromentInterface
{

    //put your code here
    protected function configure()
    {
        $this->setName('hoteles:counteroffer:expire')
                ->setDescription('Caduca las contraofertas no pagadas')
                ->addOption('mock', null, InputOption::VALUE_NONE, 'No guarda cambios en la BD');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->configureCommandEnviroment();
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $configuracion = $this->getContainer()->get('hoteles_backend.load_configuracion')->loadConfiguracion();
        $horas_fecha_expiracion = $configuracion->getTiempousuarioscontraoferta();
        // TODO: Revisar esto, igual se puede simplificar
        $id_contraofertas_grupo_a_caducar = $em->getRepository('HotelesBackendBundle:Oferta')
                ->getExpiredCounterOfferGroup($horas_fecha_expiracion);
        $output->writeln(sprintf('Buscando contraofertas desatendidas durante más de <info>%s</info> horas', $horas_fecha_expiracion));

        $output->writeln(sprintf('Encontradas <info>%s</info> grupos de contraofertas para caducar', count($id_contraofertas_grupo_a_caducar)));
        //Helpers
        $oferta_manager = $this->getContainer()->get('hoteles_backend.oferta_manager');
        $verbose = $input->getOption('verbose');
        if ($verbose) {
            $fecha_actual = new \DateTime();
            $output->writeln(sprintf('Hora de ejecucion: %s', $fecha_actual->format('d-m-Y H:i')));
        }
        foreach ($id_contraofertas_grupo_a_caducar as $id_contraoferta) {

            if ($verbose) {
                $output->writeln(sprintf('Encontrado el grupo %s con contraofertas para caducar ', $id_contraoferta['idoferta']));
            }
            $ofertas_del_grupo = $em->getRepository('HotelesBackendBundle:Oferta')
                    ->getOffersFromGroup($id_contraoferta['idoferta']);

            //Le paso una oferta cualquiera, por defecto la primera
            $oferta_manager->updateOfferGroupState('caduca_contraoferta', $ofertas_del_grupo[0]);
            $oferta_cero = $ofertas_del_grupo[0];
            $oferta_cero->removeOfferBanFromUser();
        }
        if (false === $input->getOption('mock')) {
            $em->flush();
        } else {
            $output->writeln("Cambios NO guardados");
        }
    }

    public function configureCommandEnviroment()
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost('www.roomastic.com');
        $context->setScheme('https');
    }

}
