<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class LugarType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {

        //ladybug_dump($options['attr']); exit;

        $builder
                //->add('provincia')
                ->add('provincia', 'entity', array(
                    'class' => 'HotelesBackendBundle:User',
                    'query_builder' => $options['attr']['provincias'],
                ))
                //->add('municipio')
                ->add('municipio', 'entity', array(
                    'class' => 'HotelesBackendBundle:User',
                    'query_builder' => $options['attr']['municipios'],
                ))
                ->add('zona', 'text', array(
                    'required' => false,
                ))
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_lugartype';
    }

}
