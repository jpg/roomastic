<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ConfiguracionType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('descuentoentero', 'text', array(
                    'required' => false,
                ))
                ->add('descuentodecimal', 'text', array(
                    'required' => false,
                ))
                ->add('tiempohotel', 'text', array(
                    'required' => false,
                ))
                ->add('tiempousuariosoferta', 'text', array(
                    'required' => false,
                ))
                ->add('tiempousuarioscontraoferta', 'text', array(
                    'required' => false,
                ))
                ->add('fbappid', 'text', array(
                    'required' => false,
                ))
                ->add('fbappsecret', 'text', array(
                    'required' => false,
                ))
                ->add('googappid', 'text', array(
                    'required' => false,
                ))
                ->add('googapikey', 'text', array(
                    'required' => false,
                ))
                ->add('googappsecret', 'text', array(
                    'required' => false,
                ))
                ->add('twitterappid', 'text', array(
                    'required' => false,
                ))
                ->add('twitterappsecret', 'text', array(
                    'required' => false,
                ))
                ->add('tiemporecordatoriopago', 'text', array(
                    'required' => false,
                ))
                ->add('tiempoalertapago', 'text', array(
                    'required' => false,
                ))
                ->add('entorno', 'choice', array(
                    'choices' => array('demo' => 'Demo', 'prod' => 'Produccion'),
                    'required' => false,
                    'expanded' => true
                ))
                ->add('nombre_facebook', 'text', array(
                    'required' => false,
                ))
                ->add('nombre_twitter', 'text', array(
                    'required' => false,
        ));
    }

    public function getName()
    {
        return 'hoteles_backendbundle_configuraciontype';
    }

}
