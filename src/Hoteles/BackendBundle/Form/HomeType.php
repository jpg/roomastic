<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class HomeType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                //->add('imagen')
                ->add('imagen', 'file', array(
                    'required' => false,
                ))
                //->add('status')
                ->add('status', 'choice', array(
                    'choices' => array('1' => 'Publicado', '0' => 'No publicado'),
                    'required' => true,
                ))
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_hometype';
    }

}
