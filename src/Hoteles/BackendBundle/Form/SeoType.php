<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class SeoType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('seotitulo', 'text', array(
                    'required' => false,
                ))
                ->add('seodescripcion', 'textarea', array(
                    'required' => false,
                ))
                ->add('seokeywords', 'textarea', array(
                    'required' => false,
                ))
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_seotype';
    }

}
