<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class IncidenciaType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {

        $users = $options['users'];
        $builder
                ->add('usuariofrom', 'entity', array(
                    'class' => 'HotelesBackendBundle:User',
                    'choices' => $users,
                    'property' => 'PublicUsername',
                    'multiple' => false,
                    'required' => true
                ))
                //->add('usuarioto')
                //->add('usuarioto', 'entity', array('class' => 'HotelesBackendBundle:User','query_builder' => $this->options,))
                ->add('usuarioto', 'entity', array(
                    'class' => 'HotelesBackendBundle:User',
                    'choices' => $users,
                    'property' => 'PublicUsername',
                    'multiple' => false,
                    'required' => true
                ))
                //->add('fecha')
                ->add('asunto')
                ->add('mensaje')
                ->add('atendido');
    }

    public function getName()
    {
        return 'hoteles_backendbundle_incidenciatype';
    }

    public function getDefaultOptions(array $options)
    {
        return array('users' => null);
    }

}
