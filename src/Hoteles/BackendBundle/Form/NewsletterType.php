<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class NewsletterType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('usuario')
                ->add('fecha')
                ->add('activo')
                ->add('fechadisabled')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_newslettertype';
    }

}
