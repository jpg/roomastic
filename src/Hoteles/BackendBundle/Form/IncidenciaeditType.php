<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class IncidenciaeditType extends AbstractType
{

    public function __construct($options)
    {
        $this->options = $options;
    }

    public function buildForm(FormBuilder $builder, array $options)
    {

        $builder->add('atendido');
    }

    public function getName()
    {
        return 'hoteles_backendbundle_incidenciatype';
    }

}
