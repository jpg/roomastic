<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Services;

use Doctrine\ORM\EntityManager;
use Hoteles\BackendBundle\Entity\Hotel;
use Hoteles\BackendBundle\Entity\Notificacion;

/**
 * Description of NotificacionServices
 *
 * @author joaquin
 */
class NotificacionManager
{

    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addNotificacion(Hotel $hotel, $tipo)
    {
        $notificaciones = $hotel->getNotificaciones();
        if (is_null($notificaciones)) {
            $notificaciones = new Notificacion();
            $notificaciones->setHotel($hotel);
        }
        switch ($tipo) {
            case "no-pagadas":
                $notificaciones->addOfertaNoPagadas();
                break;
            case "pagadas":
                $notificaciones->addOfertaPagadas();
                break;
            case "pendientes":
                $notificaciones->addOfertaPendiente();
                break;
            case "contraoferta-perdida":
                $notificaciones->addContraOfertaPerdida();
                break;
        };
        $this->em->persist($notificaciones);
    }

    /**
     * El filtro que usa esta funcion es el mismo que usa OfertaRepository
     */
    public function resetNotificaciones(Hotel $hotel, $tipo)
    {
        $notificaciones = $hotel->getNotificaciones();

        if (is_null($notificaciones)) {
            $notificaciones = new Notificacion();
            $notificaciones->setHotel($hotel);
        }
        switch ($tipo) {
            case "no-pagadas":
                $notificaciones->resetOfertasNoPagadas();
                break;
            case "pagadas":
                $notificaciones->resetOfertasPagadas();
                break;
            case "pendientes":
                $notificaciones->resetOfertasPendientes();
                break;
            case "contraoferta-perdida":
            case "contraoferta-caducada":
            case "contraofertas-rechazadas":
                $notificaciones->resetContraofertasPerdidas();
                break;
            case "todos":
                $notificaciones->resetOfertasNoPagadas();
                $notificaciones->resetOfertasPagadas();
                $notificaciones->resetOfertasPendientes();
                $notificaciones->resetContraofertasPerdidas();
                break;
        };
        $this->em->persist($notificaciones);
        $this->em->flush();
    }

}
