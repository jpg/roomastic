<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Services;

use Symfony\Component\HttpFoundation\Response;

/**
 * Description of getJsonResponse
 *
 * @author xokas
 */
class ReturnJsonResponse
{

    //put your code here

    public function __construct()
    {
        
    }

    public function JSONResponse($responseData, $response_code = 200)
    {
        $respuesta_json = new Response(json_encode($responseData), $response_code);
        $respuesta_json->headers->set('Content-Type', 'application/json');
        return $respuesta_json;
    }

}
