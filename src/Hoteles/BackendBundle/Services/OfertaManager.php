<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Services;

use Doctrine\ORM\EntityManager;
use Hoteles\BackendBundle\Entity\Oferta;
use Hoteles\BackendBundle\Mailer\OfertaMailer;
use Hoteles\BackendBundle\Entity\User;
use Hoteles\BackendBundle\Entity\Hotel;
use Hoteles\BackendBundle\Services\NotificacionManager;

class OfertaManager
{

    protected $em;
    protected $ofertaMailer;
    protected $notificacion_manager;

    public function __construct(EntityManager $em, OfertaMailer $ofertaMailer, NotificacionManager $notificacion_manager)
    {
        $this->em = $em;
        $this->ofertaMailer = $ofertaMailer;
        $this->notificacion_manager = $notificacion_manager;
    }

    /**
     * Devuelve la $oferta con el valor actualizado. Devuelve null si no se ha 
     * podido realizar la transicion correctamente
     * @param type $transition Tipos de transiciones que puede tener una ofera
     * @param \Hoteles\BackendBundle\Entity\Oferta $oferta     /
     */
    public function updateOfferState($transition, Oferta &$oferta, User $usuario_interactua_oferta = null)
    {
        switch ($transition) {
            case 'rechaza_oferta':
                if ($oferta->getStatus() === Oferta::OFERTA_VALIDA) {
                    $oferta->markOfferRejected($usuario_interactua_oferta);
                } else {
                    return false;
                }
                break;
            case 'caduca_oferta':
                if ($oferta->getStatus() === Oferta::OFERTA_VALIDA) {
                    $oferta->markOfferExpired();
                    //Quito esta linea de abajo porque puede generar problemas de recursividad infinita
                    $this->updateOfferGroupState('caduca_oferta', $oferta);
                } else {
                    return false;
                }
                break;
            case 'acepta_oferta':
                //Compruebo si la oferta es valida y que el grupo de estado de oferta sea INICIAL
                //Como esto puede llegar a dar problemas de concurrencia, inmediatamente la actualizo
                //NO es la mejor solucion, habría que mirar locking
                if ($oferta->getStatus() === Oferta::OFERTA_VALIDA &&
                        ( $oferta->getGroupStatus() === Oferta::GRUPO_OFERTA_ESTADO_INICIAL ||
                        $oferta->getGroupStatus() === Oferta::GRUPO_OFERTA_CONTRAOFERTA_REALIZADA)) {
                    $oferta->markOfferAccepted($usuario_interactua_oferta);
                    $this->em->persist($oferta);
                    $this->updateOfferGroupState('acepta_oferta', $oferta);
                    $oferta->markGroupOfferAccepted();
                } else {
                    return false;
                }
                break;
            case 'otro_hotel_acepta_oferta':
                if ($oferta->getStatus() === Oferta::OFERTA_VALIDA) {
                    $oferta->markOfferOtherHotelAccepted();
                } else {
                    return false;
                }
                break;
            case 'paga_oferta':
                if ($oferta->getStatus() === Oferta::OFERTA_ACEPTADA) {
                    $this->notificacion_manager->addNotificacion($oferta->getHotel(), 'pagadas');
                    $oferta->markOfferPayed();
                } else {
                    return false;
                }
                break;
            case 'no_paga_oferta':
                if ($oferta->getStatus() === Oferta::OFERTA_ACEPTADA) {
                    $oferta->markOfferNotPayed();
                    $this->notificacion_manager->addNotificacion($oferta->getHotel(), 'no-pagadas');
                } else {
                    return false;
                }
                break;
            case 'realiza_contraoferta':
                if ($oferta->getStatus() === Oferta::OFERTA_VALIDA &&
                        ( $oferta->getGroupStatus() === Oferta::GRUPO_OFERTA_ESTADO_INICIAL ||
                        $oferta->getGroupStatus() === Oferta::GRUPO_OFERTA_CONTRAOFERTA_REALIZADA)) {
                    $oferta->markCounterOfferMade($usuario_interactua_oferta);
                    $oferta->markGroupOfferCounterOffered();
                    $this->em->persist($oferta);
                    $this->updateOfferGroupState('realiza_contraoferta', $oferta);
                } else {
                    return false;
                }
                break;
            case 'rechaza_contraoferta':
                if ($oferta->getStatus() === Oferta::OFERTA_CONTRAOFERTA_REALIZADA) {
                    $oferta->markCounterOfferRejected();
                    $this->ofertaMailer->notifyHotelCounterOfferRejected($oferta);
                    $this->notificacion_manager->addNotificacion($oferta->getHotel(), 'contraoferta-perdida');
                } else {
                    return false;
                }
                break;
            case 'anula_contraoferta':
                if ($oferta->getStatus() === Oferta::OFERTA_CONTRAOFERTA_REALIZADA) {
                    $oferta->markCounterOfferAnulment();
                } else {
                    return false;
                }
                break;
            case 'caduca_contraoferta':
                if ($oferta->getStatus() === Oferta::OFERTA_CONTRAOFERTA_REALIZADA) {
                    $oferta->markCounterOfferExpired();
                    //Notifico al hotel que su contraoferta ha caducado
                    $this->ofertaMailer->notifyHotelCounterOfferExpired($oferta);
                } else {
                    return false;
                }
                break;
            case 'paga_contraoferta':
                if ($oferta->getStatus() === Oferta::OFERTA_CONTRAOFERTA_REALIZADA) {
                    $oferta->markCounterOfferPayed();
                    $this->updateOfferGroupState('paga_contraoferta', $oferta);
                } else {
                    return false;
                }
                break;
            case 'hotel_suspenso':
                $oferta_status = $oferta->getStatus();
                if(in_array($oferta_status, array(
                    Oferta::OFERTA_VALIDA, Oferta::OFERTA_ACEPTADA, Oferta::OFERTA_CONTRAOFERTA_REALIZADA))){
                    $oferta->markOfferHotelSuspended();
                }
                return true;                 
            default:
                return false;
        }
        return true;
    }

    /**
     * Actualiza un grupo de ofertas a los nuevos estados, dependiendo de en el que esten
     * @param type $transition Tipos de transiciones que puede tener un grupo de ofertas
     * @param \Hoteles\BackendBundle\Entity\Oferta $oferta     /
     */
    public function updateOfferGroupState($transition, Oferta &$oferta)
    {
        switch ($transition) {
            case 'acepta_oferta':
                $ofertas_relacionadas = $this->em->getRepository('HotelesBackendBundle:Oferta')->findBy(array('idoferta' => $oferta->getIdoferta()));
                foreach ($ofertas_relacionadas as $oferta_relacionada) {
                    //Compruebo que no es la oferta que le he pasado
                    if ($oferta->getIdofertaunico() !== $oferta_relacionada->getIdofertaunico()) {
                        $oferta_relacionada->markGroupOfferAccepted();
                        if ($oferta_relacionada->getStatus() === Oferta::OFERTA_CONTRAOFERTA_REALIZADA) {
                            $this->updateOfferState('anula_contraoferta', $oferta_relacionada);
                        } else if ($oferta_relacionada->getStatus() === Oferta::OFERTA_VALIDA) {
                            $this->updateOfferState('otro_hotel_acepta_oferta', $oferta_relacionada);
                        }
                        $this->em->persist($oferta_relacionada);
                    }
                }
                break;
            case 'paga_contraoferta':
                $ofertas_relacionadas = $this->em->getRepository('HotelesBackendBundle:Oferta')->findBy(array('idoferta' => $oferta->getIdoferta()));
                $this->notificacion_manager->addNotificacion($oferta->getHotel(), 'pagadas');
                foreach ($ofertas_relacionadas as $oferta_relacionada) {
                    //Compruebo que no es la oferta que le he pasado
                    if ($oferta->getIdofertaunico() !== $oferta_relacionada->getIdofertaunico()) {
                        //Si alguna de las ofertas tenía una contraoferta realizada
                        if ($oferta_relacionada->getStatus() === Oferta::OFERTA_CONTRAOFERTA_REALIZADA) {
                            $this->updateOfferState('rechaza_contraoferta', $oferta_relacionada);
                        }
                        $this->em->persist($oferta_relacionada);
                    }
                }
                break;
            case 'caduca_oferta':
                $ofertas_relacionadas = $this->em->getRepository('HotelesBackendBundle:Oferta')
                        ->findBy(array('idoferta' => $oferta->getIdoferta()));
                foreach ($ofertas_relacionadas as $oferta_relacionada) {
                    if ($oferta_relacionada->getStatus() === Oferta::OFERTA_VALIDA) {
                        $this->updateOfferState('caduca_oferta', $oferta_relacionada);
                    }
                    //Si alguna de las ofertas tenía una contraoferta realizada
                    //Pongo a funcionar el contador para la contraoferta
                    else if ($oferta_relacionada->getStatus() === Oferta::OFERTA_CONTRAOFERTA_REALIZADA) {
                        $oferta_relacionada->updateCounterOfferDate();
                    }
                    //Si el estado del grupo es CONTRAOFERTA_REALIZADA lo cambio al estado de CONTRAOFERTA_VALIDADA
                    if ($oferta_relacionada->getGroupStatus() === Oferta::GRUPO_OFERTA_CONTRAOFERTA_REALIZADA) {
                        $oferta_relacionada->markGroupOfferCounterOfferValidated();
                    } else if ($oferta_relacionada->getGroupStatus() === Oferta::GRUPO_OFERTA_ESTADO_INICIAL) {
                        $oferta_relacionada->markGroupOfferExpired();
                    }
                    //echo "Persistiendo. OfertaID: ".$oferta_relacionada->getId()."\n";
                    $this->em->persist($oferta_relacionada);
                }
                break;
            case 'realiza_contraoferta':
                $ofertas_relacionadas = $this->em->getRepository('HotelesBackendBundle:Oferta')
                        ->findBy(array('idoferta' => $oferta->getIdoferta()));
                foreach ($ofertas_relacionadas as $oferta_relacionada) {
                    //Compruebo que no es la oferta que le he pasado
                    if ($oferta->getIdofertaunico() !== $oferta_relacionada->getIdofertaunico()) {
                        //Actualizo el estado del grupo a contraofertada
                        $oferta_relacionada->markGroupOfferCounterOffered();
                        $this->em->persist($oferta_relacionada);
                    }
                }
                //Pendiente de quitar esto de aqui
                $this->em->flush();
                break;
            case 'caduca_contraoferta':
                $ofertas_relacionadas = $this->em->getRepository('HotelesBackendBundle:Oferta')
                        ->findBy(array('idoferta' => $oferta->getIdoferta()));
                foreach ($ofertas_relacionadas as $oferta_relacionada) {
                    //Compruebo que no es la oferta que le he pasado
                    //if ($oferta->getIdofertaunico() !== $oferta_relacionada->getIdofertaunico()) {
                    //Actualizo el estado del grupo a oferta expirada
                    if ($oferta_relacionada->getStatus() === Oferta::OFERTA_CONTRAOFERTA_REALIZADA) {
                        $this->updateOfferState('caduca_contraoferta', $oferta_relacionada);
                        $this->notificacion_manager->addNotificacion($oferta_relacionada->getHotel(), 'contraoferta-perdida');
                    }
                    $oferta_relacionada->markGroupOfferCounterOfferExpired();

                    $this->em->persist($oferta_relacionada);
                    //}
                }
                break;
            default:
                return false;
        }
    }

    /**
     * Esta es una fabrica de ofertas. Crea una oferta con los datos minimos necesarios
     * 
     */
    public function createOffer(User $user, Hotel $hotel, \DateTime $fecha, $preciohabitacion, \DateTime $fecha_entrada, \DateTime $fecha_salida, $numadultos, $numninos, $numhabitaciones, $id_grupo_oferta)
    {
        $oferta = new Oferta();
        $oferta->setFecha($fecha);
        $oferta->setUsuario($user);
        $oferta->setHotel($hotel);
        $oferta->setPreciohabitacion($preciohabitacion);
        $oferta->setFechain($fecha_entrada);
        $oferta->setFechaout($fecha_salida);
        $oferta->setNumadultos($numadultos);
        $oferta->setNumninos($numninos);
        $oferta->setNumhabitaciones($numhabitaciones);
        $oferta->setNumnoches($fecha_salida->diff($fecha_entrada)->days);
        $precio_total_oferta = $oferta->getPreciohabitacion() * $oferta->getNumhabitaciones() * $oferta->getNumnoches();
        $oferta->setPreciototaloferta($precio_total_oferta);
        $oferta->setComisionOferta($hotel->getComision());
        $oficial = (float) $hotel->getPVP();
        $minimo = (float) $hotel->getPrecioMinimo();
        
        $oferta->setGroupStatus(Oferta::GRUPO_OFERTA_ESTADO_INICIAL);
        $oferta->setIdofertaunico(sha1(uniqid(mt_rand(), true)));
        $oferta->setIdoferta($id_grupo_oferta);
        error_log("[{$id_grupo_oferta}] - Precio minimo: {$minimo}. Precio ofertado: {$oferta->getPreciohabitacion()}");
        
        if ($oferta->getPreciohabitacion() < $minimo) {
            error_log("Oferta inferior al minimo - [{$id_grupo_oferta}]. IdUnico: {$oferta->getIdofertaunico()}");
            $oferta->setStatus(Oferta::OFERTA_INFERIOR_MINIMO);
        } else if ($oferta->getPreciohabitacion() > $oficial) {
            error_log("Oferta superior al maximo - [{$id_grupo_oferta}]. IdUnico: {$oferta->getIdofertaunico()}");
            $oferta->setStatus(Oferta::OFERTA_SUPERIOR_PVP);
        } else if ($oferta->getPreciohabitacion() >= $minimo &&
                $oferta->getPreciohabitacion() <= $oficial) {
            error_log("Oferta correcta - [{$id_grupo_oferta}]. IdUnico: {$oferta->getIdofertaunico()}");
            $oferta->setStatus(Oferta::OFERTA_VALIDA);
            $this->notificacion_manager->addNotificacion($hotel, 'pendientes');
        }
        
        return $oferta;
    }

}
