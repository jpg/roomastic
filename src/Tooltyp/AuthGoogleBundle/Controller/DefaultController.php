<?php

namespace Tooltyp\AuthGoogleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Hoteles\BackendBundle\Entity\UserBasico;

class DefaultController extends Controller
{
    /*
     * El diagrama de flujo que se utiliza para realizar el login es el que viene aquí especificado
     * https://developers.google.com/+/web/signin/server-side-flow
     * Y el código php que se utiliza también es muy similar
     *      
     */

    public function indexAction(Request $request)
    {
        //include google api files
        require_once 'Google_Client.php';
        require_once 'contrib/Google_Oauth2Service.php';
        require_once 'contrib/Google_PlusService.php';

        $em = $this->getDoctrine()->getEntityManager();
        //Cargo la configuracion de google
        $configuracion = $em->getRepository('HotelesBackendBundle:Configuracion')->findAll();

        //Configuración de google
        $google_client_id = $configuracion[0]->getGoogAppId(); //'761082287320-q5e8gvt7hn49dg8l0v6e060iulle2pvt.apps.googleusercontent.com';
        $google_client_secret = $configuracion[0]->getGoogAppSecret(); //'BL9mtvTFUvCkR79wmf4KGUu2';
        //$google_developer_key = $configuracion[0]->getGoogApiKey(); //'AIzaSyAsqQ8xiKvv4w03GGOLDEtyWKQVhmTDBuE';

        $client = new \Google_Client();
        $client->setApplicationName('Roomastic');
        $client->setClientId($google_client_id);
        $client->setClientSecret($google_client_secret);
        $client->setRedirectUri('postmessage');
        $plus = new \Google_PlusService($client);
        $datos = json_decode($request->getContent(), true);
        $google = new \Google_Oauth2Service($client);
        // Intercambia el código de autorización de OAuth 2.0 por credenciales de usuario.
        if (!isset($datos['code'])) {
            $respuesta_json = new Response(json_encode(array('response' => 'Error con los datos enviados')), 400);
            $respuesta_json->headers->set('Content-Type', 'application/json');
            return $respuesta_json;
        }
        $client->authenticate($datos['code']);
        $token = json_decode($client->getAccessToken());

        //Verifico el token
        $request_url = 'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=' .
                $token->access_token;
        $google_request = new \Google_HttpRequest($request_url);

        $tokenInfo = json_decode(
                $client::getIo()->authenticatedRequest($google_request)->getResponseBody());

        // Si ha habido un error en la información del token, anula la operación.
        if (isset($tokenInfo->error)) {
            $respuesta_json = new Response(json_encode($tokenInfo->error), 500);
            $respuesta_json->headers->set('Content-Type', 'application/json');
            return $respuesta_json;
        }

        if ($tokenInfo->audience != $google_client_id) {
            $respuesta_json = new Response(json_encode(array('response' => 'Error con el token')), 400);
            $respuesta_json->headers->set('Content-Type', 'application/json');
            return $respuesta_json;
        }

        if ($client->getAccessToken()) {
            //Cargo los detalles del servidor de google
            $google_user_data = $google->userinfo->get();
            $google_user_id = $google_user_data['id'];
            $user = $em->getRepository('HotelesBackendBundle:UserBasico')
                    ->findUserByEmailFacebookEmailGoogleTwitter($google_user_id, $google_user_data['email']);
            if ($user && $user->getTypeOf() !== 'basico') {
                $response['message'] = 'El correo con el que estás intentando acceder pertenece a un usuario de mayor rango.' .
                        ' Para realizar tu oferta, debes registrarte con otro correo.';
                $respuesta_json = new Response(json_encode($response), 400);
                $respuesta_json->headers->set('Content-Type', 'application/json');
                return $respuesta_json;
            }
            if (is_null($user)) {
                $user = new UserBasico();
                $user->setRoles(array('ROLE_WEB'));
                $user->setGoogleData($google_user_data);
                $user->setOrigin("google");
                $user->setUsername(sha1(uniqid(mt_rand(), true)));
                $user->setPlainPassword(sha1(uniqid(mt_rand(), true)));
                $user->setEnabled(true);
                $response['created'] = true;
            } else {
                $response['created'] = false;
                if ($user instanceof UserBasico) {
                    $user->setGoogleData($google_user_data);
                }
            }
            $em->persist($user);
            $em->flush();
            //Logueo al usuario recién creado
            $this->get('user.force_login')->forceUserLogin($this->getRequest(), $user);
            $response['user_data'] = $user->getUserData($this->container->getParameter('limite_ofertas'));
            $respuesta_json = new Response(json_encode($response), 200);
            $respuesta_json->headers->set('Content-Type', 'application/json');
            return $respuesta_json;
        } else {
            $respuesta_json = new Response(json_encode('Error', 400));
            $respuesta_json->headers->set('Content-Type', 'application/json');
            return $respuesta_json;
        }
    }

}
