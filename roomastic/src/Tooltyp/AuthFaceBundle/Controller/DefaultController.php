<?php

namespace Tooltyp\AuthFaceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Tooltyp\AuthFaceBundle\Facebook\Facebook;
use Hoteles\BackendBundle\Entity\UserBasico;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Hoteles\FrontendBundle\Services\ForceLogin;

class DefaultController extends Controller
{

    public function indexAction()
    {

        require 'facebook.php';

        $em = $this->getDoctrine()->getEntityManager();
        $configuracion = $em->getRepository('HotelesBackendBundle:Configuracion')->findAll();

        //ladybug_dump($configuracion); exit;

        $facebook = new \Facebook(array(
            'appId' => $configuracion[0]->getFbappid(),
            'secret' => $configuracion[0]->getFbappsecret(),
        ));

        $user_id = $facebook->getUser();
        $user_profile = '';
        $response = array();

        if ($user_id) {
            $user_profile = $facebook->api('/me');
            //Supongo que ya tengo el e-mail dado que se comprueba en el cliente
            $email = $user_profile['email'];

            $user = $em->getRepository('HotelesBackendBundle:UserBasico')->findUserByEmailFacebookEmailGoogleTwitter($user_id, $email);
            if ($user && $user->getTypeOf() !== 'basico') {
                $response['message'] = 'El correo con el que estás intentando acceder pertenece a un usuario de mayor rango.' .
                        ' Para realizar tu oferta, debes registrarte con otro correo.';
                $respuesta_json = new Response(json_encode($response), 400);
                $respuesta_json->headers->set('Content-Type', 'application/json');
                return $respuesta_json;
            }
            if (is_null($user)) {
                $user = new UserBasico();
                $user->setRoles(array('ROLE_WEB'));
                $user->setFBData($user_profile);
                $user->setOrigin("facebook");
                $user->setUsername(sha1(uniqid(mt_rand(), true)));
                $user->setPlainPassword(sha1(uniqid(mt_rand(), true)));
                $user->setEnabled(true);
                $response['created'] = true;
            } else {
                $response['created'] = false;
                if ($user instanceof UserBasico) {
                    $user->setFBData($user_profile);
                }
            }
            $em->persist($user);
            $em->flush();
            // Usuario guardado en la base de datos, ahora lo cargamos en la sesion
            $this->get('user.force_login')->forceUserLogin($this->getRequest(), $user);
            $response['user_data'] = $user->getUserData($this->container->getParameter('limite_ofertas'));
            $respuesta_json = new Response(json_encode($response), 200);
            $respuesta_json->headers->set('Content-Type', 'application/json');
            return $respuesta_json;
        } else {
            $respuesta_json = new Response(json_encode($response), 400);
            $respuesta_json->headers->set('Content-Type', 'application/json');
            return $respuesta_json;
        }
    }

}
