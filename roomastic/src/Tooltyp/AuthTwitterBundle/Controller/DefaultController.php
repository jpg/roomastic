<?php

namespace Tooltyp\AuthTwitterBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction()
    {
        return $this->render('TooltypAuthTwitterBundle:Default:index.html.twig', array(
        ));
    }
}
