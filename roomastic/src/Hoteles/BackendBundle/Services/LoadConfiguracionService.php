<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Services;

use Doctrine\ORM\EntityManager;

/**
 * Description of LoadConfiguracionService
 *
 * @author joaquin
 */
class LoadConfiguracionService
{

    //put your code here
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function loadConfiguracion()
    {
        $configuracion = $this->em->getRepository('HotelesBackendBundle:Configuracion')->findAll();
        return $configuracion[0];
    }

}
