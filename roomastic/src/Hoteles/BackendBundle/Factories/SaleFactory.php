<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Factories;

use Hoteles\BackendBundle\Entity\Sale;
use Hoteles\BackendBundle\Entity\Oferta;

class SaleFactory
{

    const NUM_INICIAL_MIN = 0;
    const NUM_INICIAL_MAX = 9999;
    const NUM_CHARS_STRING = 8;

    public static function createOfferSale(Oferta &$oferta)
    {
        $sale = new Sale();
        $sale->setAmount($oferta->getPreciototaloferta());
        $sale->setOferta($oferta);
        $sale->setStatus(Sale::VENTA_PENDIENTE);
        $sale->setFirma(SaleFactory::generateSignature());
        $oferta->setSale($sale);
        return $sale;
    }

    public static function createCounterOfferSale(Oferta &$oferta)
    {
        $sale = self::createOfferSale($oferta);
        $sale->setAmount($oferta->getPreciototalcontraoferta());
        return $sale;
    }

    public static function generateOrderNum()
    {
        $ordernum_integer = mt_rand(self::NUM_INICIAL_MIN, self::NUM_INICIAL_MAX);
        $bytes = openssl_random_pseudo_bytes(self::NUM_CHARS_STRING / 2);
        $ordernum_string = bin2hex($bytes);
        return $ordernum_integer . $ordernum_string;
    }

    private static function generateSignature()
    {
        $bytes = openssl_random_pseudo_bytes(self::NUM_CHARS_STRING * 4);
        $sha1_signature = sha1(bin2hex($bytes));
        return $sha1_signature;
    }

}
