<?php

namespace Hoteles\BackendBundle\Mailer;

/**
 * Description of OfertaMailer
 *
 * @author joaquin
 */
use Hoteles\BackendBundle\Entity\Oferta;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Monolog\Logger;
use Hoteles\BackendBundle\Services\LoadConfiguracionService;

class OfertaMailer
{

    /**
     * @var MailerInterface
     */
    protected $mailer;

    /**
     * @var TemplatingInterface
     */
    protected $templating;
    /*
     * @var Logger
     */
    protected $logger;

    /**
     * @var LoadConfiguracionService
     */
    protected $configuracion;

    /**
     * Constructor
     * 
     * @param ContainerInterface $mailer
     */
    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating, Logger $logger, LoadConfiguracionService $load_configuracion)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->logger = $logger;
        $this->configuracion = $load_configuracion->loadConfiguracion();
    }

    //Este es el que se envía cuando han pasado XX horas y no se ha pasado la oferta
    public function sendFirstNotice(Oferta $oferta)
    {
        $message = \Swift_Message::newInstance()
                ->setSubject('Recordatorio pendiente de pago')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($oferta->getUsuario()->getEmail())
                ->setBody(
                $this->templating->render(
                        'HotelesBackendBundle:Mail:primerAvisoOfertaAceptada.html.twig', array(
                    'oferta' => $oferta,
                    'hotel' => $oferta->getHotel(),
                    'sale' => $oferta->getSale())), 'text/html');
        $email_enviado = $this->mailer->send($message);
        if ($email_enviado === 0) {
            $this->logger->addAlert("Error al enviar a estas direcciones:" . print_r($direcciones_en_fallo, true));
        }
    }

    //Este es el mensaje que se envía cuando han pasado NN horas y no se ha pagado la oferta
    public function sendSecondNotice(Oferta $oferta)
    {
        $message = \Swift_Message::newInstance()
                ->setSubject('Último aviso de pago de oferta')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($oferta->getUsuario()->getEmail())
                ->setBody(
                $this->templating->render(
                        'HotelesBackendBundle:Mail:segundoAvisoOfertaAceptada.html.twig', array(
                    'oferta' => $oferta,
                    'hotel' => $oferta->getHotel(),
                    'sale' => $oferta->getSale())), 'text/html');
        $email_enviado = $this->mailer->send($message);
        if ($email_enviado === 0) {
            $this->logger->addAlert("Error al enviar a estas direcciones:" . print_r($direcciones_en_fallo, true));
        }
    }

    //Este es el mensage que se envía cuando un usuario NO ha pagado su oferta
    public function sendThirdNotice(Oferta $oferta)
    {
        $message_to_user = \Swift_Message::newInstance()
                ->setSubject('Oferta no pagada - Aviso de baneo')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($oferta->getUsuario()->getEmail())
                ->setBody(
                $this->templating->render(
                        'HotelesBackendBundle:Mail:tercerAvisoOfertaAceptada.html.twig', array(
                    'oferta' => $oferta,
                    'hotel' => $oferta->getHotel(),
                    'sale' => $oferta->getSale())), 'text/html');
        $email_enviado = $this->mailer->send($message_to_user, $direcciones_en_fallo);
        if ($email_enviado === 0) {
            $this->logger->addAlert("Error al enviar a estas direcciones:" . print_r($direcciones_en_fallo, true));
        }
        $email_usuarios_hotel = array_map(function($usuario_hotel) {
            return $usuario_hotel->getEmail();
        }, $oferta->getHotel()->getUserHotel()->toArray());
        $message_to_hotel = \Swift_Message::newInstance()
                ->setSubject('Aviso: OFERTA NO PAGADA')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($oferta->getHotel()->getEmail())
                ->setCc($email_usuarios_hotel)
                ->setBody(
                $this->templating->render(
                        'HotelesBackendBundle:Mail:avisoHotelOfertaNoPagada.html.twig', array(
                    'oferta' => $oferta,
                    'hotel' => $oferta->getHotel(),
                    'sale' => $oferta->getSale())), 'text/html');
        $email_enviado = $this->mailer->send($message_to_hotel, $direcciones_en_fallo);
        if ($email_enviado === 0) {
            $this->logger->addAlert("Error al enviar a estas direcciones:" . print_r($direcciones_en_fallo, true));
        }
    }

    //Esto es el correo que se envía a un usuario si ningun hotel acepta su oferta 
    public function sendOfferNotAcceptedSummary(array $ofertas)
    {
        $primera_oferta = $ofertas[0];
        $message_to_user = \Swift_Message::newInstance()
                ->setSubject('Vaya... :(')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($primera_oferta->getUsuario()->getEmail())
                ->setBody(
                $this->templating->render('HotelesBackendBundle:Mail:resumenOfertaNoAceptada.html.twig'), 'text/html');
        $email_enviado = $this->mailer->send($message_to_user, $direcciones_en_fallo);
        if ($email_enviado === 0) {
            $this->logger->addAlert("Error al enviar a estas direcciones:" . print_r($direcciones_en_fallo, true));
        }
    }

    public function sendOfferCounterOfferedSummary(array $ofertas)
    {
        $primera_oferta = $ofertas[0];
        $message_to_user = \Swift_Message::newInstance()
                ->setSubject('Resumen de tu oferta')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($primera_oferta->getUsuario()->getEmail())
                ->setBody(
                $this->templating->render(
                        'HotelesBackendBundle:Mail:resumenOfertaContraofertada.html.twig', array(
                    'ofertas' => $ofertas)), 'text/html');
        $email_enviado = $this->mailer->send($message_to_user, $direcciones_en_fallo);
        if ($email_enviado === 0) {
            $this->logger->addAlert("Error al enviar a estas direcciones:" . print_r($direcciones_en_fallo, true));
        }
    }

    /**
     * Notifico a un hotel de que un usuario no ha pagado su contraoferta
     */
    public function notifyHotelCounterOfferExpired(Oferta $oferta)
    {
        $hotel = $oferta->getHotel();
        $usuariosHotel = $hotel->getUserHotel();
        $arrayEmailUsuariosHotel = array();
        foreach ($usuariosHotel as $usuarioHotel) {
            $arrayEmailUsuariosHotel[] = $usuarioHotel->getEmail();
        }
        $message_to_hotel = \Swift_Message::newInstance()
                ->setSubject('Tu contraoferta ha caducado')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($arrayEmailUsuariosHotel)
                ->setBody(
                $this->templating->render(
                        'HotelesBackendBundle:Mail:avisoHotelContraofertaCaducada.html.twig', array(
                    'oferta' => $oferta)), 'text/html');
        $email_enviado = $this->mailer->send($message_to_hotel, $direcciones_en_fallo);
        if ($email_enviado === 0) {
            $this->logger->addAlert("Error al enviar a estas direcciones:" . print_r($direcciones_en_fallo, true));
        }
    }

    /**
     * Notifico a un hotel de que un usuario ha pagado otra contraoferta
     */
    public function notifyHotelCounterOfferRejected(Oferta $oferta)
    {
        $hotel = $oferta->getHotel();
        $usuariosHotel = $hotel->getUserHotel();
        $arrayEmailUsuariosHotel = array();
        foreach ($usuariosHotel as $usuarioHotel) {
            $arrayEmailUsuariosHotel[] = $usuarioHotel->getEmail();
        }
        $message_to_hotel = \Swift_Message::newInstance()
                ->setSubject('Ha caducado tu contraoferta')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($arrayEmailUsuariosHotel)
                ->setBody(
                $this->templating->render(
                        'HotelesBackendBundle:Mail:avisoHotelContraofertaRechazada.html.twig', array(
                    'oferta' => $oferta)), 'text/html');
        $email_enviado = $this->mailer->send($message_to_hotel, $direcciones_en_fallo);
        if ($email_enviado === 0) {
            $this->logger->addAlert("Error al enviar a estas direcciones:" . print_r($direcciones_en_fallo, true));
        }
    }

    public function notifyHotelOffer(Oferta $oferta)
    {
        $hotel = $oferta->getHotel();
        $usuariosHotel = $hotel->getUserHotel();
        $arrayEmailUsuariosHotel = array();
        foreach ($usuariosHotel as $usuarioHotel) {
            $arrayEmailUsuariosHotel[] = $usuarioHotel->getEmail();
        }
        $message = \Swift_Message::newInstance()
                ->setSubject('Un usuario está interesado en tu hotel')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($hotel->getEmail())
                ->setCc($arrayEmailUsuariosHotel)
                ->setBody(
                $this->templating->render(
                        'HotelesBackendBundle:Mail:avisoHotelOfertaRealizada.html.twig', array(
                    'hotel,' => $hotel,
                    'oferta' => $oferta,
                    'configuracion' => $this->configuracion,)), 'text/html');

        $email_enviado = $this->mailer->send($message, $direcciones_en_fallo);
        if ($email_enviado === 0) {
            $this->logger->addAlert("Error al enviar a estas direcciones:" . print_r($direcciones_en_fallo, true));
        }
    }

}
