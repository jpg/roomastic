<?php

namespace Hoteles\BackendBundle\Mailer;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * 
 */
class Mailerfosuser
{

    const SESSION_EMAIL = 'fos_user_send_resetting_email/email';

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     * 
     * @param ContainerInterface $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Send an email to a user to confirm the account creation
     *
     */
    function sendConfirmationEmailMessage($userId)
    {

        $user = $this->container->get('fos_user.user_manager')->findUserBy(array('id' => $userId));

        if (null === $user) {
            return false;
        }


        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->container->get('session')->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));

        $this->container->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->container->get('fos_user.user_manager')->updateUser($user);

        return "sendConfirmationEmailMessage";
    }

    function sendWelcomeEmailMessage($userId)
    {

        $user = $this->container->get('fos_user.user_manager')->findUserBy(array('id' => $userId));

        if (null === $user) {
            return false;
        }


        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->container->get('session')->set('fos_user_send_confirmation_email/email', $this->getObfuscatedEmail($user));

        $this->container->get('fos_user.mailer')->sendConfirmationEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->container->get('fos_user.user_manager')->updateUser($user);

        return "sendConfirmationEmailMessage";
    }

    /**
     * Send an email to a user to confirm the password reset
     *
     */
    function sendResettingEmailMessage()
    {
        return "sendResettingEmailMessage";
    }

    protected function getObfuscatedEmail($user)
    {
        $email = $user->getEmail();
        if (false !== $pos = strpos($email, '@')) {
            $email = '...' . substr($email, $pos);
        }

        return $email;
    }

}
