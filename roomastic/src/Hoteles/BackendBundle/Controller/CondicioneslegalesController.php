<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Condicioneslegales;
use Hoteles\BackendBundle\Form\CondicioneslegalesType;

/**
 * Condicioneslegales controller.
 *
 * @Route("/admin/condicioneslegales")
 */
class CondicioneslegalesController extends Controller
{

    /**
     * Lists all Condicioneslegales entities.
     *
     * @Route("/", name="admin_condicioneslegales")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:Condicioneslegales')->findAll();

        if ($entities) {
            return $this->redirect($this->generateUrl('admin_condicioneslegales_edit', array('id' => $entities[0]->getId())));
        } else {
            return $this->redirect($this->generateUrl('admin_condicioneslegales_new'));
        }

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Condicioneslegales entity.
     *
     * @Route("/{id}/show", name="admin_condicioneslegales_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Condicioneslegales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Condicioneslegales entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Condicioneslegales entity.
     *
     * @Route("/new", name="admin_condicioneslegales_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Condicioneslegales();
        $form = $this->createForm(new CondicioneslegalesType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Condicioneslegales entity.
     *
     * @Route("/create", name="admin_condicioneslegales_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Condicioneslegales:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Condicioneslegales();
        $request = $this->getRequest();
        $form = $this->createForm(new CondicioneslegalesType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_condicioneslegales_edit', array('id' => $entity->getId())));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }


        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Condicioneslegales entity.
     *
     * @Route("/{id}/edit", name="admin_condicioneslegales_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Condicioneslegales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Condicioneslegales entity.');
        }

        $editForm = $this->createForm(new CondicioneslegalesType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Condicioneslegales entity.
     *
     * @Route("/{id}/update", name="admin_condicioneslegales_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Condicioneslegales:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Condicioneslegales')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Condicioneslegales entity.');
        }

        $editForm = $this->createForm(new CondicioneslegalesType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Editado correctamente.');

            return $this->redirect($this->generateUrl('admin_condicioneslegales_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo editar.');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Condicioneslegales entity.
     *
     * @Route("/{id}/delete", name="admin_condicioneslegales_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Condicioneslegales')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Condicioneslegales entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Borrado correctamente.');
        }

        return $this->redirect($this->generateUrl('admin_condicioneslegales'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
