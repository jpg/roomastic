<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Imagenhotel;
use Hoteles\BackendBundle\Imagenes\Gd;
use Symfony\Component\HttpFoundation\Request;

class ImageuploadController extends Controller
{

    /**
     * @Route("/admin/imageupload/{tiposubida}", name="admin_imageupload", options={"expose"=true})
     * @Template()
     */
    public function indexAction($tiposubida, Request $request)
    {
        //echo "<pre>";
        //var_dump($_FILES);
        //echo $this->get('kernel')->getRootDir()."/../web/uploads/bannershort/";
        if ($request->getMethod() === 'POST') {
            $tamano = $_FILES["archivo"]['size'];
            $tipo = $_FILES["archivo"]['type'];
            $archivo = $_FILES["archivo"]['name'];
            $prefijo = substr(md5(uniqid(rand())), 0, 6);
            if ($archivo != "") {
                // guardamos el archivo a la carpeta files
                $destino = $this->get('kernel')->getRootDir() . "/../web/uploads/temporal/" . $prefijo . "_" . $archivo;
                if (copy($_FILES['archivo']['tmp_name'], $destino)) {
                    $status = "Archivo subido: <b>" . $archivo . "</b>";
                } else {
                    $status = "Error al subir el archivo";
                }
            } else {
                $status = "Error al subir archivo";
            }
            echo $status;

            if ($tiposubida == 'hoteles') {

                $imagen = new Imagenhotel();
                $imagen->setImagen($prefijo . "_" . $archivo);
                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($imagen);
                $em->flush();
                echo '<script>window.parent.inserta(' . $imagen->getId() . ',"' . $prefijo . "_" . $archivo . '")</script>';
            } else {
                echo '<script>window.parent.inserta(' . '1' . ',"' . $prefijo . "_" . $archivo . '")</script>';
            }

            echo "string";
        }

        echo "imagen upload";
        exit;
        return array();
    }

    /**
     * @Route("/admin/cropimagen/{imagen}/{posx}/{posy}/{width}/{height}/{carpeta}", name="admin_imagecrop", options={"expose"=true})
     */
    public function cropAction($imagen, $posx, $posy, $width, $height, $carpeta)
    {

        $imagen_local = str_replace('___---___', '.', $imagen);

        $filename = $this->get('kernel')->getRootDir() . "/../web/uploads/temporal/" . $imagen_local;
        list($w, $h, $type, $attr) = getimagesize($filename);

        if (exif_imagetype($filename) == IMAGETYPE_GIF) {
            $src_im = imagecreatefromgif($filename);
        } elseif (exif_imagetype($filename) == IMAGETYPE_JPEG) {
            $src_im = imagecreatefromjpeg($filename);
        } elseif (exif_imagetype($filename) == IMAGETYPE_PNG) {
            $src_im = imagecreatefrompng($filename);
        }

        $src_x = $posx;   // comienza x 
        $src_y = $posy;   // comienza y 
        $src_w = $width; // ancho 
        $src_h = $height; // alto 
        $dst_x = '0';   // termina x 
        $dst_y = '0';   // termina y 

        $dst_im = imagecreatetruecolor($src_w, $src_h);
        $white = imagecolorallocate($dst_im, 255, 255, 255);
        imagefill($dst_im, 0, 0, $white);

        imagecopy($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h);

        if (exif_imagetype($filename) == IMAGETYPE_GIF) {
            imagegif($dst_im, $this->get('kernel')->getRootDir() . "/../web/uploads/" . $carpeta . "/" . $imagen_local, 100);
            //header("Content-type: image/gif"); 
            //imagegif($dst_im); 
        } elseif (exif_imagetype($filename) == IMAGETYPE_JPEG) {
            imagejpeg($dst_im, $this->get('kernel')->getRootDir() . "/../web/uploads/" . $carpeta . "/" . $imagen_local, 100);
            //header("Content-type: image/jpeg"); 
            //imagejpeg($dst_im); 
        } elseif (exif_imagetype($filename) == IMAGETYPE_PNG) {
            imagepng($dst_im, $this->get('kernel')->getRootDir() . "/../web/uploads/" . $carpeta . "/" . $imagen_local, 9);
            //header("Content-type: image/png"); 
            //imagepng($dst_im); 
        }
        $respuesta = array();
        $respuesta['tipo'] = 'success';


        return $this->get("backend_utilities.json_response")->JSONResponse($respuesta);
    }

    /**
     * @Route("/admin/deletetemp/{imagen}", options={"expose"=true})
     */
    public function deleteTempFileAction($imagen)
    {
        $imagen_local = str_replace('___---___', '.', $imagen);
        $filename = $this->get('kernel')->getRootDir() . "/../web/uploads/temporal/" . $imagen_local;
        unlink($filename);
        $respuesta = array();
        $respuesta['tipo'] = 'success';
        return $this->get("backend_utilities.json_response")->JSONResponse($respuesta);
    }

}
