<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Controller;

use Hoteles\BackendBundle\Entity\CronTask;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/crontasks")
 */
class CronTaskController extends Controller
{

    /**
     * @Route("/fixture")
     */
    public function fixtureAction()
    {
        //Borro todas las tareas
        $em = $this->getDoctrine()->getEntityManager();
        $query = $em->createQuery('DELETE HotelesBackendBundle:CronTask c ');
        $query->execute();
        $em->flush();
        $num_minutos_periodo = 1;
        $num_segundos_periodo = 6;

        $task_list = array();
        $offerNoticeTask = new CronTask();
        $offerNoticeTask
                ->setName('Enviar e-mails de aviso de oferta aceptada')
                ->setInterval($num_minutos_periodo * $num_segundos_periodo) // Cada 5 minutos/300 segundos
                ->setCommands(array(
                    'hoteles:offer:notice --verbose'));
        $task_list[] = $offerNoticeTask;

        $expireCounterOffers = new CronTask();
        $expireCounterOffers
                ->setName('Caduca las contraofertas no pagadas')
                ->setInterval($num_minutos_periodo * $num_segundos_periodo) // Cada 5 minutos/300 segundos
                ->setCommands(array(
                    'hoteles:counteroffer:expire --verbose'));
        $task_list[] = $expireCounterOffers;

        $changeOfferStatus = new CronTask();
        $changeOfferStatus
                ->setName('Caduca las ofertas aceptadas y no pagadas')
                ->setInterval($num_minutos_periodo * $num_segundos_periodo) // Cada 5 minutos/300 segundos
                ->setCommands(array(
                    'hoteles:offer:expire --verbose'));
        $task_list[] = $changeOfferStatus;

        $counterOfferNotice = new CronTask();
        $counterOfferNotice
                ->setName('Notifica las contraofertas realizadas')
                ->setInterval($num_minutos_periodo * $num_segundos_periodo) // Cada 5 minutos/300 segundos
                ->setCommands(array(
                    'hoteles:counteroffer:notice --verbose'));
        $task_list[] = $counterOfferNotice;

        $enviar_emails_pendientes = new CronTask();
        $enviar_emails_pendientes
                ->setName('Envia los e-mails que haya en el spool')
                ->setInterval($num_minutos_periodo * $num_segundos_periodo) // Cada 5 minutos/300 segundos
                ->setCommands(array(
                    'swiftmailer:spool:send --message-limit=20 --env=prod'));
        $task_list[] = $enviar_emails_pendientes;


        foreach ($task_list as $task) {
            $em->persist($task);
        }

        $em->flush();

        return new Response('OK!');
    }

}
