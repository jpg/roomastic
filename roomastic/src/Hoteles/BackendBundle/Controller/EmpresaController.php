<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Empresa;
use Hoteles\BackendBundle\Entity\UserEmpresa;
use Hoteles\BackendBundle\Form\EmpresaType;
use Symfony\Component\Form\FormError;

/**
 * Empresa controller.
 *
 * @Route("/admin/empresa")
 */
class EmpresaController extends Controller
{

    /**
     * Lists all Empresa entities.
     *
     * @Route("/", name="admin_empresa")
     * @Template()
     */
    public function indexAction()
    {


        $em = $this->getDoctrine()->getEntityManager();
        $securityContext = $this->container->get('security.context');

        $filtro = array();
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $filtro = array('id' => $this->obtieneEmpresa($em, $securityContext->getToken()->getUser()->getId()));
        }
        $empresas = $em->getRepository('HotelesBackendBundle:Empresa')->findBy($filtro);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $empresas,
                /* page number */ $this->get('request')->query->get('page', 1),
                /* limit per page */ $this->get('request')->query->get('show', $this->container->getParameter('paginacionpordefecto'))
        );

        return array(
            'entities' => $empresas,
            'pagination' => $pagination,
        );
    }

    public function obtieneEmpresa($em, $userId)
    {

        $userEmpresa = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($userId);
        return $userEmpresa->getEmpresa()->getId();
    }

    /**
     * Finds and displays a Empresa entity.
     *
     * @Route("/{id}/show", name="admin_empresa_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Empresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Empresa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Empresa entity.
     *
     * @Route("/new", name="admin_empresa_new")
     * @Template()
     */
    public function newAction()
    {
        $empresa = new Empresa();
        $configuracion = $this->get('hoteles_backend.load_configuracion')->loadConfiguracion();
        $empresa->setComisionEmpresaEntero($configuracion->getDescuentoentero());
        $empresa->setComisionEmpresaDecimal($configuracion->getDescuentodecimal());
        $securityContext = $this->container->get('security.context');

        $form = $this->createForm(new EmpresaType(), $empresa, array('admin' => $securityContext->isGranted('ROLE_ADMIN')));
        return array(
            'entity' => $empresa,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Empresa entity.
     *
     * @Route("/create", name="admin_empresa_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Empresa:new.html.twig")
     */
    public function createAction()
    {
        $empresa = new Empresa();
        $request = $this->getRequest();
        $securityContext = $this->container->get('security.context');
        $form = $this->createForm(new EmpresaType(), $empresa, array('admin' => $securityContext->isGranted('ROLE_ADMIN')));
        $form->bindRequest($request);

        $filtro = $this->get('hoteles_backend.funcionalidades');

        if ($filtro->compruebaEmail($form['emailpersonacontacto']->getData())) {
            $error = new formerror("Ese email está en uso");
            $form->get('emailpersonacontacto')->addError($error);
        }

        if ($filtro->compruebaUsername($form['emailpersonacontacto']->getData())) {
            $error = new formerror("Ese username está en uso");
            $form->get('emailpersonacontacto')->addError($error);
        }

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($empresa);
            $em->flush();
            $this->creaPrimerUsuario($empresa, $em);

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_empresa_edit', array('id' => $empresa->getId())));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }

        return array(
            'entity' => $empresa,
            'form' => $form->createView()
        );
    }

    public function creaPrimerUsuario(Empresa $empresa, $em)
    {

        $nuevo_usuario_empresa = new UserEmpresa();
        $nuevo_usuario_empresa->setUsername($empresa->getEmailpersonacontacto());
        $nuevo_usuario_empresa->setEmail($empresa->getEmailpersonacontacto());
        $nuevo_usuario_empresa->setNombreCompleto($empresa->getNombrepersonacontacto() . " " . +$empresa->getApellidospersonacontacto());
        $nuevo_usuario_empresa->setPassword(md5(microtime()));
        $nuevo_usuario_empresa->setRoles(array('ROLE_EMPRESA'));
        $nuevo_usuario_empresa->setEmpresa($empresa);
        //$nuevoUsuario->setImagen('a');
        $em->persist($nuevo_usuario_empresa);
        $em->flush();

        // enviar mail registro
        $this->get('hoteles_backend.mailerfosuser')->sendConfirmationEmailMessage($nuevo_usuario_empresa->getId());

        return true;
    }

    /**
     * Displays a form to edit an existing Empresa entity.
     *
     * @Route("/{id}/edit", name="admin_empresa_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $empresa = $em->getRepository('HotelesBackendBundle:Empresa')->find($id);

        if (!$empresa) {
            throw $this->createNotFoundException('Unable to find Empresa entity.');
        }

        $securityContext = $this->container->get('security.context');
        $editForm = $this->createForm(new EmpresaType(), $empresa, array('admin' => $securityContext->isGranted('ROLE_ADMIN')));
        $deleteForm = $this->createDeleteForm($id);
        $reactivateForm = $this->createReactivateForm($id);
        return array(
            'entity' => $empresa,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'reactivate_form' => $reactivateForm->createView()
        );
    }

    /**
     * Edits an existing Empresa entity.
     *
     * @Route("/{id}/update", name="admin_empresa_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Empresa:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Empresa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Empresa entity.');
        }
        $securityContext = $this->container->get('security.context');
        $admin = $securityContext->isGranted('ROLE_ADMIN');


        $editForm = $this->createForm(new EmpresaType(), $entity, array('admin' => $admin));
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        $filtro = $this->get('hoteles_backend.funcionalidades');

        $userPrimero = $filtro->sacaUserDesdeMail($entity->getEmailpersonacontacto());

        if ($filtro->compruebaEmail($editForm['emailpersonacontacto']->getData(), $userPrimero)) {
            $error = new formerror("Ese email está en uso");
            $editForm->get('emailpersonacontacto')->addError($error);
        }

        if ($filtro->compruebaUsername($editForm['emailpersonacontacto']->getData(), $userPrimero)) {
            $error = new formerror("Ese username está en uso");
            $editForm->get('emailpersonacontacto')->addError($error);
        }

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_empresa_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Empresa entity.
     *
     * @Route("/{id}/delete", name="admin_empresa_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $empresa = $em->getRepository('HotelesBackendBundle:Empresa')->find($id);

            if (!$empresa) {
                $this->get('session')->setFlash('nosuccess', 'Empresa no encontrada.');
                return $this->forward('HotelesBackendBundle:Empresa:index');
            }

            $securityContext = $this->container->get('security.context');
            if (FALSE === $securityContext->isGranted('ROLE_ADMIN')) {
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->forward('HotelesBackendBundle:Empresa:index');
            }

            foreach ($empresa->getUserEmpresa() as $user_empresa) {
                //Suspendo las cuentas de todos los usuarios empresas de esta empresa
                $user_empresa->suspendUser();
                $em->persist($user_empresa);
            }
            $oferta_manager = $this->get('hoteles_backend.oferta_manager');
            foreach ($empresa->getHoteles() as $hotel) {
                foreach ($hotel->getUserHotel() as $user_hotel) {
                    $user_hotel->suspendUser();
                    $em->persist($user_hotel);
                }
                $hotel->suspendHotel();
                $ofertas_pendientes = $em->getRepository('HotelesBackendBundle:Oferta')->getOffersPendingUserAction($hotel);
                foreach ($ofertas_pendientes as $oferta_pendiente) {
                    $oferta_manager->updateOfferState('hotel_suspenso', $oferta_pendiente);
                    $em->persist($oferta_pendiente);
                }
                $em->persist($hotel);
            }
            $empresa->suspendEmpresa();
            //Busco todas las ofertas en las que este implicado el hotel y las mando a un estado "HOTEL SUSPENSO"

            $em->persist($empresa);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_empresa'));
    }

    /**
     * Deletes a Empresa entity.
     *
     * @Route("/{id}/reactivate", name="admin_empresa_reactivate")
     * @Method("post")
     */
    public function reactivateAction($id)
    {
        $form = $this->createReactivateForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $empresa = $em->getRepository('HotelesBackendBundle:Empresa')->find($id);

            if (!$empresa) {
                $this->get('session')->setFlash('nosuccess', 'Empresa no encontrada.');
                return $this->forward('HotelesBackendBundle:Empresa:index');
            }

            $securityContext = $this->container->get('security.context');
            if (FALSE === $securityContext->isGranted('ROLE_ADMIN')) {
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->forward('HotelesBackendBundle:Empresa:index');
            }
            //Hay que definir que acción queremos para esto
            foreach ($empresa->getUserEmpresa() as $user_empresa) {
                //Suspendo las cuentas de todos los usuarios empresas de esta empresa
                $user_empresa->unsuspendUser();
                $em->persist($user_empresa);
            }
            foreach ($empresa->getHoteles() as $hotel) {
                foreach ($hotel->getUserHotel() as $user_hotel) {
                    $user_hotel->unsuspendUser();
                    $em->persist($user_hotel);
                }
                $hotel->unsuspendHotel();
                $em->persist($hotel);
            }
            $empresa->unsuspendEmpresa();
            $em->persist($empresa);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_empresa'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

    private function createReactivateForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
