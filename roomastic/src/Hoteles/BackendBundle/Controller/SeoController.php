<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Seo;
use Hoteles\BackendBundle\Form\SeoType;

/**
 * Seo controller.
 *
 * @Route("/admin/seo")
 */
class SeoController extends Controller
{

    /**
     * Lists all Seo entities.
     *
     * @Route("/", name="admin_seo")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:Seo')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Seo entity.
     *
     * @Route("/{id}/show", name="admin_seo_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Seo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Seo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Seo entity.
     *
     * @Route("/new", name="admin_seo_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Seo();
        $form = $this->createForm(new SeoType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Seo entity.
     *
     * @Route("/create", name="admin_seo_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Seo:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Seo();
        $request = $this->getRequest();
        $form = $this->createForm(new SeoType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_seo_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Seo entity.
     *
     * @Route("/{id}/edit", name="admin_seo_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Seo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Seo entity.');
        }

        $editForm = $this->createForm(new SeoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Seo entity.
     *
     * @Route("/{id}/update", name="admin_seo_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Seo:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Seo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Seo entity.');
        }

        $editForm = $this->createForm(new SeoType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_seo_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Seo entity.
     *
     * @Route("/{id}/delete", name="admin_seo_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Seo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Seo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_seo'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
