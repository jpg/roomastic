<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\UserHotel;
use Hoteles\BackendBundle\Form\UserHotelType;
use Symfony\Component\Form\FormError;

/**
 * UserHotel controller.
 *
 * @Route("/admin/userhotel")
 */
class UserHotelController extends Controller
{

    /**
     * Lists all UserHotel entities.
     *
     * @Route("/", name="admin_userhotel")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $securityContext = $this->container->get('security.context');

        $idUser = $securityContext->getToken()->getUser()->getId();
        $query = $this->getDoctrine()->getRepository('HotelesBackendBundle:UserHotel')->createQueryBuilder('u');

        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $user = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($idUser);
            $idEmpresa = $user->getEmpresa()->getId();
            $query->leftJoin('u.hotel', 'h')
                    ->leftJoin('h.empresa', 'e')
                    ->where('e.id = :idEmpresa')
                    ->setParameter('idEmpresa', $idEmpresa);
        } elseif ($securityContext->isGranted('ROLE_HOTEL')) {
            $user = $em->getRepository('HotelesBackendBundle:UserHotel')->find($idUser);
            $idHotel = $user->getHotel()->getId();
            $query->where('u.hotel = :idHotel')->setParameter('idHotel', $idHotel);
        }

        $user_hoteles = $query->getQuery()->getResult();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $user_hoteles,
                /* page number */ $this->get('request')->query->get('page', 1),
                /* resultados por pagina */ $this->get('request')->query->get('show', $this->container->getParameter('paginacionpordefecto'))
        );

        return array(
            'entities' => $user_hoteles,
            'pagination' => $pagination,
        );
    }

    /**
     * Finds and displays a UserHotel entity.
     *
     * @Route("/{id}/show", name="admin_userhotel_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $user_hotel = $em->getRepository('HotelesBackendBundle:UserHotel')->find($id);

        if (!$user_hotel) {
            $this->get('session')->setFlash('nosuccess', 'Usuario hotel no encontrado.');
            return $this->redirect($this->generateUrl("admin_hotel"));
        }

        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $empresa_user_actual = $securityContext->getToken()->getUser()->getEmpresa();
            if ($empresa_user_actual !== $user_hotel->getEmpresa()) {
                //Un usuario empresa esta intentando ver el perfil de un usuario empresa de otra empresa
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->redirect($this->generateUrl("admin_hotel"));
            }
        } else if ($securityContext->isGranted('ROLE_HOTEL')) {
            $hotel_usuario_actual = $securityContext->getToken()->getUser()->getHotel();
            if ($hotel_usuario_actual !== $user_hotel->getHotel()) {
                //Un usuario hotel esta intentando ver el perfil de un usuario hotel de otro hotel
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->redirect($this->generateUrl("admin_hotel"));
            }
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $user_hotel,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new UserHotel entity.
     *
     * @Route("/new", name="admin_userhotel_new")
     * @Template()
     */
    public function newAction()
    {
        $user_hotel = new UserHotel();
        $form = $this->generaFormularioDinamico($user_hotel)->getForm();
        $securityContext = $this->container->get('security.context');
        //Compruebo si el usuario puede ver el campo de crear usuario
        if ($securityContext->isGranted('ROLE_EMPRESA') ||
                $securityContext->isGranted('ROLE_ADMIN') || (
                $securityContext->isGranted('ROLE_HOTEL') &&
                $securityContext->getToken()->getUser()->isAdmin()
                )) {
            $show_admin = true;
        } else {
            $show_admin = false;
        }
        return array(
            'entity' => $user_hotel,
            'form' => $form->createView(),
            'imagendevuelta' => '',
            'show_admin' => $show_admin
        );
    }

    /**
     * Creates a new UserHotel entity.
     *
     * @Route("/create", name="admin_userhotel_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:UserHotel:new.html.twig")
     */
    public function createAction()
    {
        $user_hotel = new UserHotel();
        $request = $this->getRequest();
        //$form    = $this->createForm(new UserHotelType(), $entity);
        $form = $this->generaFormularioDinamico($user_hotel)->getForm();
        $form->bindRequest($request);

        $filtro = $this->get('hoteles_backend.funcionalidades');

        if ($filtro->compruebaEmail($form['email']->getData())) {
            $error = new formerror("Ese email está en uso");
            $form->get('email')->addError($error);
        }

        if ($filtro->compruebaUsername($form['email']->getData())) {
            $error = new formerror("Ese username está en uso");
            $form->get('username')->addError($error);
        }
        $securityContext = $this->container->get('security.context');

        if ($form->isValid()) {

            if (!isset($form["administrador"])) {
                $user_hotel->setAdministrador(FALSE);
            } else if ($securityContext->isGranted('ROLE_EMPRESA') ||
                    $securityContext->isGranted('ROLE_ADMIN') || (
                    $securityContext->isGranted('ROLE_HOTEL') &&
                    $securityContext->getToken()->getUser()->isAdmin()
                    )) {
                $user_hotel->setAdministrador(TRUE);
            }
            $user_hotel->setImagen($_POST["imagensubidanombre"]);
            $user_hotel->setUsername($user_hotel->getEmail());
            $user_hotel->setEmail($user_hotel->getEmail());
            $user_hotel->setPassword(md5(microtime()));
            $user_hotel->setRoles(array('ROLE_HOTEL'));
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($user_hotel);
            $em->flush();

            //Envio e-mail para cambiar contraseña
            $this->get('hoteles_backend.mailerfosuser')->sendWelcomeEmailMessage($user_hotel->getId());


            $this->get('session')->setFlash('success', 'Creado correctamente. Se ha enviado un mail a ' . $user_hotel->getEmail());

            //$this->get('session')->setFlash('success','Creado correctamente.'); 

            return $this->redirect($this->generateUrl('admin_userhotel_edit', array('id' => $user_hotel->getId())));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
            $errors = $form->getErrors();
        }
//Compruebo si el usuario puede ver el campo de crear usuario
        if ($securityContext->isGranted('ROLE_EMPRESA') ||
                $securityContext->isGranted('ROLE_ADMIN') || (
                $securityContext->isGranted('ROLE_HOTEL') &&
                $securityContext->getToken()->getUser()->isAdmin())) {
            $show_admin = true;
        } else {
            $show_admin = false;
        }
        return array(
            'entity' => $user_hotel,
            'form' => $form->createView(),
            'imagendevuelta' => $_POST["imagensubidanombre"],
            'show_admin' => $show_admin
        );
    }

    /**
     * Displays a form to edit an existing UserHotel entity.
     *
     * @Route("/{id}/edit", name="admin_userhotel_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $user_hotel = $em->getRepository('HotelesBackendBundle:UserHotel')->find($id);

        if (!$user_hotel) {
            $this->get('session')->setFlash('nosuccess', 'Usuario empresa no encontrado.');
            return $this->redirect($this->generateUrl("admin_hotel"));
        }

        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $empresa_user_actual = $securityContext->getToken()->getUser()->getEmpresa();
            if ($empresa_user_actual !== $user_hotel->getHotel()->getEmpresa()) {
                //Un usuario empresa esta intentando editar el perfil de un usuario empresa de otra empresa
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->redirect($this->generateUrl("admin_hotel"));
            }
        } else if ($securityContext->isGranted('ROLE_HOTEL')) {
            $hotel_usuario_actual = $securityContext->getToken()->getUser()->getHotel();
            if ($hotel_usuario_actual !== $user_hotel->getHotel()) {
                //Un usuario hotel esta intentando ver el perfil de un usuario hotel de otro hotel
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->redirect($this->generateUrl("admin_hotel"));
            }
        }
        //$editForm = $this->createForm(new UserHotelType(), $entity);
        $editForm = $this->generaFormularioDinamico($user_hotel)->getForm();

        $deleteForm = $this->createDeleteForm($id);
        //Compruebo si el usuario puede ver el campo de crear usuario
        if ($securityContext->isGranted('ROLE_EMPRESA') ||
                $securityContext->isGranted('ROLE_ADMIN') || (
                $securityContext->isGranted('ROLE_HOTEL') &&
                $securityContext->getToken()->getUser()->isAdmin())) {
            $show_admin = true;
        } else {
            $show_admin = false;
        }
        $reactivateForm = $this->createReactivateForm($id);

        return array(
            'entity' => $user_hotel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'reactivate_form' => $reactivateForm->createView(),
            'imagendevuelta' => $user_hotel->getImagen(),
            'show_admin' => $show_admin
        );
    }

    /**
     * Edits an existing UserHotel entity.
     *
     * @Route("/{id}/update", name="admin_userhotel_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:UserHotel:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $user_hotel = $em->getRepository('HotelesBackendBundle:UserHotel')->find($id);

        if (!$user_hotel) {
            $this->get('session')->setFlash('nosuccess', 'Usuario empresa no encontrado.');
            return $this->forward('HotelesBackendBundle:Hotel:index');
        }

        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $empresa_user_actual = $securityContext->getToken()->getUser()->getEmpresa();
            if ($empresa_user_actual !== $user_hotel->getEmpresa()) {
                //Un usuario empresa esta intentando actualizar el perfil de un usuario empresa de otra empresa
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->forward('HotelesBackendBundle:Hotel:index');
            }
        } else if ($securityContext->isGranted('ROLE_HOTEL')) {
            $hotel_usuario_actual = $securityContext->getToken()->getUser()->getHotel();
            if ($hotel_usuario_actual !== $user_hotel->getHotel()) {
                //Un usuario hotel esta intentando ver el perfil de un usuario hotel de otro hotel
                $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                return $this->forward('HotelesBackendBundle:Hotel:index');
            }
        }

        $editForm = $this->generaFormularioDinamico($user_hotel)->getForm();
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        $filtro = $this->get('hoteles_backend.funcionalidades');

        if ($filtro->compruebaEmail($editForm['email']->getData(), $user_hotel)) {
            $error = new formerror("Ese email está en uso");
            $editForm->get('email')->addError($error);
        }

        if ($filtro->compruebaUsername($editForm['email']->getData(), $user_hotel)) {
            $error = new formerror("Ese username está en uso");
            $editForm->get('username')->addError($error);
        }

        if ($editForm->isValid()) {

            $user_hotel->setImagen($_POST["imagensubidanombre"]);
            $em->persist($user_hotel);
            $em->flush();

            $this->get('session')->setFlash('success', 'Editado correctamente.');

            return $this->redirect($this->generateUrl('admin_userhotel_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo editar.');
        }

        return array(
            'entity' => $user_hotel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a UserHotel entity.
     *
     * @Route("/{id}/delete", name="admin_userhotel_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $user_hotel = $em->getRepository('HotelesBackendBundle:UserHotel')->find($id);

            if (!$user_hotel) {
                throw $this->createNotFoundException('Unable to find UserHotel entity.');
            }

            //Compruebo los permisos
            $securityContext = $this->container->get('security.context');
            if ($securityContext->isGranted('ROLE_EMPRESA')) {
                $empresa_user_actual = $securityContext->getToken()->getUser()->getEmpresa();
                if ($empresa_user_actual !== $user_hotel->getEmpresa()) {
                    //Un usuario empresa esta intentando suspender el perfil de un usuario hotel de otra empresa
                    $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                    return $this->forward('HotelesBackendBundle:Hotel:index');
                }
            } else if ($securityContext->isGranted('ROLE_HOTEL')) {
                $hotel_usuario_actual = $securityContext->getToken()->getUser()->getHotel();
                if ($hotel_usuario_actual !== $user_hotel->getHotel()) {
                    //Un usuario hotel esta intentando suspender el perfil de un usuario hotel de otro hotel
                    $this->get('session')->setFlash('nosuccess', 'No tienes permiso para esto.');
                    return $this->forward('HotelesBackendBundle:Hotel:index');
                }
            }
            $user_hotel->suspendUser();
            $em->persist($user_hotel);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_userhotel'));
    }

    /**
     * Deletes a UserHotel entity.
     *
     * @Route("/{id}/reactivate", name="admin_userhotel_reactivate")
     * @Method("post")
     */
    public function reactivateUserAction($id)
    {
        $form = $this->createReactivateForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $user_hotel = $em->getRepository('HotelesBackendBundle:UserHotel')->find($id);

            if (!$user_hotel) {
                throw $this->createNotFoundException('Unable to find UserHotel entity.');
            }
            //Compruebo los permisos
            $securityContext = $this->container->get('security.context');
            if ($securityContext->isGranted('ROLE_ADMIN')) {
                $user_hotel->unsuspendUser();
                $em->persist($user_hotel);
                $em->flush();
            } else {
                throw $this->createNotFoundException('No tienes permiso...');
            }
        }
        return $this->redirect($this->generateUrl('admin_userhotel'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm();
    }

    private function createReactivateForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

    public function generaFormularioDinamico($user_hotel)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $securityContext = $this->container->get('security.context');

        $form = $this->createFormBuilder($user_hotel);
        $idUser = $securityContext->getToken()->getUser()->getId();

        $query_user = $this->getDoctrine()->getRepository('HotelesBackendBundle:Hotel')->createQueryBuilder('h');

        if ($securityContext->isGranted('ROLE_EMPRESA')) {
            $user = $em->getRepository('HotelesBackendBundle:UserEmpresa')->find($idUser);
            $idEmpresa = $user->getEmpresa()->getId();
            $query_user
                    ->leftJoin('h.empresa', 'e')
                    ->where('e.id = :idEmpresa')
                    ->setParameter('idEmpresa', $idEmpresa);
        } elseif ($securityContext->isGranted('ROLE_HOTEL')) {
            $user = $em->getRepository('HotelesBackendBundle:UserHotel')->find($idUser);
            $idHotel = $user->getHotel()->getId();
            $query_user->where('h.id = :idHotel')->setParameter('idHotel', $idHotel);
        }

        if ($securityContext->isGranted('ROLE_EMPRESA') ||
                $securityContext->isGranted('ROLE_ADMIN') || (
                $securityContext->isGranted('ROLE_HOTEL') &&
                $securityContext->getToken()->getUser()->isAdmin()
                )) {
            $form->add('hotel', 'entity', array(
                        'class' => 'HotelesBackendBundle:Hotel',
                        'query_builder' => $query_user,
                    ))
                    ->add('email')
                    ->add('nombrecompleto')
                    ->add('administrador', 'checkbox', array(
                        'value' => 1,
                        'required' => false
                    ))->add('cargo');
        } else {
            $form->add('hotel', 'entity', array(
                        'class' => 'HotelesBackendBundle:Hotel',
                        'query_builder' => $query_user,
                    ))
                    ->add('email')
                    ->add('nombrecompleto');
        }

        return $form;
    }

}
