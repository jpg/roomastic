<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Quienessomos;
use Hoteles\BackendBundle\Form\QuienessomosType;

/**
 * Quienessomos controller.
 *
 * @Route("/admin/quienessomos")
 */
class QuienessomosController extends Controller
{

    /**
     * Lists all Quienessomos entities.
     *
     * @Route("/", name="admin_quienessomos")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:Quienessomos')->findAll();

        if ($entities) {
            return $this->redirect($this->generateUrl('admin_quienessomos_edit', array('id' => $entities[0]->getId())));
        } else {
            return $this->redirect($this->generateUrl('admin_quienessomos_new'));
        }

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Quienessomos entity.
     *
     * @Route("/{id}/show", name="admin_quienessomos_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Quienessomos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Quienessomos entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Quienessomos entity.
     *
     * @Route("/new", name="admin_quienessomos_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Quienessomos();
        $form = $this->createForm(new QuienessomosType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Quienessomos entity.
     *
     * @Route("/create", name="admin_quienessomos_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Quienessomos:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Quienessomos();
        $request = $this->getRequest();
        $form = $this->createForm(new QuienessomosType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_quienessomos_edit', array('id' => $entity->getId())));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Quienessomos entity.
     *
     * @Route("/{id}/edit", name="admin_quienessomos_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Quienessomos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Quienessomos entity.');
        }

        $editForm = $this->createForm(new QuienessomosType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Quienessomos entity.
     *
     * @Route("/{id}/update", name="admin_quienessomos_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Quienessomos:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Quienessomos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Quienessomos entity.');
        }

        $editForm = $this->createForm(new QuienessomosType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_quienessomos_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo editar.');
        }


        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Quienessomos entity.
     *
     * @Route("/{id}/delete", name="admin_quienessomos_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Quienessomos')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Quienessomos entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Borrado correctamente.');
        }

        return $this->redirect($this->generateUrl('admin_quienessomos'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
