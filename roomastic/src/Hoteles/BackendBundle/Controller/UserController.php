<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\User;
use Hoteles\BackendBundle\Entity\UserEmpresa;
use Hoteles\BackendBundle\Form\UserType;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * User controller.
 *
 * @Route("/admin/usuario")
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     * @Route("/", name="admin_usuario")
     * @Template()
     */
    public function indexAction()
    {

        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:User')->findAll();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $this->get('request')->query->get('page', 1)/* page number */, $this->get('request')->query->get('show', $this->container->getParameter('paginacionpordefecto'))/* limit per page */
        );

        return array(
            'entities' => $entities,
            'pagination' => $pagination,
        );
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}/show", name="admin_usuario_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $user = $em->getRepository('HotelesBackendBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'user' => $user,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="admin_usuario_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createForm(new UserType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/create", name="admin_usuario_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:User:new.html.twig")
     */
    public function createAction()
    {
        $user = new User();
        $request = $this->getRequest();
        $form = $this->createForm(new UserType(), $user);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_usuario_show', array('id' => $user->getId())));
        }

        return array(
            'entity' => $user,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="admin_usuario_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $user = $em->getRepository('HotelesBackendBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createForm(new UserType(), $user);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}/update", name="admin_usuario_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:User:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $user = $em->getRepository('HotelesBackendBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createForm(new UserType(), $user);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_usuario_edit', array('id' => $id)));
        }

        return array(
            'entity' => $user,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}/delete", name="admin_usuario_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $user = $em->getRepository('HotelesBackendBundle:User')->find($id);

            if (!$user) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($user);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_usuario'));
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}/delete", name="admin_usuario_deleteget")
     * @Method("get")
     */
    public function deletegetAction($id)
    {

        $request = $this->getRequest();

        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('HotelesBackendBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $em->remove($user);
        $em->flush();

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

    /**
     * Enabled User entity.
     *
     * @Route("/{id}/enabled", name="admin_usuario_enabled")
     */
    public function enabledAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('HotelesBackendBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $user->unsuspendUser();
        $em->persist($user);
        $em->flush();

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    /**
     * Disabled User entity.
     *
     * @Route("/{id}/disabled", name="admin_usuario_disabled")
     */
    public function disabledAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('HotelesBackendBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $user->suspendUser();
        $em->persist($user);
        $em->flush();

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    /**
     * Enabled User entity.
     *
     * @Route("/{id}/ban", name="admin_usuario_ban")
     */
    public function banAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('HotelesBackendBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $user->banUser();
        $em->persist($user);
        $em->flush();

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    /**
     * Disabled User entity.
     *
     * @Route("/{id}/unban", name="admin_usuario_unban")
     */
    public function unbanAction($id)
    {
        $request = $this->getRequest();
        $em = $this->getDoctrine()->getEntityManager();
        $user = $em->getRepository('HotelesBackendBundle:User')->find($id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $user->unbanUser();
        $em->persist($user);
        $em->flush();

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

}
