<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Hoteles\BackendBundle\Entity\Hotel;
use Hoteles\BackendBundle\Entity\UserHotel;
use Hoteles\BackendBundle\Form\HotelType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of NotificacionController
 *
 * @Route("/notificaciones")
 */
class NotificacionController extends Controller
{
    //put your code here

    /**
     * @Route("/update", options={"expose"=true})
     * @Method({"GET"})
     */
    public function updateCountersAction(Request $request)
    {
        $resultado = array();
        $securityContext = $this->get("security.context");
        $notificaciones = $securityContext->getToken()->getUser()->getHotel()->getNotificaciones();
        $json_response = $this->get("backend_utilities.json_response");
        if (is_null($notificaciones)) {
            $resultado['sin_notificaciones'] = true;
            return $json_response->JSONResponse($resultado);
        } else {
            $resultado['ofertas_no_pagadas'] = $notificaciones->getOfertasNoPagadas();
            $resultado['ofertas_pendientes'] = $notificaciones->getOfertasPendientes();
            $resultado['ofertas_pagadas'] = $notificaciones->getOfertasPagadas();
            $resultado['contraofertas_perdidas'] = $notificaciones->getContraofertasPerdidas();
            $resultado['total_notificaciones'] = $notificaciones->getNumNotificaciones();
            return $json_response->JSONResponse($resultado);
        }
    }

}
