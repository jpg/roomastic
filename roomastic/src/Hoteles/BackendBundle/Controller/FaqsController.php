<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Faqs;
use Hoteles\BackendBundle\Form\FaqsType;

/**
 * Faqs controller.
 *
 * @Route("/admin/faqs")
 */
class FaqsController extends Controller
{

    /**
     * Lists all Faqs entities.
     *
     * @Route("/", name="admin_faqs")
     * @Template()
     */
    public function indexAction()
    {

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Faqs');
        $query = $repository->createQueryBuilder('f')
                ->orderBy('f.posicion', 'ASC')
                ->getQuery();
        $entities = $query->getResult();

        return array('entities' => $entities);
    }

    /**
     * reordena lista de faqs
     *
     * @Route("/ordena/{orden}", name="admin_ordena", options={"expose"=true})
     * @Template()
     */
    public function ordenaAction($orden)
    {
        $elementos = explode('_', $orden);
        unset($elementos[0]);

        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('HotelesBackendBundle:Faqs')->findAll();

        $cont = 1;
        foreach ($elementos as $orden) {
            foreach ($entity as $faq) {
                if ($orden == $faq->getId()) {
                    $faq->setPosicion($cont);
                    $em->persist($faq);
                }
            }
            $cont = $cont + 1;
        }
        $em->flush();

        echo "1";
        exit;

        return array();
    }

    /**
     * Finds and displays a Faqs entity.
     *
     * @Route("/{id}/show", name="admin_faqs_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Faqs')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faqs entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Faqs entity.
     *
     * @Route("/new", name="admin_faqs_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Faqs();
        $form = $this->createForm(new FaqsType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Faqs entity.
     *
     * @Route("/create", name="admin_faqs_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Faqs:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Faqs();
        $request = $this->getRequest();
        $form = $this->createForm(new FaqsType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getEntityManager();
            $totalFaqs = $em->getRepository('HotelesBackendBundle:Faqs')->findAll();

            $entity->setPosicion(count($totalFaqs) + 1);
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_faqs_edit', array('id' => $entity->getId())));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }


        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Faqs entity.
     *
     * @Route("/{id}/edit", name="admin_faqs_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Faqs')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faqs entity.');
        }

        $editForm = $this->createForm(new FaqsType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Faqs entity.
     *
     * @Route("/{id}/update", name="admin_faqs_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Faqs:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Faqs')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faqs entity.');
        }

        $editForm = $this->createForm(new FaqsType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Editada correctamente.');

            return $this->redirect($this->generateUrl('admin_faqs_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo editar.');
        }


        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Faqs entity.
     *
     * @Route("/{id}/delete", name="admin_faqs_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Faqs')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Faqs entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_faqs'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
