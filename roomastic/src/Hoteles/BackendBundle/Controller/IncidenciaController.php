<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Incidencia;
use Hoteles\BackendBundle\Form\IncidenciaType;
use Hoteles\BackendBundle\Form\IncidenciaeditType;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Incidencia controller.
 *
 * @Route("/admin/incidencia")
 */
class IncidenciaController extends Controller
{

    /**
     * Lists all Incidencia entities.
     *
     * @Route("/", name="admin_incidencia")
     * @Template()
     */
    public function indexAction()
    {

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Incidencia');

        $query = $repository->createQueryBuilder('i');

        $securityContext = $this->container->get('security.context');
        $userId = $securityContext->getToken()->getUser()->getId();
        if ($securityContext->isGranted('ROLE_EMPRESA') || $securityContext->isGranted('ROLE_HOTEL')) {
            $query->where('i.usuariofrom = :user')
                    ->orWhere('i.usuarioto = :user')
                    ->setParameter('user', $userId);
        }

        $query->orderBy('i.fecha', 'DESC');

        $entities = $query->getQuery()->getResult();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
                $entities, $this->get('request')->query->get('page', 1)/* page number */, $this->get('request')->query->get('show', $this->container->getParameter('paginacionpordefecto'))/* limit per page */
        );


        return array(
            'entities' => $entities,
            'pagination' => $pagination,
        );
    }

    /**
     * Finds and displays a Incidencia entity.
     *
     * @Route("/{id}/show", name="admin_incidencia_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Incidencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Incidencia entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to create a new Incidencia entity.
     *
     * @Route("/new", name="admin_incidencia_new")
     * @Template()
     */
    public function newAction()
    {

        $new_incidencia = new Incidencia();
        $users = $this->getAvailableUsers();
        $form = $this->createForm(new IncidenciaType(), $new_incidencia, array('users' => $users));

        return array(
            'entity' => $new_incidencia,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Incidencia entity.
     *
     * @Route("/create", name="admin_incidencia_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Incidencia:new.html.twig")
     */
    public function createAction()
    {

        $new_incidencia = new Incidencia();
        $request = $this->getRequest();
        $users = $this->getAvailableUsers();
        $form = $this->createForm(new IncidenciaType(), $new_incidencia, array('users' => $users));
        $form->bindRequest($request);

        if ($form->isValid()) {

            $securityContext = $this->container->get('security.context');
            $userFrom = $securityContext->getToken()->getUser();
            if ($securityContext->isGranted('ROLE_EMPRESA') || $securityContext->isGranted('ROLE_HOTEL')) {
                $new_incidencia->setUsuariofrom($userFrom);
            }
            $new_incidencia->setFecha(new \Datetime());

            $em = $this->getDoctrine()->getEntityManager();
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($new_incidencia);
            $em->flush();

            $this->get('session')->setFlash('success', 'Incidencia enviada correctamente.');

            return $this->redirect($this->generateUrl('admin_incidencia'));
        } else {
            $this->get('session')->setFlash('nosuccess', 'Incidencia no enviada. Algún error ocurrió.');
        }

        return array(
            'entity' => $new_incidencia,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Incidencia entity.
     *
     * @Route("/{id}/edit", name="admin_incidencia_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Incidencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Incidencia entity.');
        }

        $query_user = $this->generaConsulta();
        $editForm = $this->createForm(new IncidenciaeditType($query_user), $entity);

        //$editForm = $this->createForm(new IncidenciaType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Incidencia entity.
     *
     * @Route("/{id}/update", name="admin_incidencia_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Incidencia:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Incidencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Incidencia entity.');
        }

        $query_user = $this->generaConsulta();
        $editForm = $this->createForm(new IncidenciaeditType($query_user), $entity);

        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Incidencia actualizada correctamente.');

            return $this->redirect($this->generateUrl('admin_incidencia_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'Incidencia no actualizada.');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Incidencia entity.
     *
     * @Route("/{id}/delete", name="admin_incidencia_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Incidencia')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Incidencia entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_incidencia'));
    }

    /**
     * Deletes a Incidencia entity.
     *
     * @Route("/{id}/atender", name="admin_incidencia_atender")
     * @Method("get")
     */
    public function atenderAction($id)
    {
        $request = $this->getRequest();

        $em = $this->getDoctrine()->getEntityManager();
        $entity = $em->getRepository('HotelesBackendBundle:Incidencia')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Incidencia entity.');
        }

        $entity->setAtendido(1);
        $em->persist($entity);
        $em->flush();

        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

    public function generaConsulta()
    {

        $securityContext = $this->container->get('security.context');
        $user = $securityContext->getToken()->getUser();

        $query_user = $this->getDoctrine()->getRepository('HotelesBackendBundle:User')->createQueryBuilder('u');
        //TODO: REFACTORIZAR ESTO ENTERO
        if ($securityContext->isGranted('ROLE_ADMIN') === false) {
            
        }

        if (!$securityContext->isGranted('ROLE_ADMIN')) {

            if ($user instanceof \Hoteles\BackendBundle\Entity\UserEmpresa) {
                $idEmpresa = $user->getEmpresa()->getId();
            } elseif ($user instanceof \Hoteles\BackendBundle\Entity\Userhotel) {
                $idEmpresa = $user->getHotel()->getEmpresa()->getId();
            }

            $repository_user_empresa = $this->getDoctrine()->getRepository('HotelesBackendBundle:UserEmpresa');
            $query_user_empresa = $repository_user_empresa->createQueryBuilder('u');
            $query_user_empresa
                    ->where('u.empresa = :idEmpresa')
                    ->setParameter('idEmpresa', $idEmpresa);
            $usuariosEmpresa = $query_user_empresa->getQuery()->getResult();

            $repository_user_hotel = $this->getDoctrine()->getRepository('HotelesBackendBundle:UserHotel');
            $query = $repository_user_hotel->createQueryBuilder('u');
            $query->leftJoin('u.hotel', 'h')
                    ->leftJoin('h.empresa', 'e')
                    ->where('e.id = :idEmpresa')
                    ->setParameter('idEmpresa', $idEmpresa);

            $usuariosHotel = $query->getQuery()->getResult();

            $em = $this->getDoctrine()->getEntityManager();
            $usuariosAdministradores = $em->getRepository('HotelesBackendBundle:UserAdministrador')->findAll();

            $arrayIdsUsuarios = array();

            foreach ($usuariosAdministradores as $user) {
                $arrayIdsUsuarios[] = $user->getId();
            }
            foreach ($usuariosEmpresa as $user) {
                $arrayIdsUsuarios[] = $user->getId();
            }
            foreach ($usuariosHotel as $user) {
                $arrayIdsUsuarios[] = $user->getId();
            }

            foreach ($arrayIdsUsuarios as $value) {
                $query_user->orWhere('u.id = ' . $value);
            }
        }

        $query_user->orderBy('u.username', 'ASC');

        return $query_user;
    }

    private function getAvailableUsers()
    {
        $securityContext = $this->container->get('security.context');

        $user = $securityContext->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        if ($securityContext->isGranted('ROLE_ADMIN')) {
            //Devuelve todos los usuarios de tipo administrador, hotel u empresa

            $usuariosAdministradores = $em->getRepository('HotelesBackendBundle:UserAdministrador')->findAll();
            $usuarios_hotel = $em->getRepository('HotelesBackendBundle:UserHotel')->findAll();
            $usuarios_empresa = $em->getRepository('HotelesBackendBundle:UserEmpresa')->findAll();
            $users = array_merge_recursive($usuariosAdministradores, $usuarios_hotel, $usuarios_empresa);
        } else if ($securityContext->isGranted('ROLE_EMPRESA')) {
            //Devuelve todos los usuarios empresa compañeros
            $users = $user->getEmpresa()->getUserEmpresa()->toArray();
            //Devuelve todos los usuarios hotel de los hoteles de esa empresa
            foreach ($user->getEmpresa()->getHoteles() as $hotel) {
                $users = array_merge($users, $hotel->getUserHotel()->toArray());
            }
        } else if ($securityContext->isGranted('ROLE_HOTEL')) {
            //Devuelve todos los usuarios hotel del mismo hotel
            $users_hotel = $user->getHotel()->getUserHotel()->toArray();
            //Devuelve todos los usuarios empresa de la empresa de ese hotel
            $users = array_merge($users_hotel, $user->getHotel()->getEmpresa()->getUserEmpresa()->toArray());
        }
        return $users;
    }

}
