<?php

namespace Hoteles\BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Hoteles\BackendBundle\Entity\Publicatuhotel;
use Hoteles\BackendBundle\Form\PublicatuhotelType;

/**
 * Publicatuhotel controller.
 *
 * @Route("/admin/publicatuhotel")
 */
class PublicatuhotelController extends Controller
{

    /**
     * Lists all Publicatuhotel entities.
     *
     * @Route("/", name="admin_publicatuhotel")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('HotelesBackendBundle:Publicatuhotel')->findAll();

        if ($entities) {
            return $this->redirect($this->generateUrl('admin_publicatuhotel_edit', array('id' => $entities[0]->getId())));
        } else {
            return $this->redirect($this->generateUrl('admin_publicatuhotel_new'));
        }

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Publicatuhotel entity.
     *
     * @Route("/{id}/show", name="admin_publicatuhotel_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Publicatuhotel')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Publicatuhotel entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),);
    }

    /**
     * Displays a form to create a new Publicatuhotel entity.
     *
     * @Route("/new", name="admin_publicatuhotel_new")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Publicatuhotel();
        $form = $this->createForm(new PublicatuhotelType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Publicatuhotel entity.
     *
     * @Route("/create", name="admin_publicatuhotel_create")
     * @Method("post")
     * @Template("HotelesBackendBundle:Publicatuhotel:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Publicatuhotel();
        $request = $this->getRequest();
        $form = $this->createForm(new PublicatuhotelType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Creado correctamente.');

            return $this->redirect($this->generateUrl('admin_publicatuhotel_edit', array('id' => $entity->getId())));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo crear.');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Publicatuhotel entity.
     *
     * @Route("/{id}/edit", name="admin_publicatuhotel_edit")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Publicatuhotel')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Publicatuhotel entity.');
        }

        $editForm = $this->createForm(new PublicatuhotelType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Publicatuhotel entity.
     *
     * @Route("/{id}/update", name="admin_publicatuhotel_update")
     * @Method("post")
     * @Template("HotelesBackendBundle:Publicatuhotel:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('HotelesBackendBundle:Publicatuhotel')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Publicatuhotel entity.');
        }

        $editForm = $this->createForm(new PublicatuhotelType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Editado correctamente.');

            return $this->redirect($this->generateUrl('admin_publicatuhotel_edit', array('id' => $id)));
        } else {
            $this->get('session')->setFlash('nosuccess', 'No se pudo editar.');
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Publicatuhotel entity.
     *
     * @Route("/{id}/delete", name="admin_publicatuhotel_delete")
     * @Method("post")
     */
    public function deleteAction($id)
    {
        $form = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('HotelesBackendBundle:Publicatuhotel')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Publicatuhotel entity.');
            }

            $em->remove($entity);
            $em->flush();

            $this->get('session')->setFlash('success', 'Borrado correctamente.');
        }

        return $this->redirect($this->generateUrl('admin_publicatuhotel'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                        ->add('id', 'hidden')
                        ->getForm()
        ;
    }

}
