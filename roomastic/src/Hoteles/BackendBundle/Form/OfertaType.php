<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class OfertaType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('usuario')
                ->add('fecha')
                ->add('hotel')
                ->add('preciohabitacion')
                ->add('fechain')
                ->add('fechaout')
                ->add('numnoches')
                ->add('numadultos')
                ->add('numninos')
                ->add('numhabitaciones')
                ->add('preciototaloferta')
                ->add('idoferta')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_ofertatype';
    }

}
