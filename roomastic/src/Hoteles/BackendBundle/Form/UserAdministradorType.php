<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Email;

class UserAdministradorType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('username', 'text', array(
                    'required' => false,
                ))
                ->add('email', 'text', array(
                    'required' => false,
                ))
        //->add('imagen')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_useradministradortype';
    }

}
