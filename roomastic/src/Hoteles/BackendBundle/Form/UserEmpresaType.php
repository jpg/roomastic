<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UserEmpresaType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('username')
                ->add('email')
                ->add('empresa')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_userempresatype';
    }

}
