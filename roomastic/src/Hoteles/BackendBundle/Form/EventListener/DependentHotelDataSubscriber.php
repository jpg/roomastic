<?php

namespace Hoteles\BackendBundle\Form\EventListener;

use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Doctrine\ORM\EntityManager;

class DependentHotelDataSubscriber implements EventSubscriberInterface
{

    private $factory;
    private $em;
    private $method;

    public function __construct(FormFactoryInterface $factory, EntityManager $em, $method)
    {
        $this->factory = $factory;
        $this->em = $em;
        $this->method = $method;
    }

    public static function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(DataEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }
        $query_municipios = $this->em->getRepository('HotelesBackendBundle:Municipio')
                        ->createQueryBuilder('m')->orderBy('m.nombre', 'ASC');

        $query_lugares = $this->em->getRepository('HotelesBackendBundle:Lugar')
                        ->createQueryBuilder('l');
        $form_municipio_options = array(
            'class' => 'HotelesBackendBundle:Municipio',
            'query_builder' => $query_municipios,
            'required' => false,
        );
        $form_lugar_options = array(
            'class' => 'HotelesBackendBundle:Lugar',
            'query_builder' => $query_lugares,
            'required' => false,);
        //Compruebo que si el hotel es nuevo o no
        if ($data->getId() && $this->method === 'edit') {
            //Compruebo que tenga una provincia
            if ($data->getProvincia()) {
                $query_municipios->andWhere('m.provincia = :provincia');
                $query_municipios->setParameter('provincia', $data->getProvincia());

                if ($data->getMunicipio()) {
                    $query_lugares->andWhere('l.municipio = :municipio');
                    $municipio = $data->getMunicipio();
                    $query_lugares->setParameter('municipio', $municipio->getId());
                }
            }
        }
        $form->add($this->factory->createNamed('entity', 'lugar'
                        , $data->getLugar()
                        , $form_lugar_options));
        
        $form->add($this->factory->createNamed('entity', 'municipio'
                        , $data->getMunicipio()
                        , $form_municipio_options));
        
    }

}
