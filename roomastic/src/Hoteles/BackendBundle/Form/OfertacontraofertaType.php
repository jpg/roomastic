<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class OfertacontraofertaType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('preciototalcontraoferta')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_ofertatype';
    }

}
