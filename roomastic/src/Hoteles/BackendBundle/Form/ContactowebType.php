<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ContactowebType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('nombre')
                ->add('apellidos')
                ->add('email')
                ->add('texto')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_contactotype';
    }

}
