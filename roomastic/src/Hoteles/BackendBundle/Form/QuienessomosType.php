<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class QuienessomosType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('texto', 'textarea', array(
                    'required' => false,
                ))
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_quienessomostype';
    }

}
