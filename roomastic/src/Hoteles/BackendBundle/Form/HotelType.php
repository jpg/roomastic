<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class HotelType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {

        $builder
                ->add('email')
        ;
        $builder
                ->add('empresa')
        ;
        $builder
                ->add('nombreempresa')
                ->add('cif')
                ->add('direccionfacturacion')
                ->add('telefono')
                ->add('numerocuenta')
                ->add('nombrepersonacontacto')
                ->add('apellidospersonacontacto')
                ->add('cargopersonacontacto')
                ->add('emailpersonacontacto')
                ->add('telefonopersonacontacto')
                ->add('nombrehotel')
                ->add('calificacion', 'choice', array(
                    'choices' => array(
                        '1' => '1',
                        '2' => '2',
                        '3' => '3',
                        '4' => '4',
                        '5' => '5',
                    ),
                    'expanded' => true,
                    'multiple' => false,
                ))
                ->add('descripcion')
                ->add('caracteristicas')
                ->add('ubicacion')
                ->add('pvpoficialenteros')
                ->add('pvpoficialdecimales')
                ->add('cantidadminimaaceptadaenteros')
                ->add('cantidadminimaaceptadadecimales')
                ->add('piscina', 'choice', array(
                    'choices' => array('1' => 'Publicado', '0' => 'No publicado'),
                    'required' => false,
                ))
                ->add('spa', 'choice', array(
                    'choices' => array('1' => 'Publicado', '0' => 'No publicado'),
                    'required' => false,
                ))
                ->add('wi_fi', 'choice', array(
                    'choices' => array('1' => 'Publicado', '0' => 'No publicado'),
                    'required' => false,
                ))
                ->add('acceso_adaptado', 'choice', array(
                    'choices' => array('1' => 'Publicado', '0' => 'No publicado'),
                    'required' => true,
                ))
                ->add('aceptan_perros', 'choice', array(
                    'choices' => array('1' => 'Publicado', '0' => 'No publicado'),
                    'required' => false,
                ))
                ->add('aparcamiento', 'choice', array(
                    'choices' => array('1' => 'Publicado', '0' => 'No publicado'),
                    'required' => false,
                ))
                ->add('business_center', 'choice', array(
                    'choices' => array('1' => 'Publicado', '0' => 'No publicado'),
                    'required' => false,
                ))
                ->add('status', 'choice', array(
                    'choices' => array('1' => 'Publicado', '0' => 'No publicado'),
                    'required' => true,
                ))
                ->add('seotitulo')
                ->add('seodescripcion')
                ->add('seokeywords')
                ->add('comision_hotel_entero')
                ->add('comision_hotel_decimal')

        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_hoteltype';
    }

}
