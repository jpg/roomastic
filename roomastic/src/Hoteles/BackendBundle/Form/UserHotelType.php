<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class UserHotelType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('username')
                ->add('email')
                ->add('hotel')
                ->add('administrador')
                ->add('cargo')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_userhoteltype';
    }

}
