<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ContactoType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('nombre')
                ->add('apellidos')
                ->add('email')
                ->add('texto')
                ->add('atendido')
                ->add('fecha', 'date', array('widget' => 'single_text', 'format' => "d/M/Y"))
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_contactotype';
    }

}
