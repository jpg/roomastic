<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EmpresaType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('email', 'text', array(
                    'required' => false,
                ))
                ->add('nombreempresa', 'text', array(
                    'required' => false,
                ))
                ->add('cif', 'text', array(
                    'required' => false,
                ))
                ->add('direccionfacturacion', 'text', array(
                    'required' => false,
                ))
                ->add('telefono', 'text', array(
                    'required' => false,
                ))
                ->add('numerocuenta', 'text', array(
                    'required' => false,
                ))
                ->add('nombrepersonacontacto', 'text', array(
                    'required' => false,
                ))
                ->add('apellidospersonacontacto', 'text', array(
                    'required' => false,
                ))
                ->add('emailpersonacontacto', 'text', array(
                    'required' => false,
                ))
                ->add('telefonopersonacontacto', 'text', array(
                    'required' => false,
                ))
                ->add('cargopersonacontacto', 'text', array(
                    'required' => false,
        ));
        if (true === $options['admin']) {
            $builder
                    ->add('comisionempresaentero', 'text', array(
                        'required' => false,
                    ))
                    ->add('comisionempresadecimal', 'text', array(
                        'required' => false,
            ));
        }
    }

    public function getName()
    {
        return 'hoteles_backendbundle_empresatype';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'admin' => FALSE,
        );
    }

}
