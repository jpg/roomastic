<?php

namespace Hoteles\BackendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ImagenhotelType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder->add('imagen')
                ->add('hotel')
                ->add('orden')
                ->add('alt');
    }

    public function getName()
    {
        return 'hoteles_backendbundle_imagenhoteltype';
    }

}
