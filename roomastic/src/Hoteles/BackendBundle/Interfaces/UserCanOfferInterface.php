<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Interfaces;

interface UserCanOfferInterface
{

    const PUEDE_OFERTAR = 'si';
    const NO_PUEDE_OFERTAR = 'no';
    const FALTAN_DATOS = 'faltan_datos';
    const OFERTA_PENDIENTE = 'oferta_realizada_pendiente';
    const USUARIO_BANEADO = 'baneado';
    public function canOffer();
}
