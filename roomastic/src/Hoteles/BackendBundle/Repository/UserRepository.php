<?php

namespace Hoteles\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

    public function findBy(array $criteria, array $orderBy = NULL, $limit = NULL, $offset = NULL)
    {
        if (count($criteria) === 1 && isset($criteria['email'])) {
            return $this
                            ->getEntityManager()
                            ->createQuery("SELECT u FROM HotelesBackendBundle:User u WHERE u.email = :email")
                            ->setParameters($criteria)
                            ->getResult();
        } else {
            return parent::findBy($criteria, $orderBy, $limit, $offset);
        }
    }

}
