<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Hoteles\BackendBundle\Entity\Hotel;
class HotelRepository extends EntityRepository
{

    public function getByIdArray($hotel_ids)
    {
        $em = $this->getEntityManager();
        $dql = "SELECT h from HotelesBackendBundle:Hotel h where h.id in (:hotel_ids) and h.status = :status_activo";
        $consulta = $em->createQuery($dql);
        $consulta->setParameter('hotel_ids', $hotel_ids);
        $consulta->setParameter('status_activo', Hotel::HOTEL_STATUS_VISIBLE);
        $resultado = $consulta->getArrayResult();
        return $resultado;
    }

}
