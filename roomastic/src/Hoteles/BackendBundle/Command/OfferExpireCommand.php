<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\BackendBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Input\InputOption;
use Hoteles\BackendBundle\Entity\Oferta;
use Doctrine\ORM\Query;
use Hoteles\BackendBundle\Interfaces\ConfigureCommandEnviromentInterface;

/**
 * Este comando se ejecuta cada XX minutos y lo que hace es:
 * - Comprueba si una oferta ha pasado su plazo de aceptación
 * - Si es así: 
 *      - Caduco las ofertas del grupo que estén con estado inicial.
 *      - Envío e-mail de resumen con todos los datos de las ofertas (Envío e-mail de NO aceptadas)
 * @author joaquin
 */
class OfferExpireCommand extends ContainerAwareCommand implements ConfigureCommandEnviromentInterface
{

    protected function configure()
    {
        $this->setName('hoteles:offer:expire')
                ->setDescription('Envía notificacion de estado de ofertas')
                ->addOption('mock', null, InputOption::VALUE_NONE, 'No guarda cambios en la BD');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->configureCommandEnviroment();
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $configuracion = $this->getContainer()->get('hoteles_backend.load_configuracion')->loadConfiguracion();
        $horas_fecha_expiracion = $configuracion->getTiempohotel();
        //Busco todas los grupos de ofertas que tengan estado valido y no hayan sido atendidas en N horas
        $output->writeln(sprintf('Buscando ofertas desatendidas de más de <info>%s</info> horas', $horas_fecha_expiracion));
        $id_ofertas_grupo_a_caducar = $em->getRepository('HotelesBackendBundle:Oferta')
                ->getPendingOfferGroup($horas_fecha_expiracion);
        $output->writeln(sprintf('Encontradas <info>%s</info> ofertas para caducar', count($id_ofertas_grupo_a_caducar)));

        //Helpers
        $oferta_mailer = $this->getContainer()->get('oferta.notice_mailer');
        $oferta_manager = $this->getContainer()->get('hoteles_backend.oferta_manager');
        $verbose = $input->getOption('verbose');
        if ($verbose) {
            $fecha_actual = new \DateTime();
            $output->writeln(sprintf('Hora de ejecucion: %s', $fecha_actual->format('d-m-Y H:i')));
        }
        foreach ($id_ofertas_grupo_a_caducar as $id_oferta_grupo) {

            //Busco las ofertas de ese grupo
            $ofertas_del_grupo = $em->getRepository('HotelesBackendBundle:Oferta')
                    ->getOffersFromGroup($id_oferta_grupo['idoferta']);
            if ($verbose) {
                $output->writeln(sprintf('Encontrado el grupo <info>%s</info> para expirar con <info>%d</info> ofertas', $id_oferta_grupo['idoferta'], count($ofertas_del_grupo)));
            }
            //Le paso una oferta cualquiera, por defecto la primera
            $oferta_cero = $ofertas_del_grupo[0];
            $oferta_manager->updateOfferGroupState('caduca_oferta', $oferta_cero);
            if ($oferta_cero->isGroupOfferExpired()) {
                $oferta_mailer->sendOfferNotAcceptedSummary($ofertas_del_grupo);
                //Quito la restriccion al usuario            
                $oferta_cero->removeOfferBanFromUser();
                //Guardo los cambios del usuario
                $em->persist($oferta_cero->getUsuario());                
            }
        }
        if (false === $input->getOption('mock')) {
            $em->flush();
        } else {
            $output->writeln("Cambios NO guardados");
        }
    }

    public function configureCommandEnviroment()
    {
        $context = $this->getContainer()->get('router')->getContext();
        $context->setHost('www.roomastic.com');
        $context->setScheme('https');
    }

}
