<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Hoteles\BackendBundle\Entity\User;

/**
 * Hoteles\BackendBundle\Entity\UserEmpresa
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class UserEmpresa extends User
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \empresa
     *
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="empresa", referencedColumnName="id")
     * })
     */
    private $empresa;

    /**
     * @var string $imagen
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable="true")
     */
    private $imagen;

    /**
     * @var string $nombrecompleto
     *
     * @ORM\Column(name="nombre_completo", type="string", length=255, nullable="true")
     */
    private $nombreCompleto;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get empresa
     *
     * @return \Hoteles\BackendBundle\Entity\empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set empresa
     *
     * @param \Hoteles\BackendBundle\Entity\Empresa $empresa
     * @return Event
     */
    public function setEmpresa(\Hoteles\BackendBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }

    /**
     * Get imagen
     *
     * @return string 
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    public function inrole($role)
    {
        return in_array($role, $this->getRoles());
    }

    /**
     * @var integer $status
     */
    private $status;

    /**
     * Set status
     *
     * @param integer $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getPublicUsername()
    {
        $resultado = $this->getNombreCompleto() . ", empresa (" . $this->getEmpresa()->getNombreempresa() . ")";
        return $resultado;
    }

    /**
     * Set nombreCompleto
     *
     * @param string $nombreCompleto
     */
    public function setNombreCompleto($nombreCompleto)
    {
        $this->nombreCompleto = $nombreCompleto;
    }

    /**
     * Get nombreCompleto
     *
     * @return string 
     */
    public function getNombreCompleto()
    {
        return $this->nombreCompleto;
    }

    public function getTypeOf()
    {
        return 'empresa';
    }

    public function getAvatarImage()
    {
        return $this->getImagen() == "" ? parent::getAvatarImage() : "/uploads/user/" . $this->getImagen();
    }

}