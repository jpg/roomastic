<?php

namespace Hoteles\BackendBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Hoteles\BackendBundle\Interfaces\UserCanOfferInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * 
 * @ORM\Table(name="fos_user")
 * @UniqueEntity(fields="email",message="user.email.unique",groups={"registration"})
 * @ORM\Entity(repositoryClass="Hoteles\BackendBundle\Repository\UserRepository")

 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="user_type", type="string")
 * @ORM\DiscriminatorMap({  "user" = "User", 
 *                          "user_administrador" = "Hoteles\BackendBundle\Entity\UserAdministrador",
 *                          "user_empresa" = "Hoteles\BackendBundle\Entity\UserEmpresa",
 *                          "user_hotel" = "Hoteles\BackendBundle\Entity\UserHotel",
 *                          "user_web" = "Hoteles\BackendBundle\Entity\UserWeb",
 *                          "user_basico" = "Hoteles\BackendBundle\Entity\UserBasico"
 * })
 */
class User extends BaseUser implements UserCanOfferInterface
{

    const USER_LOGGEDIN = '1';
    const USER_NOT_LOGGEDIN = '0';
    const USER_EXTRA_INFORMATION_VALID = '1';
    const USER_EXTRA_INFORMATION_INVALID = '0';
    const USER_STATUS_ENABLED = 0;
    const USER_STATUS_SUSPENDED = 1;
    const USER_STATUS_BANNED = 2;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $googleID
     * Valores: Los que vienen arriba
     * @ORM\Column(name="status", type="integer",nullable=true)
     */
    private $status = self::USER_STATUS_ENABLED;

    public function inrole($role)
    {
        return in_array($role, $this->getRoles());
    }

    public function __construct()
    {
        parent::__construct();
        $this->setEnabled(true);
        //$this->roles = array('ROLE_WEB');
        // your own logic
    }

    /**
     * Por defecto ninguno puede ofertar, con esto consigo que todos los que heredan 
     * de esta clase y no pueden realizar ofertas (Admin, Hotel, Empresa) por defecto no puedan
     * y dependa de la sobrecarga en cada clase el hecho de poder.
     */
    public function canOffer()
    {
        return UserCanOfferInterface::NO_PUEDE_OFERTAR;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return is_null($this->status) ? self::USER_STATUS_ENABLED : $this->status;
    }

    public function banUser()
    {
        $this->setStatus(self::USER_STATUS_BANNED);
        $this->setEnabled(FALSE);
    }

    public function unbanUser()
    {
        $this->setStatus(self::USER_STATUS_ENABLED);
        $this->setLocked(FALSE);
        $this->setEnabled(TRUE);
    }

    public function suspendUser()
    {
        $this->setStatus(self::USER_STATUS_SUSPENDED);
        $this->setLocked(TRUE);
        $this->setEnabled(FALSE);
    }

    public function unsuspendUser()
    {
        $this->setStatus(self::USER_STATUS_ENABLED);
        $this->setLocked(FALSE);
        $this->setEnabled(TRUE);
    }

    public function isBanned()
    {
        return $this->getStatus() === self::USER_STATUS_BANNED;
    }

    public function isSuspended()
    {
        return $this->getStatus() === self::USER_STATUS_SUSPENDED;
    }

    public function getPublicUsername()
    {
        return $this->getEmailCanonical();
    }

    public function getNombreCompleto()
    {
        return $this->getUsernameCanonical();
    }

    public function isActive()
    {
        $resultado = true;
        $locked = $this->isLocked();
        $enabled = $this->isEnabled();
        $resultado &= $enabled;
        $resultado &=!$locked;
        $resultado &=!($this->isBanned() || $this->isSuspended());
        return $resultado;
    }

    public function getTypeOf()
    {
        return 'basico';
    }

    public function getAvatarImage()
    {
        return '/bundles/hotelesbackend/img/avatar.jpg';
    }

}