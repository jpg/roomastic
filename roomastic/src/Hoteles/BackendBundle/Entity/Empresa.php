<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Hoteles\BackendBundle\Entity\Empresa
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Empresa
{

    const EMPRESA_STATUS_OK = 0;
    const EMPRESA_STATUS_SUSPENDED = 1;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     * 
     */
    private $email;

    /**
     * @var string $nombreempresa
     *
     * @ORM\Column(name="nombreempresa", type="string", length=255)
     * @Assert\NotBlank()
     * // TODO: Tiene que ser unico?
     */
    private $nombreempresa;

    /**
     * @var string $cif
     *
     * @ORM\Column(name="cif", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $cif;

    /**
     * @var string $direccionfacturacion
     *
     * @ORM\Column(name="direccionfacturacion", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $direccionfacturacion;

    /**
     * @var string $telefono
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $telefono;

    /**
     * @var string $numerocuenta
     *
     * @ORM\Column(name="numerocuenta", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $numerocuenta;

    /**
     * @var string $nombrepersonacontacto
     *
     * @ORM\Column(name="nombrepersonacontacto", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $nombrepersonacontacto;

    /**
     * @var string $apellidospersonacontacto
     *
     * @ORM\Column(name="apellidospersonacontacto", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $apellidospersonacontacto;

    /**
     * @var string $emailpersonacontacto
     *
     * @ORM\Column(name="emailpersonacontacto", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     * 
     */
    private $emailpersonacontacto;

    /**
     * @var string $telefonopersonacontacto
     *
     * @ORM\Column(name="telefonopersonacontacto", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $telefonopersonacontacto;

    /**
     * @var string $cargopersonacontacto
     *
     * @ORM\Column(name="cargopersonacontacto", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $cargopersonacontacto;

    /**
     *
     * @var type 
     * @ORM\Column(name="comision_empresa_entero", type="integer")
     */
    private $comision_empresa_entero;

    /**
     *
     * @var type 
     * @ORM\Column(name="comision_empresa_decimal", type="integer")
     */
    private $comision_empresa_decimal;

    /**
     * @ORM\OneToMany(targetEntity="UserEmpresa", mappedBy="empresa")
     * */
    private $user_empresa;

    /**
     * @ORM\OneToMany(targetEntity="Hotel", mappedBy="empresa")
     * */
    private $hoteles;

    /**
     *
     * @var type 
     * @ORM\Column(name="status", type="integer")
     */
    private $status = self::EMPRESA_STATUS_OK;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set nombreempresa
     *
     * @param string $nombreempresa
     */
    public function setNombreempresa($nombreempresa)
    {
        $this->nombreempresa = $nombreempresa;
    }

    /**
     * Get nombreempresa
     *
     * @return string 
     */
    public function getNombreempresa()
    {
        return $this->nombreempresa;
    }

    /**
     * Set cif
     *
     * @param string $cif
     */
    public function setCif($cif)
    {
        $this->cif = $cif;
    }

    /**
     * Get cif
     *
     * @return string 
     */
    public function getCif()
    {
        return $this->cif;
    }

    /**
     * Set direccionfacturacion
     *
     * @param string $direccionfacturacion
     */
    public function setDireccionfacturacion($direccionfacturacion)
    {
        $this->direccionfacturacion = $direccionfacturacion;
    }

    /**
     * Get direccionfacturacion
     *
     * @return string 
     */
    public function getDireccionfacturacion()
    {
        return $this->direccionfacturacion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set numerocuenta
     *
     * @param string $numerocuenta
     */
    public function setNumerocuenta($numerocuenta)
    {
        $this->numerocuenta = $numerocuenta;
    }

    /**
     * Get numerocuenta
     *
     * @return string 
     */
    public function getNumerocuenta()
    {
        return $this->numerocuenta;
    }

    /**
     * Set nombrepersonacontacto
     *
     * @param string $nombrepersonacontacto
     */
    public function setNombrepersonacontacto($nombrepersonacontacto)
    {
        $this->nombrepersonacontacto = $nombrepersonacontacto;
    }

    /**
     * Get nombrepersonacontacto
     *
     * @return string 
     */
    public function getNombrepersonacontacto()
    {
        return $this->nombrepersonacontacto;
    }

    /**
     * Set apellidospersonacontacto
     *
     * @param string $apellidospersonacontacto
     */
    public function setApellidospersonacontacto($apellidospersonacontacto)
    {
        $this->apellidospersonacontacto = $apellidospersonacontacto;
    }

    /**
     * Get apellidospersonacontacto
     *
     * @return string 
     */
    public function getApellidospersonacontacto()
    {
        return $this->apellidospersonacontacto;
    }

    /**
     * Set emailpersonacontacto
     *
     * @param string $emailpersonacontacto
     */
    public function setEmailpersonacontacto($emailpersonacontacto)
    {
        $this->emailpersonacontacto = $emailpersonacontacto;
    }

    /**
     * Get emailpersonacontacto
     *
     * @return string 
     */
    public function getEmailpersonacontacto()
    {
        return $this->emailpersonacontacto;
    }

    /**
     * Set telefonopersonacontacto
     *
     * @param string $telefonopersonacontacto
     */
    public function setTelefonopersonacontacto($telefonopersonacontacto)
    {
        $this->telefonopersonacontacto = $telefonopersonacontacto;
    }

    /**
     * Get telefonopersonacontacto
     *
     * @return string 
     */
    public function getTelefonopersonacontacto()
    {
        return $this->telefonopersonacontacto;
    }

    /**
     * Set cargopersonacontacto
     *
     * @param string $cargopersonacontacto
     */
    public function setCargopersonacontacto($cargopersonacontacto)
    {
        $this->cargopersonacontacto = $cargopersonacontacto;
    }

    /**
     * Get cargopersonacontacto
     *
     * @return string 
     */
    public function getCargopersonacontacto()
    {
        return $this->cargopersonacontacto;
    }

    public function __toString()
    {
        return $this->getNombreempresa();
    }

    /**
     * Set comision_empresa_entero
     *
     * @param integer $comisionEmpresaEntero
     */
    public function setComisionEmpresaEntero($comisionEmpresaEntero)
    {
        $this->comision_empresa_entero = $comisionEmpresaEntero;
    }

    /**
     * Get comision_empresa_entero
     *
     * @return integer 
     */
    public function getComisionEmpresaEntero()
    {
        return $this->comision_empresa_entero;
    }

    /**
     * Set comision_empresa_decimal
     *
     * @param integer $comisionEmpresaDecimal
     */
    public function setComisionEmpresaDecimal($comisionEmpresaDecimal)
    {
        $this->comision_empresa_decimal = $comisionEmpresaDecimal;
    }

    /**
     * Get comision_empresa_decimal
     *
     * @return integer 
     */
    public function getComisionEmpresaDecimal()
    {
        return $this->comision_empresa_decimal;
    }

    public function __construct()
    {
        $this->user_empresa = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user_empresa
     *
     * @param Hoteles\BackendBundle\Entity\UserEmpresa $userEmpresa
     */
    public function addUserEmpresa(\Hoteles\BackendBundle\Entity\UserEmpresa $userEmpresa)
    {
        $this->user_empresa[] = $userEmpresa;
    }

    /**
     * Get user_empresa
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getUserEmpresa()
    {
        return $this->user_empresa;
    }

    /**
     * Add hoteles
     *
     * @param Hoteles\BackendBundle\Entity\Hotel $hoteles
     */
    public function addHotel(\Hoteles\BackendBundle\Entity\Hotel $hoteles)
    {
        $this->hoteles[] = $hoteles;
    }

    /**
     * Get hoteles
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getHoteles()
    {
        return $this->hoteles;
    }

    /**
     * Set status
     *
     * @param integer $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function suspendEmpresa()
    {
        $this->setStatus(self::EMPRESA_STATUS_SUSPENDED);
    }

    public function unsuspendEmpresa()
    {
        $this->setStatus(self::EMPRESA_STATUS_OK);
    }

}