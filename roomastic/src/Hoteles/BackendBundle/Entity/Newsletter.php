<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hoteles\BackendBundle\Entity\Newsletter
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Newsletter
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \usuario
     *
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="usuario", referencedColumnName="id")
     */
    private $usuario;

    /**
     * @var datetime $fecha
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var boolean $activo
     *
     * @ORM\Column(name="activo", type="boolean", nullable="true")
     */
    private $activo;

    /**
     * @var datetime $fechadisabled
     *
     * @ORM\Column(name="fechadisabled", type="datetime", nullable="true")
     */
    private $fechadisabled;

    public function __construct()
    {
        $this->setFecha(new \DateTime());
        $this->setActivo(true);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get usuario
     *
     * @return \Hoteles\BackendBundle\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set usuario
     *
     * @param \Hoteles\BackendBundle\Entity\User $usuario
     * @return Event
     */
    public function setUsuario(\Hoteles\BackendBundle\Entity\User $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Set fecha
     *
     * @param datetime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * Get fecha
     *
     * @return datetime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;
    }

    /**
     * Get activo
     *
     * @return boolean 
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set fechadisabled
     *
     * @param datetime $fechadisabled
     */
    public function setFechadisabled($fechadisabled)
    {
        $this->fechadisabled = $fechadisabled;
    }

    /**
     * Get fechadisabled
     *
     * @return datetime 
     */
    public function getFechadisabled()
    {
        return $this->fechadisabled;
    }

    public function isActivo()
    {
        return $this->getActivo() === true;
    }

    public function makeActivo()
    {
        return $this->setActivo(true);
    }

    public function makeNotActivo()
    {
        $this->setFechadisabled(new \DateTime());
        return $this->setActivo(false);
    }

    public function exportToExcel()
    {
        $usuario = $this->getUsuario();
        $resultado = '<tr><td><span class="style2">' . $usuario->getNombreCompleto() . '</span></td>' .
                '<td><span class="style2">' . $usuario->getEmail() . '</span></td>' .
                '</tr>';
        return $resultado;
    }

}