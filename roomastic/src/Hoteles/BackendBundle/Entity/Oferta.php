<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hoteles\BackendBundle\Entity\Oferta
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Hoteles\BackendBundle\Repository\OfertaRepository")
 */
class Oferta
{

    //Estados posibles de una oferta
    //ESTADOS INICIALES
    //Cuando un usuario realiza una oferta valida
    const OFERTA_VALIDA = 0;
    //Cuando un usuario realiza una oferta superior al PVP
    const OFERTA_SUPERIOR_PVP = 1;
    //Cuando un usuario realiza una oferta inferior al minimo
    const OFERTA_INFERIOR_MINIMO = 2;
    //ESTADOS DE UNA CONTRAOFERTA
    //Cuando un hotel realiza una contraoferta válida
    const OFERTA_CONTRAOFERTA_REALIZADA = 10;
    //Cuando un usuario paga otra oferta/contraoferta
    const OFERTA_CONTRAOFERTA_RECHAZADA = 11;
    //Cuando un usuario no paga esta contraoferta ni ninguna otra
    const OFERTA_CONTRAOFERTA_CADUCADA = 12;
    //Cuando un usuario paga la contraoferta, implica que el resto de contraofertas 
    //se ponen a OFERTA_CONTRAOFERTA_RECHAZADA
    const OFERTA_CONTRAOFERTA_PAGADA = 13;
    //Se usa cuando hay una contraoferta realizada y un hotel acepta la oferta del usuario
    const OFERTA_CONTRAOFERTA_ANULADA = 14;
    //ESTADOS VALIDOS NO INICIALES DE UNA OFERTA
    //Cuando un usuario realiza el pago de una oferta
    const OFERTA_PAGADA = 20;
    //Cuando un usuario no realiza el pago de una oferta aceptada por el hotel
    const OFERTA_NO_PAGADA = 21;
    //Cuando un hotel rechaza una de las ofertas validas realizadas
    const OFERTA_RECHAZADA = 22;
    //Cuando caduca una de las ofertas sin recibir atención por parte del hotel
    const OFERTA_CADUCADA = 23;
    //Cuando un hotel acepta una de las ofertas validas realizadas, 
    //resto de ofertas cambian al estado OFERTA_HOTEL_ACEPTADA
    const OFERTA_ACEPTADA = 24;
    //Cuando un hotel acepta una oferta el resto de hoteles ven este estado
    const OFERTA_HOTEL_ACEPTADA = 25;
    //Cuando un hotel o empresa pasa a estar suspenso
    const OFERTA_HOTEL_SUSPENDED_HOTEL = 30;
    //ESTADOS POSIBLES DE UN GRUPO DE OFERTAS
    //Todas las ofertas empiezan con este estado
    const GRUPO_OFERTA_ESTADO_INICIAL = 0;
    //Algun hotel acepta una oferta, lo uso para comprobar rapidamente si un 
    //hotel quiere contraofertar
    const GRUPO_OFERTA_ESTADO_ACEPTADA = 1;
    //La oferta tenia estado valido 
    const GRUPO_OFERTA_ESTADO_CADUCADO = 2;
    //Estado invalido por si hay algun problema
    const GRUPO_OFERTA_ESTADO_INVALIDO = 3;
    //Estado de que hay una contraoferta realizada
    const GRUPO_OFERTA_CONTRAOFERTA_REALIZADA = 4;
    //Estado que tiene un grupo de ofertas al pasar a ser una contraoferta valida
    const GRUPO_OFERTA_CONTRAOFERTA_VALIDA = 5;
    //Estado de que hay una contraoferta expirada
    const GRUPO_OFERTA_CONTRAOFERTA_CADUCADA = 6;

    /**
     *  @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \usuario
     *
     * @ORM\ManyToOne(targetEntity="User",cascade={"remove"})
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="usuario", referencedColumnName="id",onDelete="CASCADE")
     * })
     */
    private $usuario;

    /**
     * @var datetime $fecha
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var \hotel
     *
     * @ORM\ManyToOne(targetEntity="Hotel")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="hotel", referencedColumnName="id")
     * })
     */
    private $hotel;

    /**
     * @var decimal $preciohabitacion
     *
     * @ORM\Column(name="preciohabitacion", type="decimal", precision=18, scale=12, nullable="true")
     */
    private $preciohabitacion;

    /**
     * @var date $fechain
     *
     * @ORM\Column(name="fechain", type="date")
     */
    private $fechain;

    /**
     * @var date $fechaout
     *
     * @ORM\Column(name="fechaout", type="date")
     */
    private $fechaout;

    /**
     * @var integer $numnoches
     *
     * @ORM\Column(name="numnoches", type="integer")
     */
    private $numnoches;

    /**
     * @var integer $numadultos
     *
     * @ORM\Column(name="numadultos", type="integer")
     */
    private $numadultos;

    /**
     * @var integer $numninos
     *
     * @ORM\Column(name="numninos", type="integer")
     */
    private $numninos;

    /**
     * @var integer $numhabitaciones
     *
     * @ORM\Column(name="numhabitaciones", type="integer")
     */
    private $numhabitaciones;

    /**
     * @var decimal $preciototaloferta
     *
     * @ORM\Column(name="preciototaloferta", type="decimal", precision=18, scale=12, nullable="true")
     */
    private $preciototaloferta;

    /**
     * @var string $idoferta
     *
     * @ORM\Column(name="idoferta", type="string", length=255)
     */
    private $idoferta;

    /**
     * @var string $idofertaunico
     *
     * @ORM\Column(name="idofertaunico", type="string", length=255)
     */
    private $idofertaunico;

    /**
     * @var datetime $fechacontraoferta
     *
     * @ORM\Column(name="fechacontraoferta", type="datetime", nullable="true")
     */
    private $fechacontraoferta;

    /**
     * @var decimal $preciototalcontraoferta
     *
     * @ORM\Column(name="preciototalcontraoferta", type="decimal", precision=18, scale=12, nullable="true")
     */
    private $preciototalcontraoferta;

    /**
     * @var datetime $fechaaceptadaoferta
     *
     * @ORM\Column(name="fechaaceptadaoferta", type="datetime", nullable="true")
     */
    private $fechaaceptadaoferta;

    /**
     * @var boolean $envio1
     *
     * @ORM\Column(name="envio1", type="boolean", nullable="true")
     */
    private $envio1;

    /**
     * @var boolean $envio2
     *
     * @ORM\Column(name="envio2", type="boolean", nullable="true")
     */
    private $envio2;

    /**
     * @var boolean $envio3
     *
     * @ORM\Column(name="envio3", type="boolean", nullable="true")
     */
    private $envio3;

    /**
     * @var boolean $envio4
     *
     * @ORM\Column(name="envio4", type="boolean", nullable="true")
     */
    private $envio4;

    /**
     * @ORM\OneToOne(targetEntity="Sale")
     * @ORM\JoinColumn(name="sale_id",referencedColumnName="id", onDelete="CASCADE")
     */
    private $sale;

    /**
     * @var string $status
     * @ORM\Column(name="status", type="integer", nullable="true")
     */
    private $status;

    /**
     * @var string $group_status
     * @ORM\Column(name="group_status", type="integer", nullable="true")
     */
    private $group_status;

    /**
     * @var boolean $envio4
     *
     * @ORM\Column(name="notificacion_contraoferta", type="boolean", nullable="true")
     */
    private $notificacion_contraoferta = FALSE;

    /**
     * 
     * @ORM\Column(name="comision_oferta", type="decimal", precision=18, scale=2, nullable="true")
     */
    private $comision_oferta;

    /**
     * @var \usuario
     *
     * @ORM\ManyToOne(targetEntity="User",cascade={"remove"})
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="usuario_hotel", referencedColumnName="id",onDelete="CASCADE", nullable="true")
     * })
     */
    private $usuario_hotel;

    /**
     * 
     * @ORM\Column(name="neto_hotel_pagado", type="decimal", precision=18, scale=2, nullable="true")
     */
    private $neto_hotel_pagado;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get usuario
     *
     * @return \Hoteles\BackendBundle\Entity\User
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set usuario
     *
     * @param \Hoteles\BackendBundle\Entity\User $usuario
     * @return Event
     */
    public function setUsuario(\Hoteles\BackendBundle\Entity\User $usuario = null)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Set fecha
     *
     * @param datetime $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * Get fecha
     *
     * @return datetime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Get hotel
     *
     * @return \Hoteles\BackendBundle\Entity\Hotel
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    /**
     * Set hotel
     *
     * @param \Hoteles\BackendBundle\Entity\Hotel $hotel
     * @return Event
     */
    public function setHotel(\Hoteles\BackendBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Set preciohabitacion
     *
     * @param decimal $preciohabitacion
     */
    public function setPreciohabitacion($preciohabitacion)
    {
        $this->preciohabitacion = $preciohabitacion;
    }

    /**
     * Get preciohabitacion
     *
     * @return decimal 
     */
    public function getPreciohabitacion()
    {
        return $this->preciohabitacion;
    }

    /**
     * Set fechain
     *
     * @param date $fechain
     */
    public function setFechain($fechain)
    {
        $this->fechain = $fechain;
    }

    /**
     * Get fechain
     *
     * @return date 
     */
    public function getFechain()
    {
        return $this->fechain;
    }

    /**
     * Set fechaout
     *
     * @param date $fechaout
     */
    public function setFechaout($fechaout)
    {
        $this->fechaout = $fechaout;
    }

    /**
     * Get fechaout
     *
     * @return date 
     */
    public function getFechaout()
    {
        return $this->fechaout;
    }

    /**
     * Set numnoches
     *
     * @param integer $numnoches
     */
    public function setNumnoches($numnoches)
    {
        $this->numnoches = $numnoches;
    }

    /**
     * Get numnoches
     *
     * @return integer 
     */
    public function getNumnoches()
    {
        return $this->numnoches;
    }

    /**
     * Set numadultos
     *
     * @param integer $numadultos
     */
    public function setNumadultos($numadultos)
    {
        $this->numadultos = $numadultos;
    }

    /**
     * Get numadultos
     *
     * @return integer 
     */
    public function getNumadultos()
    {
        return $this->numadultos;
    }

    /**
     * Set numninos
     *
     * @param integer $numninos
     */
    public function setNumninos($numninos)
    {
        $this->numninos = $numninos;
    }

    /**
     * Get numninos
     *
     * @return integer 
     */
    public function getNumninos()
    {
        return $this->numninos;
    }

    /**
     * Set numhabitaciones
     *
     * @param integer $numhabitaciones
     */
    public function setNumhabitaciones($numhabitaciones)
    {
        $this->numhabitaciones = $numhabitaciones;
    }

    /**
     * Get numhabitaciones
     *
     * @return integer 
     */
    public function getNumhabitaciones()
    {
        return $this->numhabitaciones;
    }

    /**
     * Set preciototaloferta
     *
     * @param decimal $preciototaloferta
     */
    public function setPreciototaloferta($preciototaloferta)
    {
        $this->preciototaloferta = $preciototaloferta;
    }

    /**
     * Get preciototaloferta
     *
     * @return decimal 
     */
    public function getPreciototaloferta()
    {
        return $this->preciototaloferta;
    }

    /**
     * Set idoferta
     *
     * @param string $idoferta
     */
    public function setIdoferta($idoferta)
    {
        $this->idoferta = $idoferta;
    }

    /**
     * Get idoferta
     *
     * @return string 
     */
    public function getIdoferta()
    {
        return $this->idoferta;
    }

    /**
     * Set idofertaunico
     *
     * @param string $idofertaunico
     */
    public function setIdofertaunico($idofertaunico)
    {
        $this->idofertaunico = $idofertaunico;
    }

    /**
     * Get idofertaunico
     *
     * @return string 
     */
    public function getIdofertaunico()
    {
        return $this->idofertaunico;
    }

    /**
     * Set fechacontraoferta
     *
     * @param datetime $fechacontraoferta
     */
    public function setFechacontraoferta($fechacontraoferta)
    {
        $this->fechacontraoferta = $fechacontraoferta;
    }

    /**
     * Get fechacontraoferta
     *
     * @return datetime 
     */
    public function getFechacontraoferta()
    {
        return $this->fechacontraoferta;
    }

    /**
     * Set fechaaceptadaoferta
     *
     * @param datetime $fechaaceptadaoferta
     */
    public function setFechaaceptadaoferta($fechaaceptadaoferta)
    {
        $this->fechaaceptadaoferta = $fechaaceptadaoferta;
    }

    /**
     * Get fechaaceptadaoferta
     *
     * @return datetime 
     */
    public function getFechaaceptadaoferta()
    {
        return $this->fechaaceptadaoferta;
    }

    /**
     * Set preciototalcontraoferta
     *
     * @param decimal $preciototalcontraoferta
     */
    public function setPreciototalcontraoferta($preciototalcontraoferta)
    {
        $this->preciototalcontraoferta = $preciototalcontraoferta;
    }

    /**
     * Get preciototalcontraoferta
     *
     * @return decimal 
     */
    public function getPreciototalcontraoferta()
    {
        return $this->preciototalcontraoferta;
    }

    /**
     * Set envio1
     *
     * @param boolean $envio1
     */
    public function setEnvio1($envio1)
    {
        $this->envio1 = $envio1;
    }

    /**
     * Get envio1
     *
     * @return boolean 
     */
    public function getEnvio1()
    {
        return $this->envio1;
    }

    /**
     * Set envio2
     *
     * @param boolean $envio2
     */
    public function setEnvio2($envio2)
    {
        $this->envio2 = $envio2;
    }

    /**
     * Get envio2
     *
     * @return boolean 
     */
    public function getEnvio2()
    {
        return $this->envio2;
    }

    /**
     * Set envio3
     *
     * @param boolean $envio3
     */
    public function setEnvio3($envio3)
    {
        $this->envio3 = $envio3;
    }

    /**
     * Get envio3
     *
     * @return boolean 
     */
    public function getEnvio3()
    {
        return $this->envio3;
    }

    /**
     * Set envio4
     *
     * @param boolean $envio4
     */
    public function setEnvio4($envio4)
    {
        $this->envio4 = $envio4;
    }

    /**
     * Get envio4
     *
     * @return boolean 
     */
    public function getEnvio4()
    {
        return $this->envio4;
    }

    /**
     * Set status
     *
     * @param integer $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set group_status
     *
     * @param integer $group_status
     */
    public function setGroupStatus($group_status)
    {
        $this->group_status = $group_status;
        return $this;
    }

    /**
     * Get group_status
     *
     * @return integer 
     */
    public function getGroupStatus()
    {
        return $this->group_status;
    }

    public function getSale()
    {
        return $this->sale;
    }

    public function setSale(Sale $sale)
    {
        $this->sale = $sale;
        return $this;
    }

    private function updateStatus($status)
    {

        $this->setStatus($status);
    }

    public function markOfferPayed()
    {
        $this->updateStatus(Oferta::OFERTA_PAGADA);
        $sale = $this->getSale();
        $sale->markAsPayed();
        $this->setNetoHotelPagado($this->getNeto());
    }

    public function markOfferNotPayed()
    {
        $this->updateStatus(Oferta::OFERTA_NO_PAGADA);
        $sale = $this->getSale();
        $sale->markAsExpired();
    }

    public function markOfferAccepted(User $user)
    {
        $this->updateStatus(Oferta::OFERTA_ACEPTADA);
        $this->setFechaaceptadaoferta(new \DateTime());
        $this->setUsuarioHotel($user);
    }

    public function markOfferRejected(User $user)
    {
        $this->updateStatus(Oferta::OFERTA_RECHAZADA);
        $this->setUsuarioHotel($user);
    }

    public function markOfferCounterOfferAccepted()
    {
        $this->updateStatus(Oferta::OFERTA_CONTRAOFERTA_ACEPTADA);
    }

    public function markOfferCounterOfferRejected()
    {
        $this->updateStatus(Oferta::OFERTA_CONTRAOFERTA_RECHAZADA);
    }

    public function markOfferExpired()
    {
        $this->updateStatus(Oferta::OFERTA_CADUCADA);
    }

    public function markCounterOfferMade(User $user)
    {
        $this->updateStatus(Oferta::OFERTA_CONTRAOFERTA_REALIZADA);
        $this->setUsuarioHotel($user);
        $this->setFechacontraoferta(new \DateTime());
    }

    public function markCounterOfferRejected()
    {
        $this->updateStatus(Oferta::OFERTA_CONTRAOFERTA_RECHAZADA);
        if (!is_null($this->getSale())) {
            $this->getSale()->markAsCounterOfferRejected();
        }
    }

    public function markCounterOfferAnulment()
    {
        $this->updateStatus(Oferta::OFERTA_CONTRAOFERTA_ANULADA);
    }

    public function markCounterOfferExpired()
    {
        $this->updateStatus(Oferta::OFERTA_CONTRAOFERTA_CADUCADA);
        $sale = $this->getSale();
        $sale->markAsExpired();
    }

    public function markCounterOfferPayed()
    {
        $this->updateStatus(Oferta::OFERTA_CONTRAOFERTA_PAGADA);
        $sale = $this->getSale();
        $sale->markAsPayed();
        $this->setNetoHotelPagado($this->getNeto());
    }

    public function markOfferOtherHotelAccepted()
    {
        $this->updateStatus(Oferta::OFERTA_HOTEL_ACEPTADA);
    }

    public function markOfferHotelSuspended(){
        $this->updateStatus(Oferta::OFERTA_HOTEL_SUSPENDED_HOTEL);
    }
    //Ofertas grupo
    public function markGroupOfferAccepted()
    {
        $this->setGroupStatus(Oferta::GRUPO_OFERTA_ESTADO_ACEPTADA);
    }

    public function markGroupOfferExpired()
    {
        $this->setGroupStatus(Oferta::GRUPO_OFERTA_ESTADO_CADUCADO);
    }

    //Contraofertas grupo
    public function markGroupOfferCounterOffered()
    {
        $this->setGroupStatus(Oferta::GRUPO_OFERTA_CONTRAOFERTA_REALIZADA);
    }

    public function markGroupOfferCounterOfferExpired()
    {
        $this->setGroupStatus(Oferta::GRUPO_OFERTA_CONTRAOFERTA_CADUCADA);
    }

    public function markGroupOfferCounterOfferValidated()
    {
        $this->setGroupStatus(Oferta::GRUPO_OFERTA_CONTRAOFERTA_VALIDA);
    }

    public function canHotelInteract()
    {
        if ($this->getStatus() === self::OFERTA_VALIDA) {
            return true;
        }
        return false;
    }

    public function markFirstNoticeSent()
    {
        $this->setEnvio1(true);
    }

    public function isFirstNoticeSent()
    {
        return is_null($this->getEnvio1()) === false;
    }

    public function markSecondNoticeSent()
    {
        $this->setEnvio2(true);
    }

    public function isSecondNoticeSent()
    {
        return is_null($this->getEnvio2()) === false;
    }

    public function markThirdNoticeSent()
    {
        $this->setEnvio3(true);
    }

    public function isThirdNoticeSent()
    {
        return is_null($this->getEnvio3()) === false;
    }

    public function debug()
    {
        $a = $this->isFirstNoticeSent() ? 'Si' : 'No';
        $b = $this->isSecondNoticeSent() ? 'Si' : 'No';
        $c = $this->isThirdNoticeSent() ? 'Si' : 'No';
        $resultado = "\nPrimera notificacion: " . $a;
        $resultado .= "\nSegunda notificacion: " . $b;
        $resultado .= "\nSegunda notificacion: " . $c;
        $resultado .= "\nFecha aceptacion" . $this->getFechaaceptadaoferta()->format('Y-m-d H:i:s');
        return $resultado;
    }

    public function updateCounterOfferDate()
    {
        $this->setFechacontraoferta(new \DateTime());
    }

    public function markCounterOfferNoticeSent()
    {
        $this->setNotificacionContraoferta(true);
    }

    public function isGroupOfferValidated()
    {
        return $this->getGroupStatus() === Oferta::GRUPO_OFERTA_CONTRAOFERTA_VALIDA;
    }

    public function isGroupOfferExpired()
    {
        return $this->getGroupStatus() === Oferta::GRUPO_OFERTA_CONTRAOFERTA_CADUCADA ||
                $this->getGroupStatus() === Oferta::GRUPO_OFERTA_ESTADO_CADUCADO;
    }

    /**
     * Set notificacion_contraoferta
     *
     * @param boolean $notificacionContraoferta
     */
    public function setNotificacionContraoferta($notificacionContraoferta)
    {
        $this->notificacion_contraoferta = $notificacionContraoferta;
    }

    /**
     * Get notificacion_contraoferta
     *
     * @return boolean 
     */
    public function getNotificacionContraoferta()
    {
        return $this->notificacion_contraoferta;
    }

    //Si el campo de precio de contraoferta es diferente de null, es una contraoferta
    public function isContraoferta()
    {
        return !is_null($this->getPreciototalcontraoferta());
    }

    public function getNeto()
    {
        $neto = 0;
        if ($this->isContraoferta()) {
            $neto = $this->getPreciototalcontraoferta() * (100 - $this->getComisionOferta()) / 100;
        } else {
            $neto = $this->getPreciototaloferta() * (100 - $this->getComisionOferta()) / 100;
        }
        return $neto;
    }

    public function getPrecioPorHabitacion($precioContraoferta)
    {
        $precio_por_habitacion = $precioContraoferta / ( $this->getNumnoches() * $this->getNumhabitaciones());
        return $precio_por_habitacion;
    }

    /**
     * Set comision_oferta
     *
     * @param decimal $comisionOferta
     */
    public function setComisionOferta($comisionOferta)
    {
        $this->comision_oferta = $comisionOferta;
    }

    /**
     * Get comision_oferta
     *
     * @return decimal 
     */
    public function getComisionOferta()
    {
        return $this->comision_oferta;
    }

    /**
     * Set usuario_hotel
     *
     * @param Hoteles\BackendBundle\Entity\User $usuarioHotel
     */
    public function setUsuarioHotel(\Hoteles\BackendBundle\Entity\User $usuarioHotel)
    {
        $this->usuario_hotel = $usuarioHotel;
    }

    /**
     * Get usuario_hotel
     *
     * @return Hoteles\BackendBundle\Entity\User 
     */
    public function getUsuarioHotel()
    {
        return $this->usuario_hotel;
    }

    /**
     * Set neto_hotel_pagado
     *
     * @param decimal $netoHotelPagado
     */
    public function setNetoHotelPagado($netoHotelPagado)
    {
        $this->neto_hotel_pagado = $netoHotelPagado;
    }

    /**
     * Get neto_hotel_pagado
     *
     * @return decimal 
     */
    public function getNetoHotelPagado()
    {
        return $this->neto_hotel_pagado;
    }

    public function removeOfferBanFromUser()
    {
        $this->getUsuario()->removeOfferMade();
    }

    public function canBePayed()
    {
        $resultado = $this->getStatus() === Oferta::OFERTA_ACEPTADA ||
                $this->getStatus() === Oferta::OFERTA_CONTRAOFERTA_REALIZADA;
        return $resultado;
    }

}
