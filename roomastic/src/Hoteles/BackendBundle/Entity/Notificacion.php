<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Hoteles\BackendBundle\Entity\Notificaciones
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Notificacion
{

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \hotel
     *
     * @ORM\OneToOne(targetEntity="Hotel")
     * @ORM\JoinColumn(name="hotel", referencedColumnName="id")
     */
    private $hotel;

    /**
     * @var integer $ofertas_no_pagadas
     *
     * @ORM\Column(name="ofertas_no_pagadas", type="integer")
     */
    private $ofertas_no_pagadas;

    /**
     *
     * @var DateTime $fecha_ultima_oferta_no_pagada
     * @ORM\Column(name="fecha_ultima_oferta_no_pagada", type="datetime", nullable=true)
     */
    private $fecha_ultima_oferta_no_pagada;

    /**
     * @var integer $ofertas_pendientes
     *
     * @ORM\Column(name="ofertas_pendientes", type="integer")
     */
    private $ofertas_pendientes;

    /**
     *
     * @var DateTime $fecha_ultima_oferta_pendiente
     * @ORM\Column(name="fecha_ultima_oferta_pendiente", type="datetime", nullable=true)
     */
    private $fecha_ultima_oferta_pendiente;

    /**
     * @var integer $ofertas_pagadas
     *
     * @ORM\Column(name="ofertas_pagadas", type="integer")
     */
    private $ofertas_pagadas;

    /**
     *
     * @var DateTime $fecha_ultima_oferta_pagada
     * @ORM\Column(name="fecha_ultima_oferta_pagada", type="datetime", nullable=true)
     */
    private $fecha_ultima_oferta_pagada;

    /**
     * @var integer $contraofertas_perdidas 
     *
     * @ORM\Column(name="contraofertas_perdidas", type="integer")
     */
    private $contraofertas_perdidas;

    /**
     *
     * @var DateTime $fecha_ultima_contraoferta_perdida
     * @ORM\Column(name="fecha_ultima_contraoferta_perdida", type="datetime", nullable=true)
     */
    private $fecha_ultima_contraoferta_perdida;

    public function __construct()
    {
        $this->setContraofertasPerdidas(0);
        $this->setOfertasNoPagadas(0);
        $this->setOfertasPagadas(0);
        $this->setOfertasPendientes(0);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ofertas_no_pagadas
     *
     * @param integer $ofertasNoPagadas
     */
    public function setOfertasNoPagadas($ofertasNoPagadas)
    {
        $this->ofertas_no_pagadas = $ofertasNoPagadas;
    }

    /**
     * Get ofertas_no_pagadas
     *
     * @return integer 
     */
    public function getOfertasNoPagadas()
    {
        return $this->ofertas_no_pagadas;
    }

    /**
     * Set ofertas_pendientes
     *
     * @param integer $ofertasPendientes
     */
    public function setOfertasPendientes($ofertasPendientes)
    {
        $this->ofertas_pendientes = $ofertasPendientes;
    }

    /**
     * Get ofertas_pendientes
     *
     * @return integer 
     */
    public function getOfertasPendientes()
    {
        return $this->ofertas_pendientes;
    }

    /**
     * Set ofertas_pagadas
     *
     * @param integer $ofertasPagadas
     */
    public function setOfertasPagadas($ofertasPagadas)
    {
        $this->ofertas_pagadas = $ofertasPagadas;
    }

    /**
     * Get ofertas_pagadas
     *
     * @return integer 
     */
    public function getOfertasPagadas()
    {
        return $this->ofertas_pagadas;
    }

    /**
     * Set contraofertas_perdidas
     *
     * @param integer $contraofertasPerdidas
     */
    public function setContraofertasPerdidas($contraofertasPerdidas)
    {
        $this->contraofertas_perdidas = $contraofertasPerdidas;
    }

    /**
     * Get contraofertas_perdidas
     *
     * @return integer 
     */
    public function getContraofertasPerdidas()
    {
        return $this->contraofertas_perdidas;
    }

    /**
     * Set hotel
     *
     * @param Hoteles\BackendBundle\Entity\Hotel $hotel
     */
    public function setHotel(\Hoteles\BackendBundle\Entity\Hotel $hotel)
    {
        $this->hotel = $hotel;
    }

    /**
     * Get hotel
     *
     * @return Hoteles\BackendBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }

    public function addOfertaNoPagadas($increment = 1)
    {
        $this->setOfertasNoPagadas($this->getOfertasNoPagadas() + $increment);
        $this->setFechaUltimaOfertaNoPagada(new \DateTime());
    }

    public function addOfertaPendiente($increment = 1)
    {
        $this->setOfertasPendientes($this->getOfertasPendientes() + $increment);
        $this->setFechaUltimaOfertaPendiente(new \DateTime());
    }

    public function addOfertaPagadas($increment = 1)
    {
        $this->setOfertasPagadas($this->getOfertasPagadas() + $increment);
        $this->setFechaUltimaOfertaPagada(new \DateTime());
    }

    public function addContraOfertaPerdida($increment = 1)
    {
        $this->setContraofertasPerdidas($this->getContraofertasPerdidas() + $increment);
        $this->setFechaUltimaContraofertaPerdida(new \DateTime());
    }

    public function resetOfertasNoPagadas()
    {
        $this->setOfertasNoPagadas(0);
    }

    public function resetOfertasPagadas()
    {
        $this->setOfertasPagadas(0);
    }

    public function resetOfertasPendientes()
    {
        $this->setOfertasPendientes(0);
    }

    public function resetContraofertasPerdidas()
    {
        $this->setContraofertasPerdidas(0);
    }

    public function getNumNotificaciones()
    {
        $resultado = 0;
        $resultado += $this->getContraofertasPerdidas();
        $resultado += $this->getOfertasNoPagadas();
        $resultado += $this->getOfertasPagadas();
        $resultado += $this->getOfertasPendientes();
        return $resultado;
    }

    /**
     * Set fecha_ultima_oferta_no_pagada
     *
     * @param datetime $fechaUltimaOfertaNoPagada
     */
    public function setFechaUltimaOfertaNoPagada($fechaUltimaOfertaNoPagada)
    {
        $this->fecha_ultima_oferta_no_pagada = $fechaUltimaOfertaNoPagada;
    }

    /**
     * Get fecha_ultima_oferta_no_pagada
     *
     * @return datetime 
     */
    public function getFechaUltimaOfertaNoPagada()
    {
        return $this->fecha_ultima_oferta_no_pagada;
    }

    /**
     * Set fecha_ultima_oferta_pendiente
     *
     * @param datetime $fechaUltimaOfertaPendiente
     */
    public function setFechaUltimaOfertaPendiente($fechaUltimaOfertaPendiente)
    {
        $this->fecha_ultima_oferta_pendiente = $fechaUltimaOfertaPendiente;
    }

    /**
     * Get fecha_ultima_oferta_pendiente
     *
     * @return datetime 
     */
    public function getFechaUltimaOfertaPendiente()
    {
        return $this->fecha_ultima_oferta_pendiente;
    }

    /**
     * Set fecha_ultima_oferta_pagada
     *
     * @param datetime $fechaUltimaOfertaPagada
     */
    public function setFechaUltimaOfertaPagada($fechaUltimaOfertaPagada)
    {
        $this->fecha_ultima_oferta_pagada = $fechaUltimaOfertaPagada;
    }

    /**
     * Get fecha_ultima_oferta_pagada
     *
     * @return datetime 
     */
    public function getFechaUltimaOfertaPagada()
    {
        return $this->fecha_ultima_oferta_pagada;
    }

    /**
     * Set fecha_ultima_contraoferta_perdida
     *
     * @param datetime $fechaUltimaContraofertaPerdida
     */
    public function setFechaUltimaContraofertaPerdida($fechaUltimaContraofertaPerdida)
    {
        $this->fecha_ultima_contraoferta_perdida = $fechaUltimaContraofertaPerdida;
    }

    /**
     * Get fecha_ultima_contraoferta_perdida
     *
     * @return datetime 
     */
    public function getFechaUltimaContraofertaPerdida()
    {
        return $this->fecha_ultima_contraoferta_perdida;
    }

}