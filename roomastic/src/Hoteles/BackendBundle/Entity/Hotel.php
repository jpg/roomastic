<?php

namespace Hoteles\BackendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Hoteles\BackendBundle\Entity\Empresa;

/**
 * Hoteles\BackendBundle\Entity\Hotel
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Hoteles\BackendBundle\Repository\HotelRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Hotel
{

    const HOTEL_STATUS_NOT_PUBLIC = 0;
    const HOTEL_STATUS_VISIBLE = 1;
    const HOTEL_STATUS_SUSPENDED = 2;

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     * 
     */
    private $email;

    /**
     * @var string $url
     *
     * @ORM\Column(name="url", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $url;

    /**
     * @var \empresa
     *
     * @ORM\ManyToOne(targetEntity="Empresa")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="empresa", referencedColumnName="id")
     * })
     */
    private $empresa;

    /**
     * @var string $nombreempresa
     *
     * @ORM\Column(name="nombreempresa", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $nombreempresa;

    /**
     * @var string $cif
     *
     * @ORM\Column(name="cif", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $cif;

    /**
     * @var string $direccionfacturacion
     *
     * @ORM\Column(name="direccionfacturacion", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $direccionfacturacion;

    /**
     * @var string $telefono
     *
     * @ORM\Column(name="telefono", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $telefono;

    /**
     * @var string $numerocuenta
     *
     * @ORM\Column(name="numerocuenta", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $numerocuenta;

    /**
     * @var string $nombrepersonacontacto
     *
     * @ORM\Column(name="nombrepersonacontacto", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $nombrepersonacontacto;

    /**
     * @var string $apellidospersonacontacto
     *
     * @ORM\Column(name="apellidospersonacontacto", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $apellidospersonacontacto;

    /**
     * @var string $cargopersonacontacto
     *
     * @ORM\Column(name="cargopersonacontacto", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $cargopersonacontacto;

    /**
     * @var string $emailpersonacontacto
     *
     * @ORM\Column(name="emailpersonacontacto", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     * 
     */
    private $emailpersonacontacto;

    /**
     * @var string $telefonopersonacontacto
     *
     * @ORM\Column(name="telefonopersonacontacto", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $telefonopersonacontacto;

    /**
     * @var string $nombrehotel
     *
     * @ORM\Column(name="nombrehotel", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $nombrehotel;

    /**
     * @var integer $calificacion
     *
     * @ORM\Column(name="calificacion", type="integer")
     * @Assert\NotBlank()
     * 
     */
    private $calificacion;

    /**
     * @var string $descripcion
     *
     * @ORM\Column(name="descripcion", type="string", length=300)
     * @Assert\NotBlank()
     * 
     */
    private $descripcion;

    /**
     * @var string $caracteristicas
     *
     * @ORM\Column(name="caracteristicas", type="string", length=300)
     * @Assert\NotBlank()
     * 
     */
    private $caracteristicas;

    /**
     * @var string $ubicacion
     *
     * @ORM\Column(name="ubicacion", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $ubicacion;

    /**
     * @var string $imagenes
     *
     * @ORM\Column(name="imagenes", type="string", length=10, nullable="true")
     * 
     */
    private $imagenes;

    /**
     * @var boolean $piscina
     *
     * @ORM\Column(name="piscina", type="boolean")
     */
    private $piscina;

    /**
     * @var boolean $spa
     *
     * @ORM\Column(name="spa", type="boolean")
     */
    private $spa;

    /**
     * @var boolean $wi_fi
     *
     * @ORM\Column(name="wi_fi", type="boolean")
     */
    private $wi_fi;

    /**
     * @var boolean $acceso_adaptado
     *
     * @ORM\Column(name="acceso_adaptado", type="boolean")
     */
    private $acceso_adaptado;

    /**
     * @var boolean $aceptan_perros
     *
     * @ORM\Column(name="aceptan_perros", type="boolean")
     */
    private $aceptan_perros;

    /**
     * @var boolean $aparcamiento
     *
     * @ORM\Column(name="aparcamiento", type="boolean")
     */
    private $aparcamiento;

    /**
     * @var boolean $business_center
     *
     * @ORM\Column(name="business_center", type="boolean")
     */
    private $business_center;

    /**
     * @var string $pvpoficialenteros
     *
     * @ORM\Column(name="pvpoficialenteros", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $pvpoficialenteros;

    /**
     * @var string $pvpoficialdecimales
     *
     * @ORM\Column(name="pvpoficialdecimales", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $pvpoficialdecimales;

    /**
     * @var string $cantidadminimaaceptadaenteros
     *
     * @ORM\Column(name="cantidadminimaaceptadaenteros", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $cantidadminimaaceptadaenteros;

    /**
     * @var string $cantidadminimaaceptadadecimales
     *
     * @ORM\Column(name="cantidadminimaaceptadadecimales", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $cantidadminimaaceptadadecimales;

    /**
     * @var boolean $status
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var \provincia
     *
     * @ORM\ManyToOne(targetEntity="Provincia")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="provincia", referencedColumnName="id")
     * })
     */
    private $provincia;

    /**
     * @var \municipio
     *
     * @ORM\ManyToOne(targetEntity="Municipio")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="municipio", referencedColumnName="id")
     * })
     */
    private $municipio;

    /**
     * @var \lugar
     *
     * @ORM\ManyToOne(targetEntity="Lugar")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="lugar", referencedColumnName="id",nullable="true")
     * })
     */
    private $lugar;

    /**
     * @var string $seotitulo
     *
     * @ORM\Column(name="seotitulo", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $seotitulo;

    /**
     * @var string $seodescripcion
     *
     * @ORM\Column(name="seodescripcion", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $seodescripcion;

    /**
     * @var string $seokeywords
     *
     * @ORM\Column(name="seokeywords", type="string", length=255)
     * @Assert\NotBlank()
     * 
     */
    private $seokeywords;

    /**
     *
     * @var type 
     * @ORM\Column(name="comision_hotel_entero", type="integer")
     */
    private $comision_hotel_entero;

    /**
     *
     * @var type 
     * @ORM\Column(name="comision_hotel_decimal", type="integer")
     */
    private $comision_hotel_decimal;

    /**
     * /**
     * @ORM\OneToMany(targetEntity="Imagenhotel", mappedBy="hotel")
     * */
    private $imagenes_del_hotel;

    /**
     * 
     * @ORM\Column(name="latitud", type="decimal", precision=18, scale=12, nullable="true")
     */
    private $latitud;

    /**
     * 
     * @ORM\Column(name="longitud", type="decimal", precision=18, scale=12, nullable="true")
     */
    private $longitud;

    /**
     * @var string $seokeywords
     *
     * @ORM\Column(name="filtro_servicios", type="string", length=1000)
     * 
     */
    private $filtro_servicios;

    /**
     * @ORM\OneToMany(targetEntity="UserHotel", mappedBy="hotel")
     * */
    private $user_hotel;

    /**
     * @ORM\OneToOne(targetEntity="Notificacion", mappedBy="hotel")
     * */
    private $notificaciones;

    public function __construct()
    {
        $this->imagenes_del_hotel = new ArrayCollection();
        $this->user_hotel = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set url
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get empresa
     *
     * @return \Hoteles\BackendBundle\Entity\Empresa
     */
    public function getEmpresa()
    {
        return $this->empresa;
    }

    /**
     * Set empresa
     *
     * @param \Hoteles\BackendBundle\Entity\Empresa $empresa
     * @return Event
     */
    public function setEmpresa(\Hoteles\BackendBundle\Entity\Empresa $empresa = null)
    {
        $this->empresa = $empresa;

        return $this;
    }

    /**
     * Set nombreempresa
     *
     * @param string $nombreempresa
     */
    public function setNombreempresa($nombreempresa)
    {
        $this->nombreempresa = $nombreempresa;
    }

    /**
     * Get nombreempresa
     *
     * @return string 
     */
    public function getNombreempresa()
    {
        return $this->nombreempresa;
    }

    /**
     * Set cif
     *
     * @param string $cif
     */
    public function setCif($cif)
    {
        $this->cif = $cif;
    }

    /**
     * Get cif
     *
     * @return string 
     */
    public function getCif()
    {
        return $this->cif;
    }

    /**
     * Set direccionfacturacion
     *
     * @param string $direccionfacturacion
     */
    public function setDireccionfacturacion($direccionfacturacion)
    {
        $this->direccionfacturacion = $direccionfacturacion;
    }

    /**
     * Get direccionfacturacion
     *
     * @return string 
     */
    public function getDireccionfacturacion()
    {
        return $this->direccionfacturacion;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set numerocuenta
     *
     * @param string $numerocuenta
     */
    public function setNumerocuenta($numerocuenta)
    {
        $this->numerocuenta = $numerocuenta;
    }

    /**
     * Get numerocuenta
     *
     * @return string 
     */
    public function getNumerocuenta()
    {
        return $this->numerocuenta;
    }

    /**
     * Set nombrepersonacontacto
     *
     * @param string $nombrepersonacontacto
     */
    public function setNombrepersonacontacto($nombrepersonacontacto)
    {
        $this->nombrepersonacontacto = $nombrepersonacontacto;
    }

    /**
     * Get nombrepersonacontacto
     *
     * @return string 
     */
    public function getNombrepersonacontacto()
    {
        return $this->nombrepersonacontacto;
    }

    /**
     * Set apellidospersonacontacto
     *
     * @param string $apellidospersonacontacto
     */
    public function setApellidospersonacontacto($apellidospersonacontacto)
    {
        $this->apellidospersonacontacto = $apellidospersonacontacto;
    }

    /**
     * Get apellidospersonacontacto
     *
     * @return string 
     */
    public function getApellidospersonacontacto()
    {
        return $this->apellidospersonacontacto;
    }

    /**
     * Set cargopersonacontacto
     *
     * @param string $cargopersonacontacto
     */
    public function setCargopersonacontacto($cargopersonacontacto)
    {
        $this->cargopersonacontacto = $cargopersonacontacto;
    }

    /**
     * Get cargopersonacontacto
     *
     * @return string 
     */
    public function getCargopersonacontacto()
    {
        return $this->cargopersonacontacto;
    }

    /**
     * Set emailpersonacontacto
     *
     * @param string $emailpersonacontacto
     */
    public function setEmailpersonacontacto($emailpersonacontacto)
    {
        $this->emailpersonacontacto = $emailpersonacontacto;
    }

    /**
     * Get emailpersonacontacto
     *
     * @return string 
     */
    public function getEmailpersonacontacto()
    {
        return $this->emailpersonacontacto;
    }

    /**
     * Set telefonopersonacontacto
     *
     * @param string $telefonopersonacontacto
     */
    public function setTelefonopersonacontacto($telefonopersonacontacto)
    {
        $this->telefonopersonacontacto = $telefonopersonacontacto;
    }

    /**
     * Get telefonopersonacontacto
     *
     * @return string 
     */
    public function getTelefonopersonacontacto()
    {
        return $this->telefonopersonacontacto;
    }

    /**
     * Set nombrehotel
     *
     * @param string $nombrehotel
     */
    public function setNombrehotel($nombrehotel)
    {
        $this->nombrehotel = $nombrehotel;
    }

    /**
     * Get nombrehotel
     *
     * @return string 
     */
    public function getNombrehotel()
    {
        return $this->nombrehotel;
    }

    /**
     * Set calificacion
     *
     * @param integer $calificacion
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;
    }

    /**
     * Get calificacion
     *
     * @return integer 
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set caracteristicas
     *
     * @param string $caracteristicas
     */
    public function setCaracteristicas($caracteristicas)
    {
        $this->caracteristicas = $caracteristicas;
    }

    /**
     * Get caracteristicas
     *
     * @return string 
     */
    public function getCaracteristicas()
    {
        return $this->caracteristicas;
    }

    /**
     * Set ubicacion
     *
     * @param string $ubicacion
     */
    public function setUbicacion($ubicacion)
    {
        $this->ubicacion = $ubicacion;
    }

    /**
     * Get ubicacion
     *
     * @return string 
     */
    public function getUbicacion()
    {
        return $this->ubicacion;
    }

    /**
     * Set imagenes
     *
     * @param string $imagenes
     */
    public function setImagenes($imagenes)
    {
        $this->imagenes = $imagenes;
    }

    /**
     * Get imagenes
     *
     * @return string 
     */
    public function getImagenes()
    {
        return $this->imagenes;
    }

    /**
     * Set pvpoficialenteros
     *
     * @param string $pvpoficialenteros
     */
    public function setPvpoficialenteros($pvpoficialenteros)
    {
        $this->pvpoficialenteros = $pvpoficialenteros;
    }

    /**
     * Get pvpoficialenteros
     *
     * @return string 
     */
    public function getPvpoficialenteros()
    {
        return $this->pvpoficialenteros;
    }

    /**
     * Set pvpoficialdecimales
     *
     * @param string $pvpoficialdecimales
     */
    public function setPvpoficialdecimales($pvpoficialdecimales)
    {
        $this->pvpoficialdecimales = $pvpoficialdecimales;
    }

    /**
     * Get pvpoficialdecimales
     *
     * @return string 
     */
    public function getPvpoficialdecimales()
    {
        return $this->pvpoficialdecimales;
    }

    /**
     * Set cantidadminimaaceptadaenteros
     *
     * @param string $cantidadminimaaceptadaenteros
     */
    public function setCantidadminimaaceptadaenteros($cantidadminimaaceptadaenteros)
    {
        $this->cantidadminimaaceptadaenteros = $cantidadminimaaceptadaenteros;
    }

    /**
     * Get cantidadminimaaceptadaenteros
     *
     * @return string 
     */
    public function getCantidadminimaaceptadaenteros()
    {
        return $this->cantidadminimaaceptadaenteros;
    }

    /**
     * Set cantidadminimaaceptadadecimales
     *
     * @param string $cantidadminimaaceptadadecimales
     */
    public function setCantidadminimaaceptadadecimales($cantidadminimaaceptadadecimales)
    {
        $this->cantidadminimaaceptadadecimales = $cantidadminimaaceptadadecimales;
    }

    /**
     * Get cantidadminimaaceptadadecimales
     *
     * @return string 
     */
    public function getCantidadminimaaceptadadecimales()
    {
        return $this->cantidadminimaaceptadadecimales;
    }

    /**
     * Set status
     *
     * @param boolean $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get provincia
     *
     * @return \Hoteles\BackendBundle\Entity\Provincia
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set provincia
     *
     * @param \Hoteles\BackendBundle\Entity\Provincia $provincia
     * @return Event
     */
    public function setProvincia(\Hoteles\BackendBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Set municipio
     *
     * @param \Hoteles\BackendBundle\Entity\Municipio $Municipio
     * @return Event
     */
    public function setMunicipio(\Hoteles\BackendBundle\Entity\Municipio $municipio = null)
    {
        $this->municipio = $municipio;
    }

    /**
     * Get municipio
     *
     * @return \Hoteles\BackendBundle\Entity\Municipio
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set lugar
     *
     * @param \Hoteles\BackendBundle\Entity\lugar $Lugar
     * @return Event
     */
    public function setLugar($lugar = null)
    {
        $this->lugar = $lugar;
    }

    /**
     * Get lugar
     *
     * @return \Hoteles\BackendBundle\Entity\Lugar
     */
    public function getLugar()
    {
        return $this->lugar;
    }

    /**
     * Set seotitulo
     *
     * @param string $seotitulo
     */
    public function setSeotitulo($seotitulo)
    {
        $this->seotitulo = $seotitulo;
    }

    /**
     * Get seotitulo
     *
     * @return string 
     */
    public function getSeotitulo()
    {
        return $this->seotitulo;
    }

    /**
     * Set seodescripcion
     *
     * @param string $seodescripcion
     */
    public function setSeodescripcion($seodescripcion)
    {
        $this->seodescripcion = $seodescripcion;
    }

    /**
     * Get seodescripcion
     *
     * @return string 
     */
    public function getSeodescripcion()
    {
        return $this->seodescripcion;
    }

    /**
     * Set seokeywords
     *
     * @param string $seokeywords
     */
    public function setSeokeywords($seokeywords)
    {
        $this->seokeywords = $seokeywords;
    }

    /**
     * Get seokeywords
     *
     * @return string 
     */
    public function getSeokeywords()
    {
        return $this->seokeywords;
    }

    public function __toString()
    {
        return $this->getNombrehotel();
    }

    /**
     * Set comision_hotel_entero
     *
     * @param integer $comisionHotelEntero
     */
    public function setComisionHotelEntero($comisionHotelEntero)
    {
        $this->comision_hotel_entero = $comisionHotelEntero;
    }

    /**
     * Get comision_hotel_entero
     *
     * @return integer 
     */
    public function getComisionHotelEntero()
    {
        return $this->comision_hotel_entero;
    }

    /**
     * Set comision_hotel_decimal
     *
     * @param integer $comisionHotelDecimal
     */
    public function setComisionHotelDecimal($comisionHotelDecimal)
    {
        $this->comision_hotel_decimal = $comisionHotelDecimal;
    }

    /**
     * Get comision_hotel_decimal
     *
     * @return integer 
     */
    public function getComisionHotelDecimal()
    {
        return $this->comision_hotel_decimal;
    }

    /**
     * Set piscina
     *
     * @param boolean $piscina
     */
    public function setPiscina($piscina)
    {
        $this->piscina = $piscina;
    }

    /**
     * Get piscina
     *
     * @return boolean 
     */
    public function getPiscina()
    {
        return $this->piscina;
    }

    /**
     * Set spa
     *
     * @param boolean $spa
     */
    public function setSpa($spa)
    {
        $this->spa = $spa;
    }

    /**
     * Get spa
     *
     * @return boolean 
     */
    public function getSpa()
    {
        return $this->spa;
    }

    /**
     * Set wi_fi
     *
     * @param boolean $wiFi
     */
    public function setWiFi($wiFi)
    {
        $this->wi_fi = $wiFi;
    }

    /**
     * Get wi_fi
     *
     * @return boolean 
     */
    public function getWiFi()
    {
        return $this->wi_fi;
    }

    /**
     * Set acceso_adaptado
     *
     * @param boolean $accesoAdaptado
     */
    public function setAccesoAdaptado($accesoAdaptado)
    {
        $this->acceso_adaptado = $accesoAdaptado;
    }

    /**
     * Get acceso_adaptado
     *
     * @return boolean 
     */
    public function getAccesoAdaptado()
    {
        return $this->acceso_adaptado;
    }

    /**
     * Set aceptan_perros
     *
     * @param boolean $aceptanPerros
     */
    public function setAceptanPerros($aceptanPerros)
    {
        $this->aceptan_perros = $aceptanPerros;
    }

    /**
     * Get aceptan_perros
     *
     * @return boolean 
     */
    public function getAceptanPerros()
    {
        return $this->aceptan_perros;
    }

    /**
     * Set aparcamiento
     *
     * @param boolean $aparcamiento
     */
    public function setAparcamiento($aparcamiento)
    {
        $this->aparcamiento = $aparcamiento;
    }

    /**
     * Get aparcamiento
     *
     * @return boolean 
     */
    public function getAparcamiento()
    {
        return $this->aparcamiento;
    }

    /**
     * Set business_center
     *
     * @param boolean $businessCenter
     */
    public function setBusinessCenter($businessCenter)
    {
        $this->business_center = $businessCenter;
    }

    /**
     * Get business_center
     *
     * @return boolean 
     */
    public function getBusinessCenter()
    {
        return $this->business_center;
    }

    /**
     * Add imagenes_del_hotel
     *
     * @param Hoteles\BackendBundle\Entity\Imagenhotel $imagenesDelHotel
     */
    public function addImagenhotel(\Hoteles\BackendBundle\Entity\Imagenhotel $imagenesDelHotel)
    {
        $this->imagenes_del_hotel[] = $imagenesDelHotel;
    }

    /**
     * Get imagenes_del_hotel
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getImagenesDelHotel()
    {
        return $this->imagenes_del_hotel;
    }

    //Primero ordena las imagenes y luego devuelve un array
    public function getOrderedImagenesDelHotel()
    {
        $array_de_imagenes = $this->imagenes_del_hotel->toArray();
        usort($array_de_imagenes, function($imagen_a, $imagen_b) {
            return strcmp($imagen_a->getOrden(), $imagen_b->getOrden());
        });
        return $array_de_imagenes;
    }

    //Me devuelve la carpeta en la que tengo guardada las imagenes
    public function getImagePath()
    {
        return "/uploads/hoteles";
    }

    /**
     * Set latitud
     *
     * @param decimal $latitud
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;
    }

    /**
     * Get latitud
     *
     * @return decimal 
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set longitud
     *
     * @param decimal $longitud
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;
    }

    /**
     * Get longitud
     *
     * @return decimal 
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

    /**
     * Set filtro_servicios
     *
     * @param string $filtroServicios
     */
    public function setFiltroServicios($filtroServicios)
    {
        $this->filtro_servicios = $filtroServicios;
    }

    /**
     * Get filtro_servicios
     *
     * @return string 
     */
    public function getFiltroServicios()
    {
        return $this->filtro_servicios;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function actualizarFiltroServicios()
    {
        $filtro = array();
        $filtro[] = ($this->getPiscina()) ? 'servicio-piscina' : '';
        $filtro[] = ($this->getAccesoAdaptado()) ? 'servicio-acceso-adaptado' : '';
        $filtro[] = ($this->getWiFi()) ? 'servicio-wi-fi' : '';
        $filtro[] = ($this->getBusinessCenter()) ? 'servicio-business-center' : '';
        $filtro[] = ($this->getAceptanPerros()) ? 'servicio-aceptan-perros' : '';
        $filtro[] = ($this->getAparcamiento()) ? 'servicio-aparcamiento' : '';
        $filtro[] = ($this->getSpa()) ? 'servicio-spa' : '';
        $filtro_servicios = implode(' ', $filtro);
        $this->setFiltroServicios($filtro_servicios);
    }

    function loadDatosEmpresa(Empresa $empresa)
    {
        $this->setEmpresa($empresa);
        $this->setDireccionfacturacion($empresa->getDireccionfacturacion());
        $this->setCif($empresa->getCif());
        $this->setNumerocuenta($empresa->getNumerocuenta());
        $this->setNombreempresa($empresa->getNombreempresa());
        $this->setComisionHotelEntero($empresa->getComisionEmpresaEntero());
        $this->setComisionHotelDecimal($empresa->getComisionEmpresaDecimal());
    }

    /**
     * Add user_hotel
     *
     * @param Hoteles\BackendBundle\Entity\UserHotel $userHotel
     */
    public function addUserHotel(\Hoteles\BackendBundle\Entity\UserHotel $userHotel)
    {
        $this->user_hotel[] = $userHotel;
    }

    /**
     * Get user_hotel
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getUserHotel()
    {
        return $this->user_hotel;
    }

    public function suspendHotel()
    {
        $this->setStatus(self::HOTEL_STATUS_SUSPENDED);
    }

    public function unsuspendHotel()
    {
        $this->setStatus(self::HOTEL_STATUS_VISIBLE);
    }

    public function hideHotel()
    {
        $this->setStatus(self::HOTEL_STATUS_NOT_PUBLIC);
    }

    public function showHotel()
    {
        $this->setStatus(self::HOTEL_STATUS_VISIBLE);
    }

    public function getPVP()
    {
        $resultado = $this->getPvpoficialenteros();
        $resultado += ((float) $this->getPvpoficialdecimales() / 100);
        return $resultado;
    }

    public function getPrecioMinimo()
    {
        $resultado = $this->getCantidadminimaaceptadaenteros();
        $resultado += ((float) $this->getCantidadminimaaceptadadecimales()) / 100;
        return $resultado;
    }

    public function getComision()
    {
        $resultado = $this->getComisionHotelEntero() + $this->getComisionHotelDecimal() / 100;
        return $resultado;
    }

    /**
     * Set notificaciones
     *
     * @param Hoteles\BackendBundle\Entity\Notificacion $notificaciones
     */
    public function setNotificaciones(\Hoteles\BackendBundle\Entity\Notificacion $notificaciones)
    {
        $this->notificaciones = $notificaciones;
    }

    /**
     * Get notificaciones
     *
     * @return Hoteles\BackendBundle\Entity\Notificacion 
     */
    public function getNotificaciones()
    {
        return $this->notificaciones;
    }

}