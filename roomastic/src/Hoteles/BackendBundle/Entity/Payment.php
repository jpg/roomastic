<?php

/**
 * Class Payment
 */

namespace Hoteles\BackendBundle\Entity;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Config\Definition\Exception\Exception;

class Payment
{

    private $entorno;
    private $url_prod;
    private $url_test;
    private $importe;
    private $moneda;
    private $pedido;
    private $merchantData;
    private $descripcion;
    private $titular;
    private $codigoFuc;
    private $terminal;
    private $transactionType;
    private $urlNotificacion;
    private $claveComercio;
    private $urlOK;
    private $urlKO;
    private $firma;
    private $nombreComercio;
    private $idioma;
    private $metodo;
    private $router;

    /**
     * Constructor
     */
    public function __construct(
    $url_prod, $url_test, $url_notificacion, $moneda, $terminal, $merchantData, $urlOK, $urlKO, $nombreComercio, $claveComercio, $codigoFuc, Router $router, $idioma = '001')
    {

        $this->url_prod = $url_prod;
        $this->url_test = $url_test;
        $this->moneda = $moneda;
        $this->terminal = $terminal;
        $this->merchantData = $merchantData;
        $this->nombreComercio = $nombreComercio;
        $this->claveComercio = $claveComercio;
        $this->codigoFuc = $codigoFuc;
        $this->transactionType = 0;
        $this->idioma = '001';
        $this->metodo = 'T';
        $this->urlKO = $urlKO;
        $this->urlOK = $urlOK;
        $this->urlNotificacion = $url_notificacion;
        $this->router = $router;
    }

    /**
     * Asignamos que tipo de entorno vamos a usar si Pruebas o Real (por defecto esta en pruebas)
     *
     * @param string $entorno (test,prod)
     */
    public function config_entorno($entorno = 'test')
    {
        if (trim($entorno) === 'prod') {
            //prod
            $this->entorno = $this->url_prod;
        } else if (trim($entorno) === 'test') {
            //test
            $this->entorno = $this->url_test;
        }
    }

    public function getClaveComercio()
    {
        return $this->claveComercio;
    }

    /**
     * Retornamos la URL que va en el form para los pagos ya sea en pruebas o real
     *
     * @return string URL sermepa (real o pruebas)
     */
    public function getEntorno()
    {
        return $this->entorno;
    }

    /**
     * Asignamos el importe de la compra
     *
     * @param int $importe_float    total de la compra a pagar
     * @return bool|float|int Retornamos el importe ya modificado
     */
    public function setImporte($importe_float = 0)
    {
        $importe = intval($this->parseFloat($importe_float) * 100);
        $this->importe = $importe;
        return $importe;
    }

    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * Asignamos el número de pedido a nuestra compra (Los 4 primeros dígitos deben ser numéricos)
     *
     * @param string $pedido Numero de pedido alfanumérico
     * @throws Exception
     */
    public function setPedido($pedido = '')
    {
        if (strlen(trim($pedido)) > 0) {
            $this->pedido = $pedido;
        } else {
            throw new Exception('Falta agregar el número de pedido');
        }
    }

    public function getPedido()
    {
        return $this->pedido;
    }

    public function getMetodo()
    {
        return $this->metodo;
    }

    public function getUrlOK()
    {
        return $this->urlOK;
    }

    public function getUrlKO()
    {
        return $this->urlKO;
    }

    public function getNombreComercio()
    {
        return $this->nombreComercio;
    }

    public function getUrlNotificacion()
    {
        return $this->urlNotificacion;
    }

    public function getTransactionType()
    {
        return $this->transactionType;
    }

    public function getTerminal()
    {
        return $this->terminal;
    }

    public function getCodigoFuc()
    {
        return $this->codigoFuc;
    }

    public function getMoneda()
    {
        return $this->moneda;
    }

    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Asignamos la descripción del producto (Obligatorio)
     *
     * @param string $producto Descripción del producto
     * @throws Exception
     */
    public function setDescripcion($producto = '')
    {
        if (strlen(trim($producto)) > 0 &&
                strlen(trim($producto)) < 125) {
            //asignamos el producto
            $this->descripcion = $producto;
        } else {
            throw new Exception('Falta agregar la descripción del producto o longitud excesiva (máx: 125 char)');
        }
    }

    /**
     * Asignamos el nombre del usuario que realiza la compra (Obligatorio)
     *
     * @param string $titular Nombre del usuario (por ejemplo Juan Perez)
     * @throws Exception
     */
    public function setTitular($titular = '')
    {
        if (strlen(trim($titular)) > 0 &&
                strlen(trim($titular)) < 60) {
            $this->titular = $titular;
        } else {
            throw new Exception('Falta agregar el titular que realiza la compra');
        }
    }

    /**
     * Firma que se envía a Sermepa (Obligatorio)
     *
     * @throws Exception
     */
    public function getFirma()
    {
        $mensaje = $this->importe . $this->pedido . $this->codigoFuc . $this->moneda . $this->transactionType . $this->urlNotificacion . $this->claveComercio;
        if (strlen(trim($mensaje)) > 0) {
            // Cálculo del SHA1                 
            return $this->firma = strtoupper(sha1($mensaje));
        } else {
            throw new Exception('Falta agregar la firma, Obligatorio');
        }
    }

    /**
     * Asignar el nombre del formulario
     *
     * @param string $nombre
     */
    public function set_nameform($nombre = 'servired_form')
    {
        $this->_setNameForm = $nombre;
    }

    /**
     * Comprueba si la operación ha resultado satisfactoria
     *
     * @param array $postData Datos _$POST recibidos del TPV (url de notificación)
     * @return bool
     * @throws Exception
     */
    public function comprobar($postData = '')
    {

        if ($this->claveComercio === null) {
            throw new Exception('Falta agregar la clave proporcionada por sermepa');
        }

        try {
            if (isset($postData)) {
                // creamos las variables para usar
                $Ds_Response = $postData['Ds_Response']; //codigo de respuesta
                $Ds_Amount = $postData['Ds_Amount']; //monto de la orden
                $Ds_Order = $postData['Ds_Order']; //numero de orden
                $Ds_MerchantCode = $postData['Ds_MerchantCode']; //codigo de comercio
                $Ds_Currency = $postData['Ds_Currency']; //moneda
                $firmaBanco = $postData['Ds_Signature']; //firma hecha por el banco
                $Ds_Date = $postData['Ds_Date']; //fecha
                // creamos la firma para comparar
                $firma = strtoupper(sha1($Ds_Amount . $Ds_Order . $Ds_MerchantCode . $Ds_Currency . $Ds_Response . $this->claveComercio));

                $Ds_Response = (int) $Ds_Response; //convertimos la respuesta en un numero concreto.
                //Comprueba la firma y respuesta
                //Nota: solo en el caso de las preautenticaciones (preautorizaciones separadas), se devuelve un 0 si está autorizada y el titular se autentica y, un 1 si está autorizada y el titular no se autentica.
                if ($firma == $firmaBanco) {
                    if ($Ds_Response < 100) {
                        return true;
                    } else {
                        throw new Exception("Error en la transacción, código " . $Ds_Response);
                    }
                } else {
                    throw new Exception("Las firmas no coinciden");
                }
            } else {
                throw new Exception("Debes pasar la variable POST devuelta por el banco");
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    /**
     * Generamos el form a incluir en el HTML
     *
     * @return string
     */
    public function create_form()
    {
        $formulario = '
        <form action="' . $this->entorno . '" method="post" id="' . $this->_setNameForm . '" name="' . $this->_setNameForm . '" >
            <input type="hidden" name="Ds_Merchant_Amount" value="' . $this->importe . '" />
            <input type="hidden" name="Ds_Merchant_Currency" value="' . $this->moneda . '" />
            <input type="hidden" name="Ds_Merchant_Order" value="' . $this->pedido . '" />
            <input type="hidden" name="Ds_Merchant_MerchantData" value="' . $this->merchantData . '" />
            <input type="hidden" name="Ds_Merchant_MerchantCode" value="' . $this->codigoFuc . '" />
            <input type="hidden" name="Ds_Merchant_Terminal" value="' . $this->terminal . '" />
            <input type="hidden" name="Ds_Merchant_TransactionType" value="' . $this->transactionType . '" />
            <input type="hidden" name="Ds_Merchant_Titular" value="' . $this->titular . '" />
            <input type="hidden" name="Ds_Merchant_MerchantName" value="' . $this->nombreComercio . '" />
            <input type="hidden" name="Ds_Merchant_MerchantURL" value="' . $this->urlNotificacion . '" />
            <input type="hidden" name="Ds_Merchant_ProductDescription" value="' . $this->descripcion . '" />
            <input type="hidden" name="Ds_Merchant_ConsumerLanguage " value="' . $this->idioma . '" />
            <input type="hidden" name="Ds_Merchant_UrlOK" value="' . $this->urlOK . '" />
            <input type="hidden" name="Ds_Merchant_UrlKO" value="' . $this->urlKO . '" />
            <input type="hidden" name="Ds_Merchant_MerchantSignature" value="' . $this->firma . '" />       
        ';
        $formulario.=$this->_setSubmit;
        $formulario.='
        </form>        
        ';
        return $formulario;
    }

    /**
     * Parseo a Float
     *
     * @param $ptString
     * @return bool|float
     */
    private function parseFloat($ptString)
    {
        if (strlen($ptString) == 0) {
            return false;
        }
        $pString = str_replace(" ", "", $ptString);
        if (substr_count($pString, ",") > 1)
            $pString = str_replace(",", "", $pString);
        if (substr_count($pString, ".") > 1)
            $pString = str_replace(".", "", $pString);
        $pregResult = array();
        $commaset = strpos($pString, ',');
        if ($commaset === false) {
            $commaset = -1;
        }
        $pointset = strpos($pString, '.');
        if ($pointset === false) {
            $pointset = -1;
        }
        $pregResultA = array();
        $pregResultB = array();
        if ($pointset < $commaset) {
            preg_match('#(([-]?[0-9]+(\.[0-9])?)+(,[0-9]+)?)#', $pString, $pregResultA);
        }
        preg_match('#(([-]?[0-9]+(,[0-9])?)+(\.[0-9]+)?)#', $pString, $pregResultB);
        if ((isset($pregResultA[0]) && (!isset($pregResultB[0]) || strstr($pregResultA[0], $pregResultB[0]) == 0 || !$pointset))) {
            $numberString = $pregResultA[0];
            $numberString = str_replace('.', '', $numberString);
            $numberString = str_replace(',', '.', $numberString);
        } elseif (isset($pregResultB[0]) && (!isset($pregResultA[0]) || strstr($pregResultB[0], $pregResultA[0]) == 0 || !$commaset)) {
            $numberString = $pregResultB[0];
            $numberString = str_replace(',', '', $numberString);
        } else {
            return false;
        }
        $result = (float) $numberString;
        return $result;
    }

    public function generateUrls($hash_order)
    {
        $this->urlOK = $this->router->generate($this->urlOK, array('firma' => $hash_order), true);
        $this->urlKO = $this->router->generate($this->urlKO, array('firma' => $hash_order), true);
        $this->urlNotificacion = $this->router->generate($this->urlNotificacion, array('firma' => $hash_order), true);
    }

}

?>