if (typeof ROOMASTIC === 'undefined') {
    ROOMASTIC = {};
}

if (typeof ROOMASTIC.googleID !== 'undefined') {
    function googleReady() {
        ROOMASTIC.google_scopes = '';
        //ROOMASTIC.google_scopes = 'https://www.googleapis.com/auth/userinfo.profile';
        //ROOMASTIC.google_scopes += ' https://www.googleapis.com/auth/userinfo.email ';
        //ROOMASTIC.google_scopes += 'https://www.googleapis.com/auth/plus.me ';
        //ROOMASTIC.google_scopes += ' https://www.googleapis.com/auth/plus.login ';
        //ROOMASTIC.google_scopes += ' https://www.googleapis.com/auth/plus.profile.emails.read';
        ROOMASTIC.google_scopes += ' https://www.googleapis.com/auth/userinfo.email ';

        gapi.client.setApiKey(ROOMASTIC.googleapikey);
        //gapi.client.load('plus', 'v1', function() {});
    }
    function checkPermisos(scopes) {
        if (scopes.indexOf("email") === -1 || scopes.indexOf("contact_email") === -1) {
            return false;
        } else {
            return true;
        }
    }
    ROOMASTIC.response = [];
    ROOMASTIC.try_login_google_error = false;
    function try_login_google(response) {
        var data = {};
        //Compruebo que no haya errores en la respuesta
        if (typeof response.error === 'undefined') {
            if (response.code) {
                data.code = response.code;
                data.state = ROOMASTIC.state;
            }
            $.ajax({
                url: Routing.generate('TooltypAuthGoogleBundle_login'),
                type: "POST",
                data: JSON.stringify(data),
                success: function (response) {
                    ROOMASTIC.try_login_google_error = false;
                    ROOMASTIC.user_data = response.user_data;
                    if (ROOMASTIC.user_data['canOffer'] === 'si') {
                        hacerOferta();
                    } else if (ROOMASTIC.user_data['canOffer'] === 'faltan_datos') {
                        for (var propertyName in ROOMASTIC.user_data) {
                            $('#formregisterextra .' + propertyName).val(ROOMASTIC.user_data[propertyName]);
                            if (propertyName === 'email' && ROOMASTIC.user_data[propertyName].length > 0) {
                                $('#formregisterextra .' + propertyName).prop('readonly', true);
                            }
                        }
                        showRegisterExtra();
                    } else if (ROOMASTIC.user_data['canOffer'] === 'oferta_realizada_pendiente') {
                        swal('Tu usuario tiene una oferta pendiente');
                    }
                },
                error: function (xhrResponse) {
                    if (false === ROOMASTIC.try_login_google_error) {
                        if (xhrResponse.status === 400) {
                            swal(xhrResponse.responseJSON.message);
                        } else {
                            swal('Vaya, ha habido un error. Inténtalo de nuevo tras recargar la página');
                        }
                        //swal('Vaya, ha habido un error. Inténtalo de nuevo tras recargar la página');
                        ROOMASTIC.try_login_google_error = true;
                    }
                }
            });
        } else {
            if (false === ROOMASTIC.try_login_google_error) {
                swal('Vaya, parece que ha habido un error al autorizarnos :(. \n Intentalo de nuevo tras recargar la página');
                ROOMASTIC.try_login_google_error = true;
            }
        }
    }
    $(document).ready(function () {

        $('.login-rrss.google').on('click', function (evt) {
            var parametros = {
                'clientid': ROOMASTIC.googleID, //You need to set client id
                'cookiepolicy': 'single_host_origin',
                'callback': 'try_login_google', //callback function
                'approvalprompt': 'force',
                'scope': ROOMASTIC.google_scopes //'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read https://www.googleapis.com/auth/userinfo.email'
            };
            gapi.auth.signIn(parametros);

        });
        if (window.gapi && window.gapi.client) {
            googleReady();
            //$('.login-rrss.google').click();
        }
    });
}