<?php

namespace Hoteles\FrontendBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class RegisterType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('email')
                ->add('password', 'repeated', array(
                    'type' => 'password',
                    'invalid_message' => 'Las dos contraseñas deben coincidir',
                    'options' => array('label' => 'Contraseña'),
                    'required' => false))
                ->add('nombre')
                ->add('apellidos')
                ->add('dni')
                ->add('direccioncompleta','text', array(
                    'required' => false                    
                ))
                ->add('telefono')
        ;
    }

    public function getName()
    {
        return 'hoteles_backendbundle_registertype';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'validation_groups' => array('registration'),
        );
    }

}
