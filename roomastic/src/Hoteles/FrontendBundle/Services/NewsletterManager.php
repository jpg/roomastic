<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\FrontendBundle\Services;

use Doctrine\ORM\EntityManager;
use Hoteles\BackendBundle\Entity\User;
use Hoteles\BackendBundle\Entity\Newsletter;

/**
 * Description of LoadConfiguracionService
 *
 * @author joaquin
 */
class NewsletterManager
{

    //put your code here
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addUser(User $user)
    {

        $newsletter = $this->em->getRepository('HotelesBackendBundle:Newsletter')->findOneBy(
                array('usuario' => $user->getId())
        );
        if (is_null($newsletter)) {
            $new_newsletter = new Newsletter();
            $new_newsletter->setUsuario($user);
            $this->em->persist($new_newsletter);
            $this->em->flush();
        } else {
            if ($newsletter->isActivo() === false) {
                $newsletter->makeActivo();
                $this->em->persist($newsletter);
                $this->em->flush();
            }
        }
        return;
    }

    public function removeUser(User $user)
    {
        $newsletter = $this->em->getRepository('HotelesBackendBundle:Newsletter')->findBy(
                array('usuario' => $user->getId())
        );
        if (is_null($newsletter)) {
            return false;
        } elseif ($newsletter->isActivo() === false) {
            $newsletter->makeNotActivo();
            $this->em->persist($newsletter);
            $this->em->flush();
        }
        return true;
    }

}
