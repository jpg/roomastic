<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\FrontendBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContextInterface;

class ForceLogin
{

    protected $securityContext;

    public function __construct(SecurityContextInterface $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function forceUserLogin(Request $request, $user)
    {
        $token = new UsernamePasswordToken($user, null, "main", $user->getRoles());
        $this->securityContext->setToken($token); //now the user is logged in
        $session = $request->getSession();
        $session->set('_security_main', serialize($token));
    }

}
