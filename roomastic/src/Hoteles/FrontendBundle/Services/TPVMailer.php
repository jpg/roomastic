<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\FrontendBundle\Services;

use Symfony\Component\Templating\EngineInterface;
use Hoteles\BackendBundle\Entity\Oferta;

class TPVMailer
{

    /**
     * @var MailerInterface
     */
    protected $mailer;

    /**
     * @var TemplatingInterface
     */
    protected $templating;

    /**
     * Constructor
     * 
     * @param ContainerInterface $mailer
     */
    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    public function sendUserOfferDetails(Oferta $oferta)
    {
        $message = \Swift_Message::newInstance()
                ->setSubject('Oferta pagada')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo($oferta->getUsuario()->getEmail())
                ->setBody(
                $this->templating->render(
                        'HotelesBackendBundle:Mail:avisoUsuarioOfertaPagada.html.twig', array(
                    'oferta' => $oferta,
                    'hotel' => $oferta->getHotel(),
                    'sale' => $oferta->getSale())), 'text/html');
        $this->mailer->send($message);
    }

    public function sendHotelOfferDetails(Oferta $oferta)
    {
        $usuarios_hotel_email = array_map(function($usuario) {
            return $usuario->getEmail();
        }, $oferta->getHotel()->getUserHotel()->toArray());
        $message = \Swift_Message::newInstance()
                ->setSubject('Oferta pagada')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setCC($usuarios_hotel_email)
                ->setBody(
                $this->templating->render(
                        'HotelesBackendBundle:Mail:avisoHotelOfertaPagada.html.twig', array(
                    'oferta' => $oferta,
                    'hotel' => $oferta->getHotel(),
                    'sale' => $oferta->getSale())), 'text/html');
        $this->mailer->send($message);
    }

    public function reportTPVFailure(Oferta $oferta)
    {
        $message = \Swift_Message::newInstance()
                ->setSubject('Problema con oferta')
                ->setFrom(array('mailer@roomastic.com' => 'Roomastic'))
                ->setTo('admin@roomastic.com')
                ->setBody('Problema con la oferta' . $oferta->getId() . '.Venta: ' . $oferta->getSale()->getId(), 'text/html');
        $this->mailer->send($message);
    }

}
