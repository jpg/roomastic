<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Hoteles\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Hoteles\BackendBundle\Entity\Sale;
use Hoteles\BackendBundle\Factories\SaleFactory;
use Hoteles\BackendBundle\Entity\Transaction;
use Hoteles\BackendBundle\Transaction\TransactionResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/**
 * @Route("/tpv")
 * */
class TPVController extends Controller
{

    /**
     * Ruta de notificacion online
     * @Route("/done/{firma}", options={"expose"=true})
     * */
    public function doneAction(Request $request, $firma)
    {
        if ($request->getMethod() != 'POST') {
            throw $this->createNotFoundException('Page not found');
        }
        $em = $this->getDoctrine()->getEntityManager();
        $postData = $request->request->all();
        $sale = $em->getRepository('Hoteles\BackendBundle\Entity\Sale')->findOneBy(array(
            'firma' => $firma));
        $logger = $this->container->get('tpv.logger');
        $logger->info(sprintf('Recibida notificacion asincrona de SERMEPA para Firma:' , $firma));
        $logger->debug(var_export($postData, true));
        if ($sale && $sale->canBePayed()) {
            $transaction = new Transaction();
            $transaction->bind($postData);
            $sale->addTransaction($transaction);
            $transaction->setSales($sale);
            $payment = $this->get('tpv.sermapa');
            $clave_comercio = $payment->getClaveComercio();
            $firma_comprobada = $transaction->checkSignature($clave_comercio);
            $validResponse = TransactionResponse::isValid($transaction->getDsResponse());
            if ($firma_comprobada && $validResponse) {
                //Si la firma generada y la firma que nos da el banco son la misma y el código de la transacción es válido, validamos la transacción
                $ofertaManager = $this->get('hoteles_backend.oferta_manager');
                //Busco la oferta
                $oferta = $sale->getOferta();
                if ($oferta->isContraOferta()) {
                    $resultado = $ofertaManager->updateOfferState('paga_contraoferta', $oferta);
                    $logger->debug(sprintf('Estado contraoferta', $resultado));
                    $logger->debug(sprintf('Pagando contraoferta: ', ($resultado ? 'Si' : 'No')));
                } else {
                    $resultado = $ofertaManager->updateOfferState('paga_oferta', $oferta);
                    $logger->debug(sprintf('Pagando oferta: ', ($resultado ? 'Si' : 'No')));
                }
                if (true === $resultado) {
                    $logger->info(sprintf('Transaccion valida. FirmaVenta: %s', $firma));
                    $transaction->markAsValid();
                    $sale->markAsPayed();
                } else {
                    //El pago se ha procesado pero la oferta tiene algún error
                    $logger->err(sprintf('Error. La oferta(%s) no esta en el estado correcto. Estado: %s', $sale->getOferta()->getId() ,  $sale->getOferta()->getStatus()));
                    $transaction->markAsError();
                    $sale->markAsError();
                }
                $em->persist($transaction);
                $em->persist($sale);
                $em->persist($oferta);
                $tpv_mailer = $this->get('tpv.mailer');
                if ($resultado) {
                    //Envio e-mail al usuario con los detalles
                    $tpv_mailer->sendUserOfferDetails($oferta);
                    //Envío e-mail a los usuarios hotel de ese hotel
                    $tpv_mailer->sendHotelOfferDetails($oferta);
                } else {
                    $logger->err('Notificando al admin del error');
                    //Envio e-mail/incidencia a la administración informando de los problemas
                    $tpv_mailer->reportTPVFailure($oferta);
                }
                $em->flush();
                $logger->debug('Devolviendo respuesta OK');
                return new Response('OK');
            } else {
                //Transaccion no valida
                $em->persist($transaction);
                $em->persist($sale);
                $logger->err(sprintf('Error. Transacción no valida. Id: %s', $transaction->getId()));
                $em->flush();
                return new Response('KO');
            }
        } else {
            if (is_null($sale)) {
                $logger->err(sprintf('Error. Venta no encontrada con hash: %s', $firma));
            } else {
                if ($sale->isPayed()) {
                    $logger->info(sprintf('Venta (%s) ya pagada', $sale->getId()));
                } else if ($sale->isExpired()) {
                    $logger->info(sprintf('Venta (%s) caducada', $sale->getId()));
                } else {
                    $tpv_mailer->reportTPVFailure($sale->getOferta());
                    $logger->err(sprintf(
                            'Venta con estado inconsistente. Id:%d|Status:%s',$sale->getId(),$sale->getStatus()));
                }
            }
            $logger->debug('Devolviendo respuesta KO');
            return new Response('KO');
        }
    }

    /**
     * Ruta en la que se genera el formulario de pago. Datos de tarjeta de prueba:
     * Número de tarjeta: 4548 8120 4940 0004
     * Caducidad: 12/20
     * Código CVV2: 123
     * Código CIP: 123456
     * @Route("/processpayment/{firma}")
     * */
    public function processpaymentAction($firma)
    {
        //Uso twig para crear un formulario
        //Redirijo al mismo
        $em = $this->getDoctrine()->getEntityManager();
        $sale = $em->getRepository('Hoteles\BackendBundle\Entity\Sale')->findOneBy(array(
            'firma' => $firma));
        $logger = $this->container->get('tpv.logger');
        if ($sale && $sale->canBePayed()) {
            $logger->debug(sprintf('Procesando pago con firma: %s',$firma));
            $logger->debug('Generando identificador único');
            //Genero un identificador unico para la venta
            $sale->setOrderNumber(SaleFactory::generateOrderNum());
            $em->persist($sale);
            $em->flush();
            $payment = $this->get('tpv.sermapa.factory')->createPayment(
                    $sale->getAmount(), $sale->getOrderNumber(), $firma, 'Compra en roomastic.com', $sale->getOferta()->getUsuario()->getNombreCompleto());
            return $this->render('HotelesFrontendBundle:TPV:pago-inicial.html.twig', array(
                        'sale' => $sale,
                        'hotel' => $sale->getOferta()->getHotel(),
                        'payment' => $payment,
                        'oferta' => $sale->getOferta()));
        } else {
            if (is_null($sale)) {
                $logger->err(sprintf('Error. Venta no encontrada con firma: %s', $firma));
                throw $this->createNotFoundException('Vaya, no existe ninguna venta con ese identificador');
            } else if ($sale->isPayed()) {
                $logger->debug('Venta ya procesada. Redirijo a la vista de todo ok');
                $url = $this->generateUrl('hoteles_frontend_tpv_success', array('firma' => $firma), true);
                return $this->redirect($url);
            }
        }
    }

    /**
     * Ruta de notificacion de OK. Aqui es donde redirijo al usuario si todo va bien
     * @Route("/success/{firma}", options={"expose"=true})
     * */
    public function successAction(Request $request, $firma)
    {

        $em = $this->getDoctrine()->getEntityManager();
        $logger = $this->get('tpv.logger');
        $sale = $em->getRepository('Hoteles\BackendBundle\Entity\Sale')->findOneBy(
                array('firma' => $firma));
        if ($sale) {
            return $this->render('HotelesFrontendBundle:TPV:pago-realizado.html.twig', array(
                        'oferta' => $sale->getOferta(),
                        'sale' => $sale,
                        'hotel' => $sale->getOferta()->getHotel(),
            ));
        } else {
            $logger->err(sprintf('Error con la transacción. Firma de la venta: [%s]', $firma));
            throw $this->createNotFoundException('Vaya, no existe ninguna venta con ese identificador');
        }
    }

    /**
     * Ruta de notificacion KO. . Aqui es donde redirijo al usuario si algo falla
     * @Route("/fail/{firma}", options={"expose"=true})
     * */
    public function failAction($firma)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $logger = $this->get('tpv.logger');
        $sale = $em->getRepository('Hoteles\BackendBundle\Entity\Sale')->findOneBy(
                array('firma' => $firma));
        if ($sale) {
            return $this->render('HotelesFrontendBundle:TPV:pago-no-realizado.html.twig', array(
                        'oferta' => $sale->getOferta(),
                        'sale' => $sale,
                        'hotel' => $sale->getOferta()->getHotel(),
            ));
        } else {
            $logger->err(sprintf('Error con la transacción. Firma de la venta: [%s]', $firma));
            throw $this->createNotFoundException('Vaya, no existe ninguna venta con ese identificador');
        }
    }

    /**
     * Ruta inicial en la que muestro los datos del pago y cargo el iframe
     * @Route("/pay/{firma}", options={"expose"=true})
     * */
    public function payAction($firma)
    {
        $logger = $this->get('tpv.logger');
        $logger->info('Cargando venta ' . $firma);
        $em = $this->getDoctrine()->getEntityManager();
        $sale = $em->getRepository('Hoteles\BackendBundle\Entity\Sale')->findOneBy(
                array('firma' => $firma));
        if (!is_null($sale) && $sale->canBePayed()) {
            $logger->debug(sprintf('Estado de la venta: %s', $sale->getStatus()));
            $logger->debug(sprintf('Estado de la Oferta: %s', $sale->getOferta()->getStatus()));
            $logger->debug(sprintf('Estado de grupo de la Oferta: [%s]', $sale->getOferta()->getGroupStatus()));
            return $this->render('HotelesFrontendBundle:TPV:pago-datos.html.twig', array(
                        'firma' => $firma
            ));
        } else {
            $logger->info(sprintf('Venta con firma: %s no disponible para el pago' , $firma ));
            throw $this->createNotFoundException();
        }
    }

}
