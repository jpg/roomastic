<?php

namespace Hoteles\FrontendBundle\Controller;

use Hoteles\BackendBundle\Entity\Hotel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ListadoController extends Controller
{

    public function listadoprovinciaAction($provincia, $fechain, $fechaout, Request $request)
    {
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';

        $lugar = array(
            'provincia' => $provincia,
        );
        $result = $this->logicalistado($lugar, $fechain, $fechaout);
        return $this->render('HotelesFrontendBundle:Frontend:listado.' . $format . '.twig', $result);
    }

    public function listadoprovinciamunicipioAction($provincia, $municipio, $fechain, $fechaout, Request $request)
    {
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';
        $lugar = array(
            'provincia' => $provincia,
            'municipio' => $municipio,
        );
        $result = $this->logicalistado($lugar, $fechain, $fechaout);
        return $this->render('HotelesFrontendBundle:Frontend:listado.' . $format . '.twig', $result);
    }

    public function listadoprovinciamunicipiolugarAction($provincia, $municipio, $zona, $fechain, $fechaout, Request $request)
    {
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';
        $lugar = array(
            'provincia' => $provincia,
            'municipio' => $municipio,
            'lugar' => $zona,
        );
        $result = $this->logicalistado($lugar, $fechain, $fechaout);
        return $this->render('HotelesFrontendBundle:Frontend:listado.' . $format . '.twig', $result);
    }

    public function logicalistado($lugar, $fechain, $fechaout)
    {

        $arrayHotelesconImagenes = $this->consultaHoteles($lugar, $fechain, $fechaout, 'standar');

        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Hotel');
        $query = $repository->createQueryBuilder('h');
        $query->where('h.status = :status')->setParameter('status', Hotel::HOTEL_STATUS_VISIBLE);
        $hoteles = $query->getQuery()->getResult();

        $arraydelugares = array();
        foreach ($hoteles as $hotel) {
            if ($hotel->getProvincia() != '') {
                $arraydelugares[] = $hotel->getProvincia()->getNombre();
            }
            if ($hotel->getMunicipio() != '') {
                $arraydelugares[] = $hotel->getProvincia()->getNombre() . '/' . $hotel->getMunicipio()->getNombre();
            }
            if ($hotel->getLugar() != '') {
                $arraydelugares[] = $hotel->getProvincia()->getNombre() . '/' . $hotel->getMunicipio()->getNombre() . '/' . $hotel->getLugar()->getZona();
            }
        }

        return array(
            'lugar' => $lugar,
            'fechain' => str_replace('/', '-', $fechain),
            'fechaout' => str_replace('/', '-', $fechaout),
            'fecha' => $fechain . ' - ' . $fechaout,
            'hoteles' => $arrayHotelesconImagenes,
            'listadohoteleslugares' => $hoteles,
            'arraydelugares' => array_unique($arraydelugares),
        );
    }

    public function listadoprovinciaajaxAction($provincia, $fechain, $fechaout, Request $request)
    {
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';
        $lugar = array(
            'provincia' => $provincia,
        );
        $post_data = $request->request->all();
        $num_pagina = $post_data['numero_pagina'];
        $result = $this->logicalistadoajax($lugar, $fechain, $fechaout, $num_pagina);
        $html_respuesta = $this->renderView('HotelesFrontendBundle:Frontend:listadoajax.' . $format . '.twig', $result);
        $html_lateral = $this->renderView('HotelesFrontendBundle:Frontend:listadoAJAXlateral.' . $format . '.twig', $result);
        $respuesta_json = array();
        $respuesta_json['html'] = $html_respuesta;
        $respuesta_json['html_lateral'] = $html_lateral;
        $respuesta_json['ultima_pagina'] = $result['ultima_pagina'];
        return $this->get("backend_utilities.json_response")->JSONResponse($respuesta_json);
    }

    public function listadoprovinciamunicipioajaxAction($provincia, $municipio, $fechain, $fechaout, Request $request)
    {
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';
        $lugar = array(
            'provincia' => $provincia,
            'municipio' => $municipio,
        );
        $post_data = $request->request->all();
        $num_pagina = $post_data['numero_pagina'];
        $result = $this->logicalistadoajax($lugar, $fechain, $fechaout, $num_pagina);
        $html_respuesta = $this->renderView('HotelesFrontendBundle:Frontend:listadoajax.' . $format . '.twig', $result);
        $html_lateral = $this->renderView('HotelesFrontendBundle:Frontend:listadoAJAXlateral.' . $format . '.twig', $result);
        $respuesta_json = array();
        $respuesta_json['html'] = $html_respuesta;
        $respuesta_json['html_lateral'] = $html_lateral;
        $respuesta_json['ultima_pagina'] = $result['ultima_pagina'];
        return $this->get("backend_utilities.json_response")->JSONResponse($respuesta_json);
    }

    public function listadoprovinciamunicipiolugarajaxAction($provincia, $municipio, $zona, $fechain, $fechaout, Request $request)
    {
        $format = $request->headers->get('ROOMASTIC-MV', false) ? 'mv' : 'html';
        $lugar = array(
            'provincia' => $provincia,
            'municipio' => $municipio,
            'lugar' => $zona,
        );
        $post_data = $request->request->all();
        $num_pagina = $post_data['numero_pagina'];
        $result = $this->logicalistadoajax($lugar, $fechain, $fechaout, $num_pagina);
        $html_respuesta = $this->renderView('HotelesFrontendBundle:Frontend:listadoajax.' . $format . '.twig', $result);
        $html_lateral = $this->renderView('HotelesFrontendBundle:Frontend:listadoAJAXlateral.' . $format . '.twig', $result);
        $respuesta_json = array();
        $respuesta_json['html'] = $html_respuesta;
        $respuesta_json['html_lateral'] = $html_lateral;
        $respuesta_json['ultima_pagina'] = $result['ultima_pagina'];
        return $this->get("backend_utilities.json_response")->JSONResponse($respuesta_json);
    }

    public function logicalistadoajax($lugar, $fechain, $fechaout, $num_pagina = 1)
    {

        $arrayHotelesconImagenes = $this->consultaHoteles($lugar, $fechain, $fechaout, 'ajax', $num_pagina, $ultima_pagina);

        return array(
            'lugar' => $lugar,
            'fecha' => $fechain . ' - ' . $fechaout,
            'hoteles' => $arrayHotelesconImagenes,
            'ultima_pagina' => $ultima_pagina,
        );
    }

    public function consultaHoteles($lugar, $fechain, $fechaout, $tipo, $num_pagina = 1, &$ultima_pagina = null)
    {

        $fechain = str_replace('-', '/', $fechain);
        $fechaout = str_replace('-', '/', $fechaout);
        $repository = $this->getDoctrine()->getRepository('HotelesBackendBundle:Hotel');

        $query = $repository->createQueryBuilder('h');

        //Status = 1 significa que esta activo?
        $query->where('h.status = :status')->setParameter('status', Hotel::HOTEL_STATUS_VISIBLE);
        $extra = $this->get('hoteles_backend.funcionalidades');
        $provincia = null;
        if (isset($lugar['provincia'])) {
            $provincia = $extra->sacaProvinciaPorNombre($lugar['provincia']);
            if ($provincia) {
                $query->andWhere('h.provincia = :provincia')->setParameter('provincia', $provincia);
            } else {
                throw $this->createNotFoundException('Provincia not found!');
            }
        }
        if (isset($lugar['municipio'])) {
            $municipio = $extra->sacaMunicipioPorNombreYProvincia($lugar['municipio'], $provincia->getId());
            if ($municipio) {
                $query->andWhere('h.municipio = :municipio')->setParameter('municipio', $municipio);
            } else {
                throw $this->createNotFoundException('Municipio not found!');
            }
        }
        if (isset($lugar['lugar'])) {
            $lugar = $extra->sacaZonaPorNombreProvinciaMunicipio($value, $provincia->getId(), $municipio->getId());
            if ($lugar) {
                $query->andWhere('h.lugar = :lugar')->setParameter('lugar', $lugar);
            } else {
                throw $this->createNotFoundException('Lugar not found');
            }
        }
        //Parametro definido en services.yml del FrontendBundle
        $num_resultados_pagina = $this->container->getParameter('num_resultados_pagina');
        //TODO: Revisar
        $prim_resultado = ($num_pagina - 1) * $num_resultados_pagina;
        $hoteles = $query->getQuery()->
                setFirstResult($prim_resultado)->
                setMaxResults($num_resultados_pagina)->
                getResult();

        $arrayHotelesconImagenes = array();
        foreach ($hoteles as $key => $value) {
            $arrayHotelesconImagenes[$key]['hotel'] = $value;
            $arrayHotelesconImagenes[$key]['imagenes'] = $this->imagenesHotel($value->getId());
        }
        $ultima_pagina = count($hoteles) < $num_resultados_pagina;

        return $arrayHotelesconImagenes;
    }

    public function imagenesHotel($idHotel)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $imagenes = $em->getRepository('HotelesBackendBundle:Imagenhotel')->findBy(array('hotel' => $idHotel), array('orden' => 'ASC'));
        return $imagenes;
    }

}
