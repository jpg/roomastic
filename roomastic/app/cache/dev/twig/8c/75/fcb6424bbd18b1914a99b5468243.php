<?php

/* HotelesFrontendBundle:Frontend:index.html.twig */
class __TwigTemplate_8c75fcb6424bbd18b1914a99b5468243 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'seotitle' => array($this, 'block_seotitle'),
            'seodescripcion' => array($this, 'block_seodescripcion'),
            'seokeywords' => array($this, 'block_seokeywords'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->
    <head>

        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>Portada - Roomastic</title>

        <meta name=\"title\" content=\"";
        // line 12
        $this->displayBlock('seotitle', $context, $blocks);
        echo "\">
        <meta name=\"description\" content=\"";
        // line 13
        $this->displayBlock('seodescripcion', $context, $blocks);
        echo "\">
        <meta name=\"keywords\" content=\"";
        // line 14
        $this->displayBlock('seokeywords', $context, $blocks);
        echo "\">
        <!--<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">-->

        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

        <!--<link href=\"http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css\" rel=\"stylesheet\">-->
        <link href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\">

        ";
        // line 22
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "fe29308_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_fe29308_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/fe29308_selectize.default_1.css");
            // line 23
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" />
        ";
        } else {
            // asset "fe29308"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_fe29308") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/fe29308.css");
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" />
        ";
        }
        unset($context["asset_url"]);
        // line 25
        echo "
        ";
        // line 26
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bd754b1_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_bd754b1_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/bd754b1_daterangepicker-bs3_1.css");
            // line 30
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" />
        ";
            // asset "bd754b1_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_bd754b1_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/bd754b1_jquery.fancybox_2.css");
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" />
        ";
            // asset "bd754b1_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_bd754b1_2") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/bd754b1_sweet-alert_3.css");
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" />
        ";
        } else {
            // asset "bd754b1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_bd754b1") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/bd754b1.css");
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" />
        ";
        }
        unset($context["asset_url"]);
        // line 32
        echo "        ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "46e6cf9_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_46e6cf9_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/46e6cf9_roomastic_1.css");
            // line 33
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" />
        ";
        } else {
            // asset "46e6cf9"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_46e6cf9") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/46e6cf9.css");
            echo "        <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" />
        ";
        }
        unset($context["asset_url"]);
        // line 35
        echo "        ";
        // line 36
        echo "        ";
        // line 37
        echo "        <script src=\"/bundles/hotelesfrontend/js/vendor/modernizr/modernizr.js\"></script>

        <link rel=\"icon\" type=\"image/x-icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\" />
        <script src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>

        ";
        // line 46
        echo "
        <!--[if lt IE 9]>
            <script src=\"js/vendor/html5shiv/dist/html5shiv.min.js\"></script>
        <![endif]-->

    </head> 
    ";
        // line 52
        $context["rrss"] = $this->env->getExtension('twig_extension')->getRRSS();
        // line 53
        echo "
    <body class=\"portada\">
        <!--[if lt IE 7]>
            <p class=\"browsehappy\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- aviso cookie -->
        <div id=\"cookie-bar\" class=\"fixed\"></div>

        <div class=\"wrapper\">

            <!--<div class=\"degradado_portada\"></div>-->

            <header id=\"header\">
                <div class=\"lineatop\"></div>

                <div class=\"container clearfix\">
                    <a href=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"\"><h1 class=\"logo\">Roomastic</h1></a>

                    <ul class=\"social\">
                        <li><a href=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "rrss"), "twitter"), "html", null, true);
        echo "\" target=\"_blank\" title=\"twitter\">twitter</a></li>
                        <li><a class=\"facebook\" href=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "rrss"), "facebook"), "html", null, true);
        echo "\" target=\"_blank\" title=\"facebook\">facebook</a></li>
                    </ul>
                    <ul class=\"menu\">                      
                        <li><a href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"HOME\">HOME</a></li>
                        <li><a href=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_faqs"), "html", null, true);
        echo "\" title=\"FAQS\">FAQS</a></li>
                        <li><a href=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" title=\"CONTACTO\">CONTACTO</a></li>
                        <li><a href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_quienesSomos"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos?"), "html", null, true);
        echo "</a></li>
                    </ul>
                </div><!-- container -->

            </header><!-- /header -->

            <div class=\"container\">

                <div class=\"cajabusqueda animated fadeInDown\">
                    <div class=\"cajalema\">
                        <h2 class=\"lema\">Haz tu oferta, ahorra<br /> y disfruta de tu estancia</h2>
                        <span>Cada semana más y más hoteles</span>
                    </div>

                    <form class=\"form_portada clearfix\" id=\"form-destino-fecha\">

                        <select id=\"select-beast\" class=\"selec_ciudad\" placeholder=\"\" name=\"ciudad\">
                            <option value=\"\">Destino</option>
                            ";
        // line 98
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["hotel"]) {
            // line 99
            echo "                                ";
            if (($this->getAttribute($this->getContext($context, "hotel"), "provincia") != "")) {
                // line 100
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "provincia"), "nombre"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "provincia"), "nombre"), "html", null, true);
                echo "</option>
                                ";
            }
            // line 102
            echo "                                ";
            if (($this->getAttribute($this->getContext($context, "hotel"), "municipio") != "")) {
                // line 103
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "provincia"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "municipio"), "nombre"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "provincia"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "municipio"), "nombre"), "html", null, true);
                echo "</option>
                                ";
            }
            // line 105
            echo "                                ";
            if (($this->getAttribute($this->getContext($context, "hotel"), "lugar") != "")) {
                // line 106
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "provincia"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "municipio"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "zona"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "provincia"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "municipio"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "zona"), "html", null, true);
                echo "</option>
                                ";
            }
            // line 107
            echo "                                                                
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 108
        echo "                            
                        </select>
                        <div class=\"fecha_picker\">
                            <input placeholder=\"Fechas\" class=\"select_fecha\" type=\"text\" name=\"fecha\" id=\"reservation\" />
                        </div>
                        <div class=\"btn_enviar\">
                            <input type=\"submit\" id=\"buscar\" name=\"buscar\" value=\"Buscar\">
                        </div>

                    </form>

                    <div class=\"comofunc\">
                        <a href=\"#videohome\" class=\"fancybox\" id=\"fancyhome\"> <strong>¿Quieres saber cómo funciona Roomastic?</strong> En este vídeo de 1 min. te lo explicamos </a>
                        <div id=\"videohome\" style=\"display:none;\">
                            <iframe width=\"660\" height=\"415\" src=\"https://www.youtube.com/embed/-03y5GizzKQ\" frameborder=\"0\" allowfullscreen></iframe>
                        </div>
                    </div>

                </div><!-- cajabusqueda -->
            </div><!-- container -->

        </div><!-- wrapper -->

        <footer id=\"footer\">

            <div class=\"footer1\">
                <div class=\"container\">
                    <ul class=\"menu_foot clearfix\">    
                        <li><a href=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_privacidad"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES PARTICULARES Y PRIVACIDAD"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_condicionesLegales"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES DEL SERVICIO"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_publicaTuHotel"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PUBLICA TU HOTEL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"v_movil\" href=\"";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "request"), "attributes"), "get", array(0 => "_route"), "method"), twig_array_merge($this->env->getExtension('twig_extension')->getCurrentRouteParams(), array("version" => "mobile"))), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("VERSIÓN MÓVIL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"a_hoteles\" href=\"";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_login"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ACCESO HOTELES"), "html", null, true);
        echo "</a></li>

                    </ul>
                    <div class=\"att_cliente\">
                        ATENCIÓN AL CLIENTE <br /> <span>911 610 156</span>
                    </div>
                </div><!-- container -->
            </div><!-- footer1 -->
            <div class=\"footer2\">
                <div class=\"container\">
                    <span>© Copyright. Todos los derechos reservados.</span>
                </div><!-- container -->
            </div><!-- footer2 -->

        <div id=\"boxes\">
            <div id=\"dialog\" class=\"window\">
                <iframe width=\"660\" height=\"415\" src=\"https://www.youtube.com/embed/-03y5GizzKQ\" frameborder=\"0\" allowfullscreen></iframe>
            <div id=\"mask\"></div>
        </div>


        </footer>

        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
        <script>window.jQuery || document.write('<script src=\"/bundles/hotelesfrontend/js/vendor/jquery/dist/jquery.min.js\"><\\/script>')</script>
        <script>
            \$(document).ready(function() {  

            var id = '#dialog';
                
            //Get the screen height and width
            var maskHeight = \$(document).height();
            var maskWidth = \$(window).width();
                
            //Set heigth and width to mask to fill up the whole screen
            \$('#mask').css({'width':maskWidth,'height':maskHeight});

            //transition effect
            \$('#mask').fadeIn(500); 
            \$('#mask').fadeTo(\"slow\",0.9);  
                
            //Get the window height and width
            var winH = \$(window).height();
            var winW = \$(window).width();
                          
            //Set the popup window to center
            \$(id).css('top',  winH/2-\$(id).height()/2);
            \$(id).css('left', winW/2-\$(id).width()/2);
                
            //transition effect
            \$(id).fadeIn(2000);     
                
            //if close button is clicked
            \$('.window #mask').click(function (e) {
                //Cancel the link behavior
                e.preventDefault();

                \$('#mask').hide();
                \$('.window').hide();
                \$('.window iframe').attr('src', 'http://www.youtube.com');
            });

            //if mask is clicked
            \$('#mask').click(function () {
                \$(this).hide();
                \$('.window').hide();
            });
                
            });
        </script>

        <script src=\"/bundles/hotelesfrontend/js/plugins.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.backstretch.min.js\"></script>
        <script src=\"https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/moment.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/daterangepicker.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/selectize.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/icheck.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/gmap3.min.js\"></script>

        <!-- sweet-alert -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/sweet-alert.js\"></script>
        <!-- fancybox -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.pack.js\"></script>
        <!-- cookies -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.cookiebar.js\"></script>

        <script type=\"text/javascript\">
            ";
        // line 229
        if (($this->getAttribute($this->getContext($context, "app"), "debug") == 1)) {
            // line 230
            echo "                var env = 'dev';
            ";
        } else {
            // line 232
            echo "                var env = 'prod';
            ";
        }
        // line 234
        echo "        </script>

        ";
        // line 236
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "4d7f7a7_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4d7f7a7_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4d7f7a7_roomastic_1.js");
            // line 238
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
        ";
            // asset "4d7f7a7_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4d7f7a7_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4d7f7a7_index_2.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
        ";
        } else {
            // asset "4d7f7a7"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4d7f7a7") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4d7f7a7.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
        ";
        }
        unset($context["asset_url"]);
        // line 240
        echo "

        <script>
            var imagenes = [";
        // line 243
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "imageneshome"));
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            echo "\"/uploads/home/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "imagen"), "html", null, true);
            echo "\",";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        echo " ]
            \$.backstretch(imagenes, {duration: 7000, fade: 750});
        </script>   


        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-53266623-1');
            ga('send', 'pageview');
        </script>
    </body>
</html>";
    }

    // line 12
    public function block_seotitle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["seo"]) ? $context["seo"] : null), "seotitulo", array(), "any", true, true)) ? ($this->getAttribute($this->getContext($context, "seo"), "seotitulo")) : ("")), "html", null, true);
    }

    // line 13
    public function block_seodescripcion($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["seo"]) ? $context["seo"] : null), "seodescripcion", array(), "any", true, true)) ? ($this->getAttribute($this->getContext($context, "seo"), "seodescripcion")) : ("")), "html", null, true);
    }

    // line 14
    public function block_seokeywords($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["seo"]) ? $context["seo"] : null), "seokeywords", array(), "any", true, true)) ? ($this->getAttribute($this->getContext($context, "seo"), "seokeywords")) : ("")), "html", null, true);
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  517 => 14,  511 => 13,  505 => 12,  469 => 243,  464 => 240,  444 => 238,  440 => 236,  436 => 234,  432 => 232,  428 => 230,  426 => 229,  332 => 140,  326 => 139,  320 => 138,  314 => 137,  308 => 136,  278 => 108,  271 => 107,  255 => 106,  252 => 105,  240 => 103,  237 => 102,  229 => 100,  226 => 99,  222 => 98,  197 => 80,  193 => 79,  189 => 78,  185 => 77,  179 => 74,  175 => 73,  169 => 70,  150 => 53,  148 => 52,  140 => 46,  135 => 41,  131 => 40,  126 => 37,  124 => 36,  122 => 35,  108 => 33,  103 => 32,  77 => 30,  73 => 26,  70 => 25,  56 => 23,  52 => 22,  41 => 14,  37 => 13,  33 => 12,  20 => 1,);
    }
}
