<?php

/* HotelesFrontendBundle:Frontend:listado.html.twig */
class __TwigTemplate_5a53a00ffd234364335c5c9515daabf0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->
    <head>

        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>Listado - Roomastic</title>

        <meta name=\"description\" content=\"\">
        <!--<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">-->

        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

        <!--<link href=\"http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css\" rel=\"stylesheet\">-->
        <link href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\">

        <!-- CSS plugins -->
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/daterangepicker-bs3.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/selectize.default.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/minimal.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/jquery.fancybox.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/sweet-alert.css\">



        <!-- CSS Roomastic -->
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/roomastic.css\">

        <link rel=\"icon\" type=\"image/x-icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\" />

        <script src=\"/bundles/hotelesfrontend/js/vendor/modernizr/modernizr.js\"></script>

        <script src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>

        ";
        // line 42
        echo "
        <!--[if lt IE 9]>
            <script src=\"js/vendor/html5shiv/dist/html5shiv.min.js\"></script>
        <![endif]-->

    </head> 
    ";
        // line 48
        $context["rrss"] = $this->env->getExtension('twig_extension')->getRRSS();
        // line 49
        echo "
    <body class=\"interna\">
        <!--[if lt IE 7]>
            <p class=\"browsehappy\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- aviso cookie -->
        <div id=\"cookie-bar\" class=\"fixed\"></div>

        <div class=\"wrapper\">
            <div class=\"lineatop\"></div>
            <div class=\"fnd_gris\"></div>
            <div class=\"container clearfix\">
                <div class=\"col_izq\">
                    <a href=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"\"><h1 class=\"logo\">Roomastic</h1></a>

                    <div class=\"tubusq\">TU BÚSQUEDA</div>
                    <span class=\"rosa\">Con tus criterios de búsqueda <br /> <strong>tenemos 2350 hoteles</strong></span>



                    <div class=\"formulario_int\">

                        ";
        // line 72
        $context["lugarconstruido"] = "";
        // line 73
        echo "                        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "lugar"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["dato"]) {
            // line 74
            echo "                            ";
            $context["lugarconstruido"] = ($this->getContext($context, "lugarconstruido") . $this->getContext($context, "dato"));
            // line 75
            echo "                            ";
            if (($this->getAttribute($this->getContext($context, "loop"), "index") < twig_length_filter($this->env, $this->getContext($context, "lugar")))) {
                // line 76
                echo "                                ";
                $context["lugarconstruido"] = ($this->getContext($context, "lugarconstruido") . "/");
                // line 77
                echo "                            ";
            }
            // line 78
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dato'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 79
        echo "                        ";
        $context["basic_url"] = ((($this->getAttribute($this->getContext($context, "app"), "environment") == "dev")) ? ("/app_dev.php") : (""));
        // line 80
        echo "                        <form action=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "basic_url"), "html", null, true);
        echo "/listadoajax/";
        echo twig_escape_filter($this->env, (((($this->getContext($context, "lugarconstruido") . "/") . $this->getContext($context, "fechain")) . "/") . $this->getContext($context, "fechaout")), "html", null, true);
        echo "\" method=\"post\" id=\"formajax\">
                            <div class=\"blq clearfix\">   
                                <h4 class=\"tit_form\">Destino</h4>

                                <select id=\"select-beast\" class=\"selec_ciudad ajaxupdate\" placeholder=\"\" name=\"ciudad\">
                                    ";
        // line 85
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "arraydelugares"));
        foreach ($context['_seq'] as $context["_key"] => $context["dato"]) {
            // line 86
            echo "                                        ";
            $context["seleccionado"] = $this->env->getExtension('twig_extension')->select($this->getContext($context, "lugarconstruido"), $this->getContext($context, "dato"));
            // line 87
            echo "                                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "dato"), "html", null, true);
            echo "\" ";
            echo twig_escape_filter($this->env, $this->getContext($context, "seleccionado"), "html", null, true);
            echo ">";
            echo twig_escape_filter($this->env, $this->getContext($context, "dato"), "html", null, true);
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dato'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 89
        echo "                                </select>

                                <h4 class=\"tit_form\">Fechas</h4>
                                <input placeholder=\"Fechas\" class=\"select_fecha\" type=\"text\" name=\"fecha\" id=\"reservation\" value=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->getContext($context, "fecha"), "html", null, true);
        echo "\"/>
                            </div><!-- blq -->

                            <div class=\"blq\">
                                <h4 class=\"tit_form\">Servicios</h4>

                                <ul class=\"checkboxes servicios\">
                                    <li class=\"piscina\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"piscina\" id=\"piscina\" checked data-filter=\"servicio-piscina\">
                                        <label for=\"piscina\">Piscina</label>
                                    </li>
                                    <li class=\"spa\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"spa\" id=\"Spa\" checked data-filter=\"servicio-spa\">
                                        <label for=\"Spa\">Spa</label>
                                    </li>
                                    <li class=\"wi-fi\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"wiFi\" id=\"Wi-fi\" checked data-filter=\"servicio-wi-fi\">
                                        <label for=\"Bar\">Wi-Fi</label>
                                    </li>
                                    <li class=\"acceso-adaptado\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"accesoAdaptado\" id=\"Acceso-Adaptado\" checked data-filter=\"servicio-acceso-adaptado\">
                                        <label for=\"Acceso-Adaptado\">Acceso adaptado</label>
                                    </li>
                                    <li class=\"perros\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"aceptanPerros\" id=\"perros\" checked data-filter=\"servicio-aceptan-perros\">
                                        <label for=\"perros\">Aceptan perros</label>
                                    </li>
                                    <li class=\"parking\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"aparcamiento\" id=\"parking\" checked data-filter=\"servicio-aparcamiento\">
                                        <label for=\"parking\">Aparcamiento/Parking</label>
                                    </li>
                                    <li class=\"business-center\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"businessCenter\" id=\"business-center\" checked data-filter=\"servicio-business-center\">
                                        <label for=\"business-center\">Business center</label>
                                    </li>
                                </ul>
                            </div><!-- blq -->

                            <div class=\"blq\">
                                <h4 class=\"tit_form\">Estrella</h4>

                                <ul class=\"estrellas\">
                                    <li class=\"star1\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-stars\" name=\"star1\" id=\"star1\" checked data-stars=\"1\" data-filter=\"stars-1\">
                                        <label for=\"star1\">Una estrella</label>
                                    </li>
                                    <li class=\"star2\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-stars\" name=\"star2\" id=\"star2\" checked data-stars=\"2\" data-filter=\"stars-2\">
                                        <label for=\"star2\">Dos estrellas</label>
                                    </li>
                                    <li class=\"star3\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-stars\" name=\"star3\" id=\"star3\" checked data-stars=\"3\" data-filter=\"stars-3\">
                                        <label for=\"star3\">tres estrellas</label>
                                    </li>
                                    <li class=\"star4\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-stars\" name=\"star4\" id=\"star4\" checked data-stars=\"4\" data-filter=\"stars-4\">
                                        <label for=\"star4\">Cuatro estrellas</label>
                                    </li>
                                    <li class=\"star5\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-stars\" name=\"star5\" id=\"star5\" checked data-stars=\"5\" data-filter=\"stars-5\">
                                        <label for=\"star5\">Cinco estrellas</label>
                                    </li>
                                </ul>
                            </div><!-- blq -->

                            <div class=\"blq\">
                                <h4 class=\"tit_form\">Hoteles seleccionados</h4>

                                <ul class=\"hoteles\" id=\"listado-lateral-hoteles\">
                                    ";
        // line 161
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 162
            echo "                                        <li class=\"hotel_li hotel-selected\"  style=\"display: none;\" id=\"hotel-checkbox-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\" data-hotel-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">
                                            <input type=\"checkbox\" class=\"checkbox-lateral\" id=\"";
            // line 163
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "\">
                                            <label for=\"Hotel Diamante Suites\">";
            // line 164
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "</label>
                                        </li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 167
        echo "                                </ul>
                            </div><!-- blq -->

                    </div><!-- formulario_int -->
                    </form>

                </div><!-- col_izq -->
                <div class=\"col_der\">

                    <div class=\"cabecera_int\">
                        <div class=\"menu_int clearfix menu_desp\">
                            <ul class=\"social\">
                                <li><a href=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "rrss"), "twitter"), "html", null, true);
        echo "\" target=\"_blank\" title=\"twitter\">twitter</a></li>
                                <li><a class=\"facebook\" href=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "rrss"), "facebook"), "html", null, true);
        echo "\" target=\"_blank\" title=\"facebook\">facebook</a></li>
                            </ul>
                            <ul class=\"menu\">                      
                                <li><a href=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"HOME\">HOME</a></li>
                                <li><a href=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_faqs"), "html", null, true);
        echo "\" title=\"FAQS\">FAQS</a></li>
                                <!--<li><a href=\"#\" title=\"AYUDA\">AYUDA</a></li>-->
                                <li><a href=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" title=\"CONTACTO\">CONTACTO</a></li>
                                <li><a href=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_quienesSomos"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos"), "html", null, true);
        echo "</a></li>
                            </ul>
                        </div><!-- menu_int --> 
                        <a class=\"hacer_oferta\" id=\"hacer-oferta\" title=\"\" style=\"\">hacer oferta</a>
                    </div><!-- cabecera_int -->

                    <div class=\"wrapper_hoteles\">


                        ";
        // line 196
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 197
            echo "                            <div class=\"single_hotel clearfix  stars-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "calificacion"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "filtroServicios"), "html", null, true);
            echo "\" id=\"hotel-summary-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">

                                <div class=\"clearfix\">

                                    <img class=\"estrellas_hotel\" src=\"/bundles/hotelesfrontend/img/";
            // line 201
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "calificacion"), "html", null, true);
            echo "stars.png\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "calificacion"), "html", null, true);
            echo " estrellas\">

                                    <div class=\"cont_img\">
                                        <div class=\"slideFotosList\">
                                            <div class=\"pikachoose\">
                                                <ul class=\"pikame_listado\" id=\"pikame-";
            // line 206
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "loop"), "index0"), "html", null, true);
            echo "\">

                                                    ";
            // line 208
            if ((twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "objetohotel"), "imagenes")) > 0)) {
                // line 209
                echo "                                                        ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "objetohotel"), "imagenes"));
                foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
                    // line 210
                    echo "                                                            <li>
                                                                <a  href=\"/uploads/hoteles/";
                    // line 211
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "imagen"), "html", null, true);
                    echo "\" title=\"\" >
                                                                    <img height=\"152\" width=\"215\" src=\"/uploads/hoteles/";
                    // line 212
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "imagen"), "html", null, true);
                    echo "\" alt=\"\"></a>
                                                            </li>
                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 215
                echo "                                                    ";
            }
            // line 216
            echo "                                                </ul>
                                            </div>          
                                        </div> 
                                    </div>

                                    <div class=\"hotel_cont clearfix\">
                                        <h2 class=\"tit_hotel\">";
            // line 222
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "</h2>

                                        <ul class=\"servicios clearfix\">
                                            ";
            // line 225
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "piscina") == 1)) {
                // line 226
                echo "                                                <li class=\"piscina\">Piscina
                                                </li>
                                            ";
            }
            // line 229
            echo "                                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "spa") == 1)) {
                // line 230
                echo "                                                <li class=\"spa\">Spa
                                                </li>
                                            ";
            }
            // line 233
            echo "                                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "wiFi") == 1)) {
                // line 234
                echo "                                                <li class=\"wi-fi\">Wi-fi
                                                </li>
                                            ";
            }
            // line 237
            echo "                                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "accesoAdaptado") == 1)) {
                // line 238
                echo "                                                <li class=\"acceso-adaptado\">Acceso adaptado
                                                </li>
                                            ";
            }
            // line 241
            echo "                                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "aceptanPerros") == 1)) {
                // line 242
                echo "                                                <li class=\"perros\">Aceptan perros
                                                </li>
                                            ";
            }
            // line 245
            echo "                                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "aparcamiento") == 1)) {
                // line 246
                echo "                                                <li class=\"parking\">Aparcamiento/Parking
                                                </li>
                                            ";
            }
            // line 249
            echo "                                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "businessCenter") == 1)) {
                // line 250
                echo "                                                <li class=\"business-center\">Business center
                                                </li>
                                            ";
            }
            // line 253
            echo "                                        </ul>

                                        <div class=\"botones_hotel\">
                                            <a class=\"detalle\" href=\"#\" title=\"\">DETALLE</a>
                                            <a class=\"abrir_map\" href=\"#\" title=\"\">mapa</a>
                                            <input class=\"timbre\" name=\"hotel1\" type=\"checkbox\" data-hotel-id=\"";
            // line 258
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">
                                        </div><!-- botones_hotel -->

                                    </div><!-- hotel_cont -->

                                </div><!-- clearfix -->

                                <div class=\"desplegable clearfix\">
                                    <ul class=\"galeria clearfix\" style=\"height:150px;\">
                                        ";
            // line 267
            $context["id_pikame"] = $this->getAttribute($this->getContext($context, "loop"), "index0");
            // line 268
            echo "                                        ";
            if ((twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "objetohotel"), "imagenes")) > 0)) {
                // line 269
                echo "                                            ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "objetohotel"), "imagenes"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
                    // line 270
                    echo "                                                <li>
                                                    <a href=\"#\" title=\"\" class=\"thumbnail-pikachoose\"   ><img height=\"48\" width=\"70\" src=\"/uploads/hoteles/";
                    // line 271
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "imagen"), "html", null, true);
                    echo "\" alt=\"\" data-pikame-id=\"pikame-";
                    echo twig_escape_filter($this->env, $this->getContext($context, "id_pikame"), "html", null, true);
                    echo "\" data-photo-index=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "loop"), "index0"), "html", null, true);
                    echo "\"></a>
                                                </li>
                                            ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 274
                echo "                                        ";
            }
            echo "                                        


                                    </ul>
                                    <div class=\"hotel_desc\">
                                        <p><b>";
            // line 279
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Descripción"), "html", null, true);
            echo "</b></p>
                                        <p>";
            // line 280
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "descripcion"), "html", null, true);
            echo "</p>
                                        <p><b>";
            // line 281
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Características"), "html", null, true);
            echo "</b></p>
                                        <p>";
            // line 282
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "caracteristicas"), "html", null, true);
            echo "</p>
                                        <a class=\"mas_dets rosa\" href=\"";
            // line 283
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_detallehotel", array("nombrehotel" => $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "url"))), "html", null, true);
            echo "\" title=\"\">Ver todos los detalles del hotel</a>
                                    </div>
                                </div><!-- desplegable -->

                                <div class=\"desplegable_mapa clearfix\">

                                    <div class=\"mapa\" style=\"height:150px;\" data-hotel-id=\"";
            // line 289
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\" data-longitud=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "longitud"), "html", null, true);
            echo "\" data-latitud=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "latitud"), "html", null, true);
            echo "\"></div>

                                </div><!-- desplegable -->

                            </div><!-- single_hotel -->

                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 296
        echo "                    </div><!-- wrapper_hoteles -->
                    ";
        // line 297
        $this->env->loadTemplate("HotelesBackendBundle:Extras:preload.html.twig")->display($context);
        // line 298
        echo "                    
                    ";
        // line 300
        echo "                    <div id=\"infinite-scroll-ajax\" data-last-page=\"1\"/>
                </div><!-- col_der -->
            </div><!-- container -->
        </div><!-- wrapper -->


        <footer id=\"footer\" class=\"foot-h\">

            <div class=\"footer1 footer-opaco\">
                <div class=\"container\">
                    <ul class=\"menu_foot clearfix\">                                                 
                        ";
        // line 313
        echo "                        <!--<li><a href=\"#\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("AYUDA"), "html", null, true);
        echo "</a></li>-->
                        <li><a href=\"";
        // line 314
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONTACTO"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 315
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_privacidad"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES PARTICULARES Y PRIVACIDAD"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 316
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_condicionesLegales"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES DEL SERVICIO"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 317
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_publicaTuHotel"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PUBLICA TU HOTEL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"v_movil\" href=\"";
        // line 318
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_changeVersion", array("version" => "mobile")), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("VERSIÓN MÓVIL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"a_hoteles\" href=\"";
        // line 319
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_login"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ACCESO HOTELES"), "html", null, true);
        echo "</a></li>
                    </ul>
                    <div class=\"att_cliente\">
                        ATENCIÓN AL CLIENTE <br /> <span>911 610 156</span>
                    </div>
                </div><!-- container -->
            </div><!-- footer1 -->
            <div class=\"footer2\">
                <div class=\"container\">
                    <span>© Copyright. Todos los derechos reservados. </span>
                </div><!-- container -->
            </div><!-- footer2 -->

        </footer>

        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
        <script>window.jQuery || document.write('<script src=\"/bundles/hotelesfrontend/js/vendor/jquery/dist/jquery.min.js\"><\\/script>')</script>

        <script src=\"/bundles/hotelesfrontend/js/plugins.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.backstretch.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/moment.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/daterangepicker.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/selectize.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/icheck.min.js\"></script>        

        <!-- slide fotos detalle hotel -->
        <script type=\"text/javascript\" src=\"/bundles/hotelesfrontend/js/plugins/jquery.jcarousel.min.js\"></script>
        <script type=\"text/javascript\" src=\"/bundles/hotelesfrontend/js/plugins/jquery.pikachoose.js\"></script>
        <!-- fancybox -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.pack.js\"></script>
        <!-- sweet-alert -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/sweet-alert.js\"></script>
        <!-- cookies -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.cookiebar.js\"></script>
        <script src=\"https://maps.googleapis.com/maps/api/js?v=3.exp\"></script>
        ";
        // line 355
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a2c94a4_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a2c94a4_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/a2c94a4_listado_1.js");
            // line 358
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
        ";
            // asset "a2c94a4_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a2c94a4_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/a2c94a4_roomastic_2.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
        ";
            // asset "a2c94a4_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a2c94a4_2") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/a2c94a4_ajax-preload_3.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
        ";
        } else {
            // asset "a2c94a4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a2c94a4") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/a2c94a4.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
        ";
        }
        unset($context["asset_url"]);
        // line 360
        echo "        <script>
            (function(b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function() {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-53266623-1');
            ga('send', 'pageview');
        </script>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  726 => 360,  700 => 358,  696 => 355,  655 => 319,  649 => 318,  643 => 317,  637 => 316,  631 => 315,  625 => 314,  620 => 313,  607 => 300,  604 => 298,  602 => 297,  599 => 296,  574 => 289,  565 => 283,  561 => 282,  557 => 281,  553 => 280,  549 => 279,  540 => 274,  519 => 271,  516 => 270,  498 => 269,  495 => 268,  493 => 267,  481 => 258,  474 => 253,  469 => 250,  466 => 249,  461 => 246,  458 => 245,  453 => 242,  450 => 241,  445 => 238,  442 => 237,  437 => 234,  434 => 233,  429 => 230,  426 => 229,  421 => 226,  419 => 225,  413 => 222,  405 => 216,  402 => 215,  393 => 212,  389 => 211,  386 => 210,  381 => 209,  379 => 208,  374 => 206,  364 => 201,  352 => 197,  335 => 196,  319 => 187,  315 => 186,  310 => 184,  306 => 183,  300 => 180,  296 => 179,  282 => 167,  273 => 164,  269 => 163,  262 => 162,  258 => 161,  186 => 92,  181 => 89,  168 => 87,  165 => 86,  161 => 85,  150 => 80,  147 => 79,  133 => 78,  130 => 77,  127 => 76,  124 => 75,  121 => 74,  103 => 73,  101 => 72,  89 => 63,  73 => 49,  71 => 48,  63 => 42,  58 => 37,  54 => 36,  17 => 1,);
    }
}
