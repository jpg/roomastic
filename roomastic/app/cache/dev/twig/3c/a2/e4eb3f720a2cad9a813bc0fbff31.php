<?php

/* HotelesBackendBundle:Hotel:edit.html.twig */
class __TwigTemplate_3ca2e4eb3f720a2cad9a813bc0fbff31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar hotel"), "html", null, true);
        echo "
";
    }

    // line 7
    public function block_javascripts($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 9
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "834bf80_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_834bf80_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/834bf80_part_1_hotel_create_update_1.js");
            // line 10
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "834bf80"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_834bf80") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/834bf80.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 15
        echo "

    <form action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_update", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
        echo "\" class=\"tasi-form\" method=\"post\" id=\"formulariosubida\"  ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "edit_form"));
        echo ">
        <div class=\"row\">
            <div class=\"col-lg-6\">
                <!--tab nav start-->
                <section class=\"panel\">
                    <header class=\"panel-heading\">
                        ";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar hotel"), "html", null, true);
        echo "
                    </header>
                    <header class=\"panel-heading tab-bg-dark-navy-blue \">
                        <ul class=\"nav nav-tabs\">
                            <li class=\"active ";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "email")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombreempresa")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cif")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "direccionfacturacion")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefono")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "numerocuenta")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#datoshotel\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombrepersonacontacto")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "apellidospersonacontacto")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cargopersonacontacto")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "emailpersonacontacto")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefonopersonacontacto")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#datospersona\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la persona de contacto"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombrehotel")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "url")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "descripcion")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "caracteristicas")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "ubicacion")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "pvpoficialenteros")), 6 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "pvpoficialdecimales")), 7 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cantidadminimaaceptadaenteros")), 8 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cantidadminimaaceptadadecimales")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" class=\"caracteristicashotel\" href=\"#caracteristicashotel\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ficha del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "imagenes")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" class=\"imageneshotel\" href=\"#imageneshotel\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imágenes del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "seotitulo")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "seodescripcion")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "seokeywords")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#seo\">";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Seo"), "html", null, true);
        echo "</a>
                            </li>
                            ";
        // line 42
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 43
            echo "                                <li class=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "comisionhotelentero")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "comisionhoteldecimal")))), "html", null, true);
            echo "\">
                                    <a data-toggle=\"tab\" href=\"#comisiones\">";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comisiones"), "html", null, true);
            echo "</a>
                                </li>
                            ";
        }
        // line 47
        echo "                        </ul>
                    </header>
                    <div class=\"panel-body\">
                        <div class=\"tab-content\">
                            <div id=\"datoshotel\" class=\"tab-pane active\">
                                <div class=\"form-group ";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "email"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 55
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "email"));
        echo "
                                        ";
        // line 56
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "empresa"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 62
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "empresa"));
        echo "
                                        ";
        // line 63
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "empresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>                
                                <div class=\"form-group ";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombreempresa"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de empresa"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 69
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombreempresa"));
        echo "
                                        ";
        // line 70
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "nombreempresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cif"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cif"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 76
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cif"));
        echo "
                                        ";
        // line 77
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "cif"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "direccionfacturacion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dirección de facturación"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 83
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "direccionfacturacion"));
        echo "
                                        ";
        // line 84
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "direccionfacturacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefono"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 90
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefono"));
        echo "
                                        ";
        // line 91
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "telefono"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "numerocuenta"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de cuenta"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 97
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "numerocuenta"));
        echo "
                                        ";
        // line 98
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "numerocuenta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  

                            </div>
                            <div id=\"datospersona\" class=\"tab-pane\">
                                <div class=\"form-group ";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombrepersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la persona contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 107
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombrepersonacontacto"));
        echo "
                                        ";
        // line 108
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "nombrepersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "apellidospersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 114
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "apellidospersonacontacto"));
        echo "
                                        ";
        // line 115
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "apellidospersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cargopersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 121
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cargopersonacontacto"));
        echo "
                                        ";
        // line 122
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "cargopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "emailpersonacontacto"))), "html", null, true);
        echo " \">
                                    <label>";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 128
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "emailpersonacontacto"));
        echo "
                                        ";
        // line 129
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "emailpersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefonopersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 135
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefonopersonacontacto"));
        echo "
                                        ";
        // line 136
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "telefonopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  
                            </div>
                            <div id=\"caracteristicashotel\" class=\"tab-pane\">
                                <div class=\"form-group ";
        // line 141
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombrehotel"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre del hotel"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 144
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombrehotel"));
        echo "
                                        ";
        // line 145
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "nombrehotel"), array("attr" => array("class" => "form-control nombre-hotel")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group ";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "url"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Url"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 151
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "url"));
        echo "
                                        ";
        // line 152
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "url"), array("attr" => array("class" => "form-control url-hotel")));
        echo "                        
                                    </div>
                                </div> 
                                <div class=\"form-group\">
                                    <label>";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Selecciona el número de estrellas"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 158
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "calificacion"));
        echo "
                                        ";
        // line 159
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "calificacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-2\">";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Piscina"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 165
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "piscina"));
        echo "
                                        ";
        // line 166
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "piscina"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Spa"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 171
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "spa"));
        echo "
                                        ";
        // line 172
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "spa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wi-Fi"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 177
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "wi_fi"));
        echo "
                                        ";
        // line 178
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "wi_fi"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-2\">";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso adaptado"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 184
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "acceso_adaptado"));
        echo "
                                        ";
        // line 185
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "acceso_adaptado"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptan perros"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 190
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "aceptan_perros"));
        echo "
                                        ";
        // line 191
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "aceptan_perros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aparcamiento"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 196
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "aparcamiento"));
        echo "
                                        ";
        // line 197
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "aparcamiento"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                </div>
                                <div class=\"form-group padgbott\">

                                    <label class=\"col-sm-2\">";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Business-Center"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 205
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "business_center"));
        echo "
                                        ";
        // line 206
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "business_center"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>                    
                                <div class=\"form-group ";
        // line 209
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "descripcion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica una descripción breve de aproximadamente 100 carácteres"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 212
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "descripcion"));
        echo "
                                        ";
        // line 213
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "descripcion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 216
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "caracteristicas"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enumera las características del hotel de forma breve en aproximadamente 100 carácteres"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 219
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "caracteristicas"));
        echo "
                                        ";
        // line 220
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "caracteristicas"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group ";
        // line 223
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "ubicacion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Escribe el nombre de la calle y número acompañado de la localidad, de la ubicación del hotel"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 226
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "ubicacion"));
        echo "
                                        ";
        // line 227
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "ubicacion"), array("attr" => array("class" => "form-control direccionmapa")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group\">
                                    <label>";
        // line 231
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mapa"), "html", null, true);
        echo "</label>
                                    <div>
                                        <div id=\"mapaiframe\" style=\"width:500px!important;height:250px!important;\">
                                        </div>
                                        <div style=\"clear:both;\"></div>
                                    </div>
                                </div>
                                <br>
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-1\">";
        // line 240
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Provincia"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 242
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "provincia"));
        echo "
                                        ";
        // line 243
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "provincia"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-1\">";
        // line 246
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Municipio"), "html", null, true);
        echo "&nbsp</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 248
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "municipio"));
        echo "
                                        ";
        // line 249
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "municipio"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-1\">";
        // line 252
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Zona"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 254
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "lugar"));
        echo "
                                        ";
        // line 255
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "lugar"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 258
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "pvpoficialenteros"))), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "pvpoficialdecimales"))), "html", null, true);
        echo " padgbott\">
                                    <label class=\"col-sm-6\">";
        // line 259
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica el pvp oficial en habitación individual de uso doble"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 261
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "pvpoficialenteros"));
        echo "
                                        ";
        // line 262
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "pvpoficialenteros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 265
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "pvpoficialdecimales"));
        echo "
                                        ";
        // line 266
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "pvpoficialdecimales"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>                    
                                </div>  
                                <div class=\"clearfix\"></div>
                                <div class=\"form-group ";
        // line 270
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cantidadminimaaceptadaenteros"))), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cantidadminimaaceptadadecimales"))), "html", null, true);
        echo " padgbott\">
                                    <label class=\"col-sm-6\">";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica la cantidad mínima en euros por la que estás dispuesto a recibir ofertas"), "html", null, true);
        echo "</label>        
                                    <div class=\"col-sm-2\">
                                        ";
        // line 273
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cantidadminimaaceptadaenteros"));
        echo "
                                        ";
        // line 274
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "cantidadminimaaceptadaenteros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 277
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cantidadminimaaceptadadecimales"));
        echo "
                                        ";
        // line 278
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "cantidadminimaaceptadadecimales"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>                    
                                </div>  
                                <div class=\"clearfix\"></div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-6\">";
        // line 283
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado de publicación de la ficha del hotel"), "html", null, true);
        echo "</label>
                                    <div  class=\"col-sm-3\">
                                        ";
        // line 285
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "status"));
        echo "
                                        ";
        // line 286
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "status"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  

                            </div>
                            <div id=\"imageneshotel\" class=\"tab-pane\">     
                                <div style=\"height:0px; display:none;\">
                                    ";
        // line 293
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "imagenes"), array("attr" => array("class" => "")));
        echo "   
                                </div>            
                                <div class=\"form-group ";
        // line 295
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "imagenes"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 296
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "imagenes"));
        echo "</label>
                                </div>  
                                <div id=\"imagenes\" style=\"height:15px;width:100%;\">
                                    <input type=\"file\" name=\"archivo\" id=\"inputsubida\">
                                    ";
        // line 303
        echo "                                </div>
                                <iframe src=\"";
        // line 304
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "hoteles")), "html", null, true);
        echo "\" height=\"0\" width=\"1300\" style=\"border:0px;\" name=\"inter\"></iframe>
                                <div id=\"imagenessubidas\" style=\"height:auto;width:100%;background-color:white;\"> 
                                    <div class=\"row\">
                                        <div class=\"col-md-12\">
                                            <section class=\"panel tasks-widget\">

                                                <div class=\"panel-body\">
                                                    <div class=\"task-content\">
                                                        <ul id=\"sortable\" class=\"task-list\" >
                                                            ";
        // line 313
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "imagenes"));
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 314
            echo "                                                                <li class=\"list-primary borraelemento_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "id"), "html", null, true);
            echo "\" data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "id"), "html", null, true);
            echo "\">
                                                                    <i class=\" fa fa-ellipsis-v\"></i>
                                                                    <div class=\"task-title\">
                                                                        <img src=\"/uploads/hoteles/";
            // line 317
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "imagen"), "html", null, true);
            echo "\" width=\"200\">
                                                                        <div class=\"pull-right hidden-phone\"> 
                                                                            <a href=\"#\">
                                                                                <button class=\"btn btn-success btn-xs fa fa-pencil\"></button>
                                                                            </a>&nbsp;&nbsp;
                                                                            <a href=\"#\" onClick=\"deleteElement('";
            // line 322
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "id"), "html", null, true);
            echo "');
                                                                                    return false;\">
                                                                                <button class=\"btn btn-danger btn-xs fa fa-trash-o\"></button>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 330
        echo "                                                        </ul>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>                    
                                </div>
                                <div id=\"imagenessubidasinputs\" style=\"display:none;height:0px;";
        // line 337
        echo "width:100%;background-color:white;\"> 
                                    ";
        // line 338
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "imagenes"));
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 339
            echo "                                        <input type=\"text\" name=\"imageneshotel[]\" class=\"borraelemento_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "id"), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "id"), "html", null, true);
            echo "\"><br>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 341
        echo "                                </div>

                                <a class=\"\" data-toggle=\"modal\" href=\"#myModal\" id=\"clickmodal\"></a>


                                <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" style=\"width: 923px;margin: 36px auto;height: 508px!important;\">
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h4 class=\"modal-title\">";
        // line 351
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar imagen"), "html", null, true);
        echo "</h4>
                                            </div>

                                            <div style=\"width:100%; height:auto; padding-top: 17px;\" >
                                                <img src=\"/bundles/hotelesbackend/img/logo2.png\" id=\"target\" width=\"200\" alt=\"[Jcrop Example]\" />
                                            </div>

                                            <div class=\"clearfix\"></div>

                                            <div class=\"modal-footer\">
                                                <button data-dismiss=\"modal\" id=\"closeimage\" class=\"btn btn-default\" type=\"button\">";
        // line 361
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cerrar"), "html", null, true);
        echo "</button>
                                                <button class=\"btn btn-success\" id=\"successimage\" data-id=\"\" data-url=\"\" data-img=\"\" type=\"button\">";
        // line 362
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar cambios"), "html", null, true);
        echo "</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>          
                            <div id=\"seo\" class=\"tab-pane\">
                                <div class=\"form-group ";
        // line 371
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "seotitulo"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 372
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Título para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 374
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "seotitulo"));
        echo "
                                        ";
        // line 375
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "seotitulo"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 378
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "seodescripcion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 379
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Descripción para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 381
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "seodescripcion"));
        echo "
                                        ";
        // line 382
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "seodescripcion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 385
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "seokeywords"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 386
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Keywords para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 388
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "seokeywords"));
        echo "
                                        ";
        // line 389
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "seokeywords"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                            </div>
                            ";
        // line 393
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 394
            echo "
                                <div id=\"comisiones\" class=\"tab-pane\">
                                    <div class=\"form-group\">
                                        <label for=\"exampleInputEmail1\">";
            // line 397
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Porcentaje de comisión (campo enteros) , (campo decimales)."), "html", null, true);
            echo "</label>
                                        <div class=\"clearfix\"></div>
                                        <div class=\"col-sm-2-dcnt ";
            // line 399
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "comisionhotelentero"))), "html", null, true);
            echo "\">
                                            ";
            // line 400
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "comisionhotelentero"));
            echo "
                                            ";
            // line 401
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "comisionhotelentero"), array("attr" => array("class" => "form-control")));
            echo "                     
                                        </div>
                                        <div class=\"comaclear\" style=\"float:left\"> , </div>
                                        <div class=\"col-sm-2-dcnt ";
            // line 404
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "comisionhoteldecimal"))), "html", null, true);
            echo "\">  
                                            ";
            // line 405
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "comisionhoteldecimal"));
            echo "
                                            ";
            // line 406
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "comisionhoteldecimal"), array("attr" => array("class" => "form-control")));
            echo "                                  
                                        </div>
                                        <div class=\"clearfix\"></div>
                                    </div>
                                </div>
                            ";
        }
        // line 412
        echo "
                        </div>  
                </section>
                <section class=\"panel\">

                    <div class=\"panel-body\">

                        ";
        // line 419
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "edit_form"));
        echo "
                        <button type=\"submit\" class=\"btn btn-info\">";
        // line 420
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>

                        </form>
                        <a href=\"";
        // line 423
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel"), "html", null, true);
        echo "\" class=\"btn btn-primary\">
                            ";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "
                        </a>
                        ";
        // line 426
        if (($this->getAttribute($this->getContext($context, "entity", true), "status", array(), "any", true, true) && ($this->getAttribute($this->getContext($context, "entity"), "status") == 2))) {
            // line 427
            echo "                            <form action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_reactivate", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
            echo "\" method=\"post\" class=\"btnDelete\">
                                ";
            // line 428
            echo $this->env->getExtension('form')->renderWidget($this->getContext($context, "reactivate_form"));
            echo "
                                <button type=\"submit\" class=\"btn btn-success\" onclick=\"return confirm('¿Está seguro de re-activar este hotel?');\">";
            // line 429
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Reactivar hotel"), "html", null, true);
            echo "</button>
                            </form>
                        ";
        } else {
            // line 432
            echo "                            <form action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_delete", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
            echo "\" method=\"post\" class=\"btnDelete\">
                                ";
            // line 433
            echo $this->env->getExtension('form')->renderWidget($this->getContext($context, "delete_form"));
            echo "
                                <button type=\"submit\" class=\"btn btn-danger\"  onclick=\"return confirm('¿Estás seguro que quieres suspender este hotel?\\n' +
                                                'Realizar esta acción cancelará todas las ofertas activas de los usuarios y no se podrán reactivar de ninguna forma.');\">";
            // line 435
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suspender"), "html", null, true);
            echo "</button>
                            </form>
                        ";
        }
        // line 438
        echo "                    </div>
                </section>

            </div><!-- /col -->
        </div><!-- /row -->

    </form>      

";
    }

    // line 448
    public function block_jsextras($context, array $blocks = array())
    {
        // line 449
        echo "
    <script type=\"text/javascript\">
        // carga dinamicamente municipio
        var idprovincia = \$('#form_provincia option:selected').val();
        \$valor_municipio = \$('#form_municipio').val();
        //\$('#form_municipio').load(Routing.generate('admin_lugaresfiltramunicipio', {idprovincia: idprovincia, obligatorio: 0}));
        //\$('#form_municipio').val(\$valor_municipio);

        \$('#form_lugar').html('');
        \$('#form_provincia').change(function (event) {
            var idprovincia = \$('#form_provincia option:selected').val();
            \$('#form_municipio').load(Routing.generate('admin_lugaresfiltramunicipio', {idprovincia: idprovincia, obligatorio: 0}));
        });
        // carga dinamicamente zona        
        \$('#form_municipio').change(function (event) {
            var idprovincia = \$('#form_provincia option:selected').val();
            var idmunicipio = \$('#form_municipio option:selected').val();
            \$('#form_lugar').load(Routing.generate('admin_lugaresfiltralugares', {idprovincia: idprovincia, idmunicipio: idmunicipio, obligatorio: 0}));
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            TaskList.initTaskWidget();
        });

        \$(function () {

            \$(\"#sortable\").sortable({
                stop: function (event, ui) {
                    \$('#imagenessubidasinputs').html('')
                    \$('.list-primary').each(function (indice, elemento) {
                        \$('#imagenessubidasinputs').append('<input type=\"text\" name=\"imageneshotel[]\" class=\"borraelemento_' + \$(elemento).attr('data-id') + '\" value=\"' + \$(elemento).attr('data-id') + '\"><br>')
                    });
                    //return false
                }
            });
            \$(\"#sortable\").disableSelection();
        });
    </script>

    ";
        // line 490
        $context["urlcrea"] = $this->env->getExtension('routing')->getPath("admin_hotel_update", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id")));
        // line 491
        echo "    ";
        $context["urlsubidaimagen"] = $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "hoteles"));
        // line 492
        echo "    ";
        $context["tipo"] = "hoteles";
        // line 493
        echo "    ";
        $context["crop1"] = "100";
        // line 494
        echo "    ";
        $context["crop2"] = "100";
        // line 495
        echo "    ";
        $context["crop3"] = "50";
        // line 496
        echo "    ";
        $context["crop4"] = "50";
        // line 497
        echo "    ";
        $context["crop5"] = "16";
        // line 498
        echo "    ";
        $context["crop6"] = "9";
        // line 499
        echo "
    ";
        // line 500
        $this->env->loadTemplate("HotelesBackendBundle:Crop:crop.html.twig")->display(array_merge($context, array("especifico" => "hoteles")));
        // line 501
        echo "

    <script src=\"/bundles/hotelesbackend/assets/dropzone/dropzone.js\"></script>
    <script src=\"/bundles/hotelesbackend/js/respond.min.js\" ></script>


";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Hotel:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1142 => 501,  1140 => 500,  1137 => 499,  1134 => 498,  1131 => 497,  1128 => 496,  1125 => 495,  1122 => 494,  1119 => 493,  1116 => 492,  1113 => 491,  1111 => 490,  1068 => 449,  1065 => 448,  1053 => 438,  1047 => 435,  1042 => 433,  1037 => 432,  1031 => 429,  1027 => 428,  1022 => 427,  1020 => 426,  1015 => 424,  1011 => 423,  1005 => 420,  1001 => 419,  992 => 412,  983 => 406,  979 => 405,  975 => 404,  969 => 401,  965 => 400,  961 => 399,  956 => 397,  951 => 394,  949 => 393,  942 => 389,  938 => 388,  933 => 386,  929 => 385,  923 => 382,  919 => 381,  914 => 379,  910 => 378,  904 => 375,  900 => 374,  895 => 372,  891 => 371,  879 => 362,  875 => 361,  862 => 351,  850 => 341,  839 => 339,  835 => 338,  832 => 337,  823 => 330,  809 => 322,  801 => 317,  792 => 314,  788 => 313,  776 => 304,  773 => 303,  766 => 296,  762 => 295,  757 => 293,  747 => 286,  743 => 285,  738 => 283,  730 => 278,  726 => 277,  720 => 274,  716 => 273,  711 => 271,  705 => 270,  698 => 266,  694 => 265,  688 => 262,  684 => 261,  679 => 259,  673 => 258,  667 => 255,  663 => 254,  658 => 252,  652 => 249,  648 => 248,  643 => 246,  637 => 243,  633 => 242,  628 => 240,  616 => 231,  609 => 227,  605 => 226,  600 => 224,  596 => 223,  590 => 220,  586 => 219,  581 => 217,  577 => 216,  571 => 213,  567 => 212,  562 => 210,  558 => 209,  552 => 206,  548 => 205,  543 => 203,  534 => 197,  530 => 196,  525 => 194,  519 => 191,  515 => 190,  510 => 188,  504 => 185,  500 => 184,  495 => 182,  488 => 178,  484 => 177,  479 => 175,  473 => 172,  469 => 171,  464 => 169,  458 => 166,  454 => 165,  449 => 163,  442 => 159,  438 => 158,  433 => 156,  426 => 152,  422 => 151,  417 => 149,  413 => 148,  407 => 145,  403 => 144,  398 => 142,  394 => 141,  386 => 136,  382 => 135,  377 => 133,  373 => 132,  367 => 129,  363 => 128,  358 => 126,  354 => 125,  348 => 122,  344 => 121,  339 => 119,  335 => 118,  329 => 115,  325 => 114,  320 => 112,  316 => 111,  310 => 108,  306 => 107,  301 => 105,  297 => 104,  288 => 98,  284 => 97,  279 => 95,  275 => 94,  269 => 91,  265 => 90,  260 => 88,  256 => 87,  250 => 84,  246 => 83,  241 => 81,  237 => 80,  231 => 77,  227 => 76,  222 => 74,  218 => 73,  212 => 70,  208 => 69,  203 => 67,  199 => 66,  193 => 63,  189 => 62,  184 => 60,  180 => 59,  174 => 56,  170 => 55,  165 => 53,  161 => 52,  154 => 47,  148 => 44,  143 => 43,  141 => 42,  136 => 40,  132 => 39,  127 => 37,  123 => 36,  118 => 34,  114 => 33,  109 => 31,  105 => 30,  100 => 28,  96 => 27,  89 => 23,  78 => 17,  74 => 15,  71 => 14,  55 => 10,  51 => 9,  46 => 8,  43 => 7,  37 => 5,  32 => 4,  29 => 3,);
    }
}
