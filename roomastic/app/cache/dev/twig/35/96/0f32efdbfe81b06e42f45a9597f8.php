<?php

/* WebProfilerBundle:Profiler:toolbar_js.html.twig */
class __TwigTemplate_35960f32efdbfe81b06e42f45a9597f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"sfwdt";
        echo twig_escape_filter($this->env, $this->getContext($context, "token"), "html", null, true);
        echo "\" style=\"display: none\"></div>
<script type=\"text/javascript\">/*<![CDATA[*/
    (function () {
        var wdt, xhr;
        wdt = document.getElementById('sfwdt";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "token"), "html", null, true);
        echo "');
        if (window.XMLHttpRequest) {
            xhr = new XMLHttpRequest();
        } else {
            xhr = new ActiveXObject('Microsoft.XMLHTTP');
        }
        xhr.open('GET', '";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_wdt", array("token" => $this->getContext($context, "token"))), "html", null, true);
        echo "', true);
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.onreadystatechange = function(state) {
            if (4 === xhr.readyState && 200 === xhr.status && -1 !== xhr.responseText.indexOf('sf-toolbarreset')) {
                wdt.innerHTML = xhr.responseText;
                wdt.style.display = 'block';
            }
        };
        xhr.send('');
    })();
/*]]>*/</script>
";
    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_js.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 11,  25 => 5,  17 => 1,  517 => 14,  511 => 13,  505 => 12,  469 => 243,  464 => 240,  444 => 238,  440 => 236,  436 => 234,  432 => 232,  428 => 230,  426 => 229,  332 => 140,  326 => 139,  320 => 138,  314 => 137,  308 => 136,  278 => 108,  271 => 107,  255 => 106,  252 => 105,  240 => 103,  237 => 102,  229 => 100,  226 => 99,  222 => 98,  197 => 80,  193 => 79,  189 => 78,  185 => 77,  179 => 74,  175 => 73,  169 => 70,  150 => 53,  148 => 52,  140 => 46,  135 => 41,  131 => 40,  126 => 37,  124 => 36,  122 => 35,  108 => 33,  103 => 32,  77 => 30,  73 => 26,  70 => 25,  56 => 23,  52 => 22,  41 => 14,  37 => 13,  33 => 12,  20 => 1,);
    }
}
