<?php

/* HotelesBackendBundle:Extras:preload.html.twig */
class __TwigTemplate_873cd73695a3ab05468e197e0f6595fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"fade\"></div>
<div id=\"modal\">
    ";
        // line 3
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "2be4f30_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2be4f30_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/images/2be4f30_loading_1.gif");
            // line 4
            echo "    <img id=\"loader\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" alt=\"Cargando...\" />
    ";
        } else {
            // asset "2be4f30"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2be4f30") : $this->env->getExtension('assets')->getAssetUrl("_controller/images/2be4f30.gif");
            echo "    <img id=\"loader\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" alt=\"Cargando...\" />
    ";
        }
        unset($context["asset_url"]);
        // line 5
        echo "  
</div>";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:preload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 5,  25 => 4,  21 => 3,  715 => 356,  689 => 354,  685 => 351,  644 => 315,  638 => 314,  632 => 313,  626 => 312,  620 => 311,  607 => 300,  604 => 298,  602 => 297,  599 => 296,  574 => 289,  565 => 283,  561 => 282,  557 => 281,  553 => 280,  549 => 279,  540 => 274,  519 => 271,  516 => 270,  498 => 269,  495 => 268,  493 => 267,  481 => 258,  474 => 253,  469 => 250,  466 => 249,  461 => 246,  458 => 245,  453 => 242,  450 => 241,  445 => 238,  442 => 237,  437 => 234,  434 => 233,  429 => 230,  426 => 229,  421 => 226,  419 => 225,  413 => 222,  405 => 216,  402 => 215,  393 => 212,  389 => 211,  386 => 210,  381 => 209,  379 => 208,  374 => 206,  364 => 201,  352 => 197,  335 => 196,  319 => 187,  315 => 186,  310 => 184,  306 => 183,  300 => 180,  296 => 179,  282 => 167,  273 => 164,  269 => 163,  262 => 162,  258 => 161,  186 => 92,  181 => 89,  168 => 87,  165 => 86,  161 => 85,  150 => 80,  147 => 79,  133 => 78,  130 => 77,  127 => 76,  124 => 75,  121 => 74,  103 => 73,  101 => 72,  89 => 63,  73 => 49,  71 => 48,  63 => 42,  58 => 37,  54 => 36,  17 => 1,);
    }
}
