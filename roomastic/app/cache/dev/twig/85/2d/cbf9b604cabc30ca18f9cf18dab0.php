<?php

/* HotelesBackendBundle:Crop:crop.html.twig */
class __TwigTemplate_852dcbf9b604cabc30ca18f9cf18dab0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
    var jcrop_api;
    jQuery(document).ready(function (\$) {

        \$('#inputsubida').change(function (event) {
            var urlcrea = '";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "urlcrea"), "html", null, true);
        echo "';
            var urlsubidaimagen = '";
        // line 7
        echo twig_escape_filter($this->env, $this->getContext($context, "urlsubidaimagen"), "html", null, true);
        echo "';
            \$('#formulariosubida').attr('action', urlsubidaimagen);
            \$('#formulariosubida').attr('enctype', 'multipart/form-data');
            \$('#formulariosubida').attr('target', 'inter');
            \$('#formulariosubida').submit();
            \$('#formulariosubida').attr('action', urlcrea);
            \$('#formulariosubida').removeAttr('target');
            event.preventDefault();
        });

        initJcrop();

        \$('#successimage').click(function (event) {
            url = Routing.generate('admin_imagecrop', {
                posx: ROOMASTIC.image_crop.posx,
                posy: ROOMASTIC.image_crop.posy,
                width: ROOMASTIC.image_crop.width,
                height: ROOMASTIC.image_crop.height,
                imagen: ROOMASTIC.image_crop.imagen,
                carpeta: '";
        // line 26
        echo twig_escape_filter($this->env, $this->getContext($context, "tipo"), "html", null, true);
        echo "'});
            \$.get(url, function (respuesta) {
                \$('#closeimage').click();
                var id = \$('#successimage').attr('data-id');
                var nombreImagen = \$('#successimage').attr('data-img');
    ";
        // line 31
        if (($this->getContext($context, "especifico") == "user")) {
            // line 32
            echo "                    \$('#imagenusuario').attr('src', '/uploads/";
            echo twig_escape_filter($this->env, $this->getContext($context, "tipo"), "html", null, true);
            echo "/' + nombreImagen);
                    \$('#imagensubidanombre').val(nombreImagen);
    ";
        } elseif (($this->getContext($context, "especifico") == "hoteles")) {
            // line 35
            echo "                    \$('#imagenessubidasinputs').append('<input type=\"text\" name=\"imageneshotel[]\" class=\"borraelemento_' + id + '\" value=\"' + id + '\"><br>');
                    \$('#sortable').append(' <li class=\"list-primary borraelemento_' + id + '\" data-id=\"' + id + '\"><i class=\" fa fa-ellipsis-v\"></i><div class=\"task-title\"><img src=\"/uploads/hoteles/' + nombreImagen + '\" width=\"200\"><div class=\"pull-right hidden-phone\"><a href=\"#\"><button class=\"btn btn-success btn-xs fa fa-pencil\"></button></a>&nbsp;<a href=\"#\"><button class=\"btn btn-primary btn-xs fa fa-eye\"></button></a>&nbsp;<a href=\"#\" onClick=\"deleteElement(\\'' + id + '\\'); return false;\"><button class=\"btn btn-danger btn-xs fa fa-trash-o\"></button></a></div></div></li>');
    ";
        }
        // line 38
        echo "                    \$.get(Routing.generate('hoteles_backend_imageupload_deletetempfile', {imagen: ROOMASTIC.image_crop.imagen}));
                }).fail(function () {
                    alert('ha habido un error :S');
                });

            });

        });

        function initJcrop()
        {

            \$('.requiresjcrop').hide();
            \$('#target').Jcrop({
                //onRelease: releaseCheck
                onSelect: showCoords,
                bgColor: 'black',
                bgOpacity: .4,
                setSelect: [";
        // line 56
        echo twig_escape_filter($this->env, $this->getContext($context, "crop1"), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->getContext($context, "crop2"), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->getContext($context, "crop3"), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->getContext($context, "crop4"), "html", null, true);
        echo " ],
                aspectRatio: ";
        // line 57
        echo twig_escape_filter($this->env, $this->getContext($context, "crop5"), "html", null, true);
        echo " / ";
        echo twig_escape_filter($this->env, $this->getContext($context, "crop6"), "html", null, true);
        echo ",
                        boxWidth: 800,
            }, function () {

                jcrop_api = this;
                jcrop_api.animateTo([100, 100, 400, 300]);

                \$('#can_click,#can_move,#can_size').attr('checked', 'checked');
                \$('#ar_lock,#size_lock,#bg_swap').attr('checked', false);
                \$('.requiresjcrop').show();

            });

        }
        ;

        function replaceAll(text, busca, reemplaza) {
            while (text.toString().indexOf(busca) != - 1)
                text = text.toString().replace(busca, reemplaza);
            return text;
        }

        function showCoords(c)
        {
            imagen = replaceAll(\$('#successimage').attr('data-img'), \".\", \"___---___\")
            if (typeof c.x !== \"undefined\") {
                var posx = Math.round(c.x)
                var posy = Math.round(c.y)
                var width = Math.round(c.w)
                var height = Math.round(c.h)
            }
            if (typeof ROOMASTIC === \"undefined\") {
                ROOMASTIC = {};
            }
            if (typeof ROOMASTIC.image_crop === \"undefined\") {
                ROOMASTIC.image_crop = {};
            }
            ROOMASTIC.image_crop.imagen = imagen;
            ROOMASTIC.image_crop.posx = posx;
            ROOMASTIC.image_crop.posy = posy;
            ROOMASTIC.image_crop.width = width;
            ROOMASTIC.image_crop.height = height;

            var url = '/' + imagen + '/' + posx + '/' + posy + '/' + width + '/' + height
            \$('#successimage').attr('data-url', url);
        }
        ;

        function inserta(id, nombreImagen) {
            var mayor = 0;
            var dirImages = '/uploads/temporal/';
            \$('#target').attr('src', dirImages + nombreImagen);
            \$('.jcrop-preview').attr('src', dirImages + nombreImagen);
            \$('#successimage').attr('data-id', id);
            \$('#successimage').attr('data-img', nombreImagen);
            jcrop_api.setImage(dirImages + nombreImagen);
            \$('#clickmodal').click();
        }

</script>";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Crop:crop.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 32,  58 => 31,  50 => 26,  28 => 7,  24 => 6,  17 => 1,  1000 => 291,  995 => 290,  993 => 289,  990 => 288,  974 => 284,  952 => 283,  950 => 282,  947 => 281,  935 => 276,  931 => 275,  926 => 274,  924 => 273,  921 => 272,  912 => 266,  906 => 264,  903 => 263,  898 => 262,  896 => 261,  893 => 260,  886 => 255,  877 => 253,  873 => 252,  870 => 251,  867 => 250,  865 => 249,  854 => 244,  852 => 243,  849 => 242,  842 => 237,  831 => 231,  827 => 230,  820 => 228,  818 => 227,  815 => 226,  807 => 222,  805 => 221,  802 => 220,  794 => 214,  789 => 212,  781 => 208,  778 => 207,  752 => 201,  749 => 200,  746 => 199,  741 => 197,  727 => 189,  725 => 188,  722 => 187,  715 => 184,  712 => 183,  709 => 182,  701 => 178,  696 => 176,  693 => 175,  677 => 171,  674 => 170,  672 => 169,  669 => 168,  661 => 164,  656 => 162,  653 => 161,  645 => 157,  642 => 156,  640 => 155,  629 => 150,  626 => 149,  624 => 148,  621 => 147,  613 => 143,  611 => 142,  608 => 141,  597 => 136,  595 => 135,  592 => 134,  584 => 130,  579 => 128,  574 => 126,  559 => 120,  554 => 119,  545 => 116,  540 => 114,  532 => 108,  516 => 100,  514 => 99,  511 => 98,  502 => 92,  498 => 91,  494 => 90,  490 => 89,  485 => 88,  476 => 85,  474 => 84,  471 => 83,  455 => 79,  453 => 78,  450 => 77,  434 => 73,  432 => 72,  429 => 71,  419 => 65,  416 => 64,  405 => 60,  400 => 59,  397 => 58,  388 => 55,  378 => 53,  374 => 51,  366 => 49,  361 => 48,  357 => 47,  352 => 46,  349 => 45,  347 => 44,  323 => 37,  319 => 35,  304 => 33,  300 => 32,  295 => 31,  292 => 30,  287 => 29,  285 => 28,  282 => 27,  272 => 23,  270 => 22,  267 => 21,  259 => 17,  253 => 15,  248 => 13,  245 => 12,  233 => 6,  228 => 5,  226 => 4,  223 => 3,  219 => 288,  216 => 287,  214 => 281,  211 => 280,  209 => 272,  206 => 271,  201 => 260,  198 => 259,  196 => 248,  191 => 242,  188 => 241,  185 => 239,  183 => 236,  178 => 226,  175 => 225,  173 => 220,  167 => 217,  162 => 211,  160 => 205,  157 => 204,  155 => 196,  152 => 195,  149 => 193,  147 => 187,  144 => 186,  142 => 182,  139 => 181,  137 => 175,  134 => 174,  129 => 167,  124 => 160,  122 => 154,  119 => 153,  117 => 147,  112 => 141,  107 => 134,  104 => 133,  102 => 57,  99 => 125,  97 => 114,  94 => 113,  92 => 56,  87 => 83,  84 => 82,  82 => 77,  79 => 76,  77 => 71,  72 => 38,  69 => 42,  67 => 35,  64 => 26,  62 => 21,  59 => 20,  57 => 12,  54 => 11,  52 => 3,  49 => 2,  1142 => 501,  1140 => 500,  1137 => 499,  1134 => 498,  1131 => 497,  1128 => 496,  1125 => 495,  1122 => 494,  1119 => 493,  1116 => 492,  1113 => 491,  1111 => 490,  1068 => 449,  1065 => 448,  1053 => 438,  1047 => 435,  1042 => 433,  1037 => 432,  1031 => 429,  1027 => 428,  1022 => 427,  1020 => 426,  1015 => 424,  1011 => 423,  1005 => 420,  1001 => 419,  992 => 412,  983 => 406,  979 => 405,  975 => 404,  969 => 401,  965 => 400,  961 => 399,  956 => 397,  951 => 394,  949 => 393,  942 => 389,  938 => 388,  933 => 386,  929 => 385,  923 => 382,  919 => 381,  914 => 379,  910 => 378,  904 => 375,  900 => 374,  895 => 372,  891 => 371,  879 => 362,  875 => 361,  862 => 248,  850 => 341,  839 => 236,  835 => 338,  832 => 337,  823 => 229,  809 => 322,  801 => 317,  792 => 213,  788 => 313,  776 => 206,  773 => 205,  766 => 296,  762 => 295,  757 => 293,  747 => 286,  743 => 198,  738 => 196,  730 => 190,  726 => 277,  720 => 274,  716 => 273,  711 => 271,  705 => 270,  698 => 177,  694 => 265,  688 => 262,  684 => 261,  679 => 259,  673 => 258,  667 => 255,  663 => 254,  658 => 163,  652 => 249,  648 => 248,  643 => 246,  637 => 154,  633 => 242,  628 => 240,  616 => 231,  609 => 227,  605 => 226,  600 => 137,  596 => 223,  590 => 220,  586 => 219,  581 => 129,  577 => 127,  571 => 213,  567 => 121,  562 => 210,  558 => 209,  552 => 206,  548 => 117,  543 => 115,  534 => 197,  530 => 104,  525 => 103,  519 => 101,  515 => 190,  510 => 188,  504 => 185,  500 => 184,  495 => 182,  488 => 178,  484 => 177,  479 => 86,  473 => 172,  469 => 171,  464 => 169,  458 => 166,  454 => 165,  449 => 163,  442 => 159,  438 => 158,  433 => 156,  426 => 152,  422 => 151,  417 => 149,  413 => 63,  407 => 61,  403 => 144,  398 => 142,  394 => 57,  386 => 54,  382 => 135,  377 => 133,  373 => 132,  367 => 129,  363 => 128,  358 => 126,  354 => 125,  348 => 122,  344 => 43,  339 => 119,  335 => 39,  329 => 115,  325 => 114,  320 => 112,  316 => 111,  310 => 108,  306 => 107,  301 => 105,  297 => 104,  288 => 98,  284 => 97,  279 => 95,  275 => 94,  269 => 91,  265 => 90,  260 => 88,  256 => 16,  250 => 14,  246 => 83,  241 => 81,  237 => 7,  231 => 77,  227 => 76,  222 => 74,  218 => 73,  212 => 70,  208 => 69,  203 => 269,  199 => 66,  193 => 247,  189 => 62,  184 => 60,  180 => 235,  174 => 56,  170 => 219,  165 => 212,  161 => 52,  154 => 47,  148 => 44,  143 => 43,  141 => 42,  136 => 40,  132 => 168,  127 => 161,  123 => 36,  118 => 34,  114 => 146,  109 => 140,  105 => 30,  100 => 28,  96 => 27,  89 => 97,  78 => 17,  74 => 70,  71 => 14,  55 => 10,  51 => 9,  46 => 8,  43 => 7,  37 => 5,  32 => 4,  29 => 3,);
    }
}
