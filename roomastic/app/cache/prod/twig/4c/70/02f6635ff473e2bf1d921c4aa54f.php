<?php

/* HotelesFrontendBundle:Frontend:baneo.mv.twig */
class __TwigTemplate_4c7002f6635ff473e2bf1d921c4aa54f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    <div class=\"contenido\">

        <div class=\"contenido baneo\">

            <p class=\"txt1\">";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tu oferta no ha podido ser gestionada."), "html", null, true);
        echo "<br>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tu usuario está suspendido."), "html", null, true);
        echo "</p>
            <p>";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Puedes consultar los motivos en nuestras"), "html", null, true);
        echo " <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_condicionesLegales"), "html", null, true);
        echo "\" class=\"txt_pink\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("condiciones del servicio"), "html", null, true);
        echo "</a>.</p>
            <p>";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si crees que se puede tratarse de un error, no dudes en"), "html", null, true);
        echo " <br>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ponerte en"), "html", null, true);
        echo " <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" class=\"txt_pink\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("contacto"), "html", null, true);
        echo "</a> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("con nosotros"), "html", null, true);
        echo ".</p>

        </div><!-- contenido -->
    </div><!-- contenido -->    

";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:baneo.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 56,  75 => 22,  522 => 222,  519 => 221,  507 => 216,  502 => 213,  497 => 210,  493 => 209,  484 => 205,  479 => 202,  475 => 201,  466 => 197,  457 => 193,  452 => 190,  448 => 189,  443 => 186,  440 => 185,  426 => 177,  413 => 173,  409 => 171,  392 => 167,  389 => 166,  373 => 162,  364 => 156,  352 => 146,  339 => 143,  334 => 142,  325 => 141,  320 => 140,  216 => 65,  159 => 26,  40 => 6,  721 => 339,  717 => 337,  714 => 336,  711 => 335,  708 => 334,  705 => 333,  688 => 320,  671 => 305,  653 => 302,  650 => 301,  645 => 300,  630 => 289,  616 => 279,  608 => 275,  593 => 265,  587 => 263,  582 => 262,  574 => 260,  562 => 252,  553 => 250,  542 => 243,  535 => 238,  532 => 237,  517 => 226,  508 => 220,  494 => 214,  488 => 206,  476 => 208,  470 => 198,  461 => 194,  454 => 201,  437 => 188,  433 => 182,  424 => 182,  418 => 174,  412 => 178,  406 => 176,  400 => 168,  394 => 172,  388 => 170,  382 => 168,  375 => 164,  367 => 162,  341 => 142,  333 => 137,  327 => 134,  282 => 93,  255 => 76,  250 => 74,  246 => 73,  213 => 51,  197 => 48,  105 => 21,  777 => 356,  747 => 354,  743 => 351,  702 => 315,  695 => 314,  689 => 313,  683 => 312,  677 => 311,  664 => 300,  661 => 303,  659 => 297,  656 => 296,  628 => 289,  618 => 283,  613 => 282,  609 => 281,  604 => 280,  600 => 279,  591 => 274,  567 => 271,  564 => 270,  545 => 269,  541 => 268,  538 => 267,  525 => 224,  518 => 253,  513 => 250,  509 => 249,  504 => 246,  500 => 216,  495 => 242,  491 => 241,  486 => 238,  482 => 210,  477 => 234,  473 => 233,  468 => 230,  464 => 229,  459 => 226,  456 => 225,  449 => 222,  441 => 216,  438 => 215,  428 => 212,  423 => 211,  420 => 210,  414 => 209,  411 => 208,  405 => 206,  393 => 201,  378 => 163,  344 => 187,  331 => 183,  324 => 180,  319 => 179,  295 => 164,  281 => 162,  276 => 161,  177 => 86,  277 => 92,  273 => 125,  264 => 120,  260 => 78,  257 => 117,  231 => 93,  168 => 14,  396 => 137,  391 => 136,  386 => 135,  381 => 134,  376 => 133,  371 => 132,  365 => 130,  360 => 196,  355 => 128,  350 => 127,  345 => 143,  340 => 186,  335 => 184,  330 => 123,  323 => 119,  316 => 116,  312 => 115,  305 => 167,  301 => 111,  294 => 108,  283 => 104,  279 => 103,  275 => 101,  268 => 122,  265 => 96,  262 => 95,  245 => 73,  234 => 85,  230 => 84,  205 => 69,  190 => 34,  187 => 45,  184 => 61,  169 => 38,  139 => 50,  81 => 47,  293 => 128,  290 => 163,  274 => 105,  270 => 104,  252 => 92,  248 => 96,  241 => 88,  237 => 67,  233 => 90,  226 => 56,  223 => 80,  220 => 84,  210 => 37,  188 => 73,  181 => 69,  145 => 25,  137 => 26,  128 => 21,  120 => 40,  41 => 8,  142 => 24,  129 => 35,  125 => 41,  221 => 66,  203 => 92,  198 => 60,  185 => 71,  179 => 69,  163 => 27,  157 => 30,  152 => 60,  133 => 22,  126 => 45,  110 => 22,  76 => 47,  70 => 40,  222 => 85,  207 => 81,  204 => 80,  183 => 74,  167 => 70,  164 => 69,  148 => 26,  141 => 61,  103 => 34,  98 => 28,  59 => 31,  49 => 14,  53 => 29,  21 => 2,  100 => 33,  97 => 32,  18 => 1,  151 => 53,  135 => 49,  114 => 25,  206 => 77,  201 => 75,  194 => 35,  191 => 70,  176 => 61,  166 => 59,  158 => 65,  153 => 79,  143 => 58,  134 => 25,  123 => 80,  118 => 34,  90 => 29,  87 => 11,  66 => 39,  122 => 74,  107 => 19,  101 => 72,  95 => 48,  82 => 25,  67 => 40,  52 => 11,  45 => 13,  36 => 9,  34 => 4,  266 => 117,  263 => 101,  259 => 100,  256 => 93,  242 => 13,  229 => 170,  227 => 84,  218 => 53,  209 => 78,  192 => 47,  186 => 75,  180 => 73,  174 => 95,  162 => 62,  160 => 31,  146 => 50,  140 => 92,  136 => 77,  106 => 70,  73 => 11,  69 => 18,  22 => 6,  60 => 14,  55 => 13,  102 => 16,  89 => 63,  63 => 42,  56 => 17,  50 => 11,  43 => 10,  92 => 26,  79 => 12,  57 => 9,  37 => 7,  33 => 6,  29 => 4,  19 => 1,  47 => 26,  30 => 4,  27 => 3,  249 => 14,  239 => 90,  235 => 12,  228 => 92,  224 => 90,  219 => 84,  217 => 71,  214 => 70,  211 => 64,  208 => 49,  202 => 68,  199 => 67,  193 => 64,  182 => 87,  178 => 41,  175 => 23,  172 => 85,  165 => 55,  161 => 13,  156 => 51,  154 => 12,  150 => 48,  147 => 59,  132 => 76,  127 => 26,  113 => 50,  86 => 24,  83 => 57,  78 => 54,  64 => 14,  61 => 23,  48 => 12,  32 => 3,  24 => 3,  117 => 41,  112 => 38,  109 => 20,  104 => 27,  96 => 24,  84 => 39,  80 => 19,  68 => 17,  46 => 9,  44 => 9,  26 => 3,  23 => 4,  39 => 9,  25 => 3,  20 => 2,  17 => 1,  144 => 53,  138 => 23,  130 => 46,  124 => 25,  121 => 42,  115 => 33,  111 => 52,  108 => 37,  99 => 33,  94 => 30,  91 => 29,  88 => 28,  85 => 26,  77 => 20,  74 => 16,  71 => 48,  65 => 9,  62 => 17,  58 => 37,  54 => 36,  51 => 11,  42 => 10,  38 => 5,  35 => 8,  31 => 3,  28 => 2,);
    }
}
