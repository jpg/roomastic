<?php

/* HotelesBackendBundle:Faqs:index.html.twig */
class __TwigTemplate_21cea0ef158d3c3282514d8cf87df379 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de preguntas frecuentes"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "

<div class=\"row\">
    <div class=\"col-lg-12\">
      <section class=\"panel\">
          <header class=\"panel-heading\">
             ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de preguntas frecuentes"), "html", null, true);
        echo "
          </header>
          <div class=\"panel-body\">
                <!--  buscador + rtdos -->
                <div class=\"adv-table\">
                    <div class=\"row\">
                        <div class=\"col-lg-6\">
                            ";
        // line 27
        echo "                        </div>
                        <div class=\"col-lg-6\">
                            ";
        // line 34
        echo "                        </div>
                    </div>
                    <!-- / buscador + rtdos -->
                    <!--  contentTabla -->
                     <div class=\"task-content\">
                        <ul id=\"sortable\" class=\"task-list\">

                          ";
        // line 41
        if (isset($context["entities"])) { $_entities_ = $context["entities"]; } else { $_entities_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_entities_);
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 42
            echo "                            <li class=\"list-primary\" data-id=\"";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "id"), "html", null, true);
            echo "\">
                                <i class=\" fa fa-ellipsis-v\"></i>
                                <div class=\"task-title\">
                                    <span class=\"task-title-sp\">";
            // line 45
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "pregunta"), "html", null, true);
            echo "</span>
                                    <div class=\"pull-right hidden-phone\">
                                          <a href=\"";
            // line 47
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_faqs_edit", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\">
                                              <button class=\"btn btn-success btn-xs fa fa-pencil\"></button>
                                          </a>
                                          ";
            // line 55
            echo "                                              <button class=\"btn btn-danger btn-xs fa fa-trash-o\"></button>
                                    </div>
                                </div>
                            </li>

                          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 61
        echo "

                        </ul>
                    </div>
                    <!--  /contentTabla -->
                   
                </div> <!-- /addtable -->
            </div><!-- /panelbody -->

      </section>

      <!-- btns -->
      <section class=\"panel\">
            <div class=\"panel-body\">
                <div class=\"span6\">
                    <a href=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_faqs_new"), "html", null, true);
        echo "\" class=\"btn btn-success\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear una nueva entrada"), "html", null, true);
        echo "</a>
                    <a class=\"btn btn-warning\" href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_faqs"), "html", null, true);
        echo "\" target=\"_blank\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver en la web"), "html", null, true);
        echo "</a>
                    
                </div>
                <div class=\"span6\">
                   ";
        // line 86
        echo "                </div>                  
            </div>          
      </section>
      <!-- /btns -->

    </div><!-- /col -->

</div><!-- /row -->




";
        // line 144
        echo "

";
    }

    // line 149
    public function block_jsextras($context, array $blocks = array())
    {
        // line 150
        echo "
    <script>
        jQuery(document).ready(function() {
            TaskList.initTaskWidget();
        });

        \$(function() {
            \$( \"#sortable\" ).sortable({
                stop: function( event, ui ) {
                    var nuevoOrden = ''
                    \$('.list-primary').each(function(indice, elemento) {
                        nuevoOrden = nuevoOrden+'_'+\$(elemento).attr('data-id')
                    });
                    //console.log(nuevoOrden)
                    \$.get(  Routing.generate('admin_ordena', { orden: nuevoOrden })  , function( data ) {
                        //console.log( 'resultado'+data );
                    });
                }
            });
            \$( \"#sortable\" ).disableSelection();
        });
    </script>

";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Faqs:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 150,  165 => 149,  159 => 144,  145 => 86,  136 => 77,  130 => 76,  113 => 61,  102 => 55,  95 => 47,  89 => 45,  81 => 42,  76 => 41,  67 => 34,  63 => 27,  53 => 15,  45 => 9,  42 => 8,  36 => 5,  31 => 4,  28 => 3,);
    }
}
