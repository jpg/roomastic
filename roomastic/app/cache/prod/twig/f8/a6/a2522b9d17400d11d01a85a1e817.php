<?php

/* HotelesFrontendBundle:Frontend:quienesSomos.mv.twig */
class __TwigTemplate_f8a6a2522b9d17400d11d01a85a1e817 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"contenido\">
        <h2>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes somos?"), "html", null, true);
        echo "</h2>
        <div class=\"txtBack quienesSomos\">

            ";
        // line 8
        if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
        if ($this->getAttribute($_content_, "texto", array(), "any", true, true)) {
            // line 9
            echo "                <span>\t";
            if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
            echo $this->getAttribute($_content_, "texto");
            echo "</span>
            ";
        }
        // line 11
        echo "        </div>
    </div><!-- contenido -->
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:quienesSomos.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 11,  41 => 9,  38 => 8,  32 => 5,  29 => 4,  26 => 3,);
    }
}
