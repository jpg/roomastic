<?php

/* HotelesBackendBundle:Oferta:contraoferta.html.twig */
class __TwigTemplate_bd33bc4d08a1ead5dd1feee084000656 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta > contraoferta"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta > contraoferta"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body contraoferta\">
                    <p>";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("La oferta actual es de"), "html", null, true);
        echo ": <span> ";
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($_entity_, "preciototaloferta"), 2, ",", "."), "html", null, true);
        echo " ";
        echo "euros";
        echo "</span>.</p>

                    <form role=\"form\" action=\"\"  method=\"post\" ";
        // line 19
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_edit_form_);
        echo ">

                        ";
        // line 22
        echo "

                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Precio total de la contraoferta"), "html", null, true);
        echo "</label>
                            <div class=\"col-sm-3\">
                                ";
        // line 27
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "preciototalcontraoferta"));
        echo "
                                ";
        // line 28
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "preciototalcontraoferta"), array("attr" => array("class" => "form-control")));
        echo "    
                                ";
        // line 29
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_edit_form_);
        echo "
                            </div>
                        </div>

                </div><!-- /panelbody -->

            </section>

            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-info\">";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    <a href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
                </div>
            </section>
            <!-- /btns -->


        </div><!-- /col -->

    </div><!-- /row -->

    
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Oferta:contraoferta.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 42,  108 => 40,  93 => 29,  88 => 28,  83 => 27,  78 => 25,  73 => 22,  67 => 19,  57 => 17,  51 => 14,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
