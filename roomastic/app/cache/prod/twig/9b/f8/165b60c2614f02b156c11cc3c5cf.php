<?php

/* KnpPaginatorBundle:Pagination:filtration.html.twig */
class __TwigTemplate_9bf8165b60c2614f02b156c11cc3c5cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form method=\"get\" action=\"";
        if (isset($context["action"])) { $_action_ = $context["action"]; } else { $_action_ = null; }
        echo twig_escape_filter($this->env, $_action_, "html", null, true);
        echo "\" enctype=\"application/x-www-form-urlencoded\">

    <select name=\"";
        // line 3
        if (isset($context["filterFieldName"])) { $_filterFieldName_ = $context["filterFieldName"]; } else { $_filterFieldName_ = null; }
        echo twig_escape_filter($this->env, $_filterFieldName_, "html", null, true);
        echo "\">
        ";
        // line 4
        if (isset($context["fields"])) { $_fields_ = $context["fields"]; } else { $_fields_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_fields_);
        foreach ($context['_seq'] as $context["field"] => $context["label"]) {
            // line 5
            echo "            <option value=\"";
            if (isset($context["field"])) { $_field_ = $context["field"]; } else { $_field_ = null; }
            echo twig_escape_filter($this->env, $_field_, "html", null, true);
            echo "\"";
            if (isset($context["selectedField"])) { $_selectedField_ = $context["selectedField"]; } else { $_selectedField_ = null; }
            if (isset($context["field"])) { $_field_ = $context["field"]; } else { $_field_ = null; }
            if (($_selectedField_ == $_field_)) {
                echo " selected=\"selected\"";
            }
            echo ">";
            if (isset($context["label"])) { $_label_ = $context["label"]; } else { $_label_ = null; }
            echo twig_escape_filter($this->env, $_label_, "html", null, true);
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['field'], $context['label'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 7
        echo "    </select>

    <input type=\"text\" value=\"";
        // line 9
        if (isset($context["selectedValue"])) { $_selectedValue_ = $context["selectedValue"]; } else { $_selectedValue_ = null; }
        echo twig_escape_filter($this->env, $_selectedValue_, "html", null, true);
        echo "\" name=\"";
        if (isset($context["filterValueName"])) { $_filterValueName_ = $context["filterValueName"]; } else { $_filterValueName_ = null; }
        echo twig_escape_filter($this->env, $_filterValueName_, "html", null, true);
        echo "\" />

    <button>";
        // line 11
        if (isset($context["options"])) { $_options_ = $context["options"]; } else { $_options_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_options_, "button"), "html", null, true);
        echo "</button>

</form>";
    }

    public function getTemplateName()
    {
        return "KnpPaginatorBundle:Pagination:filtration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 14,  49 => 10,  53 => 7,  21 => 2,  100 => 42,  97 => 41,  18 => 1,  151 => 75,  135 => 68,  114 => 55,  206 => 77,  201 => 76,  194 => 71,  191 => 70,  176 => 61,  166 => 58,  158 => 56,  153 => 55,  143 => 49,  134 => 44,  123 => 40,  118 => 39,  90 => 43,  87 => 47,  66 => 11,  122 => 37,  107 => 34,  101 => 33,  95 => 29,  82 => 45,  67 => 17,  52 => 11,  45 => 6,  36 => 5,  34 => 5,  266 => 117,  263 => 116,  259 => 85,  256 => 84,  242 => 13,  229 => 170,  227 => 116,  218 => 110,  209 => 78,  192 => 98,  186 => 67,  180 => 63,  174 => 95,  162 => 85,  160 => 84,  146 => 50,  140 => 70,  136 => 41,  106 => 66,  73 => 20,  69 => 17,  22 => 6,  60 => 15,  55 => 9,  102 => 19,  89 => 16,  63 => 14,  56 => 33,  50 => 12,  43 => 14,  92 => 48,  79 => 40,  57 => 9,  37 => 7,  33 => 5,  29 => 4,  19 => 1,  47 => 5,  30 => 4,  27 => 3,  249 => 14,  239 => 90,  235 => 12,  228 => 84,  224 => 82,  219 => 80,  217 => 79,  214 => 79,  211 => 77,  208 => 76,  202 => 72,  199 => 99,  193 => 67,  182 => 63,  178 => 61,  175 => 60,  172 => 59,  165 => 55,  161 => 57,  156 => 51,  154 => 50,  150 => 48,  147 => 74,  132 => 39,  127 => 43,  113 => 34,  86 => 49,  83 => 25,  78 => 38,  64 => 16,  61 => 36,  48 => 10,  32 => 5,  24 => 3,  117 => 36,  112 => 69,  109 => 53,  104 => 51,  96 => 32,  84 => 39,  80 => 24,  68 => 35,  46 => 14,  44 => 10,  26 => 3,  23 => 18,  39 => 6,  25 => 2,  20 => 1,  17 => 1,  144 => 46,  138 => 46,  130 => 66,  124 => 73,  121 => 41,  115 => 40,  111 => 36,  108 => 31,  99 => 49,  94 => 29,  91 => 17,  88 => 41,  85 => 26,  77 => 39,  74 => 18,  71 => 39,  65 => 16,  62 => 34,  58 => 8,  54 => 14,  51 => 32,  42 => 8,  38 => 8,  35 => 5,  31 => 3,  28 => 2,);
    }
}
