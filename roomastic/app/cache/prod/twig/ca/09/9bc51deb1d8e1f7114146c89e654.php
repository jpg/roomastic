<?php

/* HotelesFrontendBundle:Frontend:oferta.mv.twig */
class __TwigTemplate_ca099bc51deb1d8e1f7114146c89e654 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'modal' => array($this, 'block_modal'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_javascripts($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        if (typeof ROOMASTIC === 'undefined') {
            ROOMASTIC = {};
        }
        ROOMASTIC.googleID = '";
        // line 8
        if (isset($context["configuracion"])) { $_configuracion_ = $context["configuracion"]; } else { $_configuracion_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_configuracion_, "googAppId"), "html", null, true);
        echo "';
        ROOMASTIC.googleapikey = '";
        // line 9
        if (isset($context["configuracion"])) { $_configuracion_ = $context["configuracion"]; } else { $_configuracion_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_configuracion_, "googApiKey"), "html", null, true);
        echo "';
    </script>
    ";
        // line 11
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "7fd4248_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248_0") : $this->env->getExtension('assets')->getAssetUrl("js/7fd4248_oferta_1.js");
            // line 17
            echo "    <script src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
            // asset "7fd4248_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248_1") : $this->env->getExtension('assets')->getAssetUrl("js/7fd4248_moment_2.js");
            echo "    <script src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
            // asset "7fd4248_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248_2") : $this->env->getExtension('assets')->getAssetUrl("js/7fd4248_sweet-alert_3.js");
            echo "    <script src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
            // asset "7fd4248_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248_3") : $this->env->getExtension('assets')->getAssetUrl("js/7fd4248_facebook_login_4.js");
            echo "    <script src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
            // asset "7fd4248_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248_4") : $this->env->getExtension('assets')->getAssetUrl("js/7fd4248_google_login_5.js");
            echo "    <script src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "7fd4248"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248") : $this->env->getExtension('assets')->getAssetUrl("js/7fd4248.js");
            echo "    <script src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 20
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 21
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    ";
        // line 22
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "b3922df_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b3922df_0") : $this->env->getExtension('assets')->getAssetUrl("css/b3922df_minimal.mv_1.css");
            // line 25
            echo "       
    <link rel=\"stylesheet\" href=\"";
            // line 26
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\">
    ";
            // asset "b3922df_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b3922df_1") : $this->env->getExtension('assets')->getAssetUrl("css/b3922df_sweet-alert_2.css");
            // line 25
            echo "       
    <link rel=\"stylesheet\" href=\"";
            // line 26
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\">
    ";
            // asset "b3922df_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b3922df_2") : $this->env->getExtension('assets')->getAssetUrl("css/b3922df_oferta.mv_3.css");
            // line 25
            echo "       
    <link rel=\"stylesheet\" href=\"";
            // line 26
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\">
    ";
        } else {
            // asset "b3922df"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b3922df") : $this->env->getExtension('assets')->getAssetUrl("css/b3922df.css");
            // line 25
            echo "       
    <link rel=\"stylesheet\" href=\"";
            // line 26
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\">
    ";
        }
        unset($context["asset_url"]);
    }

    // line 30
    public function block_modal($context, array $blocks = array())
    {
        // line 31
        echo "    <div class=\"modalregister\">
        <div class=\"header\">
            <a href=\"#\" class=\"close\">X</a>
        </div>
        <div class=\"body\">
            <div class=\"registro\">

                <form class=\"form-signin\" action=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_check"), "html", null, true);
        echo "\" method=\"post\" id=\"formlogin\" ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo ">
                    <div class=\"errorlogin\"></div>

                    <h2 class=\"form-signin-heading\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Debes estar registrado para"), "html", null, true);
        echo "<br> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("hacer tu oferta"), "html", null, true);
        echo "</h2>


                    <div class=\"login-wrap \">
                        <p class=\"tit\">";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso"), "html", null, true);
        echo "</p>

                        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 47
        if (isset($context["csrf_token"])) { $_csrf_token_ = $context["csrf_token"]; } else { $_csrf_token_ = null; }
        echo twig_escape_filter($this->env, $_csrf_token_, "html", null, true);
        echo "\" />
                        <input type=\"text\" class=\"form-control\" placeholder=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuario"), "html", null, true);
        echo "\" name=\"_username\" value=\"";
        if (array_key_exists("last_username", $context)) {
            echo " ";
            if (isset($context["last_username"])) { $_last_username_ = $context["last_username"]; } else { $_last_username_ = null; }
            echo twig_escape_filter($this->env, $_last_username_, "html", null, true);
            echo " ";
        }
        echo "\" autofocus>
                        <input type=\"password\" class=\"form-control\" placeholder=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraseña"), "html", null, true);
        echo "\" name=\"_password\">

                        <button class=\"btn btn-lg btn-login btn-block\" type=\"submit\">";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceder"), "html", null, true);
        echo "</button>
                        <p class=\"forgor_ps\">
                            <a href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_request"), "html", null, true);
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado tu contraseña?"), "html", null, true);
        echo "</a>
                        </p>

                        <p>";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("O accede con tus redes sociales:"), "html", null, true);
        echo "</p>

                        <div class=\"login-social-link\">
                            <a href=\"#\" class=\"facebook fontello login-rrss\">
                                <i class=\"icon-facebook-1\"></i>
                                <!--Facebook-->
                            </a>

                            <a href=\"#\" class=\"google login-rrss\">
                                <i class=\"icon-gplus\"></i>
                                <!-- google + -->
                            </a>
                        </div>

                    </div><!-- /left -->

                    <div class=\"login-wrap \">
                        <p class=\"tit\">";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Registro"), "html", null, true);
        echo "</p>
                        <p class=\"alignleft\">";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Puedes registrarte directamente en nuestra web:"), "html", null, true);
        echo "</p>

                        <button class=\"btn btn-lg btn-login btn-block showregister\" type=\"submit\">";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("REGISTRO EN LA WEB"), "html", null, true);
        echo "</button>

                        <p class=\"alignleft\">";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("O con tu cuenta de usuario en alguna de estas redes sociales:"), "html", null, true);
        echo "</p>


                        <div class=\"login-social-link\">

                            <a href=\"#\" class=\"facebook fontello login-rrss\">
                                <i class=\"icon-facebook-1\"></i>
                                <!--Facebook-->
                            </a>
                            <div id=\"fb-root\"></div>
                            <script>
        if (typeof ROOMASTIC === 'undefined') {
            ROOMASTIC = {};
        }
        ROOMASTIC.facebookAppId = '";
        // line 92
        if (isset($context["configuracion"])) { $_configuracion_ = $context["configuracion"]; } else { $_configuracion_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_configuracion_, "fbappid"), "html", null, true);
        echo "';
        ROOMASTIC.state = '";
        // line 93
        if (isset($context["csrf_token"])) { $_csrf_token_ = $context["csrf_token"]; } else { $_csrf_token_ = null; }
        echo twig_escape_filter($this->env, $_csrf_token_, "html", null, true);
        echo "';
        (function () {
            var e = document.createElement('script');
            e.async = true;
            e.src = document.location.protocol +
                    '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());
                            </script>

                            <a href=\"#\" class=\"google special login-rrss\">
                                <i class=\"icon-gplus\"></i>
                                <!-- google + -->

                                <!-- google + -->
                                <script type=\"text/javascript\">
                                    (function () {
                                        var po = document.createElement('script');
                                        po.type = 'text/javascript';
                                        po.async = true;
                                        po.src = 'https://plus.google.com/js/client:plusone.js?onload=googleReady';
                                        var s = document.getElementsByTagName('script')[0];
                                        s.parentNode.insertBefore(po, s);
                                    })();


                                </script>
                            </a>
                        </div>


                    </div><!-- /right -->
            </div>
            <div class=\"clearfix\"></div>

            <!-- Modal -->
            <div aria-hidden=\"true\" aria-labelledby=\"myModalLabel\" role=\"dialog\" tabindex=\"-1\" id=\"myModal\" class=\"modal fade\" style=\"display:none;\">
                <div class=\"modal-dialog\">
                    <div class=\"modal-content\">
                        <div class=\"modal-header\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                            <h4 class=\"modal-title\">";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado la contraseña?"), "html", null, true);
        echo "</h4>
                        </div>
                        <div class=\"modal-body\">
                            <p>";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Escribe tu email y te reenviaremos una nueva contraseña"), "html", null, true);
        echo "</p>
                            <input type=\"text\" name=\"email\" placeholder=\"Email\" autocomplete=\"off\" class=\"form-control placeholder-no-fix\">

                        </div>
                        <div class=\"modal-footer\">
                            <button data-dismiss=\"modal\" class=\"btn btn-default\" type=\"button\">";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cancelar"), "html", null, true);
        echo "</button>
                            <button class=\"btn btn-success\" type=\"button\">";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar"), "html", null, true);
        echo "</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal -->

            </form><!-- form-signin -->


        </div>
        <!--/body -->


        <div class=\"bodyregister hide\">

            <div class=\"registro nuevo_reg\">


                <form class=\"form-signin\" action=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_registroajax"), "html", null, true);
        echo "\" id=\"formregister\" method=\"post\" ";
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_formRegister_);
        echo ">

                    <h2 class=\"form-signin-heading\">";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nuevo registro en la web"), "html", null, true);
        echo "</h2>
                    <div class=\"login-wrap\">
                        <div class=\"input_newreg\">

                            ";
        // line 168
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "nombre"), array("attr" => array("class" => "form-control width1", "placeholder" => $this->env->getExtension('translator')->trans("Nombre"))));
        echo "  

                            ";
        // line 170
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "apellidos"), array("attr" => array("class" => "form-control width2", "placeholder" => $this->env->getExtension('translator')->trans("Apellidos"))));
        echo "   

                            ";
        // line 172
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "direccioncompleta"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("Dirección completa (Campo no obligatorio)"))));
        echo "    

                            ";
        // line 174
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "dni"), array("attr" => array("class" => "form-control width1", "placeholder" => $this->env->getExtension('translator')->trans("DNI (Para dar veracidad a tu oferta)"))));
        echo "

                            ";
        // line 176
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "telefono"), array("attr" => array("class" => "form-control width1", "placeholder" => $this->env->getExtension('translator')->trans("Teléfono (Para contactarte en caso de cambios en la reserva)"))));
        echo "    

                            ";
        // line 178
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "email"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("Email"))));
        echo "

                            ";
        // line 180
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($_formRegister_, "password"), "first"), array("attr" => array("class" => "form-control width6", "placeholder" => $this->env->getExtension('translator')->trans("Contraseña"))));
        echo "

                            ";
        // line 182
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($_formRegister_, "password"), "second"), array("attr" => array("class" => "form-control width7", "placeholder" => $this->env->getExtension('translator')->trans("Repite contraseña"))));
        echo "    


                            <div class=\"clearfix\"></div>

                            <button type=\"submit\" class=\"btn btn-lg btn-login btn-block w-es\" >";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar datos y hacer oferta"), "html", null, true);
        echo "</button>
                            ";
        // line 188
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderRest($_formRegister_);
        echo "
                        </div>
                    </div>
                </form>  
            </div>

        </div>

        <div class=\"bodyregisterextra hide\">

            <div class=\"registro nuevo_reg\">


                <form class=\"form-signin\" action=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_registroextraajax"), "html", null, true);
        echo "\" id=\"formregisterextra\" method=\"post\"  ";
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_formRegisterextra_);
        echo ">
                    <h2 class=\"form-signin-heading\">";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Completa tu registro para"), "html", null, true);
        echo "<br> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("hacer tu oferta"), "html", null, true);
        echo "</h2>
                    <div class=\"login-wrap\">
                        <div class=\"input_newreg\">

                            ";
        // line 206
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "nombre"), array("attr" => array("class" => "form-control width1 nombre", "placeholder" => $this->env->getExtension('translator')->trans("Nombre"))));
        echo "  

                            ";
        // line 208
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "apellidos"), array("attr" => array("class" => "form-control width2 apellidos", "placeholder" => $this->env->getExtension('translator')->trans("Apellidos"))));
        echo "   

                            ";
        // line 210
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "direccioncompleta"), array("attr" => array("class" => "form-control width3 direccion", "placeholder" => $this->env->getExtension('translator')->trans("Dirección completa (Campo no obligatorio)"))));
        echo "

                            ";
        // line 212
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "dni"), array("attr" => array("class" => "form-control width1 dni", "placeholder " => $this->env->getExtension('translator')->trans("DNI (Para dar veracidad a tu oferta)"))));
        echo "

                            ";
        // line 214
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "telefono"), array("attr" => array("class" => "form-control width1 telefono", "placeholder" => $this->env->getExtension('translator')->trans("Teléfono (Para contactarte en caso de cambios en la reserva)"))));
        echo "    

                            ";
        // line 216
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "email"), array("attr" => array("class" => "form-control width3 email", "placeholder" => $this->env->getExtension('translator')->trans("Email"))));
        echo "

                            <div class=\"clearfix\"></div>

                            <button type=\"submit\" class=\"btn btn-lg btn-login btn-block w-es\">";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar datos y hacer oferta"), "html", null, true);
        echo "</button>

                            <div class=\"clearfix\"></div>

                        </div>
                    </div>
                    ";
        // line 226
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderRest($_formRegisterextra_);
        echo "
                </form>  

            </div>

        </div><!--/bodyregisterextra -->

    </div>
</div>
";
    }

    // line 237
    public function block_content($context, array $blocks = array())
    {
        // line 238
        echo "

    <div class=\"wpregistro\">
        <div class=\"oferta\">
            <form action=\"\" method=\"post\" id=\"formoferta\">
                ";
        // line 243
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "_token_oferta"));
        echo "


                <div id=\"tabsWithStyle\" class=\"style-tabs\">
                    <div class=\"clearfix\"></div>
                    <div id=\"tabs-1\">
                        <h2>¿Cuánto estás dispuesto a pagar?</h2>
                        <p>El precio debe ser por noche, habitación doble y en régimen de alojamiento.Los precios oficiales de los hoteles que has seleccionado rondan entre los <b>";
        // line 250
        if (isset($context["precio_hotel_min"])) { $_precio_hotel_min_ = $context["precio_hotel_min"]; } else { $_precio_hotel_min_ = null; }
        echo twig_escape_filter($this->env, $_precio_hotel_min_, "html", null, true);
        echo " € y los ";
        if (isset($context["precio_hotel_max"])) { $_precio_hotel_max_ = $context["precio_hotel_max"]; } else { $_precio_hotel_max_ = null; }
        echo twig_escape_filter($this->env, $_precio_hotel_max_, "html", null, true);
        echo "  €</b>
                        (Algunos hoteles ofrecen desayuno o media pensión incluido)</p>
                        ";
        // line 252
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "preciohabitacion"), array("attr" => array("type" => "number", "placeholder" => "Precio", "tabindex" => "0")));
        echo "
                    </div>

                    <div class=\"clearfix separadortabs\"></div>

                    <div id=\"tabs-2\">
                        <h2>Número de noches</h2>
                        <div class=\"fecha_picker\">
                            ";
        // line 260
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        if (isset($context["fecha_inicio"])) { $_fecha_inicio_ = $context["fecha_inicio"]; } else { $_fecha_inicio_ = null; }
        if (isset($context["fecha_fin"])) { $_fecha_fin_ = $context["fecha_fin"]; } else { $_fecha_fin_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "fecha"), array("attr" => array("value" => ((twig_date_format_filter($this->env, $_fecha_inicio_, "d/m/y") . " - ") . twig_date_format_filter($this->env, $_fecha_fin_, "d/m/y")), "tabindex" => "1", "class" => "select_fecha hide", "placeholder" => "Fechas")));
        echo "

                            <input type=\"text\" id=\"fromDate\" name=\"fromDate\" class=\"input_mov ll-skin-lug\" placeholder=\"Entrada\" data-from-date=\"";
        // line 262
        if (isset($context["fecha_inicio"])) { $_fecha_inicio_ = $context["fecha_inicio"]; } else { $_fecha_inicio_ = null; }
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $_fecha_inicio_, "d-m-y"), "html", null, true);
        echo "\"/>
                            <input type=\"text\" id=\"toDate\" name=\"toDate\" placeholder=\"Salida\" data-to-date=\"";
        // line 263
        if (isset($context["fecha_fin"])) { $_fecha_fin_ = $context["fecha_fin"]; } else { $_fecha_fin_ = null; }
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $_fecha_fin_, "d-m-y"), "html", null, true);
        echo "\"/>
                        </div>
                        <p>Total número de noches ";
        // line 265
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        if (isset($context["num_noches"])) { $_num_noches_ = $context["num_noches"]; } else { $_num_noches_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "numnoches"), array("attr" => array("value" => $_num_noches_, "placeholder" => "3", "readonly" => "")));
        echo "</p>

                    </div>

                    <div class=\"clearfix separadortabs\"></div>

                    <div id=\"tabs-3\">
                        <h2>Selecciona el número de huéspedes</h2>
                        <p>
                            <label>Adultos</label>
                            ";
        // line 275
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "numadultos"), array("attr" => array("value" => "1", "placeholder" => "1", "tabindex" => "2", "class" => "select_adultos")));
        echo "
                        </p>
                        <p>
                            <label>Niños hasta 3 años</label>
                            ";
        // line 279
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "numninos"), array("attr" => array("value" => "0", "placeholder" => "0", "tabindex" => "3", "class" => "select_ninios")));
        echo "
                        </p>
                    </div>

                    <div class=\"clearfix separadortabs\"></div>

                    <div id=\"tabs-4\">
                        <h2>Selecciona el número de habitaciones</h2>
                        <p>
                            <label>Habitaciones</label>
                            ";
        // line 289
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "numhabitaciones"), array("attr" => array("value" => "1", "placeholder" => "1", "tabindex" => "4", "class" => "select_habs")));
        echo "
                        </p>

                    </div>

                    <div class=\"clearfix separadortabs\"></div>

                    <div id=\"tabs-5\">
                        <h2>Hoteles</h2>

                        <ul class=\"hoteles\">
                            ";
        // line 300
        if (isset($context["hoteles"])) { $_hoteles_ = $context["hoteles"]; } else { $_hoteles_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_hoteles_);
        foreach ($context['_seq'] as $context["_key"] => $context["hotel"]) {
            // line 301
            echo "                                <li class=\"hotel_li\">
                                    <input type=\"checkbox\" class=\"checkbox-oferta\" name=\"hoteles[id][]\" id=\"";
            // line 302
            if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "id"), "html", null, true);
            echo "\" value=\"";
            if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "id"), "html", null, true);
            echo "\" checked>
                                    <label for=\"Hotel Diamante Suites\">";
            // line 303
            if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "nombrehotel"), "html", null, true);
            echo "</label>
                                </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 305
        echo "                    
                        </ul>
                    </div>

                </div><!-- tabs -->

                <div class=\"clearfix separadortabs\"></div>


                <!-- Resumen oferta -->
                <div class=\"resumenOferta\">
                    <h3>Precio total de tu oferta</h3>
                    <div class=\"caja\">
                        <p>En base a la configuración que nos acabas de facilitar, este será el precio total que vas a ofertar a los hoteles seleccionados:</p>

                        ";
        // line 320
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "preciototaloferta"), array("attr" => array("value" => "", "placeholder" => "0", "readonly" => "")));
        echo "
                        <div class=\"clearfix\"></div>

                        <div class=\"acepto1 clearfix\">
                            <input type=\"checkbox\" id=\"acepto1\" class=\"checkbox-oferta\" name=\"acepto_condiciones\" required>
                            <label for=\"acepto1\">Acepto las condiciones de venta y de uso de esta web.</label>
                        </div>

                        <div class=\"acepto2 clearfix\">
                            <input type=\"checkbox\" id=\"acepto2\"  class=\"checkbox-oferta\" name=\"acepto_suscribirme\">
                            <label for=\"acepto2\">Acepto suscribirme al boletín de noticias RooMail</label>
                        </div>
                        <div class=\"validaoferta\">
                            ";
        // line 333
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if ($this->getAttribute($_app_, "user")) {
            // line 334
            echo "                                ";
            $context["title"] = "Ya puedes hacer tu oferta";
            // line 335
            echo "                            ";
        } else {
            // line 336
            echo "                                ";
            $context["title"] = "Todavia tienes que loguearte";
            // line 337
            echo "
                            ";
        }
        // line 339
        echo "                            <a class=\"btn_oferta\"  title=\"";
        if (isset($context["title"])) { $_title_ = $context["title"]; } else { $_title_ = null; }
        echo twig_escape_filter($this->env, $_title_, "html", null, true);
        echo "\" style=\"cursor: pointer;\" >hacer oferta <span class=\"decoration\"></span></a>                        
                        </div>
                    </div><!-- caja -->
                </div><!-- resumenOferta -->
            </form>
        </div>
    </div>
    <!-- /opciones -->
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:oferta.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  721 => 339,  717 => 337,  714 => 336,  711 => 335,  708 => 334,  705 => 333,  688 => 320,  671 => 305,  661 => 303,  653 => 302,  650 => 301,  645 => 300,  630 => 289,  616 => 279,  608 => 275,  593 => 265,  587 => 263,  582 => 262,  574 => 260,  562 => 252,  553 => 250,  542 => 243,  535 => 238,  532 => 237,  517 => 226,  508 => 220,  500 => 216,  494 => 214,  488 => 212,  482 => 210,  476 => 208,  470 => 206,  461 => 202,  454 => 201,  437 => 188,  433 => 187,  424 => 182,  418 => 180,  412 => 178,  406 => 176,  400 => 174,  394 => 172,  388 => 170,  382 => 168,  375 => 164,  367 => 162,  345 => 143,  341 => 142,  333 => 137,  327 => 134,  282 => 93,  277 => 92,  260 => 78,  255 => 76,  250 => 74,  246 => 73,  226 => 56,  218 => 53,  213 => 51,  208 => 49,  197 => 48,  192 => 47,  187 => 45,  178 => 41,  169 => 38,  160 => 31,  157 => 30,  148 => 26,  145 => 25,  137 => 26,  134 => 25,  127 => 26,  124 => 25,  117 => 26,  114 => 25,  110 => 22,  105 => 21,  102 => 20,  56 => 17,  52 => 11,  46 => 9,  41 => 8,  32 => 3,  29 => 2,);
    }
}
