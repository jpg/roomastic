<?php

/* HotelesBackendBundle:User:profile-extended.html.twig */
class __TwigTemplate_b328168caec35843770a392a3c258e46 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
        if (($this->getAttribute($_user_, "getTypeOf") == "admin")) {
            // line 2
            echo "    <tr>
        <th>";
            // line 3
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre publico"), "html", null, true);
            echo "</th>
        <td>";
            // line 4
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_user_, "getPublicUsername"), "html", null, true);
            echo "</td>
    </tr>
";
        } elseif (($this->getAttribute($_user_, "getTypeOf") == "hotel")) {
            // line 7
            echo "    <tr>
        <th>";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
            echo "</th>
        <td>";
            // line 9
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_user_, "getNombreCompleto"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administrador"), "html", null, true);
            echo "</th>
        <td>
            ";
            // line 14
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            if ($this->getAttribute($_user_, "isAdmin")) {
                // line 15
                echo "                ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si"), "html", null, true);
                echo " 
            ";
            } else {
                // line 17
                echo "                ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No"), "html", null, true);
                echo "
            ";
            }
            // line 19
            echo "        </td>
    </tr>
    <tr>
        <th>";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
            echo "</th>
        <td>";
            // line 23
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_user_, "hotel"), "nombrehotel"), "html", null, true);
            echo "</td>
    </tr>
";
        } elseif (($this->getAttribute($_user_, "getTypeOf") == "empresa")) {
            // line 26
            echo "    <tr>
        <th>";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
            echo "</th>
        <td>";
            // line 28
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_user_, "getNombreCompleto"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
            echo "</th>
        <td>";
            // line 32
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_user_, "empresa"), "nombreempresa"), "html", null, true);
            echo "</td>
    </tr>
";
        } elseif (($this->getAttribute($_user_, "getTypeOf") == "basico")) {
            // line 35
            echo "    <tr>
        <th>";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
            echo "</th>
        <td>";
            // line 37
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_user_, "getNombreCompleto"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("DNI"), "html", null, true);
            echo "</th>
        <td>";
            // line 41
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_user_, "dni"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Direccion"), "html", null, true);
            echo "</th>
        <td>";
            // line 45
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_user_, "direccioncompleta"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
            echo "</th>
        <td>";
            // line 49
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_user_, "telefono"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Origen"), "html", null, true);
            echo "</th>
        <td>";
            // line 53
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_user_, "origin"), "html", null, true);
            echo "</td>
    </tr>
";
        } else {
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:User:profile-extended.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 53,  149 => 49,  145 => 48,  138 => 45,  134 => 44,  127 => 41,  123 => 40,  116 => 37,  109 => 35,  102 => 32,  91 => 28,  87 => 27,  84 => 26,  68 => 19,  56 => 15,  53 => 14,  48 => 12,  37 => 8,  34 => 7,  23 => 3,  20 => 2,  17 => 1,  168 => 61,  165 => 60,  156 => 52,  147 => 56,  143 => 55,  139 => 54,  130 => 52,  121 => 50,  118 => 49,  112 => 36,  98 => 31,  95 => 35,  89 => 33,  85 => 32,  80 => 29,  77 => 23,  73 => 22,  66 => 24,  62 => 17,  51 => 15,  44 => 10,  41 => 9,  35 => 5,  30 => 4,  27 => 4,);
    }
}
