<?php

/* HotelesBackendBundle:Oferta:status.html.twig */
class __TwigTemplate_1ce41cb6cf92f82b868111fccd44b2a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 26
        if (isset($context["status"])) { $_status_ = $context["status"]; } else { $_status_ = null; }
        if (($_status_ == "0")) {
            // line 27
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta pendiente"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "1")) {
            // line 29
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta superior al PVP"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "2")) {
            // line 31
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta inferior al minimo"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "10")) {
            // line 33
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraoferta realizada"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "11")) {
            // line 35
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraoferta rechazada"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "12")) {
            // line 37
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraoferta caducada"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "13")) {
            // line 39
            echo "    <span class=\"label label-success\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraoferta pagada"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "14")) {
            // line 41
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta aceptada por otro hotel"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "20")) {
            // line 43
            echo "    <span class=\"label label-success\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta pagada"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "21")) {
            // line 45
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta NO pagada"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "22")) {
            // line 47
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta rechazada"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "23")) {
            // line 49
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta caducada"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "24")) {
            // line 51
            echo "    <span class=\"label label-success \">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta aceptada"), "html", null, true);
            echo "</span>
";
        } elseif (($_status_ == "25")) {
            // line 53
            echo "    <span class=\"label label-success \">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta aceptada por otro hotel"), "html", null, true);
            echo "</span>
";
        } else {
            // line 55
            echo "    <span class=\"label label-info \">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado no reconocido"), "html", null, true);
            echo "</span>    
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Oferta:status.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 55,  98 => 53,  92 => 51,  86 => 49,  80 => 47,  74 => 45,  68 => 43,  62 => 41,  56 => 39,  50 => 37,  44 => 35,  38 => 33,  32 => 31,  26 => 29,  20 => 27,  17 => 26,);
    }
}
