<?php

/* HotelesBackendBundle:User:status.html.twig */
class __TwigTemplate_7a59b9327a322b507f6515f1e25894fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["status"])) { $_status_ = $context["status"]; } else { $_status_ = null; }
        if (($_status_ == 0)) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Habilitado"), "html", null, true);
            echo "
";
        } elseif (($_status_ == 1)) {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cuenta suspendida"), "html", null, true);
            echo "
";
        } elseif (($_status_ == 2)) {
            // line 6
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cuenta baneada"), "html", null, true);
            echo "
";
        } elseif (($_status_ == 3)) {
            // line 8
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta pendiente"), "html", null, true);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:User:status.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 8,  32 => 6,  26 => 4,  20 => 2,  17 => 1,  548 => 201,  543 => 198,  531 => 185,  515 => 182,  507 => 179,  503 => 177,  495 => 172,  489 => 171,  481 => 166,  475 => 165,  472 => 164,  465 => 161,  462 => 160,  459 => 159,  456 => 158,  450 => 155,  442 => 154,  440 => 153,  433 => 152,  427 => 149,  421 => 148,  415 => 145,  409 => 144,  406 => 143,  401 => 141,  394 => 140,  389 => 138,  383 => 137,  381 => 132,  378 => 131,  375 => 130,  369 => 127,  363 => 126,  361 => 125,  354 => 124,  348 => 121,  342 => 120,  336 => 117,  330 => 116,  327 => 115,  322 => 113,  315 => 112,  310 => 110,  304 => 109,  302 => 104,  299 => 103,  296 => 102,  290 => 99,  282 => 98,  280 => 97,  273 => 96,  267 => 93,  261 => 92,  255 => 89,  249 => 88,  246 => 87,  241 => 85,  234 => 84,  229 => 82,  223 => 81,  221 => 76,  217 => 75,  212 => 74,  203 => 72,  200 => 71,  194 => 70,  188 => 68,  182 => 66,  176 => 64,  170 => 62,  166 => 61,  161 => 60,  157 => 58,  150 => 56,  144 => 54,  141 => 53,  135 => 51,  127 => 50,  124 => 49,  106 => 48,  98 => 43,  94 => 42,  90 => 41,  86 => 40,  82 => 39,  78 => 38,  69 => 31,  64 => 23,  62 => 22,  51 => 14,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
