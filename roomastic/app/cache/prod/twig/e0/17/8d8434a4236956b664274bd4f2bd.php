<?php

/* HotelesFrontendBundle:Frontend:ofertarealizada.mv.twig */
class __TwigTemplate_e0178d8434a4236956b664274bd4f2bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "

    <div class=\"contenido oferta_ok content_ofertaenv\">
        <div class=\"separador\"></div>
        <h2>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Confirmación de oferta enviada"), "html", null, true);
        echo "</h2>
        <p>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si tienes alguna duda, puedes ponerte en contacto con nosotros."), "html", null, true);
        echo "</p>
        <p>";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Muchas gracias por utilizar Roomastic."), "html", null, true);
        echo "</p>
        <div class=\"separador m-es\"></div>
        <div class=\"bq_verde\">
            <p class=\"tit\">Tu oferta ha sido enviada correctamente</p>
            <p>Los establecimientos que has seleccionado han recibido correctamente tu oferta.</p>
            <p>Permanece <span>atento al email que nos has facilitado</span>, ya que los hoteles se pondrán en contacto contigo mediante esta vía.</p>
        </div>      

    </div>



";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:ofertarealizada.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 10,  39 => 9,  35 => 8,  29 => 4,  26 => 3,);
    }
}
