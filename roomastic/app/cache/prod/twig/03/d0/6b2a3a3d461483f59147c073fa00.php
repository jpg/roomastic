<?php

/* HotelesFrontendBundle:Frontend:listadoajax.html.twig */
class __TwigTemplate_03d06b2a3a3d461483f59147c073fa00 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
    ";
        // line 2
        if (isset($context["hoteles"])) { $_hoteles_ = $context["hoteles"]; } else { $_hoteles_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_hoteles_);
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 3
            echo "        <div class=\"single_hotel clearfix  stars-";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "calificacion"), "html", null, true);
            echo " ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "filtroServicios"), "html", null, true);
            echo "\" id=\"hotel-summary-";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\">

            <div class=\"clearfix\">

                <img class=\"estrellas_hotel\" src=\"/bundles/hotelesfrontend/img/";
            // line 7
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "calificacion"), "html", null, true);
            echo "stars.png\" alt=\"";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "calificacion"), "html", null, true);
            echo " estrellas\">

                <div class=\"cont_img\">
                    ";
            // line 10
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if ((twig_length_filter($this->env, $this->getAttribute($_objetohotel_, "imagenes")) > 0)) {
                // line 11
                echo "                        <a href=\"\" title=\"\"><img height=\"152\" width=\"215\" src=\"/uploads/hoteles/";
                if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_objetohotel_, "imagenes"), 0, array(), "array"), "imagen"), "html", null, true);
                echo "\" alt=\"\"></a>
                        ";
            }
            // line 13
            echo "                </div>

                <div class=\"hotel_cont clearfix\">
                    <h2 class=\"tit_hotel\">";
            // line 16
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "</h2>

                    <ul class=\"servicios clearfix\">
                        ";
            // line 19
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "piscina") == 1)) {
                // line 20
                echo "                            <li class=\"piscina\">Piscina</li>
                            ";
            }
            // line 22
            echo "                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "spa") == 1)) {
                // line 23
                echo "                            <li class=\"spa\">Spa</li>
                            ";
            }
            // line 25
            echo "                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "wiFi") == 1)) {
                // line 26
                echo "                            <li class=\"wi-fi\">Wi-fi</li>
                            ";
            }
            // line 28
            echo "                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "accesoAdaptado") == 1)) {
                // line 29
                echo "                            <li class=\"acceso-adaptado\">Acceso adaptado</li>
                            ";
            }
            // line 31
            echo "                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "aceptanPerros") == 1)) {
                // line 32
                echo "                            <li class=\"perros\">Aceptan perros</li>
                            ";
            }
            // line 34
            echo "                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "aparcamiento") == 1)) {
                // line 35
                echo "                            <li class=\"parking\">Aparcamiento/Parking</li>
                            ";
            }
            // line 37
            echo "                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "businessCenter") == 1)) {
                // line 38
                echo "                            <li class=\"business-center\">Business center</li>
                            ";
            }
            // line 40
            echo "                    </ul>

                    <div class=\"botones_hotel\">
                        <a class=\"detalle\" href=\"#\" title=\"\">DETALLE</a>
                        <a class=\"abrir_map\" href=\"#\" title=\"\">mapa</a>
                        <input class=\"timbre\" name=\"hotel1\" type=\"checkbox\" data-hotel-id=\"";
            // line 45
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\">
                        <!--<a class=\"timbre\" href=\"#\" title=\"\">toca el timbre</a>-->
                    </div><!-- botones_hotel -->

                </div><!-- hotel_cont -->

            </div><!-- clearfix -->


            <div class=\"desplegable clearfix slideFotosList\">
                <ul class=\"galeria clearfix pikame_listado\" >
                    ";
            // line 56
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if ((twig_length_filter($this->env, $this->getAttribute($_objetohotel_, "imagenes")) > 0)) {
                // line 57
                echo "                        ";
                if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($_objetohotel_, "imagenes"));
                foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
                    // line 58
                    echo "                            <li>
                                <a href=\"/uploads/hoteles/";
                    // line 59
                    if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "imagen"), "html", null, true);
                    echo "\" title=\"\"><img height=\"48\" width=\"70\" src=\"/uploads/hoteles/";
                    if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "imagen"), "html", null, true);
                    echo "\" alt=\"\"></a>
                            </li>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 62
                echo "                    ";
            }
            echo "                                        
                </ul>
                <div class=\"hotel_desc\">
                    <p>";
            // line 65
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "descripcion"), "html", null, true);
            echo "</p>
                    <p>";
            // line 66
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "caracteristicas"), "html", null, true);
            echo "</p>
                    <a class=\"mas_dets rosa\" href=\"";
            // line 67
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_detallehotel", array("nombrehotel" => $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "url"))), "html", null, true);
            echo "\" title=\"\">Ver todos los detalles del hotel</a>
                </div>
            </div><!-- desplegable -->

            <div class=\"desplegable_mapa clearfix\">

                <div class=\"mapa\" style=\"height:150px;\" data-hotel-id=\"";
            // line 73
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\" data-longitud=\"";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "longitud"), "html", null, true);
            echo "\" data-latitud=\"";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "latitud"), "html", null, true);
            echo "\"></div>

            </div><!-- desplegable -->

        </div><!-- single_hotel -->

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 80
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listadoajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 80,  201 => 73,  191 => 67,  186 => 66,  181 => 65,  174 => 62,  161 => 59,  158 => 58,  152 => 57,  149 => 56,  134 => 45,  127 => 40,  123 => 38,  119 => 37,  115 => 35,  111 => 34,  107 => 32,  103 => 31,  99 => 29,  95 => 28,  91 => 26,  87 => 25,  83 => 23,  79 => 22,  75 => 20,  72 => 19,  65 => 16,  60 => 13,  53 => 11,  50 => 10,  40 => 7,  25 => 3,  20 => 2,  17 => 1,);
    }
}
