<?php

/* HotelesFrontendBundle:Frontend:publicaTuHotel.html.twig */
class __TwigTemplate_14b60b6ac67a9070881adae00a221db3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    <div class=\"contenido\">

        <h2>";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Publica tu hotel"), "html", null, true);
        echo "</h2>
        <div class=\"contacto\">
            <div class=\"left\">
                ";
        // line 10
        if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
        if ($this->getAttribute($_content_, "texto", array(), "any", true, true)) {
            // line 11
            echo "                    ";
            if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
            echo $this->getAttribute($_content_, "texto");
            echo "
                ";
        }
        // line 13
        echo "
            </div>
            <div class=\"right\">

                ";
        // line 17
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if ($this->getAttribute($this->getAttribute($_app_, "session"), "hasFlash", array(0 => "notice"), "method")) {
            // line 18
            echo "                    <div class=\"flash-notice\">
                        ";
            // line 19
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_app_, "session"), "flash", array(0 => "notice"), "method"), "html", null, true);
            echo "
                    </div>
                ";
        }
        // line 22
        echo "
                <form action=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" method=\"post\" ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo ">

                    <input class=\"bug\" type=\"text\" id=\"hoteles_backendbundle_contactotype_nombre\" name=\"hoteles_backendbundle_contactotype[nombre]\" required=\"required\" maxlength=\"255\" placeholder=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre"), "html", null, true);
        echo "\"><br><br>
                    <input type=\"text\" id=\"hoteles_backendbundle_contactotype_apellidos\" name=\"hoteles_backendbundle_contactotype[apellidos]\" required=\"required\" maxlength=\"255\" placeholder=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos"), "html", null, true);
        echo "\" class=\"longinput\"><br><br>
                    <input type=\"text\" id=\"hoteles_backendbundle_contactotype_email\" name=\"hoteles_backendbundle_contactotype[email]\" required=\"required\" maxlength=\"255\" placeholder=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "\" class=\"longinput\"><br><br>
                    <textarea id=\"hoteles_backendbundle_contactotype_texto\" name=\"hoteles_backendbundle_contactotype[texto]\" required=\"required\" placeholder=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comentarios"), "html", null, true);
        echo "\"></textarea><br><br>
                    ";
        // line 29
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "_token"));
        echo "

                    ";
        // line 32
        echo "                    <p>
                        <button type=\"submit\" class=\"btn_generic\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar petición"), "html", null, true);
        echo "</button>
                    </p>
                </form>
            </div>\t
            <div class=\"clearfix\"></div>
        </div>

    </div><!-- contenido -->

    ";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:publicaTuHotel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 33,  102 => 32,  96 => 29,  92 => 28,  88 => 27,  84 => 26,  80 => 25,  72 => 23,  69 => 22,  62 => 19,  59 => 18,  56 => 17,  50 => 13,  43 => 11,  40 => 10,  34 => 7,  29 => 4,  26 => 3,);
    }
}
