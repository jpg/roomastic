<?php

/* HotelesBackendBundle:Lugar:index.html.twig */
class __TwigTemplate_ad7baeb619217d53a85944358ea39135 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de lugares"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
<div class=\"row\">
    <div class=\"col-lg-12\">
      <section class=\"panel\">
          <header class=\"panel-heading\">
             ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de lugares"), "html", null, true);
        echo "
          </header>
          <div class=\"panel-body\">
                <!--  buscador + rtdos -->
                <div class=\"adv-table\">
                    <div class=\"row\">
                        <div class=\"col-lg-6\">
                            <div id=\"editable-sample_length\" class=\"dataTables_length\">
                                ";
        // line 22
        $this->env->loadTemplate("HotelesBackendBundle:Extras:select.html.twig")->display($context);
        // line 23
        echo "                            </div>
                        </div>
                        <div class=\"col-lg-6\">
                            ";
        // line 31
        echo "                        </div>
                    </div>
                    <!-- / buscador + rtdos -->
                    <!--  contentTabla -->
                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-advance table-hover\" id=\"hidden-table-info\">

                          <thead>
                              <tr>
                                    <th>";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Provincia"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Municipio"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Zona"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acciones"), "html", null, true);
        echo "</th>
                              </tr>
                          </thead>
                          <tbody>
                            ";
        // line 46
        if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_pagination_);
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 47
            echo "

                                <tr>
                                    <td>";
            // line 50
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "provincia"), "html", null, true);
            echo "</td>
                                    <td>";
            // line 51
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "municipio"), "html", null, true);
            echo "</td>
                                    <td>";
            // line 52
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "zona"), "html", null, true);
            echo "</td>
                                    <td>
                                        ";
            // line 55
            echo "                                        <a href=\"";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_lugares_edit", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-xs\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar"), "html", null, true);
            echo "\"><i class=\"fa fa-pencil\"></i></a>
                                    </td>
                                </tr>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 59
        echo "                          </tbody>
                    </table>
                    <!--  /contentTabla -->
                   
                </div> <!-- /addtable -->
            </div><!-- /panelbody -->

      </section>

      <!-- btns -->
      <section class=\"panel\">
            <div class=\"panel-body\">
                <div class=\"span6\">
                    <a href=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_lugares_new"), "html", null, true);
        echo "\" class=\"btn btn-success\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear una nueva entrada"), "html", null, true);
        echo "</a>
                </div>
                <div class=\"span6\">
                  <div class=\"dataTables_paginate paging_bootstrap pagination withbuttons\">
                    ";
        // line 76
        if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
        echo $this->env->getExtension('knp_pagination')->render($_pagination_, "HotelesBackendBundle:Paginacion:paginacion.html.twig");
        echo "
                  </div>
                </div>                  
            </div>          
      </section>
      <!-- /btns -->

    </div><!-- /col -->

</div><!-- /row -->



";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Lugar:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 76,  153 => 72,  138 => 59,  124 => 55,  118 => 52,  113 => 51,  108 => 50,  103 => 47,  98 => 46,  91 => 42,  87 => 41,  83 => 40,  79 => 39,  69 => 31,  64 => 23,  62 => 22,  51 => 14,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
