<?php

/* HotelesFrontendBundle:Frontend:hotel.html.twig */
class __TwigTemplate_addd984911ff61dca2baae2a4a2e2dd4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 5
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a38f972_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a38f972_0") : $this->env->getExtension('assets')->getAssetUrl("js/a38f972_part_1_hotel_detalle_1.js");
            // line 6
            echo "    <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "a38f972"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a38f972") : $this->env->getExtension('assets')->getAssetUrl("js/a38f972.js");
            echo "    <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"contenido hotel_detalle\">
        <div class=\"bqizq\">
            <div class=\"slideFotos\">
                <div class=\"pikachoose\" style=\"width:400px;\"> 
                    ";
        // line 15
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        $context["imagen_hoteles_path"] = $this->getAttribute($_hotel_, "getImagePath");
        // line 16
        echo "                    <ul id=\"pikame\"> 
                        ";
        // line 17
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($_hotel_, "getOrderedImagenesDelHotel"));
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 18
            echo "                            ";
            if (isset($context["imagen_hoteles_path"])) { $_imagen_hoteles_path_ = $context["imagen_hoteles_path"]; } else { $_imagen_hoteles_path_ = null; }
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            $context["ruta_imagen"] = (($_imagen_hoteles_path_ . "/") . $this->getAttribute($_imagen_, "imagen"));
            // line 19
            echo "                            <li> 
                                <a href=\"";
            // line 20
            if (isset($context["ruta_imagen"])) { $_ruta_imagen_ = $context["ruta_imagen"]; } else { $_ruta_imagen_ = null; }
            echo twig_escape_filter($this->env, $_ruta_imagen_, "html", null, true);
            echo "\"> <img src=\"";
            if (isset($context["ruta_imagen"])) { $_ruta_imagen_ = $context["ruta_imagen"]; } else { $_ruta_imagen_ = null; }
            echo twig_escape_filter($this->env, $_ruta_imagen_, "html", null, true);
            echo "\" style=\"width:400px;\"/> </a> 
                            </li> 
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 22
        echo "                                     
                    </ul>                     
                </div> 
            </div><!-- slide -->

            <div class=\"clearfix\"></div>
            <div class=\"direccion\">
                ";
        // line 29
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "ubicacion"), "html", null, true);
        echo "
            </div><!-- /dirección -->

        </div><!-- /bqizq -->
        <div class=\"bqder\">
            <div class=\"hotel_cont clearfix\">
                <h2 class=\"tit_hotel\">";
        // line 35
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "nombrehotel"), "html", null, true);
        echo "</h2>
                <div class=\"estrellashotel\"><img src=\"/bundles/hotelesfrontend/img/";
        // line 36
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "calificacion"), "html", null, true);
        echo "stars.png\"></div>
                <div class=\"clearfix\"></div>
                <ul class=\"servicios clearfix\">
                    ";
        // line 39
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "piscina") == 1)) {
            // line 40
            echo "                        <li class=\"piscina\"> Piscina
                        </li>
                    ";
        }
        // line 43
        echo "
                    ";
        // line 44
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "spa") == 1)) {
            // line 45
            echo "                        <li class=\"spa\">Spa
                        </li>
                    ";
        }
        // line 48
        echo "
                    ";
        // line 49
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "wiFi") == 1)) {
            // line 50
            echo "                        <li class=\"wi-fi\">Wi-fi
                        </li>        
                    ";
        }
        // line 53
        echo "
                    ";
        // line 54
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "accesoAdaptado") == 1)) {
            // line 55
            echo "                        <li class=\"acceso-adaptado\">Acceso adaptado
                        </li>
                    ";
        }
        // line 58
        echo "
                    ";
        // line 59
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "aceptanPerros") == 1)) {
            // line 60
            echo "                        <li class=\"perros\">Aceptan perros
                        </li>
                    ";
        }
        // line 63
        echo "
                    ";
        // line 64
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "aparcamiento") == 1)) {
            // line 65
            echo "                        <li class=\"parking\">Aparcamiento/Parking
                        </li>
                    ";
        }
        // line 68
        echo "
                    ";
        // line 69
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "businessCenter") == 1)) {
            // line 70
            echo "                        <li class=\"business-center\">Business center
                        </li>
                    ";
        }
        // line 73
        echo "                </ul>
            </div>
            <div class=\"info\">
                ";
        // line 77
        echo "                <p><b>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Descripción"), "html", null, true);
        echo "</b></p>
                <p>";
        // line 78
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "descripcion"), "html", null, true);
        echo "</p>
                <!-- <input class=\"timbre\" name=\"hotel1\" type=\"checkbox\"> -->
                <p><b>";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Características"), "html", null, true);
        echo "</b></p>
                <p>";
        // line 81
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "caracteristicas"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"btn_back\" onclick=\"window.history.back();return false;\"><i class=\"icon_back\"></i>Volver a la búsqueda</a>
            </div>
        </div><!-- /bqder -->
        <div class=\"clearfix\"></div>
        <div class=\"mapa-detalle\">
            <div id=\"mapa_det\" style=\"height:290px;\" data-longitud=\"";
        // line 87
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "longitud"), "html", null, true);
        echo "\" data-latitud=\"";
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "latitud"), "html", null, true);
        echo "\" data-titulo=\"";
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "nombrehotel"), "html", null, true);
        echo "\"></div>
        </div><!-- /mapa -->
    </div>

";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:hotel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  233 => 87,  223 => 81,  219 => 80,  213 => 78,  208 => 77,  203 => 73,  198 => 70,  195 => 69,  192 => 68,  187 => 65,  184 => 64,  181 => 63,  176 => 60,  173 => 59,  170 => 58,  165 => 55,  162 => 54,  159 => 53,  154 => 50,  151 => 49,  148 => 48,  143 => 45,  140 => 44,  137 => 43,  132 => 40,  129 => 39,  122 => 36,  117 => 35,  107 => 29,  98 => 22,  85 => 20,  82 => 19,  77 => 18,  72 => 17,  69 => 16,  66 => 15,  60 => 11,  57 => 10,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
