<?php

/* HotelesBackendBundle:UserHotel:new.html.twig */
class __TwigTemplate_a716ea3dcdfa5f10e0bdb9ceb4b56048 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear nuevo usuario hotel"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">
            <section class=\"panel\"> 
                <header class=\"panel-heading\">
                    ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear nuevo usuario hotel"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <form action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_create"), "html", null, true);
        echo "\" method=\"post\"  id=\"formulariosubida\" ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo ">
                        ";
        // line 19
        echo "
                        <div class=\"form-group ";
        // line 20
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "hotel"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 23
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "hotel"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 26
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "email"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 29
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 32
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrecompleto"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 35
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "nombrecompleto"), array("attr" => array("class" => "form-control")));
        echo "                    
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 38
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cargo"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 41
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "cargo"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        ";
        // line 45
        echo "                        <div class=\"form-group \">
                            <label for=\"exampleInputEmail1\">";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imagen de usuario"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                <input type=\"file\" name=\"archivo\" id=\"inputsubida\">
                            </div>
                            <br>
                            <input type=\"hidden\" name=\"imagensubidanombre\" id=\"imagensubidanombre\" value=\"";
        // line 51
        if (isset($context["imagendevuelta"])) { $_imagendevuelta_ = $context["imagendevuelta"]; } else { $_imagendevuelta_ = null; }
        echo twig_escape_filter($this->env, $_imagendevuelta_, "html", null, true);
        echo "\">
                            ";
        // line 52
        if (isset($context["imagendevuelta"])) { $_imagendevuelta_ = $context["imagendevuelta"]; } else { $_imagendevuelta_ = null; }
        if (($_imagendevuelta_ != "")) {
            // line 53
            echo "                                <img src=\"/uploads/user/";
            if (isset($context["imagendevuelta"])) { $_imagendevuelta_ = $context["imagendevuelta"]; } else { $_imagendevuelta_ = null; }
            echo twig_escape_filter($this->env, $_imagendevuelta_, "html", null, true);
            echo "\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                            ";
        } else {
            // line 55
            echo "                                <img src=\"\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                            ";
        }
        // line 57
        echo "                            <iframe src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user")), "html", null, true);
        echo "\" height=\"0\" width=\"1300\" style=\"border:0px;\" name=\"inter\"></iframe>
                        </div>
                        ";
        // line 59
        if (isset($context["show_admin"])) { $_show_admin_ = $context["show_admin"]; } else { $_show_admin_ = null; }
        if (($_show_admin_ == true)) {
            // line 60
            echo "                            <div class=\"form-group ";
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "administrador"))), "html", null, true);
            echo "\">
                                <label>
                                    ";
            // line 62
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Marca esta casilla si quieres que este usuario administre la ficha del hotel"), "html", null, true);
            echo " 
                                    ";
            // line 63
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "administrador"), array("attr" => array("class" => "")));
            echo "                 
                                </label>
                            </div>
                        ";
        }
        // line 67
        echo "                        ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_form_);
        echo "

                </div>
            </section>
            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-success\">";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    <a href=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->



    ";
        // line 86
        echo "    ";
        // line 87
        echo "    ";
        // line 88
        echo "    ";
        // line 89
        echo "

    <a class=\"\" data-toggle=\"modal\" href=\"#myModal\" id=\"clickmodal\"></a>


    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" style=\"width: 923px;margin: 36px auto;height: 508px!important;\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\">";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar imagen"), "html", null, true);
        echo "</h4>
                </div>

                <div style=\"width:100%; height:auto; padding-top: 17px;\" >
                    <img src=\"/bundles/hotelesbackend/img/logo2.png\" id=\"target\" width=\"200\" alt=\"[Jcrop Example]\" />
                </div>

                <div class=\"clearfix\"></div>

                <div class=\"modal-footer\">
                    <button data-dismiss=\"modal\" id=\"closeimage\" class=\"btn btn-default\" type=\"button\">";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cerrar"), "html", null, true);
        echo "</button>
                    <button class=\"btn btn-success\" id=\"successimage\" data-id=\"\" data-url=\"\" data-img=\"\" type=\"button\">";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar cambios"), "html", null, true);
        echo "</button>
                </div>

            </div>
        </div>
    </div>

";
    }

    // line 120
    public function block_jsextras($context, array $blocks = array())
    {
        // line 121
        echo "
    ";
        // line 122
        $context["urlcrea"] = $this->env->getExtension('routing')->getPath("admin_userhotel_create");
        // line 123
        echo "    ";
        $context["urlsubidaimagen"] = $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user"));
        // line 124
        echo "    ";
        $context["tipo"] = "user";
        // line 125
        echo "    ";
        $context["crop1"] = "100";
        // line 126
        echo "    ";
        $context["crop2"] = "100";
        // line 127
        echo "    ";
        $context["crop3"] = "100";
        // line 128
        echo "    ";
        $context["crop4"] = "100";
        // line 129
        echo "    ";
        $context["crop5"] = "1";
        // line 130
        echo "    ";
        $context["crop6"] = "1";
        // line 131
        echo "
    ";
        // line 132
        $this->env->loadTemplate("HotelesBackendBundle:Crop:crop.html.twig")->display(array_merge($context, array("especifico" => "user")));
        // line 133
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:UserHotel:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  309 => 133,  307 => 132,  304 => 131,  301 => 130,  298 => 129,  295 => 128,  292 => 127,  289 => 126,  286 => 125,  283 => 124,  280 => 123,  278 => 122,  275 => 121,  272 => 120,  260 => 110,  256 => 109,  243 => 99,  231 => 89,  229 => 88,  227 => 87,  225 => 86,  211 => 76,  206 => 74,  194 => 67,  186 => 63,  182 => 62,  175 => 60,  172 => 59,  166 => 57,  162 => 55,  155 => 53,  152 => 52,  147 => 51,  139 => 46,  136 => 45,  129 => 41,  124 => 39,  119 => 38,  112 => 35,  107 => 33,  102 => 32,  95 => 29,  90 => 27,  85 => 26,  78 => 23,  73 => 21,  68 => 20,  65 => 19,  58 => 17,  52 => 14,  45 => 9,  42 => 8,  36 => 5,  31 => 4,  28 => 3,);
    }
}
