<?php

/* HotelesBackendBundle:Configuracion:edit.html.twig */
class __TwigTemplate_6c45c3d5ad4dec18aa5e8e50f39299ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar configuración"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">


            <!--tab nav start-->
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar configuración"), "html", null, true);
        echo "
                </header>
                <header class=\"panel-heading tab-bg-dark-navy-blue \">
                    <ul class=\"nav nav-tabs\">
                        <li class=\"active  ";
        // line 21
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "descuentoentero")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "descuentodecimal")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempohotel")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempousuariosoferta")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempousuarioscontraoferta")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#general\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("General"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"";
        // line 24
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "fbappid")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "fbappsecret")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#rrss\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("RRSS"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"";
        // line 27
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "fbappid")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "fbappsecret")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#api\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Claves de APIs"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"\">
                            <a data-toggle=\"tab\" href=\"#tpv\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tpv"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"\">
                            <a data-toggle=\"tab\" href=\"#entorno\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Entorno"), "html", null, true);
        echo "</a>
                        </li>
                    </ul>
                </header>
                <div class=\"panel-body\">
                    <div class=\"tab-content\">
                        <div id=\"general\" class=\"tab-pane active\">
                            <form action=\"";
        // line 41
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_configuracion_update", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\" method=\"post\" ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_edit_form_);
        echo ">
                                <div class=\"form-group\">
                                    <label for=\"exampleInputEmail1\">";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Porcentaje de comisión (campo enteros) , (campo decimales)."), "html", null, true);
        echo "</label>
                                    <div class=\"clearfix\"></div>
                                    <div class=\"col-sm-2-dcnt ";
        // line 45
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "descuentoentero"))), "html", null, true);
        echo "\">
                                        ";
        // line 46
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "descuentoentero"));
        echo "
                                        ";
        // line 47
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "descuentoentero"), array("attr" => array("class" => "form-control")));
        echo "                     
                                    </div>
                                    <div class=\"comaclear\" style=\"float:left\"> , </div>
                                    <div class=\"col-sm-2-dcnt ";
        // line 50
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "descuentodecimal"))), "html", null, true);
        echo "\">
                                        ";
        // line 51
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "descuentodecimal"));
        echo "
                                        ";
        // line 52
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "descuentodecimal"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div class=\"form-group clearfix  ";
        // line 56
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempohotel"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Horas para que el hotel procese las ofertas"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2-dcnt\">
                                        ";
        // line 59
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempohotel"));
        echo "
                                        ";
        // line 60
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "tiempohotel"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  clearfix ";
        // line 63
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempousuariosoferta"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Horas desde que un hotel acepta la oferta para que los usuarios paguen la oferta"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2-dcnt\">
                                        ";
        // line 66
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempousuariosoferta"));
        echo "
                                        ";
        // line 67
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "tiempousuariosoferta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group clearfix ";
        // line 70
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempousuarioscontraoferta"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Horas para que los usuarios acepten las contraofertas"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2-dcnt\">
                                        ";
        // line 73
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempousuarioscontraoferta"));
        echo "
                                        ";
        // line 74
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "tiempousuarioscontraoferta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group clearfix ";
        // line 77
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiemporecordatoriopago"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Horas desde que un hotel acepta la oferta para que se envíe el primer recordatorio de pago"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2-dcnt\">
                                        ";
        // line 80
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiemporecordatoriopago"));
        echo "
                                        ";
        // line 81
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "tiemporecordatoriopago"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group clearfix ";
        // line 84
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempoalertapago"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Horas desde que un hotel acepta la oferta para que se envíe el segundo recordatorio de pago"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2-dcnt\">
                                        ";
        // line 87
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempoalertapago"));
        echo "
                                        ";
        // line 88
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "tiempoalertapago"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                        </div>
                        <div id=\"rrss\" class=\"tab-pane\">
                            <div class=\"form-group ";
        // line 93
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombre_facebook"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre en facebook"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 96
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombre_facebook"));
        echo "
                                    ";
        // line 97
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "nombre_facebook"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 100
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombre_twitter"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre en twitter"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 103
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombre_twitter"));
        echo "
                                    ";
        // line 104
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "nombre_twitter"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>                                        
                        </div>
                        <div id=\"api\" class=\"tab-pane\">
                            <div class=\"form-group ";
        // line 109
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "fbappid"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Facebook app id"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 112
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "fbappid"));
        echo "
                                    ";
        // line 113
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "fbappid"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 116
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "fbappsecret"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Facebook app secret"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 119
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "fbappsecret"));
        echo "
                                    ";
        // line 120
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "fbappsecret"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>         

                            <div class=\"form-group ";
        // line 124
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "googappid"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 125
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Google app id"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 127
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "googappid"));
        echo "
                                    ";
        // line 128
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "googappid"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 131
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "googappsecret"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Google app secret"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 134
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "googappsecret"));
        echo "
                                    ";
        // line 135
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "googappsecret"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 138
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "googapikey"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 139
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Google api key"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 141
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "googapikey"));
        echo "
                                    ";
        // line 142
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "googapikey"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>

                            <div class=\"form-group ";
        // line 146
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "twitterappid"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Twitter app id"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 149
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "twitterappid"));
        echo "
                                    ";
        // line 150
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "twitterappid"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 153
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "twitterappsecret"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Twitter app secret"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 156
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "twitterappsecret"));
        echo "
                                    ";
        // line 157
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "twitterappsecret"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>                                        
                        </div>
                        <div id=\"tpv\" class=\"tab-pane\">";
        // line 161
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tpv"), "html", null, true);
        echo "</div>
                        <div id=\"entorno\" class=\"tab-pane\">
                            <div class=\"form-group ";
        // line 163
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "entorno"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Entorno (Demo, Producción)"), "html", null, true);
        echo "</label>
                                <div class=\"clearfix\"></div>
                                <div class=\"col-sm-9 nomargleft\">
                                    ";
        // line 167
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "entorno"));
        echo "
                                    <div class=\"col-sm-6\">
                                        ";
        // line 169
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getAttribute($_edit_form_, "entorno"), "demo"), "Demo");
        echo "
                                        ";
        // line 170
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($_edit_form_, "entorno"), "demo"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"col-sm-6\">
                                        ";
        // line 173
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderLabel($this->getAttribute($this->getAttribute($_edit_form_, "entorno"), "prod"), "Prod");
        echo "
                                        ";
        // line 174
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($_edit_form_, "entorno"), "prod"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    ";
        // line 188
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_edit_form_);
        echo "
                    <button type=\"submit\" class=\"btn btn-info\">";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->

                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->

    ";
        // line 213
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Configuracion:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  546 => 213,  534 => 189,  529 => 188,  511 => 174,  506 => 173,  499 => 170,  494 => 169,  488 => 167,  482 => 164,  477 => 163,  472 => 161,  464 => 157,  459 => 156,  454 => 154,  449 => 153,  442 => 150,  437 => 149,  432 => 147,  427 => 146,  419 => 142,  414 => 141,  409 => 139,  404 => 138,  397 => 135,  392 => 134,  387 => 132,  382 => 131,  375 => 128,  370 => 127,  365 => 125,  360 => 124,  352 => 120,  347 => 119,  342 => 117,  337 => 116,  330 => 113,  325 => 112,  320 => 110,  315 => 109,  306 => 104,  301 => 103,  296 => 101,  291 => 100,  284 => 97,  279 => 96,  274 => 94,  269 => 93,  260 => 88,  255 => 87,  250 => 85,  245 => 84,  238 => 81,  233 => 80,  228 => 78,  223 => 77,  216 => 74,  211 => 73,  206 => 71,  201 => 70,  194 => 67,  189 => 66,  184 => 64,  179 => 63,  172 => 60,  167 => 59,  162 => 57,  157 => 56,  149 => 52,  144 => 51,  139 => 50,  132 => 47,  127 => 46,  122 => 45,  117 => 43,  108 => 41,  98 => 34,  92 => 31,  86 => 28,  81 => 27,  76 => 25,  71 => 24,  66 => 22,  61 => 21,  54 => 17,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
