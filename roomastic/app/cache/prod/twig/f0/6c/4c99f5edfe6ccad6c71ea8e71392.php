<?php

/* FOSUserBundle:Resetting:request_content.html.twig */
class __TwigTemplate_f06c4c99f5edfe6ccad6c71ea8e71392 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 18
        echo "
";
    }

    // line 2
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 3
        if (array_key_exists("invalid_username", $context)) {
            // line 4
            echo "        <div class=\"alert alert-danger fade in alert-width\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
            <strong>Error</strong><br> ";
            // line 6
            if (isset($context["invalid_username"])) { $_invalid_username_ = $context["invalid_username"]; } else { $_invalid_username_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Credenciales no válidas", array("%username%" => $_invalid_username_)), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 9
        echo "    <form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_send_email"), "html", null, true);
        echo "\" method=\"POST\" class=\"fos_user_resetting_request form-signin\">
        <h2 class=\"form-signin-heading no-icon\">";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado tu contraseña?"), "html", null, true);
        echo "</h2>
        <div class=\"login-wrap\">
            <p>";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Introduce tu usuario:"), "html", null, true);
        echo "</p>
            <input class=\"form-control\" id=\"username\" name=\"username\" required=\"required\" type=\"text\" />
            <button class=\"btn btn-lg btn-login btn-block\" type=\"submit\">";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar"), "html", null, true);
        echo "</button>
        </div>
    </form>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request_content.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  59 => 14,  54 => 12,  49 => 10,  44 => 9,  37 => 6,  33 => 4,  31 => 3,  28 => 2,  23 => 18,  21 => 2,  18 => 1,  29 => 4,  26 => 3,);
    }
}
