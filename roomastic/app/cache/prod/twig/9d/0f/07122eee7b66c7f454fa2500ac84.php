<?php

/* HotelesFrontendBundle:Frontend:faqs.html.twig */
class __TwigTemplate_9d0f07122eee7b66c7f454fa2500ac84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"contenido\">

\t<h2>";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Preguntas más frecuentes (FAQs)"), "html", null, true);
        echo "</h2>

\t<ol class=\"faqs\" start=\"1\">
\t\t";
        // line 10
        if (isset($context["faqs"])) { $_faqs_ = $context["faqs"]; } else { $_faqs_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_faqs_);
        foreach ($context['_seq'] as $context["_key"] => $context["faq"]) {
            // line 11
            echo "\t\t\t<li>
\t\t\t\t<div class=\"pregunta\"><p>";
            // line 12
            if (isset($context["faq"])) { $_faq_ = $context["faq"]; } else { $_faq_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_faq_, "pregunta"), "html", null, true);
            echo "</p></div>
\t\t\t\t<div class=\"respuesta\">";
            // line 13
            if (isset($context["faq"])) { $_faq_ = $context["faq"]; } else { $_faq_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_faq_, "respuesta"), "html", null, true);
            echo "</div>
\t\t\t\t<div class=\"clearfix\"></div>
\t\t\t</li>

\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['faq'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 18
        echo "\t</ol>

</div><!-- contenido -->

";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:faqs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 18,  53 => 13,  48 => 12,  45 => 11,  40 => 10,  34 => 7,  29 => 4,  26 => 3,);
    }
}
