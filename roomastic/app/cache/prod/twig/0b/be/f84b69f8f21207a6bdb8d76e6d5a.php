<?php

/* HotelesBackendBundle:Configuracion:new.html.twig */
class __TwigTemplate_0bbef84b69f8f21207a6bdb8d76e6d5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nueva configuración"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "


    <div class=\"row\">
        <div class=\"col-lg-6\">


            <!--tab nav start-->
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nueva configuración"), "html", null, true);
        echo "
                </header>
                <header class=\"panel-heading tab-bg-dark-navy-blue \">
                    <ul class=\"nav nav-tabs\">
                        <li class=\"active ";
        // line 23
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "descuentoentero")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "descuentodecimal")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "tiempohotel")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "tiempousuariosoferta")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "tiempousuarioscontraoferta")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#general\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("General"), "html", null, true);
        echo "</a>
                        </li>                        
                        <li class=\"";
        // line 26
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "fbappid")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "fbappsecret")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#rrss\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("RRSS"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"";
        // line 29
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "fbappid")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "fbappsecret")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#api\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Claves de APIs"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"\">
                            <a data-toggle=\"tab\" href=\"#tpv\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tpv"), "html", null, true);
        echo "</a>
                        </li>
                    </ul>
                </header>
                <div class=\"panel-body\">
                    <div class=\"tab-content\">
                        <div id=\"general\" class=\"tab-pane active\">
                            <form action=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_configuracion_create"), "html", null, true);
        echo "\" method=\"post\" ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo ">
                                <div class=\"form-group\">
                                    <label for=\"exampleInputEmail1\">";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Porcentaje de comisión (campo enteros) , (campo decimales)."), "html", null, true);
        echo "</label>
                                    <div class=\"clearfix\"></div>
                                    <div class=\"col-sm-2-dcnt ";
        // line 44
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "descuentoentero"))), "html", null, true);
        echo "\">
                                        ";
        // line 45
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "descuentoentero"));
        echo "
                                        ";
        // line 46
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "descuentoentero"), array("attr" => array("class" => "form-control")));
        echo "                     
                                    </div>
                                    <div class=\"comaclear\" style=\"float:left\"> , </div>
                                    <div class=\"col-sm-2-dcnt ";
        // line 49
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "descuentodecimal"))), "html", null, true);
        echo "\">
                                        ";
        // line 50
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "descuentodecimal"));
        echo "
                                        ";
        // line 51
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "descuentodecimal"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"clearfix\"></div>
                                </div>
                                <div class=\"form-group clearfix  ";
        // line 55
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "tiempohotel"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tiempo hotel para procesar la oferta"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2-dcnt\">
                                        ";
        // line 58
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "tiempohotel"));
        echo "
                                        ";
        // line 59
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "tiempohotel"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group clearfix ";
        // line 62
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "tiempousuariosoferta"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tiempo de los usuarios para aceptar la oferta"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2-dcnt\">
                                        ";
        // line 65
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "tiempousuariosoferta"));
        echo "
                                        ";
        // line 66
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "tiempousuariosoferta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group clearfix ";
        // line 69
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "tiempousuarioscontraoferta"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tiempo usuarios para aceptar la contraoferta"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2-dcnt\">
                                        ";
        // line 72
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "tiempousuarioscontraoferta"));
        echo "
                                        ";
        // line 73
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "tiempousuarioscontraoferta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group clearfix ";
        // line 76
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempoRecordatorioPago"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Horas para que se envíe el primer recordatorio de pago"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2-dcnt\">
                                        ";
        // line 79
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempoRecordatorioPago"));
        echo "
                                        ";
        // line 80
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "tiempoRecordatorioPago"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group clearfix ";
        // line 83
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempoAlertaPago"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Horas para que se envíe el segundo recordatorio de pago"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2-dcnt\">
                                        ";
        // line 86
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "tiempoAlertaPago"));
        echo "
                                        ";
        // line 87
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "tiempoAlertaPago"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                        </div>
                        <div id=\"api\" class=\"tab-pane\">
                            <div class=\"form-group ";
        // line 92
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "fbappid"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 93
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Facebook app id"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 95
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "fbappid"));
        echo "
                                    ";
        // line 96
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "fbappid"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 99
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "fbappsecret"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Facebook app secret"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 102
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "fbappsecret"));
        echo "
                                    ";
        // line 103
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "fbappsecret"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>                                        
                        </div>
                        <div id=\"rrss\" class=\"tab-pane\">
                            <div class=\"form-group ";
        // line 108
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombre_facebook"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre en facebook"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 111
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombre_facebook"));
        echo "
                                    ";
        // line 112
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "nombre_facebook"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 115
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombre_twitter"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre en twitter"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 118
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombre_twitter"));
        echo "
                                    ";
        // line 119
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "nombre_twitter"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>                                        
                        </div>
                        <div id=\"tpv\" class=\"tab-pane\">";
        // line 123
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("tpv"), "html", null, true);
        echo "</div>
                    </div>
                </div>
            </section>

            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    ";
        // line 131
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_form_);
        echo "
                    <button type=\"submit\" class=\"btn btn-success\">";
        // line 132
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    ";
        // line 137
        echo "                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->



";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Configuracion:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  337 => 116,  332 => 115,  240 => 83,  196 => 69,  548 => 201,  465 => 161,  442 => 154,  427 => 149,  415 => 145,  369 => 127,  361 => 125,  354 => 124,  322 => 113,  299 => 103,  280 => 97,  212 => 74,  561 => 254,  536 => 249,  531 => 185,  528 => 247,  453 => 218,  447 => 214,  385 => 185,  326 => 144,  321 => 142,  307 => 136,  292 => 133,  306 => 128,  300 => 126,  288 => 122,  285 => 121,  236 => 75,  506 => 179,  478 => 163,  410 => 120,  380 => 115,  370 => 114,  366 => 131,  362 => 108,  348 => 121,  315 => 109,  310 => 108,  302 => 104,  289 => 94,  284 => 93,  272 => 90,  238 => 82,  1272 => 501,  1270 => 500,  1267 => 499,  1264 => 498,  1261 => 497,  1258 => 496,  1255 => 495,  1252 => 494,  1243 => 491,  1240 => 490,  1197 => 449,  1194 => 448,  1176 => 435,  1170 => 433,  1164 => 432,  1158 => 429,  1139 => 424,  1129 => 420,  1124 => 419,  1115 => 412,  1100 => 405,  1095 => 404,  1088 => 401,  1083 => 400,  1078 => 399,  1073 => 397,  1066 => 393,  1058 => 389,  1053 => 388,  1048 => 386,  1043 => 385,  1026 => 379,  1021 => 378,  1004 => 372,  999 => 371,  983 => 361,  970 => 351,  945 => 339,  940 => 338,  937 => 337,  913 => 322,  904 => 317,  893 => 314,  873 => 303,  860 => 295,  854 => 293,  843 => 286,  838 => 285,  833 => 283,  802 => 271,  794 => 270,  786 => 266,  781 => 265,  774 => 262,  769 => 261,  764 => 259,  756 => 258,  749 => 255,  744 => 254,  732 => 249,  722 => 246,  715 => 243,  710 => 242,  693 => 231,  685 => 227,  675 => 224,  670 => 223,  663 => 220,  648 => 216,  641 => 213,  636 => 212,  631 => 210,  626 => 209,  619 => 206,  599 => 197,  594 => 196,  572 => 188,  565 => 185,  547 => 178,  537 => 175,  530 => 172,  503 => 177,  490 => 158,  467 => 221,  455 => 145,  445 => 142,  431 => 125,  387 => 122,  343 => 103,  318 => 98,  296 => 102,  1374 => 615,  1371 => 614,  1367 => 616,  1365 => 614,  1339 => 590,  1335 => 588,  1321 => 587,  1315 => 585,  1309 => 584,  1302 => 582,  1298 => 581,  1294 => 579,  1290 => 577,  1287 => 576,  1281 => 575,  1263 => 574,  1260 => 573,  1256 => 571,  1253 => 570,  1249 => 493,  1246 => 492,  1230 => 565,  1225 => 564,  1222 => 563,  1213 => 556,  1210 => 555,  1207 => 554,  1160 => 506,  1157 => 505,  1144 => 426,  1137 => 617,  1135 => 423,  1119 => 491,  1117 => 489,  1114 => 488,  1111 => 487,  1109 => 486,  1105 => 406,  1091 => 461,  1080 => 455,  1068 => 394,  1063 => 451,  1051 => 448,  1044 => 446,  1039 => 445,  1032 => 443,  1027 => 442,  1020 => 440,  1015 => 439,  1008 => 437,  1003 => 436,  996 => 434,  991 => 433,  985 => 430,  954 => 426,  952 => 425,  941 => 419,  935 => 418,  927 => 415,  921 => 414,  918 => 413,  912 => 410,  903 => 408,  900 => 407,  898 => 406,  888 => 313,  883 => 400,  876 => 304,  871 => 397,  865 => 296,  852 => 390,  849 => 389,  847 => 388,  836 => 382,  831 => 381,  824 => 278,  819 => 277,  812 => 274,  807 => 273,  800 => 373,  788 => 370,  783 => 369,  759 => 364,  755 => 362,  753 => 361,  739 => 252,  735 => 353,  728 => 351,  713 => 347,  706 => 345,  691 => 341,  684 => 339,  680 => 226,  673 => 336,  669 => 335,  662 => 331,  654 => 329,  651 => 328,  649 => 327,  634 => 321,  622 => 314,  614 => 205,  610 => 310,  601 => 305,  575 => 287,  555 => 182,  543 => 198,  539 => 271,  523 => 264,  514 => 252,  510 => 246,  501 => 236,  496 => 234,  485 => 167,  472 => 164,  469 => 222,  451 => 214,  434 => 210,  429 => 207,  379 => 191,  374 => 189,  363 => 126,  357 => 182,  342 => 118,  317 => 132,  314 => 163,  267 => 93,  261 => 92,  155 => 50,  1182 => 438,  1180 => 444,  1177 => 443,  1174 => 442,  1171 => 441,  1168 => 440,  1165 => 439,  1162 => 438,  1159 => 437,  1156 => 436,  1153 => 428,  1150 => 489,  1147 => 427,  1134 => 423,  1130 => 422,  1121 => 416,  1110 => 409,  1102 => 403,  1092 => 397,  1087 => 396,  1082 => 395,  1075 => 454,  1070 => 391,  1065 => 390,  1060 => 388,  1056 => 449,  1054 => 385,  1046 => 381,  1041 => 380,  1036 => 382,  1031 => 381,  1024 => 374,  1019 => 373,  1014 => 375,  1009 => 374,  1002 => 367,  997 => 366,  992 => 364,  987 => 362,  975 => 354,  971 => 353,  958 => 341,  946 => 333,  933 => 331,  928 => 330,  925 => 329,  916 => 322,  895 => 319,  890 => 318,  878 => 309,  867 => 301,  862 => 300,  856 => 392,  845 => 291,  840 => 290,  835 => 288,  820 => 281,  808 => 277,  803 => 275,  795 => 372,  787 => 270,  782 => 269,  775 => 266,  770 => 265,  765 => 263,  757 => 262,  750 => 360,  745 => 258,  740 => 256,  729 => 253,  724 => 350,  712 => 247,  687 => 232,  682 => 231,  672 => 228,  665 => 225,  660 => 224,  655 => 222,  643 => 218,  638 => 217,  633 => 215,  621 => 211,  611 => 208,  603 => 204,  598 => 203,  586 => 198,  581 => 197,  576 => 195,  569 => 260,  559 => 189,  551 => 185,  546 => 184,  534 => 179,  529 => 268,  512 => 172,  489 => 171,  481 => 166,  471 => 156,  435 => 142,  399 => 118,  372 => 118,  338 => 107,  313 => 97,  308 => 95,  303 => 127,  298 => 95,  286 => 99,  269 => 93,  254 => 130,  247 => 76,  225 => 71,  215 => 66,  131 => 46,  173 => 59,  170 => 62,  200 => 71,  189 => 66,  93 => 33,  875 => 308,  825 => 282,  821 => 498,  817 => 497,  813 => 278,  798 => 485,  793 => 484,  767 => 463,  760 => 462,  754 => 461,  748 => 460,  742 => 459,  734 => 254,  727 => 248,  725 => 446,  707 => 245,  704 => 435,  701 => 434,  698 => 433,  668 => 409,  658 => 219,  647 => 404,  642 => 323,  606 => 376,  589 => 194,  577 => 190,  560 => 184,  524 => 176,  520 => 169,  515 => 182,  511 => 314,  499 => 175,  492 => 171,  462 => 160,  450 => 155,  444 => 212,  432 => 277,  421 => 148,  402 => 263,  398 => 262,  383 => 137,  377 => 181,  353 => 105,  347 => 119,  336 => 117,  328 => 104,  309 => 129,  291 => 100,  244 => 145,  119 => 48,  171 => 62,  116 => 37,  416 => 132,  408 => 195,  404 => 128,  401 => 141,  359 => 164,  346 => 138,  304 => 109,  297 => 134,  271 => 129,  251 => 100,  232 => 74,  195 => 69,  72 => 26,  149 => 49,  75 => 19,  522 => 222,  519 => 221,  507 => 179,  502 => 241,  497 => 238,  493 => 13,  484 => 205,  479 => 227,  475 => 165,  466 => 155,  457 => 218,  452 => 190,  448 => 191,  443 => 188,  440 => 153,  426 => 135,  413 => 201,  409 => 144,  392 => 167,  389 => 138,  373 => 162,  364 => 170,  352 => 139,  339 => 175,  334 => 136,  325 => 112,  320 => 111,  216 => 96,  159 => 95,  40 => 4,  721 => 339,  717 => 348,  714 => 440,  711 => 438,  708 => 334,  705 => 240,  688 => 320,  671 => 305,  653 => 217,  650 => 221,  645 => 300,  630 => 319,  616 => 210,  608 => 309,  593 => 303,  587 => 263,  582 => 191,  574 => 260,  562 => 282,  553 => 339,  542 => 251,  535 => 238,  532 => 269,  517 => 173,  508 => 165,  494 => 165,  488 => 206,  476 => 158,  470 => 291,  461 => 194,  454 => 151,  437 => 210,  433 => 152,  424 => 182,  418 => 200,  412 => 182,  406 => 143,  400 => 190,  394 => 140,  388 => 170,  382 => 121,  375 => 130,  367 => 119,  341 => 240,  333 => 105,  327 => 115,  282 => 98,  255 => 87,  250 => 86,  246 => 87,  213 => 78,  197 => 95,  105 => 33,  777 => 366,  747 => 354,  743 => 351,  702 => 344,  695 => 342,  689 => 313,  683 => 312,  677 => 229,  664 => 300,  661 => 303,  659 => 297,  656 => 296,  628 => 214,  618 => 283,  613 => 282,  609 => 203,  604 => 280,  600 => 279,  591 => 274,  567 => 284,  564 => 283,  545 => 252,  541 => 182,  538 => 267,  525 => 171,  518 => 253,  513 => 166,  509 => 249,  504 => 237,  500 => 14,  495 => 172,  491 => 241,  486 => 230,  482 => 210,  477 => 152,  473 => 233,  468 => 230,  464 => 229,  459 => 159,  456 => 158,  449 => 141,  441 => 211,  438 => 279,  428 => 212,  423 => 204,  420 => 186,  414 => 199,  411 => 133,  405 => 206,  393 => 188,  378 => 131,  344 => 187,  331 => 146,  324 => 180,  319 => 179,  295 => 160,  281 => 87,  276 => 88,  177 => 63,  277 => 159,  273 => 96,  264 => 92,  260 => 132,  257 => 131,  231 => 86,  168 => 61,  396 => 195,  391 => 187,  386 => 192,  381 => 132,  376 => 137,  371 => 132,  365 => 115,  360 => 114,  355 => 123,  350 => 111,  345 => 157,  340 => 137,  335 => 102,  330 => 116,  323 => 105,  316 => 116,  312 => 137,  305 => 219,  301 => 103,  294 => 124,  283 => 104,  279 => 96,  275 => 120,  268 => 111,  265 => 181,  262 => 133,  245 => 84,  234 => 84,  230 => 80,  205 => 133,  190 => 70,  187 => 132,  184 => 65,  169 => 58,  139 => 50,  81 => 34,  293 => 128,  290 => 99,  274 => 95,  270 => 104,  252 => 78,  248 => 128,  241 => 85,  237 => 89,  233 => 80,  226 => 79,  223 => 77,  220 => 98,  210 => 75,  188 => 68,  181 => 90,  145 => 48,  137 => 51,  128 => 40,  120 => 33,  41 => 8,  142 => 63,  129 => 67,  125 => 48,  221 => 76,  203 => 72,  198 => 81,  185 => 69,  179 => 63,  163 => 57,  157 => 56,  152 => 55,  133 => 46,  126 => 55,  110 => 35,  76 => 27,  70 => 27,  222 => 78,  207 => 85,  204 => 95,  183 => 65,  167 => 59,  164 => 52,  148 => 56,  141 => 53,  103 => 35,  98 => 31,  59 => 19,  49 => 12,  53 => 14,  21 => 3,  100 => 31,  97 => 30,  18 => 1,  151 => 74,  135 => 51,  114 => 36,  206 => 72,  201 => 70,  194 => 70,  191 => 85,  176 => 64,  166 => 61,  158 => 71,  153 => 70,  143 => 55,  134 => 49,  123 => 40,  118 => 49,  90 => 41,  87 => 27,  66 => 24,  122 => 45,  107 => 39,  101 => 36,  95 => 35,  82 => 39,  67 => 20,  52 => 15,  45 => 8,  36 => 9,  34 => 7,  266 => 90,  263 => 101,  259 => 80,  256 => 93,  242 => 126,  229 => 82,  227 => 84,  218 => 76,  209 => 133,  192 => 68,  186 => 71,  180 => 76,  174 => 62,  162 => 58,  160 => 53,  146 => 56,  140 => 45,  136 => 47,  106 => 48,  73 => 26,  69 => 31,  22 => 3,  60 => 17,  55 => 18,  102 => 32,  89 => 33,  63 => 23,  56 => 19,  50 => 9,  43 => 10,  92 => 35,  79 => 25,  57 => 9,  37 => 8,  33 => 8,  29 => 4,  19 => 2,  47 => 12,  30 => 4,  27 => 3,  249 => 88,  239 => 90,  235 => 88,  228 => 79,  224 => 77,  219 => 80,  217 => 75,  214 => 67,  211 => 73,  208 => 96,  202 => 64,  199 => 61,  193 => 59,  182 => 66,  178 => 100,  175 => 81,  172 => 85,  165 => 60,  161 => 60,  156 => 52,  154 => 42,  150 => 56,  147 => 56,  132 => 57,  127 => 46,  113 => 41,  86 => 40,  83 => 29,  78 => 27,  64 => 23,  61 => 17,  48 => 12,  32 => 6,  24 => 4,  117 => 44,  112 => 42,  109 => 35,  104 => 40,  96 => 35,  84 => 26,  80 => 29,  68 => 24,  46 => 15,  44 => 9,  26 => 4,  23 => 3,  39 => 6,  25 => 4,  20 => 2,  17 => 1,  144 => 51,  138 => 45,  130 => 52,  124 => 49,  121 => 50,  115 => 31,  111 => 38,  108 => 44,  99 => 38,  94 => 33,  91 => 28,  88 => 30,  85 => 32,  77 => 23,  74 => 32,  71 => 21,  65 => 27,  62 => 17,  58 => 18,  54 => 17,  51 => 15,  42 => 7,  38 => 8,  35 => 5,  31 => 4,  28 => 5,);
    }
}
