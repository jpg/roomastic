<?php

/* HotelesBackendBundle::layout.html.twig */
class __TwigTemplate_0bb7154f67c53fe1170d0194ff3a28bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"Mosaddek\">
        <meta name=\"keyword\" content=\"FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina\">
        <link rel=\"shortcut icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\">

        <title>";
        // line 11
        $this->displayBlock('titulo', $context, $blocks);
        echo "</title>

        <!-- Bootstrap core CSS -->
        <link href=\"/bundles/hotelesbackend/css/bootstrap.min.css\" rel=\"stylesheet\">
        <link href=\"/bundles/hotelesbackend/css/bootstrap-reset.css\" rel=\"stylesheet\">
        <!--external css-->
        <link href=\"/bundles/hotelesbackend/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
        <link href=\"/bundles/hotelesbackend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\"/>
        <link rel=\"stylesheet\" href=\"/bundles/hotelesbackend/css/owl.carousel.css\" type=\"text/css\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/bundles/hotelesbackend/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css\" />
        <!-- Custom styles for this template -->
        <link href=\"/bundles/hotelesbackend/css/style.css\" rel=\"stylesheet\">
        <link href=\"/bundles/hotelesbackend/css/style-responsive.css\" rel=\"stylesheet\" />

        <link href=\"/bundles/hotelesbackend/assets/dropzone/css/dropzone.css\" rel=\"stylesheet\"/>

        <link href=\"/bundles/hotelesbackend/css/tasks.css\" rel=\"stylesheet\">

        <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>

        <link rel=\"stylesheet\" href=\"/bundles/hotelesbackend/css/jquery.Jcrop.css\" type=\"text/css\" />

        <link rel=\"stylesheet\" type=\"text/css\" href=\"/bundles/hotelesbackend/assets/bootstrap-fileupload/bootstrap-fileupload.css\" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src=\"/bundles/hotelesbackend/js/html5shiv.js\"></script>
          <script src=\"/bundles/hotelesbackend/js/respond.min.js\"></script>
        <![endif]-->

        <style type=\"text/css\">
            .withbuttons {
                margin-top: -47px!important;
                margin-bottom: -15px!important;
            }  
            .withoutbuttons {
                margin-top: -15px!important;
                margin-bottom: -15px!important;
            }
            .errortab a{
                color: #a94442!important;
            } 
            .disabled span{
                padding: 0 10px;
                height: 28px;
            }     
        </style>

        <style type=\"text/css\">

            /* Apply these styles only when #preview-pane has
               been placed within the Jcrop widget */
            .jcrop-holder #preview-pane {
                display: block;
                position: absolute;
                z-index: 2000;
                top: 10px;
                right: -280px;
                padding: 6px;
                border: 1px rgba(0,0,0,.4) solid;
                background-color: white;

                -webkit-border-radius: 6px;
                -moz-border-radius: 6px;
                border-radius: 6px;

                -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
                -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
                box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
            }

            /* The Javascript code will set the aspect ratio of the crop
               area based on the size of the thumbnail preview,
               specified here */
            #preview-pane .preview-container {
                width: 250px;
                height: 170px;
                overflow: hidden;
            }

            .jcrop-holder{
                margin: 0 auto!important;
            }

            .jcrop-keymgr{
                display: none!important;
            }

        </style>

    </head>

    <body>

        <section id=\"container\" >
            <!--header start-->
            <header class=\"header white-bg\">
                <div class=\"sidebar-toggle-box\">
                    <div class=\"fa fa-bars tooltips\" data-placement=\"right\" data-original-title=\"Toggle Navigation\"></div>
                </div>
                <!--logo start-->
                <a href=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing"), "html", null, true);
        echo "\" class=\"logo\"><img src=\"/bundles/hotelesbackend/img/logo2.png\"></a>
                <!--logo end-->
                <div class=\"nav notify-row\" id=\"top_menu\">
                    <!--  notification start -->
                    <ul class=\"nav top-menu\">
                        ";
        // line 118
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        $context["incidencias"] = $this->env->getExtension('twig_extension')->incidencias_backend($this->getAttribute($_app_, "user"));
        // line 119
        echo "                        <!-- inbox dropdown start-->
                        <li id=\"header_inbox_bar\" class=\"dropdown\">
                            <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">
                                <i class=\"fa fa-exclamation\"></i>
                                ";
        // line 123
        if (isset($context["incidencias"])) { $_incidencias_ = $context["incidencias"]; } else { $_incidencias_ = null; }
        if ((twig_length_filter($this->env, $_incidencias_) > 0)) {
            // line 124
            echo "                                    <span class=\"badge bg-important\">";
            if (isset($context["incidencias"])) { $_incidencias_ = $context["incidencias"]; } else { $_incidencias_ = null; }
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $_incidencias_), "html", null, true);
            echo "</span>
                                ";
        }
        // line 126
        echo "                            </a>
                            <ul class=\"dropdown-menu extended inbox\">
                                <div class=\"notify-arrow notify-arrow-red\"></div>
                                <li>
                                    <p class=\"red\"> ";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tienes"), "html", null, true);
        echo " ";
        if (isset($context["incidencias"])) { $_incidencias_ = $context["incidencias"]; } else { $_incidencias_ = null; }
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $_incidencias_), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nuevos mensajes internos"), "html", null, true);
        echo "</p>
                                </li>
                                ";
        // line 132
        if (isset($context["incidencias"])) { $_incidencias_ = $context["incidencias"]; } else { $_incidencias_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_incidencias_);
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["incidencia"]) {
            // line 133
            echo "                                    ";
            if (isset($context["incidencia"])) { $_incidencia_ = $context["incidencia"]; } else { $_incidencia_ = null; }
            $this->env->loadTemplate("HotelesBackendBundle:NotificacionesHeader:incidencia.html.twig")->display(array_merge($context, array("incidencia" => $_incidencia_)));
            echo "                            
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['incidencia'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 135
        echo "                                <li>
                                    <a href=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver todos los mensajes"), "html", null, true);
        echo "</a>
                                </li>                       
                            </ul>
                        </li>
                        <!-- inbox dropdown end -->

                        ";
        // line 142
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 143
            echo "
                            ";
            // line 144
            $context["contactos"] = $this->env->getExtension('twig_extension')->contactos_backend();
            // line 145
            echo "
                            <!-- notification dropdown start-->
                            <li id=\"header_notification_bar\" class=\"dropdown\">
                                <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">
                                    <i class=\"fa fa-envelope-o\"></i>
                                    ";
            // line 150
            if (isset($context["contactos"])) { $_contactos_ = $context["contactos"]; } else { $_contactos_ = null; }
            if ((twig_length_filter($this->env, $_contactos_) > 0)) {
                // line 151
                echo "                                        <span class=\"badge bg-warning\">";
                if (isset($context["contactos"])) { $_contactos_ = $context["contactos"]; } else { $_contactos_ = null; }
                echo twig_escape_filter($this->env, twig_length_filter($this->env, $_contactos_), "html", null, true);
                echo "</span>
                                    ";
            }
            // line 153
            echo "                                </a>
                                <ul class=\"dropdown-menu extended inbox\">
                                    <div class=\"notify-arrow notify-arrow-red\"></div>
                                    <li>
                                        <p class=\"red\"> ";
            // line 157
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tienes"), "html", null, true);
            echo " ";
            if (isset($context["contactos"])) { $_contactos_ = $context["contactos"]; } else { $_contactos_ = null; }
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $_contactos_), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nuevos mensajes externos"), "html", null, true);
            echo "</p>
                                    </li>
                                    ";
            // line 159
            if (isset($context["contactos"])) { $_contactos_ = $context["contactos"]; } else { $_contactos_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_contactos_);
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["contacto"]) {
                // line 160
                echo "                                        ";
                if (isset($context["contacto"])) { $_contacto_ = $context["contacto"]; } else { $_contacto_ = null; }
                $this->env->loadTemplate("HotelesBackendBundle:NotificacionesHeader:contacto.html.twig")->display(array_merge($context, array("contacto" => $_contacto_)));
                echo "       
                                                                    
                                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contacto'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 163
            echo "                                    <li>
                                        <a href=\"";
            // line 164
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver todos los mensajes"), "html", null, true);
            echo "</a>
                                    </li>                       
                                </ul>

                            </li>
                            <!-- notification dropdown end -->

                        ";
        }
        // line 172
        echo "
                        ";
        // line 173
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if (($this->env->getExtension('security')->isGranted("ROLE_HOTEL") && ((null === $this->getAttribute($this->getAttribute($this->getAttribute($_app_, "user"), "hotel"), "notificaciones")) == false))) {
            // line 174
            echo "                            <!-- inbox notificaciones start-->
                            ";
            // line 175
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            $context["notificaciones"] = $this->getAttribute($this->getAttribute($this->getAttribute($_app_, "user"), "hotel"), "notificaciones");
            // line 176
            echo "                            <li id=\"header_inbox_bar\" class=\"dropdown\">
                                <a data-toggle=\"dropdown\" class=\"dropdown-toggle medalla_notificaciones\" href=\"#\">
                                    <i class=\"fa fa-bell-o\"></i>
                                    ";
            // line 179
            if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
            if (($this->getAttribute($_notificaciones_, "getNumNotificaciones") > 0)) {
                // line 180
                echo "                                        <span class=\"badge bg-important\">";
                if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_notificaciones_, "getNumNotificaciones"), "html", null, true);
                echo "</span>
                                    ";
            }
            // line 182
            echo "                                </a>
                                <ul class=\"dropdown-menu extended notification\">
                                    <div class=\"notify-arrow notify-arrow-red\"></div>
                                    <li>
                                        <p class=\"red\">";
            // line 186
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tienes"), "html", null, true);
            echo " <span class=\"total_notificaciones\">";
            if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_notificaciones_, "getNumNotificaciones"), "html", null, true);
            echo "</span> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notificaciones nuevas"), "html", null, true);
            echo "</p>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 189
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "no-pagadas")), "html", null, true);
            echo "\">
                                            <span class=\"label label-danger\"><i class=\"fa fa-bolt\"></i></span>
                                            <strong><span class=\"ofertas_no_pagadas\">";
            // line 191
            if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_notificaciones_, "getOfertasNoPagadas"), "html", null, true);
            echo "</span></strong> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas no pagadas"), "html", null, true);
            echo "
                                                ";
            // line 192
            if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
            if (($this->getAttribute($_notificaciones_, "getOfertasNoPagadas") != 0)) {
                // line 193
                echo "                                                <span class=\"small italic\">";
                if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('time_ago_extension')->timeAgoInWordsFilter($this->getAttribute($_notificaciones_, "getFechaUltimaOfertaNoPagada")), "html", null, true);
                echo "</span>
                                            ";
            }
            // line 195
            echo "                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 198
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "pendientes")), "html", null, true);
            echo "\">
                                            <span class=\"label label-warning\"><i class=\"fa fa-bell\"></i></span>
                                            <strong><span class=\"ofertas_pendientes\">";
            // line 200
            if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_notificaciones_, "getOfertasPendientes"), "html", null, true);
            echo "</span></strong> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas pendientes"), "html", null, true);
            echo "
                                                ";
            // line 201
            if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
            if (($this->getAttribute($_notificaciones_, "getOfertasPendientes") != 0)) {
                // line 202
                echo "                                                <span class=\"small italic\">";
                if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('time_ago_extension')->timeAgoInWordsFilter($this->getAttribute($_notificaciones_, "getFechaUltimaOfertaPendiente")), "html", null, true);
                echo "</span>
                                            ";
            }
            // line 204
            echo "
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 208
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "pagadas")), "html", null, true);
            echo "\">
                                            <span class=\"label label-success\"><i class=\"fa fa-plus\"></i></span>
                                            <strong><span class=\"ofertas_pagadas\"> ";
            // line 210
            if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_notificaciones_, "getOfertasPagadas"), "html", null, true);
            echo "</span></strong> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas pagadas"), "html", null, true);
            echo "
                                            ";
            // line 211
            if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
            if (($this->getAttribute($_notificaciones_, "getOfertasPagadas") != 0)) {
                // line 212
                echo "                                                <span class=\"small italic\">";
                if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('time_ago_extension')->timeAgoInWordsFilter($this->getAttribute($_notificaciones_, "getFechaUltimaOfertaPagada")), "html", null, true);
                echo "</span>
                                            ";
            }
            // line 214
            echo "
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 218
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "todos")), "html", null, true);
            echo "\">
                                            <span class=\"label label-info\"><i class=\"fa fa-bullhorn\"></i></span>
                                            <strong><span class=\"contraofertas_perdidas\">";
            // line 220
            if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_notificaciones_, "getContraofertasPerdidas"), "html", null, true);
            echo "</span></strong> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraofertas perdidas"), "html", null, true);
            echo "
                                                ";
            // line 221
            if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
            if (($this->getAttribute($_notificaciones_, "getContraofertasPerdidas") != 0)) {
                // line 222
                echo "                                                <span class=\"small italic\">";
                if (isset($context["notificaciones"])) { $_notificaciones_ = $context["notificaciones"]; } else { $_notificaciones_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('time_ago_extension')->timeAgoInWordsFilter($this->getAttribute($_notificaciones_, "getFechaUltimaContraofertaPerdida")), "html", null, true);
                echo "</span>
                                            ";
            }
            // line 224
            echo "
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 228
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver todas las ofertas"), "html", null, true);
            echo "</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- inbox notificaciones end -->
                        ";
        }
        // line 234
        echo "
                        ";
        // line 235
        if ($this->env->getExtension('security')->isGranted("ROLE_PREVIOUS_ADMIN")) {
            // line 236
            echo "                            <li>
                                <a href=\"";
            // line 237
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing", array("_want_to_be_this_user" => "_exit")), "html", null, true);
            echo "\"><i class=\"fa fa-power-off\"></i></a>
                            </li>
                        ";
        }
        // line 239
        echo " 

                        ";
        // line 252
        echo "
                    </ul>
                    <!--  notification end -->
                </div>
                <div class=\"top-nav \">
                    <!--search & user info start-->
                    <ul class=\"nav pull-right top-menu\">
                        ";
        // line 264
        echo "                        <!-- user login dropdown start-->
                        <li class=\"dropdown\">

                            <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">
                                ";
        // line 268
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if (($this->getAttribute($this->getAttribute($_app_, "user", array(), "any", false, true), "imagen", array(), "any", true, true) && ($this->getAttribute($this->getAttribute($_app_, "user"), "imagen") != ""))) {
            // line 269
            echo "                                    <img width=\"29\" height=\"29\" alt=\"\" src=\"/uploads/user/";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_app_, "user"), "imagen"), "html", null, true);
            echo "\">
                                ";
        } else {
            // line 271
            echo "                                    <img width=\"29\" height=\"29\" alt=\"\" src=\"/bundles/hotelesbackend/img/avatar.jpg\">
                                ";
        }
        // line 273
        echo "                                <span class=\"username\">";
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_app_, "user"), "getPublicUsername"), "html", null, true);
        echo "</span>
                                <b class=\"caret\"></b>
                            </a>

                            <ul class=\"dropdown-menu extended logout\">
                                <div class=\"log-arrow-up\"></div>
                                <li>
                                    <a href=\"";
        // line 280
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_logout"), "html", null, true);
        echo "\"><i class=\"fa fa-key\"></i>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Salir"), "html", null, true);
        echo "</a>
                                </li>
                                ";
        // line 282
        if ($this->env->getExtension('security')->isGranted("ROLE_PREVIOUS_ADMIN")) {
            // line 283
            echo "                                    <li>
                                        <a href=\"";
            // line 284
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing", array("_want_to_be_this_user" => "_exit")), "html", null, true);
            echo "\"><i class=\"fa fa-power-off\"></i>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dejar suplantación"), "html", null, true);
            echo "</a>
                                    </li>
                                ";
        }
        // line 287
        echo "
                            </ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                    <!--search & user info end-->
                </div>
            </header>
            <!--header end-->
            <!--sidebar start-->
            <aside>
                <div id=\"sidebar\"  class=\"nav-collapse \">
                    <!-- sidebar menu start-->
                    <ul class=\"sidebar-menu\" id=\"nav-accordion\">

                        <li>
                            <a class=\"";
        // line 303
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_landing", "single"), "html", null, true);
        echo "\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing"), "html", null, true);
        echo "\">
                                <i class=\"fa fa-dashboard\"></i>
                                <span>";
        // line 305
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dashboard"), "html", null, true);
        echo "</span>
                            </a>
                        </li>

                        ";
        // line 309
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 310
            echo "
                            <li>
                                <a class=\"";
            // line 312
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_usuario", "complex"), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario"), "html", null, true);
            echo "\">
                                    <i class=\"fa fa-user\"></i>
                                    <span>";
            // line 314
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuarios"), "html", null, true);
            echo "</span>
                                </a>
                            </li>

                        ";
        }
        // line 319
        echo "
                        <li>
                            <a class=\"";
        // line 321
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_incidencia", "complex"), "html", null, true);
        echo "\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia"), "html", null, true);
        echo "\">
                                <i class=\"fa fa-exclamation\"></i>
                                <span>";
        // line 323
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Incidencias"), "html", null, true);
        echo "</span>
                            </a>
                        </li>

                        ";
        // line 327
        if (($this->env->getExtension('security')->isGranted("ROLE_ADMIN") || $this->env->getExtension('security')->isGranted("ROLE_HOTEL"))) {
            // line 328
            echo "                            <li class=\"sub-menu\">
                                <a class=\"";
            // line 329
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_ofertas", "complex"), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas"), "html", null, true);
            echo "\">
                                    <i class=\"fa fa-eur\"></i>
                                    <span>";
            // line 331
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas"), "html", null, true);
            echo "</span>

                                </a>
                                <ul class=\"sub\">
                                    <li class=\"";
            // line 335
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "contraofertas")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 336
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "contraofertas")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraofertas"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 338
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "pendientes")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 339
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "pendientes")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pendientes"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 341
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "aceptadas")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 342
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "aceptadas")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptadas"), "html", null, true);
            echo "</a>
                                    </li>     
                                    <li class=\"";
            // line 344
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "rechazadas")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 345
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "rechazadas")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rechazadas"), "html", null, true);
            echo "</a>
                                    </li>     
                                    <li class=\"";
            // line 347
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "pagadas")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 348
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "pagadas")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pagadas"), "html", null, true);
            echo "</a>
                                    </li>  
                                    <li class=\"";
            // line 350
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "no-pagadas")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 351
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "no-pagadas")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No pagadas"), "html", null, true);
            echo "</a>
                                    </li>  
                                    <li class=\"";
            // line 353
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "todos")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 354
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "todos")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Todos"), "html", null, true);
            echo "</a>
                                    </li>  

                                </ul>    
                            </li>
                        ";
        }
        // line 360
        echo "
                        ";
        // line 361
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 362
            echo "
                            <li class=\"sub-menu\">
                                <a href=\"javascript:;\" class=\"";
            // line 364
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_useradministrador", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_lugares", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_contactoweb", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_configuracion", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_newsletter", "complex"), "html", null, true);
            echo "  dcjq-parent\">
                                    <i class=\"fa fa-cogs\"></i>
                                    <span>";
            // line 366
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administración"), "html", null, true);
            echo "</span>
                                </a>
                                <ul class=\"sub\">
                                    <li class=\"";
            // line 369
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_useradministrador", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 370
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_useradministrador"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuarios"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 372
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_lugares", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 373
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_lugares"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lugares"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 375
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_contactoweb", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 376
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contacto"), "html", null, true);
            echo "</a>
                                    </li>     
                                    <li class=\"";
            // line 378
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_configuracion", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 379
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_configuracion"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Configuración"), "html", null, true);
            echo "</a>
                                    </li>     
                                    <li class=\"";
            // line 381
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_newsletter", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 382
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_newsletter"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Newsletter"), "html", null, true);
            echo "</a>
                                    </li>
                                </ul>
                            </li>

                        ";
        }
        // line 388
        echo "                        ";
        // line 389
        echo "                        ";
        if (($this->env->getExtension('security')->isGranted("ROLE_ADMIN") || $this->env->getExtension('security')->isGranted("ROLE_EMPRESA"))) {
            // line 390
            echo "
                            <li class=\"sub-menu\">
                                <a href=\"javascript:;\" class=\"";
            // line 392
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_empresa", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_userempresa", "complex"), "html", null, true);
            echo " dcjq-parent\">
                                    <i class=\"fa fa-sitemap\"></i>
                                    <span>";
            // line 394
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
            echo "</span>
                                </a>
                                <ul class=\"sub\">
                                    <li class=\"";
            // line 397
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_empresa", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 398
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administración"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 400
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_userempresa", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 401
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuarios"), "html", null, true);
            echo "</a>
                                    </li>
                                </ul>
                            </li>                            
                        ";
        }
        // line 406
        echo "                        ";
        // line 407
        echo "                        <li class=\"sub-menu\">
                            <a href=\"javascript:;\" class=\"";
        // line 408
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_hotel", "complex"), "html", null, true);
        echo " ";
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_userhotel", "complex"), "html", null, true);
        echo " dcjq-parent\">
                                <i class=\"fa fa-suitcase\"></i>
                                <span>";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
        echo "</span>
                            </a>
                            <ul class=\"sub\">
                                ";
        // line 413
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if (((($this->env->getExtension('security')->isGranted("ROLE_HOTEL") && $this->getAttribute($this->getAttribute($_app_, "user"), "isAdmin")) || $this->env->getExtension('security')->isGranted("ROLE_ADMIN")) || $this->env->getExtension('security')->isGranted("ROLE_EMPRESA"))) {
            // line 414
            echo "                                    <li class=\"";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_hotel", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 415
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administración"), "html", null, true);
            echo "</a>
                                    </li>
                                ";
        }
        // line 418
        echo "                                <li class=\"";
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_userhotel", "complex"), "html", null, true);
        echo "\" >
                                    <a  href=\"";
        // line 419
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuarios"), "html", null, true);
        echo "</a>
                                </li>
                            </ul>
                        </li>


                        ";
        // line 425
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 426
            echo "
                            <li class=\"sub-menu\">
                                <a href=\"javascript:;\" class=\"";
            // line 428
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_condicioneslegales", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_privacidad", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_cookies", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_faqs", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_quienessomos", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_seo", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_homeconfiguracion", "complex"), "html", null, true);
            echo " ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_publicatuhotel", "complex"), "html", null, true);
            echo " dcjq-parent\">
                                    <i class=\"fa fa-desktop\"></i>
                                    <span>";
            // line 430
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Web"), "html", null, true);
            echo "</span>
                                </a>
                                <ul class=\"sub\">
                                    <li class=\"";
            // line 433
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_homeconfiguracion", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 434
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_homeconfiguracion"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imágenes home"), "html", null, true);
            echo "</a>
                                    </li>                            
                                    <li class=\"";
            // line 436
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_quienessomos", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 437
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_quienessomos"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Quiénes somos"), "html", null, true);
            echo "</a>
                                    </li>                        
                                    <li class=\"";
            // line 439
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_condicioneslegales", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 440
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_condicioneslegales"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Condiciones legales"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 442
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_privacidad", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 443
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_privacidad"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Privacidad"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 445
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_publicatuhotel", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 446
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_publicatuhotel"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Publica tu hotel"), "html", null, true);
            echo "</a>
                                    </li>                            
                                    <li class=\"";
            // line 448
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_cookies", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 449
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_cookies"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cookies"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 451
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_faqs", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 452
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_faqs"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Preguntas frecuentes"), "html", null, true);
            echo "</a>
                                    </li>       
                                    <li class=\"";
            // line 454
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute($_app_, "request"), "get", array(0 => "_route"), "method"), "admin_seo", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 455
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_seo"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("SEO"), "html", null, true);
            echo "</a>
                                    </li>  
                                </ul>
                            </li>

                        ";
        }
        // line 461
        echo "

                    </ul>
                    <!-- sidebar menu end-->
                </div>
            </aside>

            <!--sidebar end-->
            <section id=\"main-content\">
                <section class=\"wrapper\">

                    <div class=\"row\">
                        ";
        // line 484
        echo "                    </div>
                    <!-- avisos ok y ko -->
                    ";
        // line 486
        $this->env->loadTemplate("HotelesBackendBundle:Avisos:success.html.twig")->display(array_merge($context, array("cabecera" => "¡Genial!")));
        // line 487
        echo "                    ";
        $this->env->loadTemplate("HotelesBackendBundle:Avisos:error.html.twig")->display(array_merge($context, array("cabecera" => "¡Vaya!")));
        // line 488
        echo "
                    ";
        // line 489
        $this->displayBlock('body', $context, $blocks);
        // line 491
        echo "                </section>
            </section>

            <!--footer start-->
            <footer class=\"site-footer\">
                <div class=\"text-center\">
                    2014 &copy; Roomastic
                    <a href=\"#\" class=\"go-top\">
                        <i class=\"fa fa-angle-up\"></i>
                    </a>
                </div>
            </footer>
            <!--footer end-->
        </section>
        ";
        // line 505
        $this->displayBlock('javascripts', $context, $blocks);
        // line 617
        echo "
    </body>
</html>
";
    }

    // line 11
    public function block_titulo($context, array $blocks = array())
    {
        echo "Roomastic Backend - ";
    }

    // line 489
    public function block_body($context, array $blocks = array())
    {
        // line 490
        echo "                    ";
    }

    // line 505
    public function block_javascripts($context, array $blocks = array())
    {
        // line 506
        echo "            

            <!-- js placed at the end of the document so the pages load faster -->
            <script src=\"/bundles/hotelesbackend/js/jquery.js\"></script>
            <script src=\"/bundles/hotelesbackend/js/jquery-1.8.3.min.js\"></script>
            <script src=\"/bundles/hotelesbackend/js/bootstrap.js\"></script>
            <script class=\"include\" type=\"text/javascript\" src=\"/bundles/hotelesbackend/js/jquery.dcjqaccordion.2.7.js\"></script>
            <script src=\"/bundles/hotelesbackend/js/jquery.scrollTo.min.js\"></script>
            <script src=\"/bundles/hotelesbackend/js/jquery.nicescroll.js\" type=\"text/javascript\"></script>
            <script src=\"/bundles/hotelesbackend/js/jquery.sparkline.js\" type=\"text/javascript\"></script>
            <script src=\"/bundles/hotelesbackend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js\"></script>
            <script src=\"/bundles/hotelesbackend/js/owl.carousel.js\" ></script>
            <script src=\"/bundles/hotelesbackend/js/jquery.customSelect.min.js\" ></script>
            <script src=\"/bundles/hotelesbackend/js/respond.min.js\" ></script>

            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-datepicker/js/bootstrap-datepicker.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-daterangepicker/moment.min.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-daterangepicker/daterangepicker.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-timepicker/js/bootstrap-timepicker.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/jquery-multi-select/js/jquery.multi-select.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/jquery-multi-select/js/jquery.quicksearch.js\"></script>

            <script src=\"https://code.jquery.com/ui/1.10.3/jquery-ui.js\"></script>

            <!--common script for all pages-->
            <script src=\"/bundles/hotelesbackend/js/common-scripts.js\"></script>

            <!--script for this page-->
            <script src=\"/bundles/hotelesbackend/js/sparkline-chart.js\"></script>

            <script type=\"text/javascript\" src=\"https://maps.google.com/maps/api/js?sensor=true\"></script>
            <script src=\"/bundles/hotelesbackend/js/gmaps.js\"></script>

            <script src=\"/bundles/hotelesbackend/js/tasks.js\" type=\"text/javascript\"></script>

            <script src=\"/bundles/hotelesbackend/js/advanced-form-components.js\"></script>

            <script src=\"/bundles/hotelesbackend/js/bootstrap-switch.js\"></script>

            <script src=\"/bundles/hotelesbackend/assets/bootstrap-fileupload/bootstrap-fileupload.js\"></script>

            ";
        // line 554
        echo "            <script src=\"/bundles/hotelesbackend/js/jquery.Jcrop.js\"></script>
            ";
        // line 555
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if (($this->getAttribute($_app_, "debug") == true)) {
            // line 556
            echo "                <script>
                \$.ajaxSetup ({
                    // Disable caching of AJAX responses
                    cache: false
                });
                </script>
            ";
        }
        // line 563
        echo "            ";
        if ($this->env->getExtension('security')->isGranted("ROLE_HOTEL")) {
            // line 564
            echo "                ";
            if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
                // asset "10d7913_0"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_10d7913_0") : $this->env->getExtension('assets')->getAssetUrl("js/10d7913_notificaciones_1.js");
                // line 565
                echo "                <script type=\"text/javascript\" src=\"";
                if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
                echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
                echo "\"></script>
                ";
            } else {
                // asset "10d7913"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_10d7913") : $this->env->getExtension('assets')->getAssetUrl("js/10d7913.js");
                echo "                <script type=\"text/javascript\" src=\"";
                if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
                echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
                echo "\"></script>
                ";
            }
            unset($context["asset_url"]);
            // line 567
            echo "            ";
        }
        // line 568
        echo "            <script type=\"text/javascript\">
                jQuery('#numSelect').change(function(event) {
                ";
        // line 570
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "query"), "all")) == 0)) {
            // line 571
            echo "                        window.location.href = window.location.pathname + '?show=' + jQuery('#numSelect :selected').val()
                ";
        } else {
            // line 573
            echo "                        var url = '';
                    ";
            // line 574
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "query"), "all"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                // line 575
                echo "                            console.log('";
                if (isset($context["k"])) { $_k_ = $context["k"]; } else { $_k_ = null; }
                echo twig_escape_filter($this->env, $_k_, "html", null, true);
                echo "');
                        ";
                // line 576
                if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
                if (($this->getAttribute($_loop_, "index") == 1)) {
                    // line 577
                    echo "                                url = url + '?'
                        ";
                } else {
                    // line 579
                    echo "                                url = url + '&'
                        ";
                }
                // line 581
                echo "                        ";
                if (isset($context["k"])) { $_k_ = $context["k"]; } else { $_k_ = null; }
                if (($_k_ == "show")) {
                    // line 582
                    echo "                                url = url + '";
                    if (isset($context["k"])) { $_k_ = $context["k"]; } else { $_k_ = null; }
                    echo twig_escape_filter($this->env, $_k_, "html", null, true);
                    echo "' + '=' + jQuery('#numSelect :selected').val()
                        ";
                } else {
                    // line 584
                    echo "                                url = url + '";
                    if (isset($context["k"])) { $_k_ = $context["k"]; } else { $_k_ = null; }
                    echo twig_escape_filter($this->env, $_k_, "html", null, true);
                    echo "' + '='
                                        +";
                    // line 585
                    if (isset($context["v"])) { $_v_ = $context["v"]; } else { $_v_ = null; }
                    echo twig_escape_filter($this->env, $_v_, "html", null, true);
                    echo " ;
                        ";
                }
                // line 587
                echo "                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 588
            echo "                            window.location.href = window.location.pathname + url;
                ";
        }
        // line 590
        echo "                    });
            </script>

            <script>

                //owl carousel
                \$(document).ready(function() {
                    \$(\"#owl-demo\").owlCarousel({
                        navigation: true,
                        slideSpeed: 300,
                        paginationSpeed: 400,
                        singleItem: true,
                        autoPlay: true

                    });
                });

                //custom select box
                \$(function() {
                    \$('select.styled').customSelect();
                });

            </script>

            ";
        // line 614
        $this->displayBlock('jsextras', $context, $blocks);
        // line 616
        echo "        ";
    }

    // line 614
    public function block_jsextras($context, array $blocks = array())
    {
        // line 615
        echo "            ";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1374 => 615,  1371 => 614,  1367 => 616,  1365 => 614,  1339 => 590,  1335 => 588,  1321 => 587,  1315 => 585,  1309 => 584,  1302 => 582,  1298 => 581,  1294 => 579,  1290 => 577,  1287 => 576,  1281 => 575,  1263 => 574,  1260 => 573,  1256 => 571,  1253 => 570,  1249 => 568,  1246 => 567,  1230 => 565,  1225 => 564,  1222 => 563,  1213 => 556,  1210 => 555,  1207 => 554,  1160 => 506,  1157 => 505,  1153 => 490,  1150 => 489,  1144 => 11,  1137 => 617,  1135 => 505,  1119 => 491,  1117 => 489,  1114 => 488,  1111 => 487,  1109 => 486,  1105 => 484,  1091 => 461,  1080 => 455,  1075 => 454,  1068 => 452,  1063 => 451,  1056 => 449,  1051 => 448,  1044 => 446,  1039 => 445,  1032 => 443,  1027 => 442,  1020 => 440,  1015 => 439,  1008 => 437,  1003 => 436,  996 => 434,  991 => 433,  985 => 430,  958 => 428,  954 => 426,  952 => 425,  941 => 419,  935 => 418,  927 => 415,  921 => 414,  918 => 413,  912 => 410,  903 => 408,  900 => 407,  898 => 406,  888 => 401,  883 => 400,  876 => 398,  871 => 397,  865 => 394,  856 => 392,  852 => 390,  849 => 389,  847 => 388,  836 => 382,  831 => 381,  824 => 379,  819 => 378,  812 => 376,  807 => 375,  800 => 373,  795 => 372,  788 => 370,  783 => 369,  777 => 366,  759 => 364,  755 => 362,  753 => 361,  750 => 360,  739 => 354,  735 => 353,  728 => 351,  724 => 350,  717 => 348,  713 => 347,  706 => 345,  702 => 344,  695 => 342,  691 => 341,  684 => 339,  680 => 338,  673 => 336,  669 => 335,  662 => 331,  654 => 329,  651 => 328,  649 => 327,  642 => 323,  634 => 321,  630 => 319,  622 => 314,  614 => 312,  610 => 310,  608 => 309,  601 => 305,  593 => 303,  575 => 287,  567 => 284,  564 => 283,  562 => 282,  555 => 280,  543 => 273,  539 => 271,  532 => 269,  529 => 268,  523 => 264,  514 => 252,  504 => 237,  501 => 236,  499 => 235,  496 => 234,  485 => 228,  472 => 222,  462 => 220,  457 => 218,  451 => 214,  444 => 212,  441 => 211,  434 => 210,  423 => 204,  416 => 202,  413 => 201,  401 => 198,  396 => 195,  389 => 193,  386 => 192,  379 => 191,  374 => 189,  363 => 186,  357 => 182,  350 => 180,  347 => 179,  342 => 176,  339 => 175,  336 => 174,  333 => 173,  330 => 172,  317 => 164,  314 => 163,  295 => 160,  277 => 159,  261 => 153,  254 => 151,  251 => 150,  244 => 145,  242 => 144,  239 => 143,  237 => 142,  226 => 136,  223 => 135,  205 => 133,  187 => 132,  177 => 130,  171 => 126,  164 => 124,  161 => 123,  155 => 119,  152 => 118,  144 => 113,  58 => 30,  54 => 29,  33 => 11,  21 => 1,  569 => 260,  561 => 254,  545 => 252,  542 => 251,  536 => 249,  531 => 248,  528 => 247,  510 => 239,  502 => 241,  497 => 238,  495 => 237,  486 => 230,  479 => 224,  475 => 226,  469 => 221,  467 => 221,  459 => 219,  453 => 218,  447 => 214,  437 => 210,  429 => 208,  418 => 200,  414 => 199,  408 => 195,  406 => 200,  400 => 190,  393 => 188,  391 => 187,  385 => 185,  377 => 181,  364 => 170,  359 => 164,  355 => 163,  345 => 157,  331 => 146,  326 => 144,  321 => 142,  312 => 137,  307 => 136,  302 => 135,  297 => 134,  292 => 133,  275 => 120,  267 => 157,  255 => 109,  246 => 105,  228 => 101,  220 => 98,  216 => 96,  197 => 95,  194 => 94,  184 => 91,  181 => 90,  176 => 89,  170 => 87,  165 => 85,  151 => 74,  135 => 61,  119 => 48,  103 => 35,  85 => 21,  80 => 20,  75 => 19,  70 => 18,  60 => 10,  57 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
