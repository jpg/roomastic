<?php

/* HotelesBackendBundle:Contacto:atendido.html.twig */
class __TwigTemplate_da66952f997e521bf7f21cbef7b59967 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 5
        if (isset($context["atendido"])) { $_atendido_ = $context["atendido"]; } else { $_atendido_ = null; }
        if (($_atendido_ == 1)) {
            // line 6
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si"), "html", null, true);
            echo "
";
        } elseif (($_atendido_ == 0)) {
            // line 8
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No"), "html", null, true);
            echo "    
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Contacto:atendido.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  26 => 8,  20 => 6,  17 => 5,  204 => 67,  181 => 62,  175 => 57,  167 => 56,  162 => 55,  157 => 54,  152 => 53,  147 => 52,  139 => 51,  136 => 50,  118 => 49,  112 => 46,  108 => 45,  104 => 44,  100 => 43,  96 => 42,  92 => 41,  88 => 40,  84 => 39,  71 => 29,  65 => 25,  63 => 24,  52 => 16,  44 => 10,  41 => 9,  35 => 5,  30 => 4,  27 => 3,);
    }
}
