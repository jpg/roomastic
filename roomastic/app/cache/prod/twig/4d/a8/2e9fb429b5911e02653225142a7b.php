<?php

/* HotelesFrontendBundle:Frontend:index.mv.twig */
class __TwigTemplate_4da82e9fb429b5911e02653225142a7b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"content_home\">
        <!-- bocata -->
        <div class=\"bocata\">
            Haz tu oferta, ahorra<br>y disfruta de tu estancia
            <span class=\"triangle\"></span>
        </div>
        <!-- form ppal. -->

        <form class=\"form_portada clearfix\" id=\"form-destino-fecha\">

            <select id=\"select-beast\" class=\"selec_ciudad\" placeholder=\"\" name=\"ciudad\">
                <option value=\"\">Destino</option>
                ";
        // line 15
        if (isset($context["hoteles"])) { $_hoteles_ = $context["hoteles"]; } else { $_hoteles_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_hoteles_);
        foreach ($context['_seq'] as $context["_key"] => $context["hotel"]) {
            // line 16
            echo "                    ";
            if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
            if (($this->getAttribute($_hotel_, "provincia") != "")) {
                // line 17
                echo "                        <option value=\"";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_hotel_, "provincia"), "nombre"), "html", null, true);
                echo "\">";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_hotel_, "provincia"), "nombre"), "html", null, true);
                echo "</option>
                    ";
            }
            // line 19
            echo "                    ";
            if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
            if (($this->getAttribute($_hotel_, "municipio") != "")) {
                // line 20
                echo "                        <option value=\"";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_hotel_, "municipio"), "nombre"), "html", null, true);
                echo "/";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_hotel_, "municipio"), "provincia"), "nombre"), "html", null, true);
                echo "\">";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_hotel_, "municipio"), "nombre"), "html", null, true);
                echo "/";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_hotel_, "municipio"), "provincia"), "nombre"), "html", null, true);
                echo "</option>
                    ";
            }
            // line 22
            echo "                    ";
            if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
            if (($this->getAttribute($_hotel_, "lugar") != "")) {
                // line 23
                echo "                        <option value=\"";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_hotel_, "lugar"), "provincia"), "nombre"), "html", null, true);
                echo "/";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_hotel_, "lugar"), "municipio"), "nombre"), "html", null, true);
                echo "/";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_hotel_, "lugar"), "zona"), "html", null, true);
                echo "\">";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_hotel_, "lugar"), "provincia"), "nombre"), "html", null, true);
                echo "/";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_hotel_, "lugar"), "municipio"), "nombre"), "html", null, true);
                echo "/";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_hotel_, "lugar"), "zona"), "html", null, true);
                echo "</option>
                    ";
            }
            // line 24
            echo "                                                                
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 25
        echo "                            
            </select>
            <div class=\"fecha_picker\">
                <input type=\"text\" id=\"fromDate\" name=\"fromDate\" class=\"input_mov ll-skin-lug\"/>
                <input type=\"text\" id=\"toDate\" name=\"toDate\"/>
            </div>

            <input type=\"submit\" id=\"buscar\" name=\"buscar\" class=\"input_mov\" >

        </form>

        <!-- vídeo youtube -->
        <a href=\"https://www.youtube.com/embed/-03y5GizzKQ\" class=\"video\"><span>Te explicamos cómo funciona<br>Roomastic</span> en este vídeo</a>
    </div>
    <!-- /content_home -->
";
    }

    // line 41
    public function block_javascripts($context, array $blocks = array())
    {
        // line 42
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 43
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a101ceb_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a101ceb_0") : $this->env->getExtension('assets')->getAssetUrl("js/a101ceb_index_1.js");
            // line 44
            echo "        <script src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "a101ceb"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a101ceb") : $this->env->getExtension('assets')->getAssetUrl("js/a101ceb.js");
            echo "        <script src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:index.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 44,  143 => 43,  138 => 42,  135 => 41,  116 => 25,  109 => 24,  87 => 23,  83 => 22,  67 => 20,  63 => 19,  53 => 17,  49 => 16,  44 => 15,  30 => 3,  27 => 2,);
    }
}
