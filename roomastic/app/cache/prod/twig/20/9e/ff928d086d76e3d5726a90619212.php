<?php

/* HotelesBackendBundle:Extras:thumbs.html.twig */
class __TwigTemplate_209eff928d086d76e3d5726a90619212 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["dato"])) { $_dato_ = $context["dato"]; } else { $_dato_ = null; }
        if (($_dato_ != "")) {
            // line 2
            echo "    <img src=\"/uploads/user/";
            if (isset($context["dato"])) { $_dato_ = $context["dato"]; } else { $_dato_ = null; }
            echo twig_escape_filter($this->env, $_dato_, "html", null, true);
            echo "\" width=\"29\" height=\"29\">
";
        } else {
            // line 4
            echo "    <img src=\"/bundles/hotelesbackend/img/avatar.jpg\" width=\"29\" height=\"29\">
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:thumbs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  20 => 2,  60 => 12,  57 => 11,  38 => 9,  33 => 8,  29 => 6,  22 => 3,  19 => 2,  17 => 1,  207 => 85,  198 => 81,  186 => 71,  168 => 66,  161 => 64,  153 => 60,  150 => 59,  147 => 58,  141 => 56,  133 => 55,  130 => 54,  127 => 53,  122 => 50,  104 => 49,  96 => 44,  92 => 43,  88 => 42,  84 => 41,  80 => 40,  70 => 32,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 4,);
    }
}
