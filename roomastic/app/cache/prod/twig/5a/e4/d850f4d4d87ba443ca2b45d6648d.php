<?php

/* HotelesBackendBundle:UserEmpresa:edit.html.twig */
class __TwigTemplate_5ae4d850f4d4d87ba443ca2b45d6648d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar usuarios de empresa"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "

    <div class=\"row\">
        <div class=\"col-lg-6\">
            <section class=\"panel\"> 
                <header class=\"panel-heading\">
                    ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar usuarios de empresa"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <form action=\"";
        // line 18
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa_update", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\" method=\"post\" id=\"formulariosubida\" ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_edit_form_);
        echo ">
                        ";
        // line 20
        echo "
                        <div class=\"form-group ";
        // line 21
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "empresa"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 24
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "empresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 27
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "email"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 30
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 33
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrecompleto"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 36
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "nombrecompleto"), array("attr" => array("class" => "form-control")));
        echo "                    
                            </div>
                        </div>

                        <div class=\"form-group \">
                            <label for=\"exampleInputEmail1\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imagen de usuario"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                <input type=\"file\" name=\"archivo\" id=\"inputsubida\">
                            </div>
                            <br>
                            <input type=\"hidden\" name=\"imagensubidanombre\" id=\"imagensubidanombre\" value=\"";
        // line 46
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "imagen"), "html", null, true);
        echo "\">
                            ";
        // line 47
        if (isset($context["imagendevuelta"])) { $_imagendevuelta_ = $context["imagendevuelta"]; } else { $_imagendevuelta_ = null; }
        if ((array_key_exists("imagendevuelta", $context) && ($_imagendevuelta_ != ""))) {
            // line 48
            echo "                                <img src=\"/uploads/user/";
            if (isset($context["imagendevuelta"])) { $_imagendevuelta_ = $context["imagendevuelta"]; } else { $_imagendevuelta_ = null; }
            echo twig_escape_filter($this->env, $_imagendevuelta_, "html", null, true);
            echo "\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                            ";
        } else {
            // line 50
            echo "                                <img src=\"\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                            ";
        }
        // line 52
        echo "                            <iframe src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user")), "html", null, true);
        echo "\" height=\"0\" width=\"1300\" style=\"border:0px;\" name=\"inter\"></iframe>
                        </div>     
                        ";
        // line 54
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_edit_form_);
        echo "
                </div>
            </section>
            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-info\">";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    <a href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
                    ";
        // line 63
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 64
            echo "                        ";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            if ($this->getAttribute($_entity_, "isSuspended", array(), "method")) {
                // line 65
                echo "                            <form action=\"";
                if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa_reactivate", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
                echo "\" method=\"post\" class=\"btnDelete\">
                                ";
                // line 66
                if (isset($context["reactivate_form"])) { $_reactivate_form_ = $context["reactivate_form"]; } else { $_reactivate_form_ = null; }
                echo $this->env->getExtension('form')->renderWidget($_reactivate_form_);
                echo "
                                <button type=\"submit\" class=\"btn btn-success\" onclick=\"return confirm('¿Está seguro de reactivar este usuario?');\">";
                // line 67
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Reactivar "), "html", null, true);
                echo "</button>
                            </form>
                        ";
            } else {
                // line 70
                echo "                            <form action=\"";
                if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa_delete", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
                echo "\" method=\"post\" class=\"btnDelete\">
                                ";
                // line 71
                if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
                echo $this->env->getExtension('form')->renderWidget($_delete_form_);
                echo "
                                <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('¿Está seguro de borrar?');\">";
                // line 72
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suspender"), "html", null, true);
                echo "</button>
                            </form>
                        ";
            }
            // line 75
            echo "                    ";
        }
        // line 76
        echo "                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->




    ";
        // line 86
        echo "    ";
        // line 87
        echo "    ";
        // line 88
        echo "    ";
        // line 89
        echo "    ";
        // line 90
        echo "

    <a class=\"\" data-toggle=\"modal\" href=\"#myModal\" id=\"clickmodal\"></a>


    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" style=\"width: 923px;margin: 36px auto;height: 508px!important;\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\">";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar imagen"), "html", null, true);
        echo "</h4>
                </div>

                <div style=\"width:100%; height:auto; padding-top: 17px;\" >
                    <img src=\"/bundles/hotelesbackend/img/logo2.png\" id=\"target\" width=\"200\" alt=\"[Jcrop Example]\" />
                </div>

                <div class=\"clearfix\"></div>

                <div class=\"modal-footer\">
                    <button data-dismiss=\"modal\" id=\"closeimage\" class=\"btn btn-default\" type=\"button\">";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cerrar"), "html", null, true);
        echo "</button>
                    <button class=\"btn btn-success\" id=\"successimage\" data-id=\"\" data-url=\"\" data-img=\"\" type=\"button\">";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar cambios"), "html", null, true);
        echo "</button>
                </div>

            </div>
        </div>
    </div>
";
    }

    // line 119
    public function block_jsextras($context, array $blocks = array())
    {
        // line 120
        echo "
    ";
        // line 121
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        $context["urlcrea"] = $this->env->getExtension('routing')->getPath("admin_userempresa_update", array("id" => $this->getAttribute($_entity_, "id")));
        // line 122
        echo "        ";
        $context["urlsubidaimagen"] = $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user"));
        // line 123
        echo "    ";
        $context["tipo"] = "user";
        // line 124
        echo "    ";
        $context["crop1"] = "100";
        // line 125
        echo "    ";
        $context["crop2"] = "100";
        // line 126
        echo "    ";
        $context["crop3"] = "100";
        // line 127
        echo "    ";
        $context["crop4"] = "100";
        // line 128
        echo "    ";
        $context["crop5"] = "1";
        // line 129
        echo "    ";
        $context["crop6"] = "1";
        // line 130
        echo "
    ";
        // line 131
        $this->env->loadTemplate("HotelesBackendBundle:Crop:crop.html.twig")->display(array_merge($context, array("especifico" => "user")));
        // line 132
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:UserEmpresa:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  317 => 132,  315 => 131,  312 => 130,  309 => 129,  306 => 128,  303 => 127,  300 => 126,  297 => 125,  294 => 124,  291 => 123,  288 => 122,  285 => 121,  282 => 120,  279 => 119,  268 => 111,  264 => 110,  251 => 100,  239 => 90,  237 => 89,  235 => 88,  233 => 87,  231 => 86,  220 => 76,  217 => 75,  211 => 72,  206 => 71,  200 => 70,  194 => 67,  189 => 66,  183 => 65,  179 => 64,  177 => 63,  171 => 62,  166 => 60,  156 => 54,  150 => 52,  146 => 50,  139 => 48,  136 => 47,  131 => 46,  123 => 41,  114 => 36,  109 => 34,  104 => 33,  97 => 30,  92 => 28,  87 => 27,  80 => 24,  75 => 22,  70 => 21,  67 => 20,  59 => 18,  53 => 15,  45 => 9,  42 => 8,  36 => 5,  31 => 4,  28 => 3,);
    }
}
