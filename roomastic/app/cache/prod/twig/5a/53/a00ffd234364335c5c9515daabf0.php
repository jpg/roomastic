<?php

/* HotelesFrontendBundle:Frontend:listado.html.twig */
class __TwigTemplate_5a53a00ffd234364335c5c9515daabf0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->
    <head>

        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>Listado - Roomastic</title>

        <meta name=\"description\" content=\"\">
        <!--<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">-->

        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

        <!--<link href=\"http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css\" rel=\"stylesheet\">-->
        <link href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\">

        <!-- CSS plugins -->
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/daterangepicker-bs3.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/selectize.default.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/minimal.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/jquery.fancybox.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/sweet-alert.css\">



        <!-- CSS Roomastic -->
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/roomastic.css\">

        <link rel=\"icon\" type=\"image/x-icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\" />

        <script src=\"/bundles/hotelesfrontend/js/vendor/modernizr/modernizr.js\"></script>

        <script src=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>

        ";
        // line 42
        echo "
        <!--[if lt IE 9]>
            <script src=\"js/vendor/html5shiv/dist/html5shiv.min.js\"></script>
        <![endif]-->

    </head> 
    ";
        // line 48
        $context["rrss"] = $this->env->getExtension('twig_extension')->getRRSS();
        // line 49
        echo "
    <body class=\"interna\">
        <!--[if lt IE 7]>
            <p class=\"browsehappy\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- aviso cookie -->
        <div id=\"cookie-bar\" class=\"fixed\"></div>

        <div class=\"wrapper\">
            <div class=\"lineatop\"></div>
            <div class=\"fnd_gris\"></div>
            <div class=\"container clearfix\">
                <div class=\"col_izq\">
                    <a href=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"\"><h1 class=\"logo\">Roomastic</h1></a>

                    <div class=\"tubusq\">TU BÚSQUEDA</div>
                    <span class=\"rosa\">Cada semana  <strong>más y más hoteles</strong></span>



                    <div class=\"formulario_int\">

                        ";
        // line 72
        $context["lugarconstruido"] = "";
        // line 73
        echo "                        ";
        if (isset($context["lugar"])) { $_lugar_ = $context["lugar"]; } else { $_lugar_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_lugar_);
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["dato"]) {
            // line 74
            echo "                            ";
            if (isset($context["lugarconstruido"])) { $_lugarconstruido_ = $context["lugarconstruido"]; } else { $_lugarconstruido_ = null; }
            if (isset($context["dato"])) { $_dato_ = $context["dato"]; } else { $_dato_ = null; }
            $context["lugarconstruido"] = ($_lugarconstruido_ . $_dato_);
            // line 75
            echo "                            ";
            if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
            if (isset($context["lugar"])) { $_lugar_ = $context["lugar"]; } else { $_lugar_ = null; }
            if (($this->getAttribute($_loop_, "index") < twig_length_filter($this->env, $_lugar_))) {
                // line 76
                echo "                                ";
                if (isset($context["lugarconstruido"])) { $_lugarconstruido_ = $context["lugarconstruido"]; } else { $_lugarconstruido_ = null; }
                $context["lugarconstruido"] = ($_lugarconstruido_ . "/");
                // line 77
                echo "                            ";
            }
            // line 78
            echo "                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dato'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 79
        echo "                        ";
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        $context["basic_url"] = ((($this->getAttribute($_app_, "environment") == "dev")) ? ("/app_dev.php") : (""));
        // line 80
        echo "                        <form action=\"";
        if (isset($context["basic_url"])) { $_basic_url_ = $context["basic_url"]; } else { $_basic_url_ = null; }
        echo twig_escape_filter($this->env, $_basic_url_, "html", null, true);
        echo "/listadoajax/";
        if (isset($context["lugarconstruido"])) { $_lugarconstruido_ = $context["lugarconstruido"]; } else { $_lugarconstruido_ = null; }
        if (isset($context["fechain"])) { $_fechain_ = $context["fechain"]; } else { $_fechain_ = null; }
        if (isset($context["fechaout"])) { $_fechaout_ = $context["fechaout"]; } else { $_fechaout_ = null; }
        echo twig_escape_filter($this->env, (((($_lugarconstruido_ . "/") . $_fechain_) . "/") . $_fechaout_), "html", null, true);
        echo "\" method=\"post\" id=\"formajax\">
                            <div class=\"blq clearfix\">   
                                <h4 class=\"tit_form\">Destino</h4>

                                <select id=\"select-beast\" class=\"selec_ciudad ajaxupdate\" placeholder=\"\" name=\"ciudad\">
                                    ";
        // line 85
        if (isset($context["arraydelugares"])) { $_arraydelugares_ = $context["arraydelugares"]; } else { $_arraydelugares_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_arraydelugares_);
        foreach ($context['_seq'] as $context["_key"] => $context["dato"]) {
            // line 86
            echo "                                        ";
            if (isset($context["lugarconstruido"])) { $_lugarconstruido_ = $context["lugarconstruido"]; } else { $_lugarconstruido_ = null; }
            if (isset($context["dato"])) { $_dato_ = $context["dato"]; } else { $_dato_ = null; }
            $context["seleccionado"] = $this->env->getExtension('twig_extension')->select($_lugarconstruido_, $_dato_);
            // line 87
            echo "                                        <option value=\"";
            if (isset($context["dato"])) { $_dato_ = $context["dato"]; } else { $_dato_ = null; }
            echo twig_escape_filter($this->env, $_dato_, "html", null, true);
            echo "\" ";
            if (isset($context["seleccionado"])) { $_seleccionado_ = $context["seleccionado"]; } else { $_seleccionado_ = null; }
            echo twig_escape_filter($this->env, $_seleccionado_, "html", null, true);
            echo ">";
            if (isset($context["dato"])) { $_dato_ = $context["dato"]; } else { $_dato_ = null; }
            echo twig_escape_filter($this->env, $_dato_, "html", null, true);
            echo "</option>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dato'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 89
        echo "                                </select>

                                <h4 class=\"tit_form\">Fechas</h4>
                                <input placeholder=\"Fechas\" class=\"select_fecha\" type=\"text\" name=\"fecha\" id=\"reservation\" value=\"";
        // line 92
        if (isset($context["fecha"])) { $_fecha_ = $context["fecha"]; } else { $_fecha_ = null; }
        echo twig_escape_filter($this->env, $_fecha_, "html", null, true);
        echo "\"/>
                            </div><!-- blq -->

                            <div class=\"blq\">
                                <h4 class=\"tit_form\">Servicios</h4>

                                <ul class=\"checkboxes servicios\">
                                    <li class=\"piscina\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"piscina\" id=\"piscina\" checked data-filter=\"servicio-piscina\">
                                        <label for=\"piscina\">Piscina</label>
                                    </li>
                                    <li class=\"spa\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"spa\" id=\"Spa\" checked data-filter=\"servicio-spa\">
                                        <label for=\"Spa\">Spa</label>
                                    </li>
                                    <li class=\"wi-fi\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"wiFi\" id=\"Wi-fi\" checked data-filter=\"servicio-wi-fi\">
                                        <label for=\"Bar\">Wi-Fi</label>
                                    </li>
                                    <li class=\"acceso-adaptado\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"accesoAdaptado\" id=\"Acceso-Adaptado\" checked data-filter=\"servicio-acceso-adaptado\">
                                        <label for=\"Acceso-Adaptado\">Acceso adaptado</label>
                                    </li>
                                    <li class=\"perros\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"aceptanPerros\" id=\"perros\" checked data-filter=\"servicio-aceptan-perros\">
                                        <label for=\"perros\">Aceptan perros</label>
                                    </li>
                                    <li class=\"parking\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"aparcamiento\" id=\"parking\" checked data-filter=\"servicio-aparcamiento\">
                                        <label for=\"parking\">Aparcamiento/Parking</label>
                                    </li>
                                    <li class=\"business-center\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios\" name=\"businessCenter\" id=\"business-center\" checked data-filter=\"servicio-business-center\">
                                        <label for=\"business-center\">Business center</label>
                                    </li>
                                </ul>
                            </div><!-- blq -->

                            <div class=\"blq\">
                                <h4 class=\"tit_form\">Estrella</h4>

                                <ul class=\"estrellas\">
                                    <li class=\"star1\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-stars\" name=\"star1\" id=\"star1\" checked data-stars=\"1\" data-filter=\"stars-1\">
                                        <label for=\"star1\">Una estrella</label>
                                    </li>
                                    <li class=\"star2\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-stars\" name=\"star2\" id=\"star2\" checked data-stars=\"2\" data-filter=\"stars-2\">
                                        <label for=\"star2\">Dos estrellas</label>
                                    </li>
                                    <li class=\"star3\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-stars\" name=\"star3\" id=\"star3\" checked data-stars=\"3\" data-filter=\"stars-3\">
                                        <label for=\"star3\">tres estrellas</label>
                                    </li>
                                    <li class=\"star4\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-stars\" name=\"star4\" id=\"star4\" checked data-stars=\"4\" data-filter=\"stars-4\">
                                        <label for=\"star4\">Cuatro estrellas</label>
                                    </li>
                                    <li class=\"star5\">
                                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-stars\" name=\"star5\" id=\"star5\" checked data-stars=\"5\" data-filter=\"stars-5\">
                                        <label for=\"star5\">Cinco estrellas</label>
                                    </li>
                                </ul>
                            </div><!-- blq -->

                            <div class=\"blq\">
                                <h4 class=\"tit_form\">Hoteles seleccionados</h4>

                                <ul class=\"hoteles\" id=\"listado-lateral-hoteles\">
                                    ";
        // line 161
        if (isset($context["hoteles"])) { $_hoteles_ = $context["hoteles"]; } else { $_hoteles_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_hoteles_);
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 162
            echo "                                        <li class=\"hotel_li hotel-selected\"  style=\"display: none;\" id=\"hotel-checkbox-";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\" data-hotel-id=\"";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\">
                                            <input type=\"checkbox\" class=\"checkbox-lateral\" id=\"";
            // line 163
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "\">
                                            <label for=\"Hotel Diamante Suites\">";
            // line 164
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "</label>
                                        </li>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 167
        echo "                                </ul>
                            </div><!-- blq -->

                    </div><!-- formulario_int -->
                    </form>

                </div><!-- col_izq -->
                <div class=\"col_der\">

                    <div class=\"cabecera_int\">
                        <div class=\"menu_int clearfix menu_desp\">
                            <ul class=\"social\">
                                <li><a href=\"";
        // line 179
        if (isset($context["rrss"])) { $_rrss_ = $context["rrss"]; } else { $_rrss_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_rrss_, "twitter"), "html", null, true);
        echo "\" target=\"_blank\" title=\"twitter\">twitter</a></li>
                                <li><a class=\"facebook\" href=\"";
        // line 180
        if (isset($context["rrss"])) { $_rrss_ = $context["rrss"]; } else { $_rrss_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_rrss_, "facebook"), "html", null, true);
        echo "\" target=\"_blank\" title=\"facebook\">facebook</a></li>
                            </ul>
                            <ul class=\"menu\">                      
                                <li><a href=\"";
        // line 183
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"HOME\">HOME</a></li>
                                <li><a href=\"";
        // line 184
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_faqs"), "html", null, true);
        echo "\" title=\"FAQS\">FAQS</a></li>
                                <!--<li><a href=\"#\" title=\"AYUDA\">AYUDA</a></li>-->
                                <li><a href=\"";
        // line 186
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" title=\"CONTACTO\">CONTACTO</a></li>
                                <li><a href=\"";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_quienesSomos"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos?"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos?"), "html", null, true);
        echo "</a></li>
                            </ul>
                        </div><!-- menu_int --> 
                        <a class=\"hacer_oferta\" id=\"hacer-oferta\" title=\"\" style=\"\">hacer oferta</a>
                    </div><!-- cabecera_int -->

                    <div class=\"wrapper_hoteles\">


                        ";
        // line 196
        if (isset($context["hoteles"])) { $_hoteles_ = $context["hoteles"]; } else { $_hoteles_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_hoteles_);
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 197
            echo "                            <div class=\"single_hotel clearfix  stars-";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "calificacion"), "html", null, true);
            echo " ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "filtroServicios"), "html", null, true);
            echo "\" id=\"hotel-summary-";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\">

                                <div class=\"clearfix\">

                                    <img class=\"estrellas_hotel\" src=\"/bundles/hotelesfrontend/img/";
            // line 201
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "calificacion"), "html", null, true);
            echo "stars.png\" alt=\"";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "calificacion"), "html", null, true);
            echo " estrellas\">

                                    <div class=\"cont_img\">
                                        <div class=\"slideFotosList\">
                                            <div class=\"pikachoose\">
                                                <ul class=\"pikame_listado\" id=\"pikame-";
            // line 206
            if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_loop_, "index0"), "html", null, true);
            echo "\">

                                                    ";
            // line 208
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if ((twig_length_filter($this->env, $this->getAttribute($_objetohotel_, "imagenes")) > 0)) {
                // line 209
                echo "                                                        ";
                if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($_objetohotel_, "imagenes"));
                foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
                    // line 210
                    echo "                                                            <li>
                                                                <a  href=\"/uploads/hoteles/";
                    // line 211
                    if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "imagen"), "html", null, true);
                    echo "\" title=\"\" >
                                                                    <img height=\"152\" width=\"215\" src=\"/uploads/hoteles/";
                    // line 212
                    if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "imagen"), "html", null, true);
                    echo "\" alt=\"\"></a>
                                                            </li>
                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 215
                echo "                                                    ";
            }
            // line 216
            echo "                                                </ul>
                                            </div>          
                                        </div> 
                                    </div>

                                    <div class=\"hotel_cont clearfix\">
                                        <h2 class=\"tit_hotel\">";
            // line 222
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "</h2>

                                        <ul class=\"servicios clearfix\">
                                            ";
            // line 225
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "piscina") == 1)) {
                // line 226
                echo "                                                <li class=\"piscina\">Piscina
                                                </li>
                                            ";
            }
            // line 229
            echo "                                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "spa") == 1)) {
                // line 230
                echo "                                                <li class=\"spa\">Spa
                                                </li>
                                            ";
            }
            // line 233
            echo "                                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "wiFi") == 1)) {
                // line 234
                echo "                                                <li class=\"wi-fi\">Wi-fi
                                                </li>
                                            ";
            }
            // line 237
            echo "                                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "accesoAdaptado") == 1)) {
                // line 238
                echo "                                                <li class=\"acceso-adaptado\">Acceso adaptado
                                                </li>
                                            ";
            }
            // line 241
            echo "                                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "aceptanPerros") == 1)) {
                // line 242
                echo "                                                <li class=\"perros\">Aceptan perros
                                                </li>
                                            ";
            }
            // line 245
            echo "                                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "aparcamiento") == 1)) {
                // line 246
                echo "                                                <li class=\"parking\">Aparcamiento/Parking
                                                </li>
                                            ";
            }
            // line 249
            echo "                                            ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "businessCenter") == 1)) {
                // line 250
                echo "                                                <li class=\"business-center\">Business center
                                                </li>
                                            ";
            }
            // line 253
            echo "                                        </ul>

                                        <div class=\"botones_hotel\">
                                            <a class=\"detalle\" href=\"#\" title=\"\">DETALLE</a>
                                            <a class=\"abrir_map\" href=\"#\" title=\"\">mapa</a>
                                            <input class=\"timbre\" name=\"hotel1\" type=\"checkbox\" data-hotel-id=\"";
            // line 258
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\">
                                        </div><!-- botones_hotel -->

                                    </div><!-- hotel_cont -->

                                </div><!-- clearfix -->

                                <div class=\"desplegable clearfix\">
                                    <ul class=\"galeria clearfix\" style=\"height:150px;\">
                                        ";
            // line 267
            if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
            $context["id_pikame"] = $this->getAttribute($_loop_, "index0");
            // line 268
            echo "                                        ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if ((twig_length_filter($this->env, $this->getAttribute($_objetohotel_, "imagenes")) > 0)) {
                // line 269
                echo "                                            ";
                if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($_objetohotel_, "imagenes"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
                    // line 270
                    echo "                                                <li>
                                                    <a href=\"#\" title=\"\" class=\"thumbnail-pikachoose\"   ><img height=\"48\" width=\"70\" src=\"/uploads/hoteles/";
                    // line 271
                    if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "imagen"), "html", null, true);
                    echo "\" alt=\"\" data-pikame-id=\"pikame-";
                    if (isset($context["id_pikame"])) { $_id_pikame_ = $context["id_pikame"]; } else { $_id_pikame_ = null; }
                    echo twig_escape_filter($this->env, $_id_pikame_, "html", null, true);
                    echo "\" data-photo-index=\"";
                    if (isset($context["loop"])) { $_loop_ = $context["loop"]; } else { $_loop_ = null; }
                    echo twig_escape_filter($this->env, $this->getAttribute($_loop_, "index0"), "html", null, true);
                    echo "\"></a>
                                                </li>
                                            ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 274
                echo "                                        ";
            }
            echo "                                        


                                    </ul>
                                    <div class=\"hotel_desc\">
                                        <p><b>";
            // line 279
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Descripción"), "html", null, true);
            echo "</b></p>
                                        <p>";
            // line 280
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "descripcion"), "html", null, true);
            echo "</p>
                                        <p><b>";
            // line 281
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Características"), "html", null, true);
            echo "</b></p>
                                        <p>";
            // line 282
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "caracteristicas"), "html", null, true);
            echo "</p>
                                        <a class=\"mas_dets rosa\" href=\"";
            // line 283
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_detallehotel", array("nombrehotel" => $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "url"))), "html", null, true);
            echo "\" title=\"\">Ver todos los detalles del hotel</a>
                                    </div>
                                </div><!-- desplegable -->

                                <div class=\"desplegable_mapa clearfix\">

                                    <div class=\"mapa\" style=\"height:150px;\" data-hotel-id=\"";
            // line 289
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\" data-longitud=\"";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "longitud"), "html", null, true);
            echo "\" data-latitud=\"";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "latitud"), "html", null, true);
            echo "\"></div>

                                </div><!-- desplegable -->

                            </div><!-- single_hotel -->

                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 296
        echo "                    </div><!-- wrapper_hoteles -->
                    ";
        // line 297
        $this->env->loadTemplate("HotelesBackendBundle:Extras:preload.html.twig")->display($context);
        // line 298
        echo "                    
                    ";
        // line 300
        echo "                    <div id=\"infinite-scroll-ajax\" data-last-page=\"1\"/>
                </div><!-- col_der -->
            </div><!-- container -->
        </div><!-- wrapper -->


        <footer id=\"footer\" class=\"foot-h\">

            <div class=\"footer1 footer-opaco\">
                <div class=\"container\">
                    <ul class=\"menu_foot clearfix\">                                                 
                        <li><a href=\"";
        // line 311
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_privacidad"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES PARTICULARES Y PRIVACIDAD"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 312
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_condicionesLegales"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES DEL SERVICIO"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 313
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_publicaTuHotel"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PUBLICA TU HOTEL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"v_movil\" href=\"";
        // line 314
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "attributes"), "get", array(0 => "_route"), "method"), twig_array_merge($this->env->getExtension('twig_extension')->getCurrentRouteParams(), array("version" => "mobile"))), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("VERSIÓN MÓVIL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"a_hoteles\" href=\"";
        // line 315
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_login"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ACCESO HOTELES"), "html", null, true);
        echo "</a></li>
                    </ul>
                    <div class=\"att_cliente\">
                        ATENCIÓN AL CLIENTE <br /> <span>911 610 156</span>
                    </div>
                </div><!-- container -->
            </div><!-- footer1 -->
            <div class=\"footer2\">
                <div class=\"container\">
                    <span>© Copyright. Todos los derechos reservados. </span>
                </div><!-- container -->
            </div><!-- footer2 -->

        </footer>

        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
        <script>window.jQuery || document.write('<script src=\"/bundles/hotelesfrontend/js/vendor/jquery/dist/jquery.min.js\"><\\/script>')</script>

        <script src=\"/bundles/hotelesfrontend/js/plugins.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.backstretch.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/moment.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/daterangepicker.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/selectize.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/icheck.min.js\"></script>        

        <!-- slide fotos detalle hotel -->
        <script type=\"text/javascript\" src=\"/bundles/hotelesfrontend/js/plugins/jquery.jcarousel.min.js\"></script>
        <script type=\"text/javascript\" src=\"/bundles/hotelesfrontend/js/plugins/jquery.pikachoose.js\"></script>
        <!-- fancybox -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.pack.js\"></script>
        <!-- sweet-alert -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/sweet-alert.js\"></script>
        <!-- cookies -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.cookiebar.js\"></script>
        <script src=\"https://maps.googleapis.com/maps/api/js?v=3.exp\"></script>
        ";
        // line 351
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a2c94a4_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a2c94a4_0") : $this->env->getExtension('assets')->getAssetUrl("js/a2c94a4_listado_1.js");
            // line 354
            echo "        <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
        ";
            // asset "a2c94a4_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a2c94a4_1") : $this->env->getExtension('assets')->getAssetUrl("js/a2c94a4_roomastic_2.js");
            echo "        <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
        ";
            // asset "a2c94a4_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a2c94a4_2") : $this->env->getExtension('assets')->getAssetUrl("js/a2c94a4_ajax-preload_3.js");
            echo "        <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
        ";
        } else {
            // asset "a2c94a4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a2c94a4") : $this->env->getExtension('assets')->getAssetUrl("js/a2c94a4.js");
            echo "        <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
        ";
        }
        unset($context["asset_url"]);
        // line 356
        echo "        <script>
            (function(b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function() {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-53266623-1');
            ga('send', 'pageview');
        </script>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  777 => 356,  747 => 354,  743 => 351,  702 => 315,  695 => 314,  689 => 313,  683 => 312,  677 => 311,  664 => 300,  661 => 298,  659 => 297,  656 => 296,  628 => 289,  618 => 283,  613 => 282,  609 => 281,  604 => 280,  600 => 279,  591 => 274,  567 => 271,  564 => 270,  545 => 269,  541 => 268,  538 => 267,  525 => 258,  518 => 253,  513 => 250,  509 => 249,  504 => 246,  500 => 245,  495 => 242,  491 => 241,  486 => 238,  482 => 237,  477 => 234,  473 => 233,  468 => 230,  464 => 229,  459 => 226,  456 => 225,  449 => 222,  441 => 216,  438 => 215,  428 => 212,  423 => 211,  420 => 210,  414 => 209,  411 => 208,  405 => 206,  393 => 201,  378 => 197,  360 => 196,  344 => 187,  340 => 186,  335 => 184,  331 => 183,  324 => 180,  319 => 179,  305 => 167,  295 => 164,  290 => 163,  281 => 162,  276 => 161,  203 => 92,  198 => 89,  182 => 87,  177 => 86,  172 => 85,  157 => 80,  153 => 79,  139 => 78,  136 => 77,  132 => 76,  127 => 75,  122 => 74,  103 => 73,  101 => 72,  89 => 63,  73 => 49,  71 => 48,  63 => 42,  58 => 37,  54 => 36,  17 => 1,);
    }
}
