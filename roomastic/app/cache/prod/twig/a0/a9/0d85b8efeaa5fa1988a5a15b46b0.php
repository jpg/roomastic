<?php

/* HotelesBackendBundle:Mail:resetUserPassword.html.twig */
class __TwigTemplate_a0a90d85b8efeaa5fa1988a5a15b46b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 10
        echo "
";
        // line 11
        $this->displayBlock('body_text', $context, $blocks);
        // line 14
        echo "
";
        // line 15
        $this->displayBlock('body_html', $context, $blocks);
        // line 61
        echo "






";
    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Solicitud de recuperar tu password"), "html", null, true);
        echo "
    ";
    }

    // line 11
    public function block_body_text($context, array $blocks = array())
    {
        // line 12
        echo "
";
    }

    // line 15
    public function block_body_html($context, array $blocks = array())
    {
        // line 16
        echo "
    <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\">
        <head>
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        </head>
        <body>
            <table width=\"812\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                <table width=\"709\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                    <!-- header -->
                    <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>   
                    <tr><td><img src=\"https://roomastic.com/bundles/hotelesfrontend/img/logo.png\" width=\"146\" height=\"45\" border=\"0\" style=\"display:block;\" /></td></tr>
                    <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>
                    <!-- / header -->
                    <tr>
                        <td style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px;\">
                            <p>Hola ";
        // line 32
        if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_user_, "getNombreCompleto"), "html", null, true);
        echo "</p>
                            <p>Hemos recibido una solicitud para recuperar tú contraseña.</p>
                            <p>Ya sabes que la seguridad es muy importante y por ello, es necesario que hagas clic en el siguiente enlace para indicarnos tu nueva contraseña antes de 10 días.</p>
                            <p><a href=\"";
        // line 35
        if (isset($context["confirmationUrl"])) { $_confirmationUrl_ = $context["confirmationUrl"]; } else { $_confirmationUrl_ = null; }
        echo twig_escape_filter($this->env, $_confirmationUrl_, "html", null, true);
        echo "\">";
        if (isset($context["confirmationUrl"])) { $_confirmationUrl_ = $context["confirmationUrl"]; } else { $_confirmationUrl_ = null; }
        echo twig_escape_filter($this->env, $_confirmationUrl_, "html", null, true);
        echo "</a></p>
                            <p>Si no has cursado la opción de recuperar contraseña, no te preocupes, simplemente debes obviar este correo y seguir accediendo a Roomastic como lo has hecho hasta ahora.</p>
                        </td>   
                    </tr>
                    <!-- footer -->
                    <tr><td height=\"127\">&nbsp;</td></tr>    
                    <tr>
                        <td style=\"border-top:2px solid #e9e9e9; text-align:center; font-family:Arial, Helvetica, sans-serif;\">
                            <p style=\"height:10px;\">&nbsp;</p>
                            <span style=\"color:#e52e81; font-weight:bold; font-size:16px;\">Teléfono de atención al cliente</span><br />
                            <span style=\"color:#e52e81; font-weight:bold; font-size:27px;\">911 610 156</span><br /><br />
                        </td>
                    </tr>    
                    <tr>
                        <td style=\"font-size:9px; color:#6c6c6c; text-transform: uppercase; font-family:Arial, Helvetica, sans-serif; text-align:center;\">
                            este email ha sido enviado de manera automática por <span style=\"text-decoration:none; color:#474747;\">roomastic.com</span> © Copyright. Todos los derechos reservados. 
                        </td>
                    </tr>
                    <!-- /footer -->
                </table><!-- table content -->
            </table><!-- table margen -->
        </body>
    </html>


";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Mail:resetUserPassword.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  89 => 35,  82 => 32,  64 => 16,  61 => 15,  56 => 12,  53 => 11,  46 => 3,  43 => 2,  32 => 61,  30 => 15,  27 => 14,  25 => 11,  22 => 10,  20 => 2,);
    }
}
