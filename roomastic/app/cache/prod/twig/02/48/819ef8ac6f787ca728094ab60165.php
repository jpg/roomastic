<?php

/* HotelesBackendBundle:Hotel:new.html.twig */
class __TwigTemplate_0248819ef8ac6f787ca728094ab60165 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nuevo hotel"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_javascripts($context, array $blocks = array())
    {
        // line 9
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 10
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "834bf80_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_834bf80_0") : $this->env->getExtension('assets')->getAssetUrl("js/834bf80_part_1_hotel_create_update_1.js");
            // line 11
            echo "    <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "834bf80"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_834bf80") : $this->env->getExtension('assets')->getAssetUrl("js/834bf80.js");
            echo "    <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 15
        echo "    <form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_create"), "html", null, true);
        echo "\" method=\"post\" class=\"tasi-form\" id=\"formulariosubida\" ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo ">
        <div class=\"row\">
            <div class=\"col-lg-6\">
                <!--tab nav start-->
                <section class=\"panel\">
                    <header class=\"panel-heading\">
                        ";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear hotel"), "html", null, true);
        echo "
                    </header>
                    <header class=\"panel-heading tab-bg-dark-navy-blue \">
                        <ul class=\"nav nav-tabs\">
                            <li class=\"active ";
        // line 25
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "email")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombreempresa")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cif")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "direccionfacturacion")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefono")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "numerocuenta")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#datoshotel\">";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 28
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrepersonacontacto")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "apellidospersonacontacto")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cargopersonacontacto")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "emailpersonacontacto")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefonopersonacontacto")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#datospersona\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la persona de contacto"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 31
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrehotel")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "url")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "descripcion")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "caracteristicas")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "ubicacion")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "pvpoficialenteros")), 6 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "pvpoficialdecimales")), 7 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cantidadminimaaceptadaenteros")), 8 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cantidadminimaaceptadadecimales")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" class=\"caracteristicashotel\" href=\"#caracteristicashotel\">";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ficha del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 34
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "imagenes")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" class=\"imageneshotel\" href=\"#imageneshotel\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imágenes del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 37
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "seotitulo")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "seodescripcion")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "seokeywords")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#seo\">";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Seo"), "html", null, true);
        echo "</a>
                            </li>
                            ";
        // line 40
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 41
            echo "                                <li class=\"";
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionhotelentero")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionhoteldecimal")))), "html", null, true);
            echo "\">
                                    <a data-toggle=\"tab\" href=\"#comisiones\">";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comisiones"), "html", null, true);
            echo "</a>
                                </li>
                            ";
        }
        // line 45
        echo "                        </ul>
                    </header>



                    <div class=\"panel-body\">
                        <div class=\"tab-content\">
                            <div id=\"datoshotel\" class=\"tab-pane active\">
                                <div class=\"form-group ";
        // line 53
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "email"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 56
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "email"));
        echo "
                                        ";
        // line 57
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label>";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 63
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "empresa"));
        echo "
                                        ";
        // line 64
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "empresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>                
                                <div class=\"form-group  ";
        // line 67
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombreempresa"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de empresa"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 70
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombreempresa"));
        echo "
                                        ";
        // line 71
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "nombreempresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 74
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cif"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cif"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 77
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cif"));
        echo "
                                        ";
        // line 78
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "cif"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 81
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "direccionfacturacion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dirección de facturación"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 84
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "direccionfacturacion"));
        echo "
                                        ";
        // line 85
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "direccionfacturacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 88
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefono"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 91
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefono"));
        echo "
                                        ";
        // line 92
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "telefono"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 95
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "numerocuenta"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de cuenta"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 98
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "numerocuenta"));
        echo "
                                        ";
        // line 99
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "numerocuenta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  

                            </div>
                            <div id=\"datospersona\" class=\"tab-pane\">
                                <div class=\"form-group  ";
        // line 105
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrepersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la persona contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 108
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrepersonacontacto"));
        echo " 
                                        ";
        // line 109
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "nombrepersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 112
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "apellidospersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 115
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "apellidospersonacontacto"));
        echo "
                                        ";
        // line 116
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "apellidospersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 119
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cargopersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 122
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cargopersonacontacto"));
        echo "
                                        ";
        // line 123
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "cargopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 126
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "emailpersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 129
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "emailpersonacontacto"));
        echo "
                                        ";
        // line 130
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "emailpersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group  ";
        // line 133
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefonopersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 136
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefonopersonacontacto"));
        echo "
                                        ";
        // line 137
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "telefonopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group checkbox\">
                                    <label>
                                        ";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Marca esta casilla si quieres que este usuario administre la ficha del hotel"), "html", null, true);
        echo " 
                                        <input type=\"checkbox\" name=\"administrador\" value=\"1\">
                                    </label>
                                </div>
                            </div>
                            <div id=\"caracteristicashotel\" class=\"tab-pane\">
                                <div class=\"form-group ";
        // line 148
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrehotel"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre del hotel"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 151
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrehotel"));
        echo "
                                        ";
        // line 152
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "nombrehotel"), array("attr" => array("class" => "form-control nombre-hotel")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 155
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "url"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Url"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 158
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "url"));
        echo "
                                        ";
        // line 159
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "url"), array("attr" => array("class" => "form-control url-hotel")));
        echo "                        
                                    </div>
                                </div>                  
                                <div class=\"form-group\">
                                    <label>";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Selecciona el número de estrellas"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 165
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "calificacion"));
        echo "
                                        ";
        // line 166
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "calificacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-2\">";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Piscina"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 172
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "piscina"));
        echo "
                                        ";
        // line 173
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "piscina"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Spa"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 178
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "spa"));
        echo "
                                        ";
        // line 179
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "spa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wi-Fi"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 184
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "wi_fi"));
        echo "
                                        ";
        // line 185
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "wi_fi"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-2\">";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso adaptado"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 191
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "acceso_adaptado"));
        echo "
                                        ";
        // line 192
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "acceso_adaptado"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptan perros"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 197
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "aceptan_perros"));
        echo "
                                        ";
        // line 198
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "aceptan_perros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aparcamiento"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 203
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "aparcamiento"));
        echo "
                                        ";
        // line 204
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "aparcamiento"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>                                    
                                </div>    
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-2\">";
        // line 208
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Business-Center"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 210
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "business_center"));
        echo "
                                        ";
        // line 211
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "business_center"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 214
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "descripcion"))), "html", null, true);
        echo "\" style=\"padding-top:20px;\">
                                    <label>";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica una descripción breve de aproximadamente 200 carácteres"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 217
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "descripcion"));
        echo "
                                        ";
        // line 218
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "descripcion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 221
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "caracteristicas"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enumera las características del hotel de forma breve en aproximadamente 200 carácteres"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 224
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "caracteristicas"));
        echo "
                                        ";
        // line 225
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "caracteristicas"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group  ";
        // line 228
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "ubicacion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Escribe el nombre de la calle y número acompañado de la localidad, de la ubicación del hotel"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 231
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "ubicacion"));
        echo "
                                        ";
        // line 232
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "ubicacion"), array("attr" => array("class" => "form-control direccionmapa")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group\">
                                    <label>";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mapa"), "html", null, true);
        echo "</label>
                                    <div>
                                        <div id=\"mapaiframe\" style=\"width:500px!important;height:250px!important;\">
                                        </div>
                                        <div style=\"clear:both;\"></div>
                                    </div>
                                </div>
                                <br>
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-1\">";
        // line 245
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Provincia"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 247
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "provincia"));
        echo "
                                        ";
        // line 248
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "provincia"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-1\">";
        // line 251
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Municipio"), "html", null, true);
        echo "&nbsp</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 253
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "municipio"));
        echo "
                                        ";
        // line 254
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "municipio"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <label class=\"col-sm-1\">";
        // line 256
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Zona"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 258
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "lugar"));
        echo "
                                        ";
        // line 259
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "lugar"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 262
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "pvpoficialenteros"))), "html", null, true);
        echo " ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "pvpoficialdecimales"))), "html", null, true);
        echo " padgbott\">
                                    <label class=\"col-sm-6\">";
        // line 263
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica el pvp oficial en habitación individual de uso doble"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 265
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "pvpoficialenteros"));
        echo "
                                        ";
        // line 266
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "pvpoficialenteros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 269
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "pvpoficialdecimales"));
        echo "
                                        ";
        // line 270
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "pvpoficialdecimales"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>                    
                                </div>  
                                <div class=\"clearfix\"></div>
                                <div class=\"form-group  ";
        // line 274
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cantidadminimaaceptadaenteros"))), "html", null, true);
        echo " ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cantidadminimaaceptadadecimales"))), "html", null, true);
        echo " padgbott\">
                                    <label class=\"col-sm-6\">";
        // line 275
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica la cantidad mínima en euros por la que estás dispuesto a recibir ofertas"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 277
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cantidadminimaaceptadaenteros"));
        echo "
                                        ";
        // line 278
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "cantidadminimaaceptadaenteros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 281
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cantidadminimaaceptadadecimales"));
        echo "
                                        ";
        // line 282
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "cantidadminimaaceptadadecimales"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>                    
                                </div>  
                                <div class=\"clearfix\"></div>

                                <div class=\"form-group\">
                                    <label class=\"col-sm-6\">";
        // line 288
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado de publicación de la ficha del hotel"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 290
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "status"));
        echo "
                                        ";
        // line 291
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "status"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  

                            </div>
                            <div id=\"imageneshotel\" class=\"tab-pane\">     
                                <div style=\"height:0px; display:none;\">
                                    ";
        // line 298
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "imagenes"), array("attr" => array("class" => "")));
        echo "   
                                </div>            
                                <div class=\"form-group ";
        // line 300
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "imagenes"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 301
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "imagenes"));
        echo "</label>
                                </div>  
                                <div id=\"imagenes\" style=\"height:15px;width:100%;\">
                                    <input type=\"file\" name=\"archivo\" id=\"inputsubida\">
                                    ";
        // line 308
        echo "                                </div>
                                <iframe a src=\"";
        // line 309
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "hoteles")), "html", null, true);
        echo "\" height=\"0\" width=\"1300\" style=\"border:0px;\" name=\"inter\"></iframe>
                                <div id=\"imagenessubidas\" style=\"height:auto;width:100%;background-color:white;\"> 
                                    <div class=\"row\">
                                        <div class=\"col-md-12\">
                                            <section class=\"panel tasks-widget\">

                                                <div class=\"panel-body\">
                                                    <div class=\"task-content\">
                                                        <ul id=\"sortable\" class=\"task-list\" >
                                                            ";
        // line 318
        if (isset($context["imagenes"])) { $_imagenes_ = $context["imagenes"]; } else { $_imagenes_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_imagenes_);
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 319
            echo "                                                                <li class=\"list-primary borraelemento_";
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "id"), "html", null, true);
            echo "\" data-id=\"";
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "id"), "html", null, true);
            echo "\"><i class=\" fa fa-ellipsis-v\"></i><div class=\"task-title\"><img src=\"/uploads/hoteles/";
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "imagen"), "html", null, true);
            echo "\" width=\"200\"><div class=\"pull-right hidden-phone\"><a href=\"#\"><button class=\"btn btn-success btn-xs fa fa-pencil\"></button></a>&nbsp;";
            echo "&nbsp;<a href=\"#\" onClick=\"deleteElement('";
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "id"), "html", null, true);
            echo "');
                                                                        return false;\"><button class=\"btn btn-danger btn-xs fa fa-trash-o\"></button></a></div></div></li>
                                                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 322
        echo "                                                        </ul>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>                    
                                </div>
                                <div id=\"imagenessubidasinputs\" style=\"display:none;height:0px;";
        // line 329
        echo "width:100%;background-color:white;\"> 
                                    ";
        // line 330
        if (isset($context["imagenes"])) { $_imagenes_ = $context["imagenes"]; } else { $_imagenes_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_imagenes_);
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 331
            echo "                                        <input type=\"text\" name=\"imageneshotel[]\" class=\"borraelemento_";
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "id"), "html", null, true);
            echo "\" value=\"";
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "id"), "html", null, true);
            echo "\"><br>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 333
        echo "                                </div>

                                <a class=\"\" data-toggle=\"modal\" href=\"#myModal\" id=\"clickmodal\"></a>


                                <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" style=\"width: 923px;margin: 36px auto;height: 508px!important;\">
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h4 class=\"modal-title\">";
        // line 343
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar imagen"), "html", null, true);
        echo "</h4>
                                            </div>

                                            <div style=\"width:100%; height:auto; padding-top: 17px;\" >
                                                <img src=\"/bundles/hotelesbackend/img/logo2.png\" id=\"target\" width=\"200\" alt=\"[Jcrop Example]\" />
                                            </div>

                                            <div class=\"clearfix\"></div>

                                            <div class=\"modal-footer\">
                                                <button data-dismiss=\"modal\" id=\"closeimage\" class=\"btn btn-default\" type=\"button\">";
        // line 353
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cerrar"), "html", null, true);
        echo "</button>
                                                <button class=\"btn btn-success\" id=\"successimage\" data-id=\"\" data-url=\"\" data-img=\"\" type=\"button\">";
        // line 354
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar cambios"), "html", null, true);
        echo "</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>          
                            <div id=\"seo\" class=\"tab-pane\">
                                <div class=\"form-group ";
        // line 363
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "seotitulo"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 364
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Título para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 366
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "seotitulo"));
        echo "
                                        ";
        // line 367
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "seotitulo"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 370
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "seodescripcion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 371
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Descripción para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 373
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "seodescripcion"));
        echo "
                                        ";
        // line 374
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "seodescripcion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 377
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "seokeywords"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 378
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Keywords para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 380
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "seokeywords"));
        echo "
                                        ";
        // line 381
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "seokeywords"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                            </div>
                            ";
        // line 385
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 386
            echo "                                <div id=\"comisiones\" class=\"tab-pane\">
                                    <div class=\"form-group\">
                                        <label for=\"exampleInputEmail1\">";
            // line 388
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Porcentaje de comisión (campo enteros) , (campo decimales)."), "html", null, true);
            echo "</label>
                                        <div class=\"clearfix\"></div>
                                        <div class=\"col-sm-2-dcnt ";
            // line 390
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionhotelentero"))), "html", null, true);
            echo "\">
                                            ";
            // line 391
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionhotelentero"));
            echo "
                                            ";
            // line 392
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "comisionhotelentero"), array("attr" => array("class" => "form-control")));
            echo "                     
                                        </div>
                                        <div class=\"comaclear\" style=\"float:left\"> , </div>
                                        <div class=\"col-sm-2-dcnt ";
            // line 395
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionhoteldecimal"))), "html", null, true);
            echo "\">  
                                            ";
            // line 396
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionhoteldecimal"));
            echo "
                                            ";
            // line 397
            if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "comisionhoteldecimal"), array("attr" => array("class" => "form-control")));
            echo "                                  
                                        </div>
                                        <div class=\"clearfix\"></div>
                                    </div>
                                </div>
                            ";
        }
        // line 403
        echo "
                        </div>
                    </div>
                </section>
                <!--tab nav start-->

                ";
        // line 409
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_form_);
        echo "

                <section class=\"panel\">

                    <div class=\"panel-body\">


                        <button type=\"submit\" class=\"btn btn-success\">";
        // line 416
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear"), "html", null, true);
        echo "</button>

                        </form>



                        <a href=\"";
        // line 422
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel"), "html", null, true);
        echo "\" class=\"btn btn-primary\">
                            ";
        // line 423
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "
                        </a>
                    </div>
                </section>

            </div><!-- /col -->
        </div><!-- /row -->

    ";
    }

    // line 433
    public function block_jsextras($context, array $blocks = array())
    {
        // line 434
        echo "        ";
        $context["urlcrea"] = $this->env->getExtension('routing')->getPath("admin_hotel_create");
        // line 435
        echo "        ";
        $context["urlsubidaimagen"] = $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "hoteles"));
        // line 436
        echo "        ";
        $context["tipo"] = "hoteles";
        // line 437
        echo "        ";
        $context["crop1"] = "100";
        // line 438
        echo "        ";
        $context["crop2"] = "100";
        // line 439
        echo "        ";
        $context["crop3"] = "50";
        // line 440
        echo "        ";
        $context["crop4"] = "50";
        // line 441
        echo "        ";
        $context["crop5"] = "16";
        // line 442
        echo "        ";
        $context["crop6"] = "9";
        // line 443
        echo "
        ";
        // line 444
        $this->env->loadTemplate("HotelesBackendBundle:Crop:crop.html.twig")->display(array_merge($context, array("especifico" => "hoteles")));
        // line 445
        echo "

        <script src=\"/bundles/hotelesbackend/assets/dropzone/dropzone.js\"></script>
        <script src=\"/bundles/hotelesbackend/js/respond.min.js\" ></script>

    ";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Hotel:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1182 => 445,  1180 => 444,  1177 => 443,  1174 => 442,  1171 => 441,  1168 => 440,  1165 => 439,  1162 => 438,  1159 => 437,  1156 => 436,  1153 => 435,  1150 => 434,  1147 => 433,  1134 => 423,  1130 => 422,  1121 => 416,  1110 => 409,  1102 => 403,  1092 => 397,  1087 => 396,  1082 => 395,  1075 => 392,  1070 => 391,  1065 => 390,  1060 => 388,  1056 => 386,  1054 => 385,  1046 => 381,  1041 => 380,  1036 => 378,  1031 => 377,  1024 => 374,  1019 => 373,  1014 => 371,  1009 => 370,  1002 => 367,  997 => 366,  992 => 364,  987 => 363,  975 => 354,  971 => 353,  958 => 343,  946 => 333,  933 => 331,  928 => 330,  925 => 329,  916 => 322,  895 => 319,  890 => 318,  878 => 309,  875 => 308,  867 => 301,  862 => 300,  856 => 298,  845 => 291,  840 => 290,  835 => 288,  825 => 282,  820 => 281,  813 => 278,  808 => 277,  803 => 275,  795 => 274,  787 => 270,  782 => 269,  775 => 266,  770 => 265,  765 => 263,  757 => 262,  750 => 259,  745 => 258,  740 => 256,  734 => 254,  729 => 253,  724 => 251,  717 => 248,  712 => 247,  707 => 245,  695 => 236,  687 => 232,  682 => 231,  677 => 229,  672 => 228,  665 => 225,  660 => 224,  655 => 222,  650 => 221,  643 => 218,  638 => 217,  633 => 215,  628 => 214,  621 => 211,  616 => 210,  611 => 208,  603 => 204,  598 => 203,  593 => 201,  586 => 198,  581 => 197,  576 => 195,  569 => 192,  564 => 191,  559 => 189,  551 => 185,  546 => 184,  541 => 182,  534 => 179,  529 => 178,  524 => 176,  517 => 173,  512 => 172,  507 => 170,  499 => 166,  494 => 165,  489 => 163,  481 => 159,  476 => 158,  471 => 156,  466 => 155,  459 => 152,  454 => 151,  449 => 149,  444 => 148,  435 => 142,  426 => 137,  421 => 136,  416 => 134,  411 => 133,  404 => 130,  399 => 129,  394 => 127,  389 => 126,  382 => 123,  377 => 122,  372 => 120,  367 => 119,  360 => 116,  355 => 115,  350 => 113,  345 => 112,  338 => 109,  333 => 108,  328 => 106,  323 => 105,  313 => 99,  308 => 98,  303 => 96,  298 => 95,  291 => 92,  286 => 91,  281 => 89,  276 => 88,  269 => 85,  264 => 84,  259 => 82,  254 => 81,  247 => 78,  242 => 77,  237 => 75,  232 => 74,  225 => 71,  220 => 70,  215 => 68,  210 => 67,  203 => 64,  198 => 63,  193 => 61,  185 => 57,  180 => 56,  175 => 54,  170 => 53,  160 => 45,  154 => 42,  148 => 41,  146 => 40,  141 => 38,  136 => 37,  131 => 35,  126 => 34,  121 => 32,  116 => 31,  111 => 29,  106 => 28,  101 => 26,  96 => 25,  89 => 21,  76 => 15,  73 => 14,  55 => 11,  51 => 10,  46 => 9,  43 => 8,  37 => 5,  32 => 4,  29 => 3,);
    }
}
