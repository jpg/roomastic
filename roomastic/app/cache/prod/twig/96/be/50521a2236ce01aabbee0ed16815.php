<?php

/* HotelesFrontendBundle:Frontend:listadoAJAXlateral.html.twig */
class __TwigTemplate_96be50521a2236ce01aabbee0ed16815 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
    ";
        // line 2
        if (isset($context["hoteles"])) { $_hoteles_ = $context["hoteles"]; } else { $_hoteles_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_hoteles_);
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 3
            echo "        <li class=\"hotel_li hotel-selected\"  style=\"display: none;\" id=\"hotel-checkbox-";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\" data-hotel-id=\"";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\">
            <input type=\"checkbox\" class=\"checkbox-lateral\" id=\"";
            // line 4
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "\" checked>
            <label for=\"";
            // line 5
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "\">";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "</label>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 8
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listadoAJAXlateral.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 8,  39 => 5,  34 => 4,  221 => 80,  201 => 73,  191 => 67,  186 => 66,  181 => 65,  174 => 62,  161 => 59,  158 => 58,  152 => 57,  149 => 56,  134 => 45,  127 => 40,  123 => 38,  119 => 37,  115 => 35,  111 => 34,  107 => 32,  103 => 31,  99 => 29,  95 => 28,  91 => 26,  87 => 25,  83 => 23,  79 => 22,  75 => 20,  72 => 19,  65 => 16,  60 => 13,  53 => 11,  50 => 10,  40 => 7,  25 => 3,  20 => 2,  17 => 1,);
    }
}
