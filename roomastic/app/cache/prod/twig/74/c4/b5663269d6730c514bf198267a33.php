<?php

/* KnpPaginatorBundle:Pagination:sliding.html.twig */
class __TwigTemplate_74c4b5663269d6730c514bf198267a33 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
";
        // line 3
        if (isset($context["pageCount"])) { $_pageCount_ = $context["pageCount"]; } else { $_pageCount_ = null; }
        if (($_pageCount_ > 1)) {
            // line 4
            echo "<div class=\"pagination\">
    ";
            // line 5
            if (isset($context["current"])) { $_current_ = $context["current"]; } else { $_current_ = null; }
            if (isset($context["first"])) { $_first_ = $context["first"]; } else { $_first_ = null; }
            if ((array_key_exists("first", $context) && ($_current_ != $_first_))) {
                // line 6
                echo "        <span class=\"first\">
            <a href=\"";
                // line 7
                if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                if (isset($context["first"])) { $_first_ = $context["first"]; } else { $_first_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => $_first_))), "html", null, true);
                echo "\">&lt;&lt;</a>
        </span>
    ";
            }
            // line 10
            echo "
    ";
            // line 11
            if (array_key_exists("previous", $context)) {
                // line 12
                echo "        <span class=\"previous\">
            <a href=\"";
                // line 13
                if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                if (isset($context["previous"])) { $_previous_ = $context["previous"]; } else { $_previous_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => $_previous_))), "html", null, true);
                echo "\">&lt;</a>
        </span>
    ";
            }
            // line 16
            echo "
    ";
            // line 17
            if (isset($context["pagesInRange"])) { $_pagesInRange_ = $context["pagesInRange"]; } else { $_pagesInRange_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_pagesInRange_);
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 18
                echo "        ";
                if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
                if (isset($context["current"])) { $_current_ = $context["current"]; } else { $_current_ = null; }
                if (($_page_ != $_current_)) {
                    // line 19
                    echo "            <span class=\"page\">
                <a href=\"";
                    // line 20
                    if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                    if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                    if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                    if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => $_page_))), "html", null, true);
                    echo "\">";
                    if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
                    echo twig_escape_filter($this->env, $_page_, "html", null, true);
                    echo "</a>
            </span>
        ";
                } else {
                    // line 23
                    echo "            <span class=\"current\">";
                    if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
                    echo twig_escape_filter($this->env, $_page_, "html", null, true);
                    echo "</span>
        ";
                }
                // line 25
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 27
            echo "
    ";
            // line 28
            if (array_key_exists("next", $context)) {
                // line 29
                echo "        <span class=\"next\">
            <a href=\"";
                // line 30
                if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                if (isset($context["next"])) { $_next_ = $context["next"]; } else { $_next_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => $_next_))), "html", null, true);
                echo "\">&gt;</a>
        </span>
    ";
            }
            // line 33
            echo "
    ";
            // line 34
            if (isset($context["current"])) { $_current_ = $context["current"]; } else { $_current_ = null; }
            if (isset($context["last"])) { $_last_ = $context["last"]; } else { $_last_ = null; }
            if ((array_key_exists("last", $context) && ($_current_ != $_last_))) {
                // line 35
                echo "        <span class=\"last\">
            <a href=\"";
                // line 36
                if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                if (isset($context["last"])) { $_last_ = $context["last"]; } else { $_last_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => $_last_))), "html", null, true);
                echo "\">&gt;&gt;</a>
        </span>
    ";
            }
            // line 39
            echo "</div>
";
        }
    }

    public function getTemplateName()
    {
        return "KnpPaginatorBundle:Pagination:sliding.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 39,  129 => 35,  125 => 34,  221 => 81,  203 => 76,  198 => 74,  185 => 71,  179 => 69,  163 => 65,  157 => 61,  152 => 60,  133 => 55,  126 => 52,  110 => 48,  76 => 36,  70 => 34,  222 => 85,  207 => 81,  204 => 80,  183 => 74,  167 => 70,  164 => 69,  148 => 63,  141 => 61,  103 => 50,  98 => 49,  59 => 31,  49 => 10,  53 => 29,  21 => 2,  100 => 42,  97 => 25,  18 => 1,  151 => 75,  135 => 68,  114 => 55,  206 => 77,  201 => 75,  194 => 71,  191 => 70,  176 => 61,  166 => 66,  158 => 65,  153 => 64,  143 => 58,  134 => 59,  123 => 51,  118 => 39,  90 => 23,  87 => 47,  66 => 11,  122 => 33,  107 => 28,  101 => 33,  95 => 48,  82 => 45,  67 => 17,  52 => 11,  45 => 29,  36 => 5,  34 => 5,  266 => 117,  263 => 116,  259 => 85,  256 => 84,  242 => 13,  229 => 170,  227 => 84,  218 => 80,  209 => 78,  192 => 98,  186 => 75,  180 => 73,  174 => 95,  162 => 85,  160 => 84,  146 => 50,  140 => 57,  136 => 41,  106 => 66,  73 => 35,  69 => 18,  22 => 6,  60 => 35,  55 => 9,  102 => 46,  89 => 16,  63 => 36,  56 => 30,  50 => 12,  43 => 10,  92 => 47,  79 => 40,  57 => 34,  37 => 7,  33 => 7,  29 => 21,  19 => 1,  47 => 26,  30 => 6,  27 => 20,  249 => 14,  239 => 90,  235 => 12,  228 => 88,  224 => 82,  219 => 84,  217 => 79,  214 => 79,  211 => 77,  208 => 76,  202 => 79,  199 => 78,  193 => 67,  182 => 70,  178 => 61,  175 => 60,  172 => 59,  165 => 55,  161 => 57,  156 => 51,  154 => 50,  150 => 48,  147 => 59,  132 => 36,  127 => 56,  113 => 34,  86 => 43,  83 => 25,  78 => 38,  64 => 17,  61 => 16,  48 => 12,  32 => 22,  24 => 3,  117 => 36,  112 => 30,  109 => 29,  104 => 27,  96 => 32,  84 => 39,  80 => 24,  68 => 35,  46 => 11,  44 => 25,  26 => 5,  23 => 4,  39 => 6,  25 => 2,  20 => 3,  17 => 2,  144 => 62,  138 => 46,  130 => 66,  124 => 55,  121 => 41,  115 => 40,  111 => 52,  108 => 51,  99 => 49,  94 => 44,  91 => 43,  88 => 41,  85 => 39,  77 => 20,  74 => 19,  71 => 38,  65 => 16,  62 => 32,  58 => 8,  54 => 33,  51 => 13,  42 => 8,  38 => 8,  35 => 5,  31 => 3,  28 => 24,);
    }
}
