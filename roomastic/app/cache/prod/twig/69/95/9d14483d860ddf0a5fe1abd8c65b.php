<?php

/* HotelesFrontendBundle::layout.html.twig */
class __TwigTemplate_69959d14483d860ddf0a5fe1abd8c65b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'seotitle' => array($this, 'block_seotitle'),
            'seodescripcion' => array($this, 'block_seodescripcion'),
            'seokeywords' => array($this, 'block_seokeywords'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->
    <head>

        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>Roomastic</title>

        <meta name=\"title\" content=\"";
        // line 12
        $this->displayBlock('seotitle', $context, $blocks);
        echo "\">
        <meta name=\"description\" content=\"";
        // line 13
        $this->displayBlock('seodescripcion', $context, $blocks);
        echo "\">
        <meta name=\"keywords\" content=\"";
        // line 14
        $this->displayBlock('seokeywords', $context, $blocks);
        echo "\">
        <!--<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">-->

        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

        <!--<link href=\"http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css\" rel=\"stylesheet\">-->
        <link href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\">

        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/daterangepicker-bs3.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/selectize.default.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/owl.carousel.css\">
        <link type=\"text/css\" href=\"/bundles/hotelesfrontend/css/plugins/base.css\" rel=\"stylesheet\" />
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/jquery.fancybox.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/sweet-alert.css\">

        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/roomastic.css\">
        <script src=\"/bundles/hotelesfrontend/js/vendor/modernizr/modernizr.js\"></script>

        <link rel=\"icon\" type=\"image/x-icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\" />
        <link rel=\"apple-touch-icon\" href=\"/bundles/hotelesfrontend/img/apple-touch-icon.png\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"/bundles/hotelesfrontend/img/apple-touch-icon-72x72.png\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"/bundles/hotelesfrontend/img/apple-touch-icon-114x114.png\">

        <script src=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>

        ";
        // line 43
        echo "
        <!--[if lt IE 9]>
            <script src=\"js/vendor/html5shiv/dist/html5shiv.min.js\"></script>
        <![endif]-->

    </head> 
    ";
        // line 49
        $context["rrss"] = $this->env->getExtension('twig_extension')->getRRSS();
        // line 50
        echo "    <body class=\"portada\">
        <!--[if lt IE 7]>
            <p class=\"browsehappy\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- aviso cookie -->
        <div id=\"cookie-bar\" class=\"fixed\"></div>

        <div class=\"wrapper\">

            <!--<div class=\"degradado_portada\"></div>-->

            <header id=\"header\">
                <div class=\"lineatop\"></div>

                <div class=\"container clearfix\">
                    <a href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"\"><h1 class=\"logo\">Roomastic</h1></a>

                    <ul class=\"social\">
                        <li><a href=\"";
        // line 69
        if (isset($context["rrss"])) { $_rrss_ = $context["rrss"]; } else { $_rrss_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_rrss_, "twitter"), "html", null, true);
        echo "\" target=\"_blank\" title=\"twitter\">twitter</a></li>
                        <li><a class=\"facebook\" href=\"";
        // line 70
        if (isset($context["rrss"])) { $_rrss_ = $context["rrss"]; } else { $_rrss_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_rrss_, "facebook"), "html", null, true);
        echo "\"  target=\"_blank\" title=\"facebook\">facebook</a></li>
                    </ul>
                    <ul class=\"menu\">                      
                        <li><a href=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"HOME\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("HOME"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_faqs"), "html", null, true);
        echo "\" title=\"FAQS\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("FAQS"), "html", null, true);
        echo "</a></li>
                        <!--<li><a href=\"#\" title=\"AYUDA\">";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("AYUDA"), "html", null, true);
        echo "</a></li>-->
                        <li><a href=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" title=\"CONTACTO\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONTACTO"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_quienesSomos"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos?"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos?"), "html", null, true);
        echo "</a></li>
                    </ul>
                </div><!-- container -->

            </header><!-- /header -->

            <div class=\"container\">        
                ";
        // line 84
        $this->displayBlock('content', $context, $blocks);
        // line 85
        echo "             
            </div><!-- container -->

        </div><!-- wrapper -->

        <footer id=\"footer\">

            <div class=\"footer1 footer-opaco\">
                <div class=\"container\">
                    <ul class=\"menu_foot clearfix\">                                                 
                        <li><a href=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_privacidad"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES PARTICULARES Y PRIVACIDAD"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_condicionesLegales"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES DEL SERVICIO"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_publicaTuHotel"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PUBLICA TU HOTEL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"v_movil\" href=\"";
        // line 98
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "attributes"), "get", array(0 => "_route"), "method"), twig_array_merge($this->env->getExtension('twig_extension')->getCurrentRouteParams(), array("version" => "mobile"))), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("VERSIÓN MÓVIL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"a_hoteles\" href=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_login"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ACCESO HOTELES"), "html", null, true);
        echo "</a></li>


                    </ul>
                    <div class=\"att_cliente\">
                        ";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ATENCIÓN AL CLIENTE"), "html", null, true);
        echo " <br /> <span>911 610 156</span>
                    </div>
                </div><!-- container -->
            </div><!-- footer1 -->
            <div class=\"footer2\">
                <div class=\"container\">
                    <span>";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("© Copyright. Todos los derechos reservados."), "html", null, true);
        echo "</span>
                </div><!-- container -->
            </div><!-- footer2 -->

        </footer>

        ";
        // line 116
        $this->displayBlock('javascripts', $context, $blocks);
        // line 170
        echo "    </body>
</html>
";
    }

    // line 12
    public function block_seotitle($context, array $blocks = array())
    {
        if (isset($context["seo"])) { $_seo_ = $context["seo"]; } else { $_seo_ = null; }
        echo twig_escape_filter($this->env, (($this->getAttribute($_seo_, "seotitulo", array(), "any", true, true)) ? ($this->getAttribute($_seo_, "seotitulo")) : ("")), "html", null, true);
    }

    // line 13
    public function block_seodescripcion($context, array $blocks = array())
    {
        if (isset($context["seo"])) { $_seo_ = $context["seo"]; } else { $_seo_ = null; }
        echo twig_escape_filter($this->env, (($this->getAttribute($_seo_, "seodescripcion", array(), "any", true, true)) ? ($this->getAttribute($_seo_, "seodescripcion")) : ("")), "html", null, true);
    }

    // line 14
    public function block_seokeywords($context, array $blocks = array())
    {
        if (isset($context["seo"])) { $_seo_ = $context["seo"]; } else { $_seo_ = null; }
        echo twig_escape_filter($this->env, (($this->getAttribute($_seo_, "seokeywords", array(), "any", true, true)) ? ($this->getAttribute($_seo_, "seokeywords")) : ("")), "html", null, true);
    }

    // line 84
    public function block_content($context, array $blocks = array())
    {
        // line 85
        echo "                ";
    }

    // line 116
    public function block_javascripts($context, array $blocks = array())
    {
        // line 117
        echo "            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
            <script>window.jQuery || document.write('<script src=\"/bundles/hotelesfrontend/js/vendor/jquery/dist/jquery.min.js\"><\\/script>')</script>

            <script src=\"/bundles/hotelesfrontend/js/plugins.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.backstretch.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/moment.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/daterangepicker.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/selectize.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/icheck.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/gmap3.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/owl.carousel.min.js\"></script>
            <!-- slide fotos detalle hotel -->
            <script type=\"text/javascript\" src=\"/bundles/hotelesfrontend/js/plugins/jquery.jcarousel.min.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesfrontend/js/plugins/jquery.pikachoose.js\"></script>
            <!-- fancybox -->
            <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.pack.js\"></script>
            <!-- api maps -->
            <script src=\"https://maps.googleapis.com/maps/api/js?v=3.exp\"></script>
            <!-- cookies -->
            <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.cookiebar.js\"></script>


            <script src=\"/bundles/hotelesfrontend/js/roomastic.js\"></script>

            <!-- script slide foto hotel detalle -->
            <script type=\"text/javascript\">
                jQuery(document).ready(function() {
                    var a = function(self) {
                        self.anchor.fancybox();
                    };
                    jQuery(\"#pikame\").PikaChoose({buildFinished: a});
                });
            </script> 


            <script>
                (function(b, o, i, l, e, r) {
                    b.GoogleAnalyticsObject = l;
                    b[l] || (b[l] =
                            function() {
                                (b[l].q = b[l].q || []).push(arguments)
                            });
                    b[l].l = +new Date;
                    e = o.createElement(i);
                    r = o.getElementsByTagName(i)[0];
                    e.src = '//www.google-analytics.com/analytics.js';
                    r.parentNode.insertBefore(e, r)
                }(window, document, 'script', 'ga'));
                ga('create', 'UA-53266623-1');
                ga('send', 'pageview');
            </script>
        ";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  266 => 117,  263 => 116,  259 => 85,  256 => 84,  249 => 14,  242 => 13,  235 => 12,  229 => 170,  227 => 116,  218 => 110,  209 => 104,  199 => 99,  192 => 98,  186 => 97,  180 => 96,  174 => 95,  162 => 85,  160 => 84,  146 => 77,  140 => 76,  136 => 75,  130 => 74,  124 => 73,  117 => 70,  112 => 69,  106 => 66,  88 => 50,  86 => 49,  78 => 43,  73 => 38,  69 => 37,  43 => 14,  39 => 13,  35 => 12,  22 => 1,);
    }
}
