<?php

/* HotelesBackendBundle:Hotel:edit.html.twig */
class __TwigTemplate_3ca2e4eb3f720a2cad9a813bc0fbff31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar hotel"), "html", null, true);
        echo "
";
    }

    // line 7
    public function block_javascripts($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 9
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "834bf80_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_834bf80_0") : $this->env->getExtension('assets')->getAssetUrl("js/834bf80_part_1_hotel_create_update_1.js");
            // line 10
            echo "    <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "834bf80"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_834bf80") : $this->env->getExtension('assets')->getAssetUrl("js/834bf80.js");
            echo "    <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 15
        echo "

    <form action=\"";
        // line 17
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_update", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\" class=\"tasi-form\" method=\"post\" id=\"formulariosubida\"  ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_edit_form_);
        echo ">
        <div class=\"row\">
            <div class=\"col-lg-6\">
                <!--tab nav start-->
                <section class=\"panel\">
                    <header class=\"panel-heading\">
                        ";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar hotel"), "html", null, true);
        echo "
                    </header>
                    <header class=\"panel-heading tab-bg-dark-navy-blue \">
                        <ul class=\"nav nav-tabs\">
                            <li class=\"active ";
        // line 27
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "email")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombreempresa")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cif")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "direccionfacturacion")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefono")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "numerocuenta")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#datoshotel\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 30
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrepersonacontacto")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "apellidospersonacontacto")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cargopersonacontacto")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "emailpersonacontacto")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefonopersonacontacto")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#datospersona\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la persona de contacto"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 33
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrehotel")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "url")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "descripcion")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "caracteristicas")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "ubicacion")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "pvpoficialenteros")), 6 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "pvpoficialdecimales")), 7 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cantidadminimaaceptadaenteros")), 8 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cantidadminimaaceptadadecimales")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" class=\"caracteristicashotel\" href=\"#caracteristicashotel\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ficha del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 36
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "imagenes")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" class=\"imageneshotel\" href=\"#imageneshotel\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imágenes del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 39
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "seotitulo")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "seodescripcion")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "seokeywords")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#seo\">";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Seo"), "html", null, true);
        echo "</a>
                            </li>
                            ";
        // line 42
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 43
            echo "                                <li class=\"";
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "comisionhotelentero")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "comisionhoteldecimal")))), "html", null, true);
            echo "\">
                                    <a data-toggle=\"tab\" href=\"#comisiones\">";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comisiones"), "html", null, true);
            echo "</a>
                                </li>
                            ";
        }
        // line 47
        echo "                        </ul>
                    </header>
                    <div class=\"panel-body\">
                        <div class=\"tab-content\">
                            <div id=\"datoshotel\" class=\"tab-pane active\">
                                <div class=\"form-group ";
        // line 52
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "email"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 55
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "email"));
        echo "
                                        ";
        // line 56
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 59
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "empresa"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 62
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "empresa"));
        echo "
                                        ";
        // line 63
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "empresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>                
                                <div class=\"form-group ";
        // line 66
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombreempresa"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de empresa"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 69
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombreempresa"));
        echo "
                                        ";
        // line 70
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "nombreempresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 73
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cif"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cif"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 76
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cif"));
        echo "
                                        ";
        // line 77
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "cif"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 80
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "direccionfacturacion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dirección de facturación"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 83
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "direccionfacturacion"));
        echo "
                                        ";
        // line 84
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "direccionfacturacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 87
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefono"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 90
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefono"));
        echo "
                                        ";
        // line 91
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "telefono"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 94
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "numerocuenta"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de cuenta"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 97
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "numerocuenta"));
        echo "
                                        ";
        // line 98
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "numerocuenta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  

                            </div>
                            <div id=\"datospersona\" class=\"tab-pane\">
                                <div class=\"form-group ";
        // line 104
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrepersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la persona contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 107
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrepersonacontacto"));
        echo "
                                        ";
        // line 108
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "nombrepersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 111
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "apellidospersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 114
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "apellidospersonacontacto"));
        echo "
                                        ";
        // line 115
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "apellidospersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 118
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cargopersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 121
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cargopersonacontacto"));
        echo "
                                        ";
        // line 122
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "cargopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 125
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "emailpersonacontacto"))), "html", null, true);
        echo " \">
                                    <label>";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 128
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "emailpersonacontacto"));
        echo "
                                        ";
        // line 129
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "emailpersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 132
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefonopersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 135
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefonopersonacontacto"));
        echo "
                                        ";
        // line 136
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "telefonopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  
                            </div>
                            <div id=\"caracteristicashotel\" class=\"tab-pane\">
                                <div class=\"form-group ";
        // line 141
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrehotel"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre del hotel"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 144
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrehotel"));
        echo "
                                        ";
        // line 145
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "nombrehotel"), array("attr" => array("class" => "form-control nombre-hotel")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group ";
        // line 148
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "url"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Url"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 151
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "url"));
        echo "
                                        ";
        // line 152
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "url"), array("attr" => array("class" => "form-control url-hotel")));
        echo "                        
                                    </div>
                                </div> 
                                <div class=\"form-group\">
                                    <label>";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Selecciona el número de estrellas"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 158
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "calificacion"));
        echo "
                                        ";
        // line 159
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "calificacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-2\">";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Piscina"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 165
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "piscina"));
        echo "
                                        ";
        // line 166
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "piscina"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 169
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Spa"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 171
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "spa"));
        echo "
                                        ";
        // line 172
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "spa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wi-Fi"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 177
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "wi_fi"));
        echo "
                                        ";
        // line 178
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "wi_fi"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-2\">";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso adaptado"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 184
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "acceso_adaptado"));
        echo "
                                        ";
        // line 185
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "acceso_adaptado"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 188
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptan perros"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 190
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "aceptan_perros"));
        echo "
                                        ";
        // line 191
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "aceptan_perros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 194
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aparcamiento"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 196
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "aparcamiento"));
        echo "
                                        ";
        // line 197
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "aparcamiento"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                </div>
                                <div class=\"form-group padgbott\">

                                    <label class=\"col-sm-2\">";
        // line 203
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Business-Center"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 205
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "business_center"));
        echo "
                                        ";
        // line 206
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "business_center"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>                    
                                <div class=\"form-group ";
        // line 209
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "descripcion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 210
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica una descripción breve de aproximadamente 100 carácteres"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 212
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "descripcion"));
        echo "
                                        ";
        // line 213
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "descripcion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 216
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "caracteristicas"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 217
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enumera las características del hotel de forma breve en aproximadamente 100 carácteres"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 219
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "caracteristicas"));
        echo "
                                        ";
        // line 220
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "caracteristicas"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group ";
        // line 223
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "ubicacion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 224
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Escribe el nombre de la calle y número acompañado de la localidad, de la ubicación del hotel"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 226
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "ubicacion"));
        echo "
                                        ";
        // line 227
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "ubicacion"), array("attr" => array("class" => "form-control direccionmapa")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group\">
                                    <label>";
        // line 231
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mapa"), "html", null, true);
        echo "</label>
                                    <div>
                                        <div id=\"mapaiframe\" style=\"width:500px!important;height:250px!important;\">
                                        </div>
                                        <div style=\"clear:both;\"></div>
                                    </div>
                                </div>
                                <br>
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-1\">";
        // line 240
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Provincia"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 242
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "provincia"));
        echo "
                                        ";
        // line 243
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "provincia"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-1\">";
        // line 246
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Municipio"), "html", null, true);
        echo "&nbsp</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 248
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "municipio"));
        echo "
                                        ";
        // line 249
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "municipio"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-1\">";
        // line 252
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Zona"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 254
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "lugar"));
        echo "
                                        ";
        // line 255
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "lugar"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 258
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "pvpoficialenteros"))), "html", null, true);
        echo " ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "pvpoficialdecimales"))), "html", null, true);
        echo " padgbott\">
                                    <label class=\"col-sm-6\">";
        // line 259
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica el pvp oficial en habitación individual de uso doble"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 261
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "pvpoficialenteros"));
        echo "
                                        ";
        // line 262
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "pvpoficialenteros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 265
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "pvpoficialdecimales"));
        echo "
                                        ";
        // line 266
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "pvpoficialdecimales"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>                    
                                </div>  
                                <div class=\"clearfix\"></div>
                                <div class=\"form-group ";
        // line 270
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cantidadminimaaceptadaenteros"))), "html", null, true);
        echo " ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cantidadminimaaceptadadecimales"))), "html", null, true);
        echo " padgbott\">
                                    <label class=\"col-sm-6\">";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica la cantidad mínima en euros por la que estás dispuesto a recibir ofertas"), "html", null, true);
        echo "</label>        
                                    <div class=\"col-sm-2\">
                                        ";
        // line 273
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cantidadminimaaceptadaenteros"));
        echo "
                                        ";
        // line 274
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "cantidadminimaaceptadaenteros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 277
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cantidadminimaaceptadadecimales"));
        echo "
                                        ";
        // line 278
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "cantidadminimaaceptadadecimales"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>                    
                                </div>  
                                <div class=\"clearfix\"></div>
                                <div class=\"form-group\">
                                    <label class=\"col-sm-6\">";
        // line 283
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado de publicación de la ficha del hotel"), "html", null, true);
        echo "</label>
                                    <div  class=\"col-sm-3\">
                                        ";
        // line 285
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "status"));
        echo "
                                        ";
        // line 286
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "status"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  

                            </div>
                            <div id=\"imageneshotel\" class=\"tab-pane\">     
                                <div style=\"height:0px; display:none;\">
                                    ";
        // line 293
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "imagenes"), array("attr" => array("class" => "")));
        echo "   
                                </div>            
                                <div class=\"form-group ";
        // line 295
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "imagenes"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 296
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "imagenes"));
        echo "</label>
                                </div>  
                                <div id=\"imagenes\" style=\"height:15px;width:100%;\">
                                    <input type=\"file\" name=\"archivo\" id=\"inputsubida\">
                                    ";
        // line 303
        echo "                                </div>
                                <iframe src=\"";
        // line 304
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "hoteles")), "html", null, true);
        echo "\" height=\"0\" width=\"1300\" style=\"border:0px;\" name=\"inter\"></iframe>
                                <div id=\"imagenessubidas\" style=\"height:auto;width:100%;background-color:white;\"> 
                                    <div class=\"row\">
                                        <div class=\"col-md-12\">
                                            <section class=\"panel tasks-widget\">

                                                <div class=\"panel-body\">
                                                    <div class=\"task-content\">
                                                        <ul id=\"sortable\" class=\"task-list\" >
                                                            ";
        // line 313
        if (isset($context["imagenes"])) { $_imagenes_ = $context["imagenes"]; } else { $_imagenes_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_imagenes_);
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 314
            echo "                                                                <li class=\"list-primary borraelemento_";
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "id"), "html", null, true);
            echo "\" data-id=\"";
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "id"), "html", null, true);
            echo "\">
                                                                    <i class=\" fa fa-ellipsis-v\"></i>
                                                                    <div class=\"task-title\">
                                                                        <img src=\"/uploads/hoteles/";
            // line 317
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "imagen"), "html", null, true);
            echo "\" width=\"200\">
                                                                        <div class=\"pull-right hidden-phone\"> 
                                                                            <a href=\"#\">
                                                                                <button class=\"btn btn-success btn-xs fa fa-pencil\"></button>
                                                                            </a>&nbsp;&nbsp;
                                                                            <a href=\"#\" onClick=\"deleteElement('";
            // line 322
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "id"), "html", null, true);
            echo "');
                                                                                    return false;\">
                                                                                <button class=\"btn btn-danger btn-xs fa fa-trash-o\"></button>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 330
        echo "                                                        </ul>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>                    
                                </div>
                                <div id=\"imagenessubidasinputs\" style=\"display:none;height:0px;";
        // line 337
        echo "width:100%;background-color:white;\"> 
                                    ";
        // line 338
        if (isset($context["imagenes"])) { $_imagenes_ = $context["imagenes"]; } else { $_imagenes_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_imagenes_);
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 339
            echo "                                        <input type=\"text\" name=\"imageneshotel[]\" class=\"borraelemento_";
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "id"), "html", null, true);
            echo "\" value=\"";
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_imagen_, "id"), "html", null, true);
            echo "\"><br>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 341
        echo "                                </div>

                                <a class=\"\" data-toggle=\"modal\" href=\"#myModal\" id=\"clickmodal\"></a>


                                <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" style=\"width: 923px;margin: 36px auto;height: 508px!important;\">
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h4 class=\"modal-title\">";
        // line 351
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar imagen"), "html", null, true);
        echo "</h4>
                                            </div>

                                            <div style=\"width:100%; height:auto; padding-top: 17px;\" >
                                                <img src=\"/bundles/hotelesbackend/img/logo2.png\" id=\"target\" width=\"200\" alt=\"[Jcrop Example]\" />
                                            </div>

                                            <div class=\"clearfix\"></div>

                                            <div class=\"modal-footer\">
                                                <button data-dismiss=\"modal\" id=\"closeimage\" class=\"btn btn-default\" type=\"button\">";
        // line 361
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cerrar"), "html", null, true);
        echo "</button>
                                                <button class=\"btn btn-success\" id=\"successimage\" data-id=\"\" data-url=\"\" data-img=\"\" type=\"button\">";
        // line 362
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar cambios"), "html", null, true);
        echo "</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>          
                            <div id=\"seo\" class=\"tab-pane\">
                                <div class=\"form-group ";
        // line 371
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "seotitulo"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 372
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Título para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 374
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "seotitulo"));
        echo "
                                        ";
        // line 375
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "seotitulo"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 378
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "seodescripcion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 379
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Descripción para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 381
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "seodescripcion"));
        echo "
                                        ";
        // line 382
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "seodescripcion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 385
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "seokeywords"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 386
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Keywords para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 388
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "seokeywords"));
        echo "
                                        ";
        // line 389
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "seokeywords"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                            </div>
                            ";
        // line 393
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 394
            echo "
                                <div id=\"comisiones\" class=\"tab-pane\">
                                    <div class=\"form-group\">
                                        <label for=\"exampleInputEmail1\">";
            // line 397
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Porcentaje de comisión (campo enteros) , (campo decimales)."), "html", null, true);
            echo "</label>
                                        <div class=\"clearfix\"></div>
                                        <div class=\"col-sm-2-dcnt ";
            // line 399
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "comisionhotelentero"))), "html", null, true);
            echo "\">
                                            ";
            // line 400
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "comisionhotelentero"));
            echo "
                                            ";
            // line 401
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "comisionhotelentero"), array("attr" => array("class" => "form-control")));
            echo "                     
                                        </div>
                                        <div class=\"comaclear\" style=\"float:left\"> , </div>
                                        <div class=\"col-sm-2-dcnt ";
            // line 404
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "comisionhoteldecimal"))), "html", null, true);
            echo "\">  
                                            ";
            // line 405
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "comisionhoteldecimal"));
            echo "
                                            ";
            // line 406
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "comisionhoteldecimal"), array("attr" => array("class" => "form-control")));
            echo "                                  
                                        </div>
                                        <div class=\"clearfix\"></div>
                                    </div>
                                </div>
                            ";
        }
        // line 412
        echo "
                        </div>  
                </section>
                <section class=\"panel\">

                    <div class=\"panel-body\">

                        ";
        // line 419
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_edit_form_);
        echo "
                        <button type=\"submit\" class=\"btn btn-info\">";
        // line 420
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>

                        </form>
                        <a href=\"";
        // line 423
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel"), "html", null, true);
        echo "\" class=\"btn btn-primary\">
                            ";
        // line 424
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "
                        </a>
                        ";
        // line 426
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        if (($this->getAttribute($_entity_, "status", array(), "any", true, true) && ($this->getAttribute($_entity_, "status") == 2))) {
            // line 427
            echo "                            <form action=\"";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_reactivate", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\" method=\"post\" class=\"btnDelete\">
                                ";
            // line 428
            if (isset($context["reactivate_form"])) { $_reactivate_form_ = $context["reactivate_form"]; } else { $_reactivate_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($_reactivate_form_);
            echo "
                                <button type=\"submit\" class=\"btn btn-success\" onclick=\"return confirm('¿Está seguro de re-activar este hotel?');\">";
            // line 429
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Reactivar hotel"), "html", null, true);
            echo "</button>
                            </form>
                        ";
        } else {
            // line 432
            echo "                            <form action=\"";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_delete", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\" method=\"post\" class=\"btnDelete\">
                                ";
            // line 433
            if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($_delete_form_);
            echo "
                                <button type=\"submit\" class=\"btn btn-danger\"  onclick=\"return confirm('¿Estás seguro que quieres suspender este hotel?\\n' +
                                                'Realizar esta acción cancelará todas las ofertas activas de los usuarios y no se podrán reactivar de ninguna forma.');\">";
            // line 435
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suspender"), "html", null, true);
            echo "</button>
                            </form>
                        ";
        }
        // line 438
        echo "                    </div>
                </section>

            </div><!-- /col -->
        </div><!-- /row -->

    </form>      

";
    }

    // line 448
    public function block_jsextras($context, array $blocks = array())
    {
        // line 449
        echo "
    <script type=\"text/javascript\">
        // carga dinamicamente municipio
        var idprovincia = \$('#form_provincia option:selected').val();
        \$valor_municipio = \$('#form_municipio').val();
        //\$('#form_municipio').load(Routing.generate('admin_lugaresfiltramunicipio', {idprovincia: idprovincia, obligatorio: 0}));
        //\$('#form_municipio').val(\$valor_municipio);

        \$('#form_lugar').html('');
        \$('#form_provincia').change(function (event) {
            var idprovincia = \$('#form_provincia option:selected').val();
            \$('#form_municipio').load(Routing.generate('admin_lugaresfiltramunicipio', {idprovincia: idprovincia, obligatorio: 0}));
        });
        // carga dinamicamente zona        
        \$('#form_municipio').change(function (event) {
            var idprovincia = \$('#form_provincia option:selected').val();
            var idmunicipio = \$('#form_municipio option:selected').val();
            \$('#form_lugar').load(Routing.generate('admin_lugaresfiltralugares', {idprovincia: idprovincia, idmunicipio: idmunicipio, obligatorio: 0}));
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            TaskList.initTaskWidget();
        });

        \$(function () {

            \$(\"#sortable\").sortable({
                stop: function (event, ui) {
                    \$('#imagenessubidasinputs').html('')
                    \$('.list-primary').each(function (indice, elemento) {
                        \$('#imagenessubidasinputs').append('<input type=\"text\" name=\"imageneshotel[]\" class=\"borraelemento_' + \$(elemento).attr('data-id') + '\" value=\"' + \$(elemento).attr('data-id') + '\"><br>')
                    });
                    //return false
                }
            });
            \$(\"#sortable\").disableSelection();
        });
    </script>

    ";
        // line 490
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        $context["urlcrea"] = $this->env->getExtension('routing')->getPath("admin_hotel_update", array("id" => $this->getAttribute($_entity_, "id")));
        // line 491
        echo "    ";
        $context["urlsubidaimagen"] = $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "hoteles"));
        // line 492
        echo "    ";
        $context["tipo"] = "hoteles";
        // line 493
        echo "    ";
        $context["crop1"] = "100";
        // line 494
        echo "    ";
        $context["crop2"] = "100";
        // line 495
        echo "    ";
        $context["crop3"] = "50";
        // line 496
        echo "    ";
        $context["crop4"] = "50";
        // line 497
        echo "    ";
        $context["crop5"] = "16";
        // line 498
        echo "    ";
        $context["crop6"] = "9";
        // line 499
        echo "
    ";
        // line 500
        $this->env->loadTemplate("HotelesBackendBundle:Crop:crop.html.twig")->display(array_merge($context, array("especifico" => "hoteles")));
        // line 501
        echo "

    <script src=\"/bundles/hotelesbackend/assets/dropzone/dropzone.js\"></script>
    <script src=\"/bundles/hotelesbackend/js/respond.min.js\" ></script>


";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Hotel:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1272 => 501,  1270 => 500,  1267 => 499,  1264 => 498,  1261 => 497,  1258 => 496,  1255 => 495,  1252 => 494,  1249 => 493,  1246 => 492,  1243 => 491,  1240 => 490,  1197 => 449,  1194 => 448,  1182 => 438,  1176 => 435,  1170 => 433,  1164 => 432,  1158 => 429,  1153 => 428,  1147 => 427,  1144 => 426,  1139 => 424,  1135 => 423,  1129 => 420,  1124 => 419,  1115 => 412,  1105 => 406,  1100 => 405,  1095 => 404,  1088 => 401,  1083 => 400,  1078 => 399,  1073 => 397,  1068 => 394,  1066 => 393,  1058 => 389,  1053 => 388,  1048 => 386,  1043 => 385,  1036 => 382,  1031 => 381,  1026 => 379,  1021 => 378,  1014 => 375,  1009 => 374,  1004 => 372,  999 => 371,  987 => 362,  983 => 361,  970 => 351,  958 => 341,  945 => 339,  940 => 338,  937 => 337,  928 => 330,  913 => 322,  904 => 317,  893 => 314,  888 => 313,  876 => 304,  873 => 303,  865 => 296,  860 => 295,  854 => 293,  843 => 286,  838 => 285,  833 => 283,  824 => 278,  819 => 277,  812 => 274,  807 => 273,  802 => 271,  794 => 270,  786 => 266,  781 => 265,  774 => 262,  769 => 261,  764 => 259,  756 => 258,  749 => 255,  744 => 254,  739 => 252,  732 => 249,  727 => 248,  722 => 246,  715 => 243,  710 => 242,  705 => 240,  693 => 231,  685 => 227,  680 => 226,  675 => 224,  670 => 223,  663 => 220,  658 => 219,  653 => 217,  648 => 216,  641 => 213,  636 => 212,  631 => 210,  626 => 209,  619 => 206,  614 => 205,  609 => 203,  599 => 197,  594 => 196,  589 => 194,  582 => 191,  577 => 190,  572 => 188,  565 => 185,  560 => 184,  555 => 182,  547 => 178,  542 => 177,  537 => 175,  530 => 172,  525 => 171,  520 => 169,  513 => 166,  508 => 165,  503 => 163,  495 => 159,  490 => 158,  485 => 156,  477 => 152,  472 => 151,  467 => 149,  462 => 148,  455 => 145,  450 => 144,  445 => 142,  440 => 141,  431 => 136,  426 => 135,  421 => 133,  416 => 132,  409 => 129,  404 => 128,  399 => 126,  394 => 125,  387 => 122,  382 => 121,  377 => 119,  372 => 118,  365 => 115,  360 => 114,  355 => 112,  350 => 111,  343 => 108,  338 => 107,  333 => 105,  328 => 104,  318 => 98,  313 => 97,  308 => 95,  303 => 94,  296 => 91,  291 => 90,  286 => 88,  281 => 87,  274 => 84,  269 => 83,  264 => 81,  259 => 80,  252 => 77,  247 => 76,  242 => 74,  237 => 73,  230 => 70,  225 => 69,  220 => 67,  215 => 66,  208 => 63,  203 => 62,  198 => 60,  193 => 59,  186 => 56,  181 => 55,  176 => 53,  171 => 52,  164 => 47,  158 => 44,  152 => 43,  150 => 42,  145 => 40,  140 => 39,  135 => 37,  130 => 36,  125 => 34,  120 => 33,  115 => 31,  110 => 30,  105 => 28,  100 => 27,  93 => 23,  80 => 17,  76 => 15,  73 => 14,  55 => 10,  51 => 9,  46 => 8,  43 => 7,  37 => 5,  32 => 4,  29 => 3,);
    }
}
