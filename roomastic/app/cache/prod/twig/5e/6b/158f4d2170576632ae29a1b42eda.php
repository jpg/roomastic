<?php

/* FOSUserBundle:Profile:show_content.html.twig */
class __TwigTemplate_5e6b158f4d2170576632ae29a1b42eda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"fos_user_user_show\">
    <p>";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.username", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_user_, "username"), "html", null, true);
        echo "</p>
    <p>";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("profile.show.email", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_user_, "email"), "html", null, true);
        echo "</p>
</div>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  21 => 2,  100 => 42,  97 => 41,  18 => 1,  151 => 75,  135 => 68,  114 => 55,  206 => 77,  201 => 76,  194 => 71,  191 => 70,  176 => 61,  166 => 58,  158 => 56,  153 => 55,  143 => 49,  134 => 44,  123 => 40,  118 => 39,  90 => 43,  87 => 47,  66 => 18,  122 => 37,  107 => 34,  101 => 33,  95 => 29,  82 => 45,  67 => 17,  52 => 11,  45 => 6,  36 => 5,  34 => 6,  266 => 117,  263 => 116,  259 => 85,  256 => 84,  242 => 13,  229 => 170,  227 => 116,  218 => 110,  209 => 78,  192 => 98,  186 => 67,  180 => 63,  174 => 95,  162 => 85,  160 => 84,  146 => 50,  140 => 70,  136 => 41,  106 => 66,  73 => 20,  69 => 37,  22 => 2,  60 => 15,  55 => 9,  102 => 19,  89 => 16,  63 => 14,  56 => 33,  50 => 6,  43 => 14,  92 => 48,  79 => 40,  57 => 14,  37 => 6,  33 => 6,  29 => 4,  19 => 1,  47 => 5,  30 => 3,  27 => 3,  249 => 14,  239 => 90,  235 => 12,  228 => 84,  224 => 82,  219 => 80,  217 => 79,  214 => 79,  211 => 77,  208 => 76,  202 => 72,  199 => 99,  193 => 67,  182 => 63,  178 => 61,  175 => 60,  172 => 59,  165 => 55,  161 => 57,  156 => 51,  154 => 50,  150 => 48,  147 => 74,  132 => 39,  127 => 43,  113 => 34,  86 => 49,  83 => 25,  78 => 38,  64 => 37,  61 => 36,  48 => 10,  32 => 5,  24 => 3,  117 => 36,  112 => 69,  109 => 53,  104 => 51,  96 => 32,  84 => 39,  80 => 24,  68 => 35,  46 => 14,  44 => 9,  26 => 3,  23 => 3,  39 => 6,  25 => 2,  20 => 2,  17 => 1,  144 => 46,  138 => 46,  130 => 66,  124 => 73,  121 => 41,  115 => 40,  111 => 36,  108 => 31,  99 => 49,  94 => 29,  91 => 17,  88 => 41,  85 => 26,  77 => 39,  74 => 20,  71 => 39,  65 => 16,  62 => 34,  58 => 8,  54 => 13,  51 => 32,  42 => 9,  38 => 8,  35 => 5,  31 => 4,  28 => 3,);
    }
}
