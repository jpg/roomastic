<?php

/* HotelesBackendBundle:Extras:preload.html.twig */
class __TwigTemplate_f550077a2240ee2ae90b0b5d61f67d99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"fade\"></div>
<div id=\"modal\">
    ";
        // line 3
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "2be4f30_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2be4f30_0") : $this->env->getExtension('assets')->getAssetUrl("images/2be4f30_loading_1.gif");
            // line 4
            echo "    <img id=\"loader\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\" alt=\"Cargando...\" />
    ";
        } else {
            // asset "2be4f30"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2be4f30") : $this->env->getExtension('assets')->getAssetUrl("images/2be4f30.gif");
            echo "    <img id=\"loader\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\" alt=\"Cargando...\" />
    ";
        }
        unset($context["asset_url"]);
        // line 5
        echo "  
</div>";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:preload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 5,  25 => 4,  21 => 3,  875 => 506,  825 => 503,  821 => 498,  817 => 497,  813 => 496,  798 => 485,  793 => 484,  767 => 463,  760 => 462,  754 => 461,  748 => 460,  742 => 459,  734 => 453,  727 => 447,  725 => 446,  714 => 440,  711 => 438,  707 => 436,  704 => 435,  701 => 434,  698 => 433,  695 => 432,  677 => 419,  668 => 409,  658 => 406,  650 => 405,  647 => 404,  642 => 403,  628 => 394,  616 => 384,  606 => 376,  589 => 362,  577 => 354,  560 => 341,  553 => 339,  541 => 328,  524 => 318,  520 => 317,  515 => 315,  511 => 314,  504 => 311,  499 => 310,  492 => 306,  479 => 297,  470 => 291,  462 => 287,  456 => 285,  450 => 283,  444 => 281,  438 => 279,  432 => 277,  426 => 274,  421 => 272,  414 => 271,  402 => 263,  398 => 262,  389 => 257,  383 => 255,  377 => 253,  371 => 251,  365 => 249,  359 => 247,  353 => 245,  347 => 243,  341 => 240,  336 => 238,  328 => 236,  309 => 220,  305 => 219,  297 => 214,  291 => 211,  249 => 173,  244 => 172,  228 => 159,  223 => 157,  218 => 155,  214 => 154,  195 => 138,  187 => 135,  182 => 133,  177 => 131,  166 => 130,  161 => 129,  156 => 127,  150 => 124,  141 => 121,  119 => 101,  117 => 100,  51 => 36,  17 => 1,);
    }
}
