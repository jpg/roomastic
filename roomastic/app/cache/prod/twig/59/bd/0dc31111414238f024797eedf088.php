<?php

/* HotelesFrontendBundle:Frontend:condicionesLegales.html.twig */
class __TwigTemplate_59bd0dc31111414238f024797eedf088 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"contenido\">

\t<h2>";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aviso legal y condiciones del servicio"), "html", null, true);
        echo "</h2>
\t<div class=\"txtBack\">
\t\t";
        // line 9
        if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
        if ($this->getAttribute($_content_, "texto", array(), "any", true, true)) {
            // line 10
            echo "\t\t\t";
            if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
            echo $this->getAttribute($_content_, "texto");
            echo "
\t\t";
        }
        // line 12
        echo "\t</div>

</div><!-- contenido -->


";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:condicionesLegales.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 12,  42 => 10,  39 => 9,  34 => 7,  29 => 4,  26 => 3,);
    }
}
