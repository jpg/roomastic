<?php

/* HotelesFrontendBundle:TPV:pago-no-realizado.html.twig */
class __TwigTemplate_d4ad34c96905e552ec22916bf71749c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle:TPV:pago-terminado.html.twig");

        $this->blocks = array(
            'resultado_venta' => array($this, 'block_resultado_venta'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle:TPV:pago-terminado.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_resultado_venta($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"tpv_ko\">
        <p><span>Ha habido algún problema</span></p>
        <p>Tu pago no ha podido realizarse correctamente. Inténtalo de nuevo pasados unos minutos o con otra tarjeta. Si el problema persiste, llámanos.</p>
    </div>    
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:TPV:pago-no-realizado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 55,  293 => 6,  290 => 5,  274 => 105,  270 => 104,  252 => 97,  248 => 96,  241 => 93,  237 => 92,  233 => 90,  226 => 86,  223 => 85,  220 => 84,  210 => 81,  188 => 73,  181 => 69,  145 => 51,  137 => 55,  128 => 42,  120 => 40,  41 => 7,  142 => 50,  129 => 35,  125 => 41,  221 => 81,  203 => 78,  198 => 74,  185 => 71,  179 => 69,  163 => 58,  157 => 56,  152 => 60,  133 => 53,  126 => 52,  110 => 48,  76 => 47,  70 => 21,  222 => 85,  207 => 81,  204 => 80,  183 => 74,  167 => 70,  164 => 69,  148 => 52,  141 => 61,  103 => 34,  98 => 28,  59 => 31,  49 => 25,  53 => 29,  21 => 2,  100 => 33,  97 => 32,  18 => 1,  151 => 53,  135 => 68,  114 => 55,  206 => 77,  201 => 75,  194 => 71,  191 => 70,  176 => 61,  166 => 59,  158 => 65,  153 => 64,  143 => 58,  134 => 59,  123 => 51,  118 => 40,  90 => 23,  87 => 47,  66 => 11,  122 => 33,  107 => 28,  101 => 33,  95 => 48,  82 => 26,  67 => 40,  52 => 11,  45 => 24,  36 => 5,  34 => 5,  266 => 117,  263 => 101,  259 => 100,  256 => 84,  242 => 13,  229 => 170,  227 => 84,  218 => 80,  209 => 78,  192 => 74,  186 => 75,  180 => 73,  174 => 95,  162 => 85,  160 => 57,  146 => 50,  140 => 56,  136 => 47,  106 => 47,  73 => 22,  69 => 18,  22 => 6,  60 => 12,  55 => 14,  102 => 39,  89 => 25,  63 => 36,  56 => 30,  50 => 12,  43 => 8,  92 => 26,  79 => 21,  57 => 11,  37 => 7,  33 => 5,  29 => 3,  19 => 1,  47 => 26,  30 => 4,  27 => 3,  249 => 14,  239 => 90,  235 => 12,  228 => 88,  224 => 82,  219 => 84,  217 => 79,  214 => 82,  211 => 77,  208 => 76,  202 => 79,  199 => 77,  193 => 67,  182 => 70,  178 => 61,  175 => 60,  172 => 62,  165 => 55,  161 => 57,  156 => 51,  154 => 50,  150 => 48,  147 => 59,  132 => 36,  127 => 44,  113 => 50,  86 => 24,  83 => 57,  78 => 54,  64 => 14,  61 => 16,  48 => 12,  32 => 4,  24 => 3,  117 => 51,  112 => 38,  109 => 48,  104 => 27,  96 => 32,  84 => 39,  80 => 24,  68 => 35,  46 => 9,  44 => 11,  26 => 2,  23 => 4,  39 => 9,  25 => 2,  20 => 1,  17 => 2,  144 => 62,  138 => 46,  130 => 45,  124 => 55,  121 => 41,  115 => 39,  111 => 52,  108 => 51,  99 => 38,  94 => 27,  91 => 29,  88 => 28,  85 => 27,  77 => 20,  74 => 19,  71 => 17,  65 => 38,  62 => 17,  58 => 8,  54 => 33,  51 => 13,  42 => 8,  38 => 6,  35 => 7,  31 => 3,  28 => 24,);
    }
}
