<?php

/* HotelesBackendBundle:Lugar:new.html.twig */
class __TwigTemplate_64ecce5a1aa24fa207caff351f42a0b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear lugar"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
<div class=\"row\">
  <div class=\"col-lg-6\">
    <section class=\"panel\"> 
        <header class=\"panel-heading\">
            ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear lugar"), "html", null, true);
        echo "
        </header>
        <div class=\"panel-body\">
            <form role=\"form\" action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_lugares_create"), "html", null, true);
        echo "\"  method=\"post\" ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo ">
              <div class=\"form-group ";
        // line 18
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "provincia"))), "html", null, true);
        echo "\">
                  <label for=\"exampleInputEmail1\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Provincia"), "html", null, true);
        echo "</label>
                  <div class=\"\">
                  ";
        // line 21
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "provincia"));
        echo "
                  ";
        // line 22
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "provincia"), array("attr" => array("class" => "form-control")));
        echo "                        
                  </div>
              </div>
              <div class=\"form-group ";
        // line 25
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "municipio"))), "html", null, true);
        echo "\">
                  <label for=\"exampleInputEmail1\">";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Municipio"), "html", null, true);
        echo "</label>
                  <div class=\"\">
                  ";
        // line 28
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "municipio"));
        echo "
                  ";
        // line 29
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "municipio"), array("attr" => array("class" => "form-control")));
        echo "                        
                  </div>
              </div>
              <div class=\"form-group ";
        // line 32
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "zona"))), "html", null, true);
        echo "\">
                  <label for=\"exampleInputEmail1\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Zona"), "html", null, true);
        echo "</label>
                  <div class=\"\">
                  ";
        // line 35
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "zona"));
        echo "
                  ";
        // line 36
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "zona"), array("attr" => array("class" => "form-control")));
        echo "                        
                  </div>
              </div>              
               ";
        // line 39
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_form_);
        echo "
        </div>
    </section>
    <!-- btns -->
    <section class=\"panel\">
          <div class=\"panel-body\">
                <button type=\"submit\" class=\"btn btn-success\">";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear"), "html", null, true);
        echo "</button>   
            </form><!-- / form edit -->
               <a href=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_lugares"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
          </div>
    </section>
    <!-- /btns -->
  </div><!-- /col -->
</div><!-- /row -->

";
        // line 70
        echo "
";
    }

    // line 73
    public function block_jsextras($context, array $blocks = array())
    {
        // line 74
        echo "    <script type=\"text/javascript\">
        var id = \$('#hoteles_backendbundle_lugartype_provincia option:selected').val()
        \$('#hoteles_backendbundle_lugartype_municipio').load(Routing.generate('admin_lugaresfiltramunicipio', { idprovincia: id, obligatorio : 1 }));
        \$('#hoteles_backendbundle_lugartype_provincia').change(function(event) {
            var id = \$('#hoteles_backendbundle_lugartype_provincia option:selected').val()
            \$('#hoteles_backendbundle_lugartype_municipio').load(Routing.generate('admin_lugaresfiltramunicipio', { idprovincia: id, obligatorio : 1 }));         
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Lugar:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 74,  163 => 73,  158 => 70,  146 => 47,  141 => 45,  131 => 39,  124 => 36,  119 => 35,  114 => 33,  109 => 32,  102 => 29,  97 => 28,  92 => 26,  87 => 25,  80 => 22,  75 => 21,  70 => 19,  65 => 18,  58 => 17,  52 => 14,  45 => 9,  42 => 8,  36 => 5,  31 => 4,  28 => 3,);
    }
}
