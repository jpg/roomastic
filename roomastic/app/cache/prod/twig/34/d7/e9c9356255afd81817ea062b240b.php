<?php

/* HotelesFrontendBundle:TPV:layoutTPV.html.twig */
class __TwigTemplate_34d7e9c9356255afd81817ea062b240b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
            'carga' => array($this, 'block_carga'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>
<html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>
<html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>
<html class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class=\"no-js\"> <!--<![endif]-->
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>Roomastic</title>

        <meta name=\"description\" content=\"\">

        <link rel=\"icon\" type=\"image/x-icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\" />

        <!-- modernizr -->
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/roomastic.css\">
        <script src=\"/bundles/hotelesfrontend/js/vendor/modernizr/modernizr.js\"></script>


        <script src=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>

    </head>
    <body>
        <!--[if lt IE 7]>
        <p class=\"chromeframe\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade
            your browser</a> or <a href=\"http://www.google.com/chromeframe/?redirect=true\">activate Google Chrome Frame</a> to
            improve your experience.</p>
        <![endif]-->

        <!-- general wrapper -> layout -> keep footer at the bottom -->
        <div style=\"margin:0px;padding:0px;overflow:hidden\">
            <!-- / orange bar -->
            ";
        // line 38
        $this->displayBlock('content', $context, $blocks);
        // line 40
        echo "            <!-- / wrapper -->

            <!-- / partners -->
        </div>
        <!-- / footer -->

        <!-- jQuery -->
        ";
        // line 47
        $this->displayBlock('javascripts', $context, $blocks);
        // line 54
        echo "
        ";
        // line 55
        $this->displayBlock('carga', $context, $blocks);
        // line 57
        echo "
        <!-- comentado de momento
            <script>
                var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
                (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
                g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
                s.parentNode.insertBefore(g,s)}(document,'script'));
            </script>
                Google Analytics -->
    </div>
    <!-- / general wrapper -> layout -> keep footer at the bottom -->
</body>
</html>";
    }

    // line 38
    public function block_content($context, array $blocks = array())
    {
        // line 39
        echo "            ";
    }

    // line 47
    public function block_javascripts($context, array $blocks = array())
    {
        // line 48
        echo "            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>

            ";
        // line 50
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "7a501bd_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7a501bd_0") : $this->env->getExtension('assets')->getAssetUrl("js/7a501bd_pago-inicial_1.js");
            // line 51
            echo "            <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
            ";
        } else {
            // asset "7a501bd"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7a501bd") : $this->env->getExtension('assets')->getAssetUrl("js/7a501bd.js");
            echo "            <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
            ";
        }
        unset($context["asset_url"]);
        // line 53
        echo "        ";
    }

    // line 55
    public function block_carga($context, array $blocks = array())
    {
        // line 56
        echo "        ";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:TPV:layoutTPV.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 56,  137 => 55,  117 => 51,  113 => 50,  109 => 48,  106 => 47,  102 => 39,  99 => 38,  83 => 57,  81 => 55,  78 => 54,  76 => 47,  67 => 40,  65 => 38,  49 => 25,  45 => 24,  20 => 1,  396 => 137,  391 => 136,  386 => 135,  381 => 134,  376 => 133,  371 => 132,  365 => 130,  360 => 129,  355 => 128,  350 => 127,  345 => 126,  340 => 125,  335 => 124,  330 => 123,  323 => 119,  316 => 116,  312 => 115,  305 => 112,  301 => 111,  294 => 108,  290 => 107,  283 => 104,  279 => 103,  275 => 101,  268 => 97,  265 => 96,  262 => 95,  256 => 93,  252 => 92,  245 => 89,  241 => 88,  234 => 85,  230 => 84,  223 => 80,  214 => 73,  208 => 70,  205 => 69,  202 => 68,  199 => 67,  193 => 64,  190 => 63,  187 => 62,  184 => 61,  178 => 58,  175 => 57,  172 => 56,  169 => 55,  163 => 52,  160 => 51,  157 => 50,  154 => 49,  148 => 46,  145 => 45,  142 => 44,  139 => 43,  133 => 53,  130 => 39,  127 => 38,  124 => 37,  118 => 34,  115 => 33,  112 => 32,  103 => 27,  96 => 24,  90 => 22,  85 => 21,  80 => 19,  74 => 16,  70 => 15,  66 => 14,  60 => 10,  57 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
