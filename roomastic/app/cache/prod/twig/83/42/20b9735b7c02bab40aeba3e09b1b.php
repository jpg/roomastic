<?php

/* HotelesFrontendBundle:TPV:pago-datos.html.twig */
class __TwigTemplate_834220b9735b7c02bab40aeba3e09b1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"contenedor-contenedor-iframe\">
        <div class=\"contenedor-iframe\">
            <iframe name=\"iframe\" src=\"";
        // line 6
        if (isset($context["firma"])) { $_firma_ = $context["firma"]; } else { $_firma_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("hoteles_frontend_tpv_processpayment", array("firma" => $_firma_)), "html", null, true);
        echo "\" frameborder=\"0\" style=\"overflow:hidden;height: 100%;
                    width:100%\" height=\"123%\" width=\"100%\"></iframe>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:TPV:pago-datos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 6,  29 => 4,  26 => 3,);
    }
}
