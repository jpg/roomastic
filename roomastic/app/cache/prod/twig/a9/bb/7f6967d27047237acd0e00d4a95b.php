<?php

/* HotelesBackendBundle:Faqs:edit.html.twig */
class __TwigTemplate_a9bb7f6967d27047237acd0e00d4a95b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar preguntas frecuentes"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "

<div class=\"row\">
  <div class=\"col-lg-6\">
    <section class=\"panel\"> 
        <header class=\"panel-heading\">
            ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar preguntas frecuentes"), "html", null, true);
        echo "
        </header>
        <div class=\"panel-body\">
            <form role=\"form\" action=\"";
        // line 18
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_faqs_update", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\" method=\"post\" ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_edit_form_);
        echo ">
              
              <div class=\"form-group ";
        // line 20
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "pregunta"))), "html", null, true);
        echo "\">
                  <label for=\"exampleInputEmail1\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pregunta"), "html", null, true);
        echo "</label>
                  <div class=\"\">
                  ";
        // line 23
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "pregunta"));
        echo "
                  ";
        // line 24
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "pregunta"), array("attr" => array("class" => "form-control")));
        echo "                        
                  </div>
              </div>
              <div class=\"form-group ";
        // line 27
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "respuesta"))), "html", null, true);
        echo "\">
                  <label for=\"exampleInputEmail1\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Respuesta"), "html", null, true);
        echo "</label>
                  <div class=\"\">
                  ";
        // line 30
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "respuesta"));
        echo "
                  ";
        // line 31
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "respuesta"), array("attr" => array("class" => "form-control")));
        echo "                        
                  </div>
              </div>
               ";
        // line 34
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_edit_form_);
        echo "
        </div>
    </section>
    <!-- btns -->
    <section class=\"panel\">
          <div class=\"panel-body\">
                <button type=\"submit\" class=\"btn btn-info\">";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>   
            </form><!-- / form edit -->
               ";
        // line 43
        echo "               <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_faqs"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
              <form action=\"";
        // line 44
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_faqs_delete", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\" method=\"post\" class=\"btnDelete\">
                ";
        // line 45
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($_delete_form_);
        echo "
                <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('¿Está seguro de borrar?');\">";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Borrar"), "html", null, true);
        echo "</button>
            </form>
          </div>
    </section>
    <!-- /btns -->
  </div><!-- /col -->
</div><!-- /row -->


";
        // line 81
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Faqs:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 81,  143 => 46,  138 => 45,  133 => 44,  126 => 43,  121 => 40,  111 => 34,  104 => 31,  99 => 30,  94 => 28,  89 => 27,  82 => 24,  77 => 23,  72 => 21,  67 => 20,  58 => 18,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
