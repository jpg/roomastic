
$(document).ready(function() {
    $('#hotelSelect').on('change', function(evt) {

        window.location.href = Routing.generate('admin_ofertas_filtradas', {filtro: ROOMASTIC.filtro_ofertas.filtro, id_hotel: evt.target.value});
    });
    $('#ver-detalles-oferta').on('click', function(evt) {
        var href = evt.target.href;
        evt.preventDefault();
        $.ajax({url: href, method: 'GET'})
                .done(function(data) {
                    for (var propName in data) {
                        $('#' + propName).text(data[propName]);
                    }
                    console.log(data);
                }).error(function(data) {
                    console.log(data);
        });


    });
});