/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
    var getCounters;
    getCounters = function getCounters() {
        $.ajax({
            url: Routing.generate('hoteles_backend_notificacion_updatecounters'),
            method: 'GET',
            cache: false
        }).done(function(data) {
            var html_badge_notificaciones;
            if (data.hasOwnProperty('sin_notificaciones') === false) {
                for (var propName in data) {
                    $('.' + propName).text(data[propName]);
                }
                if (data.hasOwnProperty('total_notificaciones') && data['total_notificaciones'] !== 0) {
                    html_badge_notificaciones = '<span class="badge bg-important">' + data['total_notificaciones'] + '</span>';
                    $('.medalla_notificaciones').append(html_badge_notificaciones);
                }
            }
        });
    };
    setInterval(getCounters, 60 * 1000);
});