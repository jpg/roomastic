if (typeof ROOMASTIC === 'undefined') {
    ROOMASTIC = {};
}

$(document).ready(function() {

    ROOMASTIC.form_index = $('#form-destino-fecha')
            .validate({
                onkeyup: false,
                focusCleanup: true,
                errorClass: "error",
                validClass: "valid",
                ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
                highlight: function(element, errorClass, validClass) {
                    $(element).addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).addClass(validClass).removeClass(errorClass);
                },
                rules: {
                    ciudad: {
                        required: function(elem) {
                            if ($(elem).val().length === 0) {
                                ROOMASTIC.select_ciudad[0].selectize.open();
                            }
                            return true;
                        }
                    },
                    fecha: {
                        required: function(elem) {
                            if ($(elem).val().length === 0) {
                                ROOMASTIC.daterangepicker.click();
                            }
                            return true;

                        }
                    }
                },
                messages: {
                    ciudad: "",
                    fecha: "",
                }
            });
    $('#buscar').click(function(evt) {
        var lugar = $(".selec_ciudad option:selected").val();
        var fecha = $(".select_fecha").val();
        if ($('#form-destino-fecha').valid()) {
            fecha = fecha.replace(new RegExp("/", "g"), "-");
            var res = fecha.split(" - ");
            if (env === 'dev') {
                var url = 'app_dev.php/resultados/' + lugar + '/' + res[0] + '/' + res[1];
            } else {
                var url = '/resultados/' + lugar + '/' + res[0] + '/' + res[1];
            }
            //var url = Routing.generate('HotelesFrontendBundle_listado', { lugar: lugar, fechain: res[0], fechaout: res[1] });
            window.location.href = url
        }
        evt.preventDefault();
    });
});