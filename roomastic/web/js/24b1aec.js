/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var map_array = {};
var a;
$(document).ready(function() {
    a = function(self) {
        self.anchor.fancybox();
    };
    $(".pikame_listado").each(function() {
        $(this).PikaChoose({
            buildFinished: a,
            autoPlay: false});
    });
    function initialize() {
        $('.mapa').each(function(index, mapa_elem) {
            mapa_elem_long = mapa_elem.dataset.longitud;
            mapa_elem_lat = mapa_elem.dataset.latitud;
            mapa_elem_title = mapa_elem.dataset.titulo;
            hotel_id = mapa_elem.dataset.hotelId;
            latlng = new google.maps.LatLng(mapa_elem_lat, mapa_elem_long);
            var mapOptions = {
                zoom: 15,
                center: latlng //center: new google.maps.LatLng(mapa_elem_lat, mapa_elem_long)
            };
            map = new google.maps.Map(mapa_elem,
                    mapOptions);
            map_array['mapa_hotel_' + hotel_id ] = map;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: mapa_elem_title
            });
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    mapa = $('.single_hotel .abrir_map').closest('.single_hotel').find('.desplegable_mapa').first();
    detalles = $('.single_hotel .detalle').closest('.single_hotel').find('.desplegable').first();
    $('.wrapper_hoteles').on('click', '.detalle', function(evt) {

        var detalles = $(this).closest('.single_hotel').find('.desplegable');
        if (detalles.is(':visible')) {

            detalles.toggle(400);
            mapa.hide(400);
        } else {

            $('.desplegable').hide(400);
            $('.desplegable_mapa').hide(400);
            detalles.toggle(400);
        }
        evt.preventDefault();
    });
    /*
     * Esta funcion se encarga de cambiar la imagen cuando haces click en una de ellas en el listado
     * Esto es debido a que pikachoose no funciona del todo bien con multiples instancias 
     * en la misma página
     * 
     */
    $('.wrapper_hoteles').on('click', '.thumbnail-pikachoose', function(evt) {
        var pikameId, photoIndex;
        pikameId = evt.target.dataset.pikameId;
        photoIndex = evt.target.dataset.photoIndex;
        $('#' + pikameId).data('pikachoose').GoTo(photoIndex);
        evt.preventDefault();
    });
    $('.wrapper_hoteles').on('click', '.abrir_map', function(evt) {
        var mapa = $(this).closest('.single_hotel').find('.desplegable_mapa');
        if (mapa.is(':visible')) {
            mapa.toggle(400);
        } else {
            $('.desplegable').hide(400);
            $('.desplegable_mapa').hide(400);
            mapa.toggle(400, function() {
                $dataset = mapa.find('.mapa').data();
                google_map = map_array['mapa_hotel_' + $dataset.hotelId];
                var center = google_map.getCenter();
                google.maps.event.trigger(google_map, 'resize');
                google_map.setCenter(center);
            });
        }
        evt.preventDefault();
    });
    $('.selectize-input input').blur();
    $('.ajaxupdate').change(function(event) {
        confirm("¿Desea cambiar su lugar de busqueda?");
        var lugar = $(".selec_ciudad option:selected").val();
        var fecha = $(".select_fecha").val();
        if (lugar !== '' && fecha !== '') {
            fecha = fecha.replace(new RegExp("/", "g"), "-");
            var res = fecha.split(" - ");
            var url = '/resultados/' + lugar + '/' + res[0] + '/' + res[1];
            //var url = Routing.generate('HotelesFrontendBundle_listado', { lugar: lugar, fechain: res[0], fechaout: res[1] });
            window.location.href = url;
        }
        event.preventDefault();
    });
    $('.checkbox-servicios').on('ifUnchecked', function(event) {
        actualizarFiltroServicios(event.target, 'del');
        applyFilters();
    });
    $('.checkbox-servicios').on('ifChecked', function(event) {
        actualizarFiltroServicios(event.target, 'add');
        applyFilters();
    });
    $('.checkbox-servicios').each(function(index, elem) {
        actualizarFiltroServicios(elem, 'add');
    }).promise().done(applyFilters);
    $('.checkbox-stars').on('ifUnchecked', function(event) {
        actualizarFiltroEstrellas(event.target, 'del');
        applyFilters();
    });
    $('.checkbox-stars').on('ifChecked', function(event) {
        actualizarFiltroEstrellas(event.target, 'add');
        applyFilters();
    });
    $('.checkbox-stars').each(function(index, elem) {
        actualizarFiltroEstrellas(elem, 'add');
    }).promise().done(applyFilters);
});
function unique(element, index, array) {
    return array.indexOf(element) === index;
}
var filtro_servicios = '';
var filtro_servicios_array = [];
function actualizarFiltroServicios(elem, tipo) {
    nuevo_filtro = elem.dataset.filter;
    if ('add' === tipo) {
        filtro_servicios_array.push('.' + nuevo_filtro);
        filtro_servicios_array = filtro_servicios_array.filter(unique);
    } else if ('del' === tipo) {
        index = filtro_servicios_array.indexOf('.' + nuevo_filtro);
        if (index > -1) {
            filtro_servicios_array.splice(index, 1);
        }
    }
    filtro_servicios = filtro_servicios_array.length > 0 ? filtro_servicios_array.join(",") : '';
}

var filtro_estrellas = '';
var filtro_estrellas_array = [];
function actualizarFiltroEstrellas(elem, tipo) {
    nuevo_filtro = elem.dataset.filter;
    if ('add' === tipo) {
        filtro_estrellas_array.push('.' + nuevo_filtro);
        filtro_estrellas_array = filtro_estrellas_array.filter(unique);
    } else if ('del' === tipo) {
        index = filtro_estrellas_array.indexOf('.' + nuevo_filtro);
        if (index > -1) {
            filtro_estrellas_array.splice(index, 1);
        }
    }
    filtro_estrellas = filtro_estrellas_array.length > 0 ? filtro_estrellas_array.join(",") : '';
}

function applyFilters() {

    $('.wrapper_hoteles .single_hotel').show();
    $('.wrapper_hoteles .single_hotel').not(filtro_estrellas).hide();
    $('.wrapper_hoteles .single_hotel').not(filtro_servicios).hide();
}

var procesando = false;
//Este evento lo uso para hacer la llamada al servidor
$(window).scroll(function() {
    var wintop = $(window).scrollTop(), docheight = $(document).height(), winheight = $(window).height();
    var scrolltrigger = 0.80;
    if ((wintop / (docheight - winheight)) > scrolltrigger) {
        if (typeof last_page !== 'undefined' && last_page === false && procesando === false) {
            cargarHotelesAjax();
        }
    }
});
procesando = false;
last_page = false;
function cargarHotelesAjax() {
    var url = $('#formajax').attr('action'); // El script a dónde se realizará la petición.
    //datos_formulario = $("#formajax").serializeArray();

    numero_pagina = $('#infinite-scroll-ajax').data().lastPage + 1;
    datos = [];
    datos.push({name: "numero_pagina", value: numero_pagina});
    //Le pido al servidor los nuevos hoteles
    if (false === procesando) {
        procesando = true;
        $.ajax({
            type: "POST",
            url: url,
            data: $.param(datos), // Adjuntar los campos del formulario enviado.
            success: function(data) {
                if (data.ultima_pagina === false) {
                    //Actualizo el numero de pagina que he pedido
                    $('#infinite-scroll-ajax').data().lastPage = $('#infinite-scroll-ajax').data().lastPage + 1;
                    //Adjunto los nuevos hoteles al listado
                    $html_listado = $(data.html);
                    //$html_listado.find('input.timbre').iCheck(timbreCheckboxConfig);
                    $html_listado.not(filtro_estrellas).hide();
                    $html_listado.not(filtro_servicios).hide();
                    //Aniado mapas y los centro en ese punto
                    $html_listado.find('.mapa').each(function(index, mapa_elem) {
                        //Cargo el mapa de cada uno de los nuevos elementos
                        mapa_elem_long = mapa_elem.dataset.longitud === "" ? 0 : mapa_elem.dataset.longitud;
                        mapa_elem_lat = mapa_elem.dataset.latitud === "" ? 0 : mapa_elem.dataset.latitud;
                        mapa_elem_title = mapa_elem.dataset.titulo;
                        hotel_id = mapa_elem.dataset.hotelId;
                        latlng = new google.maps.LatLng(mapa_elem_lat, mapa_elem_long);
                        var mapOptions = {
                            zoom: 15,
                            center: latlng //center: new google.maps.LatLng(mapa_elem_lat, mapa_elem_long)
                        };
                        map = new google.maps.Map(mapa_elem,
                                mapOptions);
                        map_array['mapa_hotel_' + hotel_id ] = map;
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map,
                            title: mapa_elem_title
                        });
                    });
                    //Fancy box 
                    $html_listado.find(".pikame_listado").each(function() {
                        $(this).PikaChoose({
                            buildFinished: a,
                            autoPlay: false});
                    });
                    $html_lateral = $(data.html_lateral);
                    $html_lateral.find('input.checkbox-lateral').iCheck(checkboxLateralConfig);
                    $(".wrapper_hoteles").append($html_listado.html());
                    $("#listado-lateral-hoteles").append($html_lateral.html());
                    //applyFilters();
                    $("input.timbre").iCheck(timbreCheckboxConfig);
                    //$('input.checkbox-lateral').iCheck(checkboxLateralConfig);
                    configEventsTimbre();
                    procesando = false;
                } else {
                    ultima_pagina = true;
                }
            }, error: function(data) {
                procesando = false;
            }
        });
    }
    return false;
}

if (typeof ROOMASTIC === 'undefined') {
    ROOMASTIC = {};
}

timbreCheckboxConfig = {
    checkboxClass: 'icheckbox_timbre',
    radioClass: 'iradio_minimal',
    //increaseArea: '20%' // optional
};
checkboxLateralConfig = {
    checkboxClass: 'icheckbox_minimal',
    radioClass: 'iradio_minimal'

}

var configEventsTimbre = function() {

};

$(function() {
    //Elimino la cache para las llamadas ajax
    $.ajaxSetup({
        cache: false
    });


    $('#hacer-oferta').on('click', function(evt) {
        //Pillo las id's de los hoteles
        //$hoteles_seleccionados = $('.timbre:checked');/
        $hoteles_seleccionados = $.makeArray($('.timbre:checked')).map(
                function(hotel) {
                    return hotel.dataset.hotelId;
                }).join();

        //Pillo las fechas
        $fecha_inicio = $('#reservation').data().daterangepicker.startDate.format('DD/MM/YY');
        $fecha_fin = $('#reservation').data().daterangepicker.endDate.format('DD/MM/YY');
        location.href = Routing.generate('HotelesFrontendBundle_oferta', {hoteles: $hoteles_seleccionados, fecha_inicio: $fecha_inicio, fecha_fin: $fecha_fin});

    });
    ROOMASTIC.daterangepicker = $('#reservation').daterangepicker({});

    $(document).on('click', 'td', function(evt) {
        //Usar esto $('#reservation').data().daterangepicker.startDate para sacar la fecha de inicio y final
        $input_fecha_element = $('#reservation');
        if ($input_fecha_element.length === 0) {
            $input_fecha_element = $('#hoteles_backendbundle_ofertatype_fecha');
        }
        $fecha_inicio = $input_fecha_element.data().daterangepicker.startDate.format('DD/MM/YY');
        $fecha_fin = $input_fecha_element.data().daterangepicker.endDate.format('DD/MM/YY');

        if ($('#hoteles_backendbundle_ofertatype_fecha').length !== 0) {
            $('#hoteles_backendbundle_ofertatype_numnoches').val(dateDiff($input_fecha_element.data().daterangepicker.startDate.format('MM/DD/YY'), $input_fecha_element.data().daterangepicker.endDate.format('MM/DD/YY')).d);
            calcularPrecio();
        }
        $input_fecha_element.val($fecha_inicio + ' - ' + $fecha_fin);
    });
    $('#reservation').on('apply.daterangepicker', function(ev, picker) {

        var anoInicio = picker.startDate.format('YYYY');
        var mesInicio = picker.startDate.format('MM')
        var diaInicio = picker.startDate.format('DD')

        var anoFinal = picker.endDate.format('YYYY');
        var mesFinal = picker.endDate.format('MM');
        var diaFinal = picker.endDate.format('DD');

        var b = moment([anoInicio, mesInicio, diaInicio]);
        var a = moment([anoFinal, mesFinal, diaFinal]);
        a.diff(b, 'days'); // 1

        var difDias = a.diff(b, 'days');

        $('.caja_noches input').val(difDias);

        var valor = $('.caja_noches input').val(difDias);

        // Actualizar Total

        var maxPrice = Number($('input[name=precio]').val());
        var nNoches = valor.val();
        var nPersonas = Number($('input[name=adultos]').val());
        var nHabs = Number($('input[name=habs]').val());

        var valor = maxPrice * nNoches * nPersonas * nHabs;

        $('input[name=precio_total]').val(valor);

    });


    $('#hoteles_backendbundle_ofertatype_fecha').on('apply.daterangepicker', function(ev, picker) {

        var anoInicio = picker.startDate.format('YYYY');
        var mesInicio = picker.startDate.format('MM')
        var diaInicio = picker.startDate.format('DD')

        var anoFinal = picker.endDate.format('YYYY');
        var mesFinal = picker.endDate.format('MM');
        var diaFinal = picker.endDate.format('DD');

        /*console.log(anoInicio+mesInicio+diaInicio);
         console.log(anoFinal+mesFinal+diaFinal);*/

        var b = moment([anoInicio, mesInicio, diaInicio]);
        var a = moment([anoFinal, mesFinal, diaFinal]);
        a.diff(b, 'days'); // 1

        var difDias = a.diff(b, 'days');

        $('.caja_noches input').val(difDias);

        var valor = $('.caja_noches input').val(difDias);

        // Actualizar Total

        var maxPrice = Number($('input[name=precio]').val());
        var nNoches = valor.val();
        var nPersonas = Number($('input[name=adultos]').val());
        var nHabs = Number($('input[name=habs]').val());

        var valor = maxPrice * nNoches * nPersonas * nHabs;

        $('input[name=precio_total]').val(valor);

    });


// Calcular precio Final

    var maxPrice = Number($('input[name=precio]').val());
    var nNoches = Number($('input[name=n_noches]').val());
    var nPersonas = Number($('input[name=adultos]').val());
    var nHabs = Number($('input[name=habs]').val());


    $('input[name=precio], input[name=n_noches], input[name=adultos], input[name=habs]').change(function() {

        var maxPrice = Number($('input[name=precio]').val());
        var nNoches = Number($('input[name=n_noches]').val());
        var nPersonas = Number($('input[name=adultos]').val());
        var nHabs = Number($('input[name=habs]').val());


        var valor = maxPrice * nNoches * nPersonas * nHabs;

        $('input[name=precio_total]').val(valor);
    });


    ROOMASTIC.selectize_options = {
        create: false,
        openOnFocus: false,
        maxOptions: 5,
        sortField: {
            field: 'text',
            direction: 'asc'
        },
        dropdownParent: 'body'
    };

    ROOMASTIC.select_ciudad = $('.selec_ciudad').selectize(ROOMASTIC.selectize_options);

    $('.selectize-input input').delay(1700).focus();



    var viewportHeight = $(window).height();
    var footerHeight = $('#footer').height();

    if ((viewportHeight) < 792) {

        var viewportHeight = $(window).height();

        $('.col_izq').css('min-height', viewportHeight - footerHeight - 5);

    } else {

        var viewportHeight = $('.col_der').height();

        $('.col_izq').css('min-height', viewportHeight);

    }
    ;

    $(window).resize(function() {
        if ((viewportHeight) < 792) {
            var viewportHeight = $(window).height();
            $('.col_izq').css('min-height', viewportHeight - footerHeight - 5);
        } else {
            var viewportHeight = $('.col_der').height();
            $('.col_izq').css('min-height', viewportHeight);
        }
        ;
    });



    $('input.checkbox-lateral').iCheck(checkboxLateralConfig);
    checkboxOfertaConfig = checkboxLateralConfig;
    $('input.checkbox-oferta').iCheck(checkboxOfertaConfig);
    $('input.timbre').iCheck(timbreCheckboxConfig);

    function ocultarLlamarTimbre() {
        $('.hacer_oferta').hide(400);
        $('.menu_int').css('right', '0px');
        $('.lineatop').css({
            position: 'relative',
            minWidth: '1020px',
            marginBottom: '0px',
        });
        $('.wrapper').css({
            marginTop: 'auto',
        });
    }
    function mostrarLlamarTimbre() {
        if (!$('.hacer_oferta').is(':visible')) {
            $('.hacer_oferta').show(400);
            $('.menu_int').css('right', '160px');
            $('.hacer_oferta').addClass('posFixed');
            $('.lineatop').css({
                position: 'fixed',
                width: '100%',
                top: '0',
            });
            $('.wrapper').css({
                marginTop: '5px',
            });
        }
    }

    $(document).on('ifChecked', 'input.timbre', function(evt) {
        mostrarLlamarTimbre();
    });
    $(document).on('ifUnchecked', 'input.timbre', function(evt) {
        //No se usa el 0, porque la clase checked esta todavía puesta en este evento
        if ($('.icheckbox_timbre.checked').length === 1) {
            ocultarLlamarTimbre();
        }
    });
    if ($('input.timbre:checked').length >= 1) {
        mostrarLlamarTimbre();
    }

    $('.hotel-selected input[type=checkbox]').on('ifUnchecked', function(evt) {
        $li_padre = $(this).closest('.hotel-selected');
        $li_padre.hide();
        $hotelId = $li_padre.data().hotelId;
        $('#hotel-summary-' + $hotelId + ' .timbre').iCheck('uncheck');
    });

    //Este código hay que hacerlo inmune a los cambios que hay por AJAX
    //Hay que reestructurar el código o la manera en la que se hace lo del ajax
    $('.wrapper_hoteles').on('ifClicked', '.timbre', function(evt) {
        $hotel_id = evt.target.dataset.hotelId;
        // Busco el checkbox que tengo que mostrar
        $('#hotel-checkbox-' + $hotel_id).show();
        $('#hotel-checkbox-' + $hotel_id + ' input[type=checkbox]').iCheck('check');

    });

    $('.wrapper_hoteles').on('ifUnchecked', '.timbre', function(evt) {
        $hotel_id = evt.target.dataset.hotelId;
        // Busco el checkbox que tengo que mostrar
        $('#hotel-checkbox-' + $hotel_id).hide();
        $('#hotel-checkbox-' + $hotel_id + ' input[type=checkbox]').iCheck('uncheck');

    });

    //fancybox - vídeohome
    if ($("#fancyhome").length > 0) {
        $("#fancyhome").fancybox();
    }

    //cookies
    $.cookieBar({});

}); // Fin Document Ready


$(document).ready(function() {

    $(document).ajaxStart(function() {
        $('#modal').show();
        $('#fade').show();

    });
    $(document).ajaxStop(function() {
        $('#modal').hide();
        $('#fade').hide();
    });

});