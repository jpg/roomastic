if (typeof ROOMASTIC === 'undefined') {
    ROOMASTIC = {};
}

ROOMASTIC.urlSalir = Routing.generate('fos_user_security_logout');

function dateDiff(str1, str2) {
    var diff = Date.parse(str2) - Date.parse(str1);
    return isNaN(diff) ? NaN : {
        diff: diff,
        ms: Math.floor(diff % 1000),
        s: Math.floor(diff / 1000 % 60),
        m: Math.floor(diff / 60000 % 60),
        h: Math.floor(diff / 3600000 % 24),
        d: Math.floor(diff / 86400000)
    };
}
function validarOferta() {
    $form_oferta = $('#formoferta');
    if ($form_oferta.valid()) {
        return true;
    } else {
        $(ROOMASTIC.form_oferta.invalidElements()[0]).focus();
        if ($('#acepto1').valid() === false) {
            sweetAlert("Vaya...", "Tienes que aceptar nuestras condiciones antes de continuar!", "error");
        }
        return false;
    }
}
function showRegisterExtra() {
    $('.body').hide();
    $('.bodyregisterextra').show();
    $('.modalregister').show('fast');
}
function showLogin() {
    $('.bodyregister').hide();
    $('.body').show();
    $('.modalregister').show('fast');
    $('.clearForm').val('');
}
function showRegisterWeb() {
    $('.body').hide();
    $('.bodyregister').show();
    $('.modalregister').show('fast');
}
function hideRegLogModal() {
    $('.modalregister').hide();
    $('.bodyregister').hide();
    $('.body').show();
    $('.clearForm').val('');
}
function canMakeOffer() {
    if (typeof ROOMASTIC.user_data === 'undefined') {
        showLogin();
        return false;
    } else {
        if (ROOMASTIC.user_data['canOffer'] === 'si') {
            //Cambio el estilo del boton de enviar oferta
            //Intento hacer la oferta
            $('.btn_oferta').attr('title', 'Ya puedes hacer tu oferta');
            ////html('<a class="btn_oferta" href="#" id="submitoffer" title="" style="background:red!important" >hacer oferta</a>');
            return true;

        } else if (ROOMASTIC.user_data['canOffer'] === 'faltan_datos') {
            //Si no puedo muestro el modal de registro extra al que le precargo los datos que puedo
            showRegisterExtra();
            return false;

        } else if (ROOMASTIC.user_data['canOffer'] === 'no') {
            swal('Tu usuario no puede hacer ofertas :S');
            return false;
        } else if (ROOMASTIC.user_data['canOffer'] === 'oferta_realizada_pendiente') {
            swal('Tu usuario tiene una oferta pendiente');
            return false;
        }
    }
}

function hacerOferta() {
    if (validarOferta() && canMakeOffer()) {
        $form_oferta.submit();
        hideRegLogModal();
    } else {
        console.log('Oferta NO VALIDA');
    }
}
function logout() {
    $.ajax({
        url: Routing.generate('HotelesFrontendBundle_logout'),
        type: "GET",
        success: function(data) {
            console.log('Logout');
        },
        error: function(data) {
            console.log('Vaya, ha habido un error :S');
        }
    });
}
function calcularPrecio() {

    if (/^(\d+)$/.test($('#hoteles_backendbundle_ofertatype_preciohabitacion').val()) === false) {
        $('#hoteles_backendbundle_ofertatype_preciototaloferta').val(0);
        return;
    }

    var precio_por_noche = parseFloat($('#hoteles_backendbundle_ofertatype_preciohabitacion').val().replace('.', '').replace(' ', '').replace(',', '.'));

    if (precio_por_noche > 0 && $('#hoteles_backendbundle_ofertatype_numnoches').val() > 0) {
        $resultado = precio_por_noche * $('#hoteles_backendbundle_ofertatype_numnoches').val() * (
                parseInt($('#hoteles_backendbundle_ofertatype_numadultos').val()) +
                parseInt($('#hoteles_backendbundle_ofertatype_numninos').val())) *
                (parseInt($('#hoteles_backendbundle_ofertatype_numhabitaciones').val()));
        if (isNaN($resultado)) {
            $('#hoteles_backendbundle_ofertatype_preciototaloferta').val(0);
        } else {

            $resultado = $resultado.toLocaleString('es', {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            });
            $('#hoteles_backendbundle_ofertatype_preciototaloferta').val($resultado);
        }
    } else {
        $('#hoteles_backendbundle_ofertatype_preciototaloferta').val(0);
    }
}
$(document).ready(function() {

    $('#hoteles_backendbundle_ofertatype_preciohabitacion').on('keyup', calcularPrecio);
    $('#hoteles_backendbundle_ofertatype_numninos').on('keyup', calcularPrecio);
    $('#hoteles_backendbundle_ofertatype_numadultos').on('keyup', calcularPrecio);
    $('#hoteles_backendbundle_ofertatype_numhabitaciones').on('keyup', calcularPrecio);
    $('#hoteles_backendbundle_ofertatype_fecha').daterangepicker({});
    $(document).on('click', 'td', function(evt) {
        //Usar esto $('#reservation').data().daterangepicker.startDate para sacar la fecha de inicio y final
        $input_fecha_element = $('#reservation');
        if ($input_fecha_element.length === 0) {
            $input_fecha_element = $('#hoteles_backendbundle_ofertatype_fecha');
        }
        $fecha_inicio = $input_fecha_element.data().daterangepicker.startDate.format('DD/MM/YY');
        $fecha_fin = $input_fecha_element.data().daterangepicker.endDate.format('DD/MM/YY');

        if ($('#hoteles_backendbundle_ofertatype_fecha').length !== 0) {
            //console.log(dateDiff($input_fecha_element.data().daterangepicker.startDate.format('MM/DD/YY'), $input_fecha_element.data().daterangepicker.endDate.format('MM/DD/YY')));
            $('#hoteles_backendbundle_ofertatype_numnoches').val(dateDiff($input_fecha_element.data().daterangepicker.startDate.format('MM/DD/YY'), $input_fecha_element.data().daterangepicker.endDate.format('MM/DD/YY')).d);
            calcularPrecio();
        }
        $input_fecha_element.val($fecha_inicio + ' - ' + $fecha_fin);
    });

});

jQuery.validator.addMethod("notEqual", function(value, element, param) {
    return this.optional(element) || value != param;
}, "");
jQuery.validator.addMethod("numeroEntero", function(value, element) {
    return this.optional(element) || /^(\d+)$/.test(value);
}, "");
jQuery.validator.addMethod("atLeastChecked", function(value, element, param) {
    return $(element).closest('form').find('input[type=checkbox]:checked').length > param;
});

$(document).ready(function() {
    ROOMASTIC.form_oferta = $('#formoferta').validate({
        onkeyup: false,
        focusCleanup: true,
        errorClass: "error",
        validClass: "valid",
        highlight: function(element, errorClass, validClass) {
            $(element).addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).addClass(validClass).removeClass(errorClass);
        },
        rules: {
            "hoteles_backendbundle_ofertatype[preciohabitacion]": {
                required: true,
                numeroEntero: true
            },
            "hoteles_backendbundle_ofertatype[numnoches]": {
                required: true,
                min: 1
            },
            "hoteles_backendbundle_ofertatype[numadultos]": {
                required: true,
                min: 1
            },
            "hoteles_backendbundle_ofertatype[numhabitaciones]": {
                required: true,
                min: 1
            },
            "hoteles_backendbundle_ofertatype[preciototaloferta]": {
                required: true,
                notEqual: "0"
            },
            "acepto_condiciones": {
                required: true
            },
            "hoteles[id][]": {
                atLeastChecked: 1
            }
        },
        messages: {
            'hoteles_backendbundle_ofertatype[preciohabitacion]': {
                required: '',
                min: ''
            },
            'hoteles_backendbundle_ofertatype[numnoches]': {
                required: '',
                min: ''
            },
            'hoteles_backendbundle_ofertatype[numadultos]': {
                required: '',
                min: ''
            },
            'hoteles_backendbundle_ofertatype[numhabitaciones]': {
                required: '',
                min: ''
            },
            'hoteles_backendbundle_ofertatype[preciototaloferta]': {
                required: '',
                min: ''
            },
            "hoteles[id][]": {
                atLeastChecked: ''
            },
            "acepto_condiciones": {
                required: ''
            }
        }
    });

    $('.btn_oferta').on('click', function(evt) {
        evt.preventDefault();
        if (validarOferta()) {
            $.ajax({
                url: Routing.generate('HotelesFrontendBundle_compruebaloginuserrss'),
                type: "GET",
                success: function(data) {
                    ROOMASTIC.user_data = data.user_data;
                    hacerOferta();
                },
                error: function(data) {
                    //$('.body').hide();
                    //$('.bodyregisterextra').show();
                    $('.modalregister').show('fast');
                }
            });
        }
    });

    $('.showregister').click(function(evt) {
        evt.preventDefault();
        showRegisterWeb();
    });
    $('.showlogin').click(function(evt) {
        evt.preventDefault();
        showLogin();
    });
    $('.close').click(function(evt) {
        evt.preventDefault();
        hideRegLogModal();
    });

    $('#formlogin').submit(function(evt) {
        evt.preventDefault();
        var url = $('#formlogin').attr('action');
        $.ajax({
            type: "POST",
            url: url,
            data: $("#formlogin").serialize(),
            success: function(data) {
                //Hago una llamada para ver si puedo hacer oferta, si puedo intento hacer el submit
                if (data.hasOwnProperty('success') && data.success === true) {
                    if (data.hasOwnProperty('oferta_form_token')) {
                        $('#hoteles_backendbundle_ofertatype__token_oferta').val(data.oferta_form_token);
                    }
                    ROOMASTIC.user_data = data.user_data;
                    hacerOferta();
                }
            },
            error: function(data) {
                $('.errorlogin').html('<p style="position:absolute; background-color:#fff; top:16px;padding: 3px 10px; border-radius: 3px; color: #000;">Usuario o pass incorrectas</p>');
            }
        });
        return false;
    });

    $('#formregister').submit(function(evt) {
        evt.preventDefault();
        $.ajax({
            type: "POST",
            url: Routing.generate('HotelesFrontendBundle_registroajax'),
            data: $("#formregister").serialize(),
            success: function(data) {
                if (data.hasOwnProperty('registertype__token')) {
                    $('#hoteles_backendbundle_registertype__token').val(data.registertype__token);
                }
                ROOMASTIC.user_data = data.user_data;
                hacerOferta();
            },
            error: function(data) {
                var message = '';
                if (data.responseJSON.hasOwnProperty('email')) {
                    message += 'Vaya, parece que ese e-mail esta ya en uso';
                }
                if (data.responseJSON.hasOwnProperty('dni')) {
                    message += '\n Vaya, parece que ya hay alguien registrado con tus datos';
                }
                message === '' ? swal(data) : swal(message);
            }
        });
    });

    $('#formregisterextra').submit(function(evt) {
        evt.preventDefault();
        $.ajax({
            type: "POST",
            url: Routing.generate('HotelesFrontendBundle_registroextraajax'),
            data: $("#formregisterextra").serialize(),
            success: function(data) {
                ROOMASTIC.user_data = data.user_data;
                hacerOferta();
            },
            error: function(data) {
                var message = '';
                if (data.responseJSON.hasOwnProperty('email')) {
                    message += 'Vaya, parece que ese e-mail esta ya en uso';
                }
                if (data.responseJSON.hasOwnProperty('dni')) {
                    message += '\n Vaya, parece que ya hay alguien registrado con tus datos';
                }
                swal(message);
            }
        });
    });

});