timbreCheckboxConfig = {
    checkboxClass: 'icheckbox_timbre',
    radioClass: 'iradio_minimal',
    //increaseArea: '20%' // optional
};
var configEventsTimbre = function() {

};

$(function() {
    //Elimino la cache para las llamadas ajax
    $.ajaxSetup({
        cache: false
    });
    $('#buscar').click(function() {
        var lugar = $(".selec_ciudad option:selected").val();
        var fecha = $(".select_fecha").val();
        if (lugar !== '' && fecha !== '') {
            fecha = fecha.replace(new RegExp("/", "g"), "-");
            var res = fecha.split(" - ");
            if (env === 'dev') {
                var url = 'app_dev.php/resultados/' + lugar + '/' + res[0] + '/' + res[1];
            } else {
                var url = '/resultados/' + lugar + '/' + res[0] + '/' + res[1];
            }
            //var url = Routing.generate('HotelesFrontendBundle_listado', { lugar: lugar, fechain: res[0], fechaout: res[1] });
            window.location.href = url
        }
        return false;
    });

    $('#hacer-oferta').on('click', function(evt) {
        //Pillo las id's de los hoteles
        //$hoteles_seleccionados = $('.timbre:checked');/
        $hoteles_seleccionados = $.makeArray($('.timbre:checked')).map(
                function(hotel) {
                    return hotel.dataset.hotelId;
                }).join();

        //Pillo las fechas
        $fecha_inicio = $('#reservation').data().daterangepicker.startDate.format('DD/MM/YY');
        $fecha_fin = $('#reservation').data().daterangepicker.endDate.format('DD/MM/YY');
        location.href = Routing.generate('HotelesFrontendBundle_oferta', {hoteles: $hoteles_seleccionados, fecha_inicio: $fecha_inicio, fecha_fin: $fecha_fin});

    });
    $('#reservation').daterangepicker({
    });

    $(document).on('click', 'td', function(evt) {
        //Usar esto $('#reservation').data().daterangepicker.startDate para sacar la fecha de inicio y final
        $input_fecha_element = $('#reservation');
        if ($input_fecha_element.length === 0) {
            $input_fecha_element = $('#hoteles_backendbundle_ofertatype_fecha');
        }
        $fecha_inicio = $input_fecha_element.data().daterangepicker.startDate.format('DD/MM/YY');
        $fecha_fin = $input_fecha_element.data().daterangepicker.endDate.format('DD/MM/YY');

        if ($('#hoteles_backendbundle_ofertatype_fecha').length !== 0) {
            //console.log(dateDiff($input_fecha_element.data().daterangepicker.startDate.format('MM/DD/YY'), $input_fecha_element.data().daterangepicker.endDate.format('MM/DD/YY')));
            $('#hoteles_backendbundle_ofertatype_numnoches').val(dateDiff($input_fecha_element.data().daterangepicker.startDate.format('MM/DD/YY'), $input_fecha_element.data().daterangepicker.endDate.format('MM/DD/YY')).d);
            calcularPrecio();
        }
        $input_fecha_element.val($fecha_inicio + ' - ' + $fecha_fin);
    });
    $('#reservation').on('apply.daterangepicker', function(ev, picker) {

        var anoInicio = picker.startDate.format('YYYY');
        var mesInicio = picker.startDate.format('MM')
        var diaInicio = picker.startDate.format('DD')

        var anoFinal = picker.endDate.format('YYYY');
        var mesFinal = picker.endDate.format('MM');
        var diaFinal = picker.endDate.format('DD');

        /*console.log(anoInicio+mesInicio+diaInicio);
         console.log(anoFinal+mesFinal+diaFinal);*/

        var b = moment([anoInicio, mesInicio, diaInicio]);
        var a = moment([anoFinal, mesFinal, diaFinal]);
        a.diff(b, 'days'); // 1

        var difDias = a.diff(b, 'days');

        $('.caja_noches input').val(difDias);

        var valor = $('.caja_noches input').val(difDias);

        // Actualizar Total

        var maxPrice = Number($('input[name=precio]').val());
        var nNoches = valor.val();
        var nPersonas = Number($('input[name=adultos]').val());
        var nHabs = Number($('input[name=habs]').val());

        var valor = maxPrice * nNoches * nPersonas * nHabs;

        $('input[name=precio_total]').val(valor);

    });


    $('#hoteles_backendbundle_ofertatype_fecha').on('apply.daterangepicker', function(ev, picker) {

        var anoInicio = picker.startDate.format('YYYY');
        var mesInicio = picker.startDate.format('MM')
        var diaInicio = picker.startDate.format('DD')

        var anoFinal = picker.endDate.format('YYYY');
        var mesFinal = picker.endDate.format('MM');
        var diaFinal = picker.endDate.format('DD');

        /*console.log(anoInicio+mesInicio+diaInicio);
         console.log(anoFinal+mesFinal+diaFinal);*/

        var b = moment([anoInicio, mesInicio, diaInicio]);
        var a = moment([anoFinal, mesFinal, diaFinal]);
        a.diff(b, 'days'); // 1

        var difDias = a.diff(b, 'days');

        $('.caja_noches input').val(difDias);

        var valor = $('.caja_noches input').val(difDias);

        // Actualizar Total

        var maxPrice = Number($('input[name=precio]').val());
        var nNoches = valor.val();
        var nPersonas = Number($('input[name=adultos]').val());
        var nHabs = Number($('input[name=habs]').val());

        var valor = maxPrice * nNoches * nPersonas * nHabs;

        $('input[name=precio_total]').val(valor);

    });


// Calcular precio Final

    var maxPrice = Number($('input[name=precio]').val());
    var nNoches = Number($('input[name=n_noches]').val());
    var nPersonas = Number($('input[name=adultos]').val());
    var nHabs = Number($('input[name=habs]').val());


    $('input[name=precio], input[name=n_noches], input[name=adultos], input[name=habs]').change(function() {

        var maxPrice = Number($('input[name=precio]').val());
        var nNoches = Number($('input[name=n_noches]').val());
        var nPersonas = Number($('input[name=adultos]').val());
        var nHabs = Number($('input[name=habs]').val());

        /*console.log( 'Precio ' + maxPrice );
         console.log( 'Noches ' + nNoches );
         console.log( 'Adultos ' + nPersonas );
         console.log( 'Habitaciones ' + nHabs );*/

        var valor = maxPrice * nNoches * nPersonas * nHabs;

        console.log(valor);

        $('input[name=precio_total]').val(valor);
    });



    $('.selec_ciudad').selectize({
        create: false,
        openOnFocus: false,
        maxOptions: 5,
        sortField: {
            field: 'text',
            direction: 'asc'
        },
        dropdownParent: 'body'
    });

    $('.selectize-input input').delay(1700).focus();



    var viewportHeight = $(window).height();
    var footerHeight = $('#footer').height();

    if ((viewportHeight) < 792) {

        var viewportHeight = $(window).height();

        $('.col_izq').css('min-height', viewportHeight - footerHeight - 5);

    } else {

        var viewportHeight = $('.col_der').height();

        $('.col_izq').css('min-height', viewportHeight);

    }
    ;

    $(window).resize(function() {
        if ((viewportHeight) < 792) {

            var viewportHeight = $(window).height();

            $('.col_izq').css('min-height', viewportHeight - footerHeight - 5);

        } else {

            var viewportHeight = $('.col_der').height();
            $('.col_izq').css('min-height', viewportHeight);

        }
        ;
    });



    $('input').iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal',
        //increaseArea: '20%' // optional
    });

    $('input.timbre').iCheck(timbreCheckboxConfig);

    function ocultarLlamarTimbre() {
        $('.hacer_oferta').hide(400);
        $('.menu_int').css('right', '0px');
        $('.lineatop').css({
            position: 'relative',
            minWidth: '1020px',
            marginBottom: '0px',
        });
        $('.wrapper').css({
            marginTop: 'auto',
        });
    }
    function mostrarLlamarTimbre() {
        if (!$('.hacer_oferta').is(':visible')) {
            $('.hacer_oferta').show(400);
            $('.menu_int').css('right', '160px');
            $('.hacer_oferta').addClass('posFixed');
            $('.lineatop').css({
                position: 'fixed',
                width: '100%',
                top: '0',
            });
            $('.wrapper').css({
                marginTop: '5px',
            });
        }
    }

    $(document).on('ifChecked','input.timbre', function(evt) {
        mostrarLlamarTimbre();
    });
    $(document).on('ifUnchecked','input.timbre', function(evt) {
        //No se usa el 0, porque la clase checked esta todavía puesta en este evento
        if ($('.icheckbox_timbre.checked').length === 1) {
            ocultarLlamarTimbre();
        }
    });

    $('.hotel-selected input[type=checkbox]').on('ifUnchecked', function(evt) {
        $li_padre = $(this).closest('.hotel-selected');
        $li_padre.hide();
        $hotelId = $li_padre.data().hotelId;
        $('#hotel-summary-' + $hotelId + ' .timbre').iCheck('uncheck');
    });

    //Este código hay que hacerlo inmune a los cambios que hay por AJAX
    //Hay que reestructurar el código o la manera en la que se hace lo del ajax
    $('.wrapper_hoteles').on('ifClicked', '.timbre', function(evt) {
        $hotel_id = evt.target.dataset.hotelId;
        // Busco el checkbox que tengo que mostrar
        $('#hotel-checkbox-' + $hotel_id).show();
        $('#hotel-checkbox-' + $hotel_id + ' input[type=checkbox]').iCheck('check');

    });

    $('.wrapper_hoteles').on('ifUnchecked', '.timbre', function(evt) {
        $hotel_id = evt.target.dataset.hotelId;
        // Busco el checkbox que tengo que mostrar
        $('#hotel-checkbox-' + $hotel_id).hide();
        $('#hotel-checkbox-' + $hotel_id + ' input[type=checkbox]').iCheck('uncheck');

    });

    $('.estrellas input[type=checkbox]').on('ifUnchecked', function(evt) {
        $stars = $(this).data().stars;
        $('.stars-' + $stars).attr('visibility', 'hidden');
        console.log('Stars fuera: ' + $stars);
    });
    $('.estrellas input[type=checkbox]').on('ifChecked', function(evt) {
        $stars = $(this).data().stars;
        $('.stars-' + $stars).attr('visibility', 'initial');
        $timbre = $('input.timbre');
        $timbre.iCheck({
            checkboxClass: 'icheckbox_timbre',
            radioClass: 'iradio_minimal',
            //increaseArea: '20%' // optional
        });
        console.log('Stars dentro: ' + $stars);
    });
}); // Fin Document Ready


/*
 //script sweet alert 
 //Aviso Error
 document.querySelector('.alert-error').onclick = function() {
 swal("Vaya...", "Ha ocurrido un error", "error");
 };
 //Aviso Éxito
 document.querySelector('.alert-success').onclick = function() {
 swal("Perfecto", "Se ha realizado correctamente", "success");
 };*/

