git fetch origin;
git checkout master;
git merge origin/master --no-ff;
php app/console cache:clear --env=prod --no-debug; 
php app/console assets:install web --symlink; 
php app/console assetic:dump --env=prod --no-debug;
export SYMFONY_ENV=prod;

