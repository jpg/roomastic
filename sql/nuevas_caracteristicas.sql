ALTER TABLE Hotel CHANGE caracteristica1 piscina TINYINT(1) NOT NULL;
ALTER TABLE Hotel CHANGE caracteristica2 spa TINYINT(1) NOT NULL;
ALTER TABLE Hotel CHANGE caracteristica3 wi_fi TINYINT(1) NOT NULL;
ALTER TABLE Hotel CHANGE caracteristica4 acceso_adaptado TINYINT(1) NOT NULL;
ALTER TABLE Hotel CHANGE caracteristica5 aceptan_perros TINYINT(1) NOT NULL;
ALTER TABLE Hotel CHANGE caracteristica6 aparcamiento TINYINT(1) NOT NULL;
ALTER TABLE Hotel CHANGE caracteristica7 business_center TINYINT(1) NOT NULL;
