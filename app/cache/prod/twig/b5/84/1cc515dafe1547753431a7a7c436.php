<?php

/* HotelesBackendBundle:Oferta:index.html.twig */
class __TwigTemplate_b5841cc515dafe1547753431a7a7c436 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de ofertas"), "html", null, true);
        echo "
";
    }

    // line 7
    public function block_javascripts($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        if (typeof ROOMASTIC === 'undefined') {
            ROOMASTIC = {};
            if (typeof ROOMASTIC.filtro_ofertas === 'undefined') {
                ROOMASTIC.filtro_ofertas = {};
            }
        }
        ROOMASTIC.filtro_ofertas.filtro = '";
        // line 16
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "attributes"), "get", array(0 => "filtro"), "method"), "html", null, true);
        echo "';
        ROOMASTIC.filtro_ofertas.id_hotel = '";
        // line 17
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "attributes"), "get", array(0 => "id_hotel"), "method"), "html", null, true);
        echo "';

    </script>
    ";
        // line 20
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "7cd9f50_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7cd9f50_0") : $this->env->getExtension('assets')->getAssetUrl("js/7cd9f50_part_1_listadoOfertas_1.js");
            echo "      
    <script type=\"text/javascript\" src=\"";
            // line 21
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "7cd9f50"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7cd9f50") : $this->env->getExtension('assets')->getAssetUrl("js/7cd9f50.js");
            // line 20
            echo "      
    <script type=\"text/javascript\" src=\"";
            // line 21
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 25
    public function block_body($context, array $blocks = array())
    {
        // line 26
        echo "

    <div class=\"row\">
        <div class=\"col-lg-12\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de ofertas"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <!--  buscador + rtdos -->
                    <div class=\"adv-table\">
                        <div class=\"row\">
                            <div class=\"col-lg-9\">
                                <div id=\"editable-sample_length\" class=\"dataTables_length\">
                                    ";
        // line 40
        $this->env->loadTemplate("HotelesBackendBundle:Extras:select.html.twig")->display($context);
        // line 41
        echo "                                </div>

                            </div>
                            ";
        // line 44
        if (($this->env->getExtension('security')->isGranted("ROLE_EMPRESA") || $this->env->getExtension('security')->isGranted("ROLE_ADMIN"))) {
            // line 45
            echo "                                <div class=\"col-lg-3\">
                                    <div class=\"dataTables_length_hotel\">
                                        <label>
                                            ";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de hoteles"), "html", null, true);
            echo "
                                            <select size=\"1\" name=\"editable-sample_length\" aria-controls=\"editable-sample\" class=\"form-control xsmall\" id=\"hotelSelect\">
                                                ";
            // line 50
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            $context["hotel_seleccionado"] = $this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "attributes"), "get", array(0 => "id_hotel"), "method");
            // line 51
            echo "                                                <option value=\"\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Todos"), "html", null, true);
            echo "</option>
                                                ";
            // line 52
            if (isset($context["hoteles"])) { $_hoteles_ = $context["hoteles"]; } else { $_hoteles_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_hoteles_);
            foreach ($context['_seq'] as $context["_key"] => $context["hotel"]) {
                // line 53
                echo "                                                    <option value=\"";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "id"), "html", null, true);
                echo "\" ";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                if (isset($context["hotel_seleccionado"])) { $_hotel_seleccionado_ = $context["hotel_seleccionado"]; } else { $_hotel_seleccionado_ = null; }
                if (($this->getAttribute($_hotel_, "id") == $_hotel_seleccionado_)) {
                    echo "selected=\"selected\"";
                }
                echo ">";
                if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "nombrehotel"), "html", null, true);
                echo "</option>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hotel'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 55
            echo "                                            </select> 

                                        </label>
                                    </div>
                                </div>
                            ";
        }
        // line 61
        echo "
                            <!-- / buscador + rtdos -->
                            <!--  contentTabla -->
                            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-advance table-hover\" id=\"hidden-table-info\">

                                <thead>
                                    <tr>
                                        ";
        // line 69
        echo "                                        <th>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha oferta"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Precio habitación"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Entrada"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Salida"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Noches"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Adultos"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Niños"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hab."), "html", null, true);
        echo "</th>
                                        <th>";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Importe Total"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha contraoferta"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total contraoferta"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comision"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Neto"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acciones"), "html", null, true);
        echo "</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    ";
        // line 89
        if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_pagination_);
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["oferta"]) {
            // line 90
            echo "                                        <tr>
                                            ";
            // line 92
            echo "                                            <td>";
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            if ($this->getAttribute($_oferta_, "fecha")) {
                if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_oferta_, "fecha"), "d-m-Y H:i:s"), "html", null, true);
            }
            echo "</td>
                                            <td>";
            // line 93
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_oferta_, "hotel"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 94
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($_oferta_, "preciohabitacion"), 2, ",", "."), "html", null, true);
            echo "</td>
                                            <td>";
            // line 95
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            if ($this->getAttribute($_oferta_, "fechain")) {
                if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_oferta_, "fechain"), "d-m-Y"), "html", null, true);
            }
            echo "</td>
                                            <td>";
            // line 96
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            if ($this->getAttribute($_oferta_, "fechaout")) {
                if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_oferta_, "fechaout"), "d-m-Y"), "html", null, true);
            }
            echo "</td>
                                            <td>";
            // line 97
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_oferta_, "numnoches"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 98
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_oferta_, "numadultos"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 99
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_oferta_, "numninos"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 100
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_oferta_, "numhabitaciones"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 101
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            echo twig_escape_filter($this->env, (twig_number_format_filter($this->env, $this->getAttribute($_oferta_, "preciototaloferta"), 2, ",", ".") . $this->env->getExtension('translator')->trans("€")), "html", null, true);
            echo "</td>
                                            <td>";
            // line 102
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            if (($this->getAttribute($_oferta_, "fechacontraoferta") != null)) {
                if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_oferta_, "fechacontraoferta"), "d-m-y H:i"), "html", null, true);
            }
            echo "</td>
                                            <td>";
            // line 103
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            echo twig_escape_filter($this->env, (twig_number_format_filter($this->env, $this->getAttribute($_oferta_, "preciototalcontraoferta"), 2, ",", ".") . $this->env->getExtension('translator')->trans("€")), "html", null, true);
            echo "</td>
                                            <td>";
            // line 104
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            echo twig_escape_filter($this->env, (twig_number_format_filter($this->env, $this->getAttribute($_oferta_, "getComisionOferta"), 2, ",", ".") . "%"), "html", null, true);
            echo "</td>
                                            <td style=\"font-weight: 900;\">";
            // line 105
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            echo twig_escape_filter($this->env, (twig_number_format_filter($this->env, $this->getAttribute($_oferta_, "getNeto"), 2, ",", ".") . $this->env->getExtension('translator')->trans("€")), "html", null, true);
            echo "</td>
                                            <td>
                                                ";
            // line 107
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            $this->env->loadTemplate("HotelesBackendBundle:Oferta:status.html.twig")->display(array_merge($context, array("status" => $this->getAttribute($_oferta_, "status"))));
            // line 108
            echo "                                            </td>
                                            <td>
                                                ";
            // line 113
            echo "                                                ";
            if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
            if ($this->getAttribute($_oferta_, "canHotelInteract")) {
                // line 114
                echo "                                                    <a href=\"";
                if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("acepta_oferta", array("id" => $this->getAttribute($_oferta_, "id"))), "html", null, true);
                echo "\" style=\"margin: 3px 0;\" class=\"btn btn-primary btn-xs\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptar"), "html", null, true);
                echo "\"><i class=\"fa fa-check\"></i>  ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptar"), "html", null, true);
                echo "</a> <br>
                                                    <a href=\"";
                // line 115
                if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("declina_oferta", array("id" => $this->getAttribute($_oferta_, "id"))), "html", null, true);
                echo "\" style=\"margin: 3px 0;\" class=\"btn btn-primary btn-xs\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Declinar"), "html", null, true);
                echo "\"><i class=\"fa fa-times\"></i>  ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Declinar"), "html", null, true);
                echo "</a><br>
                                                    <a href=\"";
                // line 116
                if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_contraoferta", array("id" => $this->getAttribute($_oferta_, "id"))), "html", null, true);
                echo "\" style=\"margin: 3px 0;\" class=\"btn btn-primary btn-xs\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraofertar"), "html", null, true);
                echo "\"><i class=\"fa fa-usd\"></i>  ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contra"), "html", null, true);
                echo "</a>
                                                ";
            } elseif ((((null === $this->getAttribute($_oferta_, "sale")) == false) && $this->getAttribute($this->getAttribute($_oferta_, "sale"), "isPayed", array(), "method"))) {
                // line 118
                echo "                                                    <a href=\"";
                if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("hoteles_backend_oferta_ofertadetails", array("id" => $this->getAttribute($_oferta_, "id"))), "html", null, true);
                echo "\" style=\"margin: 3px 0;\" class=\"btn btn-primary btn-xs\" id=\"ver-detalles-oferta\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Detalles de la oferta"), "html", null, true);
                echo "\" data-toggle=\"modal\" data-target=\"#offer-details-modal\" data-remote=\"false\"><i class=\"fa fa-user\"></i>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver detalles usuario"), "html", null, true);
                echo "</a>
                                                ";
            } else {
                // line 120
                echo "                                                    
                                                ";
            }
            // line 121
            echo "                                         
                                            </td>
                                        </tr>
                                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oferta'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 125
        echo "                                </tbody>
                            </table>
                            <!--  /contentTabla -->

                        </div> <!-- /addtable -->
                    </div><!-- /panelbody -->

            </section>

            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"span6\">   
                    </div>
                    <div class=\"span6\">
                        <div class=\"dataTables_paginate paging_bootstrap pagination withoutbuttons\">
                            ";
        // line 141
        if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
        echo $this->env->getExtension('knp_pagination')->render($_pagination_, "HotelesBackendBundle:Paginacion:paginacion.html.twig");
        echo "
                        </div>
                    </div>                  
                </div>          
            </section>
            <!-- /btns -->


        </div><!-- /col -->

    </div><!-- /row -->
    <div class=\"modal fade\" id=\"offer-details-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"offer-details-modal-label\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\" id=\"fee-details-label\">";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Detalles de la oferta"), "html", null, true);
        echo "</h4>
                </div>
                <div class=\"modal-body\">
                    <table class=\"table table-condensed\">

                        <tr>
                            <th>";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
        echo "</th>
                            <td id=\"nombre_completo\"></td>
                        </tr>
                        <tr>
                            <th>";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</th>
                            <td id=\"email\"></td>
                        </tr>
                        <tr>
                            <th>";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("DNI"), "html", null, true);
        echo "</th>
                            <td id=\"dni\"></td>
                        </tr>
                        <tr>
                            <th>";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Direccion"), "html", null, true);
        echo "</th>
                            <td id=\"direccion\"></td>
                        </tr>
                        <tr>
                            <th>";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Telefono"), "html", null, true);
        echo "</th>
                            <td id=\"telefono\"></td>
                        </tr>

                    </table>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Oferta:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  506 => 179,  499 => 175,  492 => 171,  485 => 167,  478 => 163,  469 => 157,  449 => 141,  431 => 125,  414 => 121,  410 => 120,  399 => 118,  389 => 116,  380 => 115,  370 => 114,  366 => 113,  362 => 108,  359 => 107,  353 => 105,  348 => 104,  343 => 103,  335 => 102,  330 => 101,  325 => 100,  320 => 99,  315 => 98,  310 => 97,  302 => 96,  294 => 95,  289 => 94,  284 => 93,  275 => 92,  272 => 90,  254 => 89,  246 => 84,  242 => 83,  238 => 82,  234 => 81,  230 => 80,  226 => 79,  222 => 78,  218 => 77,  214 => 76,  210 => 75,  206 => 74,  202 => 73,  198 => 72,  194 => 71,  190 => 70,  185 => 69,  176 => 61,  168 => 55,  149 => 53,  144 => 52,  139 => 51,  136 => 50,  131 => 48,  126 => 45,  124 => 44,  119 => 41,  117 => 40,  106 => 32,  98 => 26,  95 => 25,  86 => 21,  83 => 20,  75 => 21,  69 => 20,  62 => 17,  57 => 16,  45 => 8,  42 => 7,  36 => 5,  31 => 4,  28 => 3,);
    }
}
