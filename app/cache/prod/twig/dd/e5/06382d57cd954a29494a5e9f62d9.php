<?php

/* HotelesBackendBundle:Landing:porcentaje-de-ofertas-atendidas.html.twig */
class __TwigTemplate_dde506382d57cd954a29494a5e9f62d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["porcentaje"])) { $_porcentaje_ = $context["porcentaje"]; } else { $_porcentaje_ = null; }
        if (($_porcentaje_ < 20)) {
            // line 2
            echo "    <span class=\"badge bg-error\">";
            if (isset($context["porcentaje"])) { $_porcentaje_ = $context["porcentaje"]; } else { $_porcentaje_ = null; }
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $_porcentaje_, 2, ",", "."), "html", null, true);
            echo "%</span>

";
        } elseif (($_porcentaje_ < 40)) {
            // line 5
            echo "    <span class=\"badge bg-warning\">";
            if (isset($context["porcentaje"])) { $_porcentaje_ = $context["porcentaje"]; } else { $_porcentaje_ = null; }
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $_porcentaje_, 2, ",", "."), "html", null, true);
            echo "%</span>

";
        } elseif (($_porcentaje_ < 60)) {
            // line 8
            echo "    <span class=\"badge bg-warning\">";
            if (isset($context["porcentaje"])) { $_porcentaje_ = $context["porcentaje"]; } else { $_porcentaje_ = null; }
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $_porcentaje_, 2, ",", "."), "html", null, true);
            echo "%</span>

";
        } elseif (($_porcentaje_ < 80)) {
            // line 11
            echo "    <span class=\"badge bg-success\">";
            if (isset($context["porcentaje"])) { $_porcentaje_ = $context["porcentaje"]; } else { $_porcentaje_ = null; }
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $_porcentaje_, 2, ",", "."), "html", null, true);
            echo "%</span>

";
        } elseif (($_porcentaje_ <= 100)) {
            // line 14
            echo "    <span class=\"badge bg-success\">";
            if (isset($context["porcentaje"])) { $_porcentaje_ = $context["porcentaje"]; } else { $_porcentaje_ = null; }
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $_porcentaje_, 2, ",", "."), "html", null, true);
            echo "%</span>
";
        } else {
        }
        // line 17
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Landing:porcentaje-de-ofertas-atendidas.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 17,  52 => 14,  44 => 11,  36 => 8,  28 => 5,  20 => 2,  17 => 1,);
    }
}
