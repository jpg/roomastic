<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_0cf915133504416975f9f0624a592d14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array(), "any", false, true), "username", array(), "any", true, true)) {
            echo "   
    <script>window.location.href = '";
            // line 2
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing"), "html", null, true);
            echo "'</script>
";
        }
        // line 4
        echo "
<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"Mosaddek\">
        <meta name=\"keyword\" content=\"FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina\">
        <link rel=\"shortcut icon\" href=\"img/favicon.png\">

        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

        <title></title>

        <!-- Bootstrap core CSS -->
        <link href=\"/bundles/hotelesbackend/css/bootstrap.min.css\" rel=\"stylesheet\">
        <link href=\"/bundles/hotelesbackend/css/bootstrap-reset.css\" rel=\"stylesheet\">
        <!--external css-->
        <link href=\"/bundles/hotelesbackend/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
        <!-- Custom styles for this template -->
        <link href=\"/bundles/hotelesbackend/css/style.css\" rel=\"stylesheet\">
        <link href=\"/bundles/hotelesbackend/css/style-responsive.css\" rel=\"stylesheet\" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src=\"js/html5shiv.js\"></script>
        <script src=\"js/respond.min.js\"></script>
        <![endif]-->
    </head>


    ";
        // line 36
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 37
            echo "        <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans((isset($context["error"]) ? $context["error"] : null)), "html", null, true);
            echo "</div>
    ";
        }
        // line 39
        echo "
    <body class=\"login-body\">

        <div class=\"container\">

            <form class=\"form-signin\" action=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_check"), "html", null, true);
        echo "\" method=\"post\">
                <h2 class=\"form-signin-heading\">";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Accede a tu cuenta"), "html", null, true);
        echo "</h2>
                <div class=\"login-wrap\">
                    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 47
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : null), "html", null, true);
        echo "\" />
                    <input type=\"text\" class=\"form-control\" placeholder=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuario"), "html", null, true);
        echo "\" name=\"_username\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : null), "html", null, true);
        echo "\" autofocus>
                    <input type=\"password\" class=\"form-control\" placeholder=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraseña"), "html", null, true);
        echo "\" name=\"_password\">
                    <label class=\"checkbox\">
                        <input type=\"checkbox\" value=\"remember-me\"> ";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Recordarme en este equipo"), "html", null, true);
        echo "
                    </label>
                    <button class=\"btn btn-lg btn-login btn-block\" type=\"submit\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceder"), "html", null, true);
        echo "</button>
                    <p class=\"forgor_ps\">
                        <a href=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_request"), "html", null, true);
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado tu contraseña?"), "html", null, true);
        echo "</a>
                    </p>
                </div>
            </form>

            <!-- Modal -->
            <div aria-hidden=\"true\" aria-labelledby=\"myModalLabel\" role=\"dialog\" tabindex=\"-1\" id=\"myModal\" class=\"modal fade\">
                <div class=\"modal-dialog\">
                    <div class=\"modal-content\">
                        <div class=\"modal-header\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                            <h4 class=\"modal-title\">";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado la contraseña?"), "html", null, true);
        echo "</h4>
                        </div>                            
                        <form method=\"POST\" action=\"";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_send_email"), "html", null, true);
        echo "\" >
                            <div class=\"modal-body\">
                                <p>";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Escribe tu email y te reenviaremos una nueva contraseña"), "html", null, true);
        echo "</p>
                                <input type=\"text\" name=\"username\" placeholder=\"Email\" autocomplete=\"off\" class=\"form-control placeholder-no-fix\"/>
                            </div>
                            <div class=\"modal-footer\">
                                <button data-dismiss=\"modal\" class=\"btn btn-default\" type=\"button\">";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cancelar"), "html", null, true);
        echo "</button>
                                <button class=\"btn btn-success\" type=\"button\">";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar"), "html", null, true);
        echo "</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <!-- modal -->


        </div>



        <!-- js placed at the end of the document so the pages load faster -->
        <script src=\"/bundles/hotelesbackend/js/jquery.js\"></script>
        <script src=\"/bundles/hotelesbackend/js/bootstrap.min.js\"></script>


    </body>
</html>";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 75,  142 => 74,  135 => 70,  130 => 68,  125 => 66,  109 => 55,  104 => 53,  99 => 51,  94 => 49,  88 => 48,  84 => 47,  79 => 45,  75 => 44,  68 => 39,  62 => 37,  60 => 36,  26 => 4,  21 => 2,  17 => 1,);
    }
}
