<?php

/* HotelesFrontendBundle:Frontend:ofertarealizada.html.twig */
class __TwigTemplate_5ace27f37cec82159634679de4dd8449 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "

<div class=\"contenido oferta_ok\">

\t<h2>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Confirmación de oferta enviada"), "html", null, true);
        echo "</h2>
\t<p>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si tienes alguna duda, puedes ponerte en contacto con nosotros."), "html", null, true);
        echo "</p>
\t<p>";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Muchas gracias por utilizar Roomastic."), "html", null, true);
        echo "</p>
\t<div class=\"separador\"></div>

\t<div class=\"bq\">
\t\t<div class=\"txt1\">
\t\t\t<p><span>Tu oferta ha sido enviada correctamente</span></p>
\t\t\t<p>Los establecimientos que has seleccionado han recibido correctamente tu oferta.</p>
\t\t\t<p>Permanece <b>atento al email que nos has facilitado</b>, ya que los hoteles se pondrán en contacto contigo mediante esta vía.</p>\t
\t\t</div>
\t</div>
\t<div class=\"separador no-margin\"></div>

</div>



";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:ofertarealizada.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 10,  39 => 9,  35 => 8,  29 => 4,  26 => 3,);
    }
}
