<?php

/* HotelesBackendBundle:NotificacionesHeader:contacto.html.twig */
class __TwigTemplate_da8721cd3ebe9300322fc51035cb7a95 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<li>
    <a href=\"";
        // line 2
        if (isset($context["contacto"])) { $_contacto_ = $context["contacto"]; } else { $_contacto_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb_edit", array("id" => $this->getAttribute($_contacto_, "id"))), "html", null, true);
        echo "\">
        <span class=\"photo\"><img alt=\"avatar\" src=\"/bundles/hotelesbackend/img/avatar.jpg\"></span>
        <span class=\"subject\">
            <span class=\"from\">";
        // line 5
        if (isset($context["contacto"])) { $_contacto_ = $context["contacto"]; } else { $_contacto_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_contacto_, "getNombreCompleto"), "html", null, true);
        echo "</span>
            <span class=\"time\">";
        // line 6
        if (isset($context["contacto"])) { $_contacto_ = $context["contacto"]; } else { $_contacto_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('time_ago_extension')->timeAgoInWordsFilter($this->getAttribute($_contacto_, "fecha")), "html", null, true);
        echo "</span>
        </span>
        <span class=\"message\">
            ";
        // line 9
        if (isset($context["contacto"])) { $_contacto_ = $context["contacto"]; } else { $_contacto_ = null; }
        echo twig_escape_filter($this->env, (((twig_length_filter($this->env, $this->getAttribute($_contacto_, "texto")) > 30)) ? ((twig_slice($this->env, $this->getAttribute($_contacto_, "texto"), 0, 30) . "...")) : ($this->getAttribute($_contacto_, "texto"))), "html", null, true);
        echo "
        </span>
    </a>
</li>";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:NotificacionesHeader:contacto.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 9,  32 => 6,  27 => 5,  20 => 2,  17 => 1,);
    }
}
