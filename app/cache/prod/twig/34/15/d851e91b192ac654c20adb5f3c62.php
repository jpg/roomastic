<?php

/* HotelesBackendBundle:Empresa:new.html.twig */
class __TwigTemplate_3415d851e91b192ac654c20adb5f3c62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear empresa"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">

            <!--tab nav start-->
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear empresa"), "html", null, true);
        echo "
                </header>
                <header class=\"panel-heading tab-bg-dark-navy-blue \">
                    <ul class=\"nav nav-tabs\">
                        <li class=\"active ";
        // line 20
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "email")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombreempresa")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cif")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "direccionfacturacion")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefono")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "numerocuenta")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#general\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la empresa"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"";
        // line 23
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrepersonacontacto")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "apellidospersonacontacto")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "emailpersonacontacto")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefonopersonacontacto")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cargopersonacontacto")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#api\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la persona de contacto"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"";
        // line 26
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionempresaentero")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionempresadecimal")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#comisiones\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comisiones"), "html", null, true);
        echo "</a>
                        </li>
                    </ul>
                </header>
                <div class=\"panel-body\">
                    <div class=\"tab-content\">
                        <div id=\"general\" class=\"tab-pane active\">
                            <form action=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_create"), "html", null, true);
        echo "\" method=\"post\" ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo ">
                                ";
        // line 36
        echo "                                <div class=\"form-group  ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "email"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 39
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "email"));
        echo "
                                        ";
        // line 40
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 43
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombreempresa"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la empresa"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 46
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombreempresa"));
        echo "
                                        ";
        // line 47
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "nombreempresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 50
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cif"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CIF"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 53
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cif"));
        echo "
                                        ";
        // line 54
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "cif"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 57
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "direccionfacturacion"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dirección de facturación"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 60
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "direccionfacturacion"));
        echo "
                                        ";
        // line 61
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "direccionfacturacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 64
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefono"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 67
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefono"));
        echo "
                                        ";
        // line 68
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "telefono"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 71
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "numerocuenta"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de cuenta"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 74
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "numerocuenta"));
        echo "
                                        ";
        // line 75
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "numerocuenta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                        </div>
                        <div id=\"api\" class=\"tab-pane\">
                            <div class=\"form-group ";
        // line 80
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrepersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 83
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrepersonacontacto"));
        echo "
                                    ";
        // line 84
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "nombrepersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 87
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "apellidospersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 90
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "apellidospersonacontacto"));
        echo "
                                    ";
        // line 91
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "apellidospersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 94
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "emailpersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 97
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "emailpersonacontacto"));
        echo "
                                    ";
        // line 98
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "emailpersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 101
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefonopersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 104
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "telefonopersonacontacto"));
        echo "
                                    ";
        // line 105
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "telefonopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 108
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cargopersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 111
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "cargopersonacontacto"));
        echo "
                                    ";
        // line 112
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "cargopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                        </div>
                        <div id=\"comisiones\" class=\"tab-pane\">

                             <div class=\"form-group\">
                                <label for=\"exampleInputEmail1\">";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Porcentaje de comisión (campo enteros) , (campo decimales)."), "html", null, true);
        echo "</label>
                                <div class=\"clearfix\"></div>
                                <div class=\"col-sm-2-dcnt ";
        // line 121
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionempresaentero"))), "html", null, true);
        echo "\">
                                    ";
        // line 122
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionempresaentero"));
        echo "
                                    ";
        // line 123
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "comisionempresaentero"), array("attr" => array("class" => "form-control")));
        echo "                     
                                </div>
                                <div class=\"comaclear\" style=\"float:left\"> , </div>
                                <div class=\"col-sm-2-dcnt ";
        // line 126
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionempresadecimal"))), "html", null, true);
        echo "\">  
                                    ";
        // line 127
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "comisionempresadecimal"));
        echo "
                                    ";
        // line 128
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "comisionempresadecimal"), array("attr" => array("class" => "form-control")));
        echo "                                  
                                </div>
                                <div class=\"clearfix\"></div>
                            </div>
                        </div>
                        <div id=\"tpv\" class=\"tab-pane\">";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("TPV"), "html", null, true);
        echo "</div>
                    </div>
                </div>
            </section>
            ";
        // line 137
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_form_);
        echo "
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-success\">";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    ";
        // line 143
        echo "                </div>
            </section>

            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->










    ";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Empresa:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  411 => 143,  406 => 140,  399 => 137,  392 => 133,  383 => 128,  378 => 127,  373 => 126,  366 => 123,  361 => 122,  356 => 121,  351 => 119,  340 => 112,  335 => 111,  330 => 109,  325 => 108,  318 => 105,  313 => 104,  308 => 102,  303 => 101,  296 => 98,  291 => 97,  286 => 95,  281 => 94,  274 => 91,  269 => 90,  264 => 88,  259 => 87,  252 => 84,  247 => 83,  242 => 81,  237 => 80,  228 => 75,  223 => 74,  218 => 72,  213 => 71,  206 => 68,  201 => 67,  196 => 65,  191 => 64,  184 => 61,  179 => 60,  174 => 58,  169 => 57,  162 => 54,  157 => 53,  152 => 51,  147 => 50,  140 => 47,  135 => 46,  130 => 44,  125 => 43,  118 => 40,  113 => 39,  108 => 37,  102 => 36,  95 => 34,  85 => 27,  80 => 26,  75 => 24,  70 => 23,  65 => 21,  60 => 20,  53 => 16,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
