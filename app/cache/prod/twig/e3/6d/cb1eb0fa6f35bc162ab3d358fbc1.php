<?php

/* HotelesBackendBundle:Mail:welcomeUser.html.twig */
class __TwigTemplate_e36dcb1eb0fa6f35bc162ab3d358fbc1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 10
        echo "
";
        // line 11
        $this->displayBlock('body_text', $context, $blocks);
        // line 14
        echo "
";
        // line 15
        $this->displayBlock('body_html', $context, $blocks);
        // line 213
        echo "






";
    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Bienvenido a Roomastic"), "html", null, true);
        echo "
    ";
    }

    // line 11
    public function block_body_text($context, array $blocks = array())
    {
        // line 12
        echo "
";
    }

    // line 15
    public function block_body_html($context, array $blocks = array())
    {
        // line 16
        echo "
    <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
    <html xmlns=\"http://www.w3.org/1999/xhtml\">
        <head>
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
        </head>
        <body>
            ";
        // line 24
        echo "            ";
        if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
        if ($this->getAttribute($_user_, "hasRole", array(0 => "ROLE_ADMIN"), "method")) {
            // line 25
            echo "
                <table width=\"812\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                    <table width=\"709\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                        <!-- header -->
                        <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>   
                        <tr><td><img src=\"https://roomastic.com/bundles/hotelesfrontend/img/logo.png\" width=\"146\" height=\"45\" border=\"0\" style=\"display:block;\" /></td></tr>
                        <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>
                        <!-- / header -->
                        <tr>
                            <td style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px;\">
                                <p style=\"font-weight:bold; font-size:29px; margin:0;\">Estás a solo un paso.</p>
                                <p>Haz clic <a href=\"";
            // line 36
            if (isset($context["confirmationUrl"])) { $_confirmationUrl_ = $context["confirmationUrl"]; } else { $_confirmationUrl_ = null; }
            echo twig_escape_filter($this->env, $_confirmationUrl_, "html", null, true);
            echo "\">aquí</a> para verificar tu perfil y elige tu contraseña para empezar a recibir ofertas y disfrutar de las ventajas de estar registrado</p>
                                <p>¡Bienvenido!</p>
                                <p>El equipo de Roomastic.</p>
                            </td>   
                        </tr>
                        <!-- footer -->
                        <tr><td height=\"127\">&nbsp;</td></tr>    
                        <tr>
                            <td style=\"border-top:2px solid #e9e9e9; text-align:center; font-family:Arial, Helvetica, sans-serif;\">
                                <p style=\"height:10px;\">&nbsp;</p>
                                <span style=\"color:#e52e81; font-weight:bold; font-size:16px;\">Teléfono de atención al cliente</span><br />
                                <span style=\"color:#e52e81; font-weight:bold; font-size:27px;\">911 610 156</span><br /><br />
                            </td>
                        </tr>    
                        <tr>
                            <td style=\"font-size:9px; color:#6c6c6c; text-transform: uppercase; font-family:Arial, Helvetica, sans-serif; text-align:center;\">
                                este email ha sido enviado de manera automática por <span style=\"text-decoration:none; color:#474747;\">roomastic.com</span> © Copyright. Todos los derechos reservados. 
                            </td>
                        </tr>
                        <!-- /footer -->
                    </table><!-- table content -->
                </table><!-- table margen -->
                ";
            // line 59
            echo "            ";
        } elseif ($this->getAttribute($_user_, "hasRole", array(0 => "ROLE_EMPRESA"), "method")) {
            // line 60
            echo "
                <table width=\"812\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                    <table width=\"709\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                        <!-- header -->
                        <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>   
                        <tr><td><img src=\"https://roomastic.com/bundles/hotelesfrontend/img/logo.png\" width=\"146\" height=\"45\" border=\"0\" style=\"display:block;\" /></td></tr>
                        <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>
                        <!-- / header -->
                        <tr>
                            <td style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px;\">
                                <p style=\"font-weight:bold; font-size:29px; margin:0;\">Estás a solo un paso.</p>
                                <p>Haz clic <a href=\"";
            // line 71
            if (isset($context["confirmationUrl"])) { $_confirmationUrl_ = $context["confirmationUrl"]; } else { $_confirmationUrl_ = null; }
            echo twig_escape_filter($this->env, $_confirmationUrl_, "html", null, true);
            echo "\">aquí</a> para verificar tu perfil y elige tu contraseña para empezar a recibir ofertas y disfrutar de las ventajas de estar registrado</p>
                                <p>Bienvenido!</p>
                                <p>El equipo de Roomastic.</p>
                            </td>   
                        </tr>
                        <!-- footer -->
                        <tr><td height=\"127\">&nbsp;</td></tr>    
                        <tr>
                            <td style=\"border-top:2px solid #e9e9e9; text-align:center; font-family:Arial, Helvetica, sans-serif;\">
                                <p style=\"height:10px;\">&nbsp;</p>
                                <span style=\"color:#e52e81; font-weight:bold; font-size:16px;\">Teléfono de atención al cliente</span><br />
                                <span style=\"color:#e52e81; font-weight:bold; font-size:27px;\">911 610 156</span><br /><br />
                            </td>
                        </tr>    
                        <tr>
                            <td style=\"font-size:9px; color:#6c6c6c; text-transform: uppercase; font-family:Arial, Helvetica, sans-serif; text-align:center;\">
                                este email ha sido enviado de manera automática por <span style=\"text-decoration:none; color:#474747;\">roomastic.com</span> © Copyright. Todos los derechos reservados. 
                            </td>
                        </tr>
                        <!-- /footer -->
                    </table><!-- table content -->
                </table><!-- table margen -->

                ";
            // line 95
            echo "            ";
        } elseif ($this->getAttribute($_user_, "hasRole", array(0 => "ROLE_HOTEL"), "method")) {
            // line 96
            echo "
                <table width=\"812\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                    <table width=\"709\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                        <!-- header -->
                        <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>   
                        <tr><td><img src=\"https://roomastic.com/bundles/hotelesfrontend/img/logo.png\" width=\"146\" height=\"45\" border=\"0\" style=\"display:block;\" /></td></tr>
                        <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>
                        <!-- / header -->
                        <tr>
                            <td style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px;\">
                                <p><span style=\"text-transform:uppercase; font-weight:bold;\">";
            // line 106
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_user_, "hotel"), "nombreempresa"), "html", null, true);
            echo "</span> te ha registrado en www.roomastic.com.</p>
                                <p>Haz clic <a href=\"";
            // line 107
            if (isset($context["confirmationUrl"])) { $_confirmationUrl_ = $context["confirmationUrl"]; } else { $_confirmationUrl_ = null; }
            echo twig_escape_filter($this->env, $_confirmationUrl_, "html", null, true);
            echo "\">aquí</a> para verificar tu perfil y elige tu contraseña para empezar a recibir ofertas y disfrutar de las ventajas de estar registrado</p>
                                <p>¡Bienvenido!</p>
                                <p>El equipo de Roomastic.</p>  
                            </td>   
                        </tr>
                        <!-- footer -->
                        <tr><td height=\"127\">&nbsp;</td></tr>    
                        <tr>
                            <td style=\"border-top:2px solid #e9e9e9; text-align:center; font-family:Arial, Helvetica, sans-serif;\">
                                <p style=\"height:10px;\">&nbsp;</p>
                                <span style=\"color:#e52e81; font-weight:bold; font-size:16px;\">Teléfono de atención al cliente</span><br />
                                <span style=\"color:#e52e81; font-weight:bold; font-size:27px;\">911 610 156</span><br /><br />
                            </td>
                        </tr>    
                        <tr>
                            <td style=\"font-size:9px; color:#6c6c6c; text-transform: uppercase; font-family:Arial, Helvetica, sans-serif; text-align:center;\">
                                este email ha sido enviado de manera automática por <span style=\"text-decoration:none; color:#474747;\">roomastic.com</span> © Copyright. Todos los derechos reservados. 
                            </td>
                        </tr>
                        <!-- /footer -->
                    </table><!-- table content -->
                </table><!-- table margen -->

                <!-- REGISTER USER -->

            ";
        } elseif ($this->getAttribute($_user_, "hasRole", array(0 => "ROLE_WEB"), "method")) {
            // line 133
            echo "
                <table width=\"812\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                    <table width=\"709\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                        <!-- header -->
                        <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>   
                        <tr><td><img src=\"https://roomastic.com/bundles/hotelesfrontend/img/logo.png\" width=\"146\" height=\"45\" border=\"0\" style=\"display:block;\" /></td></tr>
                        <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>
                        <!-- / header -->
                        <tr>
                            <td style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px;\">
                                <p style=\"font-weight:bold; font-size:29px; margin:0;\">Hola ";
            // line 143
            if (isset($context["user"])) { $_user_ = $context["user"]; } else { $_user_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_user_, "nombre"), "html", null, true);
            echo "!</p>
                                <p>Te damos la bienvenida a Roomastic, la primera plataforma donde podrás realizar ofertas a hoteles, y ahorrar mucho dinero en tus estancias favoritas.</p>
                                <p>En breve recibirás un correo electrónico con información del estado de la oferta que acabas de realizar.</p>
                                <p>Es muy importante que leas nuestras condiciones, para que saques el mayor partido a tus ofertas. Puedes hacerlo, pinchando aquí. <a href=\"#\" style=\"color:#e52e81; text-decoration:none;\">[http://www.roomastic.com/condiciones-legales]</a></p>
                                <p>Si tienes cualquier duda, recuerda que puedes ponerte en contacto con nosotros en el siguiente enlace <a href=\"#\" style=\"color:#e52e81; text-decoration:none;\">[http://www.roomastic.com/contacto]</a></p>
                                <p>Un saludo y gracias por confiar en Roomastic.</p>
                            </td>   
                        </tr>
                        <!-- footer -->
                        <tr><td height=\"127\">&nbsp;</td></tr>    
                        <tr>
                            <td style=\"border-top:2px solid #e9e9e9; text-align:center; font-family:Arial, Helvetica, sans-serif;\">
                                <p style=\"height:10px;\">&nbsp;</p>
                                <span style=\"color:#e52e81; font-weight:bold; font-size:16px;\">Teléfono de atención al cliente</span><br />
                                <span style=\"color:#e52e81; font-weight:bold; font-size:27px;\">911 610 156</span><br /><br />
                            </td>
                        </tr>    
                        <tr>
                            <td style=\"font-size:9px; color:#6c6c6c; text-transform: uppercase; font-family:Arial, Helvetica, sans-serif; text-align:center;\">
                                este email ha sido enviado de manera automática por <span style=\"text-decoration:none; color:#474747;\">roomastic.com</span> © Copyright. Todos los derechos reservados. 
                            </td>
                        </tr>
                        <!-- /footer -->
                    </table><!-- table content -->
                </table><!-- table margen -->


                <!-- /REGISTER USER -->
            ";
        } else {
            // line 172
            echo "                <table width=\"812\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                    <table width=\"709\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                        <!-- header -->
                        <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>   
                        <tr><td><img src=\"https://roomastic.com/bundles/hotelesfrontend/img/logo.png\" width=\"146\" height=\"45\" border=\"0\" style=\"display:block;\" /></td></tr>
                        <tr><td height=\"54\" width=\"100%\">&nbsp;</td></tr>
                        <!-- / header -->
                        <tr>
                            <td style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px;\">
                                <p>Haz clic <a href=\"";
            // line 181
            if (isset($context["confirmationUrl"])) { $_confirmationUrl_ = $context["confirmationUrl"]; } else { $_confirmationUrl_ = null; }
            echo twig_escape_filter($this->env, $_confirmationUrl_, "html", null, true);
            echo "\">aquí</a> para verificar tu perfil y elige tu contraseña para empezar a recibir ofertas y disfrutar de las ventajas de estar registrado</p>
                                <p>Bienvenido!</p> 
                                <p>El equipo de Roomastic.</p>  
                            </td>   
                        </tr>
                        <!-- footer -->
                        <tr><td height=\"127\">&nbsp;</td></tr>    
                        <tr>
                            <td style=\"border-top:2px solid #e9e9e9; text-align:center; font-family:Arial, Helvetica, sans-serif;\">
                                <p style=\"height:10px;\">&nbsp;</p>
                                <span style=\"color:#e52e81; font-weight:bold; font-size:16px;\">Teléfono de atención al cliente</span><br />
                                <span style=\"color:#e52e81; font-weight:bold; font-size:27px;\">911 610 156</span><br /><br />
                            </td>
                        </tr>    
                        <tr>
                            <td style=\"font-size:9px; color:#6c6c6c; text-transform: uppercase; font-family:Arial, Helvetica, sans-serif; text-align:center;\">
                                este email ha sido enviado de manera automática por <span style=\"text-decoration:none; color:#474747;\">roomastic.com</span> © Copyright. Todos los derechos reservados. 
                            </td>
                        </tr>
                        <!-- /footer -->
                    </table><!-- table content -->
                </table><!-- table margen -->


            ";
        }
        // line 206
        echo "

        </body>
    </html>


";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Mail:welcomeUser.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  294 => 206,  265 => 181,  254 => 172,  221 => 143,  209 => 133,  179 => 107,  174 => 106,  162 => 96,  159 => 95,  132 => 71,  119 => 60,  116 => 59,  90 => 36,  77 => 25,  73 => 24,  64 => 16,  61 => 15,  56 => 12,  53 => 11,  46 => 3,  43 => 2,  32 => 213,  30 => 15,  27 => 14,  25 => 11,  22 => 10,  20 => 2,);
    }
}
