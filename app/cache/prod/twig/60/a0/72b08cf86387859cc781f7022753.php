<?php

/* HotelesFrontendBundle:TPV:pago.html.twig */
class __TwigTemplate_60a072b08cf86387859cc781f7022753 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <iframe src=\"";
        if (isset($context["firma"])) { $_firma_ = $context["firma"]; } else { $_firma_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("hoteles_frontend_tpv_processpayment", array("firma" => $_firma_)), "html", null, true);
        echo "\" style=\"width:100%;height:800px;\" ></iframe>
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:TPV:pago.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 44,  128 => 42,  120 => 40,  41 => 7,  142 => 39,  129 => 35,  125 => 41,  221 => 81,  203 => 76,  198 => 74,  185 => 71,  179 => 69,  163 => 65,  157 => 61,  152 => 60,  133 => 55,  126 => 52,  110 => 48,  76 => 20,  70 => 34,  222 => 85,  207 => 81,  204 => 80,  183 => 74,  167 => 70,  164 => 69,  148 => 48,  141 => 61,  103 => 31,  98 => 28,  59 => 31,  49 => 10,  53 => 29,  21 => 2,  100 => 42,  97 => 25,  18 => 1,  151 => 75,  135 => 68,  114 => 55,  206 => 77,  201 => 75,  194 => 71,  191 => 70,  176 => 61,  166 => 66,  158 => 65,  153 => 64,  143 => 58,  134 => 59,  123 => 51,  118 => 39,  90 => 23,  87 => 47,  66 => 11,  122 => 33,  107 => 28,  101 => 33,  95 => 48,  82 => 45,  67 => 17,  52 => 11,  45 => 29,  36 => 5,  34 => 5,  266 => 117,  263 => 116,  259 => 85,  256 => 84,  242 => 13,  229 => 170,  227 => 84,  218 => 80,  209 => 78,  192 => 98,  186 => 75,  180 => 73,  174 => 95,  162 => 85,  160 => 84,  146 => 50,  140 => 57,  136 => 41,  106 => 66,  73 => 35,  69 => 18,  22 => 6,  60 => 12,  55 => 9,  102 => 46,  89 => 25,  63 => 36,  56 => 30,  50 => 12,  43 => 8,  92 => 26,  79 => 21,  57 => 11,  37 => 7,  33 => 7,  29 => 3,  19 => 1,  47 => 26,  30 => 6,  27 => 20,  249 => 14,  239 => 90,  235 => 12,  228 => 88,  224 => 82,  219 => 84,  217 => 79,  214 => 79,  211 => 77,  208 => 76,  202 => 79,  199 => 78,  193 => 67,  182 => 70,  178 => 61,  175 => 60,  172 => 59,  165 => 55,  161 => 57,  156 => 51,  154 => 50,  150 => 48,  147 => 59,  132 => 36,  127 => 56,  113 => 34,  86 => 24,  83 => 25,  78 => 38,  64 => 14,  61 => 16,  48 => 12,  32 => 4,  24 => 3,  117 => 36,  112 => 36,  109 => 29,  104 => 27,  96 => 32,  84 => 39,  80 => 24,  68 => 35,  46 => 9,  44 => 25,  26 => 2,  23 => 4,  39 => 6,  25 => 2,  20 => 3,  17 => 2,  144 => 62,  138 => 46,  130 => 66,  124 => 55,  121 => 41,  115 => 37,  111 => 52,  108 => 51,  99 => 49,  94 => 27,  91 => 43,  88 => 41,  85 => 39,  77 => 20,  74 => 19,  71 => 17,  65 => 16,  62 => 32,  58 => 8,  54 => 33,  51 => 13,  42 => 8,  38 => 6,  35 => 5,  31 => 3,  28 => 24,);
    }
}
