<?php

/* HotelesBackendBundle:Extras:select.html.twig */
class __TwigTemplate_666bf1b299c2b2db82a3c6ecc326e587 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["marcado"] = "50";
        // line 2
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if ($this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "query"), "get", array(0 => "show"), "method")) {
            // line 3
            echo "    ";
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            $context["marcado"] = $this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "query"), "get", array(0 => "show"), "method");
        }
        // line 5
        $context["valores"] = array(0 => "25", 1 => "50", 2 => "75");
        // line 6
        echo "<label>
    <select size=\"1\" name=\"editable-sample_length\" aria-controls=\"editable-sample\" class=\"form-control xsmall\" id=\"numSelect\">
        ";
        // line 8
        if (isset($context["valores"])) { $_valores_ = $context["valores"]; } else { $_valores_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_valores_);
        foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
            // line 9
            echo "            <option value=\"";
            if (isset($context["v"])) { $_v_ = $context["v"]; } else { $_v_ = null; }
            echo twig_escape_filter($this->env, $_v_, "html", null, true);
            echo "\" ";
            if (isset($context["v"])) { $_v_ = $context["v"]; } else { $_v_ = null; }
            if (isset($context["marcado"])) { $_marcado_ = $context["marcado"]; } else { $_marcado_ = null; }
            if (($_v_ == $_marcado_)) {
                echo "selected=\"selected\"";
            }
            echo ">";
            if (isset($context["v"])) { $_v_ = $context["v"]; } else { $_v_ = null; }
            echo twig_escape_filter($this->env, $_v_, "html", null, true);
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 11
        echo "    </select> 
    ";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Resultados por página"), "html", null, true);
        echo "
</label>
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:select.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 12,  57 => 11,  38 => 9,  33 => 8,  29 => 6,  22 => 3,  19 => 2,  17 => 1,  207 => 85,  198 => 81,  186 => 71,  168 => 66,  161 => 64,  153 => 60,  150 => 59,  147 => 58,  141 => 56,  133 => 55,  130 => 54,  127 => 53,  122 => 50,  104 => 49,  96 => 44,  92 => 43,  88 => 42,  84 => 41,  80 => 40,  70 => 32,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 5,);
    }
}
