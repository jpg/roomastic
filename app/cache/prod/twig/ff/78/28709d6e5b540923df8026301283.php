<?php

/* HotelesBackendBundle:Lugar:sacamunicipios.html.twig */
class __TwigTemplate_ff7828709d6e5b540923df8026301283 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["obligatorio"])) { $_obligatorio_ = $context["obligatorio"]; } else { $_obligatorio_ = null; }
        if (($_obligatorio_ == 0)) {
            // line 2
            echo "    <option></option>
";
        }
        // line 4
        if (isset($context["municipios"])) { $_municipios_ = $context["municipios"]; } else { $_municipios_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_municipios_);
        foreach ($context['_seq'] as $context["_key"] => $context["municipio"]) {
            // line 5
            echo "    <option value=\"";
            if (isset($context["municipio"])) { $_municipio_ = $context["municipio"]; } else { $_municipio_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_municipio_, "id"), "html", null, true);
            echo "\">";
            if (isset($context["municipio"])) { $_municipio_ = $context["municipio"]; } else { $_municipio_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_municipio_, "nombre"), "html", null, true);
            echo "</option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['municipio'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Lugar:sacamunicipios.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 5,  24 => 4,  20 => 2,  17 => 1,);
    }
}
