<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_507864f77ce03d6fa8dfd32bf0a38c3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["status_code"])) { $_status_code_ = $context["status_code"]; } else { $_status_code_ = null; }
        if (isset($context["status_text"])) { $_status_text_ = $context["status_text"]; } else { $_status_text_ = null; }
        echo twig_jsonencode_filter(array("error" => array("code" => $_status_code_, "message" => $_status_text_)));
        echo "
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  266 => 117,  263 => 116,  259 => 85,  256 => 84,  242 => 13,  229 => 170,  227 => 116,  218 => 110,  209 => 104,  192 => 98,  186 => 97,  180 => 96,  174 => 95,  162 => 85,  160 => 84,  146 => 77,  140 => 76,  136 => 75,  106 => 66,  73 => 38,  69 => 37,  22 => 4,  60 => 13,  55 => 11,  102 => 19,  89 => 16,  63 => 14,  56 => 12,  50 => 6,  43 => 14,  92 => 39,  79 => 40,  57 => 22,  37 => 8,  33 => 6,  29 => 4,  19 => 1,  47 => 5,  30 => 3,  27 => 3,  249 => 14,  239 => 90,  235 => 12,  228 => 84,  224 => 83,  219 => 80,  217 => 79,  214 => 78,  211 => 77,  208 => 76,  202 => 72,  199 => 99,  193 => 67,  182 => 63,  178 => 61,  175 => 60,  172 => 59,  165 => 55,  161 => 54,  156 => 51,  154 => 50,  150 => 48,  147 => 47,  132 => 44,  127 => 43,  113 => 34,  86 => 49,  83 => 27,  78 => 43,  64 => 14,  61 => 13,  48 => 19,  32 => 5,  24 => 4,  117 => 70,  112 => 69,  109 => 20,  104 => 19,  96 => 18,  84 => 14,  80 => 12,  68 => 24,  46 => 14,  44 => 9,  26 => 3,  23 => 3,  39 => 13,  25 => 2,  20 => 2,  17 => 1,  144 => 46,  138 => 45,  130 => 74,  124 => 73,  121 => 41,  115 => 40,  111 => 38,  108 => 31,  99 => 32,  94 => 29,  91 => 17,  88 => 50,  85 => 26,  77 => 39,  74 => 20,  71 => 19,  65 => 16,  62 => 15,  58 => 8,  54 => 11,  51 => 10,  42 => 9,  38 => 7,  35 => 4,  31 => 5,  28 => 4,);
    }
}
