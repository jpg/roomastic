<?php

/* HotelesBackendBundle:Extras:select.html.twig */
class __TwigTemplate_c88d55a91fbd73244f80a5c87f2f496e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["marcado"] = "50";
        // line 2
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "query"), "get", array(0 => "show"), "method")) {
            // line 3
            echo "    ";
            $context["marcado"] = $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "query"), "get", array(0 => "show"), "method");
        }
        // line 5
        $context["valores"] = array(0 => "25", 1 => "50", 2 => "75");
        // line 6
        echo "<label>
    <select size=\"1\" name=\"editable-sample_length\" aria-controls=\"editable-sample\" class=\"form-control xsmall\" id=\"numSelect\">
        ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["valores"]) ? $context["valores"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
            // line 9
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
            echo "\" ";
            if (((isset($context["v"]) ? $context["v"] : null) == (isset($context["marcado"]) ? $context["marcado"] : null))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 11
        echo "    </select> 
    ";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Resultados por página"), "html", null, true);
        echo "
</label>
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:select.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 12,  50 => 11,  35 => 9,  27 => 6,  25 => 5,  21 => 3,  19 => 2,  17 => 1,  470 => 179,  463 => 175,  456 => 171,  449 => 167,  442 => 163,  433 => 157,  414 => 141,  396 => 125,  379 => 121,  375 => 120,  365 => 118,  356 => 116,  348 => 115,  339 => 114,  336 => 113,  332 => 108,  330 => 107,  325 => 105,  321 => 104,  317 => 103,  311 => 102,  307 => 101,  303 => 100,  299 => 99,  295 => 98,  291 => 97,  285 => 96,  279 => 95,  275 => 94,  271 => 93,  264 => 92,  261 => 90,  244 => 89,  236 => 84,  232 => 83,  228 => 82,  224 => 81,  220 => 80,  216 => 79,  212 => 78,  208 => 77,  204 => 76,  200 => 75,  196 => 74,  192 => 73,  188 => 72,  184 => 71,  180 => 70,  175 => 69,  166 => 61,  158 => 55,  143 => 53,  139 => 52,  134 => 51,  132 => 50,  127 => 48,  122 => 45,  120 => 44,  115 => 41,  113 => 40,  102 => 32,  94 => 26,  91 => 25,  83 => 21,  80 => 20,  73 => 21,  67 => 20,  61 => 17,  57 => 16,  45 => 8,  42 => 7,  36 => 5,  31 => 8,  28 => 3,);
    }
}
