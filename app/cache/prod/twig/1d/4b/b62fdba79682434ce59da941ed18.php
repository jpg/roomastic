<?php

/* HotelesBackendBundle:Lugar:sacalugares.html.twig */
class __TwigTemplate_1d4bb62fdba79682434ce59da941ed18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["obligatorio"])) { $_obligatorio_ = $context["obligatorio"]; } else { $_obligatorio_ = null; }
        if (($_obligatorio_ == 0)) {
            // line 2
            echo "    <option>

    </option>
";
        }
        // line 6
        if (isset($context["lugares"])) { $_lugares_ = $context["lugares"]; } else { $_lugares_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_lugares_);
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["lugar"]) {
            // line 7
            echo "    <option value=\"";
            if (isset($context["lugar"])) { $_lugar_ = $context["lugar"]; } else { $_lugar_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_lugar_, "id"), "html", null, true);
            echo "\">
        ";
            // line 8
            if (isset($context["lugar"])) { $_lugar_ = $context["lugar"]; } else { $_lugar_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_lugar_, "zona"), "html", null, true);
            echo "
    </option>
";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 11
            echo "    <option selected value=\"\">
        ";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No hay lugares"), "html", null, true);
            echo "
    </option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lugar'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Lugar:sacalugares.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 12,  47 => 11,  38 => 8,  32 => 7,  26 => 6,  20 => 2,  17 => 1,);
    }
}
