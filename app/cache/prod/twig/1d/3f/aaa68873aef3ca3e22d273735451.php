<?php

/* HotelesFrontendBundle::layout.mv.twig */
class __TwigTemplate_1d3faaa68873aef3ca3e22d273735451 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'seotitle' => array($this, 'block_seotitle'),
            'seodescripcion' => array($this, 'block_seodescripcion'),
            'seokeywords' => array($this, 'block_seokeywords'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'modal' => array($this, 'block_modal'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->
    <head>

        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        <meta name=\"title\" content=\"";
        // line 12
        $this->displayBlock('seotitle', $context, $blocks);
        echo "\">
        <meta name=\"description\" content=\"";
        // line 13
        $this->displayBlock('seodescripcion', $context, $blocks);
        echo "\">
        <meta name=\"keywords\" content=\"";
        // line 14
        $this->displayBlock('seokeywords', $context, $blocks);
        echo "\">
        <!--<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">-->
        <!-- favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\" />
        <link rel=\"apple-touch-icon\" href=\"/bundles/hotelesfrontend/img/apple-touch-icon.png\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"/bundles/hotelesfrontend/img/apple-touch-icon-72x72.png\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"/bundles/hotelesfrontend/img/apple-touch-icon-114x114.png\">
        <!-- meta para mv -->
        <meta name=\"viewport\" content=\"width=700, target-densityDpi=device-dpi\">
        ";
        // line 23
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 38
        echo "
        <script src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>
        <!--[if lt IE 9]>
            <script src=\"js/vendor/html5shiv/dist/html5shiv.min.js\"></script>
        <![endif]-->

    </head> 
    ";
        // line 46
        $context["rrss"] = $this->env->getExtension('twig_extension')->getRRSS();
        // line 47
        echo "    <script>
        if (typeof ROOMASTIC === 'undefined') {
            ROOMASTIC = {};
        }
        ROOMASTIC.version_mv = true;
    </script>
    <body class=\"portada\">
        <!--[if lt IE 7]>
            <p class=\"browsehappy\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- aviso cookie -->

        <!-- wrapper -->
        <div class=\"wrapper\">
            <!-- aviso cookie -->
            <div id=\"cookie-bar\" class=\"fixed\"></div>
            <!-- aviso cookie -->
            <!-- header -->
            <div class=\"headerpg\">
                <h1><a href=\"index\">Roomastic</a></h1>
            </div>
            <div class=\"container\">        
                ";
        // line 70
        $this->displayBlock('content', $context, $blocks);
        // line 71
        echo "             
            </div><!-- container -->

            <!-- footer -->
            <div class=\"footer\">
                <ul>
                    <li>
                        <a href=\"";
        // line 78
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "attributes"), "get", array(0 => "_route"), "method"), twig_array_merge($this->env->getExtension('twig_extension')->getCurrentRouteParams(), array("version" => "full"))), "html", null, true);
        echo "\" class=\"web\">VERSIÓN COMPLETA WEB</a>
                    </li>
                    <li class=\"last\"><a href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_condicionesLegales"), "html", null, true);
        echo "\" class=\"legal\">AVISO LEGAL Y CONDICIONES </a></li>
                    <div class=\"clearfix\"></div>    
                </ul>
                <p>© Copyright. Todos los derechos reservados.</p>

            </div>
            <!-- /footer -->
        </div> 
        <!-- /wrapper -->
        ";
        // line 89
        $this->displayBlock('modal', $context, $blocks);
        // line 91
        echo "
        ";
        // line 92
        $this->displayBlock('javascripts', $context, $blocks);
        // line 157
        echo "    </body>
</html>
";
    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        echo "Roomastic";
    }

    // line 12
    public function block_seotitle($context, array $blocks = array())
    {
        if (isset($context["seo"])) { $_seo_ = $context["seo"]; } else { $_seo_ = null; }
        echo twig_escape_filter($this->env, (($this->getAttribute($_seo_, "seotitulo", array(), "any", true, true)) ? ($this->getAttribute($_seo_, "seotitulo")) : ("")), "html", null, true);
    }

    // line 13
    public function block_seodescripcion($context, array $blocks = array())
    {
        if (isset($context["seo"])) { $_seo_ = $context["seo"]; } else { $_seo_ = null; }
        echo twig_escape_filter($this->env, (($this->getAttribute($_seo_, "seodescripcion", array(), "any", true, true)) ? ($this->getAttribute($_seo_, "seodescripcion")) : ("")), "html", null, true);
    }

    // line 14
    public function block_seokeywords($context, array $blocks = array())
    {
        if (isset($context["seo"])) { $_seo_ = $context["seo"]; } else { $_seo_ = null; }
        echo twig_escape_filter($this->env, (($this->getAttribute($_seo_, "seokeywords", array(), "any", true, true)) ? ($this->getAttribute($_seo_, "seokeywords")) : ("")), "html", null, true);
    }

    // line 23
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 24
        echo "            <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

            <!--<link href=\"http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css\" rel=\"stylesheet\">-->
            <link href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\">
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/daterangepicker-bs3.css\">
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/selectize.default.css\">
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/owl.carousel.css\">
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/base.css\"  />
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/jquery.fancybox.css\">
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/sweet-alert.css\">
            ";
        // line 34
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bfb8d30_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_bfb8d30_0") : $this->env->getExtension('assets')->getAssetUrl("css/bfb8d30_movil_1.css");
            // line 35
            echo "            <link rel=\"stylesheet\" href=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\" />
            ";
        } else {
            // asset "bfb8d30"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_bfb8d30") : $this->env->getExtension('assets')->getAssetUrl("css/bfb8d30.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\" />
            ";
        }
        unset($context["asset_url"]);
        // line 37
        echo "        ";
    }

    // line 70
    public function block_content($context, array $blocks = array())
    {
        // line 71
        echo "                ";
    }

    // line 89
    public function block_modal($context, array $blocks = array())
    {
        // line 90
        echo "        ";
    }

    // line 92
    public function block_javascripts($context, array $blocks = array())
    {
        // line 93
        echo "            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
            <script>window.jQuery || document.write('<script src=\"/bundles/hotelesfrontend/js/vendor/jquery/dist/jquery.min.js\"><\\/script>')</script>

            <script src=\"/bundles/hotelesfrontend/js/plugins.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.backstretch.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/moment.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/daterangepicker.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/selectize.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/icheck.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/gmap3.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/owl.carousel.min.js\"></script>
            <script src=\"https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js\"></script>
            <!-- slide fotos detalle hotel -->
            <script type=\"text/javascript\" src=\"/bundles/hotelesfrontend/js/plugins/jquery.jcarousel.min.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesfrontend/js/plugins/jquery.pikachoose.js\"></script>
            <!-- fancybox -->
            <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.pack.js\"></script>
            <!-- api maps -->
            <script src=\"https://maps.googleapis.com/maps/api/js?v=3.exp\"></script>
            <!-- cookies -->
            <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.cookiebar.js\"></script>
            <script src=\"//code.jquery.com/ui/1.11.1/jquery-ui.js\"></script>
            <script type=\"text/javascript\">
                ";
        // line 117
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if (($this->getAttribute($_app_, "debug") == 1)) {
            // line 118
            echo "                    var env = 'dev';
                ";
        } else {
            // line 120
            echo "                    var env = 'prod';
                ";
        }
        // line 122
        echo "            </script>


            ";
        // line 125
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "592cb5a_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_592cb5a_0") : $this->env->getExtension('assets')->getAssetUrl("js/592cb5a_roomastic_1.js");
            // line 126
            echo "            <script src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
            ";
        } else {
            // asset "592cb5a"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_592cb5a") : $this->env->getExtension('assets')->getAssetUrl("js/592cb5a.js");
            echo "            <script src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
            ";
        }
        unset($context["asset_url"]);
        // line 128
        echo "
            <!-- script slide foto hotel detalle -->
            <script type=\"text/javascript\">
                    jQuery(document).ready(function () {
                        var a = function (self) {
                            self.anchor.fancybox();
                        };
                        jQuery(\"#pikame\").PikaChoose({buildFinished: a});
                    });
            </script> 


            <script>
                (function (b, o, i, l, e, r) {
                    b.GoogleAnalyticsObject = l;
                    b[l] || (b[l] =
                            function () {
                                (b[l].q = b[l].q || []).push(arguments)
                            });
                    b[l].l = +new Date;
                    e = o.createElement(i);
                    r = o.getElementsByTagName(i)[0];
                    e.src = '//www.google-analytics.com/analytics.js';
                    r.parentNode.insertBefore(e, r)
                }(window, document, 'script', 'ga'));
                ga('create', 'UA-53266623-1');
                ga('send', 'pageview');
            </script>
        ";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  293 => 128,  277 => 126,  273 => 125,  268 => 122,  264 => 120,  260 => 118,  257 => 117,  231 => 93,  228 => 92,  224 => 90,  221 => 89,  217 => 71,  214 => 70,  210 => 37,  194 => 35,  190 => 34,  178 => 24,  175 => 23,  168 => 14,  161 => 13,  154 => 12,  148 => 10,  142 => 157,  140 => 92,  137 => 91,  135 => 89,  123 => 80,  117 => 78,  108 => 71,  106 => 70,  81 => 47,  79 => 46,  70 => 40,  66 => 39,  63 => 38,  61 => 23,  49 => 14,  45 => 13,  41 => 12,  36 => 10,  25 => 1,);
    }
}
