<?php

/* HotelesFrontendBundle:Frontend:privacidad.mv.twig */
class __TwigTemplate_2895579fe6997ff5bac6fb592b978a5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"contenido\">
        <h2>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES PARTICULARES Y PRIVACIDAD"), "html", null, true);
        echo "</h2>
        <div class=\"txtBack\">
            ";
        // line 7
        if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
        if ($this->getAttribute($_content_, "texto", array(), "any", true, true)) {
            // line 8
            echo "                ";
            if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
            echo $this->getAttribute($_content_, "texto");
            echo "
            ";
        }
        // line 10
        echo "        </div>
    </div><!-- contenido -->
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:privacidad.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 10,  40 => 8,  37 => 7,  32 => 5,  29 => 4,  26 => 3,);
    }
}
