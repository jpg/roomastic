<?php

/* HotelesBackendBundle:Quienessomos:edit.html.twig */
class __TwigTemplate_8b13a7be8d16d009439a1d8494d43f18 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar quiénes somos"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "

<div class=\"row\">
  <div class=\"col-lg-6\">
    <section class=\"panel\"> 
        <header class=\"panel-heading\">
            ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar quiénes somos"), "html", null, true);
        echo "
        </header>
        <div class=\"panel-body  ";
        // line 17
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "texto"))), "html", null, true);
        echo "\">
            <form role=\"form\" action=\" ";
        // line 18
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_quienessomos_update", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\"  method=\"post\" ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_edit_form_);
        echo ">
                <label for=\"exampleInputEmail1\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Texto"), "html", null, true);
        echo "</label>
                <!--wysihtml5 start-->
                ";
        // line 21
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "texto"));
        echo "
                ";
        // line 22
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "texto"), array("attr" => array("class" => "wysihtml5 form-control", "rows" => "10")));
        echo "       
                ";
        // line 23
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_edit_form_);
        echo "
                <!--wysihtml5 end-->
        </div>
    </section>
    <!-- btns -->
    <section class=\"panel\">
          <div class=\"panel-body\">
                <button type=\"submit\" class=\"btn btn-info\">";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>   
            </form><!-- / form edit -->
                <a class=\"btn btn-warning\" href=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_quienesSomos"), "html", null, true);
        echo "\" target=\"_blank\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver en la web"), "html", null, true);
        echo "</a>
               ";
        // line 34
        echo "              <form action=\"";
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_quienessomos_delete", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\" method=\"post\" class=\"btnDelete\">
                ";
        // line 35
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($_delete_form_);
        echo "
                <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('¿Está seguro de borrar?');\">";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Borrar"), "html", null, true);
        echo "</button>
            </form>
          </div>
    </section>
    <!-- /btns -->
  </div><!-- /col -->
</div><!-- /row -->

";
        // line 73
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Quienessomos:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 73,  118 => 36,  113 => 35,  107 => 34,  101 => 32,  96 => 30,  85 => 23,  80 => 22,  75 => 21,  70 => 19,  62 => 18,  57 => 17,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
