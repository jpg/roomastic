<?php

/* TwigBundle:Exception:error.html.twig */
class __TwigTemplate_7a179709de5b0381ccd2fa1ac2c96a7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
    <div class=\"contenido pagerror\">
        <p class=\"txt1\">";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lo sentimos, no hemos podido"), "html", null, true);
        echo "<br>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("encontrar el contenido"), "html", null, true);
        echo "</p>
        <p class=\"txt2\">";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("La página que estás buscando no ha sido encontrada."), "html", null, true);
        echo "<br>
            ";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Este problema puede deberse a alguno de estos motivos:"), "html", null, true);
        echo "</p>
        <p>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("La URL no está correctamente escrita o se ha producido un fallo al acceder a ella."), "html", null, true);
        echo "</p>
        <p>";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("El hotel que buscas ya no está disponible."), "html", null, true);
        echo "</p>
        <p>";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Se ha producido un problema con nuestro servidor o con tu conexión a internet."), "html", null, true);
        echo "</p>
        <div class=\"separador\"></div>
        <a href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\"><img src=\"/bundles/hotelesfrontend/img/icon_flecha.png\"></a>
        <p><a href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" class=\"txt_pink\">Vuelve a la home e inicia de nuevo tu búsqueda.</a></p>
        <p>Si después de realizar tu nueva búsqueda sigues con problemas, por favor, contacta con nosotros.</p>

    </div><!-- contenido -->

";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 14,  60 => 13,  55 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 6,  29 => 4,  26 => 3,);
    }
}
