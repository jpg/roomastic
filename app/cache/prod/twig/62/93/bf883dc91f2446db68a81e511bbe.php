<?php

/* HotelesBackendBundle:Extras:empresaStatus.html.twig */
class __TwigTemplate_6293bf883dc91f2446db68a81e511bbe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        if (isset($context["status"])) { $_status_ = $context["status"]; } else { $_status_ = null; }
        if (($_status_ == 0)) {
            // line 7
            echo "    <span class=\"fa-lg\">
        <i class=\"fa fa-check\"></i>
    </span>
";
        } elseif (($_status_ == 1)) {
            // line 11
            echo "    <span class=\"fa-stack fa-lg\">
        <i class=\"fa fa-user fa-stack-1x\"></i>
        <i class=\"fa fa-ban fa-stack-2x text-danger\"></i>
    </span>
";
        } else {
            // line 16
            echo "    <span class=\"fa-lg\">
        <i class=\"fa fa-question\"></i>
    </span>
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:empresaStatus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  33 => 16,  26 => 11,  20 => 7,  17 => 6,  257 => 94,  252 => 91,  244 => 89,  242 => 88,  227 => 75,  206 => 71,  202 => 69,  199 => 68,  193 => 66,  188 => 65,  183 => 64,  175 => 63,  170 => 62,  165 => 61,  160 => 60,  155 => 59,  150 => 58,  145 => 55,  127 => 54,  120 => 50,  116 => 49,  112 => 48,  108 => 47,  104 => 46,  100 => 45,  96 => 44,  92 => 43,  88 => 42,  84 => 41,  80 => 40,  70 => 32,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
