<?php

/* HotelesBackendBundle:UserHotel:edit.html.twig */
class __TwigTemplate_1e187a203da196ab1685e4c45c4547ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar usuarios del hotel"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">
            <section class=\"panel\"> 
                <header class=\"panel-heading\">
                    ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar usuarios del hotel"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <form action=\"";
        // line 17
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_update", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\" method=\"post\"  id=\"formulariosubida\"  ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_edit_form_);
        echo ">
                        <div class=\"form-group ";
        // line 18
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "hotel"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 21
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "hotel"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 24
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "email"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 27
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 30
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrecompleto"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 33
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "nombrecompleto"), array("attr" => array("class" => "form-control")));
        echo "                    
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 36
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cargo"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 39
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "cargo"), array("attr" => array("class" => "form-control")));
        echo "                    
                            </div>
                        </div>
                        <div class=\"form-group \">
                            <label for=\"exampleInputEmail1\">";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imagen de usuario"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                <input type=\"file\" name=\"archivo\" id=\"inputsubida\">
                            </div>
                            <br>
                            <input type=\"hidden\" name=\"imagensubidanombre\" id=\"imagensubidanombre\" value=\"";
        // line 48
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "imagen"), "html", null, true);
        echo "\">
                            ";
        // line 49
        if (isset($context["imagendevuelta"])) { $_imagendevuelta_ = $context["imagendevuelta"]; } else { $_imagendevuelta_ = null; }
        if ((array_key_exists("imagendevuelta", $context) && ($_imagendevuelta_ != ""))) {
            // line 50
            echo "                                <img src=\"/uploads/user/";
            if (isset($context["imagendevuelta"])) { $_imagendevuelta_ = $context["imagendevuelta"]; } else { $_imagendevuelta_ = null; }
            echo twig_escape_filter($this->env, $_imagendevuelta_, "html", null, true);
            echo "\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                            ";
        } else {
            // line 52
            echo "                                <img src=\"\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                            ";
        }
        // line 54
        echo "                            <iframe src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user")), "html", null, true);
        echo "\" height=\"0\" width=\"1300\" style=\"border:0px;\" name=\"inter\"></iframe>
                        </div>
                        ";
        // line 56
        if (isset($context["show_admin"])) { $_show_admin_ = $context["show_admin"]; } else { $_show_admin_ = null; }
        if (($_show_admin_ == true)) {
            // line 57
            echo "                            <div class=\"form-group ";
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "administrador"))), "html", null, true);
            echo "\">
                                <label>
                                    ";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Marca esta casilla si quieres que este usuario administre la ficha del hotel"), "html", null, true);
            echo " 
                                    ";
            // line 60
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "administrador"), array("attr" => array("class" => "")));
            echo "                 
                                </label>
                            </div>
                        ";
        }
        // line 64
        echo "                        ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_edit_form_);
        echo "
                </div>
            </section>
            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-info\">";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    <a href=\"";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
                    ";
        // line 73
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 74
            echo "                        ";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            if ($this->getAttribute($_entity_, "isSuspended", array(), "method")) {
                // line 75
                echo "                            <form action=\"";
                if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_reactivate", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
                echo "\" method=\"post\" class=\"btnDelete\">
                                ";
                // line 76
                if (isset($context["reactivate_form"])) { $_reactivate_form_ = $context["reactivate_form"]; } else { $_reactivate_form_ = null; }
                echo $this->env->getExtension('form')->renderWidget($_reactivate_form_);
                echo "
                                <button type=\"submit\" class=\"btn btn-success\" onclick=\"return confirm('¿Está seguro de reactivar este usuario?');\">";
                // line 77
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Reactivar "), "html", null, true);
                echo "</button>
                            </form>
                        ";
            } else {
                // line 80
                echo "                            <form action=\"";
                if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_delete", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
                echo "\" method=\"post\" class=\"btnDelete\">
                                ";
                // line 81
                if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
                echo $this->env->getExtension('form')->renderWidget($_delete_form_);
                echo "
                                <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('¿Está seguro de borrar?');\">";
                // line 82
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suspender"), "html", null, true);
                echo "</button>
                            </form>
                        ";
            }
            // line 85
            echo "                    ";
        }
        // line 86
        echo "                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->

    ";
        // line 93
        echo "    ";
        // line 94
        echo "    ";
        // line 95
        echo "    ";
        // line 96
        echo "    ";
        // line 97
        echo "
    <a class=\"\" data-toggle=\"modal\" href=\"#myModal\" id=\"clickmodal\"></a>


    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" style=\"width: 923px;margin: 36px auto;height: 508px!important;\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\">";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar imagen"), "html", null, true);
        echo "</h4>
                </div>

                <div style=\"width:100%; height:auto; padding-top: 17px;\" >
                    <img src=\"/bundles/hotelesbackend/img/logo2.png\" id=\"target\" width=\"200\" alt=\"[Jcrop Example]\" />
                </div>

                <div class=\"clearfix\"></div>

                <div class=\"modal-footer\">
                    <button data-dismiss=\"modal\" id=\"closeimage\" class=\"btn btn-default\" type=\"button\">";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cerrar"), "html", null, true);
        echo "</button>
                    <button class=\"btn btn-success\" id=\"successimage\" data-id=\"\" data-url=\"\" data-img=\"\" type=\"button\">";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar cambios"), "html", null, true);
        echo "</button>
                </div>
            </div>
        </div>
    </div>
    ";
        // line 123
        echo "    ";
        // line 124
        echo "    ";
        // line 125
        echo "    ";
        // line 126
        echo "    ";
        // line 127
        echo "
";
    }

    // line 130
    public function block_jsextras($context, array $blocks = array())
    {
        // line 131
        echo "
    ";
        // line 132
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        $context["urlcrea"] = $this->env->getExtension('routing')->getPath("admin_userhotel_update", array("id" => $this->getAttribute($_entity_, "id")));
        // line 133
        echo "    ";
        $context["urlsubidaimagen"] = $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user"));
        // line 134
        echo "    ";
        $context["tipo"] = "user";
        // line 135
        echo "    ";
        $context["crop1"] = "100";
        // line 136
        echo "    ";
        $context["crop2"] = "100";
        // line 137
        echo "    ";
        $context["crop3"] = "100";
        // line 138
        echo "    ";
        $context["crop4"] = "100";
        // line 139
        echo "    ";
        $context["crop5"] = "1";
        // line 140
        echo "    ";
        $context["crop6"] = "1";
        // line 141
        echo "
    ";
        // line 142
        $this->env->loadTemplate("HotelesBackendBundle:Crop:crop.html.twig")->display(array_merge($context, array("especifico" => "user")));
        // line 143
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:UserHotel:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  358 => 143,  356 => 142,  353 => 141,  350 => 140,  347 => 139,  344 => 138,  341 => 137,  338 => 136,  335 => 135,  332 => 134,  329 => 133,  326 => 132,  323 => 131,  320 => 130,  315 => 127,  313 => 126,  311 => 125,  309 => 124,  307 => 123,  299 => 117,  295 => 116,  282 => 106,  271 => 97,  269 => 96,  267 => 95,  265 => 94,  263 => 93,  255 => 86,  252 => 85,  246 => 82,  241 => 81,  235 => 80,  229 => 77,  224 => 76,  218 => 75,  214 => 74,  212 => 73,  206 => 72,  201 => 70,  190 => 64,  182 => 60,  178 => 59,  171 => 57,  168 => 56,  162 => 54,  158 => 52,  151 => 50,  148 => 49,  143 => 48,  135 => 43,  127 => 39,  122 => 37,  117 => 36,  110 => 33,  105 => 31,  100 => 30,  93 => 27,  88 => 25,  83 => 24,  76 => 21,  71 => 19,  66 => 18,  58 => 17,  52 => 14,  45 => 9,  42 => 8,  36 => 5,  31 => 4,  28 => 3,);
    }
}
