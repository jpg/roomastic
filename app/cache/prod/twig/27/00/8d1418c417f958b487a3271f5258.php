<?php

/* HotelesBackendBundle:Lugar:edit.html.twig */
class __TwigTemplate_27008d1418c417f958b487a3271f5258 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar lugar"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
<div class=\"row\">
  <div class=\"col-lg-6\">
    <section class=\"panel\"> 
        <header class=\"panel-heading\">
            ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar lugar"), "html", null, true);
        echo "
        </header>
        <div class=\"panel-body\">
            <form role=\"form\" action=\"";
        // line 17
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_lugares_update", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\"  method=\"post\" ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_edit_form_);
        echo ">
              <div class=\"form-group ";
        // line 18
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "provincia"))), "html", null, true);
        echo "\">
                  <label for=\"exampleInputEmail1\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Provincia"), "html", null, true);
        echo "</label>
                  <div class=\"\">
                  ";
        // line 21
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "provincia"));
        echo "
                  ";
        // line 22
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "provincia"), array("attr" => array("class" => "form-control")));
        echo "                        
                  </div>
              </div>
              <div class=\"form-group ";
        // line 25
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "municipio"))), "html", null, true);
        echo "\">
                  <label for=\"exampleInputEmail1\">";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Municipio"), "html", null, true);
        echo "</label>
                  <div class=\"\">
                  ";
        // line 28
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "municipio"));
        echo "
                  ";
        // line 29
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "municipio"), array("attr" => array("class" => "form-control")));
        echo "                        
                  </div>
              </div>
              <div class=\"form-group ";
        // line 32
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "zona"))), "html", null, true);
        echo "\">
                  <label for=\"exampleInputEmail1\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Zona"), "html", null, true);
        echo "</label>
                  <div class=\"\">
                  ";
        // line 35
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "zona"));
        echo "
                  ";
        // line 36
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "zona"), array("attr" => array("class" => "form-control")));
        echo "                        
                  </div>
              </div>              
               ";
        // line 39
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_edit_form_);
        echo "
        </div>
    </section>
    <!-- btns -->
    <section class=\"panel\">
          <div class=\"panel-body\">
                <button type=\"submit\" class=\"btn btn-info\">";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>   
            </form><!-- / form edit -->
               ";
        // line 48
        echo "               <a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_lugares"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
              <form action=\"";
        // line 49
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_lugares_delete", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\" method=\"post\" class=\"btnDelete\">
                ";
        // line 50
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($_delete_form_);
        echo "
                <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('¿Está seguro de borrar?');\">";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Borrar"), "html", null, true);
        echo "</button>
            </form>
          </div>
    </section>
    <!-- /btns -->
  </div><!-- /col -->
</div><!-- /row -->

";
    }

    // line 83
    public function block_jsextras($context, array $blocks = array())
    {
        // line 84
        echo "    <script type=\"text/javascript\">

        var id = \$('#hoteles_backendbundle_lugartype_provincia option:selected').val()
        \$('#hoteles_backendbundle_lugartype_municipio').load(Routing.generate('admin_lugaresfiltramunicipio', { idprovincia: id, obligatorio : 1 }));

        \$('#hoteles_backendbundle_lugartype_provincia').change(function(event) {
            var id = \$('#hoteles_backendbundle_lugartype_provincia option:selected').val()
            \$('#hoteles_backendbundle_lugartype_municipio').load(Routing.generate('admin_lugaresfiltramunicipio', { idprovincia: id, obligatorio : 1 }));
            
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Lugar:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  180 => 84,  177 => 83,  164 => 51,  159 => 50,  154 => 49,  147 => 48,  142 => 45,  132 => 39,  125 => 36,  120 => 35,  115 => 33,  110 => 32,  103 => 29,  98 => 28,  93 => 26,  88 => 25,  81 => 22,  76 => 21,  71 => 19,  66 => 18,  58 => 17,  52 => 14,  45 => 9,  42 => 8,  36 => 5,  31 => 4,  28 => 3,);
    }
}
