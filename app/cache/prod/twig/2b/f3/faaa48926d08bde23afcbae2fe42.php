<?php

/* HotelesBackendBundle::layout.html.twig */
class __TwigTemplate_2bf3faaa48926d08bde23afcbae2fe42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"Mosaddek\">
        <meta name=\"keyword\" content=\"FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina\">
        <link rel=\"shortcut icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\">

        <title>";
        // line 11
        $this->displayBlock('titulo', $context, $blocks);
        echo "</title>

        <!-- Bootstrap core CSS -->
        <link href=\"/bundles/hotelesbackend/css/bootstrap.min.css\" rel=\"stylesheet\">
        <link href=\"/bundles/hotelesbackend/css/bootstrap-reset.css\" rel=\"stylesheet\">
        <!--external css-->
        <link href=\"/bundles/hotelesbackend/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
        <link href=\"/bundles/hotelesbackend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css\" rel=\"stylesheet\" type=\"text/css\" media=\"screen\"/>
        <link rel=\"stylesheet\" href=\"/bundles/hotelesbackend/css/owl.carousel.css\" type=\"text/css\">
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/bundles/hotelesbackend/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css\" />
        <!-- Custom styles for this template -->
        <link href=\"/bundles/hotelesbackend/css/style.css\" rel=\"stylesheet\">
        <link href=\"/bundles/hotelesbackend/css/style-responsive.css\" rel=\"stylesheet\" />

        <link href=\"/bundles/hotelesbackend/assets/dropzone/css/dropzone.css\" rel=\"stylesheet\"/>

        <link href=\"/bundles/hotelesbackend/css/tasks.css\" rel=\"stylesheet\">

        <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>

        <link rel=\"stylesheet\" href=\"/bundles/hotelesbackend/css/jquery.Jcrop.css\" type=\"text/css\" />

        <link rel=\"stylesheet\" type=\"text/css\" href=\"/bundles/hotelesbackend/assets/bootstrap-fileupload/bootstrap-fileupload.css\" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
          <script src=\"/bundles/hotelesbackend/js/html5shiv.js\"></script>
          <script src=\"/bundles/hotelesbackend/js/respond.min.js\"></script>
        <![endif]-->

        <style type=\"text/css\">
            .withbuttons {
                margin-top: -47px!important;
                margin-bottom: -15px!important;
            }  
            .withoutbuttons {
                margin-top: -15px!important;
                margin-bottom: -15px!important;
            }
            .errortab a{
                color: #a94442!important;
            } 
            .disabled span{
                padding: 0 10px;
                height: 28px;
            }     
        </style>

        <style type=\"text/css\">

            /* Apply these styles only when #preview-pane has
               been placed within the Jcrop widget */
            .jcrop-holder #preview-pane {
                display: block;
                position: absolute;
                z-index: 2000;
                top: 10px;
                right: -280px;
                padding: 6px;
                border: 1px rgba(0,0,0,.4) solid;
                background-color: white;

                -webkit-border-radius: 6px;
                -moz-border-radius: 6px;
                border-radius: 6px;

                -webkit-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
                -moz-box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
                box-shadow: 1px 1px 5px 2px rgba(0, 0, 0, 0.2);
            }

            /* The Javascript code will set the aspect ratio of the crop
               area based on the size of the thumbnail preview,
               specified here */
            #preview-pane .preview-container {
                width: 250px;
                height: 170px;
                overflow: hidden;
            }

            .jcrop-holder{
                margin: 0 auto!important;
            }

            .jcrop-keymgr{
                display: none!important;
            }

        </style>

    </head>

    <body>

        <section id=\"container\" >
            <!--header start-->
            <header class=\"header white-bg\">
                <div class=\"sidebar-toggle-box\">
                    <div class=\"fa fa-bars tooltips\" data-placement=\"right\" data-original-title=\"Toggle Navigation\"></div>
                </div>
                <!--logo start-->
                <a href=\"";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing"), "html", null, true);
        echo "\" class=\"logo\"><img src=\"/bundles/hotelesbackend/img/logo2.png\"></a>
                <!--logo end-->
                <div class=\"nav notify-row\" id=\"top_menu\">
                    <!--  notification start -->
                    <ul class=\"nav top-menu\">
                        ";
        // line 118
        $context["incidencias"] = $this->env->getExtension('twig_extension')->incidencias_backend($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"));
        // line 119
        echo "                        <!-- inbox dropdown start-->
                        <li id=\"header_inbox_bar\" class=\"dropdown\">
                            <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">
                                <i class=\"fa fa-exclamation\"></i>
                                ";
        // line 123
        if ((twig_length_filter($this->env, (isset($context["incidencias"]) ? $context["incidencias"] : null)) > 0)) {
            // line 124
            echo "                                    <span class=\"badge bg-important\">";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["incidencias"]) ? $context["incidencias"] : null)), "html", null, true);
            echo "</span>
                                ";
        }
        // line 126
        echo "                            </a>
                            <ul class=\"dropdown-menu extended inbox\">
                                <div class=\"notify-arrow notify-arrow-red\"></div>
                                <li>
                                    <p class=\"red\"> ";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tienes"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["incidencias"]) ? $context["incidencias"] : null)), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nuevos mensajes internos"), "html", null, true);
        echo "</p>
                                </li>
                                ";
        // line 132
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["incidencias"]) ? $context["incidencias"] : null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["incidencia"]) {
            // line 133
            echo "                                    ";
            $this->env->loadTemplate("HotelesBackendBundle:NotificacionesHeader:incidencia.html.twig")->display(array_merge($context, array("incidencia" => (isset($context["incidencia"]) ? $context["incidencia"] : null))));
            echo "                            
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['incidencia'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 135
        echo "                                <li>
                                    <a href=\"";
        // line 136
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver todos los mensajes"), "html", null, true);
        echo "</a>
                                </li>                       
                            </ul>
                        </li>
                        <!-- inbox dropdown end -->

                        ";
        // line 142
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 143
            echo "
                            ";
            // line 144
            $context["contactos"] = $this->env->getExtension('twig_extension')->contactos_backend();
            // line 145
            echo "
                            <!-- notification dropdown start-->
                            <li id=\"header_notification_bar\" class=\"dropdown\">
                                <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">
                                    <i class=\"fa fa-envelope-o\"></i>
                                    ";
            // line 150
            if ((twig_length_filter($this->env, (isset($context["contactos"]) ? $context["contactos"] : null)) > 0)) {
                // line 151
                echo "                                        <span class=\"badge bg-warning\">";
                echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["contactos"]) ? $context["contactos"] : null)), "html", null, true);
                echo "</span>
                                    ";
            }
            // line 153
            echo "                                </a>
                                <ul class=\"dropdown-menu extended inbox\">
                                    <div class=\"notify-arrow notify-arrow-red\"></div>
                                    <li>
                                        <p class=\"red\"> ";
            // line 157
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tienes"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["contactos"]) ? $context["contactos"] : null)), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("nuevos mensajes externos"), "html", null, true);
            echo "</p>
                                    </li>
                                    ";
            // line 159
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["contactos"]) ? $context["contactos"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["contacto"]) {
                // line 160
                echo "                                        ";
                $this->env->loadTemplate("HotelesBackendBundle:NotificacionesHeader:contacto.html.twig")->display(array_merge($context, array("contacto" => (isset($context["contacto"]) ? $context["contacto"] : null))));
                echo "       
                                                                    
                                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contacto'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 163
            echo "                                    <li>
                                        <a href=\"";
            // line 164
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver todos los mensajes"), "html", null, true);
            echo "</a>
                                    </li>                       
                                </ul>

                            </li>
                            <!-- notification dropdown end -->

                        ";
        }
        // line 172
        echo "
                        ";
        // line 173
        if (($this->env->getExtension('security')->isGranted("ROLE_HOTEL") && ((null === $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hotel"), "notificaciones")) == false))) {
            // line 174
            echo "                            <!-- inbox notificaciones start-->
                            ";
            // line 175
            $context["notificaciones"] = $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "hotel"), "notificaciones");
            // line 176
            echo "                            <li id=\"header_inbox_bar\" class=\"dropdown\">
                                <a data-toggle=\"dropdown\" class=\"dropdown-toggle medalla_notificaciones\" href=\"#\">
                                    <i class=\"fa fa-bell-o\"></i>
                                    ";
            // line 179
            if (($this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getNumNotificaciones") > 0)) {
                // line 180
                echo "                                        <span class=\"badge bg-important\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getNumNotificaciones"), "html", null, true);
                echo "</span>
                                    ";
            }
            // line 182
            echo "                                </a>
                                <ul class=\"dropdown-menu extended notification\">
                                    <div class=\"notify-arrow notify-arrow-red\"></div>
                                    <li>
                                        <p class=\"red\">";
            // line 186
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Tienes"), "html", null, true);
            echo " <span class=\"total_notificaciones\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getNumNotificaciones"), "html", null, true);
            echo "</span> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("notificaciones nuevas"), "html", null, true);
            echo "</p>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 189
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "no-pagadas")), "html", null, true);
            echo "\">
                                            <span class=\"label label-danger\"><i class=\"fa fa-bolt\"></i></span>
                                            <strong><span class=\"ofertas_no_pagadas\">";
            // line 191
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getOfertasNoPagadas"), "html", null, true);
            echo "</span></strong> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas no pagadas"), "html", null, true);
            echo "
                                                ";
            // line 192
            if (($this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getOfertasNoPagadas") != 0)) {
                // line 193
                echo "                                                <span class=\"small italic\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('time_ago_extension')->timeAgoInWordsFilter($this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getFechaUltimaOfertaNoPagada")), "html", null, true);
                echo "</span>
                                            ";
            }
            // line 195
            echo "                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 198
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "pendientes")), "html", null, true);
            echo "\">
                                            <span class=\"label label-warning\"><i class=\"fa fa-bell\"></i></span>
                                            <strong><span class=\"ofertas_pendientes\">";
            // line 200
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getOfertasPendientes"), "html", null, true);
            echo "</span></strong> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas pendientes"), "html", null, true);
            echo "
                                                ";
            // line 201
            if (($this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getOfertasPendientes") != 0)) {
                // line 202
                echo "                                                <span class=\"small italic\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('time_ago_extension')->timeAgoInWordsFilter($this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getFechaUltimaOfertaPendiente")), "html", null, true);
                echo "</span>
                                            ";
            }
            // line 204
            echo "
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 208
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "pagadas")), "html", null, true);
            echo "\">
                                            <span class=\"label label-success\"><i class=\"fa fa-plus\"></i></span>
                                            <strong><span class=\"ofertas_pagadas\"> ";
            // line 210
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getOfertasPagadas"), "html", null, true);
            echo "</span></strong> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas pagadas"), "html", null, true);
            echo "
                                            ";
            // line 211
            if (($this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getOfertasPagadas") != 0)) {
                // line 212
                echo "                                                <span class=\"small italic\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('time_ago_extension')->timeAgoInWordsFilter($this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getFechaUltimaOfertaPagada")), "html", null, true);
                echo "</span>
                                            ";
            }
            // line 214
            echo "
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 218
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "todos")), "html", null, true);
            echo "\">
                                            <span class=\"label label-info\"><i class=\"fa fa-bullhorn\"></i></span>
                                            <strong><span class=\"contraofertas_perdidas\">";
            // line 220
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getContraofertasPerdidas"), "html", null, true);
            echo "</span></strong> ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraofertas perdidas"), "html", null, true);
            echo "
                                                ";
            // line 221
            if (($this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getContraofertasPerdidas") != 0)) {
                // line 222
                echo "                                                <span class=\"small italic\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('time_ago_extension')->timeAgoInWordsFilter($this->getAttribute((isset($context["notificaciones"]) ? $context["notificaciones"] : null), "getFechaUltimaContraofertaPerdida")), "html", null, true);
                echo "</span>
                                            ";
            }
            // line 224
            echo "
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"";
            // line 228
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver todas las ofertas"), "html", null, true);
            echo "</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- inbox notificaciones end -->
                        ";
        }
        // line 234
        echo "
                        ";
        // line 235
        if ($this->env->getExtension('security')->isGranted("ROLE_PREVIOUS_ADMIN")) {
            // line 236
            echo "                            <li>
                                <a href=\"";
            // line 237
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing", array("_want_to_be_this_user" => "_exit")), "html", null, true);
            echo "\"><i class=\"fa fa-power-off\"></i></a>
                            </li>
                        ";
        }
        // line 239
        echo " 

                        ";
        // line 252
        echo "
                    </ul>
                    <!--  notification end -->
                </div>
                <div class=\"top-nav \">
                    <!--search & user info start-->
                    <ul class=\"nav pull-right top-menu\">
                        ";
        // line 264
        echo "                        <!-- user login dropdown start-->
                        <li class=\"dropdown\">

                            <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"#\">
                                ";
        // line 268
        if (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array(), "any", false, true), "imagen", array(), "any", true, true) && ($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "imagen") != ""))) {
            // line 269
            echo "                                    <img width=\"29\" height=\"29\" alt=\"\" src=\"/uploads/user/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "imagen"), "html", null, true);
            echo "\">
                                ";
        } else {
            // line 271
            echo "                                    <img width=\"29\" height=\"29\" alt=\"\" src=\"/bundles/hotelesbackend/img/avatar.jpg\">
                                ";
        }
        // line 273
        echo "                                <span class=\"username\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "getPublicUsername"), "html", null, true);
        echo "</span>
                                <b class=\"caret\"></b>
                            </a>

                            <ul class=\"dropdown-menu extended logout\">
                                <div class=\"log-arrow-up\"></div>
                                <li>
                                    <a href=\"";
        // line 280
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_logout"), "html", null, true);
        echo "\"><i class=\"fa fa-key\"></i>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Salir"), "html", null, true);
        echo "</a>
                                </li>
                                ";
        // line 282
        if ($this->env->getExtension('security')->isGranted("ROLE_PREVIOUS_ADMIN")) {
            // line 283
            echo "                                    <li>
                                        <a href=\"";
            // line 284
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing", array("_want_to_be_this_user" => "_exit")), "html", null, true);
            echo "\"><i class=\"fa fa-power-off\"></i>";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dejar suplantación"), "html", null, true);
            echo "</a>
                                    </li>
                                ";
        }
        // line 287
        echo "
                            </ul>
                        </li>
                        <!-- user login dropdown end -->
                    </ul>
                    <!--search & user info end-->
                </div>
            </header>
            <!--header end-->
            <!--sidebar start-->
            <aside>
                <div id=\"sidebar\"  class=\"nav-collapse \">
                    <!-- sidebar menu start-->
                    <ul class=\"sidebar-menu\" id=\"nav-accordion\">

                        <li>
                            <a class=\"";
        // line 303
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_landing", "single"), "html", null, true);
        echo "\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing"), "html", null, true);
        echo "\">
                                <i class=\"fa fa-dashboard\"></i>
                                <span>";
        // line 305
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dashboard"), "html", null, true);
        echo "</span>
                            </a>
                        </li>

                        ";
        // line 309
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 310
            echo "
                            <li>
                                <a class=\"";
            // line 312
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_usuario", "complex"), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario"), "html", null, true);
            echo "\">
                                    <i class=\"fa fa-user\"></i>
                                    <span>";
            // line 314
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuarios"), "html", null, true);
            echo "</span>
                                </a>
                            </li>

                        ";
        }
        // line 319
        echo "
                        <li>
                            <a class=\"";
        // line 321
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_incidencia", "complex"), "html", null, true);
        echo "\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia"), "html", null, true);
        echo "\">
                                <i class=\"fa fa-exclamation\"></i>
                                <span>";
        // line 323
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Incidencias"), "html", null, true);
        echo "</span>
                            </a>
                        </li>

                        ";
        // line 327
        if ((($this->env->getExtension('security')->isGranted("ROLE_ADMIN") || $this->env->getExtension('security')->isGranted("ROLE_HOTEL")) || $this->env->getExtension('security')->isGranted("ROLE_EMPRESA"))) {
            // line 328
            echo "                            <li class=\"sub-menu\">
                                <a class=\"";
            // line 329
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_ofertas", "complex"), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas"), "html", null, true);
            echo "\">
                                    <i class=\"fa fa-eur\"></i>
                                    <span>";
            // line 331
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas"), "html", null, true);
            echo "</span>

                                </a>
                                <ul class=\"sub\">
                                    <li class=\"";
            // line 335
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "contraofertas")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 336
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "contraofertas")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraofertas"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 338
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "pendientes")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 339
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "pendientes")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pendientes"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 341
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "aceptadas")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 342
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "aceptadas")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptadas"), "html", null, true);
            echo "</a>
                                    </li>     
                                    <li class=\"";
            // line 344
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "rechazadas")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 345
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "rechazadas")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rechazadas"), "html", null, true);
            echo "</a>
                                    </li>     
                                    <li class=\"";
            // line 347
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "pagadas")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 348
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "pagadas")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pagadas"), "html", null, true);
            echo "</a>
                                    </li>  
                                    <li class=\"";
            // line 350
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "no-pagadas")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 351
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "no-pagadas")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No pagadas"), "html", null, true);
            echo "</a>
                                    </li>  
                                    <li class=\"";
            // line 353
            echo ((($this->env->getExtension('twig_extension')->routerParams("filtro") == "todos")) ? ("active") : (""));
            echo "\" >
                                        <a  href=\"";
            // line 354
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "todos")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Todos"), "html", null, true);
            echo "</a>
                                    </li>  

                                </ul>    
                            </li>
                        ";
        }
        // line 360
        echo "
                        ";
        // line 361
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 362
            echo "
                            <li class=\"sub-menu\">
                                <a href=\"javascript:;\" class=\"";
            // line 364
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_useradministrador", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_lugares", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_contactoweb", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_configuracion", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_newsletter", "complex"), "html", null, true);
            echo "  dcjq-parent\">
                                    <i class=\"fa fa-cogs\"></i>
                                    <span>";
            // line 366
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administración"), "html", null, true);
            echo "</span>
                                </a>
                                <ul class=\"sub\">
                                    <li class=\"";
            // line 369
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_useradministrador", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 370
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_useradministrador"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuarios"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 372
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_lugares", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 373
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_lugares"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lugares"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 375
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_contactoweb", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 376
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contacto"), "html", null, true);
            echo "</a>
                                    </li>     
                                    <li class=\"";
            // line 378
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_configuracion", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 379
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_configuracion"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Configuración"), "html", null, true);
            echo "</a>
                                    </li>     
                                    <li class=\"";
            // line 381
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_newsletter", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 382
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_newsletter"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Newsletter"), "html", null, true);
            echo "</a>
                                    </li>
                                </ul>
                            </li>

                        ";
        }
        // line 388
        echo "                        ";
        // line 389
        echo "                        ";
        if (($this->env->getExtension('security')->isGranted("ROLE_ADMIN") || $this->env->getExtension('security')->isGranted("ROLE_EMPRESA"))) {
            // line 390
            echo "
                            <li class=\"sub-menu\">
                                <a href=\"javascript:;\" class=\"";
            // line 392
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_empresa", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_userempresa", "complex"), "html", null, true);
            echo " dcjq-parent\">
                                    <i class=\"fa fa-sitemap\"></i>
                                    <span>";
            // line 394
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
            echo "</span>
                                </a>
                                <ul class=\"sub\">
                                    <li class=\"";
            // line 397
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_empresa", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 398
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administración"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 400
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_userempresa", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 401
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuarios"), "html", null, true);
            echo "</a>
                                    </li>
                                </ul>
                            </li>                            
                        ";
        }
        // line 406
        echo "                        ";
        // line 407
        echo "                        <li class=\"sub-menu\">
                            <a href=\"javascript:;\" class=\"";
        // line 408
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_hotel", "complex"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_userhotel", "complex"), "html", null, true);
        echo " dcjq-parent\">
                                <i class=\"fa fa-suitcase\"></i>
                                <span>";
        // line 410
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
        echo "</span>
                            </a>
                            <ul class=\"sub\">
                                ";
        // line 413
        if (((($this->env->getExtension('security')->isGranted("ROLE_HOTEL") && $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user"), "isAdmin")) || $this->env->getExtension('security')->isGranted("ROLE_ADMIN")) || $this->env->getExtension('security')->isGranted("ROLE_EMPRESA"))) {
            // line 414
            echo "                                    <li class=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_hotel", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 415
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administración"), "html", null, true);
            echo "</a>
                                    </li>
                                ";
        }
        // line 418
        echo "                                <li class=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_userhotel", "complex"), "html", null, true);
        echo "\" >
                                    <a  href=\"";
        // line 419
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuarios"), "html", null, true);
        echo "</a>
                                </li>
                            </ul>
                        </li>


                        ";
        // line 425
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 426
            echo "
                            <li class=\"sub-menu\">
                                <a href=\"javascript:;\" class=\"";
            // line 428
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_condicioneslegales", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_privacidad", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_cookies", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_faqs", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_quienessomos", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_seo", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_homeconfiguracion", "complex"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_publicatuhotel", "complex"), "html", null, true);
            echo " dcjq-parent\">
                                    <i class=\"fa fa-desktop\"></i>
                                    <span>";
            // line 430
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Web"), "html", null, true);
            echo "</span>
                                </a>
                                <ul class=\"sub\">
                                    <li class=\"";
            // line 433
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_homeconfiguracion", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 434
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_homeconfiguracion"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imágenes home"), "html", null, true);
            echo "</a>
                                    </li>                            
                                    <li class=\"";
            // line 436
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_quienessomos", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 437
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_quienessomos"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Quiénes somos"), "html", null, true);
            echo "</a>
                                    </li>                        
                                    <li class=\"";
            // line 439
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_condicioneslegales", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 440
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_condicioneslegales"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Condiciones legales"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 442
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_privacidad", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 443
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_privacidad"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Privacidad"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 445
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_publicatuhotel", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 446
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_publicatuhotel"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Publica tu hotel"), "html", null, true);
            echo "</a>
                                    </li>                            
                                    <li class=\"";
            // line 448
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_cookies", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 449
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_cookies"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cookies"), "html", null, true);
            echo "</a>
                                    </li>
                                    <li class=\"";
            // line 451
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_faqs", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 452
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_faqs"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Preguntas frecuentes"), "html", null, true);
            echo "</a>
                                    </li>       
                                    <li class=\"";
            // line 454
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->active_menu($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "get", array(0 => "_route"), "method"), "admin_seo", "complex"), "html", null, true);
            echo "\" >
                                        <a  href=\"";
            // line 455
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_seo"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("SEO"), "html", null, true);
            echo "</a>
                                    </li>  
                                </ul>
                            </li>

                        ";
        }
        // line 461
        echo "

                    </ul>
                    <!-- sidebar menu end-->
                </div>
            </aside>

            <!--sidebar end-->
            <section id=\"main-content\">
                <section class=\"wrapper\">

                    <div class=\"row\">
                        ";
        // line 484
        echo "                    </div>
                    <!-- avisos ok y ko -->
                    ";
        // line 486
        $this->env->loadTemplate("HotelesBackendBundle:Avisos:success.html.twig")->display(array_merge($context, array("cabecera" => "¡Genial!")));
        // line 487
        echo "                    ";
        $this->env->loadTemplate("HotelesBackendBundle:Avisos:error.html.twig")->display(array_merge($context, array("cabecera" => "¡Vaya!")));
        // line 488
        echo "
                    ";
        // line 489
        $this->displayBlock('body', $context, $blocks);
        // line 491
        echo "                </section>
            </section>

            <!--footer start-->
            <footer class=\"site-footer\">
                <div class=\"text-center\">
                    2014 &copy; Roomastic
                    <a href=\"#\" class=\"go-top\">
                        <i class=\"fa fa-angle-up\"></i>
                    </a>
                </div>
            </footer>
            <!--footer end-->
        </section>
        ";
        // line 505
        $this->displayBlock('javascripts', $context, $blocks);
        // line 617
        echo "
    </body>
</html>
";
    }

    // line 11
    public function block_titulo($context, array $blocks = array())
    {
        echo "Roomastic Backend - ";
    }

    // line 489
    public function block_body($context, array $blocks = array())
    {
        // line 490
        echo "                    ";
    }

    // line 505
    public function block_javascripts($context, array $blocks = array())
    {
        // line 506
        echo "            

            <!-- js placed at the end of the document so the pages load faster -->
            <script src=\"/bundles/hotelesbackend/js/jquery.js\"></script>
            <script src=\"/bundles/hotelesbackend/js/jquery-1.8.3.min.js\"></script>
            <script src=\"/bundles/hotelesbackend/js/bootstrap.js\"></script>
            <script class=\"include\" type=\"text/javascript\" src=\"/bundles/hotelesbackend/js/jquery.dcjqaccordion.2.7.js\"></script>
            <script src=\"/bundles/hotelesbackend/js/jquery.scrollTo.min.js\"></script>
            <script src=\"/bundles/hotelesbackend/js/jquery.nicescroll.js\" type=\"text/javascript\"></script>
            <script src=\"/bundles/hotelesbackend/js/jquery.sparkline.js\" type=\"text/javascript\"></script>
            <script src=\"/bundles/hotelesbackend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js\"></script>
            <script src=\"/bundles/hotelesbackend/js/owl.carousel.js\" ></script>
            <script src=\"/bundles/hotelesbackend/js/jquery.customSelect.min.js\" ></script>
            <script src=\"/bundles/hotelesbackend/js/respond.min.js\" ></script>

            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-wysihtml5/wysihtml5-0.3.0.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-wysihtml5/bootstrap-wysihtml5.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-datepicker/js/bootstrap-datepicker.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-daterangepicker/moment.min.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-daterangepicker/daterangepicker.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-colorpicker/js/bootstrap-colorpicker.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/bootstrap-timepicker/js/bootstrap-timepicker.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/jquery-multi-select/js/jquery.multi-select.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesbackend/assets/jquery-multi-select/js/jquery.quicksearch.js\"></script>

            <script src=\"https://code.jquery.com/ui/1.10.3/jquery-ui.js\"></script>

            <!--common script for all pages-->
            <script src=\"/bundles/hotelesbackend/js/common-scripts.js\"></script>

            <!--script for this page-->
            <script src=\"/bundles/hotelesbackend/js/sparkline-chart.js\"></script>

            <script type=\"text/javascript\" src=\"https://maps.google.com/maps/api/js?sensor=true\"></script>
            <script src=\"/bundles/hotelesbackend/js/gmaps.js\"></script>

            <script src=\"/bundles/hotelesbackend/js/tasks.js\" type=\"text/javascript\"></script>

            <script src=\"/bundles/hotelesbackend/js/advanced-form-components.js\"></script>

            <script src=\"/bundles/hotelesbackend/js/bootstrap-switch.js\"></script>

            <script src=\"/bundles/hotelesbackend/assets/bootstrap-fileupload/bootstrap-fileupload.js\"></script>

            ";
        // line 554
        echo "            <script src=\"/bundles/hotelesbackend/js/jquery.Jcrop.js\"></script>
            ";
        // line 555
        if (($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "debug") == true)) {
            // line 556
            echo "                <script>
                \$.ajaxSetup ({
                    // Disable caching of AJAX responses
                    cache: false
                });
                </script>
            ";
        }
        // line 563
        echo "            ";
        if ($this->env->getExtension('security')->isGranted("ROLE_HOTEL")) {
            // line 564
            echo "                ";
            if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
                // asset "10d7913_0"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_10d7913_0") : $this->env->getExtension('assets')->getAssetUrl("js/10d7913_notificaciones_1.js");
                // line 565
                echo "                <script type=\"text/javascript\" src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
                echo "\"></script>
                ";
            } else {
                // asset "10d7913"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_10d7913") : $this->env->getExtension('assets')->getAssetUrl("js/10d7913.js");
                echo "                <script type=\"text/javascript\" src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : null), "html", null, true);
                echo "\"></script>
                ";
            }
            unset($context["asset_url"]);
            // line 567
            echo "            ";
        }
        // line 568
        echo "            <script type=\"text/javascript\">
                jQuery('#numSelect').change(function(event) {
                ";
        // line 570
        if ((twig_length_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "query"), "all")) == 0)) {
            // line 571
            echo "                        window.location.href = window.location.pathname + '?show=' + jQuery('#numSelect :selected').val()
                ";
        } else {
            // line 573
            echo "                        var url = '';
                    ";
            // line 574
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request"), "query"), "all"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["k"] => $context["v"]) {
                // line 575
                echo "                            console.log('";
                echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                echo "');
                        ";
                // line 576
                if (($this->getAttribute((isset($context["loop"]) ? $context["loop"] : null), "index") == 1)) {
                    // line 577
                    echo "                                url = url + '?'
                        ";
                } else {
                    // line 579
                    echo "                                url = url + '&'
                        ";
                }
                // line 581
                echo "                        ";
                if (((isset($context["k"]) ? $context["k"] : null) == "show")) {
                    // line 582
                    echo "                                url = url + '";
                    echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                    echo "' + '=' + jQuery('#numSelect :selected').val()
                        ";
                } else {
                    // line 584
                    echo "                                url = url + '";
                    echo twig_escape_filter($this->env, (isset($context["k"]) ? $context["k"] : null), "html", null, true);
                    echo "' + '='
                                        +";
                    // line 585
                    echo twig_escape_filter($this->env, (isset($context["v"]) ? $context["v"] : null), "html", null, true);
                    echo " ;
                        ";
                }
                // line 587
                echo "                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['k'], $context['v'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 588
            echo "                            window.location.href = window.location.pathname + url;
                ";
        }
        // line 590
        echo "                    });
            </script>

            <script>

                //owl carousel
                \$(document).ready(function() {
                    \$(\"#owl-demo\").owlCarousel({
                        navigation: true,
                        slideSpeed: 300,
                        paginationSpeed: 400,
                        singleItem: true,
                        autoPlay: true

                    });
                });

                //custom select box
                \$(function() {
                    \$('select.styled').customSelect();
                });

            </script>

            ";
        // line 614
        $this->displayBlock('jsextras', $context, $blocks);
        // line 616
        echo "        ";
    }

    // line 614
    public function block_jsextras($context, array $blocks = array())
    {
        // line 615
        echo "            ";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1293 => 615,  1290 => 614,  1286 => 616,  1284 => 614,  1258 => 590,  1254 => 588,  1240 => 587,  1235 => 585,  1230 => 584,  1224 => 582,  1221 => 581,  1217 => 579,  1213 => 577,  1211 => 576,  1206 => 575,  1189 => 574,  1186 => 573,  1182 => 571,  1180 => 570,  1176 => 568,  1173 => 567,  1159 => 565,  1154 => 564,  1151 => 563,  1142 => 556,  1140 => 555,  1137 => 554,  1090 => 506,  1087 => 505,  1083 => 490,  1080 => 489,  1074 => 11,  1067 => 617,  1065 => 505,  1049 => 491,  1047 => 489,  1044 => 488,  1041 => 487,  1039 => 486,  1035 => 484,  1021 => 461,  1010 => 455,  1006 => 454,  999 => 452,  995 => 451,  988 => 449,  984 => 448,  977 => 446,  973 => 445,  966 => 443,  962 => 442,  955 => 440,  951 => 439,  944 => 437,  940 => 436,  933 => 434,  929 => 433,  923 => 430,  904 => 428,  900 => 426,  898 => 425,  887 => 419,  882 => 418,  874 => 415,  869 => 414,  867 => 413,  861 => 410,  854 => 408,  851 => 407,  849 => 406,  839 => 401,  835 => 400,  828 => 398,  824 => 397,  818 => 394,  811 => 392,  807 => 390,  804 => 389,  802 => 388,  791 => 382,  787 => 381,  780 => 379,  776 => 378,  769 => 376,  765 => 375,  758 => 373,  754 => 372,  747 => 370,  743 => 369,  737 => 366,  724 => 364,  720 => 362,  718 => 361,  715 => 360,  704 => 354,  700 => 353,  693 => 351,  689 => 350,  682 => 348,  678 => 347,  671 => 345,  667 => 344,  660 => 342,  656 => 341,  649 => 339,  645 => 338,  638 => 336,  634 => 335,  627 => 331,  620 => 329,  617 => 328,  615 => 327,  608 => 323,  601 => 321,  597 => 319,  589 => 314,  582 => 312,  578 => 310,  576 => 309,  569 => 305,  562 => 303,  544 => 287,  536 => 284,  533 => 283,  531 => 282,  524 => 280,  509 => 271,  503 => 269,  501 => 268,  495 => 264,  486 => 252,  476 => 237,  473 => 236,  471 => 235,  468 => 234,  457 => 228,  451 => 224,  445 => 222,  443 => 221,  437 => 220,  432 => 218,  426 => 214,  420 => 212,  418 => 211,  407 => 208,  401 => 204,  395 => 202,  393 => 201,  387 => 200,  377 => 195,  371 => 193,  369 => 192,  358 => 189,  348 => 186,  342 => 182,  336 => 180,  334 => 179,  329 => 176,  327 => 175,  324 => 174,  322 => 173,  319 => 172,  306 => 164,  303 => 163,  285 => 160,  268 => 159,  259 => 157,  253 => 153,  247 => 151,  245 => 150,  238 => 145,  236 => 144,  231 => 142,  220 => 136,  217 => 135,  200 => 133,  183 => 132,  174 => 130,  168 => 126,  162 => 124,  160 => 123,  154 => 119,  152 => 118,  144 => 113,  54 => 29,  33 => 11,  21 => 1,  537 => 260,  529 => 254,  513 => 273,  511 => 251,  506 => 249,  502 => 248,  499 => 247,  482 => 239,  474 => 241,  469 => 238,  467 => 237,  458 => 230,  452 => 227,  448 => 226,  442 => 222,  440 => 221,  433 => 219,  427 => 218,  421 => 214,  412 => 210,  404 => 207,  394 => 200,  390 => 199,  384 => 195,  382 => 198,  376 => 190,  370 => 188,  368 => 187,  363 => 191,  356 => 181,  343 => 170,  339 => 164,  335 => 163,  326 => 157,  312 => 146,  307 => 144,  302 => 142,  294 => 137,  290 => 136,  286 => 135,  282 => 134,  278 => 133,  262 => 120,  254 => 119,  242 => 109,  233 => 143,  216 => 101,  210 => 98,  206 => 96,  188 => 95,  185 => 94,  176 => 91,  173 => 90,  169 => 89,  164 => 87,  159 => 85,  145 => 74,  129 => 61,  113 => 48,  97 => 35,  80 => 21,  76 => 20,  72 => 19,  68 => 18,  58 => 30,  55 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
