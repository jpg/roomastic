<?php

/* HotelesFrontendBundle:TPV:pago-terminado.html.twig */
class __TwigTemplate_d99547033bd1770d3541a792549cb584 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle:TPV:layoutTPV.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'resultado_venta' => array($this, 'block_resultado_venta'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle:TPV:layoutTPV.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"contenido confirmaciontpv\">
        ";
        // line 5
        $this->displayBlock('resultado_venta', $context, $blocks);
        // line 7
        echo "        <div class=\"separador\"></div>
        <div class=\"bq\">
            <div class=\"tit\">";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("El hotel"), "html", null, true);
        echo "</div>
            <div class=\"cont clearfix\">
                <p class=\"name\">";
        // line 11
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "nombrehotel"), "html", null, true);
        echo "</p>
                <p>";
        // line 12
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "ubicacion"), "html", null, true);
        echo "</p>
                <div class=\"estrellas\">
                    <img src=\"/bundles/hotelesfrontend/img/";
        // line 14
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "calificacion"), "html", null, true);
        echo "stars.png\">
                </div>
                <div class=\"left\">
                    <img src=\"";
        // line 17
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, (($this->getAttribute($_hotel_, "getImagePath") . "/") . $this->getAttribute($this->getAttribute($this->getAttribute($_hotel_, "getOrderedImagenesDelHotel"), 0, array(), "array"), "imagen")), "html", null, true);
        echo "\" class=\"imgppal\">
                </div>
                <div class=\"right\">
                    <ul class=\"servicios\">
                        ";
        // line 21
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "piscina") == 1)) {
            // line 22
            echo "                            <li class=\"piscina\">
                                <label for=\"piscina\">";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Piscina"), "html", null, true);
            echo "</label>
                            </li>
                        ";
        }
        // line 26
        echo "
                        ";
        // line 27
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "spa") == 1)) {
            // line 28
            echo "                            <li class=\"spa\">
                                <label for=\"Spa\">";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Spa"), "html", null, true);
            echo "</label>
                            </li>
                        ";
        }
        // line 32
        echo "
                        ";
        // line 33
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "wiFi") == 1)) {
            // line 34
            echo "                            <li class=\"wi-fi\">
                                <label for=\"Bar\">";
            // line 35
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wi-Fi"), "html", null, true);
            echo "</label>
                            </li>\t
                        ";
        }
        // line 38
        echo "
                        ";
        // line 39
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "accesoAdaptado") == 1)) {
            // line 40
            echo "                            <li class=\"acceso-adaptado\">
                                <label for=\"Acceso-Adaptado\">";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso adaptado"), "html", null, true);
            echo "</label>
                            </li>
                        ";
        }
        // line 44
        echo "
                        ";
        // line 45
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "aceptanPerros") == 1)) {
            // line 46
            echo "                            <li class=\"perros\">
                                <label for=\"perros\">";
            // line 47
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptan perros"), "html", null, true);
            echo "</label>
                            </li>
                        ";
        }
        // line 50
        echo "
                        ";
        // line 51
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "aparcamiento") == 1)) {
            // line 52
            echo "                            <li class=\"parking\">
                                <label for=\"parking\">";
            // line 53
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aparcamiento/Parking"), "html", null, true);
            echo "</label>
                            </li>
                        ";
        }
        // line 56
        echo "
                        ";
        // line 57
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "businessCenter") == 1)) {
            // line 58
            echo "                            <li class=\"business-center\">
                                <label for=\"business-center\">";
            // line 59
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Business center"), "html", null, true);
            echo "</label>
                            </li>
                        ";
        }
        // line 62
        echo "                    </ul>

                </div>
            </div>

        </div>
        <div class=\"bq\">
            <div class=\"tit\">";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la reserva"), "html", null, true);
        echo "</div>
            <div class=\"cont\">
                <ul class=\"datos\">
                    <li class=\"left\">
                        <p>";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha de entrada"), "html", null, true);
        echo "</p>
                        <p><span>";
        // line 74
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_oferta_, "fechain"), "d/m/Y"), "html", null, true);
        echo "</span></p>
                    </li>
                    <li class=\"left\">
                        <p>";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de adultos"), "html", null, true);
        echo "</p>
                        <p><span>";
        // line 78
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_oferta_, "numadultos"), "html", null, true);
        echo "</span></p>
                    </li>
                    <li class=\"left\">
                        <p>";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de noches"), "html", null, true);
        echo "</p>
                        <p><span>";
        // line 82
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_oferta_, "numnoches"), "html", null, true);
        echo "</span></p>
                    </li>
                    ";
        // line 84
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        if (($this->getAttribute($_oferta_, "numninos") != 0)) {
            // line 85
            echo "                        <li class=\"left\">
                            <p>";
            // line 86
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de niños"), "html", null, true);
            echo "</p>
                            <p><span>oferta.numninos</span></p>
                        </li>
                    ";
        }
        // line 90
        echo "
                    <li class=\"left\">
                        <p>";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre y apellidos"), "html", null, true);
        echo "</p>
                        <p><span>";
        // line 93
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_oferta_, "usuario"), "getNombreCompleto"), "html", null, true);
        echo "</span></p>
                    </li>
                    <li class=\"left\">
                        <p>";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Código de tu operación"), "html", null, true);
        echo "</p>
                        <p><span>";
        // line 97
        if (isset($context["sale"])) { $_sale_ = $context["sale"]; } else { $_sale_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_sale_, "firma"), "html", null, true);
        echo "</span></p>
                    </li>
                    <li>
                        <p>";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("DNI"), "html", null, true);
        echo "</p>
                        <p><span>";
        // line 101
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_oferta_, "usuario"), "dni"), "html", null, true);
        echo "</span></p>
                    </li>
                    <li>
                        <p>";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Importe total de la reserva"), "html", null, true);
        echo "</p>
                        <p><span>";
        // line 105
        if (isset($context["sale"])) { $_sale_ = $context["sale"]; } else { $_sale_ = null; }
        echo twig_escape_filter($this->env, (twig_number_format_filter($this->env, $this->getAttribute($_sale_, "amount"), 2, ",", ".") . $this->env->getExtension('translator')->trans("€")), "html", null, true);
        echo "</span></p>
                    </li>
                </ul>
            </div>

        </div>
        <div class=\"clearfix\"></div>
        <div class=\"separador\"></div>
    </div><!-- contenido -->

";
    }

    // line 5
    public function block_resultado_venta($context, array $blocks = array())
    {
        // line 6
        echo "        ";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:TPV:pago-terminado.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  293 => 6,  290 => 5,  274 => 105,  270 => 104,  252 => 97,  248 => 96,  241 => 93,  237 => 92,  233 => 90,  226 => 86,  223 => 85,  220 => 84,  210 => 81,  188 => 73,  181 => 69,  145 => 51,  137 => 44,  128 => 42,  120 => 40,  41 => 7,  142 => 50,  129 => 35,  125 => 41,  221 => 81,  203 => 78,  198 => 74,  185 => 71,  179 => 69,  163 => 58,  157 => 56,  152 => 60,  133 => 46,  126 => 52,  110 => 48,  76 => 23,  70 => 21,  222 => 85,  207 => 81,  204 => 80,  183 => 74,  167 => 70,  164 => 69,  148 => 52,  141 => 61,  103 => 34,  98 => 28,  59 => 31,  49 => 12,  53 => 29,  21 => 2,  100 => 33,  97 => 32,  18 => 1,  151 => 53,  135 => 68,  114 => 55,  206 => 77,  201 => 75,  194 => 71,  191 => 70,  176 => 61,  166 => 59,  158 => 65,  153 => 64,  143 => 58,  134 => 59,  123 => 51,  118 => 40,  90 => 23,  87 => 47,  66 => 11,  122 => 33,  107 => 28,  101 => 33,  95 => 48,  82 => 26,  67 => 17,  52 => 11,  45 => 29,  36 => 5,  34 => 5,  266 => 117,  263 => 101,  259 => 100,  256 => 84,  242 => 13,  229 => 170,  227 => 84,  218 => 80,  209 => 78,  192 => 74,  186 => 75,  180 => 73,  174 => 95,  162 => 85,  160 => 57,  146 => 50,  140 => 57,  136 => 47,  106 => 35,  73 => 22,  69 => 18,  22 => 6,  60 => 12,  55 => 14,  102 => 46,  89 => 25,  63 => 36,  56 => 30,  50 => 12,  43 => 8,  92 => 26,  79 => 21,  57 => 11,  37 => 7,  33 => 5,  29 => 3,  19 => 1,  47 => 26,  30 => 4,  27 => 3,  249 => 14,  239 => 90,  235 => 12,  228 => 88,  224 => 82,  219 => 84,  217 => 79,  214 => 82,  211 => 77,  208 => 76,  202 => 79,  199 => 77,  193 => 67,  182 => 70,  178 => 61,  175 => 60,  172 => 62,  165 => 55,  161 => 57,  156 => 51,  154 => 50,  150 => 48,  147 => 59,  132 => 36,  127 => 44,  113 => 34,  86 => 24,  83 => 25,  78 => 38,  64 => 14,  61 => 16,  48 => 12,  32 => 4,  24 => 3,  117 => 36,  112 => 38,  109 => 29,  104 => 27,  96 => 32,  84 => 39,  80 => 24,  68 => 35,  46 => 9,  44 => 11,  26 => 2,  23 => 4,  39 => 9,  25 => 2,  20 => 3,  17 => 2,  144 => 62,  138 => 46,  130 => 45,  124 => 55,  121 => 41,  115 => 39,  111 => 52,  108 => 51,  99 => 49,  94 => 27,  91 => 29,  88 => 28,  85 => 27,  77 => 20,  74 => 19,  71 => 17,  65 => 16,  62 => 17,  58 => 8,  54 => 33,  51 => 13,  42 => 8,  38 => 6,  35 => 7,  31 => 3,  28 => 24,);
    }
}
