<?php

/* HotelesFrontendBundle:Frontend:privacidad.html.twig */
class __TwigTemplate_127b80723f078db8c5297ccd694c12fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "

    <div class=\"contenido\">

        <h2>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES PARTICULARES Y PRIVACIDAD"), "html", null, true);
        echo "</h2>
        <div class=\"txtBack\">
            ";
        // line 10
        if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
        if ($this->getAttribute($_content_, "texto", array(), "any", true, true)) {
            // line 11
            echo "                ";
            if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
            echo $this->getAttribute($_content_, "texto");
            echo "
            ";
        }
        // line 13
        echo "        </div>

    </div><!-- contenido -->


";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:privacidad.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 13,  43 => 11,  40 => 10,  35 => 8,  29 => 4,  26 => 3,);
    }
}
