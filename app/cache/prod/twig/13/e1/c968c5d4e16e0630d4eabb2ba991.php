<?php

/* HotelesBackendBundle:UserEmpresa:new.html.twig */
class __TwigTemplate_13e1c968c5d4e16e0630d4eabb2ba991 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear usuario de empresa"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
<div class=\"row\">
  <div class=\"col-lg-6\">
    <section class=\"panel\"> 
        <header class=\"panel-heading\">
            ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear usuario de empresa"), "html", null, true);
        echo "
        </header>
        <div class=\"panel-body\">
            <form action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa_create"), "html", null, true);
        echo "\" method=\"post\" id=\"formulariosubida\"  ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo ">
                 ";
        // line 19
        echo "
                  <div class=\"form-group ";
        // line 20
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "empresa"))), "html", null, true);
        echo "\">
                      <label for=\"exampleInputEmail1\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
        echo "</label>
                      <div class=\"\">
                         ";
        // line 23
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "empresa"), array("attr" => array("class" => "form-control")));
        echo "                     
                      </div>
                  </div>
                  <div class=\"form-group ";
        // line 26
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "email"))), "html", null, true);
        echo "\">
                      <label for=\"exampleInputEmail1\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                      <div class=\"\">
                         ";
        // line 29
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "email"), array("attr" => array("class" => "form-control")));
        echo "                    
                      </div>
                  </div>
                  <div class=\"form-group ";
        // line 32
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_form_, "nombrecompleto"))), "html", null, true);
        echo "\">
                      <label for=\"exampleInputEmail1\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
        echo "</label>
                      <div class=\"\">
                         ";
        // line 35
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "nombrecompleto"), array("attr" => array("class" => "form-control")));
        echo "                    
                      </div>
                  </div>

                  <div class=\"form-group \">
                        <label for=\"exampleInputEmail1\">";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imagen de usuario"), "html", null, true);
        echo "</label>
                        <div class=\"\">
                            <input type=\"file\" name=\"archivo\" id=\"inputsubida\">
                        </div>
                        <br>
                        <input type=\"hidden\" name=\"imagensubidanombre\" id=\"imagensubidanombre\">
                        ";
        // line 46
        if (isset($context["imagendevuelta"])) { $_imagendevuelta_ = $context["imagendevuelta"]; } else { $_imagendevuelta_ = null; }
        if (($_imagendevuelta_ != "")) {
            // line 47
            echo "                            <img src=\"/uploads/user/";
            if (isset($context["imagendevuelta"])) { $_imagendevuelta_ = $context["imagendevuelta"]; } else { $_imagendevuelta_ = null; }
            echo twig_escape_filter($this->env, $_imagendevuelta_, "html", null, true);
            echo "\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                        ";
        } else {
            // line 49
            echo "                            <img src=\"\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                        ";
        }
        // line 51
        echo "                            <iframe src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user")), "html", null, true);
        echo "\" height=\"0\" width=\"1300\" style=\"border:0px;\" name=\"inter\"></iframe>
                    </div>              
                    ";
        // line 53
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_form_);
        echo "    

        </div>
    </section>
    <!-- btns -->
    <section class=\"panel\">
          <div class=\"panel-body\">
                <button type=\"submit\" class=\"btn btn-success\">";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear"), "html", null, true);
        echo "</button>   
            </form><!-- / form edit -->
               <a href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
          </div>
    </section>
    <!-- /btns -->
  </div><!-- /col -->
</div><!-- /row -->



";
        // line 76
        echo "
                   <a class=\"\" data-toggle=\"modal\" href=\"#myModal\" id=\"clickmodal\"></a>
 

<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\" style=\"width: 923px;margin: 36px auto;height: 508px!important;\">
        <div class=\"modal-content\">
             <div class=\"modal-header\">
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                  <h4 class=\"modal-title\">";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar imagen"), "html", null, true);
        echo "</h4>
              </div>
                
                <div style=\"width:100%; height:auto; padding-top: 17px;\" >
                  <img src=\"/bundles/hotelesbackend/img/logo2.png\" id=\"target\" width=\"200\" alt=\"[Jcrop Example]\" />
                </div>
 
            <div class=\"clearfix\"></div>

          <div class=\"modal-footer\">
              <button data-dismiss=\"modal\" id=\"closeimage\" class=\"btn btn-default\" type=\"button\">";
        // line 95
        echo "Cerrar";
        echo "</button>
              <button class=\"btn btn-success\" id=\"successimage\" data-id=\"\" data-url=\"\" data-img=\"\" type=\"button\">";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar cambios"), "html", null, true);
        echo "</button>
          </div>

        </div>
    </div>
</div>
";
        // line 107
        echo "
";
        // line 117
        echo "
";
    }

    // line 120
    public function block_jsextras($context, array $blocks = array())
    {
        // line 121
        echo "
    ";
        // line 122
        $context["urlcrea"] = $this->env->getExtension('routing')->getPath("admin_userempresa_create");
        // line 123
        echo "    ";
        $context["urlsubidaimagen"] = $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user"));
        // line 124
        echo "    ";
        $context["tipo"] = "user";
        // line 125
        echo "    ";
        $context["crop1"] = "100";
        // line 126
        echo "    ";
        $context["crop2"] = "100";
        // line 127
        echo "    ";
        $context["crop3"] = "100";
        // line 128
        echo "    ";
        $context["crop4"] = "100";
        // line 129
        echo "    ";
        $context["crop5"] = "1";
        // line 130
        echo "    ";
        $context["crop6"] = "1";
        // line 131
        echo "
    ";
        // line 132
        $this->env->loadTemplate("HotelesBackendBundle:Crop:crop.html.twig")->display(array_merge($context, array("especifico" => "user")));
        // line 133
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:UserEmpresa:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  262 => 133,  260 => 132,  257 => 131,  254 => 130,  251 => 129,  248 => 128,  245 => 127,  242 => 126,  239 => 125,  236 => 124,  233 => 123,  231 => 122,  228 => 121,  225 => 120,  220 => 117,  217 => 107,  208 => 96,  204 => 95,  191 => 85,  180 => 76,  166 => 62,  161 => 60,  150 => 53,  144 => 51,  140 => 49,  133 => 47,  130 => 46,  121 => 40,  112 => 35,  107 => 33,  102 => 32,  95 => 29,  90 => 27,  85 => 26,  78 => 23,  73 => 21,  68 => 20,  65 => 19,  58 => 17,  52 => 14,  45 => 9,  42 => 8,  36 => 5,  31 => 4,  28 => 3,);
    }
}
