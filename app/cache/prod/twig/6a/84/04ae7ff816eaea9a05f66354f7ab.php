<?php

/* HotelesBackendBundle:Empresa:edit.html.twig */
class __TwigTemplate_6a8404ae7ff816eaea9a05f66354f7ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar empresa"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">

            <!--tab nav start-->
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar empresa"), "html", null, true);
        echo "
                </header>
                <header class=\"panel-heading tab-bg-dark-navy-blue \">
                    <ul class=\"nav nav-tabs\">
                        <li class=\"active ";
        // line 20
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "email")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombreempresa")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cif")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "direccionfacturacion")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefono")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "numerocuenta")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#general\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la empresa"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"";
        // line 23
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrepersonacontacto")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "apellidospersonacontacto")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "emailpersonacontacto")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefonopersonacontacto")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cargopersonacontacto")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#api\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la persona de contacto"), "html", null, true);
        echo "</a>
                        </li>
                        ";
        // line 26
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 27
            echo "                            <li class=\"\">
                                <a data-toggle=\"tab\" href=\"#com\">";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comisiones"), "html", null, true);
            echo "</a>
                            </li>
                        ";
        }
        // line 31
        echo "                    </ul>
                </header>
                <div class=\"panel-body\">
                    <div class=\"tab-content\">
                        <div id=\"general\" class=\"tab-pane active\">
                            <form action=\"";
        // line 36
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_update", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\" method=\"post\" ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_edit_form_);
        echo ">
                                ";
        // line 38
        echo "                                <div class=\"form-group  ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "email"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 41
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "email"));
        echo "
                                        ";
        // line 42
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 45
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombreempresa"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la empresa"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 48
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombreempresa"));
        echo "
                                        ";
        // line 49
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "nombreempresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 52
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cif"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CIF"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 55
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cif"));
        echo "
                                        ";
        // line 56
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "cif"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 59
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "direccionfacturacion"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dirección de facturación"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 62
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "direccionfacturacion"));
        echo "
                                        ";
        // line 63
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "direccionfacturacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 66
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefono"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 69
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefono"));
        echo "
                                        ";
        // line 70
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "telefono"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 73
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "numerocuenta"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de cuenta"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 76
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "numerocuenta"));
        echo "
                                        ";
        // line 77
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "numerocuenta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                        </div>
                        <div id=\"api\" class=\"tab-pane\">
                            <div class=\"form-group ";
        // line 82
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrepersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 85
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombrepersonacontacto"));
        echo "
                                    ";
        // line 86
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "nombrepersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 89
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "apellidospersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 92
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "apellidospersonacontacto"));
        echo "
                                    ";
        // line 93
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "apellidospersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 96
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "emailpersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 99
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "emailpersonacontacto"));
        echo "
                                    ";
        // line 100
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "emailpersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 103
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefonopersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 106
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "telefonopersonacontacto"));
        echo "
                                    ";
        // line 107
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "telefonopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 110
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cargopersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo de la persona contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 113
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "cargopersonacontacto"));
        echo "
                                    ";
        // line 114
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "cargopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                        </div>
                        ";
        // line 118
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 119
            echo "                            <div id=\"com\" class=\"tab-pane\">                                
                                <div class=\"form-group\">
                                    <label for=\"exampleInputEmail1\">";
            // line 121
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Porcentaje de comisión (campo enteros) , (campo decimales)."), "html", null, true);
            echo "</label>
                                    <div class=\"clearfix\"></div>
                                    <div class=\"col-sm-2-dcnt ";
            // line 123
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "comisionempresaentero"))), "html", null, true);
            echo "\">
                                        ";
            // line 124
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "comisionempresaentero"));
            echo "
                                        ";
            // line 125
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "comisionempresaentero"), array("attr" => array("class" => "form-control")));
            echo "                     
                                    </div>
                                    <div class=\"comaclear\" style=\"float:left\"> , </div>
                                    <div class=\"col-sm-2-dcnt ";
            // line 128
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "comisionempresadecimal"))), "html", null, true);
            echo "\">  
                                        ";
            // line 129
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "comisionempresadecimal"));
            echo "
                                        ";
            // line 130
            if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "comisionempresadecimal"), array("attr" => array("class" => "form-control")));
            echo "                                  
                                    </div>
                                    <div class=\"clearfix\"></div>
                                </div>

                            </div>
                            <div id=\"tpv\" class=\"tab-pane\">tpv</div>
                        ";
        }
        // line 138
        echo "                            ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_edit_form_);
        echo "
                    </div>
                </div>
            </section>

            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-info\">";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    ";
        // line 147
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        if (($this->getAttribute($_entity_, "status", array(), "any", true, true) && ($this->getAttribute($_entity_, "status") == 1))) {
            // line 148
            echo "                        <form action=\"";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_reactivate", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\" method=\"post\" class=\"btnDelete\">
                            ";
            // line 149
            if (isset($context["reactivate_form"])) { $_reactivate_form_ = $context["reactivate_form"]; } else { $_reactivate_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($_reactivate_form_);
            echo "
                            <button type=\"submit\" class=\"btn btn-success\" onclick=\"return confirm('¿Está seguro de re-activar esta empresa?');\">";
            // line 150
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Reactivar empresa"), "html", null, true);
            echo "</button>
                        </form>
                    ";
        } else {
            // line 153
            echo "                        <form action=\"";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_delete", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\" method=\"post\" class=\"btnDelete\">
                            ";
            // line 154
            if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
            echo $this->env->getExtension('form')->renderWidget($_delete_form_);
            echo "
                            <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('¿Estás seguro que quieres suspender esta empresa?\\n' +
                                            'Realizar esta acción cancelará todas las ofertas activas sobre todos los hoteles de la empresa y los usuarios y no se podrán reactivar de ninguna forma.');\">";
            // line 156
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suspender"), "html", null, true);
            echo "</button>
                        </form>
                    ";
        }
        // line 159
        echo "                    ";
        // line 160
        echo "
                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Empresa:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  459 => 160,  457 => 159,  451 => 156,  445 => 154,  439 => 153,  433 => 150,  428 => 149,  422 => 148,  419 => 147,  414 => 145,  402 => 138,  390 => 130,  385 => 129,  380 => 128,  373 => 125,  368 => 124,  363 => 123,  358 => 121,  354 => 119,  352 => 118,  344 => 114,  339 => 113,  334 => 111,  329 => 110,  322 => 107,  317 => 106,  312 => 104,  307 => 103,  300 => 100,  295 => 99,  290 => 97,  285 => 96,  278 => 93,  273 => 92,  268 => 90,  263 => 89,  256 => 86,  251 => 85,  246 => 83,  241 => 82,  232 => 77,  227 => 76,  222 => 74,  217 => 73,  210 => 70,  205 => 69,  200 => 67,  195 => 66,  188 => 63,  183 => 62,  178 => 60,  173 => 59,  166 => 56,  161 => 55,  156 => 53,  151 => 52,  144 => 49,  139 => 48,  134 => 46,  129 => 45,  122 => 42,  117 => 41,  112 => 39,  106 => 38,  98 => 36,  91 => 31,  85 => 28,  82 => 27,  80 => 26,  75 => 24,  70 => 23,  65 => 21,  60 => 20,  53 => 16,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
