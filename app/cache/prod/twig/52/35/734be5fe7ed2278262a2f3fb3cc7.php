<?php

/* HotelesFrontendBundle:Frontend:hotel.mv.twig */
class __TwigTemplate_5235734be5fe7ed2278262a2f3fb3cc7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 5
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a38f972_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a38f972_0") : $this->env->getExtension('assets')->getAssetUrl("js/a38f972_part_1_hotel_detalle_1.js");
            // line 6
            echo "    <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "a38f972"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a38f972") : $this->env->getExtension('assets')->getAssetUrl("js/a38f972.js");
            echo "    <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"content_hotel\">
        <a href=\"\" onClick=\"window.history.back();\" class=\"btn_volver\"> &lt; VOLVER AL LISTADO</a>
        <div class=\"hotel\">
            <h2 class=\"name\">";
        // line 17
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "nombrehotel"), "html", null, true);
        echo "</h2>
            <ul class=\"servicios clearfix\">
                ";
        // line 19
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "piscina") == 1)) {
            // line 20
            echo "                    <li class=\"piscina\">Piscina
                    </li>
                ";
        }
        // line 23
        echo "
                ";
        // line 24
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "spa") == 1)) {
            // line 25
            echo "                    <li class=\"spa\">Spa
                    </li>
                ";
        }
        // line 28
        echo "
                ";
        // line 29
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "wiFi") == 1)) {
            // line 30
            echo "                    <li class=\"wi-fi\">Wi-fi
                    </li>        
                ";
        }
        // line 33
        echo "
                ";
        // line 34
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "accesoAdaptado") == 1)) {
            // line 35
            echo "                    <li class=\"acceso-adaptado\">Acceso adaptado
                    </li>
                ";
        }
        // line 38
        echo "
                ";
        // line 39
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "aceptanPerros") == 1)) {
            // line 40
            echo "                    <li class=\"perros\">Aceptan perros
                    </li>
                ";
        }
        // line 43
        echo "
                ";
        // line 44
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "aparcamiento") == 1)) {
            // line 45
            echo "                    <li class=\"parking\">Aparcamiento/Parking
                    </li>
                ";
        }
        // line 48
        echo "
                ";
        // line 49
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "businessCenter") == 1)) {
            // line 50
            echo "                    <li class=\"business-center\">Business center
                    </li>
                ";
        }
        // line 53
        echo "            </ul>
            <div class=\"slidehotel owl-carousel\">

                ";
        // line 56
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        $context["imagen_hoteles_path"] = $this->getAttribute($_hotel_, "getImagePath");
        echo "                    
                ";
        // line 57
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($_hotel_, "getOrderedImagenesDelHotel"));
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 58
            echo "                    ";
            if (isset($context["imagen_hoteles_path"])) { $_imagen_hoteles_path_ = $context["imagen_hoteles_path"]; } else { $_imagen_hoteles_path_ = null; }
            if (isset($context["imagen"])) { $_imagen_ = $context["imagen"]; } else { $_imagen_ = null; }
            $context["ruta_imagen"] = (($_imagen_hoteles_path_ . "/") . $this->getAttribute($_imagen_, "imagen"));
            // line 59
            echo "                    <div>
                        <img src=\"";
            // line 60
            if (isset($context["ruta_imagen"])) { $_ruta_imagen_ = $context["ruta_imagen"]; } else { $_ruta_imagen_ = null; }
            echo twig_escape_filter($this->env, $_ruta_imagen_, "html", null, true);
            echo "\">
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 62
        echo "                                     

            </div>
            ";
        // line 65
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "calificacion") > 0)) {
            // line 66
            echo "                <ul class=\"estrellas\">
                    ";
            // line 67
            if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($_hotel_, "calificacion")));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 68
                echo "                        <li class=\"star\">
                        </li>                
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 71
            echo "                </ul>
            ";
        }
        // line 73
        echo "            <div class=\"clearfix\"></div>
            <!-- description -->
            <div class=\"line\"></div> 
            <p><b>Descripción</b></p>
            <p>";
        // line 77
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "descripcion"), "html", null, true);
        echo "</p>
            <p><b>Características</b></p>
            <p>";
        // line 79
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "caracteristicas"), "html", null, true);
        echo "</p>
            <div class=\"mapa\"> ";
        // line 81
        echo "                <div class=\"direccion\">
                    <p>";
        // line 82
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "ubicacion"), "html", null, true);
        echo "</p>
                </div>
                <div class=\"mapa-detalle\">
                    <div id=\"mapa_det\" style=\"height:290px;\" data-longitud=\"";
        // line 85
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "longitud"), "html", null, true);
        echo "\" data-latitud=\"";
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "latitud"), "html", null, true);
        echo "\" data-titulo=\"";
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "nombrehotel"), "html", null, true);
        echo "\"></div>
                </div><!-- /mapa -->
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:hotel.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  244 => 85,  237 => 82,  234 => 81,  229 => 79,  223 => 77,  217 => 73,  213 => 71,  205 => 68,  200 => 67,  197 => 66,  194 => 65,  189 => 62,  179 => 60,  176 => 59,  171 => 58,  166 => 57,  161 => 56,  156 => 53,  151 => 50,  148 => 49,  145 => 48,  140 => 45,  137 => 44,  134 => 43,  129 => 40,  126 => 39,  123 => 38,  118 => 35,  115 => 34,  112 => 33,  107 => 30,  104 => 29,  101 => 28,  96 => 25,  93 => 24,  90 => 23,  85 => 20,  82 => 19,  76 => 17,  71 => 14,  68 => 13,  61 => 11,  58 => 10,  40 => 6,  36 => 5,  31 => 4,  28 => 3,);
    }
}
