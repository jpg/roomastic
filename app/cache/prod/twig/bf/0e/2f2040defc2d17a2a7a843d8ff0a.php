<?php

/* HotelesBackendBundle:Empresa:show.html.twig */
class __TwigTemplate_bf0e2f2040defc2d17a2a7a843d8ff0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
<h1>Empresa</h1>

<table class=\"record_properties\">
    <tbody>
        <tr>
            <th>Id</th>
            <td>";
        // line 11
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "id"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Email</th>
            <td>";
        // line 15
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Email"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Nombreempresa</th>
            <td>";
        // line 19
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Nombreempresa"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Cif</th>
            <td>";
        // line 23
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Cif"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Direccionfacturacion</th>
            <td>";
        // line 27
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Direccionfacturacion"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Telefono</th>
            <td>";
        // line 31
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Telefono"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Numerocuenta</th>
            <td>";
        // line 35
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Numerocuenta"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Nombrepersonacontacto</th>
            <td>";
        // line 39
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Nombrepersonacontacto"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Apellidospersonacontacto</th>
            <td>";
        // line 43
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Apellidospersonacontacto"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Emailpersonacontacto</th>
            <td>";
        // line 47
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Emailpersonacontacto"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Telefonopersonacontacto</th>
            <td>";
        // line 51
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Telefonopersonacontacto"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Cargopersonacontacto</th>
            <td>";
        // line 55
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "Cargopersonacontacto"), "html", null, true);
        echo "</td>
        </tr>
        <tr>
            <th>Comisión</th>
            <td>";
        // line 59
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "comisionComisionEmpresaEntero"), "html", null, true);
        echo ", ";
        if (isset($context["ComisionEmpresaDecimal"])) { $_ComisionEmpresaDecimal_ = $context["ComisionEmpresaDecimal"]; } else { $_ComisionEmpresaDecimal_ = null; }
        echo twig_escape_filter($this->env, $_ComisionEmpresaDecimal_, "html", null, true);
        echo "</td>
        </tr>
    </tbody>
</table>

<ul class=\"record_actions\">
    <li>
        <a href=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa"), "html", null, true);
        echo "\">
            ";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Volver a la lista"), "html", null, true);
        echo "
        </a>
    </li>
    <li>
        <a href=\"";
        // line 71
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_edit", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\">
            ";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "
        </a>
    </li>
    <li>
        <form action=\"";
        // line 76
        if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_delete", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
        echo "\" method=\"post\">
            ";
        // line 77
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($_delete_form_);
        echo "
            <button type=\"submit\">";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suspender"), "html", null, true);
        echo "</button>
        </form>
    </li>
</ul>

";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Empresa:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  439 => 153,  422 => 148,  390 => 130,  368 => 124,  358 => 121,  329 => 110,  278 => 93,  356 => 121,  351 => 119,  419 => 147,  397 => 135,  337 => 116,  332 => 115,  240 => 83,  196 => 65,  548 => 201,  465 => 161,  442 => 150,  427 => 146,  415 => 145,  369 => 127,  361 => 122,  354 => 119,  322 => 107,  299 => 103,  280 => 97,  212 => 74,  561 => 254,  536 => 249,  531 => 185,  528 => 247,  453 => 218,  447 => 214,  385 => 129,  326 => 144,  321 => 142,  307 => 103,  292 => 133,  306 => 104,  300 => 100,  288 => 122,  285 => 96,  236 => 75,  506 => 173,  478 => 163,  410 => 120,  380 => 128,  370 => 127,  366 => 123,  362 => 108,  348 => 121,  315 => 109,  310 => 108,  302 => 104,  289 => 94,  284 => 97,  272 => 90,  238 => 81,  1272 => 501,  1270 => 500,  1267 => 499,  1264 => 498,  1261 => 497,  1258 => 496,  1255 => 495,  1252 => 494,  1243 => 491,  1240 => 490,  1197 => 449,  1194 => 448,  1176 => 435,  1170 => 433,  1164 => 432,  1158 => 429,  1139 => 424,  1129 => 420,  1124 => 419,  1115 => 412,  1100 => 405,  1095 => 404,  1088 => 401,  1083 => 400,  1078 => 399,  1073 => 397,  1066 => 393,  1058 => 389,  1053 => 388,  1048 => 386,  1043 => 385,  1026 => 379,  1021 => 378,  1004 => 372,  999 => 371,  983 => 361,  970 => 351,  945 => 339,  940 => 338,  937 => 337,  913 => 322,  904 => 317,  893 => 314,  873 => 303,  860 => 295,  854 => 293,  843 => 286,  838 => 285,  833 => 283,  802 => 271,  794 => 270,  786 => 266,  781 => 265,  774 => 262,  769 => 261,  764 => 259,  756 => 258,  749 => 255,  744 => 254,  732 => 249,  722 => 246,  715 => 243,  710 => 242,  693 => 231,  685 => 227,  675 => 224,  670 => 223,  663 => 220,  648 => 216,  641 => 213,  636 => 212,  631 => 210,  626 => 209,  619 => 206,  599 => 197,  594 => 196,  572 => 188,  565 => 185,  547 => 178,  537 => 175,  530 => 172,  503 => 177,  490 => 158,  467 => 221,  455 => 145,  445 => 154,  431 => 125,  387 => 132,  343 => 103,  318 => 105,  296 => 98,  1374 => 615,  1371 => 614,  1367 => 616,  1365 => 614,  1339 => 590,  1335 => 588,  1321 => 587,  1315 => 585,  1309 => 584,  1302 => 582,  1298 => 581,  1294 => 579,  1290 => 577,  1287 => 576,  1281 => 575,  1263 => 574,  1260 => 573,  1256 => 571,  1253 => 570,  1249 => 493,  1246 => 492,  1230 => 565,  1225 => 564,  1222 => 563,  1213 => 556,  1210 => 555,  1207 => 554,  1160 => 506,  1157 => 505,  1144 => 426,  1137 => 617,  1135 => 423,  1119 => 491,  1117 => 489,  1114 => 488,  1111 => 487,  1109 => 486,  1105 => 406,  1091 => 461,  1080 => 455,  1068 => 394,  1063 => 451,  1051 => 448,  1044 => 446,  1039 => 445,  1032 => 443,  1027 => 442,  1020 => 440,  1015 => 439,  1008 => 437,  1003 => 436,  996 => 434,  991 => 433,  985 => 430,  954 => 426,  952 => 425,  941 => 419,  935 => 418,  927 => 415,  921 => 414,  918 => 413,  912 => 410,  903 => 408,  900 => 407,  898 => 406,  888 => 313,  883 => 400,  876 => 304,  871 => 397,  865 => 296,  852 => 390,  849 => 389,  847 => 388,  836 => 382,  831 => 381,  824 => 278,  819 => 277,  812 => 274,  807 => 273,  800 => 373,  788 => 370,  783 => 369,  759 => 364,  755 => 362,  753 => 361,  739 => 252,  735 => 353,  728 => 351,  713 => 347,  706 => 345,  691 => 341,  684 => 339,  680 => 226,  673 => 336,  669 => 335,  662 => 331,  654 => 329,  651 => 328,  649 => 327,  634 => 321,  622 => 314,  614 => 205,  610 => 310,  601 => 305,  575 => 287,  555 => 182,  543 => 198,  539 => 271,  523 => 264,  514 => 252,  510 => 246,  501 => 236,  496 => 234,  485 => 167,  472 => 161,  469 => 222,  451 => 156,  434 => 210,  429 => 207,  379 => 191,  374 => 189,  363 => 123,  357 => 182,  342 => 117,  317 => 106,  314 => 163,  267 => 93,  261 => 92,  155 => 59,  1182 => 438,  1180 => 444,  1177 => 443,  1174 => 442,  1171 => 441,  1168 => 440,  1165 => 439,  1162 => 438,  1159 => 437,  1156 => 436,  1153 => 428,  1150 => 489,  1147 => 427,  1134 => 423,  1130 => 422,  1121 => 416,  1110 => 409,  1102 => 403,  1092 => 397,  1087 => 396,  1082 => 395,  1075 => 454,  1070 => 391,  1065 => 390,  1060 => 388,  1056 => 449,  1054 => 385,  1046 => 381,  1041 => 380,  1036 => 382,  1031 => 381,  1024 => 374,  1019 => 373,  1014 => 375,  1009 => 374,  1002 => 367,  997 => 366,  992 => 364,  987 => 362,  975 => 354,  971 => 353,  958 => 341,  946 => 333,  933 => 331,  928 => 330,  925 => 329,  916 => 322,  895 => 319,  890 => 318,  878 => 309,  867 => 301,  862 => 300,  856 => 392,  845 => 291,  840 => 290,  835 => 288,  820 => 281,  808 => 277,  803 => 275,  795 => 372,  787 => 270,  782 => 269,  775 => 266,  770 => 265,  765 => 263,  757 => 262,  750 => 360,  745 => 258,  740 => 256,  729 => 253,  724 => 350,  712 => 247,  687 => 232,  682 => 231,  672 => 228,  665 => 225,  660 => 224,  655 => 222,  643 => 218,  638 => 217,  633 => 215,  621 => 211,  611 => 208,  603 => 204,  598 => 203,  586 => 198,  581 => 197,  576 => 195,  569 => 260,  559 => 189,  551 => 185,  546 => 213,  534 => 189,  529 => 188,  512 => 172,  489 => 171,  481 => 166,  471 => 156,  435 => 142,  399 => 137,  372 => 118,  338 => 107,  313 => 104,  308 => 102,  303 => 101,  298 => 95,  286 => 95,  269 => 90,  254 => 130,  247 => 83,  225 => 71,  215 => 66,  131 => 39,  173 => 59,  170 => 62,  200 => 67,  189 => 66,  93 => 26,  875 => 308,  825 => 282,  821 => 498,  817 => 497,  813 => 278,  798 => 485,  793 => 484,  767 => 463,  760 => 462,  754 => 461,  748 => 460,  742 => 459,  734 => 254,  727 => 248,  725 => 446,  707 => 245,  704 => 435,  701 => 434,  698 => 433,  668 => 409,  658 => 219,  647 => 404,  642 => 323,  606 => 376,  589 => 194,  577 => 190,  560 => 184,  524 => 176,  520 => 169,  515 => 182,  511 => 174,  499 => 170,  492 => 171,  462 => 160,  450 => 155,  444 => 212,  432 => 147,  421 => 148,  402 => 138,  398 => 262,  383 => 128,  377 => 181,  353 => 105,  347 => 119,  336 => 117,  328 => 104,  309 => 129,  291 => 97,  244 => 89,  119 => 35,  171 => 76,  116 => 49,  416 => 132,  408 => 195,  404 => 138,  401 => 141,  359 => 164,  346 => 138,  304 => 109,  297 => 134,  271 => 129,  251 => 85,  232 => 77,  195 => 66,  72 => 21,  149 => 52,  75 => 24,  522 => 222,  519 => 221,  507 => 179,  502 => 241,  497 => 238,  493 => 13,  484 => 205,  479 => 227,  475 => 165,  466 => 155,  457 => 159,  452 => 190,  448 => 191,  443 => 188,  440 => 153,  426 => 135,  413 => 201,  409 => 139,  392 => 133,  389 => 138,  373 => 125,  364 => 170,  352 => 118,  339 => 113,  334 => 111,  325 => 108,  320 => 110,  216 => 74,  159 => 71,  40 => 4,  721 => 339,  717 => 348,  714 => 440,  711 => 438,  708 => 334,  705 => 240,  688 => 320,  671 => 305,  653 => 217,  650 => 221,  645 => 300,  630 => 319,  616 => 210,  608 => 309,  593 => 303,  587 => 263,  582 => 191,  574 => 260,  562 => 282,  553 => 339,  542 => 251,  535 => 238,  532 => 269,  517 => 173,  508 => 165,  494 => 169,  488 => 167,  476 => 158,  470 => 291,  461 => 194,  454 => 154,  437 => 149,  433 => 150,  424 => 182,  418 => 200,  412 => 182,  406 => 140,  400 => 190,  394 => 140,  388 => 170,  382 => 131,  375 => 128,  367 => 119,  341 => 240,  333 => 105,  327 => 115,  282 => 98,  255 => 87,  250 => 85,  246 => 83,  213 => 71,  197 => 71,  105 => 33,  777 => 366,  747 => 354,  743 => 351,  702 => 344,  695 => 342,  689 => 313,  683 => 312,  677 => 229,  664 => 300,  661 => 303,  659 => 297,  656 => 296,  628 => 214,  618 => 283,  613 => 282,  609 => 203,  604 => 280,  600 => 279,  591 => 274,  567 => 284,  564 => 283,  545 => 252,  541 => 182,  538 => 267,  525 => 171,  518 => 253,  513 => 166,  509 => 249,  504 => 237,  500 => 14,  495 => 172,  491 => 241,  486 => 230,  482 => 164,  477 => 163,  473 => 233,  468 => 230,  464 => 157,  459 => 160,  456 => 158,  449 => 153,  441 => 211,  438 => 279,  428 => 149,  423 => 204,  420 => 186,  414 => 145,  411 => 143,  405 => 206,  393 => 188,  378 => 127,  344 => 114,  331 => 146,  324 => 180,  319 => 179,  295 => 99,  281 => 94,  276 => 88,  177 => 83,  277 => 159,  273 => 92,  264 => 88,  260 => 88,  257 => 94,  231 => 86,  168 => 150,  396 => 195,  391 => 187,  386 => 192,  381 => 132,  376 => 137,  371 => 132,  365 => 125,  360 => 124,  355 => 123,  350 => 111,  345 => 157,  340 => 112,  335 => 111,  330 => 109,  323 => 105,  316 => 116,  312 => 104,  305 => 219,  301 => 103,  294 => 124,  283 => 104,  279 => 96,  275 => 120,  268 => 90,  265 => 181,  262 => 133,  245 => 84,  234 => 84,  230 => 80,  205 => 69,  190 => 70,  187 => 132,  184 => 61,  169 => 57,  139 => 48,  81 => 42,  293 => 128,  290 => 97,  274 => 91,  270 => 104,  252 => 91,  248 => 128,  241 => 82,  237 => 80,  233 => 80,  226 => 79,  223 => 74,  220 => 98,  210 => 70,  188 => 65,  181 => 78,  145 => 55,  137 => 51,  128 => 66,  120 => 50,  41 => 8,  142 => 45,  129 => 45,  125 => 43,  221 => 76,  203 => 72,  198 => 81,  185 => 69,  179 => 60,  163 => 73,  157 => 53,  152 => 67,  133 => 44,  126 => 55,  110 => 47,  76 => 41,  70 => 27,  222 => 74,  207 => 73,  204 => 67,  183 => 64,  167 => 56,  164 => 72,  148 => 66,  141 => 45,  103 => 47,  98 => 36,  59 => 19,  49 => 12,  53 => 16,  21 => 3,  100 => 45,  97 => 28,  18 => 1,  151 => 52,  135 => 46,  114 => 33,  206 => 71,  201 => 67,  194 => 67,  191 => 64,  176 => 77,  166 => 56,  158 => 70,  153 => 72,  143 => 46,  134 => 59,  123 => 40,  118 => 51,  90 => 40,  87 => 32,  66 => 18,  122 => 42,  107 => 39,  101 => 29,  95 => 34,  82 => 27,  67 => 23,  52 => 15,  45 => 9,  36 => 5,  34 => 7,  266 => 90,  263 => 89,  259 => 87,  256 => 86,  242 => 88,  229 => 82,  227 => 75,  218 => 72,  209 => 133,  192 => 68,  186 => 68,  180 => 84,  174 => 58,  162 => 54,  160 => 60,  146 => 47,  140 => 47,  136 => 77,  106 => 38,  73 => 20,  69 => 18,  22 => 3,  60 => 20,  55 => 22,  102 => 43,  89 => 45,  63 => 23,  56 => 16,  50 => 12,  43 => 16,  92 => 43,  79 => 33,  57 => 22,  37 => 8,  33 => 6,  29 => 4,  19 => 2,  47 => 11,  30 => 4,  27 => 3,  249 => 88,  239 => 90,  235 => 88,  228 => 75,  224 => 77,  219 => 80,  217 => 73,  214 => 67,  211 => 73,  208 => 96,  202 => 69,  199 => 68,  193 => 66,  182 => 66,  178 => 60,  175 => 63,  172 => 60,  165 => 61,  161 => 55,  156 => 53,  154 => 49,  150 => 58,  147 => 50,  132 => 39,  127 => 54,  113 => 39,  86 => 35,  83 => 22,  78 => 31,  64 => 18,  61 => 17,  48 => 17,  32 => 7,  24 => 4,  117 => 41,  112 => 48,  109 => 32,  104 => 46,  96 => 44,  84 => 41,  80 => 40,  68 => 18,  46 => 15,  44 => 9,  26 => 3,  23 => 3,  39 => 6,  25 => 4,  20 => 2,  17 => 1,  144 => 49,  138 => 45,  130 => 44,  124 => 55,  121 => 40,  115 => 33,  111 => 34,  108 => 47,  99 => 38,  94 => 39,  91 => 31,  88 => 42,  85 => 28,  77 => 23,  74 => 32,  71 => 19,  65 => 24,  62 => 23,  58 => 18,  54 => 19,  51 => 18,  42 => 8,  38 => 11,  35 => 5,  31 => 4,  28 => 3,);
    }
}
