<?php

/* HotelesFrontendBundle:Frontend:contacto.mv.twig */
class __TwigTemplate_7346d46596b241ecab531bbea4de3755 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"contacto\">
        <h2>";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Atención al cliente"), "html", null, true);
        echo "</h2>
        <p>Ponte en contacto con nosotros rellenando el siguiente formulario de contacto. En un breve plazo de tiempo nos pondremos en contacto contigo.</p>
        <p>Si lo prefieres, puedes ponerte en contacto con nosotros por teléfono de lunes a viernes de 9 a 19h.</p>
        <p class=\"rosa\"><span>911 610 156</span></p>

        ";
        // line 10
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if ($this->getAttribute($this->getAttribute($_app_, "session"), "hasFlash", array(0 => "notice"), "method")) {
            // line 11
            echo "            <div class=\"flash-notice\">
                ";
            // line 12
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_app_, "session"), "flash", array(0 => "notice"), "method"), "html", null, true);
            echo "
            </div>
        ";
        }
        // line 15
        echo "
        <form action=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" method=\"post\" ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo ">

            <input class=\"bug\" type=\"text\" id=\"hoteles_backendbundle_contactotype_nombre\" name=\"hoteles_backendbundle_contactotype[nombre]\" required=\"required\" maxlength=\"255\" placeholder=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre"), "html", null, true);
        echo "\"><br><br>
            <input type=\"text\" id=\"hoteles_backendbundle_contactotype_apellidos\" name=\"hoteles_backendbundle_contactotype[apellidos]\" required=\"required\" maxlength=\"255\" placeholder=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos"), "html", null, true);
        echo "\" class=\"longinput\"><br><br>
            <input type=\"text\" id=\"hoteles_backendbundle_contactotype_email\" name=\"hoteles_backendbundle_contactotype[email]\" required=\"required\" maxlength=\"255\" placeholder=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "\" class=\"longinput\"><br><br>
            <textarea id=\"hoteles_backendbundle_contactotype_texto\" name=\"hoteles_backendbundle_contactotype[texto]\" required=\"required\" placeholder=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comentarios"), "html", null, true);
        echo "\"></textarea><br><br>
            ";
        // line 22
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "_token"));
        echo "
            ";
        // line 24
        echo "            <p>
                <button type=\"submit\" class=\"btn_generic\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar petición"), "html", null, true);
        echo "</button>
            </p>
        </form>
    </div>
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:contacto.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 25,  85 => 24,  80 => 22,  76 => 21,  72 => 20,  68 => 19,  64 => 18,  56 => 16,  53 => 15,  46 => 12,  43 => 11,  40 => 10,  32 => 5,  29 => 4,  26 => 3,);
    }
}
