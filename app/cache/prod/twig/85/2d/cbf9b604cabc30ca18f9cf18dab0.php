<?php

/* HotelesBackendBundle:Crop:crop.html.twig */
class __TwigTemplate_852dcbf9b604cabc30ca18f9cf18dab0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
    var jcrop_api;
    jQuery(document).ready(function (\$) {

        \$('#inputsubida').change(function (event) {
            var urlcrea = '";
        // line 6
        if (isset($context["urlcrea"])) { $_urlcrea_ = $context["urlcrea"]; } else { $_urlcrea_ = null; }
        echo twig_escape_filter($this->env, $_urlcrea_, "html", null, true);
        echo "';
            var urlsubidaimagen = '";
        // line 7
        if (isset($context["urlsubidaimagen"])) { $_urlsubidaimagen_ = $context["urlsubidaimagen"]; } else { $_urlsubidaimagen_ = null; }
        echo twig_escape_filter($this->env, $_urlsubidaimagen_, "html", null, true);
        echo "';
            \$('#formulariosubida').attr('action', urlsubidaimagen);
            \$('#formulariosubida').attr('enctype', 'multipart/form-data');
            \$('#formulariosubida').attr('target', 'inter');
            \$('#formulariosubida').submit();
            \$('#formulariosubida').attr('action', urlcrea);
            \$('#formulariosubida').removeAttr('target');
            event.preventDefault();
        });

        initJcrop();

        \$('#successimage').click(function (event) {
            url = Routing.generate('admin_imagecrop', {
                posx: ROOMASTIC.image_crop.posx,
                posy: ROOMASTIC.image_crop.posy,
                width: ROOMASTIC.image_crop.width,
                height: ROOMASTIC.image_crop.height,
                imagen: ROOMASTIC.image_crop.imagen,
                carpeta: '";
        // line 26
        if (isset($context["tipo"])) { $_tipo_ = $context["tipo"]; } else { $_tipo_ = null; }
        echo twig_escape_filter($this->env, $_tipo_, "html", null, true);
        echo "'});
            \$.get(url, function (respuesta) {
                \$('#closeimage').click();
                var id = \$('#successimage').attr('data-id');
                var nombreImagen = \$('#successimage').attr('data-img');
    ";
        // line 31
        if (isset($context["especifico"])) { $_especifico_ = $context["especifico"]; } else { $_especifico_ = null; }
        if (($_especifico_ == "user")) {
            // line 32
            echo "                    \$('#imagenusuario').attr('src', '/uploads/";
            if (isset($context["tipo"])) { $_tipo_ = $context["tipo"]; } else { $_tipo_ = null; }
            echo twig_escape_filter($this->env, $_tipo_, "html", null, true);
            echo "/' + nombreImagen);
                    \$('#imagensubidanombre').val(nombreImagen);
    ";
        } elseif (($_especifico_ == "hoteles")) {
            // line 35
            echo "                    \$('#imagenessubidasinputs').append('<input type=\"text\" name=\"imageneshotel[]\" class=\"borraelemento_' + id + '\" value=\"' + id + '\"><br>');
                    \$('#sortable').append(' <li class=\"list-primary borraelemento_' + id + '\" data-id=\"' + id + '\"><i class=\" fa fa-ellipsis-v\"></i><div class=\"task-title\"><img src=\"/uploads/hoteles/' + nombreImagen + '\" width=\"200\"><div class=\"pull-right hidden-phone\"><a href=\"#\"><button class=\"btn btn-success btn-xs fa fa-pencil\"></button></a>&nbsp;<a href=\"#\"><button class=\"btn btn-primary btn-xs fa fa-eye\"></button></a>&nbsp;<a href=\"#\" onClick=\"deleteElement(\\'' + id + '\\'); return false;\"><button class=\"btn btn-danger btn-xs fa fa-trash-o\"></button></a></div></div></li>');
    ";
        }
        // line 38
        echo "                    \$.get(Routing.generate('hoteles_backend_imageupload_deletetempfile', {imagen: ROOMASTIC.image_crop.imagen}));
                }).fail(function () {
                    alert('ha habido un error :S');
                });

            });

        });

        function initJcrop()
        {

            \$('.requiresjcrop').hide();
            \$('#target').Jcrop({
                //onRelease: releaseCheck
                onSelect: showCoords,
                bgColor: 'black',
                bgOpacity: .4,
                setSelect: [";
        // line 56
        if (isset($context["crop1"])) { $_crop1_ = $context["crop1"]; } else { $_crop1_ = null; }
        echo twig_escape_filter($this->env, $_crop1_, "html", null, true);
        echo ", ";
        if (isset($context["crop2"])) { $_crop2_ = $context["crop2"]; } else { $_crop2_ = null; }
        echo twig_escape_filter($this->env, $_crop2_, "html", null, true);
        echo ", ";
        if (isset($context["crop3"])) { $_crop3_ = $context["crop3"]; } else { $_crop3_ = null; }
        echo twig_escape_filter($this->env, $_crop3_, "html", null, true);
        echo ", ";
        if (isset($context["crop4"])) { $_crop4_ = $context["crop4"]; } else { $_crop4_ = null; }
        echo twig_escape_filter($this->env, $_crop4_, "html", null, true);
        echo " ],
                aspectRatio: ";
        // line 57
        if (isset($context["crop5"])) { $_crop5_ = $context["crop5"]; } else { $_crop5_ = null; }
        echo twig_escape_filter($this->env, $_crop5_, "html", null, true);
        echo " / ";
        if (isset($context["crop6"])) { $_crop6_ = $context["crop6"]; } else { $_crop6_ = null; }
        echo twig_escape_filter($this->env, $_crop6_, "html", null, true);
        echo ",
                        boxWidth: 800,
            }, function () {

                jcrop_api = this;
                jcrop_api.animateTo([100, 100, 400, 300]);

                \$('#can_click,#can_move,#can_size').attr('checked', 'checked');
                \$('#ar_lock,#size_lock,#bg_swap').attr('checked', false);
                \$('.requiresjcrop').show();

            });

        }
        ;

        function replaceAll(text, busca, reemplaza) {
            while (text.toString().indexOf(busca) != - 1)
                text = text.toString().replace(busca, reemplaza);
            return text;
        }

        function showCoords(c)
        {
            imagen = replaceAll(\$('#successimage').attr('data-img'), \".\", \"___---___\")
            if (typeof c.x !== \"undefined\") {
                var posx = Math.round(c.x)
                var posy = Math.round(c.y)
                var width = Math.round(c.w)
                var height = Math.round(c.h)
            }
            if (typeof ROOMASTIC === \"undefined\") {
                ROOMASTIC = {};
            }
            if (typeof ROOMASTIC.image_crop === \"undefined\") {
                ROOMASTIC.image_crop = {};
            }
            ROOMASTIC.image_crop.imagen = imagen;
            ROOMASTIC.image_crop.posx = posx;
            ROOMASTIC.image_crop.posy = posy;
            ROOMASTIC.image_crop.width = width;
            ROOMASTIC.image_crop.height = height;

            var url = '/' + imagen + '/' + posx + '/' + posy + '/' + width + '/' + height
            \$('#successimage').attr('data-url', url);
        }
        ;

        function inserta(id, nombreImagen) {
            var mayor = 0;
            var dirImages = '/uploads/temporal/';
            \$('#target').attr('src', dirImages + nombreImagen);
            \$('.jcrop-preview').attr('src', dirImages + nombreImagen);
            \$('#successimage').attr('data-id', id);
            \$('#successimage').attr('data-img', nombreImagen);
            jcrop_api.setImage(dirImages + nombreImagen);
            \$('#clickmodal').click();
        }

</script>";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Crop:crop.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 57,  77 => 38,  72 => 35,  64 => 32,  61 => 31,  52 => 26,  29 => 7,  24 => 6,  17 => 1,  317 => 132,  315 => 131,  312 => 130,  309 => 129,  306 => 128,  303 => 127,  300 => 126,  297 => 125,  294 => 124,  291 => 123,  288 => 122,  285 => 121,  282 => 120,  279 => 119,  268 => 111,  264 => 110,  251 => 100,  239 => 90,  237 => 89,  235 => 88,  233 => 87,  231 => 86,  220 => 76,  217 => 75,  211 => 72,  206 => 71,  200 => 70,  194 => 67,  189 => 66,  183 => 65,  179 => 64,  177 => 63,  171 => 62,  166 => 60,  156 => 54,  150 => 52,  146 => 50,  139 => 48,  136 => 47,  131 => 46,  123 => 41,  114 => 36,  109 => 34,  104 => 33,  97 => 56,  92 => 28,  87 => 27,  80 => 24,  75 => 22,  70 => 21,  67 => 20,  59 => 18,  53 => 15,  45 => 9,  42 => 8,  36 => 5,  31 => 4,  28 => 3,);
    }
}
