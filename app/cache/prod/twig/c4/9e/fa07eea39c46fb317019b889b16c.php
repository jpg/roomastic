<?php

/* HotelesBackendBundle:UserHotel:index.html.twig */
class __TwigTemplate_c49efa07eea39c46fb317019b889b16c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de usuarios del hotel"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de usuarios del hotel"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <table class=\"table table-striped table-advance table-hover\">
                        <thead>
                            <tr>
                                <th></th>
                                <th>";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de usuario"), "html", null, true);
        echo "</th>
                                <th>";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</th>
                                <th>";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado"), "html", null, true);
        echo "</th>
                                <th>";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administrador"), "html", null, true);
        echo "</th>
                                <th>";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
        echo "</th>
                                <th>";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
        echo "</th>                                 
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                            ";
        // line 32
        if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_pagination_);
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 33
            echo "                                <tr>
                                    <td>
                                        ";
            // line 35
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            $this->env->loadTemplate("HotelesBackendBundle:Extras:thumbs.html.twig")->display(array_merge($context, array("dato" => $this->getAttribute($_entity_, "imagen"))));
            // line 36
            echo "                                    </td>                                      
                                    <td><a href=\"";
            // line 37
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_edit", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\">";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "getPublicUsername"), "html", null, true);
            echo "</a></td>
                                    <td>";
            // line 38
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "email"), "html", null, true);
            echo "</td>
                                    <td>
                                        ";
            // line 40
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            $this->env->loadTemplate("HotelesBackendBundle:Extras:checked.html.twig")->display(array_merge($context, array("dato" => $this->getAttribute($_entity_, "enabled"))));
            // line 41
            echo "                                    </td>
                                    <td>";
            // line 42
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            if ($this->getAttribute($_entity_, "administrador")) {
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si"), "html", null, true);
            } else {
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No"), "html", null, true);
            }
            echo "</td>
                                    <td>";
            // line 43
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_entity_, "hotel"), "empresa"), "html", null, true);
            echo "</td>
                                    <td>";
            // line 44
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "hotel"), "html", null, true);
            echo "</td>            
                                    <td>
                                        <a href=\"";
            // line 46
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_edit", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-xs\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar"), "html", null, true);
            echo "\"><i class=\"fa fa-pencil\"></i></a>
                                    </td>
                                </tr>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 50
        echo "                        </tbody>
                    </table>
                </div>
            </section>
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"span6\">
                        <a href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_new"), "html", null, true);
        echo "\" class=\"btn btn-success\">
                            ";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear un nuevo usuario hotel"), "html", null, true);
        echo "
                        </a>
                    </div>
                    <div class=\"span6\">
                        <div class=\"dataTables_paginate paging_bootstrap pagination withbuttons\">
                            ";
        // line 63
        if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
        echo $this->env->getExtension('knp_pagination')->render($_pagination_, "HotelesBackendBundle:Paginacion:paginacion.html.twig");
        echo "
                        </div>
                    </div>                  
                </div>          
            </section>
        </div>
    </div>    

";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:UserHotel:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 63,  193 => 58,  189 => 57,  180 => 50,  159 => 46,  153 => 44,  148 => 43,  138 => 42,  135 => 41,  132 => 40,  126 => 38,  118 => 37,  115 => 36,  112 => 35,  108 => 33,  90 => 32,  81 => 26,  77 => 25,  73 => 24,  69 => 23,  65 => 22,  61 => 21,  51 => 14,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
