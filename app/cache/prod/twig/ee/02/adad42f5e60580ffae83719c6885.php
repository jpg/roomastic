<?php

/* HotelesFrontendBundle:Frontend:listadoajax.mv.twig */
class __TwigTemplate_ee02adad42f5e60580ffae83719c6885 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
    ";
        // line 2
        if (isset($context["hoteles"])) { $_hoteles_ = $context["hoteles"]; } else { $_hoteles_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_hoteles_);
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 3
            echo "        <div class=\"single_hotel clearfix  stars-";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "calificacion"), "html", null, true);
            echo " ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "filtroServicios"), "html", null, true);
            echo "\" id=\"hotel-summary-";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\">

            <div class=\"clearfix\">

                <div class=\"cont_img\">
                    ";
            // line 8
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if ((twig_length_filter($this->env, $this->getAttribute($_objetohotel_, "imagenes")) > 0)) {
                // line 9
                echo "                        <a href=\"\" title=\"\"><img height=\"152\" width=\"215\" src=\"/uploads/hoteles/";
                if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($_objetohotel_, "imagenes"), 0, array(), "array"), "imagen"), "html", null, true);
                echo "\" alt=\"\"></a>
                        ";
            }
            // line 11
            echo "                </div>
                <ul class=\"estrellas left\">
                    ";
            // line 13
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "calificacion")));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 14
                echo "                        <li class=\"star\">
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 17
            echo "                    <div class=\"clearfix\"></div>
                </ul>

                <div class=\"dates left\">

                    <h2 class=\"name tit_hotel\"><a >";
            // line 22
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "</a></h2>
                    <!-- servicios -->
                    <ul class=\"servicios\">
                        ";
            // line 25
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "piscina") == 1)) {
                // line 26
                echo "                            <li class=\"piscina\">Piscina
                            </li>
                        ";
            }
            // line 29
            echo "                        ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "spa") == 1)) {
                // line 30
                echo "                            <li class=\"spa\">Spa
                            </li>
                        ";
            }
            // line 33
            echo "                        ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "wiFi") == 1)) {
                // line 34
                echo "                            <li class=\"wi-fi\">Wi-fi
                            </li>
                        ";
            }
            // line 37
            echo "                        ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "accesoAdaptado") == 1)) {
                // line 38
                echo "                            <li class=\"acceso-adaptado\">Acceso adaptado
                            </li>
                        ";
            }
            // line 41
            echo "                        ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "aceptanPerros") == 1)) {
                // line 42
                echo "                            <li class=\"perros\">Aceptan perros
                            </li>
                        ";
            }
            // line 45
            echo "                        ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "aparcamiento") == 1)) {
                // line 46
                echo "                            <li class=\"parking\">Aparcamiento/Parking
                            </li>
                        ";
            }
            // line 49
            echo "                        ";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            if (($this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "businessCenter") == 1)) {
                // line 50
                echo "                            <li class=\"business-center\">Business center
                            </li>
                        ";
            }
            // line 53
            echo "                        <div class=\"clearfix\"></div>
                    </ul>
                    <!-- icheckbox_timbre -->
                    <input class=\"icheckbox_timbre timbre\" name=\"hotel1\" type=\"checkbox\" data-hotel-id=\"";
            // line 56
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\">
                </div> 
                <div class=\"clearfix\"></div><!-- hotel_cont -->
            </div><!-- clearfix -->
        </div><!-- single_hotel -->
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 62
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listadoajax.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 62,  149 => 56,  144 => 53,  139 => 50,  135 => 49,  130 => 46,  126 => 45,  121 => 42,  117 => 41,  112 => 38,  108 => 37,  103 => 34,  99 => 33,  94 => 30,  90 => 29,  85 => 26,  82 => 25,  75 => 22,  68 => 17,  60 => 14,  55 => 13,  51 => 11,  44 => 9,  41 => 8,  25 => 3,  20 => 2,  17 => 1,);
    }
}
