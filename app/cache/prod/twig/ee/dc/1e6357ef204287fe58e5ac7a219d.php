<?php

/* HotelesFrontendBundle:Frontend:oferta.html.twig */
class __TwigTemplate_eedc1e6357ef204287fe58e5ac7a219d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->
    <head>

        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>Oferta - Roomastic</title>

        <meta name=\"description\" content=\"\">
        <!--<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">-->

        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

        <!--<link href=\"http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css\" rel=\"stylesheet\">-->
        <link href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\">

        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/daterangepicker-bs3.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/selectize.default.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/minimal.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/sweet-alert.css\">


        <link rel=\"icon\" type=\"image/x-icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\" />

        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/roomastic.css\">

        <script type=\"text/javascript\" src=\"https://maps.google.com/maps/api/js?sensor=true\"></script>
        <script src=\"/bundles/hotelesfrontend/js/vendor/modernizr/modernizr.js\"></script>

        ";
        // line 36
        echo "
        <!--[if lt IE 9]>
            <script src=\"js/vendor/html5shiv/dist/html5shiv.min.js\"></script>
        <![endif]-->
        <style type=\"text/css\">

            .wrapper.wpregistro {
                min-height: 0;
            }

            .modalregister {
                display: none;
                position: absolute;
                height: 100%;
                width: 100%;
                z-index: 4;
                background: rgba(59, 59, 59, 0.8);

            }
            .modalregister .header {
                width: 100%;
                height: 50px;
                padding: 5px 0 0 0; 
            }
            .modalregister .header .close {
                float: right;
                margin-right: 10px;
                color: #fff;
                font-size: 20px;
                font-weight: bold;
            }
            .modalregister .body{
                width: 100%;
                height: 550px;
            }
            .modalregister .bodyregister, .modalregister .bodyregisterextra {
                width: 100%;
                height: 550px;
            }            
            .modalregister .bodyregister .left, .modalregister .bodyregister .right {
                height:  100%;
                width:  44%;
                padding: 0 0 0 40px;

            }
            .hide{
                display: none;
            }
            input[type=password] {
                .radius;
                height: 31px;
                float: left;
                background-color: #fff;
                border: none;
                box-shadow: 0 0 0 2px rgba(235, 235, 235, 0.9) !important;
                color: #474747;
                font-size: 15px;
                font-weight: 700;
                box-sizing: border-box;
                padding: 0 9px;
            }            
        </style>

    </head> 
    ";
        // line 100
        $context["rrss"] = $this->env->getExtension('twig_extension')->getRRSS();
        // line 101
        echo "
    <body class=\"interna\">
        <!--[if lt IE 7]>
            <p class=\"browsehappy\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- aviso cookie -->
        <div id=\"cookie-bar\" class=\"fixed\"></div>

        <div class=\"wrapper wpregistro\">

            <div class=\"modalregister\">


                <div class=\"header\">
                    <a href=\"#\" class=\"close\">X</a>
                </div>

                <div class=\"body\">

                    <form class=\"form-signin\" action=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_check"), "html", null, true);
        echo "\" method=\"post\" id=\"formlogin\" ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo ">
                        <div class=\"errorlogin\"></div>

                        <h2 class=\"form-signin-heading\">";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Debes estar registrado para hacer tu oferta"), "html", null, true);
        echo "</h2>

                        <div class=\"login-wrap left\">
                            <p class=\"tit\">";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso"), "html", null, true);
        echo "</p>

                            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 129
        if (isset($context["csrf_token"])) { $_csrf_token_ = $context["csrf_token"]; } else { $_csrf_token_ = null; }
        echo twig_escape_filter($this->env, $_csrf_token_, "html", null, true);
        echo "\" />
                            <input type=\"text\" class=\"form-control\" placeholder=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuario"), "html", null, true);
        echo "\" name=\"_username\" value=\"";
        if (array_key_exists("last_username", $context)) {
            echo " ";
            if (isset($context["last_username"])) { $_last_username_ = $context["last_username"]; } else { $_last_username_ = null; }
            echo twig_escape_filter($this->env, $_last_username_, "html", null, true);
            echo " ";
        }
        echo "\" autofocus>
                            <input type=\"password\" class=\"form-control\" placeholder=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraseña"), "html", null, true);
        echo "\" name=\"_password\">

                            <button class=\"btn btn-lg btn-login btn-block\" type=\"submit\">";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceder"), "html", null, true);
        echo "</button>
                            <p class=\"forgor_ps\">
                                <a href=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_request"), "html", null, true);
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado tu contraseña?"), "html", null, true);
        echo "</a>
                            </p>

                            <p>";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("O accede con tus redes sociales:"), "html", null, true);
        echo "</p>

                            <div class=\"login-social-link\">
                                <a href=\"#\" class=\"facebook login-rrss\">
                                    <i class=\"fa fa-facebook\"></i>
                                    <!--Facebook-->
                                </a>
                                <a href=\"#\" class=\"google login-rrss\">
                                    <i class=\"fa fa-google-plus\"></i>
                                    <!-- google + -->
                                </a>
                            </div>

                        </div><!-- /left -->

                        <div class=\"login-wrap right\">
                            <p class=\"tit\">";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Registro"), "html", null, true);
        echo "</p>
                            <p>";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Puedes registrarte directamente en nuestra web:"), "html", null, true);
        echo "</p>

                            <button class=\"btn btn-lg btn-login btn-block showregister\" type=\"submit\">";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("REGISTRO EN LA WEB"), "html", null, true);
        echo "</button>

                            <p>";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("O con tu cuenta de usuario en alguna de estas redes sociales:"), "html", null, true);
        echo "</p>

                            <div class=\"login-social-link\">

                                <a href=\"#\" class=\"facebook login-rrss\">
                                    <i class=\"fa fa-facebook\"></i>
                                </a>

                                <div id=\"fb-root\"></div>
                                <script>
                                    if (typeof ROOMASTIC === 'undefined') {
                                        ROOMASTIC = {};
                                    }
                                    ROOMASTIC.facebookAppId = '";
        // line 172
        if (isset($context["configuracion"])) { $_configuracion_ = $context["configuracion"]; } else { $_configuracion_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_configuracion_, "fbappid"), "html", null, true);
        echo "';
                                    ROOMASTIC.state = '";
        // line 173
        if (isset($context["csrf_token"])) { $_csrf_token_ = $context["csrf_token"]; } else { $_csrf_token_ = null; }
        echo twig_escape_filter($this->env, $_csrf_token_, "html", null, true);
        echo "';
                                    (function () {
                                        var e = document.createElement('script');
                                        e.async = true;
                                        e.src = document.location.protocol +
                                                '//connect.facebook.net/en_US/all.js';
                                        document.getElementById('fb-root').appendChild(e);
                                    }());
                                </script>

                                <a href=\"#\" class=\"google login-rrss\">
                                    <i class=\"fa fa-google-plus\"></i>
                                    <!-- google + -->
                                    <script type=\"text/javascript\">
                                        (function () {
                                            var po = document.createElement('script');
                                            po.type = 'text/javascript';
                                            po.async = true;
                                            po.src = 'https://plus.google.com/js/client:plusone.js?onload=googleReady';
                                            var s = document.getElementsByTagName('script')[0];
                                            s.parentNode.insertBefore(po, s);
                                        })();


                                    </script>
                                </a>
                            </div>


                        </div><!-- /right -->
                        <div class=\"clearfix\"></div>

                        <!-- Modal -->
                        <div aria-hidden=\"true\" aria-labelledby=\"myModalLabel\" role=\"dialog\" tabindex=\"-1\" id=\"myModal\" class=\"modal fade\" style=\"display:none;\">
                            <div class=\"modal-dialog\">
                                <div class=\"modal-content\">
                                    <div class=\"modal-header\">
                                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                        <h4 class=\"modal-title\">";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado la contraseña?"), "html", null, true);
        echo "</h4>
                                    </div>
                                    <div class=\"modal-body\">
                                        <p>";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Escribe tu email y te reenviaremos una nueva contraseña"), "html", null, true);
        echo "</p>
                                        <input type=\"text\" name=\"email\" placeholder=\"Email\" autocomplete=\"off\" class=\"form-control placeholder-no-fix\">

                                    </div>
                                    <div class=\"modal-footer\">
                                        <button data-dismiss=\"modal\" class=\"btn btn-default\" type=\"button\">";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cancelar"), "html", null, true);
        echo "</button>
                                        <button class=\"btn btn-success\" type=\"button\">";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar"), "html", null, true);
        echo "</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal -->

                    </form><!-- form-signin -->


                </div>
                <!--/body -->


                <div class=\"bodyregister hide\">

                    <form class=\"form-signin\" action=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_registroajax"), "html", null, true);
        echo "\" id=\"formregister\" method=\"post\" ";
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_formRegister_);
        echo ">

                        <h2 class=\"form-signin-heading\">";
        // line 238
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Debes estar registrado para hacer tu oferta"), "html", null, true);
        echo "</h2>
                        <div class=\"login-wrap\">
                            <p class=\"tit\">";
        // line 240
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso"), "html", null, true);
        echo "</p>
                            <div class=\"input_newreg\">

                                ";
        // line 243
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "nombre"), array("attr" => array("class" => "form-control width1", "placeholder" => $this->env->getExtension('translator')->trans("Nombre"))));
        echo "  

                                ";
        // line 245
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "apellidos"), array("attr" => array("class" => "form-control width2", "placeholder" => $this->env->getExtension('translator')->trans("Apellidos"))));
        echo "   

                                ";
        // line 247
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "direccioncompleta"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("Dirección completa (Campo no obligatorio)"))));
        echo "    

                                ";
        // line 249
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "dni"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("DNI (Para dar veracidad a tu oferta)"))));
        echo "

                                ";
        // line 251
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "telefono"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("Teléfono (Para contactarte en caso de cambios en la reserva)"))));
        echo "    

                                ";
        // line 253
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegister_, "email"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("Email"))));
        echo "

                                ";
        // line 255
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($_formRegister_, "password"), "first"), array("attr" => array("class" => "form-control width6", "placeholder" => $this->env->getExtension('translator')->trans("Contraseña"))));
        echo "

                                ";
        // line 257
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($_formRegister_, "password"), "second"), array("attr" => array("class" => "form-control width7", "placeholder" => $this->env->getExtension('translator')->trans("Repite contraseña"))));
        echo "    


                                <div class=\"clearfix\"></div>

                                <button type=\"submit\" class=\"btn btn-lg btn-login btn-block w-es\" >";
        // line 262
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar datos y hacer oferta"), "html", null, true);
        echo "</button>
                                ";
        // line 263
        if (isset($context["formRegister"])) { $_formRegister_ = $context["formRegister"]; } else { $_formRegister_ = null; }
        echo $this->env->getExtension('form')->renderRest($_formRegister_);
        echo "
                            </div>
                        </div>
                    </form>  
                </div>

                <div class=\"bodyregisterextra hide\">

                    <form class=\"form-signin\" action=\"";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_registroextraajax"), "html", null, true);
        echo "\" id=\"formregisterextra\" method=\"post\"  ";
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_formRegisterextra_);
        echo ">
                        <h2 class=\"form-signin-heading\">";
        // line 272
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Completa tu registro para hacer tu oferta"), "html", null, true);
        echo "</h2>
                        <div class=\"login-wrap\">
                            <p class=\"tit\">";
        // line 274
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso"), "html", null, true);
        echo "</p>
                            <div class=\"input_newreg\">

                                ";
        // line 277
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "nombre"), array("attr" => array("class" => "form-control width1 nombre", "placeholder" => $this->env->getExtension('translator')->trans("Nombre"))));
        echo "  

                                ";
        // line 279
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "apellidos"), array("attr" => array("class" => "form-control width2 apellidos", "placeholder" => $this->env->getExtension('translator')->trans("Apellidos"))));
        echo "   

                                ";
        // line 281
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "direccioncompleta"), array("attr" => array("class" => "form-control width3 direccion", "placeholder" => $this->env->getExtension('translator')->trans("Dirección completa (Campo no obligatorio)"))));
        echo "

                                ";
        // line 283
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "dni"), array("attr" => array("class" => "form-control width3 dni", "placeholder " => $this->env->getExtension('translator')->trans("DNI (Para dar veracidad a tu oferta)"))));
        echo "

                                ";
        // line 285
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "telefono"), array("attr" => array("class" => "form-control width3 telefono", "placeholder" => $this->env->getExtension('translator')->trans("Teléfono (Para contactarte en caso de cambios en la reserva)"))));
        echo "    

                                ";
        // line 287
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_formRegisterextra_, "email"), array("attr" => array("class" => "form-control width3 email", "placeholder" => $this->env->getExtension('translator')->trans("Email"))));
        echo "

                                <div class=\"clearfix\"></div>

                                <button type=\"submit\" class=\"btn btn-lg btn-login btn-block w-es\">";
        // line 291
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar datos y hacer oferta"), "html", null, true);
        echo "</button>

                                <div class=\"clearfix\"></div>

                            </div>
                        </div>
                        ";
        // line 297
        if (isset($context["formRegisterextra"])) { $_formRegisterextra_ = $context["formRegisterextra"]; } else { $_formRegisterextra_ = null; }
        echo $this->env->getExtension('form')->renderRest($_formRegisterextra_);
        echo "
                    </form>  
                </div><!--/bodyregisterextra -->

            </div>
            <header id=\"header\" class=\"oferta\">
                <div class=\"lineatop\"></div>

                <div class=\"container clearfix\">
                    <a class=\"logo\" href=\"";
        // line 306
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"\"><h1>Roomastic</h1></a>

                    <div class=\"menu_int clearfix\">
                        <ul class=\"social\">
                            <li><a href=\"";
        // line 310
        if (isset($context["rrss"])) { $_rrss_ = $context["rrss"]; } else { $_rrss_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_rrss_, "twitter"), "html", null, true);
        echo "\" target=\"_blank\" title=\"twitter\">twitter</a></li>
                            <li><a class=\"facebook\" href=\"";
        // line 311
        if (isset($context["rrss"])) { $_rrss_ = $context["rrss"]; } else { $_rrss_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_rrss_, "facebook"), "html", null, true);
        echo "\" target=\"_blank\" title=\"facebook\">facebook</a></li>
                        </ul>
                        <ul class=\"menu\">                      
                            <li><a href=\"";
        // line 314
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"HOME\">HOME</a></li>
                            <li><a href=\"";
        // line 315
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_faqs"), "html", null, true);
        echo "\" title=\"FAQS\">FAQS</a></li>
                            <!--<li><a href=\"#\" title=\"AYUDA\">AYUDA</a></li>-->
                            <li><a href=\"";
        // line 317
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" title=\"CONTACTO\">CONTACTO</a></li>
                            <li><a href=\"";
        // line 318
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_quienesSomos"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos?"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos?"), "html", null, true);
        echo "</a></li>
                        </ul>
                    </div><!-- menu_int --> 
                </div><!-- container -->

            </header><!-- /header -->

            <div class=\"container clearfix\">

                <form action=\"\" method=\"post\" id=\"formoferta\">
                    ";
        // line 328
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "_token_oferta"));
        echo "


                    <div class=\"col_1\">

                        <h3 class=\"tit_int pagar\">¿Cuánto estás <br />dispuesto a pagar?</h3>
                        <div class=\"c_pagar clearfix\">
                            <p class=\"primero\">El precio debe ser por noche, habitación doble y en régimen de alojamiento. (Algunos hoteles ofrecen desayuno o media pensión incluido)</p>
                            ";
        // line 339
        echo "                            ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "preciohabitacion"), array("attr" => array("type" => "number", "placeholder" => "Precio", "tabindex" => "0")));
        echo "
                            <div class=\"clearfix\"></div>
                            <p>Los precios oficiales de los hoteles que has seleccionado <br /><strong>rondan entre los ";
        // line 341
        if (isset($context["precio_hotel_min"])) { $_precio_hotel_min_ = $context["precio_hotel_min"]; } else { $_precio_hotel_min_ = null; }
        echo twig_escape_filter($this->env, $_precio_hotel_min_, "html", null, true);
        echo " € y los ";
        if (isset($context["precio_hotel_max"])) { $_precio_hotel_max_ = $context["precio_hotel_max"]; } else { $_precio_hotel_max_ = null; }
        echo twig_escape_filter($this->env, $_precio_hotel_max_, "html", null, true);
        echo "  €</strong></p>
                        </div><!-- c_pagar -->

                        <hr>

                        <div class=\"clearfix\">
                            <h3 class=\"tit_int n_noches\">Número <br />de noches</h3>

                            <div class=\"caja_fecha\">
                                <span class=\"mtit_grey\">Fechas</span>
                                ";
        // line 354
        echo "                                ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        if (isset($context["fecha_inicio"])) { $_fecha_inicio_ = $context["fecha_inicio"]; } else { $_fecha_inicio_ = null; }
        if (isset($context["fecha_fin"])) { $_fecha_fin_ = $context["fecha_fin"]; } else { $_fecha_fin_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "fecha"), array("attr" => array("value" => ((twig_date_format_filter($this->env, $_fecha_inicio_, "d/m/y") . " - ") . twig_date_format_filter($this->env, $_fecha_fin_, "d/m/y")), "tabindex" => "1", "class" => "select_fecha", "placeholder" => "Fechas")));
        echo "
                            </div>

                            <div class=\"caja_noches\">
                                <span class=\"mtit_grey\">Noches</span>
                                ";
        // line 362
        echo "                                ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        if (isset($context["num_noches"])) { $_num_noches_ = $context["num_noches"]; } else { $_num_noches_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "numnoches"), array("attr" => array("value" => $_num_noches_, "placeholder" => "3", "readonly" => "")));
        echo "
                            </div>
                        </div><!-- clearfix -->

                        <br>

                        <h3 class=\"tit_int n_gente\">Selecciona el <br />número de huéspedes</h3>

                        <div class=\"clearfix\">
                            <div class=\"caja_adultos\">
                                <span class=\"mtit_grey\">Adultos</span>
                                ";
        // line 376
        echo "                                ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "numadultos"), array("attr" => array("value" => "1", "placeholder" => "1", "tabindex" => "2", "class" => "select_adultos")));
        echo "
                            </div>

                            <div class=\"caja_ninos\">
                                <span class=\"mtit_grey\">Niños hasta 3 años</span>
                                ";
        // line 384
        echo "                                ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "numninos"), array("attr" => array("value" => "0", "placeholder" => "0", "tabindex" => "3", "class" => "select_ninios")));
        echo "
                            </div>
                        </div><!-- clearfix -->

                        <hr>

                        <h3 class=\"tit_int n_habs\">Número de <br />habitaciones</h3>
                            ";
        // line 394
        echo "                            ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "numhabitaciones"), array("attr" => array("value" => "1", "placeholder" => "1", "tabindex" => "4", "class" => "select_habs")));
        echo "

                    </div><!-- col_1 -->
                    <div class=\"col_2\">
                        <h3 class=\"tit_int\">Hoteles <br />seleccionados</h3>
                        <p>Estos son los hoteles que has seleccionado y a los que vas a enviar tu oferta:</p>
                        <hr>

                        <ul class=\"hoteles\">
                            ";
        // line 403
        if (isset($context["hoteles"])) { $_hoteles_ = $context["hoteles"]; } else { $_hoteles_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_hoteles_);
        foreach ($context['_seq'] as $context["_key"] => $context["hotel"]) {
            // line 404
            echo "                                <li class=\"hotel_li\">
                                    <input type=\"checkbox\" class=\"checkbox-oferta\" name=\"hoteles[id][]\" id=\"";
            // line 405
            if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "id"), "html", null, true);
            echo "\" value=\"";
            if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "id"), "html", null, true);
            echo "\" checked>
                                    <label for=\"Hotel Diamante Suites\">";
            // line 406
            if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "nombrehotel"), "html", null, true);
            echo "</label>
                                </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 409
        echo "                        </ul>

                    </div><!-- col_2 -->
                    <div class=\"col_3\">
                        <h3>Precio total de tu oferta</h3>
                        <div class=\"caja\">
                            <p>En base a la configuración que nos acabas de facilitar, este será el precio total que vas a ofertar a los hoteles seleccionados:</p>
                            ";
        // line 419
        echo "                            ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_form_, "preciototaloferta"), array("attr" => array("value" => "", "placeholder" => "0", "readonly" => "")));
        echo "
                            <div class=\"clearfix\"></div>

                            <div class=\"acepto1 clearfix\">
                                <input type=\"checkbox\" id=\"acepto1\" class=\"checkbox-oferta\" name=\"acepto_condiciones\" required>
                                <label for=\"acepto1\">Acepto las condiciones de venta y de uso de esta web.</label>
                            </div>

                            <div class=\"acepto2 clearfix\">
                                <input type=\"checkbox\" id=\"acepto2\" class=\"checkbox-oferta\" name=\"acepto_suscribirme\" >
                                <label for=\"acepto2\">Acepto suscribirme al boletín de noticias RooMail</label>
                            </div>
                            <div class=\"validaoferta\">
                                ";
        // line 432
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if ($this->getAttribute($_app_, "user")) {
            // line 433
            echo "                                    ";
            $context["title"] = "Ya puedes hacer tu oferta";
            // line 434
            echo "                                ";
        } else {
            // line 435
            echo "                                    ";
            $context["title"] = "Todavia tienes que loguearte";
            // line 436
            echo "
                                ";
        }
        // line 438
        echo "
                                ";
        // line 440
        echo "                                <a class=\"btn_oferta\"  title=\"";
        if (isset($context["title"])) { $_title_ = $context["title"]; } else { $_title_ = null; }
        echo twig_escape_filter($this->env, $_title_, "html", null, true);
        echo "\" style=\"cursor: pointer;\" >hacer oferta</a>
                            </div>

                            </form>
                        </div><!-- caja -->
                    </div><!-- col_3 -->
                    ";
        // line 446
        $this->env->loadTemplate("HotelesBackendBundle:Extras:preload.html.twig")->display($context);
        // line 447
        echo "
            </div><!-- container -->

        </div><!-- wrapper -->

        ";
        // line 453
        echo "
        <footer id=\"footer\">

            <div class=\"footer1 footer-opaco\">
                <div class=\"container\">
                    <ul class=\"menu_foot clearfix\">                                                 
                        <li><a href=\"";
        // line 459
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_privacidad"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES PARTICULARES Y PRIVACIDAD"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 460
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_condicionesLegales"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES DEL SERVICIO"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 461
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_publicaTuHotel"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PUBLICA TU HOTEL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"v_movil\" href=\"";
        // line 462
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute($_app_, "request"), "attributes"), "get", array(0 => "_route"), "method"), twig_array_merge($this->env->getExtension('twig_extension')->getCurrentRouteParams(), array("version" => "mobile"))), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("VERSIÓN MÓVIL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"a_hoteles\" href=\"";
        // line 463
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_login"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ACCESO HOTELES"), "html", null, true);
        echo "</a></li>
                    </ul>
                    <div class=\"att_cliente\">
                        ATENCIÓN AL CLIENTE <br /> <span>911 610 156</span>
                    </div>
                </div><!-- container -->
            </div><!-- footer1 -->
            <div class=\"footer2\">
                <div class=\"container\">
                    <span>© Copyright. Todos los derechos reservados.</span>
                </div><!-- container -->
            </div><!-- footer2 -->

        </footer>

        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
        <script>window.jQuery || document.write('<script src=\"/bundles/hotelesfrontend/js/vendor/jquery/dist/jquery.min.js\"><\\/script>')</script>
        <script>
            if (typeof ROOMASTIC === 'undefined') {
                ROOMASTIC = {};
            }
            ROOMASTIC.googleID = '";
        // line 484
        if (isset($context["configuracion"])) { $_configuracion_ = $context["configuracion"]; } else { $_configuracion_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_configuracion_, "googAppId"), "html", null, true);
        echo "';
            ROOMASTIC.googleapikey = '";
        // line 485
        if (isset($context["configuracion"])) { $_configuracion_ = $context["configuracion"]; } else { $_configuracion_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_configuracion_, "googApiKey"), "html", null, true);
        echo "';
            ROOMASTIC.version_mv = false;
        </script>
        <script src=\"/bundles/hotelesfrontend/js/plugins.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.backstretch.min.js\"></script>
        <script src=\"https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/moment.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/daterangepicker.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/selectize.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/icheck.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/gmap3.min.js\"></script>
        <script src=\"";
        // line 496
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 497
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>
        ";
        // line 498
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "844e296_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296_0") : $this->env->getExtension('assets')->getAssetUrl("js/844e296_roomastic_1.js");
            // line 503
            echo "        <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>

        ";
            // asset "844e296_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296_1") : $this->env->getExtension('assets')->getAssetUrl("js/844e296_ajax-preload_2.js");
            echo "        <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>

        ";
            // asset "844e296_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296_2") : $this->env->getExtension('assets')->getAssetUrl("js/844e296_facebook_login_3.js");
            echo "        <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>

        ";
            // asset "844e296_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296_3") : $this->env->getExtension('assets')->getAssetUrl("js/844e296_google_login_4.js");
            echo "        <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>

        ";
            // asset "844e296_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296_4") : $this->env->getExtension('assets')->getAssetUrl("js/844e296_oferta_5.js");
            echo "        <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>

        ";
        } else {
            // asset "844e296"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296") : $this->env->getExtension('assets')->getAssetUrl("js/844e296.js");
            echo "        <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>

        ";
        }
        unset($context["asset_url"]);
        // line 506
        echo "        <!-- 
        <script src=\"/bundles/hotelesfrontend/js/roomastic.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/facebook_login.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/google_login.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/oferta.js\"></script> 
        -->

        <!-- sweet-alert -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/sweet-alert.js\"></script>
        <!-- cookies -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.cookiebar.js\"></script>

        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-53266623-1');
            ga('send', 'pageview');
        </script>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:oferta.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  875 => 506,  825 => 503,  821 => 498,  817 => 497,  813 => 496,  798 => 485,  793 => 484,  767 => 463,  760 => 462,  754 => 461,  748 => 460,  742 => 459,  734 => 453,  727 => 447,  725 => 446,  714 => 440,  711 => 438,  707 => 436,  704 => 435,  701 => 434,  698 => 433,  695 => 432,  677 => 419,  668 => 409,  658 => 406,  650 => 405,  647 => 404,  642 => 403,  628 => 394,  616 => 384,  606 => 376,  589 => 362,  577 => 354,  560 => 341,  553 => 339,  541 => 328,  524 => 318,  520 => 317,  515 => 315,  511 => 314,  504 => 311,  499 => 310,  492 => 306,  479 => 297,  470 => 291,  462 => 287,  456 => 285,  450 => 283,  444 => 281,  438 => 279,  432 => 277,  426 => 274,  421 => 272,  414 => 271,  402 => 263,  398 => 262,  389 => 257,  383 => 255,  377 => 253,  371 => 251,  365 => 249,  359 => 247,  353 => 245,  347 => 243,  341 => 240,  336 => 238,  328 => 236,  309 => 220,  305 => 219,  297 => 214,  291 => 211,  249 => 173,  244 => 172,  228 => 159,  223 => 157,  218 => 155,  214 => 154,  195 => 138,  187 => 135,  182 => 133,  177 => 131,  166 => 130,  161 => 129,  156 => 127,  150 => 124,  141 => 121,  119 => 101,  117 => 100,  51 => 36,  17 => 1,);
    }
}
