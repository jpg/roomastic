<?php

/* HotelesBackendBundle:Empresa:index.html.twig */
class __TwigTemplate_3753a095f4bb666a685a4ab1c7c0c091 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de empresas"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "

    <div class=\"row\">
        <div class=\"col-lg-12\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de empresas"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <!--  buscador + rtdos -->
                    <div class=\"adv-table\">
                        <div class=\"row\">
                            <div class=\"col-lg-6\">
                                <div id=\"editable-sample_length\" class=\"dataTables_length\">
                                    ";
        // line 23
        $this->env->loadTemplate("HotelesBackendBundle:Extras:select.html.twig")->display($context);
        // line 24
        echo "                                </div>
                            </div>
                            <div class=\"col-lg-6\">
                                ";
        // line 32
        echo "                            </div>
                        </div>
                        <!-- / buscador + rtdos -->
                        <!--  contentTabla -->
                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-advance table-hover\" id=\"hidden-table-info\">

                            <thead>
                                <tr>
                                    <th>";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la empresa"), "html", null, true);
        echo "</th>                                    
                                    <th>";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cif"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dirección de facturación"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Persona de contacto"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email de la persona de contacto"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono de la persona de contacto"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo de la persona contacto"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acciones"), "html", null, true);
        echo "</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 54
        if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_pagination_);
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 55
            echo "

                                    <tr>
                                        <td>";
            // line 58
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "nombreempresa"), "html", null, true);
            echo "</td>                                    
                                        <td>";
            // line 59
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "email"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 60
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "cif"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 61
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "direccionfacturacion"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 62
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "telefono"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 63
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "nombrepersonacontacto"), "html", null, true);
            echo " ";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "apellidospersonacontacto"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 64
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "emailpersonacontacto"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 65
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "telefonopersonacontacto"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 66
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "cargopersonacontacto"), "html", null, true);
            echo "</td>
                                        <td>
                                            ";
            // line 68
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            $this->env->loadTemplate("HotelesBackendBundle:Extras:empresaStatus.html.twig")->display(array_merge($context, array("status" => $this->getAttribute($_entity_, "status"))));
            // line 69
            echo "                                        </td>
                                        <td>
                                            <a href=\"";
            // line 71
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_edit", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-xs\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar"), "html", null, true);
            echo "\"><i class=\"fa fa-pencil\"></i></a>
                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 75
        echo "                            </tbody>
                        </table>
                        <!--  /contentTabla -->

                    </div> <!-- /addtable -->
                </div><!-- /panelbody -->

            </section>

            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"span6\">
                        ";
        // line 88
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 89
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_new"), "html", null, true);
            echo "\" class=\"btn btn-success\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear una nueva entrada"), "html", null, true);
            echo "</a>
                        ";
        }
        // line 91
        echo "                    </div>
                    <div class=\"span6\">
                        <div class=\"dataTables_paginate paging_bootstrap pagination withbuttons\">
                            ";
        // line 94
        if (isset($context["pagination"])) { $_pagination_ = $context["pagination"]; } else { $_pagination_ = null; }
        echo $this->env->getExtension('knp_pagination')->render($_pagination_, "HotelesBackendBundle:Paginacion:paginacion.html.twig");
        echo "
                        </div>
                    </div>                  
                </div>          
            </section>
            <!-- /btns -->

        </div><!-- /col -->

    </div><!-- /row -->

    ";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Empresa:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  257 => 94,  252 => 91,  244 => 89,  242 => 88,  227 => 75,  206 => 71,  202 => 69,  199 => 68,  193 => 66,  188 => 65,  183 => 64,  175 => 63,  170 => 62,  165 => 61,  160 => 60,  155 => 59,  150 => 58,  145 => 55,  127 => 54,  120 => 50,  116 => 49,  112 => 48,  108 => 47,  104 => 46,  100 => 45,  96 => 44,  92 => 43,  88 => 42,  84 => 41,  80 => 40,  70 => 32,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
