<?php

/* HotelesBackendBundle:Paginacion:paginacion.html.twig */
class __TwigTemplate_57ec1568321ebb4e004e460446517849 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["pageCount"]) ? $context["pageCount"] : null) > 1)) {
            // line 2
            echo "
    <ul>

    ";
            // line 5
            if (array_key_exists("previous", $context)) {
                // line 6
                echo "        <li class=\"prev\">
            <a href=\"";
                // line 7
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => (isset($context["previous"]) ? $context["previous"] : null)))), "html", null, true);
                echo "\">&laquo;&nbsp;";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Previous"), "html", null, true);
                echo "</a>
        </li>
    ";
            } else {
                // line 10
                echo "        <li class=\"disabled prev\">
            <a href=\"#\" onclick=\"return false\">&laquo;&nbsp;";
                // line 11
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Previous"), "html", null, true);
                echo "</a>
        </li>
    ";
            }
            // line 14
            echo "
    ";
            // line 15
            if (((isset($context["startPage"]) ? $context["startPage"] : null) > 1)) {
                // line 16
                echo "        <li>
            <a href=\"";
                // line 17
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => 1))), "html", null, true);
                echo "\">1</a>
        </li>
        ";
                // line 19
                if (((isset($context["startPage"]) ? $context["startPage"] : null) == 3)) {
                    // line 20
                    echo "            <li>
                <a href=\"";
                    // line 21
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => 2))), "html", null, true);
                    echo "\">2</a>
            </li>
        ";
                } elseif (((isset($context["startPage"]) ? $context["startPage"] : null) != 2)) {
                    // line 24
                    echo "        <li class=\"disabled\">
            <a href=\"#\" onclick=\"return false\">&hellip;</a>
            ";
                    // line 29
                    echo "        </li>
        ";
                }
                // line 31
                echo "    ";
            }
            // line 32
            echo "
    ";
            // line 33
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["pagesInRange"]) ? $context["pagesInRange"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 34
                echo "        ";
                if (((isset($context["page"]) ? $context["page"] : null) != (isset($context["current"]) ? $context["current"] : null))) {
                    // line 35
                    echo "            <li>
                <a href=\"";
                    // line 36
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => (isset($context["page"]) ? $context["page"] : null)))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["page"]) ? $context["page"] : null), "html", null, true);
                    echo "</a>
            </li>
        ";
                } else {
                    // line 39
                    echo "            <li class=\"active\">
                <a href=\"#\" onClick=\"return false\">";
                    // line 40
                    echo twig_escape_filter($this->env, (isset($context["page"]) ? $context["page"] : null), "html", null, true);
                    echo "</a>
            </li>
        ";
                }
                // line 43
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 45
            echo "
    ";
            // line 46
            if (((isset($context["pageCount"]) ? $context["pageCount"] : null) > (isset($context["endPage"]) ? $context["endPage"] : null))) {
                // line 47
                echo "        ";
                if (((isset($context["pageCount"]) ? $context["pageCount"] : null) > ((isset($context["endPage"]) ? $context["endPage"] : null) + 1))) {
                    // line 48
                    echo "            ";
                    if (((isset($context["pageCount"]) ? $context["pageCount"] : null) > ((isset($context["endPage"]) ? $context["endPage"] : null) + 2))) {
                        // line 49
                        echo "                <li class=\"disabled\">
                    <a href=\"#\" onclick=\"return false\">&hellip;</a>
                    ";
                        // line 54
                        echo "                </li>
            ";
                    } else {
                        // line 56
                        echo "                <li>
                    <a href=\"";
                        // line 57
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => ((isset($context["pageCount"]) ? $context["pageCount"] : null) - 1)))), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, ((isset($context["pageCount"]) ? $context["pageCount"] : null) - 1), "html", null, true);
                        echo "</a>
                </li>
            ";
                    }
                    // line 60
                    echo "        ";
                }
                // line 61
                echo "        <li>
            <a href=\"";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => (isset($context["pageCount"]) ? $context["pageCount"] : null)))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, (isset($context["pageCount"]) ? $context["pageCount"] : null), "html", null, true);
                echo "</a>
        </li>
    ";
            }
            // line 65
            echo "
    ";
            // line 66
            if (array_key_exists("next", $context)) {
                // line 67
                echo "        <li>
            <a href=\"";
                // line 68
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath((isset($context["route"]) ? $context["route"] : null), twig_array_merge((isset($context["query"]) ? $context["query"] : null), array((isset($context["pageParameterName"]) ? $context["pageParameterName"] : null) => (isset($context["next"]) ? $context["next"] : null)))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Next"), "html", null, true);
                echo "&nbsp;&raquo;</a>
        </li>
    ";
            } else {
                // line 71
                echo "        <li class=\"disabled\">
            <a href=\"#\" onClick=\"return false\">";
                // line 72
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Next"), "html", null, true);
                echo "&nbsp;&raquo;</a>
        </li>
    ";
            }
            // line 75
            echo "    </ul>

";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Paginacion:paginacion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 75,  181 => 72,  178 => 71,  170 => 68,  167 => 67,  165 => 66,  162 => 65,  154 => 62,  151 => 61,  148 => 60,  140 => 57,  137 => 56,  133 => 54,  129 => 49,  126 => 48,  123 => 47,  121 => 46,  118 => 45,  111 => 43,  105 => 40,  88 => 34,  84 => 33,  81 => 32,  78 => 31,  74 => 29,  70 => 24,  64 => 21,  59 => 19,  54 => 17,  51 => 16,  46 => 14,  40 => 11,  29 => 7,  26 => 6,  24 => 5,  103 => 55,  97 => 53,  85 => 49,  79 => 47,  55 => 39,  49 => 15,  43 => 35,  37 => 10,  53 => 12,  50 => 11,  35 => 9,  27 => 6,  25 => 29,  21 => 3,  19 => 2,  17 => 1,  470 => 179,  463 => 175,  456 => 171,  449 => 167,  442 => 163,  433 => 157,  414 => 141,  396 => 125,  379 => 121,  375 => 120,  365 => 118,  356 => 116,  348 => 115,  339 => 114,  336 => 113,  332 => 108,  330 => 107,  325 => 105,  321 => 104,  317 => 103,  311 => 102,  307 => 101,  303 => 100,  299 => 99,  295 => 98,  291 => 97,  285 => 96,  279 => 95,  275 => 94,  271 => 93,  264 => 92,  261 => 90,  244 => 89,  236 => 84,  232 => 83,  228 => 82,  224 => 81,  220 => 80,  216 => 79,  212 => 78,  208 => 77,  204 => 76,  200 => 75,  196 => 74,  192 => 73,  188 => 72,  184 => 71,  180 => 70,  175 => 69,  166 => 61,  158 => 55,  143 => 53,  139 => 52,  134 => 51,  132 => 50,  127 => 48,  122 => 45,  120 => 44,  115 => 41,  113 => 40,  102 => 39,  94 => 36,  91 => 35,  83 => 21,  80 => 20,  73 => 45,  67 => 43,  61 => 20,  57 => 16,  45 => 8,  42 => 7,  36 => 5,  31 => 31,  28 => 3,);
    }
}
