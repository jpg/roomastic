<?php

/* HotelesBackendBundle:Avisos:success.html.twig */
class __TwigTemplate_0dd58bced617a73cffb26f6837cd1cb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
        if ($this->getAttribute($this->getAttribute($_app_, "session"), "hasFlash", array(0 => "success"), "method")) {
            // line 2
            echo "    <!-- aviso éxito -->
    <div class=\"row\">
      <div class=\"col-lg-6\">
        <section class=\"panel\">
           <div class=\"alert alert-success alert-block fade in\">
              <button data-dismiss=\"alert\" class=\"close close-sm\" type=\"button\">
                  <i class=\"fa fa-times\"></i>
              </button>
              <h4>
                  <i class=\"fa fa-ok-sign\"></i>
                  ";
            // line 12
            if (isset($context["cabecera"])) { $_cabecera_ = $context["cabecera"]; } else { $_cabecera_ = null; }
            echo twig_escape_filter($this->env, $_cabecera_, "html", null, true);
            echo "
              </h4>
              <p>";
            // line 14
            if (isset($context["app"])) { $_app_ = $context["app"]; } else { $_app_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_app_, "session"), "flash", array(0 => "success"), "method"), "html", null, true);
            echo "</p>
          </div>
        </section>
      </div>
    </div>
    <!-- / aviso éxito -->
";
        }
        // line 21
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Avisos:success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 21,  38 => 14,  32 => 12,  20 => 2,  17 => 1,  1374 => 615,  1371 => 614,  1367 => 616,  1365 => 614,  1339 => 590,  1335 => 588,  1321 => 587,  1315 => 585,  1309 => 584,  1302 => 582,  1298 => 581,  1294 => 579,  1290 => 577,  1287 => 576,  1281 => 575,  1263 => 574,  1260 => 573,  1256 => 571,  1253 => 570,  1249 => 568,  1246 => 567,  1230 => 565,  1225 => 564,  1222 => 563,  1213 => 556,  1210 => 555,  1207 => 554,  1160 => 506,  1157 => 505,  1153 => 490,  1150 => 489,  1144 => 11,  1137 => 617,  1135 => 505,  1119 => 491,  1117 => 489,  1114 => 488,  1111 => 487,  1109 => 486,  1105 => 484,  1091 => 461,  1080 => 455,  1075 => 454,  1068 => 452,  1063 => 451,  1056 => 449,  1051 => 448,  1044 => 446,  1039 => 445,  1032 => 443,  1027 => 442,  1020 => 440,  1015 => 439,  1008 => 437,  1003 => 436,  996 => 434,  991 => 433,  985 => 430,  958 => 428,  954 => 426,  952 => 425,  941 => 419,  935 => 418,  927 => 415,  921 => 414,  918 => 413,  912 => 410,  903 => 408,  900 => 407,  898 => 406,  888 => 401,  883 => 400,  876 => 398,  871 => 397,  865 => 394,  856 => 392,  852 => 390,  849 => 389,  847 => 388,  836 => 382,  831 => 381,  824 => 379,  819 => 378,  812 => 376,  807 => 375,  800 => 373,  795 => 372,  788 => 370,  783 => 369,  777 => 366,  759 => 364,  755 => 362,  753 => 361,  750 => 360,  739 => 354,  735 => 353,  728 => 351,  724 => 350,  717 => 348,  713 => 347,  706 => 345,  702 => 344,  695 => 342,  691 => 341,  684 => 339,  680 => 338,  673 => 336,  669 => 335,  662 => 331,  654 => 329,  651 => 328,  649 => 327,  642 => 323,  634 => 321,  630 => 319,  622 => 314,  614 => 312,  610 => 310,  608 => 309,  601 => 305,  593 => 303,  575 => 287,  567 => 284,  564 => 283,  562 => 282,  555 => 280,  543 => 273,  539 => 271,  532 => 269,  529 => 268,  523 => 264,  514 => 252,  504 => 237,  501 => 236,  499 => 235,  496 => 234,  485 => 228,  472 => 222,  462 => 220,  457 => 218,  451 => 214,  444 => 212,  441 => 211,  434 => 210,  423 => 204,  416 => 202,  413 => 201,  401 => 198,  396 => 195,  389 => 193,  386 => 192,  379 => 191,  374 => 189,  363 => 186,  357 => 182,  350 => 180,  347 => 179,  342 => 176,  339 => 175,  336 => 174,  333 => 173,  330 => 172,  317 => 164,  314 => 163,  295 => 160,  277 => 159,  261 => 153,  254 => 151,  251 => 150,  244 => 145,  242 => 144,  239 => 143,  237 => 142,  226 => 136,  223 => 135,  205 => 133,  187 => 132,  177 => 130,  171 => 126,  164 => 124,  161 => 123,  155 => 119,  152 => 118,  144 => 113,  58 => 30,  54 => 29,  33 => 11,  21 => 1,  569 => 260,  561 => 254,  545 => 252,  542 => 251,  536 => 249,  531 => 248,  528 => 247,  510 => 239,  502 => 241,  497 => 238,  495 => 237,  486 => 230,  479 => 224,  475 => 226,  469 => 221,  467 => 221,  459 => 219,  453 => 218,  447 => 214,  437 => 210,  429 => 208,  418 => 200,  414 => 199,  408 => 195,  406 => 200,  400 => 190,  393 => 188,  391 => 187,  385 => 185,  377 => 181,  364 => 170,  359 => 164,  355 => 163,  345 => 157,  331 => 146,  326 => 144,  321 => 142,  312 => 137,  307 => 136,  302 => 135,  297 => 134,  292 => 133,  275 => 120,  267 => 157,  255 => 109,  246 => 105,  228 => 101,  220 => 98,  216 => 96,  197 => 95,  194 => 94,  184 => 91,  181 => 90,  176 => 89,  170 => 87,  165 => 85,  151 => 74,  135 => 61,  119 => 48,  103 => 35,  85 => 21,  80 => 20,  75 => 19,  70 => 18,  60 => 10,  57 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
