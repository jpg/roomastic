<?php

/* HotelesFrontendBundle:Frontend:listadoAJAXlateral.mv.twig */
class __TwigTemplate_a5530b61e431067234f51980c01ec2bd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
    ";
        // line 2
        if (isset($context["hoteles"])) { $_hoteles_ = $context["hoteles"]; } else { $_hoteles_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_hoteles_);
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 3
            echo "        <li class=\"hotel_li hotel-selected\"  style=\"display: none;\" id=\"hotel-checkbox-";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\" data-hotel-id=\"";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "id"), "html", null, true);
            echo "\">
            <input type=\"checkbox\" class=\"checkbox-lateral\" id=\"";
            // line 4
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "\" checked>
            <label for=\"";
            // line 5
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "\">";
            if (isset($context["objetohotel"])) { $_objetohotel_ = $context["objetohotel"]; } else { $_objetohotel_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_objetohotel_, "hotel"), "nombrehotel"), "html", null, true);
            echo "</label>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 8
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listadoAJAXlateral.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 8,  39 => 5,  34 => 4,  162 => 62,  149 => 56,  144 => 53,  139 => 50,  135 => 49,  130 => 46,  126 => 45,  121 => 42,  117 => 41,  112 => 38,  108 => 37,  103 => 34,  99 => 33,  94 => 30,  90 => 29,  85 => 26,  82 => 25,  75 => 22,  68 => 17,  60 => 14,  55 => 13,  51 => 11,  44 => 9,  41 => 8,  25 => 3,  20 => 2,  17 => 1,);
    }
}
