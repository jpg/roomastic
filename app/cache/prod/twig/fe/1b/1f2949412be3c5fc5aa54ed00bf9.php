<?php

/* HotelesBackendBundle:Contacto:index.html.twig */
class __TwigTemplate_fe1b1f2949412be3c5fc5aa54ed00bf9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de contacto"), "html", null, true);
        echo "
";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "

    <div class=\"row\">
        <div class=\"col-lg-12\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de contacto"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <!--  buscador + rtdos -->
                    <div class=\"adv-table\">
                        <div class=\"row\">
                            <div class=\"col-lg-6\">
                                <div id=\"editable-sample_length\" class=\"dataTables_length\">
                                    ";
        // line 24
        $this->env->loadTemplate("HotelesBackendBundle:Extras:select.html.twig")->display($context);
        // line 25
        echo "                                </div>
                            </div>
                            <div class=\"col-lg-6\">
                                <div class=\"dataTables_filter\" id=\"editable-sample_filter\">
                                    <label>";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Buscar"), "html", null, true);
        echo ": <input type=\"text\" aria-controls=\"editable-sample\" class=\"form-control medium\"></label>
                                </div>
                            </div>
                        </div>
                        <!-- / buscador + rtdos -->
                        <!--  contentTabla -->
                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-advance table-hover\" id=\"hidden-table-info\">

                            <thead>
                                <tr>
                                    <th>";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Id"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Texto"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha"), "html", null, true);
        echo "   </th>
                                    <th>";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Atendido?"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acciones"), "html", null, true);
        echo "</th>   
                            </thead>
                            <tbody>
                                ";
        // line 49
        if (isset($context["entities"])) { $_entities_ = $context["entities"]; } else { $_entities_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_entities_);
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 50
            echo "                                    <tr>
                                        <td><a href=\"";
            // line 51
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb_show", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\">";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "id"), "html", null, true);
            echo "</a></td>
                                        <td>";
            // line 52
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "nombre"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 53
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "apellidos"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 54
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "email"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 55
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "texto"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 56
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            if ($this->getAttribute($_entity_, "fecha")) {
                if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_entity_, "fecha"), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                                        <td>";
            // line 57
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            $this->env->loadTemplate("HotelesBackendBundle:Contacto:atendido.html.twig")->display(array_merge($context, array("atendido" => $this->getAttribute($_entity_, "atendido"))));
            echo "</td>
                                        <td>
                                            ";
            // line 62
            echo "                                            <a href=\"";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb_edit", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-xs\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar"), "html", null, true);
            echo "\"><i class=\"fa fa-pencil\"></i></a>
                                        </td>
                                    </tr>

                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 67
        echo "                            </tbody>
                        </table>
                        <!--  /contentTabla -->

                    </div> <!-- /addtable -->
                </div><!-- /panelbody -->

            </section>


        </div><!-- /col -->

    </div><!-- /row -->



";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Contacto:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 67,  181 => 62,  175 => 57,  167 => 56,  162 => 55,  157 => 54,  152 => 53,  147 => 52,  139 => 51,  136 => 50,  118 => 49,  112 => 46,  108 => 45,  104 => 44,  100 => 43,  96 => 42,  92 => 41,  88 => 40,  84 => 39,  71 => 29,  65 => 25,  63 => 24,  52 => 16,  44 => 10,  41 => 9,  35 => 5,  30 => 4,  27 => 3,);
    }
}
