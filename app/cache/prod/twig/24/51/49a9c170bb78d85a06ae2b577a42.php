<?php

/* HotelesFrontendBundle:TPV:pago-inicial.html.twig */
class __TwigTemplate_245149a9c170bb78d85a06ae2b577a42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle:TPV:layoutTPV.html.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle:TPV:layoutTPV.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 5
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "7a501bd_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7a501bd_0") : $this->env->getExtension('assets')->getAssetUrl("js/7a501bd_pago-inicial_1.js");
            // line 6
            echo "    <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "7a501bd"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7a501bd") : $this->env->getExtension('assets')->getAssetUrl("js/7a501bd.js");
            echo "    <script type=\"text/javascript\" src=\"";
            if (isset($context["asset_url"])) { $_asset_url_ = $context["asset_url"]; } else { $_asset_url_ = null; }
            echo twig_escape_filter($this->env, $_asset_url_, "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 9
    public function block_content($context, array $blocks = array())
    {
        // line 10
        echo "    <div class=\"set-height-container\">

        <div class=\"contenido confirmaciontpv\">

            <h2>";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Confirma tus datos y realiza el pago"), "html", null, true);
        echo "</h2>
            <p>";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("El hotel ha aceptado tu oferta. A continuación te presentamos un resumen de la reserva que vas a realizar."), "html", null, true);
        echo "</p>
            <p>";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Revisa cuidadosamente los datos y realiza tu pago."), "html", null, true);
        echo "</p>
            <div class=\"separador\"></div>
            <div class=\"bq\">
                <div class=\"tit\">";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("El hotel"), "html", null, true);
        echo "</div>
                <div class=\"cont clearfix\">
                    <p class=\"name\">";
        // line 21
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "nombrehotel"), "html", null, true);
        echo "</p>
                    <p>";
        // line 22
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "ubicacion"), "html", null, true);
        echo "</p>
                    <div class=\"estrellas\">
                        <img src=\"/bundles/hotelesfrontend/img/";
        // line 24
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_hotel_, "calificacion"), "html", null, true);
        echo "stars.png\">
                    </div>
                    <div class=\"left\">
                        <img src=\"";
        // line 27
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        echo twig_escape_filter($this->env, (($this->getAttribute($_hotel_, "getImagePath") . "/") . $this->getAttribute($this->getAttribute($this->getAttribute($_hotel_, "getOrderedImagenesDelHotel"), 0, array(), "array"), "imagen")), "html", null, true);
        echo "\" class=\"imgppal\">
                    </div>
                    <div class=\"right\">
                        <ul class=\"servicios\">

                            ";
        // line 32
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "piscina") == 1)) {
            // line 33
            echo "                                <li class=\"piscina\">
                                    <label for=\"piscina\">";
            // line 34
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Piscina"), "html", null, true);
            echo "</label>
                                </li>
                            ";
        }
        // line 37
        echo "
                            ";
        // line 38
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "spa") == 1)) {
            // line 39
            echo "                                <li class=\"spa\">
                                    <label for=\"Spa\">";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Spa"), "html", null, true);
            echo "</label>
                                </li>
                            ";
        }
        // line 43
        echo "
                            ";
        // line 44
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "wiFi") == 1)) {
            // line 45
            echo "                                <li class=\"wi-fi\">
                                    <label for=\"Bar\">";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wi-Fi"), "html", null, true);
            echo "</label>
                                </li>\t
                            ";
        }
        // line 49
        echo "
                            ";
        // line 50
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "accesoAdaptado") == 1)) {
            // line 51
            echo "                                <li class=\"acceso-adaptado\">
                                    <label for=\"Acceso-Adaptado\">";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso adaptado"), "html", null, true);
            echo "</label>
                                </li>
                            ";
        }
        // line 55
        echo "
                            ";
        // line 56
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "aceptanPerros") == 1)) {
            // line 57
            echo "                                <li class=\"perros\">
                                    <label for=\"perros\">";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptan perros"), "html", null, true);
            echo "</label>
                                </li>
                            ";
        }
        // line 61
        echo "
                            ";
        // line 62
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "aparcamiento") == 1)) {
            // line 63
            echo "                                <li class=\"parking\">
                                    <label for=\"parking\">";
            // line 64
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aparcamiento/Parking"), "html", null, true);
            echo "</label>
                                </li>
                            ";
        }
        // line 67
        echo "
                            ";
        // line 68
        if (isset($context["hotel"])) { $_hotel_ = $context["hotel"]; } else { $_hotel_ = null; }
        if (($this->getAttribute($_hotel_, "businessCenter") == 1)) {
            // line 69
            echo "                                <li class=\"business-center\">
                                    <label for=\"business-center\">";
            // line 70
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Business center"), "html", null, true);
            echo "</label>
                                </li>
                            ";
        }
        // line 73
        echo "                        </ul>

                    </div>
                </div>

            </div>
            <div class=\"bq\">
                <div class=\"tit\">";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la reserva"), "html", null, true);
        echo "</div>
                <div class=\"cont\">
                    <ul class=\"datos\">
                        <li class=\"left\">
                            <p>";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha de entrada"), "html", null, true);
        echo "</p>
                            <p><span>";
        // line 85
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($_oferta_, "fechain"), "d/m/Y"), "html", null, true);
        echo "</span></p>
                        </li>
                        <li class=\"left\">
                            <p>";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de adultos"), "html", null, true);
        echo "</p>
                            <p><span>";
        // line 89
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_oferta_, "numadultos"), "html", null, true);
        echo "</span></p>
                        </li>
                        <li class=\"left\">
                            <p>";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de noches"), "html", null, true);
        echo "</p>
                            <p><span>";
        // line 93
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_oferta_, "numnoches"), "html", null, true);
        echo "</span></p>
                        </li>
                        ";
        // line 95
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        if (($this->getAttribute($_oferta_, "numninos") != 0)) {
            // line 96
            echo "                            <li class=\"left\">
                                <p>";
            // line 97
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de niños"), "html", null, true);
            echo "</p>
                                <p><span>oferta.numninos</span></p>
                            </li>
                        ";
        }
        // line 101
        echo "
                        <li class=\"left\">
                            <p>";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre y apellidos"), "html", null, true);
        echo "</p>
                            <p><span>";
        // line 104
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_oferta_, "usuario"), "getNombreCompleto"), "html", null, true);
        echo "</span></p>
                        </li>
                        <li class=\"left\">
                            <p>";
        // line 107
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Código de tu operación"), "html", null, true);
        echo "</p>
                            <p><span>";
        // line 108
        if (isset($context["sale"])) { $_sale_ = $context["sale"]; } else { $_sale_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_sale_, "firma"), "html", null, true);
        echo "</span></p>
                        </li>
                        <li>
                            <p>";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("DNI"), "html", null, true);
        echo "</p>
                            <p><span>";
        // line 112
        if (isset($context["oferta"])) { $_oferta_ = $context["oferta"]; } else { $_oferta_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($_oferta_, "usuario"), "dni"), "html", null, true);
        echo "</span></p>
                        </li>
                        <li>
                            <p>";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Importe total de la reserva"), "html", null, true);
        echo "</p>
                            <p><span>";
        // line 116
        if (isset($context["sale"])) { $_sale_ = $context["sale"]; } else { $_sale_ = null; }
        echo twig_escape_filter($this->env, (twig_number_format_filter($this->env, $this->getAttribute($_sale_, "amount"), 2, ",", ".") . $this->env->getExtension('translator')->trans("€")), "html", null, true);
        echo "</span></p>
                        </li>
                    </ul>
                    <a href=\"#\" class=\"btn boton-pago\">";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("pagar con tarjeta"), "html", null, true);
        echo " <img src=\"/bundles/hotelesfrontend/img/icon_tarjeta.png\" alt=\"\"></a>
                    <div>
                        <iframe id=\"iframe\" name=\"iframe\" style=\"width:100%;height:650px;border:0;margin-top:30px;\"></iframe>
                    </div>
                    <form target=\"iframe\" class=\"formulario-pago-inicial\"name=\"compra\" action=\"";
        // line 123
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "entorno"), "html", null, true);
        echo "\" method=\"post\" > 
                        <input type=\"hidden\" name=\"Ds_Merchant_Amount\" value='";
        // line 124
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "importe"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_Currency\" value='";
        // line 125
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "moneda"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_Order\"  value='";
        // line 126
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "pedido"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_MerchantCode\" value='";
        // line 127
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "codigoFuc"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_Terminal\" value='";
        // line 128
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "terminal"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_TransactionType\" value='";
        // line 129
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "transactionType"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_MerchantURL\" value='";
        // line 130
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "urlNotificacion"), "html", null, true);
        echo "'>

                        <input type=\"hidden\" name=\"Ds_Merchant_MerchantSignature\" value='";
        // line 132
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "getFirma"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_UrlOK\" value='";
        // line 133
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "urlOK"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_UrlKO\" value='";
        // line 134
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "urlKO"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_MerchantName\" value='";
        // line 135
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "nombreComercio"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_PayMethods\" value='";
        // line 136
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "metodo"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" name=\"Ds_Merchant_ProductDescription\" value='";
        // line 137
        if (isset($context["payment"])) { $_payment_ = $context["payment"]; } else { $_payment_ = null; }
        echo twig_escape_filter($this->env, $this->getAttribute($_payment_, "descripcion"), "html", null, true);
        echo "'>
                        <input type=\"hidden\" id=\"send\" />
                    </form>
                </div>

            </div>
            <div class=\"clearfix\"></div>
            <div class=\"separador\"></div>

        </div><!-- contenido -->
    </div>
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:TPV:pago-inicial.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  396 => 137,  391 => 136,  386 => 135,  381 => 134,  376 => 133,  371 => 132,  365 => 130,  360 => 129,  355 => 128,  350 => 127,  345 => 126,  340 => 125,  335 => 124,  330 => 123,  323 => 119,  316 => 116,  312 => 115,  305 => 112,  301 => 111,  294 => 108,  290 => 107,  283 => 104,  279 => 103,  275 => 101,  268 => 97,  265 => 96,  262 => 95,  256 => 93,  252 => 92,  245 => 89,  241 => 88,  234 => 85,  230 => 84,  223 => 80,  214 => 73,  208 => 70,  205 => 69,  202 => 68,  199 => 67,  193 => 64,  190 => 63,  187 => 62,  184 => 61,  178 => 58,  175 => 57,  172 => 56,  169 => 55,  163 => 52,  160 => 51,  157 => 50,  154 => 49,  148 => 46,  145 => 45,  142 => 44,  139 => 43,  133 => 40,  130 => 39,  127 => 38,  124 => 37,  118 => 34,  115 => 33,  112 => 32,  103 => 27,  96 => 24,  90 => 22,  85 => 21,  80 => 19,  74 => 16,  70 => 15,  66 => 14,  60 => 10,  57 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
