<?php

/* HotelesBackendBundle:Paginacion:paginacion.html.twig */
class __TwigTemplate_68ff76624248c78214e46f9eb12d980f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["pageCount"])) { $_pageCount_ = $context["pageCount"]; } else { $_pageCount_ = null; }
        if (($_pageCount_ > 1)) {
            // line 2
            echo "
    <ul>

    ";
            // line 5
            if (array_key_exists("previous", $context)) {
                // line 6
                echo "        <li class=\"prev\">
            <a href=\"";
                // line 7
                if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                if (isset($context["previous"])) { $_previous_ = $context["previous"]; } else { $_previous_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => $_previous_))), "html", null, true);
                echo "\">&laquo;&nbsp;";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Previous"), "html", null, true);
                echo "</a>
        </li>
    ";
            } else {
                // line 10
                echo "        <li class=\"disabled prev\">
            <a href=\"#\" onclick=\"return false\">&laquo;&nbsp;";
                // line 11
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Previous"), "html", null, true);
                echo "</a>
        </li>
    ";
            }
            // line 14
            echo "
    ";
            // line 15
            if (isset($context["startPage"])) { $_startPage_ = $context["startPage"]; } else { $_startPage_ = null; }
            if (($_startPage_ > 1)) {
                // line 16
                echo "        <li>
            <a href=\"";
                // line 17
                if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => 1))), "html", null, true);
                echo "\">1</a>
        </li>
        ";
                // line 19
                if (isset($context["startPage"])) { $_startPage_ = $context["startPage"]; } else { $_startPage_ = null; }
                if (($_startPage_ == 3)) {
                    // line 20
                    echo "            <li>
                <a href=\"";
                    // line 21
                    if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                    if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                    if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => 2))), "html", null, true);
                    echo "\">2</a>
            </li>
        ";
                } elseif (($_startPage_ != 2)) {
                    // line 24
                    echo "        <li class=\"disabled\">
            <a href=\"#\" onclick=\"return false\">&hellip;</a>
            ";
                    // line 29
                    echo "        </li>
        ";
                }
                // line 31
                echo "    ";
            }
            // line 32
            echo "
    ";
            // line 33
            if (isset($context["pagesInRange"])) { $_pagesInRange_ = $context["pagesInRange"]; } else { $_pagesInRange_ = null; }
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($_pagesInRange_);
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 34
                echo "        ";
                if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
                if (isset($context["current"])) { $_current_ = $context["current"]; } else { $_current_ = null; }
                if (($_page_ != $_current_)) {
                    // line 35
                    echo "            <li>
                <a href=\"";
                    // line 36
                    if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                    if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                    if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                    if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => $_page_))), "html", null, true);
                    echo "\">";
                    if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
                    echo twig_escape_filter($this->env, $_page_, "html", null, true);
                    echo "</a>
            </li>
        ";
                } else {
                    // line 39
                    echo "            <li class=\"active\">
                <a href=\"#\" onClick=\"return false\">";
                    // line 40
                    if (isset($context["page"])) { $_page_ = $context["page"]; } else { $_page_ = null; }
                    echo twig_escape_filter($this->env, $_page_, "html", null, true);
                    echo "</a>
            </li>
        ";
                }
                // line 43
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 45
            echo "
    ";
            // line 46
            if (isset($context["pageCount"])) { $_pageCount_ = $context["pageCount"]; } else { $_pageCount_ = null; }
            if (isset($context["endPage"])) { $_endPage_ = $context["endPage"]; } else { $_endPage_ = null; }
            if (($_pageCount_ > $_endPage_)) {
                // line 47
                echo "        ";
                if (isset($context["pageCount"])) { $_pageCount_ = $context["pageCount"]; } else { $_pageCount_ = null; }
                if (isset($context["endPage"])) { $_endPage_ = $context["endPage"]; } else { $_endPage_ = null; }
                if (($_pageCount_ > ($_endPage_ + 1))) {
                    // line 48
                    echo "            ";
                    if (isset($context["pageCount"])) { $_pageCount_ = $context["pageCount"]; } else { $_pageCount_ = null; }
                    if (isset($context["endPage"])) { $_endPage_ = $context["endPage"]; } else { $_endPage_ = null; }
                    if (($_pageCount_ > ($_endPage_ + 2))) {
                        // line 49
                        echo "                <li class=\"disabled\">
                    <a href=\"#\" onclick=\"return false\">&hellip;</a>
                    ";
                        // line 54
                        echo "                </li>
            ";
                    } else {
                        // line 56
                        echo "                <li>
                    <a href=\"";
                        // line 57
                        if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                        if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                        if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                        if (isset($context["pageCount"])) { $_pageCount_ = $context["pageCount"]; } else { $_pageCount_ = null; }
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => ($_pageCount_ - 1)))), "html", null, true);
                        echo "\">";
                        if (isset($context["pageCount"])) { $_pageCount_ = $context["pageCount"]; } else { $_pageCount_ = null; }
                        echo twig_escape_filter($this->env, ($_pageCount_ - 1), "html", null, true);
                        echo "</a>
                </li>
            ";
                    }
                    // line 60
                    echo "        ";
                }
                // line 61
                echo "        <li>
            <a href=\"";
                // line 62
                if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                if (isset($context["pageCount"])) { $_pageCount_ = $context["pageCount"]; } else { $_pageCount_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => $_pageCount_))), "html", null, true);
                echo "\">";
                if (isset($context["pageCount"])) { $_pageCount_ = $context["pageCount"]; } else { $_pageCount_ = null; }
                echo twig_escape_filter($this->env, $_pageCount_, "html", null, true);
                echo "</a>
        </li>
    ";
            }
            // line 65
            echo "
    ";
            // line 66
            if (array_key_exists("next", $context)) {
                // line 67
                echo "        <li>
            <a href=\"";
                // line 68
                if (isset($context["route"])) { $_route_ = $context["route"]; } else { $_route_ = null; }
                if (isset($context["query"])) { $_query_ = $context["query"]; } else { $_query_ = null; }
                if (isset($context["pageParameterName"])) { $_pageParameterName_ = $context["pageParameterName"]; } else { $_pageParameterName_ = null; }
                if (isset($context["next"])) { $_next_ = $context["next"]; } else { $_next_ = null; }
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($_route_, twig_array_merge($_query_, array($_pageParameterName_ => $_next_))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Next"), "html", null, true);
                echo "&nbsp;&raquo;</a>
        </li>
    ";
            } else {
                // line 71
                echo "        <li class=\"disabled\">
            <a href=\"#\" onClick=\"return false\">";
                // line 72
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Next"), "html", null, true);
                echo "&nbsp;&raquo;</a>
        </li>
    ";
            }
            // line 75
            echo "    </ul>

";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Paginacion:paginacion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  229 => 75,  223 => 72,  220 => 71,  208 => 68,  205 => 67,  203 => 66,  200 => 65,  187 => 62,  184 => 61,  181 => 60,  165 => 56,  157 => 49,  152 => 48,  143 => 46,  140 => 45,  126 => 40,  123 => 39,  110 => 36,  107 => 35,  102 => 34,  97 => 33,  94 => 32,  91 => 31,  87 => 29,  83 => 24,  74 => 21,  71 => 20,  68 => 19,  54 => 15,  51 => 14,  45 => 11,  42 => 10,  25 => 5,  24 => 4,  20 => 2,  60 => 17,  57 => 16,  38 => 9,  33 => 8,  29 => 6,  22 => 3,  19 => 2,  17 => 1,  207 => 85,  198 => 81,  186 => 71,  168 => 57,  161 => 54,  153 => 60,  150 => 59,  147 => 47,  141 => 56,  133 => 43,  130 => 54,  127 => 53,  122 => 50,  104 => 49,  96 => 44,  92 => 43,  88 => 42,  84 => 41,  80 => 40,  70 => 32,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 7,  27 => 6,);
    }
}
