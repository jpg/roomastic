<?php

/* HotelesBackendBundle:Privacidad:index.html.twig */
class __TwigTemplate_67470177bf00cf6888caf61cdf20fe25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "
<h1>Privacidad list</h1>

<table class=\"records_list\">
    <thead>
        <tr>
            <th>Id</th>
            <th>Texto</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
    ";
        // line 16
        if (isset($context["entities"])) { $_entities_ = $context["entities"]; } else { $_entities_ = null; }
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($_entities_);
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 17
            echo "        <tr>
            <td><a href=\"";
            // line 18
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_privacidad_show", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\">";
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "id"), "html", null, true);
            echo "</a></td>
            <td>";
            // line 19
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->getAttribute($_entity_, "texto"), "html", null, true);
            echo "</td>
            <td>
                <ul>
                    <li>
                        <a href=\"";
            // line 23
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_privacidad_show", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\">show</a>
                    </li>
                    <li>
                        <a href=\"";
            // line 26
            if (isset($context["entity"])) { $_entity_ = $context["entity"]; } else { $_entity_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_privacidad_edit", array("id" => $this->getAttribute($_entity_, "id"))), "html", null, true);
            echo "\">edit</a>
                    </li>
                </ul>
            </td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 32
        echo "    </tbody>
</table>

";
        // line 35
        if (isset($context["entities"])) { $_entities_ = $context["entities"]; } else { $_entities_ = null; }
        if ((twig_length_filter($this->env, $_entities_) == 0)) {
            // line 36
            echo "<ul>
    <li>
        <a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_privacidad_new"), "html", null, true);
            echo "\">
            Create a new entry
        </a>
    </li>
</ul>
";
        }
        // line 44
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Privacidad:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  306 => 128,  300 => 126,  288 => 122,  285 => 121,  236 => 75,  506 => 179,  478 => 163,  410 => 120,  380 => 115,  370 => 114,  366 => 113,  362 => 108,  348 => 104,  315 => 131,  310 => 97,  302 => 96,  289 => 94,  284 => 93,  272 => 90,  238 => 82,  1272 => 501,  1270 => 500,  1267 => 499,  1264 => 498,  1261 => 497,  1258 => 496,  1255 => 495,  1252 => 494,  1243 => 491,  1240 => 490,  1197 => 449,  1194 => 448,  1176 => 435,  1170 => 433,  1164 => 432,  1158 => 429,  1139 => 424,  1129 => 420,  1124 => 419,  1115 => 412,  1100 => 405,  1095 => 404,  1088 => 401,  1083 => 400,  1078 => 399,  1073 => 397,  1066 => 393,  1058 => 389,  1053 => 388,  1048 => 386,  1043 => 385,  1026 => 379,  1021 => 378,  1004 => 372,  999 => 371,  983 => 361,  970 => 351,  945 => 339,  940 => 338,  937 => 337,  913 => 322,  904 => 317,  893 => 314,  873 => 303,  860 => 295,  854 => 293,  843 => 286,  838 => 285,  833 => 283,  802 => 271,  794 => 270,  786 => 266,  781 => 265,  774 => 262,  769 => 261,  764 => 259,  756 => 258,  749 => 255,  744 => 254,  732 => 249,  722 => 246,  715 => 243,  710 => 242,  693 => 231,  685 => 227,  675 => 224,  670 => 223,  663 => 220,  648 => 216,  641 => 213,  636 => 212,  631 => 210,  626 => 209,  619 => 206,  599 => 197,  594 => 196,  572 => 188,  565 => 185,  547 => 178,  537 => 175,  530 => 172,  503 => 163,  490 => 158,  467 => 149,  455 => 145,  445 => 142,  431 => 125,  387 => 122,  343 => 103,  318 => 98,  296 => 91,  1374 => 615,  1371 => 614,  1367 => 616,  1365 => 614,  1339 => 590,  1335 => 588,  1321 => 587,  1315 => 585,  1309 => 584,  1302 => 582,  1298 => 581,  1294 => 579,  1290 => 577,  1287 => 576,  1281 => 575,  1263 => 574,  1260 => 573,  1256 => 571,  1253 => 570,  1249 => 493,  1246 => 492,  1230 => 565,  1225 => 564,  1222 => 563,  1213 => 556,  1210 => 555,  1207 => 554,  1160 => 506,  1157 => 505,  1144 => 426,  1137 => 617,  1135 => 423,  1119 => 491,  1117 => 489,  1114 => 488,  1111 => 487,  1109 => 486,  1105 => 406,  1091 => 461,  1080 => 455,  1068 => 394,  1063 => 451,  1051 => 448,  1044 => 446,  1039 => 445,  1032 => 443,  1027 => 442,  1020 => 440,  1015 => 439,  1008 => 437,  1003 => 436,  996 => 434,  991 => 433,  985 => 430,  954 => 426,  952 => 425,  941 => 419,  935 => 418,  927 => 415,  921 => 414,  918 => 413,  912 => 410,  903 => 408,  900 => 407,  898 => 406,  888 => 313,  883 => 400,  876 => 304,  871 => 397,  865 => 296,  852 => 390,  849 => 389,  847 => 388,  836 => 382,  831 => 381,  824 => 278,  819 => 277,  812 => 274,  807 => 273,  800 => 373,  788 => 370,  783 => 369,  759 => 364,  755 => 362,  753 => 361,  739 => 252,  735 => 353,  728 => 351,  713 => 347,  706 => 345,  691 => 341,  684 => 339,  680 => 226,  673 => 336,  669 => 335,  662 => 331,  654 => 329,  651 => 328,  649 => 327,  634 => 321,  622 => 314,  614 => 205,  610 => 310,  601 => 305,  575 => 287,  555 => 182,  543 => 273,  539 => 271,  523 => 264,  514 => 252,  510 => 239,  501 => 236,  496 => 234,  485 => 167,  472 => 151,  469 => 157,  451 => 214,  434 => 210,  429 => 208,  379 => 191,  374 => 189,  363 => 186,  357 => 182,  342 => 176,  317 => 132,  314 => 163,  267 => 157,  261 => 153,  155 => 50,  1182 => 438,  1180 => 444,  1177 => 443,  1174 => 442,  1171 => 441,  1168 => 440,  1165 => 439,  1162 => 438,  1159 => 437,  1156 => 436,  1153 => 428,  1150 => 489,  1147 => 427,  1134 => 423,  1130 => 422,  1121 => 416,  1110 => 409,  1102 => 403,  1092 => 397,  1087 => 396,  1082 => 395,  1075 => 454,  1070 => 391,  1065 => 390,  1060 => 388,  1056 => 449,  1054 => 385,  1046 => 381,  1041 => 380,  1036 => 382,  1031 => 381,  1024 => 374,  1019 => 373,  1014 => 375,  1009 => 374,  1002 => 367,  997 => 366,  992 => 364,  987 => 362,  975 => 354,  971 => 353,  958 => 341,  946 => 333,  933 => 331,  928 => 330,  925 => 329,  916 => 322,  895 => 319,  890 => 318,  878 => 309,  867 => 301,  862 => 300,  856 => 392,  845 => 291,  840 => 290,  835 => 288,  820 => 281,  808 => 277,  803 => 275,  795 => 372,  787 => 270,  782 => 269,  775 => 266,  770 => 265,  765 => 263,  757 => 262,  750 => 360,  745 => 258,  740 => 256,  729 => 253,  724 => 350,  712 => 247,  687 => 232,  682 => 231,  672 => 228,  665 => 225,  660 => 224,  655 => 222,  643 => 218,  638 => 217,  633 => 215,  621 => 211,  611 => 208,  603 => 204,  598 => 203,  586 => 198,  581 => 197,  576 => 195,  569 => 192,  559 => 189,  551 => 185,  546 => 184,  534 => 179,  529 => 268,  512 => 172,  489 => 163,  481 => 159,  471 => 156,  435 => 142,  399 => 118,  372 => 118,  338 => 107,  313 => 97,  308 => 95,  303 => 127,  298 => 95,  286 => 88,  269 => 83,  254 => 130,  247 => 76,  225 => 71,  215 => 66,  131 => 46,  173 => 59,  170 => 80,  200 => 70,  189 => 62,  93 => 29,  875 => 308,  825 => 282,  821 => 498,  817 => 497,  813 => 278,  798 => 485,  793 => 484,  767 => 463,  760 => 462,  754 => 461,  748 => 460,  742 => 459,  734 => 254,  727 => 248,  725 => 446,  707 => 245,  704 => 435,  701 => 434,  698 => 433,  668 => 409,  658 => 219,  647 => 404,  642 => 323,  606 => 376,  589 => 194,  577 => 190,  560 => 184,  524 => 176,  520 => 169,  515 => 315,  511 => 314,  499 => 175,  492 => 171,  462 => 148,  450 => 144,  444 => 212,  432 => 277,  421 => 133,  402 => 263,  398 => 262,  383 => 255,  377 => 119,  353 => 105,  347 => 179,  336 => 174,  328 => 104,  309 => 129,  291 => 123,  244 => 145,  119 => 60,  171 => 62,  116 => 67,  416 => 132,  408 => 180,  404 => 128,  401 => 198,  359 => 107,  346 => 138,  304 => 108,  297 => 125,  271 => 129,  251 => 100,  232 => 74,  195 => 69,  72 => 14,  149 => 53,  75 => 15,  522 => 222,  519 => 221,  507 => 170,  502 => 213,  497 => 210,  493 => 13,  484 => 205,  479 => 224,  475 => 201,  466 => 155,  457 => 218,  452 => 190,  448 => 191,  443 => 188,  440 => 141,  426 => 135,  413 => 201,  409 => 129,  392 => 167,  389 => 116,  373 => 162,  364 => 156,  352 => 139,  339 => 175,  334 => 136,  325 => 100,  320 => 99,  216 => 75,  159 => 95,  40 => 4,  721 => 339,  717 => 348,  714 => 440,  711 => 438,  708 => 334,  705 => 240,  688 => 320,  671 => 305,  653 => 217,  650 => 221,  645 => 300,  630 => 319,  616 => 210,  608 => 309,  593 => 303,  587 => 263,  582 => 191,  574 => 260,  562 => 282,  553 => 339,  542 => 177,  535 => 238,  532 => 269,  517 => 173,  508 => 165,  494 => 165,  488 => 206,  476 => 158,  470 => 291,  461 => 194,  454 => 151,  437 => 188,  433 => 182,  424 => 182,  418 => 174,  412 => 182,  406 => 200,  400 => 168,  394 => 125,  388 => 170,  382 => 121,  375 => 164,  367 => 119,  341 => 240,  333 => 105,  327 => 134,  282 => 120,  255 => 103,  250 => 74,  246 => 84,  213 => 78,  197 => 63,  105 => 33,  777 => 366,  747 => 354,  743 => 351,  702 => 344,  695 => 342,  689 => 313,  683 => 312,  677 => 229,  664 => 300,  661 => 303,  659 => 297,  656 => 296,  628 => 214,  618 => 283,  613 => 282,  609 => 203,  604 => 280,  600 => 279,  591 => 274,  567 => 284,  564 => 283,  545 => 269,  541 => 182,  538 => 267,  525 => 171,  518 => 253,  513 => 166,  509 => 249,  504 => 237,  500 => 14,  495 => 159,  491 => 241,  486 => 12,  482 => 210,  477 => 152,  473 => 233,  468 => 230,  464 => 229,  459 => 152,  456 => 285,  449 => 141,  441 => 211,  438 => 279,  428 => 212,  423 => 204,  420 => 186,  414 => 121,  411 => 133,  405 => 206,  393 => 201,  378 => 163,  344 => 187,  331 => 183,  324 => 180,  319 => 179,  295 => 160,  281 => 87,  276 => 88,  177 => 63,  277 => 159,  273 => 125,  264 => 110,  260 => 132,  257 => 131,  231 => 86,  168 => 59,  396 => 195,  391 => 136,  386 => 192,  381 => 134,  376 => 133,  371 => 251,  365 => 115,  360 => 114,  355 => 112,  350 => 111,  345 => 112,  340 => 137,  335 => 102,  330 => 101,  323 => 105,  316 => 116,  312 => 130,  305 => 219,  301 => 111,  294 => 124,  283 => 104,  279 => 119,  275 => 92,  268 => 111,  265 => 181,  262 => 133,  245 => 127,  234 => 81,  230 => 80,  205 => 133,  190 => 70,  187 => 132,  184 => 61,  169 => 58,  139 => 45,  81 => 34,  293 => 128,  290 => 163,  274 => 84,  270 => 104,  252 => 78,  248 => 128,  241 => 100,  237 => 89,  233 => 87,  226 => 79,  223 => 135,  220 => 76,  210 => 75,  188 => 74,  181 => 97,  145 => 47,  137 => 51,  128 => 64,  120 => 33,  41 => 8,  142 => 63,  129 => 67,  125 => 48,  221 => 143,  203 => 62,  198 => 81,  185 => 69,  179 => 60,  163 => 57,  157 => 88,  152 => 50,  133 => 50,  126 => 55,  110 => 35,  76 => 23,  70 => 27,  222 => 78,  207 => 85,  204 => 95,  183 => 65,  167 => 77,  164 => 52,  148 => 56,  141 => 52,  103 => 45,  98 => 44,  59 => 19,  49 => 12,  53 => 15,  21 => 3,  100 => 31,  97 => 30,  18 => 1,  151 => 49,  135 => 44,  114 => 36,  206 => 71,  201 => 73,  194 => 67,  191 => 85,  176 => 61,  166 => 57,  158 => 71,  153 => 70,  143 => 45,  134 => 44,  123 => 39,  118 => 38,  90 => 31,  87 => 32,  66 => 20,  122 => 39,  107 => 39,  101 => 32,  95 => 36,  82 => 36,  67 => 23,  52 => 15,  45 => 8,  36 => 5,  34 => 7,  266 => 90,  263 => 101,  259 => 80,  256 => 93,  242 => 126,  229 => 80,  227 => 84,  218 => 77,  209 => 133,  192 => 68,  186 => 71,  180 => 76,  174 => 59,  162 => 96,  160 => 45,  146 => 56,  140 => 45,  136 => 47,  106 => 33,  73 => 20,  69 => 18,  22 => 3,  60 => 19,  55 => 16,  102 => 36,  89 => 39,  63 => 23,  56 => 16,  50 => 9,  43 => 16,  92 => 35,  79 => 21,  57 => 22,  37 => 3,  33 => 8,  29 => 4,  19 => 2,  47 => 12,  30 => 4,  27 => 3,  249 => 118,  239 => 90,  235 => 88,  228 => 121,  224 => 77,  219 => 80,  217 => 75,  214 => 67,  211 => 66,  208 => 96,  202 => 64,  199 => 61,  193 => 59,  182 => 83,  178 => 100,  175 => 81,  172 => 85,  165 => 55,  161 => 60,  156 => 59,  154 => 42,  150 => 71,  147 => 70,  132 => 57,  127 => 41,  113 => 41,  86 => 30,  83 => 22,  78 => 21,  64 => 16,  61 => 17,  48 => 17,  32 => 16,  24 => 4,  117 => 35,  112 => 34,  109 => 34,  104 => 49,  96 => 34,  84 => 22,  80 => 22,  68 => 18,  46 => 15,  44 => 9,  26 => 3,  23 => 3,  39 => 21,  25 => 4,  20 => 2,  17 => 1,  144 => 47,  138 => 42,  130 => 47,  124 => 49,  121 => 47,  115 => 31,  111 => 35,  108 => 44,  99 => 38,  94 => 29,  91 => 33,  88 => 27,  85 => 23,  77 => 23,  74 => 26,  71 => 21,  65 => 27,  62 => 18,  58 => 18,  54 => 19,  51 => 18,  42 => 7,  38 => 11,  35 => 5,  31 => 4,  28 => 3,);
    }
}
