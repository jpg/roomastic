<?php

/* HotelesFrontendBundle:Frontend:confirmacion-compra.html.twig */
class __TwigTemplate_47cb6f2d05b78511eb5d534a513be9bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"contenido confirmaciontpv\">

\t<h2>";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Confirmación de compra"), "html", null, true);
        echo "</h2>
\t<p>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("A continuación te presentamos un resumen de la reserva que acabas de realizar."), "html", null, true);
        echo "</p>
\t<p>";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si tienes alguna duda, puedes ponerte en contacto con nosotros."), "html", null, true);
        echo "</p>
\t<div class=\"separador\"></div>
\t<!-- sms_ko -->
\t<div class=\"tpv_ko\" style=\"display:none;\">
\t\t<p><span>Ha habido algún problema</span></p>
\t\t<p>Tu pago no ha podido realizarse correctamente. Inténtalo de nuevo pasados unos minutos o con otra tarjeta. Si el problema persiste, llámanos.</p>
\t</div>
\t<!-- /sms_ko -->
\t<!-- sms_ko -->
\t<div class=\"tpv_ok\">
\t\t<div class=\"txt1 left\">
\t\t\t<p><span>¡Enhorabuena!</span></p>
\t\t\t<p>Tu compra se ha realizado correctamente.</p>\t
\t\t</div>
\t\t<div class=\"txt2 left\">
\t\t\t<p>Te acabamos de enviar un email a tu correo con la confirmación de tu reserva. Te recomendamos imprimir esta página como resguardo de tu compra.</p>
\t\t</div>
\t\t<a href=\"#\" class=\"left\" ><img src=\"/bundles/hotelesfrontend/img/icon_print.png\" alt=\"\"></a>

\t\t
\t\t<div class=\"clearfix\"></div>
\t</div>
\t<!-- /sms_ko -->
\t<div class=\"separador\"></div>

\t<div class=\"bq\">
\t\t<div class=\"tit\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("El hotel"), "html", null, true);
        echo "</div>
\t\t<div class=\"cont clearfix\">
\t\t\t<p class=\"name\">Hotel Diamante Suites Spa</p>
\t\t\t<p>C/ Antonio Ruiz Álvarez 7 38400 Puerto de la Cruz. Tenerife.</p>
\t\t\t<div class=\"estrellas\">
\t\t\t\t<img src=\"/bundles/hotelesfrontend/img/5stars_print.png\">
\t\t\t\t<!-- NOTA!! TENER EN CUENTA QUE LAS IMG \"STARTS\" DEBEN IR EN ESTA PARTE SEGUIDAS DE \"_print\" para LA VERSIÓN IMPRESA   -->
\t\t\t</div>
\t\t\t<div class=\"left\">
\t\t\t\t<img src=\"/bundles/hotelesfrontend/img/img_pruebaConfTpv.png\" class=\"imgppal\">
\t\t\t</div>
\t\t\t<div class=\"right\">
\t\t\t\t<ul class=\"servicios\">
                    <li class=\"piscina\">
                        <label for=\"piscina\">";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Piscina"), "html", null, true);
        echo "</label>
                    </li>
                    <li class=\"spa\">
                        <label for=\"Spa\">";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Spa"), "html", null, true);
        echo "</label>
                    </li>
                    <li class=\"wi-fi\">
                        <label for=\"Bar\">";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wi-Fi"), "html", null, true);
        echo "</label>
                    </li>\t
                    <li class=\"acceso-adaptado\">
                        <label for=\"Acceso-Adaptado\">";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso adaptado"), "html", null, true);
        echo "</label>
                    </li>
                    <li class=\"perros\">
                        <label for=\"perros\">";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptan perros"), "html", null, true);
        echo "</label>
                    </li>
                    <li class=\"parking\">
                        <label for=\"parking\">";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aparcamiento/Parking"), "html", null, true);
        echo "</label>
                    </li>
                    <li class=\"business-center\">
                        <label for=\"business-center\">";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Business center"), "html", null, true);
        echo "</label>
                    </li>
                </ul>
\t\t\t\t
\t\t\t</div>
\t\t</div>

\t</div>
\t<div class=\"bq\">
\t\t<div class=\"tit\">";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la reserva"), "html", null, true);
        echo "</div>
\t\t<div class=\"cont\">
\t\t\t<ul class=\"datos\">
\t\t\t\t<li class=\"left\">
\t\t\t\t\t<p>";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha de entrada"), "html", null, true);
        echo "</p>
\t\t\t\t\t<p><span>26 de Septiembre de 2014</span></p>
\t\t\t\t</li>
\t\t\t\t<li class=\"left\">
\t\t\t\t\t<p>";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de adultos"), "html", null, true);
        echo "</p>
\t\t\t\t\t<p><span>2</span></p>
\t\t\t\t</li>
\t\t\t\t<li class=\"left\">
\t\t\t\t\t<p>";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de noches"), "html", null, true);
        echo "</p>
\t\t\t\t\t<p><span>2</span></p>
\t\t\t\t</li>
\t\t\t\t<li class=\"left\">
\t\t\t\t\t<p>";
        // line 92
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de niños"), "html", null, true);
        echo "</p>
\t\t\t\t\t<p><span>0</span></p>
\t\t\t\t</li>
\t\t\t\t<li class=\"left\">
\t\t\t\t\t<p>";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre y apellidos"), "html", null, true);
        echo "</p>
\t\t\t\t\t<p><span>Alejandro López Sebastián</span></p>
\t\t\t\t</li>
\t\t\t\t<li class=\"left\">
\t\t\t\t\t<p>";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Código de tu operación"), "html", null, true);
        echo "</p>
\t\t\t\t\t<p><span>7845AH7L0AT8G</span></p>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<p>";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("DNI"), "html", null, true);
        echo "</p>
\t\t\t\t\t<p><span>578415874D</span></p>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\t<p>";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Importe total de la reserva"), "html", null, true);
        echo "</p>
\t\t\t\t\t<p><span>156 €</span></p>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</div>

\t</div>
\t<div class=\"clearfix\"></div>
\t<div class=\"separador\"></div>





</div><!-- contenido -->

";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:confirmacion-compra.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 96,  116 => 25,  416 => 184,  408 => 180,  404 => 178,  401 => 177,  359 => 140,  346 => 138,  304 => 108,  297 => 107,  271 => 105,  251 => 102,  232 => 98,  195 => 77,  72 => 20,  149 => 56,  75 => 26,  522 => 222,  519 => 221,  507 => 216,  502 => 213,  497 => 210,  493 => 13,  484 => 205,  479 => 202,  475 => 201,  466 => 197,  457 => 193,  452 => 190,  448 => 191,  443 => 188,  440 => 185,  426 => 177,  413 => 173,  409 => 171,  392 => 167,  389 => 166,  373 => 162,  364 => 156,  352 => 139,  339 => 143,  334 => 136,  325 => 141,  320 => 140,  216 => 65,  159 => 26,  40 => 10,  721 => 339,  717 => 337,  714 => 336,  711 => 335,  708 => 334,  705 => 333,  688 => 320,  671 => 305,  653 => 302,  650 => 301,  645 => 300,  630 => 289,  616 => 279,  608 => 275,  593 => 265,  587 => 263,  582 => 262,  574 => 260,  562 => 252,  553 => 250,  542 => 243,  535 => 238,  532 => 237,  517 => 226,  508 => 220,  494 => 214,  488 => 206,  476 => 208,  470 => 198,  461 => 194,  454 => 201,  437 => 188,  433 => 182,  424 => 182,  418 => 174,  412 => 182,  406 => 176,  400 => 168,  394 => 172,  388 => 170,  382 => 168,  375 => 164,  367 => 162,  341 => 142,  333 => 137,  327 => 134,  282 => 93,  255 => 103,  250 => 74,  246 => 73,  213 => 51,  197 => 48,  105 => 33,  777 => 356,  747 => 354,  743 => 351,  702 => 315,  695 => 314,  689 => 313,  683 => 312,  677 => 311,  664 => 300,  661 => 303,  659 => 297,  656 => 296,  628 => 289,  618 => 283,  613 => 282,  609 => 281,  604 => 280,  600 => 279,  591 => 274,  567 => 271,  564 => 270,  545 => 269,  541 => 268,  538 => 267,  525 => 224,  518 => 253,  513 => 250,  509 => 249,  504 => 246,  500 => 14,  495 => 242,  491 => 241,  486 => 12,  482 => 210,  477 => 234,  473 => 233,  468 => 230,  464 => 229,  459 => 226,  456 => 225,  449 => 222,  441 => 216,  438 => 215,  428 => 212,  423 => 211,  420 => 186,  414 => 209,  411 => 208,  405 => 206,  393 => 201,  378 => 163,  344 => 187,  331 => 183,  324 => 180,  319 => 179,  295 => 164,  281 => 162,  276 => 161,  177 => 70,  277 => 92,  273 => 125,  264 => 120,  260 => 78,  257 => 117,  231 => 93,  168 => 14,  396 => 137,  391 => 136,  386 => 135,  381 => 134,  376 => 133,  371 => 132,  365 => 130,  360 => 196,  355 => 128,  350 => 127,  345 => 143,  340 => 137,  335 => 184,  330 => 123,  323 => 119,  316 => 116,  312 => 115,  305 => 167,  301 => 111,  294 => 108,  283 => 104,  279 => 103,  275 => 106,  268 => 122,  265 => 96,  262 => 95,  245 => 73,  234 => 85,  230 => 84,  205 => 69,  190 => 34,  187 => 45,  184 => 61,  169 => 38,  139 => 40,  81 => 47,  293 => 128,  290 => 163,  274 => 105,  270 => 104,  252 => 92,  248 => 96,  241 => 100,  237 => 99,  233 => 90,  226 => 56,  223 => 80,  220 => 84,  210 => 37,  188 => 74,  181 => 69,  145 => 25,  137 => 26,  128 => 21,  120 => 40,  41 => 9,  142 => 24,  129 => 35,  125 => 41,  221 => 66,  203 => 79,  198 => 60,  185 => 104,  179 => 69,  163 => 27,  157 => 88,  152 => 60,  133 => 22,  126 => 45,  110 => 22,  76 => 21,  70 => 40,  222 => 85,  207 => 80,  204 => 80,  183 => 73,  167 => 70,  164 => 92,  148 => 46,  141 => 61,  103 => 34,  98 => 28,  59 => 18,  49 => 11,  53 => 13,  21 => 2,  100 => 55,  97 => 32,  18 => 1,  151 => 53,  135 => 41,  114 => 33,  206 => 77,  201 => 75,  194 => 35,  191 => 70,  176 => 61,  166 => 59,  158 => 53,  153 => 79,  143 => 80,  134 => 37,  123 => 80,  118 => 64,  90 => 29,  87 => 23,  66 => 39,  122 => 74,  107 => 19,  101 => 72,  95 => 48,  82 => 25,  67 => 20,  52 => 8,  45 => 11,  36 => 9,  34 => 7,  266 => 117,  263 => 101,  259 => 100,  256 => 93,  242 => 13,  229 => 170,  227 => 84,  218 => 53,  209 => 78,  192 => 108,  186 => 75,  180 => 73,  174 => 95,  162 => 62,  160 => 31,  146 => 50,  140 => 92,  136 => 76,  106 => 58,  73 => 11,  69 => 22,  22 => 6,  60 => 14,  55 => 13,  102 => 32,  89 => 63,  63 => 19,  56 => 16,  50 => 13,  43 => 11,  92 => 28,  79 => 30,  57 => 9,  37 => 7,  33 => 12,  29 => 4,  19 => 1,  47 => 10,  30 => 3,  27 => 2,  249 => 14,  239 => 90,  235 => 12,  228 => 92,  224 => 90,  219 => 84,  217 => 71,  214 => 70,  211 => 64,  208 => 49,  202 => 68,  199 => 78,  193 => 64,  182 => 87,  178 => 100,  175 => 23,  172 => 85,  165 => 55,  161 => 13,  156 => 52,  154 => 12,  150 => 84,  147 => 44,  132 => 36,  127 => 26,  113 => 50,  86 => 24,  83 => 22,  78 => 54,  64 => 18,  61 => 23,  48 => 11,  32 => 5,  24 => 3,  117 => 41,  112 => 61,  109 => 24,  104 => 27,  96 => 29,  84 => 26,  80 => 22,  68 => 19,  46 => 12,  44 => 15,  26 => 3,  23 => 4,  39 => 8,  25 => 3,  20 => 2,  17 => 1,  144 => 53,  138 => 42,  130 => 35,  124 => 67,  121 => 42,  115 => 33,  111 => 52,  108 => 37,  99 => 33,  94 => 52,  91 => 29,  88 => 49,  85 => 24,  77 => 20,  74 => 16,  71 => 35,  65 => 18,  62 => 19,  58 => 37,  54 => 36,  51 => 11,  42 => 9,  38 => 8,  35 => 8,  31 => 3,  28 => 2,);
    }
}
