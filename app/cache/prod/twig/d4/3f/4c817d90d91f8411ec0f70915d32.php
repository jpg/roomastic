<?php

/* HotelesBackendBundle:Contacto:edit.html.twig */
class __TwigTemplate_d43f4c817d90d91f8411ec0f70915d32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar contacto"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">
            <section class=\"panel\"> 
                <header class=\"panel-heading\">
                    ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar contacto"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <form role=\"form\" action=\"";
        // line 17
        if (isset($context["contacto"])) { $_contacto_ = $context["contacto"]; } else { $_contacto_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb_update", array("id" => $this->getAttribute($_contacto_, "id"))), "html", null, true);
        echo "\" method=\"post\" ";
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_edit_form_);
        echo ">
                        ";
        // line 19
        echo "                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 22
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "nombre"));
        echo "
                                ";
        // line 23
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "nombre"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 29
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "apellidos"));
        echo "
                                ";
        // line 30
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "apellidos"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 36
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "email"));
        echo "
                                ";
        // line 37
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Texto"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 43
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "texto"));
        echo "
                                ";
        // line 44
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "texto"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 50
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "fecha"));
        echo "
                                ";
        // line 51
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "fecha"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>                        
                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Atendido"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 57
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($_edit_form_, "atendido"));
        echo "
                                ";
        // line 58
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($_edit_form_, "atendido"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        ";
        // line 61
        if (isset($context["edit_form"])) { $_edit_form_ = $context["edit_form"]; } else { $_edit_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_edit_form_);
        echo "
                </div>
            </section>

            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-info\">";
        // line 68
        echo "Guardar y marcar como atendido";
        echo "</button>   
                    </form><!-- / form edit -->
                    <a href=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
               <form action=\"";
        // line 71
        if (isset($context["contacto"])) { $_contacto_ = $context["contacto"]; } else { $_contacto_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb_delete", array("id" => $this->getAttribute($_contacto_, "id"))), "html", null, true);
        echo "\" method=\"post\" class=\"btnDelete\">
                        ";
        // line 72
        if (isset($context["delete_form"])) { $_delete_form_ = $context["delete_form"]; } else { $_delete_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($_delete_form_);
        echo "
                        <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Está seguro de borrar?"), "html", null, true);
        echo "');\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Borrar"), "html", null, true);
        echo "</button>
                    </form>
                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->



    ";
        // line 109
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Contacto:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 109,  207 => 73,  202 => 72,  197 => 71,  191 => 70,  186 => 68,  175 => 61,  168 => 58,  163 => 57,  158 => 55,  150 => 51,  145 => 50,  140 => 48,  132 => 44,  127 => 43,  122 => 41,  114 => 37,  109 => 36,  104 => 34,  96 => 30,  91 => 29,  86 => 27,  78 => 23,  73 => 22,  68 => 20,  65 => 19,  57 => 17,  51 => 14,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
