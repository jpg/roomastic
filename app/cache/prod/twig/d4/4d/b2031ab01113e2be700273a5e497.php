<?php

/* HotelesFrontendBundle:Frontend:condicionesLegales.mv.twig */
class __TwigTemplate_d44db2031ab01113e2be700273a5e497 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
    <div class=\"contenido\">

        <h2>";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aviso legal y condiciones del servicio"), "html", null, true);
        echo "</h2>
        <div class=\"txtBack\">
            ";
        // line 8
        if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
        if ($this->getAttribute($_content_, "texto", array(), "any", true, true)) {
            // line 9
            echo "                ";
            if (isset($context["content"])) { $_content_ = $context["content"]; } else { $_content_ = null; }
            echo $this->getAttribute($_content_, "texto");
            echo "
            ";
        }
        // line 11
        echo "        </div>

    </div><!-- contenido -->
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:condicionesLegales.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 11,  42 => 9,  39 => 8,  34 => 6,  29 => 3,  26 => 2,);
    }
}
