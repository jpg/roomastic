<?php

/* HotelesBackendBundle:Extras:hotelStatus.html.twig */
class __TwigTemplate_61038b745a214fc5b7d2a4962b9bd0fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 7
        if (isset($context["status"])) { $_status_ = $context["status"]; } else { $_status_ = null; }
        if (($_status_ == 0)) {
            // line 8
            echo "    <span class=\"fa-lg\">
        <i class=\"fa fa-2x fa-eye-slash\"></i>
    </span>
";
        } elseif (($_status_ == 1)) {
            // line 12
            echo "    <span class=\"fa-lg\">
        <i class=\"fa fa-check\"></i>
    </span>
";
        } elseif (($_status_ == 2)) {
            // line 16
            echo "    <span class=\"fa-stack fa-lg\">
        <i class=\"fa fa-user fa-stack-1x\"></i>
        <i class=\"fa fa-ban fa-stack-2x text-danger\"></i>
    </span>
";
        } else {
            // line 21
            echo "    <span class=\"fa-lg\">
        <i class=\"fa fa-question\"></i>
    </span>
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:hotelStatus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 21,  32 => 16,  26 => 12,  20 => 8,  17 => 7,  229 => 80,  224 => 77,  216 => 75,  214 => 74,  199 => 61,  183 => 58,  174 => 56,  171 => 55,  167 => 53,  164 => 52,  155 => 50,  147 => 49,  142 => 48,  137 => 47,  129 => 46,  126 => 45,  108 => 44,  101 => 40,  97 => 39,  93 => 38,  89 => 37,  85 => 36,  81 => 35,  77 => 34,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
