<?php

/* FOSUserBundle:Resetting:reset_content.html.twig */
class __TwigTemplate_6134aedda19e5de927f99cfdb27cfd98 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        if (isset($context["theme"])) { $_theme_ = $context["theme"]; } else { $_theme_ = null; }
        echo $this->env->getExtension('form')->setTheme($_form_, array($_theme_, ));
        // line 2
        echo "<form action=\"";
        if (isset($context["token"])) { $_token_ = $context["token"]; } else { $_token_ = null; }
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_reset", array("token" => $_token_)), "html", null, true);
        echo "\" ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderEnctype($_form_);
        echo " method=\"POST\" class=\"fos_user_resetting_reset form-signin\">
    ";
        // line 4
        echo "    ";
        if (array_key_exists("invalid_username", $context)) {
            // line 5
            echo "        <div class=\"alert alert-danger fade in alert-width\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
            <strong>Error</strong><br> ";
            // line 7
            if (isset($context["invalid_username"])) { $_invalid_username_ = $context["invalid_username"]; } else { $_invalid_username_ = null; }
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Credenciales no válidas", array("%username%" => $_invalid_username_)), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 10
        echo "    <h2 class=\"form-signin-heading no-icon\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Introduce tu contraseña"), "html", null, true);
        echo "</h2>
    <div class=\"login-wrap\">
        <p>";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Introduce tu contraseña: (que sea facil de recordar)"), "html", null, true);
        echo "</p>
        ";
        // line 14
        echo "        ";
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($_form_, "new"), "first"), array("attr" => array("class" => "form-control")));
        echo "            
        <p>";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Repite tu contraseña:"), "html", null, true);
        echo "</p>
        ";
        // line 16
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($_form_, "new"), "second"), array("attr" => array("class" => "form-control")));
        echo "         
        ";
        // line 17
        if (isset($context["form"])) { $_form_ = $context["form"]; } else { $_form_ = null; }
        echo $this->env->getExtension('form')->renderRest($_form_);
        echo "
        <button class=\"btn btn-lg btn-login btn-block\" type=\"submit\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.reset.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar"), "html", null, true);
        echo "</button>
    </div>    
</form>


";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 18,  69 => 17,  64 => 16,  60 => 15,  54 => 14,  50 => 12,  44 => 10,  37 => 7,  33 => 5,  30 => 4,  21 => 2,  17 => 1,  29 => 4,  26 => 3,);
    }
}
