<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;


/**
 * appprodUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appprodUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    static private $declaredRouteNames = array(
       'TooltypAuthGoogleBundle_login' => true,
       'TooltypAuthTwitterBundle_homepage' => true,
       'TooltypAuthFaceBundle_login' => true,
       'hoteles_backend_export_exportnewsletter' => true,
       'admin_imageupload' => true,
       'admin_imagecrop' => true,
       'hoteles_backend_imageupload_deletetempfile' => true,
       'admin_configuracion' => true,
       'admin_configuracion_show' => true,
       'admin_configuracion_new' => true,
       'admin_configuracion_create' => true,
       'admin_configuracion_edit' => true,
       'admin_configuracion_update' => true,
       'admin_configuracion_delete' => true,
       'admin_lugares' => true,
       'admin_lugaresfiltramunicipio' => true,
       'admin_lugaresfiltralugares' => true,
       'admin_lugares_show' => true,
       'admin_lugares_new' => true,
       'admin_lugares_create' => true,
       'admin_lugares_edit' => true,
       'admin_lugares_update' => true,
       'admin_lugares_delete' => true,
       'admin_contactoweb' => true,
       'admin_contactoweb_show' => true,
       'admin_contactoweb_new' => true,
       'admin_contactoweb_create' => true,
       'admin_contactoweb_edit' => true,
       'admin_contactoweb_update' => true,
       'admin_contactoweb_delete' => true,
       'admin_usuario' => true,
       'admin_usuario_show' => true,
       'admin_usuario_new' => true,
       'admin_usuario_create' => true,
       'admin_usuario_edit' => true,
       'admin_usuario_update' => true,
       'admin_usuario_delete' => true,
       'admin_usuario_deleteget' => true,
       'admin_usuario_enabled' => true,
       'admin_usuario_disabled' => true,
       'admin_usuario_ban' => true,
       'admin_usuario_unban' => true,
       'admin_hotel' => true,
       'admin_hotel_show' => true,
       'admin_hotel_new' => true,
       'admin_hotel_create' => true,
       'admin_hotel_edit' => true,
       'admin_hotel_update' => true,
       'admin_hotel_delete' => true,
       'admin_hotel_reactivate' => true,
       'admin_hotel_check_slug' => true,
       'admin_userempresa' => true,
       'admin_userempresa_show' => true,
       'admin_userempresa_new' => true,
       'admin_userempresa_create' => true,
       'admin_userempresa_edit' => true,
       'admin_userempresa_update' => true,
       'admin_userempresa_delete' => true,
       'admin_userempresa_reactivate' => true,
       'admin_useradministrador' => true,
       'admin_useradministrador_show' => true,
       'admin_useradministrador_new' => true,
       'admin_useradministrador_create' => true,
       'admin_useradministrador_edit' => true,
       'admin_useradministrador_update' => true,
       'admin_useradministrador_delete' => true,
       'admin_userhotel' => true,
       'admin_userhotel_show' => true,
       'admin_userhotel_new' => true,
       'admin_userhotel_create' => true,
       'admin_userhotel_edit' => true,
       'admin_userhotel_update' => true,
       'admin_userhotel_delete' => true,
       'admin_userhotel_reactivate' => true,
       'admin_ofertas' => true,
       'admin_ofertas_filtradas' => true,
       'admin_ofertas_show' => true,
       'admin_ofertas_new' => true,
       'admin_ofertas_create' => true,
       'admin_ofertas_edit' => true,
       'admin_ofertas_update' => true,
       'admin_ofertas_delete' => true,
       'acepta_oferta' => true,
       'hoteles_backend_oferta_aceptarofertadesdeemail' => true,
       'hoteles_backend_oferta_rechazarofertadesdeemail' => true,
       'declina_oferta' => true,
       'admin_ofertas_contraoferta' => true,
       'hoteles_backend_oferta_ofertadetails' => true,
       'hoteles_backend_crontask_fixture' => true,
       'admin_homeconfiguracion' => true,
       'admin_ordenahome' => true,
       'admin_homeconfiguracion_show' => true,
       'admin_homeconfiguracion_new' => true,
       'admin_homeconfiguracion_create' => true,
       'admin_homeconfiguracion_edit' => true,
       'admin_homeconfiguracion_update' => true,
       'admin_homeconfiguracion_delete' => true,
       'admin_homeconfiguracion_deleteget' => true,
       'admin_publicatuhotel' => true,
       'admin_publicatuhotel_show' => true,
       'admin_publicatuhotel_new' => true,
       'admin_publicatuhotel_create' => true,
       'admin_publicatuhotel_edit' => true,
       'admin_publicatuhotel_update' => true,
       'admin_publicatuhotel_delete' => true,
       'admin_condicioneslegales' => true,
       'admin_condicioneslegales_show' => true,
       'admin_condicioneslegales_new' => true,
       'admin_condicioneslegales_create' => true,
       'admin_condicioneslegales_edit' => true,
       'admin_condicioneslegales_update' => true,
       'admin_condicioneslegales_delete' => true,
       'admin_quienessomos' => true,
       'admin_quienessomos_show' => true,
       'admin_quienessomos_new' => true,
       'admin_quienessomos_create' => true,
       'admin_quienessomos_edit' => true,
       'admin_quienessomos_update' => true,
       'admin_quienessomos_delete' => true,
       'admin_incidencia' => true,
       'admin_incidencia_show' => true,
       'admin_incidencia_new' => true,
       'admin_incidencia_create' => true,
       'admin_incidencia_edit' => true,
       'admin_incidencia_update' => true,
       'admin_incidencia_delete' => true,
       'admin_incidencia_atender' => true,
       'admin_faqs' => true,
       'admin_ordena' => true,
       'admin_faqs_show' => true,
       'admin_faqs_new' => true,
       'admin_faqs_create' => true,
       'admin_faqs_edit' => true,
       'admin_faqs_update' => true,
       'admin_faqs_delete' => true,
       'admin_landing' => true,
       'admin_empresa' => true,
       'admin_empresa_show' => true,
       'admin_empresa_new' => true,
       'admin_empresa_create' => true,
       'admin_empresa_edit' => true,
       'admin_empresa_update' => true,
       'admin_empresa_delete' => true,
       'admin_empresa_reactivate' => true,
       'admin_seo' => true,
       'admin_seo_show' => true,
       'admin_seo_new' => true,
       'admin_seo_create' => true,
       'admin_seo_edit' => true,
       'admin_seo_update' => true,
       'admin_seo_delete' => true,
       'hoteles_backend_notificacion_updatecounters' => true,
       'admin_cookies' => true,
       'admin_cookies_show' => true,
       'admin_cookies_new' => true,
       'admin_cookies_create' => true,
       'admin_cookies_edit' => true,
       'admin_cookies_update' => true,
       'admin_cookies_delete' => true,
       'admin_imagenhotel' => true,
       'admin_imagenhotel_show' => true,
       'admin_imagenhotel_new' => true,
       'admin_imagenhotel_create' => true,
       'admin_imagenhotel_edit' => true,
       'admin_imagenhotel_update' => true,
       'admin_imagenhotel_delete' => true,
       'admin_privacidad' => true,
       'admin_privacidad_show' => true,
       'admin_privacidad_new' => true,
       'admin_privacidad_create' => true,
       'admin_privacidad_edit' => true,
       'admin_privacidad_update' => true,
       'admin_privacidad_delete' => true,
       'admin_newsletter' => true,
       'admin_newsletter_show' => true,
       'admin_newsletter_new' => true,
       'admin_newsletter_create' => true,
       'admin_newsletter_edit' => true,
       'admin_newsletter_update' => true,
       'admin_newsletter_delete' => true,
       'admin_newsletter_activate' => true,
       'hoteles_backend_passwordreset_afterreset' => true,
       'HotelesFrontendBundle_homepage' => true,
       'HotelesFrontendBundle_compruebalogin' => true,
       'HotelesFrontendBundle_getUserData' => true,
       'HotelesFrontendBundle_logout' => true,
       'HotelesFrontendBundle_compruebaloginuserrss' => true,
       'HotelesFrontendBundle_listadoprovincia' => true,
       'HotelesFrontendBundle_listadoprovinciamunicipio' => true,
       'HotelesFrontendBundle_listadoprovinciamunicipiolugar' => true,
       'HotelesFrontendBundle_listadoprovinciaajax' => true,
       'HotelesFrontendBundle_listadoprovinciamunicipioajax' => true,
       'HotelesFrontendBundle_listadoprovinciamunicipiolugarajax' => true,
       'HotelesFrontendBundle_detallehotel' => true,
       'HotelesFrontendBundle_oferta' => true,
       'HotelesFrontendBundle_registroajax' => true,
       'HotelesFrontendBundle_registroextraajax' => true,
       'HotelesFrontendBundle_registronewsletterajax' => true,
       'HotelesFrontendBundle_ofertarealizada' => true,
       'HotelesFrontendBundle_usuariobaneado' => true,
       'HotelesFrontendBundle_quienesSomos' => true,
       'HotelesFrontendBundle_condicionesLegales' => true,
       'HotelesFrontendBundle_privacidad' => true,
       'HotelesFrontendBundle_cookies' => true,
       'HotelesFrontendBundle_faqs' => true,
       'HotelesFrontendBundle_publicaTuHotel' => true,
       'HotelesFrontendBundle_contacto' => true,
       'HotelesFrontendBundle_mapaweb' => true,
       'hoteles_frontend_tpv_done' => true,
       'hoteles_frontend_tpv_processpayment' => true,
       'hoteles_frontend_tpv_success' => true,
       'hoteles_frontend_tpv_fail' => true,
       'hoteles_frontend_tpv_pay' => true,
       'prueba_de_render' => true,
       'fos_user_security_login' => true,
       'fos_user_security_check' => true,
       'fos_user_security_logout' => true,
       'fos_user_profile_show' => true,
       'fos_user_profile_edit' => true,
       'fos_user_resetting_request' => true,
       'fos_user_resetting_send_email' => true,
       'fos_user_resetting_check_email' => true,
       'fos_user_resetting_reset' => true,
       'fos_user_registration_register' => true,
       'fos_user_registration_check_email' => true,
       'fos_user_registration_confirm' => true,
       'fos_user_registration_confirmed' => true,
       'fos_js_routing_js' => true,
       'remove_trailing_slash' => true,
    );

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function generate($name, $parameters = array(), $absolute = false)
    {
        if (!isset(self::$declaredRouteNames[$name])) {
            throw new RouteNotFoundException(sprintf('Route "%s" does not exist.', $name));
        }

        $escapedName = str_replace('.', '__', $name);

        list($variables, $defaults, $requirements, $tokens) = $this->{'get'.$escapedName.'RouteInfo'}();

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $absolute);
    }

    private function getTooltypAuthGoogleBundle_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Tooltyp\\AuthGoogleBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login/google',  ),));
    }

    private function getTooltypAuthTwitterBundle_homepageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Tooltyp\\AuthTwitterBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login/twitter',  ),));
    }

    private function getTooltypAuthFaceBundle_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Tooltyp\\AuthFaceBundle\\Controller\\DefaultController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login/facebook',  ),));
    }

    private function gethoteles_backend_export_exportnewsletterRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ExportController::exportNewsletter',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/export/newsletter',  ),));
    }

    private function getadmin_imageuploadRouteInfo()
    {
        return array(array (  0 => 'tiposubida',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImageuploadController::indexAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'tiposubida',  ),  1 =>   array (    0 => 'text',    1 => '/admin/imageupload',  ),));
    }

    private function getadmin_imagecropRouteInfo()
    {
        return array(array (  0 => 'imagen',  1 => 'posx',  2 => 'posy',  3 => 'width',  4 => 'height',  5 => 'carpeta',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImageuploadController::cropAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'carpeta',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'height',  ),  2 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'width',  ),  3 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'posy',  ),  4 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'posx',  ),  5 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'imagen',  ),  6 =>   array (    0 => 'text',    1 => '/admin/cropimagen',  ),));
    }

    private function gethoteles_backend_imageupload_deletetempfileRouteInfo()
    {
        return array(array (  0 => 'imagen',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImageuploadController::deleteTempFileAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'imagen',  ),  1 =>   array (    0 => 'text',    1 => '/admin/deletetemp',  ),));
    }

    private function getadmin_configuracionRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/configuracion/',  ),));
    }

    private function getadmin_configuracion_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/configuracion',  ),));
    }

    private function getadmin_configuracion_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/configuracion/new',  ),));
    }

    private function getadmin_configuracion_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/configuracion/create',  ),));
    }

    private function getadmin_configuracion_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/configuracion',  ),));
    }

    private function getadmin_configuracion_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/configuracion',  ),));
    }

    private function getadmin_configuracion_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/configuracion',  ),));
    }

    private function getadmin_lugaresRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/lugares/',  ),));
    }

    private function getadmin_lugaresfiltramunicipioRouteInfo()
    {
        return array(array (  0 => 'idprovincia',  1 => 'obligatorio',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::sacamunicipiosAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'obligatorio',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'idprovincia',  ),  2 =>   array (    0 => 'text',    1 => '/admin/lugares/filtramunicipios',  ),));
    }

    private function getadmin_lugaresfiltralugaresRouteInfo()
    {
        return array(array (  0 => 'idprovincia',  1 => 'idmunicipio',  2 => 'obligatorio',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::sacalugaresAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'obligatorio',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'idmunicipio',  ),  2 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'idprovincia',  ),  3 =>   array (    0 => 'text',    1 => '/admin/lugares/filtrazonas',  ),));
    }

    private function getadmin_lugares_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/lugares',  ),));
    }

    private function getadmin_lugares_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/lugares/new',  ),));
    }

    private function getadmin_lugares_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/lugares/create',  ),));
    }

    private function getadmin_lugares_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/lugares',  ),));
    }

    private function getadmin_lugares_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/lugares',  ),));
    }

    private function getadmin_lugares_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/lugares',  ),));
    }

    private function getadmin_contactowebRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/contactoweb/',  ),));
    }

    private function getadmin_contactoweb_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/contactoweb',  ),));
    }

    private function getadmin_contactoweb_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/contactoweb/new',  ),));
    }

    private function getadmin_contactoweb_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/contactoweb/create',  ),));
    }

    private function getadmin_contactoweb_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/contactoweb',  ),));
    }

    private function getadmin_contactoweb_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/contactoweb',  ),));
    }

    private function getadmin_contactoweb_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/contactoweb',  ),));
    }

    private function getadmin_usuarioRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/usuario/',  ),));
    }

    private function getadmin_usuario_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/usuario',  ),));
    }

    private function getadmin_usuario_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/usuario/new',  ),));
    }

    private function getadmin_usuario_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/usuario/create',  ),));
    }

    private function getadmin_usuario_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/usuario',  ),));
    }

    private function getadmin_usuario_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/usuario',  ),));
    }

    private function getadmin_usuario_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/usuario',  ),));
    }

    private function getadmin_usuario_deletegetRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::deletegetAction',), array (  '_method' => 'get',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/usuario',  ),));
    }

    private function getadmin_usuario_enabledRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::enabledAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/enabled',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/usuario',  ),));
    }

    private function getadmin_usuario_disabledRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::disabledAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/disabled',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/usuario',  ),));
    }

    private function getadmin_usuario_banRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::banAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/ban',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/usuario',  ),));
    }

    private function getadmin_usuario_unbanRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::unbanAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/unban',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/usuario',  ),));
    }

    private function getadmin_hotelRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/hotel/',  ),));
    }

    private function getadmin_hotel_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/hotel',  ),));
    }

    private function getadmin_hotel_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/hotel/new',  ),));
    }

    private function getadmin_hotel_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/hotel/create',  ),));
    }

    private function getadmin_hotel_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/hotel',  ),));
    }

    private function getadmin_hotel_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/hotel',  ),));
    }

    private function getadmin_hotel_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/hotel',  ),));
    }

    private function getadmin_hotel_reactivateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::reactivateHotelAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/reactivate',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/hotel',  ),));
    }

    private function getadmin_hotel_check_slugRouteInfo()
    {
        return array(array (  0 => 'slug',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::checkHotelSlugAction',), array (  '_method' => 'get',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'slug',  ),  1 =>   array (    0 => 'text',    1 => '/admin/hotel/check-hotel-slug',  ),));
    }

    private function getadmin_userempresaRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/userempresa/',  ),));
    }

    private function getadmin_userempresa_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/userempresa',  ),));
    }

    private function getadmin_userempresa_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/userempresa/new',  ),));
    }

    private function getadmin_userempresa_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/userempresa/create',  ),));
    }

    private function getadmin_userempresa_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/userempresa',  ),));
    }

    private function getadmin_userempresa_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/userempresa',  ),));
    }

    private function getadmin_userempresa_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/userempresa',  ),));
    }

    private function getadmin_userempresa_reactivateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::reactivateUserAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/reactivate',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/userempresa',  ),));
    }

    private function getadmin_useradministradorRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/useradministrador/',  ),));
    }

    private function getadmin_useradministrador_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/useradministrador',  ),));
    }

    private function getadmin_useradministrador_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/useradministrador/new',  ),));
    }

    private function getadmin_useradministrador_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/useradministrador/create',  ),));
    }

    private function getadmin_useradministrador_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/useradministrador',  ),));
    }

    private function getadmin_useradministrador_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/useradministrador',  ),));
    }

    private function getadmin_useradministrador_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/useradministrador',  ),));
    }

    private function getadmin_userhotelRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/userhotel/',  ),));
    }

    private function getadmin_userhotel_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/userhotel',  ),));
    }

    private function getadmin_userhotel_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/userhotel/new',  ),));
    }

    private function getadmin_userhotel_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/userhotel/create',  ),));
    }

    private function getadmin_userhotel_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/userhotel',  ),));
    }

    private function getadmin_userhotel_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/userhotel',  ),));
    }

    private function getadmin_userhotel_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/userhotel',  ),));
    }

    private function getadmin_userhotel_reactivateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::reactivateUserAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/reactivate',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/userhotel',  ),));
    }

    private function getadmin_ofertasRouteInfo()
    {
        return array(array (), array (  'filtro' => 'todos',  'id_hotel' => NULL,  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/ofertas/',  ),));
    }

    private function getadmin_ofertas_filtradasRouteInfo()
    {
        return array(array (  0 => 'filtro',  1 => 'id_hotel',), array (  'filtro' => 'todos',  'id_hotel' => NULL,  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::indexAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id_hotel',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'filtro',  ),  2 =>   array (    0 => 'text',    1 => '/admin/ofertas/filtro',  ),));
    }

    private function getadmin_ofertas_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/ofertas',  ),));
    }

    private function getadmin_ofertas_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/ofertas/new',  ),));
    }

    private function getadmin_ofertas_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/ofertas/create',  ),));
    }

    private function getadmin_ofertas_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/ofertas',  ),));
    }

    private function getadmin_ofertas_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/ofertas',  ),));
    }

    private function getadmin_ofertas_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/ofertas',  ),));
    }

    private function getacepta_ofertaRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::aceptaofertaAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/admin/ofertas/aceptaoferta',  ),));
    }

    private function gethoteles_backend_oferta_aceptarofertadesdeemailRouteInfo()
    {
        return array(array (  0 => 'idofertaunico',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::aceptarOfertaDesdeEmailAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'idofertaunico',  ),  1 =>   array (    0 => 'text',    1 => '/admin/ofertas/acepta-oferta',  ),));
    }

    private function gethoteles_backend_oferta_rechazarofertadesdeemailRouteInfo()
    {
        return array(array (  0 => 'idofertaunico',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::rechazarOfertaDesdeEmailAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'idofertaunico',  ),  1 =>   array (    0 => 'text',    1 => '/admin/ofertas/rechazar-oferta',  ),));
    }

    private function getdeclina_ofertaRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::declinaofertaAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/admin/ofertas/declinaoferta',  ),));
    }

    private function getadmin_ofertas_contraofertaRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::contraofertaAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/admin/ofertas/contraoferta',  ),));
    }

    private function gethoteles_backend_oferta_ofertadetailsRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::ofertaDetailsAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  1 =>   array (    0 => 'text',    1 => '/admin/ofertas/oferta-details',  ),));
    }

    private function gethoteles_backend_crontask_fixtureRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CronTaskController::fixtureAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/crontasks/fixture',  ),));
    }

    private function getadmin_homeconfiguracionRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/homeconfiguracion/',  ),));
    }

    private function getadmin_ordenahomeRouteInfo()
    {
        return array(array (  0 => 'orden',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::ordenaAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'orden',  ),  1 =>   array (    0 => 'text',    1 => '/admin/homeconfiguracion/ordena',  ),));
    }

    private function getadmin_homeconfiguracion_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/homeconfiguracion',  ),));
    }

    private function getadmin_homeconfiguracion_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/homeconfiguracion/new',  ),));
    }

    private function getadmin_homeconfiguracion_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/homeconfiguracion/create',  ),));
    }

    private function getadmin_homeconfiguracion_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/homeconfiguracion',  ),));
    }

    private function getadmin_homeconfiguracion_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/homeconfiguracion',  ),));
    }

    private function getadmin_homeconfiguracion_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/homeconfiguracion',  ),));
    }

    private function getadmin_homeconfiguracion_deletegetRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::deletegetAction',), array (  '_method' => 'get',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/homeconfiguracion',  ),));
    }

    private function getadmin_publicatuhotelRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/publicatuhotel/',  ),));
    }

    private function getadmin_publicatuhotel_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/publicatuhotel',  ),));
    }

    private function getadmin_publicatuhotel_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/publicatuhotel/new',  ),));
    }

    private function getadmin_publicatuhotel_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/publicatuhotel/create',  ),));
    }

    private function getadmin_publicatuhotel_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/publicatuhotel',  ),));
    }

    private function getadmin_publicatuhotel_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/publicatuhotel',  ),));
    }

    private function getadmin_publicatuhotel_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/publicatuhotel',  ),));
    }

    private function getadmin_condicioneslegalesRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/condicioneslegales/',  ),));
    }

    private function getadmin_condicioneslegales_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/condicioneslegales',  ),));
    }

    private function getadmin_condicioneslegales_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/condicioneslegales/new',  ),));
    }

    private function getadmin_condicioneslegales_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/condicioneslegales/create',  ),));
    }

    private function getadmin_condicioneslegales_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/condicioneslegales',  ),));
    }

    private function getadmin_condicioneslegales_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/condicioneslegales',  ),));
    }

    private function getadmin_condicioneslegales_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/condicioneslegales',  ),));
    }

    private function getadmin_quienessomosRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/quienessomos/',  ),));
    }

    private function getadmin_quienessomos_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/quienessomos',  ),));
    }

    private function getadmin_quienessomos_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/quienessomos/new',  ),));
    }

    private function getadmin_quienessomos_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/quienessomos/create',  ),));
    }

    private function getadmin_quienessomos_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/quienessomos',  ),));
    }

    private function getadmin_quienessomos_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/quienessomos',  ),));
    }

    private function getadmin_quienessomos_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/quienessomos',  ),));
    }

    private function getadmin_incidenciaRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/incidencia/',  ),));
    }

    private function getadmin_incidencia_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/incidencia',  ),));
    }

    private function getadmin_incidencia_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/incidencia/new',  ),));
    }

    private function getadmin_incidencia_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/incidencia/create',  ),));
    }

    private function getadmin_incidencia_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/incidencia',  ),));
    }

    private function getadmin_incidencia_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/incidencia',  ),));
    }

    private function getadmin_incidencia_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/incidencia',  ),));
    }

    private function getadmin_incidencia_atenderRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::atenderAction',), array (  '_method' => 'get',), array (  0 =>   array (    0 => 'text',    1 => '/atender',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/incidencia',  ),));
    }

    private function getadmin_faqsRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/faqs/',  ),));
    }

    private function getadmin_ordenaRouteInfo()
    {
        return array(array (  0 => 'orden',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::ordenaAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'orden',  ),  1 =>   array (    0 => 'text',    1 => '/admin/faqs/ordena',  ),));
    }

    private function getadmin_faqs_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/faqs',  ),));
    }

    private function getadmin_faqs_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/faqs/new',  ),));
    }

    private function getadmin_faqs_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/faqs/create',  ),));
    }

    private function getadmin_faqs_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/faqs',  ),));
    }

    private function getadmin_faqs_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/faqs',  ),));
    }

    private function getadmin_faqs_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/faqs',  ),));
    }

    private function getadmin_landingRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LandingController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/dashboard/',  ),));
    }

    private function getadmin_empresaRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/empresa/',  ),));
    }

    private function getadmin_empresa_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/empresa',  ),));
    }

    private function getadmin_empresa_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/empresa/new',  ),));
    }

    private function getadmin_empresa_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/empresa/create',  ),));
    }

    private function getadmin_empresa_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/empresa',  ),));
    }

    private function getadmin_empresa_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/empresa',  ),));
    }

    private function getadmin_empresa_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/empresa',  ),));
    }

    private function getadmin_empresa_reactivateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::reactivateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/reactivate',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/empresa',  ),));
    }

    private function getadmin_seoRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/seo/',  ),));
    }

    private function getadmin_seo_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/seo',  ),));
    }

    private function getadmin_seo_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/seo/new',  ),));
    }

    private function getadmin_seo_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/seo/create',  ),));
    }

    private function getadmin_seo_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/seo',  ),));
    }

    private function getadmin_seo_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/seo',  ),));
    }

    private function getadmin_seo_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/seo',  ),));
    }

    private function gethoteles_backend_notificacion_updatecountersRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NotificacionController::updateCountersAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/notificaciones/update',  ),));
    }

    private function getadmin_cookiesRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/cookies/',  ),));
    }

    private function getadmin_cookies_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/cookies',  ),));
    }

    private function getadmin_cookies_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/cookies/new',  ),));
    }

    private function getadmin_cookies_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/cookies/create',  ),));
    }

    private function getadmin_cookies_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/cookies',  ),));
    }

    private function getadmin_cookies_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/cookies',  ),));
    }

    private function getadmin_cookies_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/cookies',  ),));
    }

    private function getadmin_imagenhotelRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/imagenhotel/',  ),));
    }

    private function getadmin_imagenhotel_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/imagenhotel',  ),));
    }

    private function getadmin_imagenhotel_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/imagenhotel/new',  ),));
    }

    private function getadmin_imagenhotel_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/imagenhotel/create',  ),));
    }

    private function getadmin_imagenhotel_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/imagenhotel',  ),));
    }

    private function getadmin_imagenhotel_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/imagenhotel',  ),));
    }

    private function getadmin_imagenhotel_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/imagenhotel',  ),));
    }

    private function getadmin_privacidadRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/privacidad/',  ),));
    }

    private function getadmin_privacidad_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/privacidad',  ),));
    }

    private function getadmin_privacidad_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/privacidad/new',  ),));
    }

    private function getadmin_privacidad_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/privacidad/create',  ),));
    }

    private function getadmin_privacidad_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/privacidad',  ),));
    }

    private function getadmin_privacidad_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/privacidad',  ),));
    }

    private function getadmin_privacidad_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/privacidad',  ),));
    }

    private function getadmin_newsletterRouteInfo()
    {
        return array(array (  0 => 'email',), array (  'email' => NULL,  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::indexAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'email',  ),  1 =>   array (    0 => 'text',    1 => '/admin/newsletter',  ),));
    }

    private function getadmin_newsletter_showRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::showAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/show',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/newsletter',  ),));
    }

    private function getadmin_newsletter_newRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::newAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/admin/newsletter/new',  ),));
    }

    private function getadmin_newsletter_createRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::createAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/admin/newsletter/create',  ),));
    }

    private function getadmin_newsletter_editRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::editAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/edit',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/newsletter',  ),));
    }

    private function getadmin_newsletter_updateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::updateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/update',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/newsletter',  ),));
    }

    private function getadmin_newsletter_deleteRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::deleteAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/delete',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/newsletter',  ),));
    }

    private function getadmin_newsletter_activateRouteInfo()
    {
        return array(array (  0 => 'id',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::activateAction',), array (  '_method' => 'post',), array (  0 =>   array (    0 => 'text',    1 => '/activate',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'id',  ),  2 =>   array (    0 => 'text',    1 => '/admin/newsletter',  ),));
    }

    private function gethoteles_backend_passwordreset_afterresetRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PasswordResetController::afterResetAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/password-cambiada',  ),));
    }

    private function getHotelesFrontendBundle_homepageRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::indexAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/',  ),));
    }

    private function getHotelesFrontendBundle_compruebaloginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::compruebaloginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/compruebalogin',  ),));
    }

    private function getHotelesFrontendBundle_getUserDataRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::getUserDataAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/getuserdata',  ),));
    }

    private function getHotelesFrontendBundle_logoutRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::logoutAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/logoutuser',  ),));
    }

    private function getHotelesFrontendBundle_compruebaloginuserrssRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::compruebaloginuserrssAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/compruebaloginuserrss',  ),));
    }

    private function getHotelesFrontendBundle_listadoprovinciaRouteInfo()
    {
        return array(array (  0 => 'provincia',  1 => 'fechain',  2 => 'fechaout',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciaAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechaout',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechain',  ),  2 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'provincia',  ),  3 =>   array (    0 => 'text',    1 => '/resultados',  ),));
    }

    private function getHotelesFrontendBundle_listadoprovinciamunicipioRouteInfo()
    {
        return array(array (  0 => 'provincia',  1 => 'municipio',  2 => 'fechain',  3 => 'fechaout',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciamunicipioAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechaout',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechain',  ),  2 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'municipio',  ),  3 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'provincia',  ),  4 =>   array (    0 => 'text',    1 => '/resultados',  ),));
    }

    private function getHotelesFrontendBundle_listadoprovinciamunicipiolugarRouteInfo()
    {
        return array(array (  0 => 'provincia',  1 => 'municipio',  2 => 'zona',  3 => 'fechain',  4 => 'fechaout',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciamunicipiolugarAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechaout',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechain',  ),  2 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'zona',  ),  3 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'municipio',  ),  4 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'provincia',  ),  5 =>   array (    0 => 'text',    1 => '/resultados',  ),));
    }

    private function getHotelesFrontendBundle_listadoprovinciaajaxRouteInfo()
    {
        return array(array (  0 => 'provincia',  1 => 'fechain',  2 => 'fechaout',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciaajaxAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechaout',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechain',  ),  2 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'provincia',  ),  3 =>   array (    0 => 'text',    1 => '/listadoajax',  ),));
    }

    private function getHotelesFrontendBundle_listadoprovinciamunicipioajaxRouteInfo()
    {
        return array(array (  0 => 'provincia',  1 => 'municipio',  2 => 'fechain',  3 => 'fechaout',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciamunicipioajaxAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechaout',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechain',  ),  2 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'municipio',  ),  3 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'provincia',  ),  4 =>   array (    0 => 'text',    1 => '/listadoajax',  ),));
    }

    private function getHotelesFrontendBundle_listadoprovinciamunicipiolugarajaxRouteInfo()
    {
        return array(array (  0 => 'provincia',  1 => 'municipio',  2 => 'zona',  3 => 'fechain',  4 => 'fechaout',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciamunicipiolugarajaxAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechaout',  ),  1 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'fechain',  ),  2 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'zona',  ),  3 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'municipio',  ),  4 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'provincia',  ),  5 =>   array (    0 => 'text',    1 => '/listadoajax',  ),));
    }

    private function getHotelesFrontendBundle_detallehotelRouteInfo()
    {
        return array(array (  0 => 'nombrehotel',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::detallehotelAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'nombrehotel',  ),  1 =>   array (    0 => 'text',    1 => '/hotel',  ),));
    }

    private function getHotelesFrontendBundle_ofertaRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::ofertaAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/oferta',  ),));
    }

    private function getHotelesFrontendBundle_registroajaxRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::registroajaxAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/registroajax',  ),));
    }

    private function getHotelesFrontendBundle_registroextraajaxRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::registroextraajaxAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/registroextraajax',  ),));
    }

    private function getHotelesFrontendBundle_registronewsletterajaxRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::registronewsletterajaxAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/registronewsletterajax',  ),));
    }

    private function getHotelesFrontendBundle_ofertarealizadaRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::ofertarealizadaAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/oferta-realizada',  ),));
    }

    private function getHotelesFrontendBundle_usuariobaneadoRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::usuarioBaneadoAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/usuario-baneado',  ),));
    }

    private function getHotelesFrontendBundle_quienesSomosRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::quienesSomosAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/quienes-somos',  ),));
    }

    private function getHotelesFrontendBundle_condicionesLegalesRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::condicionesLegalesAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/condiciones-legales',  ),));
    }

    private function getHotelesFrontendBundle_privacidadRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::privacidadAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/condiciones-particulares-y-privacidad',  ),));
    }

    private function getHotelesFrontendBundle_cookiesRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::cookiesAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/cookies',  ),));
    }

    private function getHotelesFrontendBundle_faqsRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::faqsAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/faqs',  ),));
    }

    private function getHotelesFrontendBundle_publicaTuHotelRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::publicaTuHotelAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/publica-tu-hotel',  ),));
    }

    private function getHotelesFrontendBundle_contactoRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::contactoAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/contacto',  ),));
    }

    private function getHotelesFrontendBundle_mapawebRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::mapawebAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/mapaweb',  ),));
    }

    private function gethoteles_frontend_tpv_doneRouteInfo()
    {
        return array(array (  0 => 'firma',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\TPVController::doneAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'firma',  ),  1 =>   array (    0 => 'text',    1 => '/tpv/done',  ),));
    }

    private function gethoteles_frontend_tpv_processpaymentRouteInfo()
    {
        return array(array (  0 => 'firma',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\TPVController::processpaymentAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'firma',  ),  1 =>   array (    0 => 'text',    1 => '/tpv/processpayment',  ),));
    }

    private function gethoteles_frontend_tpv_successRouteInfo()
    {
        return array(array (  0 => 'firma',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\TPVController::successAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'firma',  ),  1 =>   array (    0 => 'text',    1 => '/tpv/success',  ),));
    }

    private function gethoteles_frontend_tpv_failRouteInfo()
    {
        return array(array (  0 => 'firma',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\TPVController::failAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'firma',  ),  1 =>   array (    0 => 'text',    1 => '/tpv/fail',  ),));
    }

    private function gethoteles_frontend_tpv_payRouteInfo()
    {
        return array(array (  0 => 'firma',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\TPVController::payAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'firma',  ),  1 =>   array (    0 => 'text',    1 => '/tpv/pay',  ),));
    }

    private function getprueba_de_renderRouteInfo()
    {
        return array(array (  0 => 'templateName',), array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::renderAction',), array (), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'templateName',  ),  1 =>   array (    0 => 'text',    1 => '/render',  ),));
    }

    private function getfos_user_security_loginRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login',  ),));
    }

    private function getfos_user_security_checkRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/login_check',  ),));
    }

    private function getfos_user_security_logoutRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/logout',  ),));
    }

    private function getfos_user_profile_showRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::urlRedirectAction',  'path' => '/password-cambiada',  'permanent' => true,), array (), array (  0 =>   array (    0 => 'text',    1 => '/profile',  ),));
    }

    private function getfos_user_profile_editRouteInfo()
    {
        return array(array (), array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::urlRedirectAction',  'path' => '/password-cambiada',  'permanent' => true,), array (), array (  0 =>   array (    0 => 'text',    1 => '/profile/edit',  ),));
    }

    private function getfos_user_resetting_requestRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/request',  ),));
    }

    private function getfos_user_resetting_send_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',), array (  '_method' => 'POST',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/send-email',  ),));
    }

    private function getfos_user_resetting_check_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/resetting/check-email',  ),));
    }

    private function getfos_user_resetting_resetRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',), array (  '_method' => 'GET|POST',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/resetting/reset',  ),));
    }

    private function getfos_user_registration_registerRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',), array (), array (  0 =>   array (    0 => 'text',    1 => '/register/',  ),));
    }

    private function getfos_user_registration_check_emailRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/register/check-email',  ),));
    }

    private function getfos_user_registration_confirmRouteInfo()
    {
        return array(array (  0 => 'token',), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '[^/]+?',    3 => 'token',  ),  1 =>   array (    0 => 'text',    1 => '/register/confirm',  ),));
    }

    private function getfos_user_registration_confirmedRouteInfo()
    {
        return array(array (), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',), array (  '_method' => 'GET',), array (  0 =>   array (    0 => 'text',    1 => '/register/confirmed',  ),));
    }

    private function getfos_js_routing_jsRouteInfo()
    {
        return array(array (  0 => '_format',), array (  '_controller' => 'fos_js_routing.controller:indexAction',  '_format' => 'js',), array (  '_format' => 'js|json',), array (  0 =>   array (    0 => 'variable',    1 => '.',    2 => 'js|json',    3 => '_format',  ),  1 =>   array (    0 => 'text',    1 => '/js/routing',  ),));
    }

    private function getremove_trailing_slashRouteInfo()
    {
        return array(array (  0 => 'url',), array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\RedirectingController::removeTrailingSlashAction',), array (  'url' => '.*/',  '_method' => 'GET',), array (  0 =>   array (    0 => 'variable',    1 => '/',    2 => '.*/',    3 => 'url',  ),));
    }
}
