<?php

/* FOSUserBundle:Resetting:reset_content.html.twig */
class __TwigTemplate_9eb199ae1d9243920e546d431d558f3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo $this->env->getExtension('form')->setTheme($this->getContext($context, "form"), array($this->getContext($context, "theme"), ));
        // line 2
        echo "<form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_reset", array("token" => $this->getContext($context, "token"))), "html", null, true);
        echo "\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo " method=\"POST\" class=\"fos_user_resetting_reset form-signin\">
    ";
        // line 4
        echo "    ";
        if (array_key_exists("invalid_username", $context)) {
            // line 5
            echo "        <div class=\"alert alert-danger fade in alert-width\">
            <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>
            <strong>Error</strong><br> ";
            // line 7
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Credenciales no válidas", array("%username%" => $this->getContext($context, "invalid_username"))), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 10
        echo "    <h2 class=\"form-signin-heading no-icon\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Introduce tu contraseña"), "html", null, true);
        echo "</h2>
    <div class=\"login-wrap\">
        <p>";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Introduce tu contraseña: (que sea facil de recordar)"), "html", null, true);
        echo "</p>
        ";
        // line 14
        echo "        ";
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "new"), "first"), array("attr" => array("class" => "form-control")));
        echo "            
        <p>";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Repite tu contraseña:"), "html", null, true);
        echo "</p>
        ";
        // line 16
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "form"), "new"), "second"), array("attr" => array("class" => "form-control")));
        echo "         
        ";
        // line 17
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
        <button class=\"btn btn-lg btn-login btn-block\" type=\"submit\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("resetting.reset.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar"), "html", null, true);
        echo "</button>
    </div>    
</form>


";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 18,  62 => 17,  58 => 16,  54 => 15,  49 => 14,  45 => 12,  39 => 10,  33 => 7,  19 => 2,  17 => 1,  96 => 42,  93 => 41,  86 => 43,  84 => 41,  80 => 39,  74 => 38,  65 => 35,  60 => 34,  55 => 33,  51 => 32,  18 => 1,  29 => 5,  26 => 4,);
    }
}
