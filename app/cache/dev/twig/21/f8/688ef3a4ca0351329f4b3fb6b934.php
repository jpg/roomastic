<?php

/* HotelesFrontendBundle:Frontend:oferta.html.twig */
class __TwigTemplate_21f8688ef3a4ca0351329f4b3fb6b934 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->
    <head>

        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>Oferta - Roomastic</title>

        <meta name=\"description\" content=\"\">
        <!--<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">-->

        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

        <!--<link href=\"http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css\" rel=\"stylesheet\">-->
        <link href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\">

        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/daterangepicker-bs3.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/selectize.default.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/minimal.css\">
        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/sweet-alert.css\">


        <link rel=\"icon\" type=\"image/x-icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\" />

        <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/roomastic.css\">

        <script type=\"text/javascript\" src=\"https://maps.google.com/maps/api/js?sensor=true\"></script>
        <script src=\"/bundles/hotelesfrontend/js/vendor/modernizr/modernizr.js\"></script>

        ";
        // line 36
        echo "
        <!--[if lt IE 9]>
            <script src=\"js/vendor/html5shiv/dist/html5shiv.min.js\"></script>
        <![endif]-->
        <style type=\"text/css\">

            .wrapper.wpregistro {
                min-height: 0;
            }

            .modalregister {
                display: none;
                position: absolute;
                height: 100%;
                width: 100%;
                z-index: 4;
                background: rgba(59, 59, 59, 0.8);

            }
            .modalregister .header {
                width: 100%;
                height: 50px;
                padding: 5px 0 0 0; 
            }
            .modalregister .header .close {
                float: right;
                margin-right: 10px;
                color: #fff;
                font-size: 20px;
                font-weight: bold;
            }
            .modalregister .body{
                width: 100%;
                height: 550px;
            }
            .modalregister .bodyregister, .modalregister .bodyregisterextra {
                width: 100%;
                height: 550px;
            }            
            .modalregister .bodyregister .left, .modalregister .bodyregister .right {
                height:  100%;
                width:  44%;
                padding: 0 0 0 40px;

            }
            .hide{
                display: none;
            }
            input[type=password] {
                /*.radius;*/
                height: 31px;
                float: left;
                background-color: #fff;
                border: none;
                box-shadow: 0 0 0 2px rgba(235, 235, 235, 0.9) !important;
                color: #474747;
                font-size: 15px;
                font-weight: 700;
                box-sizing: border-box;
                padding: 0 9px;
            }            
        </style>

    </head> 
    ";
        // line 100
        $context["rrss"] = $this->env->getExtension('twig_extension')->getRRSS();
        // line 101
        echo "
    <body class=\"interna\">
        <!--[if lt IE 7]>
            <p class=\"browsehappy\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- aviso cookie -->
        <div id=\"cookie-bar\" class=\"fixed\"></div>

        <div class=\"wrapper wpregistro\">

            <div class=\"modalregister\">


                <div class=\"header\">
                    <a href=\"#\" class=\"close\">X</a>
                </div>

                <div class=\"body\">

                    <form class=\"form-signin\" action=\"";
        // line 121
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_check"), "html", null, true);
        echo "\" method=\"post\" id=\"formlogin\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">
                        <div class=\"errorlogin\"></div>

                        <h2 class=\"form-signin-heading\">";
        // line 124
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Debes estar registrado para hacer tu oferta"), "html", null, true);
        echo "</h2>

                        <div class=\"login-wrap left\">
                            <p class=\"tit\">";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso"), "html", null, true);
        echo "</p>

                            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 129
        echo twig_escape_filter($this->env, $this->getContext($context, "csrf_token"), "html", null, true);
        echo "\" />
                            <input type=\"text\" class=\"form-control\" placeholder=\"";
        // line 130
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuario"), "html", null, true);
        echo "\" name=\"_username\" value=\"";
        if (array_key_exists("last_username", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getContext($context, "last_username"), "html", null, true);
            echo " ";
        }
        echo "\" autofocus>
                            <input type=\"password\" class=\"form-control\" placeholder=\"";
        // line 131
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraseña"), "html", null, true);
        echo "\" name=\"_password\">

                            <button class=\"btn btn-lg btn-login btn-block\" type=\"submit\">";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceder"), "html", null, true);
        echo "</button>
                            <p class=\"forgor_ps\">
                                <a href=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_request"), "html", null, true);
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado tu contraseña?"), "html", null, true);
        echo "</a>
                            </p>

                            <p>";
        // line 138
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("O accede con tus redes sociales:"), "html", null, true);
        echo "</p>

                            <div class=\"login-social-link\">
                                <a href=\"#\" class=\"facebook login-rrss\">
                                    <i class=\"fa fa-facebook\"></i>
                                    <!--Facebook-->
                                </a>
                                <a href=\"#\" class=\"google login-rrss\">
                                    <i class=\"fa fa-google-plus\"></i>
                                    <!-- google + -->
                                </a>
                            </div>

                        </div><!-- /left -->

                        <div class=\"login-wrap right\">
                            <p class=\"tit\">";
        // line 154
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Registro"), "html", null, true);
        echo "</p>
                            <p>";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Puedes registrarte directamente en nuestra web:"), "html", null, true);
        echo "</p>

                            <button class=\"btn btn-lg btn-login btn-block showregister\" type=\"submit\">";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("REGISTRO EN LA WEB"), "html", null, true);
        echo "</button>

                            <p>";
        // line 159
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("O con tu cuenta de usuario en alguna de estas redes sociales:"), "html", null, true);
        echo "</p>

                            <div class=\"login-social-link\">

                                <a href=\"#\" class=\"facebook login-rrss\">
                                    <i class=\"fa fa-facebook\"></i>
                                </a>

                                <div id=\"fb-root\"></div>
                                <script>
                                    if (typeof ROOMASTIC === 'undefined') {
                                        ROOMASTIC = {};
                                    }
                                    ROOMASTIC.facebookAppId = '";
        // line 172
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "configuracion"), "fbappid"), "html", null, true);
        echo "';
                                    ROOMASTIC.state = '";
        // line 173
        echo twig_escape_filter($this->env, $this->getContext($context, "csrf_token"), "html", null, true);
        echo "';
                                    (function () {
                                        var e = document.createElement('script');
                                        e.async = true;
                                        e.src = document.location.protocol +
                                                '//connect.facebook.net/en_US/all.js';
                                        document.getElementById('fb-root').appendChild(e);
                                    }());
                                </script>

                                <a href=\"#\" class=\"google login-rrss\">
                                    <i class=\"fa fa-google-plus\"></i>
                                    <!-- google + -->
                                    <script type=\"text/javascript\">
                                        (function () {
                                            var po = document.createElement('script');
                                            po.type = 'text/javascript';
                                            po.async = true;
                                            po.src = 'https://plus.google.com/js/client:plusone.js?onload=googleReady';
                                            var s = document.getElementsByTagName('script')[0];
                                            s.parentNode.insertBefore(po, s);
                                        })();


                                    </script>
                                </a>
                            </div>


                        </div><!-- /right -->
                        <div class=\"clearfix\"></div>

                        <!-- Modal -->
                        <div aria-hidden=\"true\" aria-labelledby=\"myModalLabel\" role=\"dialog\" tabindex=\"-1\" id=\"myModal\" class=\"modal fade\" style=\"display:none;\">
                            <div class=\"modal-dialog\">
                                <div class=\"modal-content\">
                                    <div class=\"modal-header\">
                                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                        <h4 class=\"modal-title\">";
        // line 211
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado la contraseña?"), "html", null, true);
        echo "</h4>
                                    </div>
                                    <div class=\"modal-body\">
                                        <p>";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Escribe tu email y te reenviaremos una nueva contraseña"), "html", null, true);
        echo "</p>
                                        <input type=\"text\" name=\"email\" placeholder=\"Email\" autocomplete=\"off\" class=\"form-control placeholder-no-fix\">

                                    </div>
                                    <div class=\"modal-footer\">
                                        <button data-dismiss=\"modal\" class=\"btn btn-default\" type=\"button\">";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cancelar"), "html", null, true);
        echo "</button>
                                        <button class=\"btn btn-success\" type=\"button\">";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar"), "html", null, true);
        echo "</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- modal -->

                    </form><!-- form-signin -->


                </div>
                <!--/body -->


                <div class=\"bodyregister hide\">

                    <form class=\"form-signin\" action=\"";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_registroajax"), "html", null, true);
        echo "\" id=\"formregister\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "formRegister"));
        echo ">

                        <h2 class=\"form-signin-heading\">";
        // line 238
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Debes estar registrado para hacer tu oferta"), "html", null, true);
        echo "</h2>
                        <div class=\"login-wrap\">
                            <p class=\"tit\">";
        // line 240
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso"), "html", null, true);
        echo "</p>
                            <div class=\"input_newreg\">

                                ";
        // line 243
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "nombre"), array("attr" => array("class" => "form-control width1", "placeholder" => $this->env->getExtension('translator')->trans("Nombre"))));
        echo "  

                                ";
        // line 245
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "apellidos"), array("attr" => array("class" => "form-control width2", "placeholder" => $this->env->getExtension('translator')->trans("Apellidos"))));
        echo "   

                                ";
        // line 247
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "direccioncompleta"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("Dirección completa (Campo no obligatorio)"))));
        echo "    

                                ";
        // line 249
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "dni"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("DNI (Para dar veracidad a tu oferta)"))));
        echo "

                                ";
        // line 251
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "telefono"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("Teléfono (Para contactarte en caso de cambios en la reserva)"))));
        echo "    

                                ";
        // line 253
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "email"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("Email"))));
        echo "

                                ";
        // line 255
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "formRegister"), "password"), "first"), array("attr" => array("class" => "form-control width6", "placeholder" => $this->env->getExtension('translator')->trans("Contraseña"))));
        echo "

                                ";
        // line 257
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "formRegister"), "password"), "second"), array("attr" => array("class" => "form-control width7", "placeholder" => $this->env->getExtension('translator')->trans("Repite contraseña"))));
        echo "    


                                <div class=\"clearfix\"></div>

                                <button type=\"submit\" class=\"btn btn-lg btn-login btn-block w-es\" >";
        // line 262
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar datos y hacer oferta"), "html", null, true);
        echo "</button>
                                ";
        // line 263
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "formRegister"));
        echo "
                            </div>
                        </div>
                    </form>  
                </div>

                <div class=\"bodyregisterextra hide\">

                    <form class=\"form-signin\" action=\"";
        // line 271
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_registroextraajax"), "html", null, true);
        echo "\" id=\"formregisterextra\" method=\"post\"  ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "formRegisterextra"));
        echo ">
                        <h2 class=\"form-signin-heading\">";
        // line 272
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Completa tu registro para hacer tu oferta"), "html", null, true);
        echo "</h2>
                        <div class=\"login-wrap\">
                            <p class=\"tit\">";
        // line 274
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso"), "html", null, true);
        echo "</p>
                            <div class=\"input_newreg\">

                                ";
        // line 277
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "nombre"), array("attr" => array("class" => "form-control width1 nombre", "placeholder" => $this->env->getExtension('translator')->trans("Nombre"))));
        echo "  

                                ";
        // line 279
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "apellidos"), array("attr" => array("class" => "form-control width2 apellidos", "placeholder" => $this->env->getExtension('translator')->trans("Apellidos"))));
        echo "   

                                ";
        // line 281
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "direccioncompleta"), array("attr" => array("class" => "form-control width3 direccion", "placeholder" => $this->env->getExtension('translator')->trans("Dirección completa (Campo no obligatorio)"))));
        echo "

                                ";
        // line 283
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "dni"), array("attr" => array("class" => "form-control width3 dni", "placeholder " => $this->env->getExtension('translator')->trans("DNI (Para dar veracidad a tu oferta)"))));
        echo "

                                ";
        // line 285
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "telefono"), array("attr" => array("class" => "form-control width3 telefono", "placeholder" => $this->env->getExtension('translator')->trans("Teléfono (Para contactarte en caso de cambios en la reserva)"))));
        echo "    

                                ";
        // line 287
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "email"), array("attr" => array("class" => "form-control width3 email", "placeholder" => $this->env->getExtension('translator')->trans("Email"))));
        echo "

                                <div class=\"clearfix\"></div>

                                <button type=\"submit\" class=\"btn btn-lg btn-login btn-block w-es\">";
        // line 291
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar datos y hacer oferta"), "html", null, true);
        echo "</button>

                                <div class=\"clearfix\"></div>

                            </div>
                        </div>
                        ";
        // line 297
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "formRegisterextra"));
        echo "
                    </form>  
                </div><!--/bodyregisterextra -->

            </div>
            <header id=\"header\" class=\"oferta\">
                <div class=\"lineatop\"></div>

                <div class=\"container clearfix\">
                    <a class=\"logo\" href=\"";
        // line 306
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"\"><h1>Roomastic</h1></a>

                    <div class=\"menu_int clearfix\">
                        <ul class=\"social\">
                            <li><a href=\"";
        // line 310
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "rrss"), "twitter"), "html", null, true);
        echo "\" target=\"_blank\" title=\"twitter\">twitter</a></li>
                            <li><a class=\"facebook\" href=\"";
        // line 311
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "rrss"), "facebook"), "html", null, true);
        echo "\" target=\"_blank\" title=\"facebook\">facebook</a></li>
                        </ul>
                        <ul class=\"menu\">                      
                            <li><a href=\"";
        // line 314
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_homepage"), "html", null, true);
        echo "\" title=\"HOME\">HOME</a></li>
                            <li><a href=\"";
        // line 315
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_faqs"), "html", null, true);
        echo "\" title=\"FAQS\">FAQS</a></li>
                            <!--<li><a href=\"#\" title=\"AYUDA\">AYUDA</a></li>-->
                            <li><a href=\"";
        // line 317
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" title=\"CONTACTO\">CONTACTO</a></li>
                            <li><a href=\"";
        // line 318
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_quienesSomos"), "html", null, true);
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos?"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Quiénes Somos?"), "html", null, true);
        echo "</a></li>
                        </ul>
                    </div><!-- menu_int --> 
                </div><!-- container -->

            </header><!-- /header -->

            <div class=\"container clearfix\">

                <form action=\"\" method=\"post\" id=\"formoferta\">
                    ";
        // line 328
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "_token_oferta"));
        echo "


                    <div class=\"col_1\">

                        <h3 class=\"tit_int pagar\">¿Cuánto estás <br />dispuesto a pagar?</h3>
                        <div class=\"c_pagar clearfix\">
                            <p class=\"primero\">El precio debe ser por noche, habitación doble y en régimen de alojamiento. (Algunos hoteles ofrecen desayuno o media pensión incluido)</p>
                            ";
        // line 339
        echo "                            ";
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "preciohabitacion"), array("attr" => array("type" => "number", "placeholder" => "Precio", "tabindex" => "0")));
        echo "
                            <div class=\"clearfix\"></div>
                            <p>Los precios oficiales de los hoteles que has seleccionado <br /><strong>rondan entre los ";
        // line 341
        echo twig_escape_filter($this->env, $this->getContext($context, "precio_hotel_min"), "html", null, true);
        echo " € y los ";
        echo twig_escape_filter($this->env, $this->getContext($context, "precio_hotel_max"), "html", null, true);
        echo "  €</strong></p>
                        </div><!-- c_pagar -->

                        <hr>

                        <div class=\"clearfix\">
                            <h3 class=\"tit_int n_noches\">Número <br />de noches</h3>

                            <div class=\"caja_fecha\">
                                <span class=\"mtit_grey\">Fechas</span>
                                ";
        // line 354
        echo "                                ";
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "fecha"), array("attr" => array("value" => ((twig_date_format_filter($this->env, $this->getContext($context, "fecha_inicio"), "d/m/y") . " - ") . twig_date_format_filter($this->env, $this->getContext($context, "fecha_fin"), "d/m/y")), "tabindex" => "1", "class" => "select_fecha", "placeholder" => "Fechas")));
        echo "
                            </div>

                            <div class=\"caja_noches\">
                                <span class=\"mtit_grey\">Noches</span>
                                ";
        // line 362
        echo "                                ";
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "numnoches"), array("attr" => array("value" => $this->getContext($context, "num_noches"), "placeholder" => "3", "readonly" => "")));
        echo "
                            </div>
                        </div><!-- clearfix -->

                        <br>

                        <h3 class=\"tit_int n_gente\">Selecciona el <br />número de huéspedes</h3>

                        <div class=\"clearfix\">
                            <div class=\"caja_adultos\">
                                <span class=\"mtit_grey\">Adultos</span>
                                ";
        // line 376
        echo "                                ";
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "numadultos"), array("attr" => array("value" => "1", "placeholder" => "1", "tabindex" => "2", "class" => "select_adultos")));
        echo "
                            </div>

                            <div class=\"caja_ninos\">
                                <span class=\"mtit_grey\">Niños hasta 3 años</span>
                                ";
        // line 384
        echo "                                ";
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "numninos"), array("attr" => array("value" => "0", "placeholder" => "0", "tabindex" => "3", "class" => "select_ninios")));
        echo "
                            </div>
                        </div><!-- clearfix -->

                        <hr>

                        <h3 class=\"tit_int n_habs\">Número de <br />habitaciones</h3>
                            ";
        // line 394
        echo "                            ";
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "numhabitaciones"), array("attr" => array("value" => "1", "placeholder" => "1", "tabindex" => "4", "class" => "select_habs")));
        echo "

                    </div><!-- col_1 -->
                    <div class=\"col_2\">
                        <h3 class=\"tit_int\">Hoteles <br />seleccionados</h3>
                        <p>Estos son los hoteles que has seleccionado y a los que vas a enviar tu oferta:</p>
                        <hr>

                        <ul class=\"hoteles\">
                            ";
        // line 403
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["hotel"]) {
            // line 404
            echo "                                <li class=\"hotel_li\">
                                    <input type=\"checkbox\" class=\"checkbox-oferta\" name=\"hoteles[id][]\" id=\"";
            // line 405
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "id"), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "id"), "html", null, true);
            echo "\" checked>
                                    <label for=\"Hotel Diamante Suites\">";
            // line 406
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "nombrehotel"), "html", null, true);
            echo "</label>
                                </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 409
        echo "                        </ul>

                    </div><!-- col_2 -->
                    <div class=\"col_3\">
                        <h3>Precio total de tu oferta</h3>
                        <div class=\"caja\">
                            <p>En base a la configuración que nos acabas de facilitar, este será el precio total que vas a ofertar a los hoteles seleccionados:</p>
                            ";
        // line 419
        echo "                            ";
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "preciototaloferta"), array("attr" => array("value" => "", "placeholder" => "0", "readonly" => "")));
        echo "
                            <div class=\"clearfix\"></div>

                            <div class=\"acepto1 clearfix\">
                                <input type=\"checkbox\" id=\"acepto1\" class=\"checkbox-oferta\" name=\"acepto_condiciones\" required>
                                <label for=\"acepto1\">Acepto las condiciones de venta y de uso de esta web.</label>
                            </div>

                            <div class=\"acepto2 clearfix\">
                                <input type=\"checkbox\" id=\"acepto2\" class=\"checkbox-oferta\" name=\"acepto_suscribirme\" >
                                <label for=\"acepto2\">Acepto suscribirme al boletín de noticias RooMail</label>
                            </div>
                            <div class=\"validaoferta\">
                                ";
        // line 432
        if ($this->getAttribute($this->getContext($context, "app"), "user")) {
            // line 433
            echo "                                    ";
            $context["title"] = "Ya puedes hacer tu oferta";
            // line 434
            echo "                                ";
        } else {
            // line 435
            echo "                                    ";
            $context["title"] = "Todavia tienes que loguearte";
            // line 436
            echo "
                                ";
        }
        // line 438
        echo "
                                ";
        // line 440
        echo "                                <a class=\"btn_oferta\"  title=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
        echo "\" style=\"cursor: pointer;\" >hacer oferta</a>
                            </div>

                            </form>
                        </div><!-- caja -->
                    </div><!-- col_3 -->
                    ";
        // line 446
        $this->env->loadTemplate("HotelesBackendBundle:Extras:preload.html.twig")->display($context);
        // line 447
        echo "
            </div><!-- container -->

        </div><!-- wrapper -->

        ";
        // line 453
        echo "
        <footer id=\"footer\">

            <div class=\"footer1 footer-opaco\">
                <div class=\"container\">
                    <ul class=\"menu_foot clearfix\">                                                 
                        <li><a href=\"";
        // line 459
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_privacidad"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES PARTICULARES Y PRIVACIDAD"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 460
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_condicionesLegales"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CONDICIONES DEL SERVICIO"), "html", null, true);
        echo "</a></li>
                        <li><a href=\"";
        // line 461
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_publicaTuHotel"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("PUBLICA TU HOTEL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"v_movil\" href=\"";
        // line 462
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "request"), "attributes"), "get", array(0 => "_route"), "method"), twig_array_merge($this->env->getExtension('twig_extension')->getCurrentRouteParams(), array("version" => "mobile"))), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("VERSIÓN MÓVIL"), "html", null, true);
        echo "</a></li>
                        <li><a class=\"a_hoteles\" href=\"";
        // line 463
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_login"), "html", null, true);
        echo "\" title=\"\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("ACCESO HOTELES"), "html", null, true);
        echo "</a></li>
                    </ul>
                    <div class=\"att_cliente\">
                        ATENCIÓN AL CLIENTE <br /> <span>911 610 156</span>
                    </div>
                </div><!-- container -->
            </div><!-- footer1 -->
            <div class=\"footer2\">
                <div class=\"container\">
                    <span>© Copyright. Todos los derechos reservados.</span>
                </div><!-- container -->
            </div><!-- footer2 -->

        </footer>

        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
        <script>window.jQuery || document.write('<script src=\"/bundles/hotelesfrontend/js/vendor/jquery/dist/jquery.min.js\"><\\/script>')</script>
        <script>
            if (typeof ROOMASTIC === 'undefined') {
                ROOMASTIC = {};
            }
            ROOMASTIC.googleID = '";
        // line 484
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "configuracion"), "googAppId"), "html", null, true);
        echo "';
            ROOMASTIC.googleapikey = '";
        // line 485
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "configuracion"), "googApiKey"), "html", null, true);
        echo "';
            ROOMASTIC.version_mv = false;
        </script>
        <script src=\"/bundles/hotelesfrontend/js/plugins.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.backstretch.min.js\"></script>
        <script src=\"https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/moment.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/daterangepicker.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/selectize.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/icheck.min.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/plugins/gmap3.min.js\"></script>
        <script src=\"";
        // line 496
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 497
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>
        ";
        // line 498
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "844e296_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/844e296_roomastic_1.js");
            // line 503
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>

        ";
            // asset "844e296_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/844e296_ajax-preload_2.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>

        ";
            // asset "844e296_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296_2") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/844e296_facebook_login_3.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>

        ";
            // asset "844e296_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296_3") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/844e296_google_login_4.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>

        ";
            // asset "844e296_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296_4") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/844e296_oferta_5.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>

        ";
        } else {
            // asset "844e296"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_844e296") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/844e296.js");
            echo "        <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>

        ";
        }
        unset($context["asset_url"]);
        // line 506
        echo "        <!-- 
        <script src=\"/bundles/hotelesfrontend/js/roomastic.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/facebook_login.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/google_login.js\"></script>
        <script src=\"/bundles/hotelesfrontend/js/oferta.js\"></script> 
        -->

        <!-- sweet-alert -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/sweet-alert.js\"></script>
        <!-- cookies -->
        <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.cookiebar.js\"></script>

        <script>
            (function (b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function () {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-53266623-1');
            ga('send', 'pageview');
        </script>
    </body>
</html>
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:oferta.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  822 => 506,  778 => 503,  774 => 498,  770 => 497,  766 => 496,  752 => 485,  748 => 484,  722 => 463,  716 => 462,  710 => 461,  704 => 460,  698 => 459,  690 => 453,  683 => 447,  681 => 446,  671 => 440,  668 => 438,  664 => 436,  661 => 435,  658 => 434,  655 => 433,  653 => 432,  636 => 419,  627 => 409,  618 => 406,  612 => 405,  609 => 404,  605 => 403,  592 => 394,  581 => 384,  572 => 376,  557 => 362,  548 => 354,  533 => 341,  527 => 339,  516 => 328,  499 => 318,  495 => 317,  490 => 315,  486 => 314,  480 => 311,  476 => 310,  469 => 306,  457 => 297,  448 => 291,  441 => 287,  436 => 285,  431 => 283,  426 => 281,  421 => 279,  416 => 277,  410 => 274,  405 => 272,  399 => 271,  388 => 263,  384 => 262,  376 => 257,  371 => 255,  366 => 253,  361 => 251,  356 => 249,  351 => 247,  346 => 245,  341 => 243,  335 => 240,  330 => 238,  323 => 236,  304 => 220,  300 => 219,  292 => 214,  286 => 211,  245 => 173,  241 => 172,  225 => 159,  220 => 157,  215 => 155,  211 => 154,  192 => 138,  184 => 135,  179 => 133,  174 => 131,  164 => 130,  160 => 129,  155 => 127,  149 => 124,  141 => 121,  119 => 101,  117 => 100,  51 => 36,  17 => 1,);
    }
}
