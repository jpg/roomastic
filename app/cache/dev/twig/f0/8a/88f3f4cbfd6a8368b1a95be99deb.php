<?php

/* HotelesBackendBundle:Hotel:new.html.twig */
class __TwigTemplate_f08a88f3f4cbfd6a8368b1a95be99deb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nuevo hotel"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_javascripts($context, array $blocks = array())
    {
        // line 9
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 10
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "834bf80_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_834bf80_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/834bf80_part_1_hotel_create_update_1.js");
            // line 11
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "834bf80"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_834bf80") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/834bf80.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 15
        echo "    <form action=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_create"), "html", null, true);
        echo "\" method=\"post\" class=\"tasi-form\" id=\"formulariosubida\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">
        <div class=\"row\">
            <div class=\"col-lg-6\">
                <!--tab nav start-->
                <section class=\"panel\">
                    <header class=\"panel-heading\">
                        ";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear hotel"), "html", null, true);
        echo "
                    </header>
                    <header class=\"panel-heading tab-bg-dark-navy-blue \">
                        <ul class=\"nav nav-tabs\">
                            <li class=\"active ";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "email")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombreempresa")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cif")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "direccionfacturacion")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefono")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "numerocuenta")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#datoshotel\">";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombrepersonacontacto")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "apellidospersonacontacto")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cargopersonacontacto")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "emailpersonacontacto")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefonopersonacontacto")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#datospersona\">";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la persona de contacto"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombrehotel")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "url")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "descripcion")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "caracteristicas")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "ubicacion")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "pvpoficialenteros")), 6 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "pvpoficialdecimales")), 7 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cantidadminimaaceptadaenteros")), 8 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cantidadminimaaceptadadecimales")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" class=\"caracteristicashotel\" href=\"#caracteristicashotel\">";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ficha del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "imagenes")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" class=\"imageneshotel\" href=\"#imageneshotel\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imágenes del hotel"), "html", null, true);
        echo "</a>
                            </li>
                            <li class=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "seotitulo")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "seodescripcion")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "seokeywords")))), "html", null, true);
        echo "\">
                                <a data-toggle=\"tab\" href=\"#seo\">";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Seo"), "html", null, true);
        echo "</a>
                            </li>
                            ";
        // line 40
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 41
            echo "                                <li class=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionhotelentero")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionhoteldecimal")))), "html", null, true);
            echo "\">
                                    <a data-toggle=\"tab\" href=\"#comisiones\">";
            // line 42
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comisiones"), "html", null, true);
            echo "</a>
                                </li>
                            ";
        }
        // line 45
        echo "                        </ul>
                    </header>



                    <div class=\"panel-body\">
                        <div class=\"tab-content\">
                            <div id=\"datoshotel\" class=\"tab-pane active\">
                                <div class=\"form-group ";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "email"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 56
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "email"));
        echo "
                                        ";
        // line 57
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group\">
                                    <label>";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 63
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "empresa"));
        echo "
                                        ";
        // line 64
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "empresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>                
                                <div class=\"form-group  ";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombreempresa"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 68
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de empresa"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 70
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombreempresa"));
        echo "
                                        ";
        // line 71
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "nombreempresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cif"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cif"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 77
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cif"));
        echo "
                                        ";
        // line 78
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "cif"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "direccionfacturacion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dirección de facturación"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 84
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "direccionfacturacion"));
        echo "
                                        ";
        // line 85
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "direccionfacturacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefono"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 91
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefono"));
        echo "
                                        ";
        // line 92
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "telefono"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group ";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "numerocuenta"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de cuenta"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 98
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "numerocuenta"));
        echo "
                                        ";
        // line 99
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "numerocuenta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  

                            </div>
                            <div id=\"datospersona\" class=\"tab-pane\">
                                <div class=\"form-group  ";
        // line 105
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombrepersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 106
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la persona contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 108
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombrepersonacontacto"));
        echo " 
                                        ";
        // line 109
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "nombrepersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 112
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "apellidospersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 113
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 115
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "apellidospersonacontacto"));
        echo "
                                        ";
        // line 116
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "apellidospersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cargopersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 120
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 122
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cargopersonacontacto"));
        echo "
                                        ";
        // line 123
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "cargopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "emailpersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 129
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "emailpersonacontacto"));
        echo "
                                        ";
        // line 130
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "emailpersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>   
                                <div class=\"form-group  ";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefonopersonacontacto"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono de la persona de contacto"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 136
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefonopersonacontacto"));
        echo "
                                        ";
        // line 137
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "telefonopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group checkbox\">
                                    <label>
                                        ";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Marca esta casilla si quieres que este usuario administre la ficha del hotel"), "html", null, true);
        echo " 
                                        <input type=\"checkbox\" name=\"administrador\" value=\"1\">
                                    </label>
                                </div>
                            </div>
                            <div id=\"caracteristicashotel\" class=\"tab-pane\">
                                <div class=\"form-group ";
        // line 148
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombrehotel"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 149
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre del hotel"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 151
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombrehotel"));
        echo "
                                        ";
        // line 152
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "nombrehotel"), array("attr" => array("class" => "form-control nombre-hotel")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 155
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "url"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Url"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 158
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "url"));
        echo "
                                        ";
        // line 159
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "url"), array("attr" => array("class" => "form-control url-hotel")));
        echo "                        
                                    </div>
                                </div>                  
                                <div class=\"form-group\">
                                    <label>";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Selecciona el número de estrellas"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 165
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "calificacion"));
        echo "
                                        ";
        // line 166
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "calificacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-2\">";
        // line 170
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Piscina"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 172
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "piscina"));
        echo "
                                        ";
        // line 173
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "piscina"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Spa"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 178
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "spa"));
        echo "
                                        ";
        // line 179
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "spa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Wi-Fi"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 184
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "wi_fi"));
        echo "
                                        ";
        // line 185
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "wi_fi"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-2\">";
        // line 189
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso adaptado"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 191
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "acceso_adaptado"));
        echo "
                                        ";
        // line 192
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "acceso_adaptado"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 195
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptan perros"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 197
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "aceptan_perros"));
        echo "
                                        ";
        // line 198
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "aceptan_perros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-2\">";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aparcamiento"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 203
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "aparcamiento"));
        echo "
                                        ";
        // line 204
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "aparcamiento"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>                                    
                                </div>    
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-2\">";
        // line 208
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Business-Center"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 210
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "business_center"));
        echo "
                                        ";
        // line 211
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "business_center"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 214
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "descripcion"))), "html", null, true);
        echo "\" style=\"padding-top:20px;\">
                                    <label>";
        // line 215
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica una descripción breve de aproximadamente 200 carácteres"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 217
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "descripcion"));
        echo "
                                        ";
        // line 218
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "descripcion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 221
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "caracteristicas"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 222
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enumera las características del hotel de forma breve en aproximadamente 200 carácteres"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 224
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "caracteristicas"));
        echo "
                                        ";
        // line 225
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "caracteristicas"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group  ";
        // line 228
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "ubicacion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 229
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Escribe el nombre de la calle y número acompañado de la localidad, de la ubicación del hotel"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 231
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "ubicacion"));
        echo "
                                        ";
        // line 232
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "ubicacion"), array("attr" => array("class" => "form-control direccionmapa")));
        echo "                        
                                    </div>
                                </div>  
                                <div class=\"form-group\">
                                    <label>";
        // line 236
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Mapa"), "html", null, true);
        echo "</label>
                                    <div>
                                        <div id=\"mapaiframe\" style=\"width:500px!important;height:250px!important;\">
                                        </div>
                                        <div style=\"clear:both;\"></div>
                                    </div>
                                </div>
                                <br>
                                <div class=\"form-group padgbott\">
                                    <label class=\"col-sm-1\">";
        // line 245
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Provincia"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 247
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "provincia"));
        echo "
                                        ";
        // line 248
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "provincia"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>

                                    <label class=\"col-sm-1\">";
        // line 251
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Municipio"), "html", null, true);
        echo "&nbsp</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 253
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "municipio"));
        echo "
                                        ";
        // line 254
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "municipio"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <label class=\"col-sm-1\">";
        // line 256
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Zona"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 258
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "lugar"));
        echo "
                                        ";
        // line 259
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "lugar"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 262
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "pvpoficialenteros"))), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "pvpoficialdecimales"))), "html", null, true);
        echo " padgbott\">
                                    <label class=\"col-sm-6\">";
        // line 263
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica el pvp oficial en habitación individual de uso doble"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 265
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "pvpoficialenteros"));
        echo "
                                        ";
        // line 266
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "pvpoficialenteros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 269
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "pvpoficialdecimales"));
        echo "
                                        ";
        // line 270
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "pvpoficialdecimales"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>                    
                                </div>  
                                <div class=\"clearfix\"></div>
                                <div class=\"form-group  ";
        // line 274
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cantidadminimaaceptadaenteros"))), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cantidadminimaaceptadadecimales"))), "html", null, true);
        echo " padgbott\">
                                    <label class=\"col-sm-6\">";
        // line 275
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Indica la cantidad mínima en euros por la que estás dispuesto a recibir ofertas"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 277
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cantidadminimaaceptadaenteros"));
        echo "
                                        ";
        // line 278
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "cantidadminimaaceptadaenteros"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                    <div class=\"col-sm-2\">
                                        ";
        // line 281
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cantidadminimaaceptadadecimales"));
        echo "
                                        ";
        // line 282
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "cantidadminimaaceptadadecimales"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>                    
                                </div>  
                                <div class=\"clearfix\"></div>

                                <div class=\"form-group\">
                                    <label class=\"col-sm-6\">";
        // line 288
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado de publicación de la ficha del hotel"), "html", null, true);
        echo "</label>
                                    <div class=\"col-sm-3\">
                                        ";
        // line 290
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "status"));
        echo "
                                        ";
        // line 291
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "status"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>  

                            </div>
                            <div id=\"imageneshotel\" class=\"tab-pane\">     
                                <div style=\"height:0px; display:none;\">
                                    ";
        // line 298
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "imagenes"), array("attr" => array("class" => "")));
        echo "   
                                </div>            
                                <div class=\"form-group ";
        // line 300
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "imagenes"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 301
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "imagenes"));
        echo "</label>
                                </div>  
                                <div id=\"imagenes\" style=\"height:15px;width:100%;\">
                                    <input type=\"file\" name=\"archivo\" id=\"inputsubida\">
                                    ";
        // line 308
        echo "                                </div>
                                <iframe a src=\"";
        // line 309
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "hoteles")), "html", null, true);
        echo "\" height=\"0\" width=\"1300\" style=\"border:0px;\" name=\"inter\"></iframe>
                                <div id=\"imagenessubidas\" style=\"height:auto;width:100%;background-color:white;\"> 
                                    <div class=\"row\">
                                        <div class=\"col-md-12\">
                                            <section class=\"panel tasks-widget\">

                                                <div class=\"panel-body\">
                                                    <div class=\"task-content\">
                                                        <ul id=\"sortable\" class=\"task-list\" >
                                                            ";
        // line 318
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "imagenes"));
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 319
            echo "                                                                <li class=\"list-primary borraelemento_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "id"), "html", null, true);
            echo "\" data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "id"), "html", null, true);
            echo "\"><i class=\" fa fa-ellipsis-v\"></i><div class=\"task-title\"><img src=\"/uploads/hoteles/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "imagen"), "html", null, true);
            echo "\" width=\"200\"><div class=\"pull-right hidden-phone\"><a href=\"#\"><button class=\"btn btn-success btn-xs fa fa-pencil\"></button></a>&nbsp;";
            echo "&nbsp;<a href=\"#\" onClick=\"deleteElement('";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "id"), "html", null, true);
            echo "');
                                                                        return false;\"><button class=\"btn btn-danger btn-xs fa fa-trash-o\"></button></a></div></div></li>
                                                                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 322
        echo "                                                        </ul>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>                    
                                </div>
                                <div id=\"imagenessubidasinputs\" style=\"display:none;height:0px;";
        // line 329
        echo "width:100%;background-color:white;\"> 
                                    ";
        // line 330
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "imagenes"));
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 331
            echo "                                        <input type=\"text\" name=\"imageneshotel[]\" class=\"borraelemento_";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "id"), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "id"), "html", null, true);
            echo "\"><br>
                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 333
        echo "                                </div>

                                <a class=\"\" data-toggle=\"modal\" href=\"#myModal\" id=\"clickmodal\"></a>


                                <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                                    <div class=\"modal-dialog\" style=\"width: 923px;margin: 36px auto;height: 508px!important;\">
                                        <div class=\"modal-content\">
                                            <div class=\"modal-header\">
                                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                                                <h4 class=\"modal-title\">";
        // line 343
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar imagen"), "html", null, true);
        echo "</h4>
                                            </div>

                                            <div style=\"width:100%; height:auto; padding-top: 17px;\" >
                                                <img src=\"/bundles/hotelesbackend/img/logo2.png\" id=\"target\" width=\"200\" alt=\"[Jcrop Example]\" />
                                            </div>

                                            <div class=\"clearfix\"></div>

                                            <div class=\"modal-footer\">
                                                <button data-dismiss=\"modal\" id=\"closeimage\" class=\"btn btn-default\" type=\"button\">";
        // line 353
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cerrar"), "html", null, true);
        echo "</button>
                                                <button class=\"btn btn-success\" id=\"successimage\" data-id=\"\" data-url=\"\" data-img=\"\" type=\"button\">";
        // line 354
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar cambios"), "html", null, true);
        echo "</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>          
                            <div id=\"seo\" class=\"tab-pane\">
                                <div class=\"form-group ";
        // line 363
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "seotitulo"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 364
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Título para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 366
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "seotitulo"));
        echo "
                                        ";
        // line 367
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "seotitulo"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 370
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "seodescripcion"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 371
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Descripción para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 373
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "seodescripcion"));
        echo "
                                        ";
        // line 374
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "seodescripcion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group ";
        // line 377
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "seokeywords"))), "html", null, true);
        echo "\">
                                    <label>";
        // line 378
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Keywords para SEO"), "html", null, true);
        echo "</label>
                                    <div>
                                        ";
        // line 380
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "seokeywords"));
        echo "
                                        ";
        // line 381
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "seokeywords"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                            </div>
                            ";
        // line 385
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 386
            echo "                                <div id=\"comisiones\" class=\"tab-pane\">
                                    <div class=\"form-group\">
                                        <label for=\"exampleInputEmail1\">";
            // line 388
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Porcentaje de comisión (campo enteros) , (campo decimales)."), "html", null, true);
            echo "</label>
                                        <div class=\"clearfix\"></div>
                                        <div class=\"col-sm-2-dcnt ";
            // line 390
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionhotelentero"))), "html", null, true);
            echo "\">
                                            ";
            // line 391
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionhotelentero"));
            echo "
                                            ";
            // line 392
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "comisionhotelentero"), array("attr" => array("class" => "form-control")));
            echo "                     
                                        </div>
                                        <div class=\"comaclear\" style=\"float:left\"> , </div>
                                        <div class=\"col-sm-2-dcnt ";
            // line 395
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionhoteldecimal"))), "html", null, true);
            echo "\">  
                                            ";
            // line 396
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionhoteldecimal"));
            echo "
                                            ";
            // line 397
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "comisionhoteldecimal"), array("attr" => array("class" => "form-control")));
            echo "                                  
                                        </div>
                                        <div class=\"clearfix\"></div>
                                    </div>
                                </div>
                            ";
        }
        // line 403
        echo "
                        </div>
                    </div>
                </section>
                <!--tab nav start-->

                ";
        // line 409
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "

                <section class=\"panel\">

                    <div class=\"panel-body\">


                        <button type=\"submit\" class=\"btn btn-success\">";
        // line 416
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear"), "html", null, true);
        echo "</button>

                        </form>



                        <a href=\"";
        // line 422
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel"), "html", null, true);
        echo "\" class=\"btn btn-primary\">
                            ";
        // line 423
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "
                        </a>
                    </div>
                </section>

            </div><!-- /col -->
        </div><!-- /row -->

    ";
    }

    // line 433
    public function block_jsextras($context, array $blocks = array())
    {
        // line 434
        echo "        ";
        $context["urlcrea"] = $this->env->getExtension('routing')->getPath("admin_hotel_create");
        // line 435
        echo "        ";
        $context["urlsubidaimagen"] = $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "hoteles"));
        // line 436
        echo "        ";
        $context["tipo"] = "hoteles";
        // line 437
        echo "        ";
        $context["crop1"] = "100";
        // line 438
        echo "        ";
        $context["crop2"] = "100";
        // line 439
        echo "        ";
        $context["crop3"] = "50";
        // line 440
        echo "        ";
        $context["crop4"] = "50";
        // line 441
        echo "        ";
        $context["crop5"] = "16";
        // line 442
        echo "        ";
        $context["crop6"] = "9";
        // line 443
        echo "
        ";
        // line 444
        $this->env->loadTemplate("HotelesBackendBundle:Crop:crop.html.twig")->display(array_merge($context, array("especifico" => "hoteles")));
        // line 445
        echo "

        <script src=\"/bundles/hotelesbackend/assets/dropzone/dropzone.js\"></script>
        <script src=\"/bundles/hotelesbackend/js/respond.min.js\" ></script>

    ";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Hotel:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1060 => 445,  1058 => 444,  1055 => 443,  1052 => 442,  1049 => 441,  1046 => 440,  1043 => 439,  1040 => 438,  1037 => 437,  1034 => 436,  1031 => 435,  1028 => 434,  1025 => 433,  1012 => 423,  1008 => 422,  999 => 416,  989 => 409,  981 => 403,  972 => 397,  968 => 396,  964 => 395,  958 => 392,  954 => 391,  950 => 390,  945 => 388,  941 => 386,  939 => 385,  932 => 381,  928 => 380,  923 => 378,  919 => 377,  913 => 374,  909 => 373,  904 => 371,  900 => 370,  894 => 367,  890 => 366,  885 => 364,  881 => 363,  869 => 354,  865 => 353,  852 => 343,  840 => 333,  829 => 331,  825 => 330,  822 => 329,  813 => 322,  796 => 319,  792 => 318,  780 => 309,  777 => 308,  770 => 301,  766 => 300,  761 => 298,  751 => 291,  747 => 290,  742 => 288,  733 => 282,  729 => 281,  723 => 278,  719 => 277,  714 => 275,  708 => 274,  701 => 270,  697 => 269,  691 => 266,  687 => 265,  682 => 263,  676 => 262,  670 => 259,  666 => 258,  661 => 256,  656 => 254,  652 => 253,  647 => 251,  641 => 248,  637 => 247,  632 => 245,  620 => 236,  613 => 232,  609 => 231,  604 => 229,  600 => 228,  594 => 225,  590 => 224,  585 => 222,  581 => 221,  575 => 218,  571 => 217,  566 => 215,  562 => 214,  556 => 211,  552 => 210,  547 => 208,  540 => 204,  536 => 203,  531 => 201,  525 => 198,  521 => 197,  516 => 195,  510 => 192,  506 => 191,  501 => 189,  494 => 185,  490 => 184,  485 => 182,  479 => 179,  475 => 178,  470 => 176,  464 => 173,  460 => 172,  455 => 170,  448 => 166,  444 => 165,  439 => 163,  432 => 159,  428 => 158,  423 => 156,  419 => 155,  413 => 152,  409 => 151,  404 => 149,  400 => 148,  391 => 142,  383 => 137,  379 => 136,  374 => 134,  370 => 133,  364 => 130,  360 => 129,  355 => 127,  351 => 126,  345 => 123,  341 => 122,  336 => 120,  332 => 119,  326 => 116,  322 => 115,  317 => 113,  313 => 112,  307 => 109,  303 => 108,  298 => 106,  294 => 105,  285 => 99,  281 => 98,  276 => 96,  272 => 95,  266 => 92,  262 => 91,  257 => 89,  253 => 88,  247 => 85,  243 => 84,  238 => 82,  234 => 81,  228 => 78,  224 => 77,  219 => 75,  215 => 74,  209 => 71,  205 => 70,  200 => 68,  196 => 67,  190 => 64,  186 => 63,  181 => 61,  174 => 57,  170 => 56,  165 => 54,  161 => 53,  151 => 45,  145 => 42,  140 => 41,  138 => 40,  133 => 38,  129 => 37,  124 => 35,  120 => 34,  115 => 32,  111 => 31,  106 => 29,  102 => 28,  97 => 26,  93 => 25,  86 => 21,  74 => 15,  71 => 14,  55 => 11,  51 => 10,  46 => 9,  43 => 8,  37 => 5,  32 => 4,  29 => 3,);
    }
}
