<?php

/* HotelesBackendBundle:Newsletter:status.html.twig */
class __TwigTemplate_f0fa56eec2f84e55b8d7691e675f5cbf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getContext($context, "activo") == 1)) {
            // line 2
            echo "    <i class=\"fa fa-check\"></i>
";
        } else {
            // line 4
            echo "    <i class=\"fa fa-times\"></i>
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Newsletter:status.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 4,  19 => 2,  17 => 1,  251 => 90,  237 => 78,  221 => 75,  211 => 71,  201 => 67,  199 => 66,  192 => 64,  188 => 63,  182 => 62,  178 => 61,  174 => 60,  170 => 59,  166 => 58,  163 => 57,  146 => 56,  139 => 52,  135 => 51,  131 => 50,  127 => 49,  123 => 48,  119 => 47,  115 => 46,  101 => 35,  92 => 29,  81 => 21,  73 => 15,  70 => 14,  54 => 10,  50 => 9,  45 => 8,  42 => 7,  36 => 5,  31 => 4,  28 => 3,);
    }
}
