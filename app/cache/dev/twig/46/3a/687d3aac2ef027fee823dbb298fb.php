<?php

/* HotelesFrontendBundle:Frontend:hotel.html.twig */
class __TwigTemplate_463a687d3aac2ef027fee823dbb298fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 5
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a38f972_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a38f972_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/a38f972_part_1_hotel_detalle_1.js");
            // line 6
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "a38f972"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a38f972") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/a38f972.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "    <div class=\"contenido hotel_detalle\">
        <div class=\"bqizq\">
            <div class=\"slideFotos\">
                <div class=\"pikachoose\" style=\"width:400px;\"> 
                    ";
        // line 15
        $context["imagen_hoteles_path"] = $this->getAttribute($this->getContext($context, "hotel"), "getImagePath");
        // line 16
        echo "                    <ul id=\"pikame\"> 
                        ";
        // line 17
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "hotel"), "getOrderedImagenesDelHotel"));
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 18
            echo "                            ";
            $context["ruta_imagen"] = (($this->getContext($context, "imagen_hoteles_path") . "/") . $this->getAttribute($this->getContext($context, "imagen"), "imagen"));
            // line 19
            echo "                            <li> 
                                <a href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->getContext($context, "ruta_imagen"), "html", null, true);
            echo "\"> <img src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "ruta_imagen"), "html", null, true);
            echo "\" style=\"width:400px;\"/> </a> 
                            </li> 
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 22
        echo "                                     
                    </ul>                     
                </div> 
            </div><!-- slide -->

            <div class=\"clearfix\"></div>
            <div class=\"direccion\">
                ";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "ubicacion"), "html", null, true);
        echo "
            </div><!-- /dirección -->

        </div><!-- /bqizq -->
        <div class=\"bqder\">
            <div class=\"hotel_cont clearfix\">
                <h2 class=\"tit_hotel\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "nombrehotel"), "html", null, true);
        echo "</h2>
                <div class=\"estrellashotel\"><img src=\"/bundles/hotelesfrontend/img/";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "calificacion"), "html", null, true);
        echo "stars.png\"></div>
                <div class=\"clearfix\"></div>
                <ul class=\"servicios clearfix\">
                    ";
        // line 39
        if (($this->getAttribute($this->getContext($context, "hotel"), "piscina") == 1)) {
            // line 40
            echo "                        <li class=\"piscina\"> Piscina
                        </li>
                    ";
        }
        // line 43
        echo "
                    ";
        // line 44
        if (($this->getAttribute($this->getContext($context, "hotel"), "spa") == 1)) {
            // line 45
            echo "                        <li class=\"spa\">Spa
                        </li>
                    ";
        }
        // line 48
        echo "
                    ";
        // line 49
        if (($this->getAttribute($this->getContext($context, "hotel"), "wiFi") == 1)) {
            // line 50
            echo "                        <li class=\"wi-fi\">Wi-fi
                        </li>        
                    ";
        }
        // line 53
        echo "
                    ";
        // line 54
        if (($this->getAttribute($this->getContext($context, "hotel"), "accesoAdaptado") == 1)) {
            // line 55
            echo "                        <li class=\"acceso-adaptado\">Acceso adaptado
                        </li>
                    ";
        }
        // line 58
        echo "
                    ";
        // line 59
        if (($this->getAttribute($this->getContext($context, "hotel"), "aceptanPerros") == 1)) {
            // line 60
            echo "                        <li class=\"perros\">Aceptan perros
                        </li>
                    ";
        }
        // line 63
        echo "
                    ";
        // line 64
        if (($this->getAttribute($this->getContext($context, "hotel"), "aparcamiento") == 1)) {
            // line 65
            echo "                        <li class=\"parking\">Aparcamiento/Parking
                        </li>
                    ";
        }
        // line 68
        echo "
                    ";
        // line 69
        if (($this->getAttribute($this->getContext($context, "hotel"), "businessCenter") == 1)) {
            // line 70
            echo "                        <li class=\"business-center\">Business center
                        </li>
                    ";
        }
        // line 73
        echo "                </ul>
            </div>
            <div class=\"info\">
                ";
        // line 77
        echo "                <p><b>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Descripción"), "html", null, true);
        echo "</b></p>
                <p>";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "descripcion"), "html", null, true);
        echo "</p>
                <!-- <input class=\"timbre\" name=\"hotel1\" type=\"checkbox\"> -->
                <p><b>";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Características"), "html", null, true);
        echo "</b></p>
                <p>";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "caracteristicas"), "html", null, true);
        echo "</p>
                <a href=\"#\" class=\"btn_back\" onclick=\"window.history.back();return false;\"><i class=\"icon_back\"></i>Volver a la búsqueda</a>
            </div>
        </div><!-- /bqder -->
        <div class=\"clearfix\"></div>
        <div class=\"mapa-detalle\">
            <div id=\"mapa_det\" style=\"height:290px;\" data-longitud=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "longitud"), "html", null, true);
        echo "\" data-latitud=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "latitud"), "html", null, true);
        echo "\" data-titulo=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "nombrehotel"), "html", null, true);
        echo "\"></div>
        </div><!-- /mapa -->
    </div>

";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:hotel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 87,  204 => 81,  200 => 80,  195 => 78,  190 => 77,  185 => 73,  180 => 70,  178 => 69,  175 => 68,  170 => 65,  168 => 64,  165 => 63,  160 => 60,  158 => 59,  155 => 58,  150 => 55,  148 => 54,  145 => 53,  140 => 50,  138 => 49,  135 => 48,  130 => 45,  128 => 44,  125 => 43,  120 => 40,  118 => 39,  112 => 36,  108 => 35,  99 => 29,  90 => 22,  79 => 20,  76 => 19,  73 => 18,  69 => 17,  66 => 16,  64 => 15,  58 => 11,  55 => 10,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
