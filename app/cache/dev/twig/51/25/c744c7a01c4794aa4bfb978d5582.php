<?php

/* HotelesFrontendBundle:Frontend:listado.mv.twig */
class __TwigTemplate_5125c744c7a01c4794aa4bfb978d5582 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_javascripts($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 4
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "4146cbf_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4146cbf_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4146cbf_listado.mv_1.js");
            // line 6
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
            // asset "4146cbf_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4146cbf_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4146cbf_ajax-preload_2.js");
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "4146cbf"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_4146cbf") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/4146cbf.js");
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 10
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    ";
        // line 11
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "f7a92ea_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a92ea_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/f7a92ea_minimal.mv_1.css");
            echo "       
    <link rel=\"stylesheet\" href=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\">
    ";
        } else {
            // asset "f7a92ea"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_f7a92ea") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/f7a92ea.css");
            // line 11
            echo "       
    <link rel=\"stylesheet\" href=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\">
    ";
        }
        unset($context["asset_url"]);
    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        // line 16
        echo "    <!-- nav-options -->
    <div class=\"opciones\" style=\"display:none;\" id=\"nav-options\">
        <a class=\"btn_volver\"> < VOLVER AL LISTADO</a>
        ";
        // line 19
        $context["lugarconstruido"] = "";
        // line 20
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "lugar"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["dato"]) {
            // line 21
            echo "            ";
            $context["lugarconstruido"] = ($this->getContext($context, "lugarconstruido") . $this->getContext($context, "dato"));
            // line 22
            echo "            ";
            if (($this->getAttribute($this->getContext($context, "loop"), "index") < twig_length_filter($this->env, $this->getContext($context, "lugar")))) {
                // line 23
                echo "                ";
                $context["lugarconstruido"] = ($this->getContext($context, "lugarconstruido") . "/");
                // line 24
                echo "            ";
            }
            // line 25
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dato'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 26
        echo "        ";
        $context["basic_url"] = ((($this->getAttribute($this->getContext($context, "app"), "environment") == "dev")) ? ("/app_dev.php") : (""));
        // line 27
        echo "        <div id=\"tabsWithStyle\" class=\"style-tabs\">
            <ul>
                <li>
                    <a href=\"#tabs-1\">
                        <span class=\"triangle_active\">
                        </span>
                        <div class=\"icon buscador-icon\">
                        </div>
                    </a>
                </li>
                <li>
                    <a href=\"#tabs-2\">
                        <span class=\"triangle_active\"></span>
                        <div class=\"icon star-icon\"></div>
                    </a>
                </li>
                <li>
                    <a href=\"#tabs-3\">
                        <span class=\"triangle_active\"></span>
                        <div class=\"icon servicios-icon\"></div>
                    </a>
                </li>
                <li class=\"last\">
                    <a href=\"#tabs-4\">
                        <span class=\"triangle_active\"></span>
                        <div class=\"icon listado-icon\"></div>
                    </a>
                </li>
            </ul>
            <div class=\"clearfix\"></div>
            <div id=\"tabs-1\">


                <form class=\"form_portada clearfix\" action=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->getContext($context, "basic_url"), "html", null, true);
        echo "/listadoajax/";
        echo twig_escape_filter($this->env, (((($this->getContext($context, "lugarconstruido") . "/") . $this->getContext($context, "fechain")) . "/") . $this->getContext($context, "fechaout")), "html", null, true);
        echo "\" method=\"post\" id=\"formajax\">

                    <select id=\"select-beast\" class=\"selec_ciudad\" placeholder=\"\" name=\"ciudad\">
                        <option value=\"\">Destino</option>
                        ";
        // line 64
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "arraydelugares"));
        foreach ($context['_seq'] as $context["_key"] => $context["dato"]) {
            // line 65
            echo "                            ";
            $context["seleccionado"] = $this->env->getExtension('twig_extension')->select($this->getContext($context, "lugarconstruido"), $this->getContext($context, "dato"));
            // line 66
            echo "                            <option value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "dato"), "html", null, true);
            echo "\" ";
            echo twig_escape_filter($this->env, $this->getContext($context, "seleccionado"), "html", null, true);
            echo ">";
            echo twig_escape_filter($this->env, $this->getContext($context, "dato"), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dato'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 67
        echo "                            
                    </select>
                    <div class=\"fecha_picker\">
                        <input type=\"text\" id=\"fromDate\" name=\"fromDate\" class=\"input_mov ll-skin-lug\"/>
                        <input type=\"text\" id=\"toDate\" name=\"toDate\"/>
                        <script>
                            ROOMASTIC.fecha_in = '";
        // line 73
        echo twig_escape_filter($this->env, $this->getContext($context, "fechain"), "html", null, true);
        echo "';
                            ROOMASTIC.fecha_out = '";
        // line 74
        echo twig_escape_filter($this->env, $this->getContext($context, "fechaout"), "html", null, true);
        echo "';
                        </script>
                    </div>

                    <input type=\"submit\" id=\"buscar\" name=\"buscar\" value=\"buscar hoteles\">

                </form>
            </div>
            <div id=\"tabs-2\">
                <ul class=\"estrellas\">
                    <li class=\"star1\">
                        <input type=\"checkbox\" class=\"ajaxupdate left  checkbox-stars\" name=\"star1\" id=\"star1\" checked data-stars=\"1\" data-filter=\"stars-1\">
                        <label for=\"star1\">Hoteles de <span>1</span> estrella</label>
                    </li>
                    <li class=\"star2\">
                        <input type=\"checkbox\" class=\"ajaxupdate left checkbox-stars\" name=\"star2\" id=\"star2\" checked data-stars=\"2\" data-filter=\"stars-2\">
                        <label for=\"star2\">Hoteles de <span>2</span> estrellas</label>
                    </li>
                    <li class=\"star3\">
                        <input type=\"checkbox\" class=\"ajaxupdate left checkbox-stars\" name=\"star3\" id=\"star3\" checked data-stars=\"3\" data-filter=\"stars-3\">
                        <label for=\"star3\">Hoteles de <span>3</span> estrellas</label>
                    </li>
                    <li class=\"star4\">
                        <input type=\"checkbox\" class=\"ajaxupdate left checkbox-stars\" name=\"star4\" id=\"star4\" checked data-stars=\"4\" data-filter=\"stars-4\">
                        <label for=\"star4\">Hoteles de <span>4</span> estrellas</label>
                    </li>
                    <li class=\"star5\">
                        <input type=\"checkbox\" class=\"ajaxupdate left checkbox-stars\" name=\"star5\" id=\"star5\" checked data-stars=\"5\" data-filter=\"stars-5\">
                        <label for=\"star5\">Hoteles de <span>5</span> estrellas</label>
                    </li>
                </ul>
            </div>
            <div id=\"tabs-3\">
                <ul class=\"checkboxes servicios_sel\">
                    <li class=\"piscina\">
                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios left\" name=\"piscina\" id=\"piscina\" checked data-filter=\"servicio-piscina\">
                        <label for=\"piscina\">Piscina</label>
                    </li>
                    <li class=\"spa\">
                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios left\" name=\"spa\" id=\"Spa\" checked data-filter=\"servicio-spa\">
                        <label for=\"Spa\">Spa</label>
                    </li>
                    <li class=\"wi-fi\">
                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios left\" name=\"wiFi\" id=\"Wi-fi\" checked data-filter=\"servicio-wi-fi\">
                        <label for=\"Bar\">Wi-Fi</label>
                    </li>
                    <li class=\"acceso-adaptado\">
                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios left\" name=\"accesoAdaptado\" id=\"Acceso-Adaptado\" checked data-filter=\"servicio-acceso-adaptado\">
                        <label for=\"Acceso-Adaptado\">Acceso adaptado</label>
                    </li>
                    <li class=\"perros\">
                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios left\" name=\"aceptanPerros\" id=\"perros\" checked data-filter=\"servicio-aceptan-perros\">
                        <label for=\"perros\">Aceptan perros</label>
                    </li>
                    <li class=\"parking\">
                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios left\" name=\"aparcamiento\" id=\"parking\" checked data-filter=\"servicio-aparcamiento\">
                        <label for=\"parking\">Aparcamiento/Parking</label>
                    </li>
                    <li class=\"business-center\">
                        <input type=\"checkbox\" class=\"ajaxupdate checkbox-lateral checkbox-servicios left\" name=\"businessCenter\" id=\"business-center\" checked data-filter=\"servicio-business-center\">
                        <label for=\"business-center\">Business center</label>
                    </li>
                </ul>
            </div>
            <div id=\"tabs-4\">
                <ul class=\"hoteles\" id=\"listado-lateral-hoteles\">
                    ";
        // line 140
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 141
            echo "                        <li class=\"hotel_li hotel-selected\"  style=\"display: none;\" id=\"hotel-checkbox-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\" data-hotel-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">
                            <input type=\"checkbox\" class=\"checkbox-lateral\" id=\"";
            // line 142
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "\">
                            <label for=\"";
            // line 143
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "</label>
                        </li>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 146
        echo "                </ul>
            </div>
        </div><!-- /style-tabs -->

    </div>
    <!-- /nav-options -->

    <!-- content listado -->
    <div class=\"content_listado\">
        <!-- btn hacer oferta -->
        <a class=\"hacer_oferta\" href=\"";
        // line 156
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_oferta"), "html", null, true);
        echo "\" title=\"\" style=\"\" id=\"hacer-oferta\">hacer oferta <span class=\"decoration\"></span></a>
        <!-- /btn hacer oferta -->

        <a href=\"#\" class=\"btn_opciones\" id=\"nav-options-open\">ver opciones</a>

        <!-- hotel rtdo. -->
        ";
        // line 162
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 163
            echo "            <div class=\"singlehotel stars-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "calificacion"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "filtroServicios"), "html", null, true);
            echo "\">
                <div class=\"visual left\">
                    <!-- img -->
                    ";
            // line 166
            if ((twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "objetohotel"), "imagenes")) > 0)) {
                // line 167
                echo "                        <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_detallehotel", array("nombrehotel" => $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "url"))), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver detalles del hotel"), "html", null, true);
                echo "\" >
                            <img height=\"152\" width=\"215\" src=\"/uploads/hoteles/";
                // line 168
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "imagenes"), 0, array(), "array"), "imagen"), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver detalles del hotel"), "html", null, true);
                echo "\">
                        </a>
                    ";
            }
            // line 171
            echo "                    <!-- estrellas -->
                    <ul class=\"estrellas left\">
                        ";
            // line 173
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "calificacion")));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 174
                echo "                            <li class=\"star\">
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 177
            echo "                        <div class=\"clearfix\"></div>
                    </ul>
                </div> 
                <div class=\"dates left\">

                    <h2 class=\"name tit_hotel\"><a >";
            // line 182
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "</a></h2>
                    <!-- servicios -->
                    <ul class=\"servicios\">
                        ";
            // line 185
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "piscina") == 1)) {
                // line 186
                echo "                            <li class=\"piscina\">Piscina
                            </li>
                        ";
            }
            // line 189
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "spa") == 1)) {
                // line 190
                echo "                            <li class=\"spa\">Spa
                            </li>
                        ";
            }
            // line 193
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "wiFi") == 1)) {
                // line 194
                echo "                            <li class=\"wi-fi\">Wi-fi
                            </li>
                        ";
            }
            // line 197
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "accesoAdaptado") == 1)) {
                // line 198
                echo "                            <li class=\"acceso-adaptado\">Acceso adaptado
                            </li>
                        ";
            }
            // line 201
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "aceptanPerros") == 1)) {
                // line 202
                echo "                            <li class=\"perros\">Aceptan perros
                            </li>
                        ";
            }
            // line 205
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "aparcamiento") == 1)) {
                // line 206
                echo "                            <li class=\"parking\">Aparcamiento/Parking
                            </li>
                        ";
            }
            // line 209
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "businessCenter") == 1)) {
                // line 210
                echo "                            <li class=\"business-center\">Business center
                            </li>
                        ";
            }
            // line 213
            echo "                        <div class=\"clearfix\"></div>
                    </ul>
                    <!-- icheckbox_timbre -->
                    <input class=\"icheckbox_timbre timbre\" name=\"hotel1\" type=\"checkbox\" data-hotel-id=\"";
            // line 216
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">
                </div> 
                <div class=\"clearfix\"></div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 221
        echo "        ";
        $this->env->loadTemplate("HotelesBackendBundle:Extras:preload.html.twig")->display($context);
        // line 222
        echo "
        ";
        // line 224
        echo "        <div id=\"infinite-scroll-ajax\" data-last-page=\"1\"/>
        <!-- hotel rtdo. -->
    </div>
    <!-- /content_hotel -->
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listado.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  479 => 224,  476 => 222,  473 => 221,  462 => 216,  457 => 213,  452 => 210,  449 => 209,  444 => 206,  441 => 205,  436 => 202,  433 => 201,  428 => 198,  425 => 197,  420 => 194,  417 => 193,  412 => 190,  409 => 189,  404 => 186,  402 => 185,  396 => 182,  389 => 177,  381 => 174,  377 => 173,  373 => 171,  365 => 168,  358 => 167,  356 => 166,  347 => 163,  343 => 162,  334 => 156,  322 => 146,  311 => 143,  307 => 142,  300 => 141,  296 => 140,  227 => 74,  223 => 73,  215 => 67,  202 => 66,  199 => 65,  195 => 64,  186 => 60,  151 => 27,  148 => 26,  134 => 25,  131 => 24,  128 => 23,  125 => 22,  122 => 21,  104 => 20,  102 => 19,  97 => 16,  94 => 15,  86 => 12,  83 => 11,  76 => 12,  70 => 11,  65 => 10,  62 => 9,  40 => 6,  36 => 4,  31 => 3,  28 => 2,);
    }
}
