<?php

/* HotelesBackendBundle:UserHotel:new.html.twig */
class __TwigTemplate_323cfe29a481ae251d5eef7f3daa47a9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear nuevo usuario hotel"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">
            <section class=\"panel\"> 
                <header class=\"panel-heading\">
                    ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear nuevo usuario hotel"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <form action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_create"), "html", null, true);
        echo "\" method=\"post\"  id=\"formulariosubida\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">
                        ";
        // line 19
        echo "
                        <div class=\"form-group ";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "hotel"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 23
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "hotel"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "email"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 29
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombrecompleto"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 35
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "nombrecompleto"), array("attr" => array("class" => "form-control")));
        echo "                    
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cargo"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 41
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "cargo"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        ";
        // line 45
        echo "                        <div class=\"form-group \">
                            <label for=\"exampleInputEmail1\">";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imagen de usuario"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                <input type=\"file\" name=\"archivo\" id=\"inputsubida\">
                            </div>
                            <br>
                            <input type=\"hidden\" name=\"imagensubidanombre\" id=\"imagensubidanombre\" value=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->getContext($context, "imagendevuelta"), "html", null, true);
        echo "\">
                            ";
        // line 52
        if (($this->getContext($context, "imagendevuelta") != "")) {
            // line 53
            echo "                                <img src=\"/uploads/user/";
            echo twig_escape_filter($this->env, $this->getContext($context, "imagendevuelta"), "html", null, true);
            echo "\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                            ";
        } else {
            // line 55
            echo "                                <img src=\"\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                            ";
        }
        // line 57
        echo "                            <iframe src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user")), "html", null, true);
        echo "\" height=\"0\" width=\"1300\" style=\"border:0px;\" name=\"inter\"></iframe>
                        </div>
                        ";
        // line 59
        if (($this->getContext($context, "show_admin") == true)) {
            // line 60
            echo "                            <div class=\"form-group ";
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "administrador"))), "html", null, true);
            echo "\">
                                <label>
                                    ";
            // line 62
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Marca esta casilla si quieres que este usuario administre la ficha del hotel"), "html", null, true);
            echo " 
                                    ";
            // line 63
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "administrador"), array("attr" => array("class" => "")));
            echo "                 
                                </label>
                            </div>
                        ";
        }
        // line 67
        echo "                        ";
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "

                </div>
            </section>
            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-success\">";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    <a href=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->



    ";
        // line 86
        echo "    ";
        // line 87
        echo "    ";
        // line 88
        echo "    ";
        // line 89
        echo "

    <a class=\"\" data-toggle=\"modal\" href=\"#myModal\" id=\"clickmodal\"></a>


    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" style=\"width: 923px;margin: 36px auto;height: 508px!important;\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\">";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar imagen"), "html", null, true);
        echo "</h4>
                </div>

                <div style=\"width:100%; height:auto; padding-top: 17px;\" >
                    <img src=\"/bundles/hotelesbackend/img/logo2.png\" id=\"target\" width=\"200\" alt=\"[Jcrop Example]\" />
                </div>

                <div class=\"clearfix\"></div>

                <div class=\"modal-footer\">
                    <button data-dismiss=\"modal\" id=\"closeimage\" class=\"btn btn-default\" type=\"button\">";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cerrar"), "html", null, true);
        echo "</button>
                    <button class=\"btn btn-success\" id=\"successimage\" data-id=\"\" data-url=\"\" data-img=\"\" type=\"button\">";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar cambios"), "html", null, true);
        echo "</button>
                </div>

            </div>
        </div>
    </div>

";
    }

    // line 120
    public function block_jsextras($context, array $blocks = array())
    {
        // line 121
        echo "
    ";
        // line 122
        $context["urlcrea"] = $this->env->getExtension('routing')->getPath("admin_userhotel_create");
        // line 123
        echo "    ";
        $context["urlsubidaimagen"] = $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user"));
        // line 124
        echo "    ";
        $context["tipo"] = "user";
        // line 125
        echo "    ";
        $context["crop1"] = "100";
        // line 126
        echo "    ";
        $context["crop2"] = "100";
        // line 127
        echo "    ";
        $context["crop3"] = "100";
        // line 128
        echo "    ";
        $context["crop4"] = "100";
        // line 129
        echo "    ";
        $context["crop5"] = "1";
        // line 130
        echo "    ";
        $context["crop6"] = "1";
        // line 131
        echo "
    ";
        // line 132
        $this->env->loadTemplate("HotelesBackendBundle:Crop:crop.html.twig")->display(array_merge($context, array("especifico" => "user")));
        // line 133
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:UserHotel:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  293 => 133,  291 => 132,  288 => 131,  285 => 130,  282 => 129,  279 => 128,  276 => 127,  273 => 126,  270 => 125,  267 => 124,  264 => 123,  262 => 122,  259 => 121,  256 => 120,  244 => 110,  240 => 109,  227 => 99,  215 => 89,  213 => 88,  211 => 87,  209 => 86,  195 => 76,  190 => 74,  179 => 67,  172 => 63,  168 => 62,  162 => 60,  160 => 59,  154 => 57,  150 => 55,  144 => 53,  142 => 52,  138 => 51,  130 => 46,  127 => 45,  121 => 41,  116 => 39,  112 => 38,  106 => 35,  101 => 33,  97 => 32,  91 => 29,  86 => 27,  82 => 26,  76 => 23,  71 => 21,  67 => 20,  64 => 19,  58 => 17,  52 => 14,  45 => 9,  42 => 8,  36 => 5,  31 => 4,  28 => 3,);
    }
}
