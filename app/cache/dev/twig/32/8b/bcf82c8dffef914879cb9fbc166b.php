<?php

/* HotelesBackendBundle:User:profile-extended.html.twig */
class __TwigTemplate_328bbcf82c8dffef914879cb9fbc166b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getAttribute($this->getContext($context, "user"), "getTypeOf") == "admin")) {
            // line 2
            echo "    <tr>
        <th>";
            // line 3
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre publico"), "html", null, true);
            echo "</th>
        <td>";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "getPublicUsername"), "html", null, true);
            echo "</td>
    </tr>
";
        } elseif (($this->getAttribute($this->getContext($context, "user"), "getTypeOf") == "hotel")) {
            // line 7
            echo "    <tr>
        <th>";
            // line 8
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
            echo "</th>
        <td>";
            // line 9
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "getNombreCompleto"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administrador"), "html", null, true);
            echo "</th>
        <td>
            ";
            // line 14
            if ($this->getAttribute($this->getContext($context, "user"), "isAdmin")) {
                // line 15
                echo "                ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si"), "html", null, true);
                echo " 
            ";
            } else {
                // line 17
                echo "                ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No"), "html", null, true);
                echo "
            ";
            }
            // line 19
            echo "        </td>
    </tr>
    <tr>
        <th>";
            // line 22
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
            echo "</th>
        <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "user"), "hotel"), "nombrehotel"), "html", null, true);
            echo "</td>
    </tr>
";
        } elseif (($this->getAttribute($this->getContext($context, "user"), "getTypeOf") == "empresa")) {
            // line 26
            echo "    <tr>
        <th>";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
            echo "</th>
        <td>";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "getNombreCompleto"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
            echo "</th>
        <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "user"), "empresa"), "nombreempresa"), "html", null, true);
            echo "</td>
    </tr>
";
        } elseif (($this->getAttribute($this->getContext($context, "user"), "getTypeOf") == "basico")) {
            // line 35
            echo "    <tr>
        <th>";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
            echo "</th>
        <td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "getNombreCompleto"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 40
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("DNI"), "html", null, true);
            echo "</th>
        <td>";
            // line 41
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "dni"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Direccion"), "html", null, true);
            echo "</th>
        <td>";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "direccioncompleta"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
            echo "</th>
        <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "telefono"), "html", null, true);
            echo "</td>
    </tr>
    <tr>
        <th>";
            // line 52
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Origen"), "html", null, true);
            echo "</th>
        <td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "origin"), "html", null, true);
            echo "</td>
    </tr>
";
        } else {
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:User:profile-extended.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 53,  145 => 52,  139 => 49,  129 => 45,  125 => 44,  119 => 41,  115 => 40,  109 => 37,  105 => 36,  102 => 35,  96 => 32,  86 => 28,  82 => 27,  79 => 26,  73 => 23,  69 => 22,  64 => 19,  58 => 17,  52 => 15,  50 => 14,  45 => 12,  39 => 9,  32 => 7,  26 => 4,  22 => 3,  19 => 2,  17 => 1,  157 => 61,  154 => 60,  146 => 58,  138 => 56,  135 => 48,  132 => 54,  124 => 52,  116 => 50,  114 => 49,  108 => 48,  94 => 36,  92 => 31,  87 => 33,  83 => 32,  78 => 29,  76 => 28,  72 => 27,  66 => 24,  62 => 23,  51 => 15,  44 => 10,  41 => 9,  35 => 8,  30 => 4,  27 => 3,);
    }
}
