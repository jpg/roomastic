<?php

/* HotelesBackendBundle:User:show.html.twig */
class __TwigTemplate_25505a80c9a110590d759d1d73856d75 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuario"), "html", null, true);
        echo "
";
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuario"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body \">
                    <div class=\"adv-table\">
                        <!--  contentTabla -->
                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-advance table-hover\" id=\"hidden-table-info\">
                            <tbody>
                                <tr>
                                    <th>";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</th>
                                    <td>";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "email"), "html", null, true);
        echo "</td>
                                </tr>
                                <tr>
                                    <th>";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado"), "html", null, true);
        echo "</th>
                                    <td>";
        // line 28
        $this->env->loadTemplate("HotelesBackendBundle:User:status.html.twig")->display(array_merge($context, array("status" => $this->getAttribute($this->getContext($context, "user"), "status"))));
        // line 29
        echo "                                    </td>
                                </tr>
                                <tr>
                                    <th>";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Último login"), "html", null, true);
        echo "</th>
                                    <td>";
        // line 33
        if ((!(null === $this->getAttribute($this->getContext($context, "user"), "lastLogin")))) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "lastLogin"), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
                                </tr>
                                ";
        // line 35
        $this->env->loadTemplate("HotelesBackendBundle:User:profile-extended.html.twig")->display(array_merge($context, array("user" => $this->getContext($context, "user"))));
        // line 36
        echo "
                            </tbody>

                        </table>
                        <!--  /contentTabla -->

                    </div> <!-- /addtable -->
                </div> 
            </section>
            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <a href=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
                    ";
        // line 49
        if (($this->getAttribute($this->getContext($context, "user"), "isSuspended") == false)) {
            // line 50
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_disabled", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suspender a este usuario"), "html", null, true);
            echo "</a>
                    ";
        } else {
            // line 52
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_enabled", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Quitar suspensión a este usuario"), "html", null, true);
            echo "</a>
                    ";
        }
        // line 54
        echo "                    ";
        if (($this->getAttribute($this->getContext($context, "user"), "getTypeOf") == "basico")) {
            // line 55
            echo "                        ";
            if (($this->getAttribute($this->getContext($context, "user"), "isBanned") == false)) {
                // line 56
                echo "                            <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_ban", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                echo "\" class=\"btn btn-primary\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Banear a este usuario"), "html", null, true);
                echo "</a>
                        ";
            } else {
                // line 58
                echo "                            <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_unban", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                echo "\" class=\"btn btn-primary\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Quitar baneo a este usuario"), "html", null, true);
                echo "</a>
                        ";
            }
            // line 60
            echo "                    ";
        }
        // line 61
        echo "                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->

";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:User:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 61,  156 => 60,  148 => 58,  140 => 56,  137 => 55,  134 => 54,  126 => 52,  118 => 50,  116 => 49,  110 => 48,  96 => 36,  94 => 35,  87 => 33,  83 => 32,  78 => 29,  76 => 28,  72 => 27,  66 => 24,  62 => 23,  51 => 15,  44 => 10,  41 => 9,  35 => 5,  30 => 4,  27 => 3,);
    }
}
