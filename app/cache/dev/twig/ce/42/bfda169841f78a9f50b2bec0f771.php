<?php

/* HotelesBackendBundle:Landing:index.html.twig */
class __TwigTemplate_ce42bfda169841f78a9f50b2bec0f771 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 5
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "171e67e_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_171e67e_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/171e67e_dashboard_1.js");
            // line 6
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "171e67e"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_171e67e") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/171e67e.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        // line 10
        echo "    <!--main content start-->
    <script>
        if (typeof ROOMASTIC === 'undefined') {
            ROOMASTIC = {};
        }
        ROOMASTIC.dashboard = {};
        ROOMASTIC.dashboard.header = {};

        ROOMASTIC.dashboard.header.count_ofertas_recibidas = ";
        // line 18
        echo twig_escape_filter($this->env, $this->getContext($context, "ofertas_recibidas_30_dias"), "html", null, true);
        echo ";
                ROOMASTIC.dashboard.header.count_ofertas_aceptadas = ";
        // line 19
        echo twig_escape_filter($this->env, $this->getContext($context, "ofertas_aceptadas_30_dias"), "html", null, true);
        echo ";
                ROOMASTIC.dashboard.header.count_ofertas_pagadas = ";
        // line 20
        echo twig_escape_filter($this->env, $this->getContext($context, "reservas_efectuadas_30_dias"), "html", null, true);
        echo ";
                ROOMASTIC.dashboard.header.count_ofertas_minimas = ";
        // line 21
        echo twig_escape_filter($this->env, $this->getContext($context, "ofertas_debajo_minimo"), "html", null, true);
        echo ";</script>

    <section class=\"wrapper\">
        <!--state overview start-->
        <div class=\"row state-overview\">
            <div class=\"col-lg-3 col-sm-6\">
                <section class=\"panel\">
                    <div class=\"symbol terques\">
                        <i class=\"fa fa-user\"></i>
                    </div>
                    <div class=\"value\">
                        <h1 id=\"count_ofertas_recibidas\">
                            0
                        </h1>
                        <p>";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas recibidas en los últimos 30 días"), "html", null, true);
        echo "</p>
                    </div>
                </section>
            </div>
            <div class=\"col-lg-3 col-sm-6\">
                <section class=\"panel\">
                    <div class=\"symbol red\">
                        <i class=\"fa fa-tags\"></i>
                    </div>
                    <div class=\"value\">
                        <h1 id=\"count_ofertas_aceptadas\">
                            0
                        </h1>
                        <p>";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas aceptadas en los últimos 30 días"), "html", null, true);
        echo "</p>
                    </div>
                </section>
            </div>
            <div class=\"col-lg-3 col-sm-6\">
                <section class=\"panel\">
                    <div class=\"symbol yellow\">
                        <i class=\"fa fa-shopping-cart\"></i>
                    </div>
                    <div class=\"value\">
                        <h1 id=\"count_ofertas_pagadas\">
                            0
                        </h1>
                        <p>";
        // line 61
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Reservas efectuadas en los últimos 30 días"), "html", null, true);
        echo "</p>
                    </div>
                </section>
            </div>
            <div class=\"col-lg-3 col-sm-6\">
                <section class=\"panel\">
                    <div class=\"symbol blue\">
                        <i class=\"fa fa-bar-chart-o\"></i>
                    </div>
                    <div class=\"value\">
                        <h1 id=\"count_ofertas_minimas\">
                            0
                        </h1>
                        <p>";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas por debajo del minimo en los últimos 30 días"), "html", null, true);
        echo "</p>
                    </div> 
                </section>
            </div>
        </div>
        <!--state overview end-->

        <div class=\"row\">
            <div class=\"col-lg-8\">
                <!--custom chart start-->
                <div class=\"border-head\">
                    <h3>";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ofertas recibidas por mes en el último año"), "html", null, true);
        echo "</h3>
                </div>
                <div class=\"custom-bar-chart\" id=\"custom-bar-chart\" data-max-value=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->getContext($context, "num_ofertas_anio_mes_max"), "html", null, true);
        echo "\">
                    <ul class=\"y-axis\">
                        ";
        // line 89
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "rango_y"));
        foreach ($context['_seq'] as $context["_key"] => $context["step"]) {
            // line 90
            echo "                            <li>
                                <span>";
            // line 91
            echo twig_escape_filter($this->env, $this->getContext($context, "step"), "html", null, true);
            echo "</span>
                            </li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['step'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 94
        echo "                    </ul>
                    ";
        // line 95
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "num_ofertas_anio_mes"));
        $context['_iterated'] = false;
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["ofertas_por_mes"]) {
            // line 96
            echo "                        <div class=\"bar\">
                            <div class=\"title\">
                                ";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "nombre_meses"), $this->getAttribute($this->getContext($context, "loop"), "index0"), array(), "array"), "html", null, true);
            echo "
                            </div>
                            <div class=\"value tooltips\" data-original-title=\"ofertas_por_mes\" data-toggle=\"tooltip\" data-placement=\"top\">
                                ";
            // line 101
            echo twig_escape_filter($this->env, $this->getContext($context, "ofertas_por_mes"), "html", null, true);
            echo "
                            </div>
                        </div>
                        ";
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 105
            echo "                            <script>
                                        console.log('error');
                            </script>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['ofertas_por_mes'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 109
        echo "
                            </div>
                            <!--custom chart end-->
                        </div>
                        <div class=\"col-lg-4\">
                            <!--new earning start-->
                            <div class=\"panel terques-chart\">
                                <div class=\"panel-body chart-texture\">
                                    <div class=\"chart\">
                                        <div class=\"heading\">
                                            <span id=\"titulo-periodo\" data-titulo-mes=\"";
        // line 119
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total último mes"), "html", null, true);
        echo "\" data-titulo-semana=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total última semana"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total último mes"), "html", null, true);
        echo "</span>
                                            <strong id=\"sum-ofertas-periodo\">";
        // line 120
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getContext($context, "sum_ofertas_mes"), 2, ",", "."), "html", null, true);
        echo " €</strong>
                                        </div>
                                        <div class=\"sparkline grafico-facturacion\" 
                                             data-type=\"line\" 
                                             data-resize=\"true\" 
                                             data-height=\"75\" 
                                             data-width=\"90%\" 
                                             data-line-width=\"1\" 
                                             data-line-color=\"#fff\" 
                                             data-spot-color=\"#fff\" 
                                             data-fill-color=\"\" 
                                             data-highlight-line-color=\"#fff\" 
                                             data-spot-radius=\"4\" 
                                             data-data-mes =\"";
        // line 133
        echo twig_escape_filter($this->env, $this->getContext($context, "daily_sum_ofertas_mes"), "html", null, true);
        echo "\"
                                             data-data-semana =\"";
        // line 134
        echo twig_escape_filter($this->env, $this->getContext($context, "daily_sum_ofertas_semana"), "html", null, true);
        echo "\"
                                             data-data=\"";
        // line 135
        echo twig_escape_filter($this->env, $this->getContext($context, "daily_sum_ofertas_mes"), "html", null, true);
        echo "\"                                 
                                             data-sum-ofertas-mes =\"";
        // line 136
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getContext($context, "sum_ofertas_mes"), 2, ",", "."), "html", null, true);
        echo "\"
                                             data-sum-ofertas-semana=\"";
        // line 137
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getContext($context, "sum_ofertas_semana"), 2, ",", "."), "html", null, true);
        echo "\"
                                             ></div>
                                    </div>
                                </div>
                                <div class=\"chart-tittle\">
                                    <span class=\"title\">";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total facturado por periodo"), "html", null, true);
        echo "</span>
                                    <span class=\"value\">
                                        <a href=\"#\" class=\"active facturacion-ultimo-mes\">";
        // line 144
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Último mes"), "html", null, true);
        echo "</a>
                                        |
                                        <a href=\"#\" class=\"facturacion-ultima-semana\">";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Última semana"), "html", null, true);
        echo "</a>
                                    </span>
                                </div>
                            </div>
                            <!--new earning end-->

                            <!--total earning start-->
                            <div class=\"panel green-chart\">
                                <div class=\"panel-body\">
                                    <div class=\"chart\">
                                        <div id=\"barchart\"
                                             data-data=\"";
        // line 157
        echo twig_escape_filter($this->env, $this->getContext($context, "monthly_ofertas_yearly"), "html", null, true);
        echo "\"                          

                                             ></div>
                                    </div>
                                </div>
                                <div class=\"chart-tittle\">
                                    <span class=\"title\">";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Facturación últimos 12 meses"), "html", null, true);
        echo "</span>
                                    <span class=\"value\">";
        // line 164
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getContext($context, "sum_monthly_ofertas_yearly"), 2, ",", "."), "html", null, true);
        echo " €</span>
                                    ";
        // line 170
        echo "                                </div>
                            </div>
                            <!--total earning end-->
                        </div>
                    </div>
                    <div class=\"row\">
                        <div class=\"col-lg-4\">
                            <!--user info table start-->
                            <section class=\"panel\">
                                <div class=\"panel-body\">
                                    <a href=\"#\" class=\"task-thumb\">
                                        <img  src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "getAvatarImage", array(), "method"), "html", null, true);
        echo "\" style=\"width:90px;height:90px;\" alt=\"\">
                                    </a>
                                    <div class=\"task-thumb-details\">
                                        <h1>
                                            <a href=\"#\">";
        // line 185
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "getNombreCompleto"), "html", null, true);
        echo "</a>
                                        </h1>
                                        ";
        // line 187
        if ($this->env->getExtension('security')->isGranted("ROLE_HOTEL")) {
            // line 188
            echo "                                            <p>";
            echo twig_escape_filter($this->env, (($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array(), "any", false, true), "cargo", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array(), "any", false, true), "cargo"), $this->env->getExtension('translator')->trans("Sin especificar"))) : ($this->env->getExtension('translator')->trans("Sin especificar"))), "html", null, true);
            echo "</p>
                                        ";
        }
        // line 190
        echo "                                    </div>
                                </div>
                                <table class=\"table table-hover personal-task\">
                                    <tbody>
                                        ";
        // line 194
        if ($this->env->getExtension('security')->isGranted("ROLE_HOTEL")) {
            // line 195
            echo "                                            <tr>
                                                <td>
                                                    <i class=\" fa fa-tasks\"></i>
                                                </td>
                                                <td>";
            // line 199
            echo "Numero de ofertas gestionadas";
            echo "</td>
                                                <td>";
            // line 200
            echo twig_escape_filter($this->env, ((array_key_exists("num_ofertas_gestionadas", $context)) ? (_twig_default_filter($this->getContext($context, "num_ofertas_gestionadas"), "0")) : ("0")), "html", null, true);
            echo "</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <i class=\"fa fa-bell-o\"></i>
                                                </td>
                                                <td>
                                                    <a  href=\"";
            // line 207
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "pendientes")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Numero de ofertas pendientes"), "html", null, true);
            echo "</a>
                                                </td>
                                                <td>
                                                    <a  href=\"";
            // line 210
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_filtradas", array("filtro" => "pendientes")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, ((array_key_exists("num_ofertas_pendientes", $context)) ? (_twig_default_filter($this->getContext($context, "num_ofertas_pendientes"), "0")) : ("0")), "html", null, true);
            echo "</a>
                                                </td>
                                            </tr>
                                        ";
        }
        // line 214
        echo "                                        <tr>
                                            <td>
                                                <i class=\"fa fa-exclamation\"></i>
                                            </td>
                                            <td><a href=\"";
        // line 218
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Incidencias"), "html", null, true);
        echo "</a></td>
                                            <td><a href=\"";
        // line 219
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getContext($context, "incidencias")), "html", null, true);
        echo "</a></td>
                                        </tr>
                                        ";
        // line 221
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 222
            echo "                                            <tr>
                                                <td>
                                                    <i class=\" fa fa-envelope\"></i>
                                                </td>
                                                <td>";
            // line 226
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Buzón de contacta"), "html", null, true);
            echo "</td>
                                                <td>";
            // line 227
            echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getContext($context, "contactos")), "html", null, true);
            echo "</td>
                                            </tr>
                                        ";
        }
        // line 230
        echo "                                    </tbody>
                                </table>
                            </section>
                            <!--user info table end-->
                        </div>
                        <div class=\"col-lg-8\">
                            <!--work progress start-->
                            ";
        // line 237
        if (($this->env->getExtension('security')->isGranted("ROLE_EMPRESA") || $this->env->getExtension('security')->isGranted("ROLE_ADMIN"))) {
            // line 238
            echo "                                <section class=\"panel\">
                                    <div class=\"panel-body progress-panel\">
                                        <div class=\"task-progress\">
                                            <h1>";
            // line 241
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ranking de hoteles"), "html", null, true);
            echo "</h1>
                                        </div>                           
                                    </div>
                                    <table class=\"table table-hover personal-task\">
                                        <tbody>
                                            ";
            // line 246
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "ranking"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["nombre_hotel"] => $context["porcentaje"]) {
                // line 247
                echo "                                                <tr>
                                                    <td>";
                // line 248
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "loop"), "index"), "html", null, true);
                echo "</td>
                                                    <td>";
                // line 249
                echo twig_escape_filter($this->env, $this->getContext($context, "nombre_hotel"), "html", null, true);
                echo "</td>
                                                    <td>
                                                        ";
                // line 251
                $this->env->loadTemplate("HotelesBackendBundle:Landing:porcentaje-de-ofertas-atendidas.html.twig")->display(array_merge($context, array("porcentaje" => $this->getContext($context, "porcentaje"))));
                // line 252
                echo "                                                    </td>
                                                </tr>
                                            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['nombre_hotel'], $context['porcentaje'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 254
            echo "                              

                                        </tbody>
                                    </table>
                                </section>
                            ";
        }
        // line 260
        echo "                            <!--work progress end-->
                        </div>
                    </div>                   
                </section>

                ";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Landing:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  537 => 260,  529 => 254,  513 => 252,  511 => 251,  506 => 249,  502 => 248,  499 => 247,  482 => 246,  474 => 241,  469 => 238,  467 => 237,  458 => 230,  452 => 227,  448 => 226,  442 => 222,  440 => 221,  433 => 219,  427 => 218,  421 => 214,  412 => 210,  404 => 207,  394 => 200,  390 => 199,  384 => 195,  382 => 194,  376 => 190,  370 => 188,  368 => 187,  363 => 185,  356 => 181,  343 => 170,  339 => 164,  335 => 163,  326 => 157,  312 => 146,  307 => 144,  302 => 142,  294 => 137,  290 => 136,  286 => 135,  282 => 134,  278 => 133,  262 => 120,  254 => 119,  242 => 109,  233 => 105,  216 => 101,  210 => 98,  206 => 96,  188 => 95,  185 => 94,  176 => 91,  173 => 90,  169 => 89,  164 => 87,  159 => 85,  145 => 74,  129 => 61,  113 => 48,  97 => 35,  80 => 21,  76 => 20,  72 => 19,  68 => 18,  58 => 10,  55 => 9,  39 => 6,  35 => 5,  30 => 4,  27 => 3,);
    }
}
