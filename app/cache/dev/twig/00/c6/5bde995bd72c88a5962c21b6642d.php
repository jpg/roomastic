<?php

/* FOSUserBundle::layout.html.twig */
class __TwigTemplate_00c65bde995bd72c88a5962c21b6642d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"Mosaddek\">
        <meta name=\"keyword\" content=\"FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina\">
        <link rel=\"shortcut icon\" href=\"img/favicon.png\">

        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

        <title></title>

        <!-- Bootstrap core CSS -->
        <link href=\"/bundles/hotelesbackend/css/bootstrap.min.css\" rel=\"stylesheet\">
        <link href=\"/bundles/hotelesbackend/css/bootstrap-reset.css\" rel=\"stylesheet\">
        <!--external css-->
        <link href=\"/bundles/hotelesbackend/assets/font-awesome/css/font-awesome.css\" rel=\"stylesheet\" />
        <!-- Custom styles for this template -->
        <link href=\"/bundles/hotelesbackend/css/style.css\" rel=\"stylesheet\">
        <link href=\"/bundles/hotelesbackend/css/style-responsive.css\" rel=\"stylesheet\" />

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src=\"js/html5shiv.js\"></script>
        <script src=\"js/respond.min.js\"></script>
        <![endif]-->
    </head>
    <body class=\"login-body\">

        <div class=\"container\">        
            ";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "getFlashes"));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 33
            echo "                ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "messages"));
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 34
                echo "                    <div class=\"";
                echo twig_escape_filter($this->env, $this->getContext($context, "type"), "html", null, true);
                echo "\">
                        ";
                // line 35
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "message"), array(), "FOSUserBundle"), "html", null, true);
                echo "
                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 38
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 39
        echo "
            <div>
                ";
        // line 41
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 43
        echo "            </div>
        </div>
    </body>
</html>";
    }

    // line 41
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 42
        echo "                ";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 42,  93 => 41,  86 => 43,  84 => 41,  80 => 39,  74 => 38,  65 => 35,  60 => 34,  55 => 33,  51 => 32,  18 => 1,  29 => 4,  26 => 3,);
    }
}
