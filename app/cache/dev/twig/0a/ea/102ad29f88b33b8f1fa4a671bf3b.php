<?php

/* HotelesBackendBundle:Extras:empresaStatus.html.twig */
class __TwigTemplate_0aea102ad29f88b33b8f1fa4a671bf3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 6
        if (($this->getContext($context, "status") == 0)) {
            // line 7
            echo "    <span class=\"fa-lg\">
        <i class=\"fa fa-check\"></i>
    </span>
";
        } elseif (($this->getContext($context, "status") == 1)) {
            // line 11
            echo "    <span class=\"fa-stack fa-lg\">
        <i class=\"fa fa-user fa-stack-1x\"></i>
        <i class=\"fa fa-ban fa-stack-2x text-danger\"></i>
    </span>
";
        } else {
            // line 16
            echo "    <span class=\"fa-lg\">
        <i class=\"fa fa-question\"></i>
    </span>
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:empresaStatus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 16,  25 => 11,  19 => 7,  17 => 6,  244 => 94,  239 => 91,  231 => 89,  229 => 88,  214 => 75,  194 => 71,  190 => 69,  188 => 68,  183 => 66,  179 => 65,  175 => 64,  169 => 63,  165 => 62,  161 => 61,  157 => 60,  153 => 59,  149 => 58,  144 => 55,  127 => 54,  120 => 50,  116 => 49,  112 => 48,  108 => 47,  104 => 46,  100 => 45,  96 => 44,  92 => 43,  88 => 42,  84 => 41,  80 => 40,  70 => 32,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
