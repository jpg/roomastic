<?php

/* HotelesBackendBundle:Extras:checked.html.twig */
class __TwigTemplate_0a5743d789817a1411537bd9454364f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getContext($context, "dato") == 1)) {
            // line 2
            echo "    <i class=\"fa fa-check\"></i>
";
        } else {
            // line 4
            echo "    <i class=\"fa fa-times\"></i>
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:checked.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  23 => 4,  25 => 4,  19 => 2,  17 => 1,  199 => 85,  190 => 81,  178 => 71,  160 => 66,  154 => 64,  147 => 60,  144 => 59,  142 => 58,  137 => 56,  131 => 55,  128 => 54,  126 => 53,  121 => 50,  104 => 49,  96 => 44,  92 => 43,  88 => 42,  84 => 41,  80 => 40,  70 => 32,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
