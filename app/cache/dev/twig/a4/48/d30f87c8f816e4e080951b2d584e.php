<?php

/* HotelesBackendBundle:Crop:crop.html.twig */
class __TwigTemplate_a448d30f87c8f816e4e080951b2d584e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
    var jcrop_api;
    jQuery(document).ready(function (\$) {

        \$('#inputsubida').change(function (event) {
            var urlcrea = '";
        // line 6
        echo twig_escape_filter($this->env, $this->getContext($context, "urlcrea"), "html", null, true);
        echo "';
            var urlsubidaimagen = '";
        // line 7
        echo twig_escape_filter($this->env, $this->getContext($context, "urlsubidaimagen"), "html", null, true);
        echo "';
            \$('#formulariosubida').attr('action', urlsubidaimagen);
            \$('#formulariosubida').attr('enctype', 'multipart/form-data');
            \$('#formulariosubida').attr('target', 'inter');
            \$('#formulariosubida').submit();
            \$('#formulariosubida').attr('action', urlcrea);
            \$('#formulariosubida').removeAttr('target');
            event.preventDefault();
        });

        initJcrop();

        \$('#successimage').click(function (event) {
            url = Routing.generate('admin_imagecrop', {
                posx: ROOMASTIC.image_crop.posx,
                posy: ROOMASTIC.image_crop.posy,
                width: ROOMASTIC.image_crop.width,
                height: ROOMASTIC.image_crop.height,
                imagen: ROOMASTIC.image_crop.imagen,
                carpeta: '";
        // line 26
        echo twig_escape_filter($this->env, $this->getContext($context, "tipo"), "html", null, true);
        echo "'});
            \$.get(url, function (respuesta) {
                \$('#closeimage').click();
                var id = \$('#successimage').attr('data-id');
                var nombreImagen = \$('#successimage').attr('data-img');
    ";
        // line 31
        if (($this->getContext($context, "especifico") == "user")) {
            // line 32
            echo "                    \$('#imagenusuario').attr('src', '/uploads/";
            echo twig_escape_filter($this->env, $this->getContext($context, "tipo"), "html", null, true);
            echo "/' + nombreImagen);
                    \$('#imagensubidanombre').val(nombreImagen);
    ";
        } elseif (($this->getContext($context, "especifico") == "hoteles")) {
            // line 35
            echo "                    \$('#imagenessubidasinputs').append('<input type=\"text\" name=\"imageneshotel[]\" class=\"borraelemento_' + id + '\" value=\"' + id + '\"><br>');
                    \$('#sortable').append(' <li class=\"list-primary borraelemento_' + id + '\" data-id=\"' + id + '\"><i class=\" fa fa-ellipsis-v\"></i><div class=\"task-title\"><img src=\"/uploads/hoteles/' + nombreImagen + '\" width=\"200\"><div class=\"pull-right hidden-phone\"><a href=\"#\"><button class=\"btn btn-success btn-xs fa fa-pencil\"></button></a>&nbsp;<a href=\"#\"><button class=\"btn btn-primary btn-xs fa fa-eye\"></button></a>&nbsp;<a href=\"#\" onClick=\"deleteElement(\\'' + id + '\\'); return false;\"><button class=\"btn btn-danger btn-xs fa fa-trash-o\"></button></a></div></div></li>');
    ";
        }
        // line 38
        echo "                    \$.get(Routing.generate('hoteles_backend_imageupload_deletetempfile', {imagen: ROOMASTIC.image_crop.imagen}));
                }).fail(function () {
                    alert('ha habido un error :S');
                });

            });

        });

        function initJcrop()
        {

            \$('.requiresjcrop').hide();
            \$('#target').Jcrop({
                //onRelease: releaseCheck
                onSelect: showCoords,
                bgColor: 'black',
                bgOpacity: .4,
                setSelect: [";
        // line 56
        echo twig_escape_filter($this->env, $this->getContext($context, "crop1"), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->getContext($context, "crop2"), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->getContext($context, "crop3"), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, $this->getContext($context, "crop4"), "html", null, true);
        echo " ],
                aspectRatio: ";
        // line 57
        echo twig_escape_filter($this->env, $this->getContext($context, "crop5"), "html", null, true);
        echo " / ";
        echo twig_escape_filter($this->env, $this->getContext($context, "crop6"), "html", null, true);
        echo ",
                        boxWidth: 800,
            }, function () {

                jcrop_api = this;
                jcrop_api.animateTo([100, 100, 400, 300]);

                \$('#can_click,#can_move,#can_size').attr('checked', 'checked');
                \$('#ar_lock,#size_lock,#bg_swap').attr('checked', false);
                \$('.requiresjcrop').show();

            });

        }
        ;

        function replaceAll(text, busca, reemplaza) {
            while (text.toString().indexOf(busca) != - 1)
                text = text.toString().replace(busca, reemplaza);
            return text;
        }

        function showCoords(c)
        {
            imagen = replaceAll(\$('#successimage').attr('data-img'), \".\", \"___---___\")
            if (typeof c.x !== \"undefined\") {
                var posx = Math.round(c.x)
                var posy = Math.round(c.y)
                var width = Math.round(c.w)
                var height = Math.round(c.h)
            }
            if (typeof ROOMASTIC === \"undefined\") {
                ROOMASTIC = {};
            }
            if (typeof ROOMASTIC.image_crop === \"undefined\") {
                ROOMASTIC.image_crop = {};
            }
            ROOMASTIC.image_crop.imagen = imagen;
            ROOMASTIC.image_crop.posx = posx;
            ROOMASTIC.image_crop.posy = posy;
            ROOMASTIC.image_crop.width = width;
            ROOMASTIC.image_crop.height = height;

            var url = '/' + imagen + '/' + posx + '/' + posy + '/' + width + '/' + height
            \$('#successimage').attr('data-url', url);
        }
        ;

        function inserta(id, nombreImagen) {
            var mayor = 0;
            var dirImages = '/uploads/temporal/';
            \$('#target').attr('src', dirImages + nombreImagen);
            \$('.jcrop-preview').attr('src', dirImages + nombreImagen);
            \$('#successimage').attr('data-id', id);
            \$('#successimage').attr('data-img', nombreImagen);
            jcrop_api.setImage(dirImages + nombreImagen);
            \$('#clickmodal').click();
        }

</script>";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Crop:crop.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 56,  72 => 38,  67 => 35,  60 => 32,  58 => 31,  50 => 26,  28 => 7,  24 => 6,  17 => 1,  1060 => 445,  1058 => 444,  1055 => 443,  1052 => 442,  1049 => 441,  1046 => 440,  1043 => 439,  1040 => 438,  1037 => 437,  1034 => 436,  1031 => 435,  1028 => 434,  1025 => 433,  1012 => 423,  1008 => 422,  999 => 416,  989 => 409,  981 => 403,  972 => 397,  968 => 396,  964 => 395,  958 => 392,  954 => 391,  950 => 390,  945 => 388,  941 => 386,  939 => 385,  932 => 381,  928 => 380,  923 => 378,  919 => 377,  913 => 374,  909 => 373,  904 => 371,  900 => 370,  894 => 367,  890 => 366,  885 => 364,  881 => 363,  869 => 354,  865 => 353,  852 => 343,  840 => 333,  829 => 331,  825 => 330,  822 => 329,  813 => 322,  796 => 319,  792 => 318,  780 => 309,  777 => 308,  770 => 301,  766 => 300,  761 => 298,  751 => 291,  747 => 290,  742 => 288,  733 => 282,  729 => 281,  723 => 278,  719 => 277,  714 => 275,  708 => 274,  701 => 270,  697 => 269,  691 => 266,  687 => 265,  682 => 263,  676 => 262,  670 => 259,  666 => 258,  661 => 256,  656 => 254,  652 => 253,  647 => 251,  641 => 248,  637 => 247,  632 => 245,  620 => 236,  613 => 232,  609 => 231,  604 => 229,  600 => 228,  594 => 225,  590 => 224,  585 => 222,  581 => 221,  575 => 218,  571 => 217,  566 => 215,  562 => 214,  556 => 211,  552 => 210,  547 => 208,  540 => 204,  536 => 203,  531 => 201,  525 => 198,  521 => 197,  516 => 195,  510 => 192,  506 => 191,  501 => 189,  494 => 185,  490 => 184,  485 => 182,  479 => 179,  475 => 178,  470 => 176,  464 => 173,  460 => 172,  455 => 170,  448 => 166,  444 => 165,  439 => 163,  432 => 159,  428 => 158,  423 => 156,  419 => 155,  413 => 152,  409 => 151,  404 => 149,  400 => 148,  391 => 142,  383 => 137,  379 => 136,  374 => 134,  370 => 133,  364 => 130,  360 => 129,  355 => 127,  351 => 126,  345 => 123,  341 => 122,  336 => 120,  332 => 119,  326 => 116,  322 => 115,  317 => 113,  313 => 112,  307 => 109,  303 => 108,  298 => 106,  294 => 105,  285 => 99,  281 => 98,  276 => 96,  272 => 95,  266 => 92,  262 => 91,  257 => 89,  253 => 88,  247 => 85,  243 => 84,  238 => 82,  234 => 81,  228 => 78,  224 => 77,  219 => 75,  215 => 74,  209 => 71,  205 => 70,  200 => 68,  196 => 67,  190 => 64,  186 => 63,  181 => 61,  174 => 57,  170 => 56,  165 => 54,  161 => 53,  151 => 45,  145 => 42,  140 => 41,  138 => 40,  133 => 38,  129 => 37,  124 => 35,  120 => 34,  115 => 32,  111 => 31,  106 => 29,  102 => 57,  97 => 26,  93 => 25,  86 => 21,  74 => 15,  71 => 14,  55 => 11,  51 => 10,  46 => 9,  43 => 8,  37 => 5,  32 => 4,  29 => 3,);
    }
}
