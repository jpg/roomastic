<?php

/* HotelesBackendBundle:Mail:avisoHotelOfertaRealizada.html.twig */
class __TwigTemplate_a6dc1706241964fa2cf6d7411384fae2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "

";
        // line 4
        echo "

<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\">
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    </head>
    <body>

        <table width=\"812\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
            <table width=\"709\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\">
                <!-- header -->
                <tr>
                    <td height=\"54\" width=\"100%\">&nbsp;</td>
                </tr>   
                <tr>
                    <td>
                        <img src=\"https://roomastic.com/bundles/hotelesfrontend/img/logo.png\" width=\"146\" height=\"45\" border=\"0\" style=\"display:block;\" />
                    </td>
                </tr>
                <tr>
                    <td height=\"54\" width=\"100%\">&nbsp;</td>
                </tr><!-- / header -->

                <tr>
                    <td style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px;\">
                        <p style=\"font-weight:bold; font-size:29px; margin:0;\">Un usuario está interesado en tu hotel.</p>
                        <p>Código de la oferta: ";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "idofertaunico"), "html", null, true);
        echo "</p>
                    </td>   
                </tr>
                <!-- table dates -->
                <tr><td height=\"36\">&nbsp;</td></tr>    
                <tr>
                    <td>
                        <table width=\"709\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" bgcolor=\"#FFFFFF\" style=\"border-top:2px solid #e9e9e9;\">
                            <tr>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; border-bottom:2px solid #e9e9e9;\">FECHA DE ENTRADA</td>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; font-weight:bold; text-align:right; border-bottom:2px solid #e9e9e9;\">";
        // line 41
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "fechain"), "d/m/Y"), "html", null, true);
        echo "</td>
                            </tr>
                            <tr>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; border-bottom:2px solid #e9e9e9;\">FECHA DE SALIDA</td>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; font-weight:bold; text-align:right; border-bottom:2px solid #e9e9e9;\">";
        // line 45
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "fechaout"), "d/m/Y"), "html", null, true);
        echo "</td>
                            </tr>
                            <tr>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; border-bottom:2px solid #e9e9e9;\">NÚMERO DE NOCHES</td>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; font-weight:bold; text-align:right; border-bottom:2px solid #e9e9e9;\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "numnoches"), "html", null, true);
        echo "</td>
                            </tr>
                            <tr>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; border-bottom:2px solid #e9e9e9;\">NÚMERO DE HABITACIONES</td>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; font-weight:bold; text-align:right; border-bottom:2px solid #e9e9e9;\">";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "numhabitaciones"), "html", null, true);
        echo "</td>
                            </tr>
                            <tr>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; border-bottom:2px solid #e9e9e9;\">NÚMERO DE ADULTOS</td>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; font-weight:bold; text-align:right; border-bottom:2px solid #e9e9e9;\">";
        // line 57
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "numadultos"), "html", null, true);
        echo "</td>
                            </tr>
                            <tr>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; border-bottom:2px solid #e9e9e9;\">NÚMERO DE NIÑOS</td>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; font-weight:bold; text-align:right; border-bottom:2px solid #e9e9e9;\">";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "numninos"), "html", null, true);
        echo "</td>
                            </tr>
                            <tr>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; border-bottom:2px solid #e9e9e9;\">TOTAL DE LA OFERTA:</td>
                                <td height=\"38\" style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px; font-weight:bold; text-align:right; border-bottom:2px solid #e9e9e9;\">";
        // line 65
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "getNeto"), 2, ",", "."), "html", null, true);
        echo "</td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr><td height=\"20\">&nbsp;</td></tr>
                <!-- /table dates -->
                <tr>
                    <td style=\"font-family:Arial, Helvetica, sans-serif; color:#474747; font-size:15px;\">
                        <p>Puedes contestarle así de sencillo y ahora mismo:</p>
                        <a href=\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("hoteles_backend_oferta_aceptarofertadesdeemail", array("idofertaunico" => $this->getAttribute($this->getContext($context, "oferta"), "idofertaunico"))), "html", null, true);
        echo "\" style=\"background-color:#e72f82; padding:5px 10px; color:#fff; text-decoration:none; margin-right:20px;\">Aceptar</a>                
                        <a href=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getUrl("hoteles_backend_oferta_rechazarofertadesdeemail", array("idofertaunico" => $this->getAttribute($this->getContext($context, "oferta"), "idofertaunico"))), "html", null, true);
        echo "\" style=\"background-color:#e72f82; padding:5px 10px; color:#fff; text-decoration:none;\">Rechazar</a><br/>                
                        <p>Procura responder cuanto antes y evita que otro hotel se anticipe. Recuerda que tienes un máximo de ";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "configuracion"), "tiempohotel"), "html", null, true);
        echo " horas para responder.</p>
                        <!--
                        <p>este mensaje se ha enviado desde la página de roomastic.com. el usuario no conoce la dirección de tu correo y la aceptación o rechazo de esta oferta será algo privado entre el usuario y el hotel</p>
                        -->
                        <p>¡Qué no se te adelanten!</p>
                        <p>El equipo de Roomastic.</p>
                    </td>
                </tr>

                <!-- footer -->
                <tr><td height=\"127\">&nbsp;</td></tr>    
                <tr>
                    <td style=\"border-top:2px solid #e9e9e9; text-align:center; font-family:Arial, Helvetica, sans-serif;\">
                        <p style=\"height:10px;\">&nbsp;</p>
                        <span style=\"color:#e52e81; font-weight:bold; font-size:16px;\">Teléfono de atención al cliente</span><br />
                        <span style=\"color:#e52e81; font-weight:bold; font-size:27px;\">911 610 156</span><br /><br />
                    </td>
                </tr>    
                <tr>
                    <td style=\"font-size:9px; color:#6c6c6c; text-transform: uppercase; font-family:Arial, Helvetica, sans-serif; text-align:center;\">
                        este email ha sido enviado de manera automática por <span style=\"text-decoration:none; color:#474747;\">roomastic.com</span> © Copyright. Todos los derechos reservados.  
                    </td>
                </tr>
                <!-- /footer -->

            </table><!-- table content -->
        </table><!-- table margen -->
    </body>
</html>

";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Mail:avisoHotelOfertaRealizada.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 78,  123 => 77,  119 => 76,  105 => 65,  98 => 61,  91 => 57,  84 => 53,  77 => 49,  70 => 45,  63 => 41,  50 => 31,  21 => 4,  17 => 1,);
    }
}
