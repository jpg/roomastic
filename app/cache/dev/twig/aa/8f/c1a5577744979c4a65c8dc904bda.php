<?php

/* TwigBundle:Exception:trace.html.twig */
class __TwigTemplate_aa8fc1a5577744979c4a65c8dc904bda extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute($this->getContext($context, "trace"), "function")) {
            // line 2
            echo "    at
    <strong>
        <abbr title=\"";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trace"), "class"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "trace"), "short_class"), "html", null, true);
            echo "</abbr>
        ";
            // line 5
            echo twig_escape_filter($this->env, ($this->getAttribute($this->getContext($context, "trace"), "type") . $this->getAttribute($this->getContext($context, "trace"), "function")), "html", null, true);
            echo "
    </strong>
    (";
            // line 7
            echo $this->env->getExtension('code')->formatArgs($this->getAttribute($this->getContext($context, "trace"), "args"));
            echo ")
";
        }
        // line 9
        echo "
";
        // line 10
        if (((($this->getAttribute($this->getContext($context, "trace", true), "file", array(), "any", true, true) && $this->getAttribute($this->getContext($context, "trace"), "file")) && $this->getAttribute($this->getContext($context, "trace", true), "line", array(), "any", true, true)) && $this->getAttribute($this->getContext($context, "trace"), "line"))) {
            // line 11
            echo "    ";
            echo (($this->getAttribute($this->getContext($context, "trace"), "function")) ? ("<br />") : (""));
            echo "
    in ";
            // line 12
            echo $this->env->getExtension('code')->formatFile($this->getAttribute($this->getContext($context, "trace"), "file"), $this->getAttribute($this->getContext($context, "trace"), "line"));
            echo "&nbsp;
    ";
            // line 13
            ob_start();
            // line 14
            echo "    <a href=\"#\" onclick=\"toggle('trace_";
            echo twig_escape_filter($this->env, (($this->getContext($context, "prefix") . "_") . $this->getContext($context, "i")), "html", null, true);
            echo "'); switchIcons('icon_";
            echo twig_escape_filter($this->env, (($this->getContext($context, "prefix") . "_") . $this->getContext($context, "i")), "html", null, true);
            echo "_open', 'icon_";
            echo twig_escape_filter($this->env, (($this->getContext($context, "prefix") . "_") . $this->getContext($context, "i")), "html", null, true);
            echo "_close'); return false;\">
        <img class=\"toggle\" id=\"icon_";
            // line 15
            echo twig_escape_filter($this->env, (($this->getContext($context, "prefix") . "_") . $this->getContext($context, "i")), "html", null, true);
            echo "_close\" alt=\"-\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/images/blue_picto_less.gif"), "html", null, true);
            echo "\" style=\"visibility: ";
            echo (((0 == $this->getContext($context, "i"))) ? ("visible") : ("hidden"));
            echo "\" />
        <img class=\"toggle\" id=\"icon_";
            // line 16
            echo twig_escape_filter($this->env, (($this->getContext($context, "prefix") . "_") . $this->getContext($context, "i")), "html", null, true);
            echo "_open\" alt=\"+\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/images/blue_picto_more.gif"), "html", null, true);
            echo "\" style=\"visibility: ";
            echo (((0 == $this->getContext($context, "i"))) ? ("hidden") : ("visible"));
            echo "; margin-left: -18px\" />
    </a>
    ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 19
            echo "    <div id=\"trace_";
            echo twig_escape_filter($this->env, (($this->getContext($context, "prefix") . "_") . $this->getContext($context, "i")), "html", null, true);
            echo "\" style=\"display: ";
            echo (((0 == $this->getContext($context, "i"))) ? ("block") : ("none"));
            echo "\" class=\"trace\">
        ";
            // line 20
            echo $this->env->getExtension('code')->fileExcerpt($this->getAttribute($this->getContext($context, "trace"), "file"), $this->getAttribute($this->getContext($context, "trace"), "line"));
            echo "
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:trace.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 14,  53 => 13,  42 => 10,  34 => 7,  23 => 4,  95 => 21,  88 => 19,  78 => 17,  75 => 16,  71 => 14,  40 => 7,  38 => 6,  31 => 5,  22 => 3,  20 => 2,  224 => 96,  200 => 83,  195 => 80,  177 => 71,  158 => 61,  156 => 60,  146 => 55,  135 => 50,  131 => 48,  126 => 46,  120 => 45,  113 => 43,  103 => 36,  47 => 19,  39 => 9,  17 => 1,  86 => 6,  46 => 14,  37 => 8,  33 => 7,  24 => 6,  19 => 2,  44 => 11,  30 => 4,  27 => 3,  1000 => 291,  995 => 290,  993 => 289,  990 => 288,  974 => 284,  952 => 283,  950 => 282,  947 => 281,  935 => 276,  931 => 275,  926 => 274,  924 => 273,  921 => 272,  912 => 266,  906 => 264,  903 => 263,  898 => 262,  896 => 261,  893 => 260,  886 => 255,  877 => 253,  873 => 252,  870 => 251,  867 => 250,  865 => 249,  862 => 248,  854 => 244,  852 => 243,  849 => 242,  842 => 237,  839 => 236,  831 => 231,  827 => 230,  823 => 229,  820 => 228,  818 => 227,  815 => 226,  807 => 222,  805 => 221,  802 => 220,  794 => 214,  792 => 213,  789 => 212,  781 => 208,  778 => 207,  776 => 206,  773 => 205,  752 => 201,  749 => 200,  746 => 199,  743 => 198,  741 => 197,  738 => 196,  730 => 190,  727 => 189,  725 => 188,  722 => 187,  715 => 184,  712 => 183,  709 => 182,  701 => 178,  698 => 177,  696 => 176,  693 => 175,  677 => 171,  674 => 170,  672 => 169,  669 => 168,  661 => 164,  658 => 163,  645 => 157,  642 => 156,  640 => 155,  637 => 154,  629 => 150,  626 => 149,  624 => 148,  621 => 147,  613 => 143,  611 => 142,  608 => 141,  597 => 136,  595 => 135,  592 => 134,  584 => 130,  581 => 129,  579 => 128,  577 => 127,  574 => 126,  567 => 121,  559 => 120,  554 => 119,  548 => 117,  545 => 116,  543 => 115,  532 => 108,  530 => 104,  525 => 103,  519 => 101,  516 => 100,  514 => 99,  511 => 98,  502 => 92,  498 => 91,  494 => 90,  490 => 89,  485 => 88,  479 => 86,  476 => 85,  471 => 83,  455 => 79,  453 => 78,  450 => 77,  434 => 73,  432 => 72,  429 => 71,  419 => 65,  416 => 64,  413 => 63,  405 => 60,  400 => 59,  397 => 58,  388 => 55,  386 => 54,  378 => 53,  366 => 49,  361 => 48,  352 => 46,  349 => 45,  347 => 44,  344 => 43,  335 => 39,  323 => 37,  319 => 35,  304 => 33,  300 => 32,  295 => 31,  292 => 30,  287 => 29,  285 => 28,  282 => 27,  272 => 23,  267 => 21,  259 => 17,  256 => 16,  250 => 14,  248 => 13,  237 => 7,  233 => 6,  228 => 5,  226 => 4,  223 => 3,  219 => 288,  216 => 287,  214 => 281,  209 => 272,  206 => 271,  201 => 260,  196 => 248,  191 => 242,  188 => 77,  185 => 76,  180 => 235,  178 => 226,  175 => 225,  173 => 220,  170 => 219,  167 => 217,  162 => 63,  160 => 205,  155 => 196,  152 => 195,  149 => 193,  144 => 186,  142 => 54,  132 => 168,  129 => 47,  119 => 153,  114 => 146,  112 => 141,  107 => 134,  104 => 24,  99 => 34,  92 => 20,  89 => 20,  87 => 83,  84 => 82,  82 => 19,  77 => 28,  74 => 27,  72 => 16,  69 => 42,  67 => 12,  64 => 15,  62 => 24,  59 => 23,  57 => 9,  52 => 3,  284 => 128,  270 => 22,  261 => 122,  257 => 120,  253 => 15,  251 => 117,  225 => 93,  222 => 92,  218 => 90,  215 => 90,  208 => 70,  204 => 84,  190 => 78,  186 => 34,  171 => 67,  159 => 13,  153 => 59,  147 => 187,  141 => 157,  139 => 181,  136 => 91,  122 => 154,  117 => 44,  108 => 71,  81 => 47,  79 => 40,  70 => 26,  66 => 39,  63 => 38,  61 => 23,  49 => 12,  36 => 10,  25 => 4,  666 => 339,  662 => 337,  659 => 336,  656 => 162,  653 => 161,  651 => 333,  635 => 320,  618 => 305,  609 => 303,  603 => 302,  600 => 137,  596 => 300,  582 => 289,  569 => 279,  562 => 275,  549 => 265,  544 => 263,  540 => 114,  535 => 260,  524 => 252,  517 => 250,  507 => 243,  500 => 238,  497 => 237,  483 => 226,  474 => 84,  467 => 216,  462 => 214,  457 => 212,  452 => 210,  447 => 208,  442 => 206,  433 => 202,  427 => 201,  411 => 188,  407 => 61,  399 => 182,  394 => 57,  389 => 178,  384 => 176,  379 => 174,  374 => 51,  369 => 170,  364 => 168,  357 => 47,  350 => 162,  328 => 143,  324 => 142,  316 => 137,  310 => 134,  266 => 125,  262 => 92,  245 => 12,  240 => 76,  235 => 74,  231 => 73,  211 => 88,  203 => 269,  198 => 259,  193 => 79,  183 => 236,  179 => 72,  174 => 24,  165 => 212,  157 => 204,  148 => 31,  145 => 30,  137 => 51,  134 => 174,  127 => 161,  124 => 160,  118 => 26,  115 => 25,  109 => 140,  106 => 70,  102 => 126,  97 => 22,  94 => 31,  54 => 11,  50 => 11,  45 => 13,  41 => 7,  32 => 11,  29 => 5,);
    }
}
