<?php

/* HotelesFrontendBundle:Frontend:hotel.mv.twig */
class __TwigTemplate_28ac2af39b989e093bd7e965b73d276d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_javascripts($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 5
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a38f972_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a38f972_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/a38f972_part_1_hotel_detalle_1.js");
            // line 6
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "a38f972"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a38f972") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/a38f972.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 10
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 11
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "    <div class=\"content_hotel\">
        <a href=\"\" onClick=\"window.history.back();\" class=\"btn_volver\"> &lt; VOLVER AL LISTADO</a>
        <div class=\"hotel\">
            <h2 class=\"name\">";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "nombrehotel"), "html", null, true);
        echo "</h2>
            <ul class=\"servicios clearfix\">
                ";
        // line 19
        if (($this->getAttribute($this->getContext($context, "hotel"), "piscina") == 1)) {
            // line 20
            echo "                    <li class=\"piscina\">Piscina
                    </li>
                ";
        }
        // line 23
        echo "
                ";
        // line 24
        if (($this->getAttribute($this->getContext($context, "hotel"), "spa") == 1)) {
            // line 25
            echo "                    <li class=\"spa\">Spa
                    </li>
                ";
        }
        // line 28
        echo "
                ";
        // line 29
        if (($this->getAttribute($this->getContext($context, "hotel"), "wiFi") == 1)) {
            // line 30
            echo "                    <li class=\"wi-fi\">Wi-fi
                    </li>        
                ";
        }
        // line 33
        echo "
                ";
        // line 34
        if (($this->getAttribute($this->getContext($context, "hotel"), "accesoAdaptado") == 1)) {
            // line 35
            echo "                    <li class=\"acceso-adaptado\">Acceso adaptado
                    </li>
                ";
        }
        // line 38
        echo "
                ";
        // line 39
        if (($this->getAttribute($this->getContext($context, "hotel"), "aceptanPerros") == 1)) {
            // line 40
            echo "                    <li class=\"perros\">Aceptan perros
                    </li>
                ";
        }
        // line 43
        echo "
                ";
        // line 44
        if (($this->getAttribute($this->getContext($context, "hotel"), "aparcamiento") == 1)) {
            // line 45
            echo "                    <li class=\"parking\">Aparcamiento/Parking
                    </li>
                ";
        }
        // line 48
        echo "
                ";
        // line 49
        if (($this->getAttribute($this->getContext($context, "hotel"), "businessCenter") == 1)) {
            // line 50
            echo "                    <li class=\"business-center\">Business center
                    </li>
                ";
        }
        // line 53
        echo "            </ul>
            <div class=\"slidehotel owl-carousel\">

                ";
        // line 56
        $context["imagen_hoteles_path"] = $this->getAttribute($this->getContext($context, "hotel"), "getImagePath");
        echo "                    
                ";
        // line 57
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "hotel"), "getOrderedImagenesDelHotel"));
        foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
            // line 58
            echo "                    ";
            $context["ruta_imagen"] = (($this->getContext($context, "imagen_hoteles_path") . "/") . $this->getAttribute($this->getContext($context, "imagen"), "imagen"));
            // line 59
            echo "                    <div>
                        <img src=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->getContext($context, "ruta_imagen"), "html", null, true);
            echo "\">
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 62
        echo "                                     

            </div>
            ";
        // line 65
        if (($this->getAttribute($this->getContext($context, "hotel"), "calificacion") > 0)) {
            // line 66
            echo "                <ul class=\"estrellas\">
                    ";
            // line 67
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($this->getContext($context, "hotel"), "calificacion")));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 68
                echo "                        <li class=\"star\">
                        </li>                
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 71
            echo "                </ul>
            ";
        }
        // line 73
        echo "            <div class=\"clearfix\"></div>
            <!-- description -->
            <div class=\"line\"></div> 
            <p><b>Descripción</b></p>
            <p>";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "descripcion"), "html", null, true);
        echo "</p>
            <p><b>Características</b></p>
            <p>";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "caracteristicas"), "html", null, true);
        echo "</p>
            <div class=\"mapa\"> ";
        // line 81
        echo "                <div class=\"direccion\">
                    <p>";
        // line 82
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "ubicacion"), "html", null, true);
        echo "</p>
                </div>
                <div class=\"mapa-detalle\">
                    <div id=\"mapa_det\" style=\"height:290px;\" data-longitud=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "longitud"), "html", null, true);
        echo "\" data-latitud=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "latitud"), "html", null, true);
        echo "\" data-titulo=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "nombrehotel"), "html", null, true);
        echo "\"></div>
                </div><!-- /mapa -->
            </div>
        </div>
    </div>

";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:hotel.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  224 => 85,  218 => 82,  215 => 81,  211 => 79,  206 => 77,  200 => 73,  196 => 71,  188 => 68,  184 => 67,  181 => 66,  179 => 65,  174 => 62,  165 => 60,  162 => 59,  159 => 58,  155 => 57,  151 => 56,  146 => 53,  141 => 50,  139 => 49,  136 => 48,  131 => 45,  129 => 44,  126 => 43,  121 => 40,  119 => 39,  116 => 38,  111 => 35,  109 => 34,  106 => 33,  101 => 30,  99 => 29,  96 => 28,  91 => 25,  89 => 24,  86 => 23,  81 => 20,  79 => 19,  74 => 17,  69 => 14,  66 => 13,  59 => 11,  56 => 10,  40 => 6,  36 => 5,  31 => 4,  28 => 3,);
    }
}
