<?php

/* HotelesBackendBundle:Newsletter:index.html.twig */
class __TwigTemplate_4d4c2ecc0519b915396e4115caf693b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de newsletter"), "html", null, true);
        echo "
";
    }

    // line 7
    public function block_javascripts($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 9
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "636ee4d_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_636ee4d_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/636ee4d_part_1_newsletter_1.js");
            // line 10
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "636ee4d"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_636ee4d") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/636ee4d.js");
            echo "    <script type=\"text/javascript\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        // line 15
        echo "

    <div class=\"row\">
        <div class=\"col-lg-12\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de newsletter"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <!--  buscador + rtdos -->
                    <div class=\"adv-table\">
                        <div class=\"row\">
                            <div class=\"col-lg-6\">
                                <div id=\"editable-sample_length\" class=\"dataTables_length\">
                                    ";
        // line 29
        $this->env->loadTemplate("HotelesBackendBundle:Extras:select.html.twig")->display($context);
        echo "     
                                </div>
                            </div>
                            <div class=\"col-lg-6\">
                                <div class=\"dataTables_filter\" id=\"editable-sample_filter\">
                                    <form action=\"\" method=\"POST\" id=\"form-buscar-newsletter\">
                                        <label>";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Buscar"), "html", null, true);
        echo ": <input type=\"text\" name=\"email\" aria-controls=\"editable-sample\" class=\"form-control medium buscar\"></label>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- / buscador + rtdos -->
                        <!--  contentTabla -->
                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-advance table-hover\" id=\"hidden-table-info\">

                            <thead>
                                <tr>
                                    <th>";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Id"), "html", null, true);
        echo "</th>   
                                    <th>";
        // line 47
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuario"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("E-mail"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha Alta"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Activo"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha baja"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acciones"), "html", null, true);
        echo "</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 56
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "entities"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["newsletter"]) {
            // line 57
            echo "
                                    <tr data-newsletter-id=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "newsletter"), "id"), "html", null, true);
            echo "\">
                                        <td>";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "loop"), "index"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "newsletter"), "usuario"), "getNombreCompleto"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "newsletter"), "usuario"), "email"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 62
            if ($this->getAttribute($this->getContext($context, "newsletter"), "fecha")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "newsletter"), "fecha"), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                                        <td class=\"status\"> ";
            // line 63
            $this->env->loadTemplate("HotelesBackendBundle:Newsletter:status.html.twig")->display(array_merge($context, array("activo" => $this->getAttribute($this->getContext($context, "newsletter"), "activo"))));
            echo "</td>
                                        <td>";
            // line 64
            if ($this->getAttribute($this->getContext($context, "newsletter"), "fechadisabled")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "newsletter"), "fechadisabled"), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                                        <td>
                                            ";
            // line 66
            if (($this->getAttribute($this->getContext($context, "newsletter"), "activo") == 1)) {
                // line 67
                echo "                                                <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_newsletter_delete", array("id" => $this->getAttribute($this->getContext($context, "newsletter"), "id"))), "html", null, true);
                echo "\" class=\"btn btn-primary btn-xs unsuscribe-newsletter\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dar de baja"), "html", null, true);
                echo "\">
                                                    <i class=\"fa fa-times\"></i>
                                                </a>
                                            ";
            } else {
                // line 71
                echo "                                                <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_newsletter_activate", array("id" => $this->getAttribute($this->getContext($context, "newsletter"), "id"))), "html", null, true);
                echo "\" class=\"btn btn-primary btn-xs suscribe-newsletter\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dar de alta"), "html", null, true);
                echo "\">
                                                    <i class=\"fa fa-check\"></i>
                                                </a>
                                            ";
            }
            // line 75
            echo "                                        </td>                                   
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['newsletter'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 78
        echo "                            </tbody>
                        </table>
                        <!--  /contentTabla -->

                    </div> <!-- /addtable -->
                </div><!-- /panelbody -->

            </section>

            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <a href=\"";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("hoteles_backend_export_exportnewsletter"), "html", null, true);
        echo "\" class=\"btn btn-success\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Exportar"), "html", null, true);
        echo "</a>
                </div>
            </section>
            <!-- /btns -->

        </div><!-- /col -->

    </div><!-- /row -->



";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Newsletter:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  251 => 90,  237 => 78,  221 => 75,  211 => 71,  201 => 67,  199 => 66,  192 => 64,  188 => 63,  182 => 62,  178 => 61,  174 => 60,  170 => 59,  166 => 58,  163 => 57,  146 => 56,  139 => 52,  135 => 51,  131 => 50,  127 => 49,  123 => 48,  119 => 47,  115 => 46,  101 => 35,  92 => 29,  81 => 21,  73 => 15,  70 => 14,  54 => 10,  50 => 9,  45 => 8,  42 => 7,  36 => 5,  31 => 4,  28 => 3,);
    }
}
