<?php

/* HotelesBackendBundle:User:status.html.twig */
class __TwigTemplate_fa6fc192bda993c51b83ccc5d337b230 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getContext($context, "status") == 0)) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Habilitado"), "html", null, true);
            echo "
";
        } elseif (($this->getContext($context, "status") == 1)) {
            // line 4
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cuenta suspendida"), "html", null, true);
            echo "
";
        } elseif (($this->getContext($context, "status") == 2)) {
            // line 6
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cuenta baneada"), "html", null, true);
            echo "
";
        } elseif (($this->getContext($context, "status") == 3)) {
            // line 8
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta pendiente"), "html", null, true);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:User:status.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 8,  53 => 12,  50 => 11,  31 => 6,  25 => 4,  21 => 3,  19 => 2,  17 => 1,  508 => 201,  503 => 198,  491 => 185,  475 => 182,  467 => 179,  463 => 177,  455 => 172,  450 => 171,  442 => 166,  437 => 165,  435 => 164,  429 => 161,  426 => 160,  423 => 159,  420 => 158,  414 => 155,  407 => 154,  405 => 153,  400 => 152,  394 => 149,  389 => 148,  383 => 145,  378 => 144,  376 => 143,  371 => 141,  365 => 140,  360 => 138,  355 => 137,  353 => 132,  350 => 131,  347 => 130,  341 => 127,  336 => 126,  334 => 125,  329 => 124,  323 => 121,  318 => 120,  312 => 117,  307 => 116,  305 => 115,  300 => 113,  294 => 112,  289 => 110,  284 => 109,  282 => 104,  279 => 103,  276 => 102,  270 => 99,  263 => 98,  261 => 97,  256 => 96,  250 => 93,  245 => 92,  239 => 89,  234 => 88,  232 => 87,  227 => 85,  221 => 84,  216 => 82,  211 => 81,  209 => 76,  206 => 75,  202 => 74,  195 => 72,  192 => 71,  186 => 70,  180 => 68,  174 => 66,  168 => 64,  162 => 62,  159 => 61,  155 => 60,  151 => 58,  145 => 56,  139 => 54,  137 => 53,  132 => 51,  126 => 50,  123 => 49,  106 => 48,  98 => 43,  94 => 42,  90 => 41,  86 => 40,  82 => 39,  78 => 38,  69 => 31,  64 => 23,  62 => 22,  51 => 14,  44 => 9,  41 => 8,  35 => 9,  30 => 4,  27 => 6,);
    }
}
