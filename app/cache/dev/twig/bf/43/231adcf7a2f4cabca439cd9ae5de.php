<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_bf43231adcf7a2f4cabca439cd9ae5de extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\"/>
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/css/exception_layout.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/css/exception.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
    </head>
    <body>
        <div id=\"content\">
            <div class=\"header clear_fix\">
                <div class=\"header_logo\">
                    <img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/images/logo_symfony.gif"), "html", null, true);
        echo "\" alt=\"Symfony\" />
                </div>

                <div class=\"search\">
                  <form method=\"get\" action=\"http://symfony.com/search\">
                    <div class=\"form_row\">

                      <label for=\"search_id\">
                          <img src=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/images/grey_magnifier.png"), "html", null, true);
        echo "\" alt=\"Search on Symfony website\" />
                      </label>

                      <input name=\"q\" id=\"search_id\" type=\"search\" placeholder=\"Search on Symfony website\" />

                      <button type=\"submit\">
                        <span class=\"border_l\">
                          <span class=\"border_r\">
                            <span class=\"btn_bg\">OK</span>
                          </span>
                        </span>
                      </button>
                    </div>
                   </form>
                </div>
            </div>

            ";
        // line 39
        $this->displayBlock('body', $context, $blocks);
        // line 40
        echo "        </div>
    </body>
</html>
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "";
    }

    // line 39
    public function block_body($context, array $blocks = array())
    {
        echo "";
    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 6,  46 => 14,  37 => 8,  33 => 7,  24 => 4,  19 => 1,  44 => 8,  30 => 4,  27 => 3,  1000 => 291,  995 => 290,  993 => 289,  990 => 288,  974 => 284,  952 => 283,  950 => 282,  947 => 281,  935 => 276,  931 => 275,  926 => 274,  924 => 273,  921 => 272,  912 => 266,  906 => 264,  903 => 263,  898 => 262,  896 => 261,  893 => 260,  886 => 255,  877 => 253,  873 => 252,  870 => 251,  867 => 250,  865 => 249,  862 => 248,  854 => 244,  852 => 243,  849 => 242,  842 => 237,  839 => 236,  831 => 231,  827 => 230,  823 => 229,  820 => 228,  818 => 227,  815 => 226,  807 => 222,  805 => 221,  802 => 220,  794 => 214,  792 => 213,  789 => 212,  781 => 208,  778 => 207,  776 => 206,  773 => 205,  752 => 201,  749 => 200,  746 => 199,  743 => 198,  741 => 197,  738 => 196,  730 => 190,  727 => 189,  725 => 188,  722 => 187,  715 => 184,  712 => 183,  709 => 182,  701 => 178,  698 => 177,  696 => 176,  693 => 175,  677 => 171,  674 => 170,  672 => 169,  669 => 168,  661 => 164,  658 => 163,  645 => 157,  642 => 156,  640 => 155,  637 => 154,  629 => 150,  626 => 149,  624 => 148,  621 => 147,  613 => 143,  611 => 142,  608 => 141,  597 => 136,  595 => 135,  592 => 134,  584 => 130,  581 => 129,  579 => 128,  577 => 127,  574 => 126,  567 => 121,  559 => 120,  554 => 119,  548 => 117,  545 => 116,  543 => 115,  532 => 108,  530 => 104,  525 => 103,  519 => 101,  516 => 100,  514 => 99,  511 => 98,  502 => 92,  498 => 91,  494 => 90,  490 => 89,  485 => 88,  479 => 86,  476 => 85,  471 => 83,  455 => 79,  453 => 78,  450 => 77,  434 => 73,  432 => 72,  429 => 71,  419 => 65,  416 => 64,  413 => 63,  405 => 60,  400 => 59,  397 => 58,  388 => 55,  386 => 54,  378 => 53,  366 => 49,  361 => 48,  352 => 46,  349 => 45,  347 => 44,  344 => 43,  335 => 39,  323 => 37,  319 => 35,  304 => 33,  300 => 32,  295 => 31,  292 => 30,  287 => 29,  285 => 28,  282 => 27,  272 => 23,  267 => 21,  259 => 17,  256 => 16,  250 => 14,  248 => 13,  237 => 7,  233 => 6,  228 => 5,  226 => 4,  223 => 3,  219 => 288,  216 => 287,  214 => 281,  209 => 272,  206 => 271,  201 => 260,  196 => 248,  191 => 242,  188 => 241,  185 => 239,  180 => 235,  178 => 226,  175 => 225,  173 => 220,  170 => 219,  167 => 217,  162 => 211,  160 => 205,  155 => 196,  152 => 195,  149 => 193,  144 => 186,  142 => 182,  132 => 168,  129 => 167,  119 => 153,  114 => 146,  112 => 141,  107 => 134,  104 => 133,  99 => 125,  92 => 39,  89 => 97,  87 => 83,  84 => 82,  82 => 77,  77 => 39,  74 => 70,  72 => 43,  69 => 42,  67 => 27,  64 => 26,  62 => 21,  59 => 20,  57 => 22,  52 => 3,  284 => 128,  270 => 22,  261 => 122,  257 => 120,  253 => 15,  251 => 117,  225 => 93,  222 => 92,  218 => 90,  215 => 89,  208 => 70,  204 => 37,  190 => 35,  186 => 34,  171 => 23,  159 => 13,  153 => 12,  147 => 187,  141 => 157,  139 => 181,  136 => 91,  122 => 154,  117 => 147,  108 => 71,  81 => 47,  79 => 40,  70 => 40,  66 => 39,  63 => 38,  61 => 23,  49 => 2,  36 => 10,  25 => 1,  666 => 339,  662 => 337,  659 => 336,  656 => 162,  653 => 161,  651 => 333,  635 => 320,  618 => 305,  609 => 303,  603 => 302,  600 => 137,  596 => 300,  582 => 289,  569 => 279,  562 => 275,  549 => 265,  544 => 263,  540 => 114,  535 => 260,  524 => 252,  517 => 250,  507 => 243,  500 => 238,  497 => 237,  483 => 226,  474 => 84,  467 => 216,  462 => 214,  457 => 212,  452 => 210,  447 => 208,  442 => 206,  433 => 202,  427 => 201,  411 => 188,  407 => 61,  399 => 182,  394 => 57,  389 => 178,  384 => 176,  379 => 174,  374 => 51,  369 => 170,  364 => 168,  357 => 47,  350 => 162,  328 => 143,  324 => 142,  316 => 137,  310 => 134,  266 => 125,  262 => 92,  245 => 12,  240 => 76,  235 => 74,  231 => 73,  211 => 280,  203 => 269,  198 => 259,  193 => 247,  183 => 236,  179 => 47,  174 => 24,  165 => 212,  157 => 204,  148 => 31,  145 => 30,  137 => 175,  134 => 174,  127 => 161,  124 => 160,  118 => 26,  115 => 25,  109 => 140,  106 => 70,  102 => 126,  97 => 114,  94 => 113,  54 => 11,  50 => 11,  45 => 13,  41 => 7,  32 => 3,  29 => 6,);
    }
}
