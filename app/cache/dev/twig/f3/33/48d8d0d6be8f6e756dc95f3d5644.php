<?php

/* HotelesBackendBundle:Oferta:index.html.twig */
class __TwigTemplate_f33348d8d0d6be8f6e756dc95f3d5644 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'javascripts' => array($this, 'block_javascripts'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de ofertas"), "html", null, true);
        echo "
";
    }

    // line 7
    public function block_javascripts($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        if (typeof ROOMASTIC === 'undefined') {
            ROOMASTIC = {};
            if (typeof ROOMASTIC.filtro_ofertas === 'undefined') {
                ROOMASTIC.filtro_ofertas = {};
            }
        }
        ROOMASTIC.filtro_ofertas.filtro = '";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "request"), "attributes"), "get", array(0 => "filtro"), "method"), "html", null, true);
        echo "';
        ROOMASTIC.filtro_ofertas.id_hotel = '";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "request"), "attributes"), "get", array(0 => "id_hotel"), "method"), "html", null, true);
        echo "';

    </script>
    ";
        // line 20
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "7cd9f50_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7cd9f50_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/7cd9f50_part_1_listadoOfertas_1.js");
            echo "      
    <script type=\"text/javascript\" src=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "7cd9f50"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7cd9f50") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/7cd9f50.js");
            // line 20
            echo "      
    <script type=\"text/javascript\" src=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 25
    public function block_body($context, array $blocks = array())
    {
        // line 26
        echo "

    <div class=\"row\">
        <div class=\"col-lg-12\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de ofertas"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <!--  buscador + rtdos -->
                    <div class=\"adv-table\">
                        <div class=\"row\">
                            <div class=\"col-lg-9\">
                                <div id=\"editable-sample_length\" class=\"dataTables_length\">
                                    ";
        // line 40
        $this->env->loadTemplate("HotelesBackendBundle:Extras:select.html.twig")->display($context);
        // line 41
        echo "                                </div>

                            </div>
                            ";
        // line 44
        if (($this->env->getExtension('security')->isGranted("ROLE_EMPRESA") || $this->env->getExtension('security')->isGranted("ROLE_ADMIN"))) {
            // line 45
            echo "                                <div class=\"col-lg-3\">
                                    <div class=\"dataTables_length_hotel\">
                                        <label>
                                            ";
            // line 48
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de hoteles"), "html", null, true);
            echo "
                                            <select size=\"1\" name=\"editable-sample_length\" aria-controls=\"editable-sample\" class=\"form-control xsmall\" id=\"hotelSelect\">
                                                ";
            // line 50
            $context["hotel_seleccionado"] = $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "request"), "attributes"), "get", array(0 => "id_hotel"), "method");
            // line 51
            echo "                                                <option value=\"\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Todos"), "html", null, true);
            echo "</option>
                                                ";
            // line 52
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
            foreach ($context['_seq'] as $context["_key"] => $context["hotel"]) {
                // line 53
                echo "                                                    <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "id"), "html", null, true);
                echo "\" ";
                if (($this->getAttribute($this->getContext($context, "hotel"), "id") == $this->getContext($context, "hotel_seleccionado"))) {
                    echo "selected=\"selected\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "nombrehotel"), "html", null, true);
                echo "</option>
                                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hotel'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 55
            echo "                                            </select> 

                                        </label>
                                    </div>
                                </div>
                            ";
        }
        // line 61
        echo "
                            <!-- / buscador + rtdos -->
                            <!--  contentTabla -->
                            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-advance table-hover\" id=\"hidden-table-info\">

                                <thead>
                                    <tr>
                                        ";
        // line 69
        echo "                                        <th>";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha oferta"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Precio habitación"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Entrada"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Salida"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Noches"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 75
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Adultos"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Niños"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hab."), "html", null, true);
        echo "</th>
                                        <th>";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Importe Total"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha contraoferta"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Total contraoferta"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comision"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Neto"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado"), "html", null, true);
        echo "</th>
                                        <th>";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acciones"), "html", null, true);
        echo "</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    ";
        // line 89
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "pagination"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["oferta"]) {
            // line 90
            echo "                                        <tr>
                                            ";
            // line 92
            echo "                                            <td>";
            if ($this->getAttribute($this->getContext($context, "oferta"), "fecha")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "fecha"), "d-m-Y H:i:s"), "html", null, true);
            }
            echo "</td>
                                            <td>";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "hotel"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 94
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "preciohabitacion"), 2, ",", "."), "html", null, true);
            echo "</td>
                                            <td>";
            // line 95
            if ($this->getAttribute($this->getContext($context, "oferta"), "fechain")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "fechain"), "d-m-Y"), "html", null, true);
            }
            echo "</td>
                                            <td>";
            // line 96
            if ($this->getAttribute($this->getContext($context, "oferta"), "fechaout")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "fechaout"), "d-m-Y"), "html", null, true);
            }
            echo "</td>
                                            <td>";
            // line 97
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "numnoches"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 98
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "numadultos"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 99
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "numninos"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 100
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "numhabitaciones"), "html", null, true);
            echo "</td>
                                            <td>";
            // line 101
            echo twig_escape_filter($this->env, (twig_number_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "preciototaloferta"), 2, ",", ".") . $this->env->getExtension('translator')->trans("€")), "html", null, true);
            echo "</td>
                                            <td>";
            // line 102
            if (($this->getAttribute($this->getContext($context, "oferta"), "fechacontraoferta") != null)) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "fechacontraoferta"), "d-m-y H:i"), "html", null, true);
            }
            echo "</td>
                                            <td>";
            // line 103
            echo twig_escape_filter($this->env, (twig_number_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "preciototalcontraoferta"), 2, ",", ".") . $this->env->getExtension('translator')->trans("€")), "html", null, true);
            echo "</td>
                                            <td>";
            // line 104
            echo twig_escape_filter($this->env, (twig_number_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "getComisionOferta"), 2, ",", ".") . "%"), "html", null, true);
            echo "</td>
                                            <td style=\"font-weight: 900;\">";
            // line 105
            echo twig_escape_filter($this->env, (twig_number_format_filter($this->env, $this->getAttribute($this->getContext($context, "oferta"), "getNeto"), 2, ",", ".") . $this->env->getExtension('translator')->trans("€")), "html", null, true);
            echo "</td>
                                            <td>
                                                ";
            // line 107
            $this->env->loadTemplate("HotelesBackendBundle:Oferta:status.html.twig")->display(array_merge($context, array("status" => $this->getAttribute($this->getContext($context, "oferta"), "status"))));
            // line 108
            echo "                                            </td>
                                            <td>
                                                ";
            // line 113
            echo "                                                ";
            if ($this->getAttribute($this->getContext($context, "oferta"), "canHotelInteract")) {
                // line 114
                echo "                                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("acepta_oferta", array("id" => $this->getAttribute($this->getContext($context, "oferta"), "id"))), "html", null, true);
                echo "\" style=\"margin: 3px 0;\" class=\"btn btn-primary btn-xs\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptar"), "html", null, true);
                echo "\"><i class=\"fa fa-check\"></i>  ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aceptar"), "html", null, true);
                echo "</a> <br>
                                                    <a href=\"";
                // line 115
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("declina_oferta", array("id" => $this->getAttribute($this->getContext($context, "oferta"), "id"))), "html", null, true);
                echo "\" style=\"margin: 3px 0;\" class=\"btn btn-primary btn-xs\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Declinar"), "html", null, true);
                echo "\"><i class=\"fa fa-times\"></i>  ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Declinar"), "html", null, true);
                echo "</a><br>
                                                    <a href=\"";
                // line 116
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_ofertas_contraoferta", array("id" => $this->getAttribute($this->getContext($context, "oferta"), "id"))), "html", null, true);
                echo "\" style=\"margin: 3px 0;\" class=\"btn btn-primary btn-xs\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraofertar"), "html", null, true);
                echo "\"><i class=\"fa fa-usd\"></i>  ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contra"), "html", null, true);
                echo "</a>
                                                ";
            } elseif ((((null === $this->getAttribute($this->getContext($context, "oferta"), "sale")) == false) && $this->getAttribute($this->getAttribute($this->getContext($context, "oferta"), "sale"), "isPayed", array(), "method"))) {
                // line 118
                echo "                                                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("hoteles_backend_oferta_ofertadetails", array("id" => $this->getAttribute($this->getContext($context, "oferta"), "id"))), "html", null, true);
                echo "\" style=\"margin: 3px 0;\" class=\"btn btn-primary btn-xs\" id=\"ver-detalles-oferta\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Detalles de la oferta"), "html", null, true);
                echo "\" data-toggle=\"modal\" data-target=\"#offer-details-modal\" data-remote=\"false\"><i class=\"fa fa-user\"></i>";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Ver detalles usuario"), "html", null, true);
                echo "</a>
                                                ";
            } else {
                // line 120
                echo "                                                    
                                                ";
            }
            // line 121
            echo "                                         
                                            </td>
                                        </tr>
                                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oferta'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 125
        echo "                                </tbody>
                            </table>
                            <!--  /contentTabla -->

                        </div> <!-- /addtable -->
                    </div><!-- /panelbody -->

            </section>

            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"span6\">   
                    </div>
                    <div class=\"span6\">
                        <div class=\"dataTables_paginate paging_bootstrap pagination withoutbuttons\">
                            ";
        // line 141
        echo $this->env->getExtension('knp_pagination')->render($this->getContext($context, "pagination"), "HotelesBackendBundle:Paginacion:paginacion.html.twig");
        echo "
                        </div>
                    </div>                  
                </div>          
            </section>
            <!-- /btns -->


        </div><!-- /col -->

    </div><!-- /row -->
    <div class=\"modal fade\" id=\"offer-details-modal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"offer-details-modal-label\" aria-hidden=\"true\">
        <div class=\"modal-dialog\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\" id=\"fee-details-label\">";
        // line 157
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Detalles de la oferta"), "html", null, true);
        echo "</h4>
                </div>
                <div class=\"modal-body\">
                    <table class=\"table table-condensed\">

                        <tr>
                            <th>";
        // line 163
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
        echo "</th>
                            <td id=\"nombre_completo\"></td>
                        </tr>
                        <tr>
                            <th>";
        // line 167
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</th>
                            <td id=\"email\"></td>
                        </tr>
                        <tr>
                            <th>";
        // line 171
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("DNI"), "html", null, true);
        echo "</th>
                            <td id=\"dni\"></td>
                        </tr>
                        <tr>
                            <th>";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Direccion"), "html", null, true);
        echo "</th>
                            <td id=\"direccion\"></td>
                        </tr>
                        <tr>
                            <th>";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Telefono"), "html", null, true);
        echo "</th>
                            <td id=\"telefono\"></td>
                        </tr>

                    </table>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Oferta:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  470 => 179,  463 => 175,  456 => 171,  449 => 167,  442 => 163,  433 => 157,  414 => 141,  396 => 125,  379 => 121,  375 => 120,  365 => 118,  356 => 116,  348 => 115,  339 => 114,  336 => 113,  332 => 108,  330 => 107,  325 => 105,  321 => 104,  317 => 103,  311 => 102,  307 => 101,  303 => 100,  299 => 99,  295 => 98,  291 => 97,  285 => 96,  279 => 95,  275 => 94,  271 => 93,  264 => 92,  261 => 90,  244 => 89,  236 => 84,  232 => 83,  228 => 82,  224 => 81,  220 => 80,  216 => 79,  212 => 78,  208 => 77,  204 => 76,  200 => 75,  196 => 74,  192 => 73,  188 => 72,  184 => 71,  180 => 70,  175 => 69,  166 => 61,  158 => 55,  143 => 53,  139 => 52,  134 => 51,  132 => 50,  127 => 48,  122 => 45,  120 => 44,  115 => 41,  113 => 40,  102 => 32,  94 => 26,  91 => 25,  83 => 21,  80 => 20,  73 => 21,  67 => 20,  61 => 17,  57 => 16,  45 => 8,  42 => 7,  36 => 5,  31 => 4,  28 => 3,);
    }
}
