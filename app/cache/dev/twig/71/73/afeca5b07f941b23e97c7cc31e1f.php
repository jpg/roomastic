<?php

/* HotelesBackendBundle:NotificacionesHeader:contacto.html.twig */
class __TwigTemplate_7173afeca5b07f941b23e97c7cc31e1f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<li>
    <a href=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb_edit", array("id" => $this->getAttribute($this->getContext($context, "contacto"), "id"))), "html", null, true);
        echo "\">
        <span class=\"photo\"><img alt=\"avatar\" src=\"/bundles/hotelesbackend/img/avatar.jpg\"></span>
        <span class=\"subject\">
            <span class=\"from\">";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "contacto"), "getNombreCompleto"), "html", null, true);
        echo "</span>
            <span class=\"time\">";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('time_ago_extension')->timeAgoInWordsFilter($this->getAttribute($this->getContext($context, "contacto"), "fecha")), "html", null, true);
        echo "</span>
        </span>
        <span class=\"message\">
            ";
        // line 9
        echo twig_escape_filter($this->env, (((twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "contacto"), "texto")) > 30)) ? ((twig_slice($this->env, $this->getAttribute($this->getContext($context, "contacto"), "texto"), 0, 30) . "...")) : ($this->getAttribute($this->getContext($context, "contacto"), "texto"))), "html", null, true);
        echo "
        </span>
    </a>
</li>";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:NotificacionesHeader:contacto.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  36 => 9,  30 => 6,  26 => 5,  20 => 2,  17 => 1,);
    }
}
