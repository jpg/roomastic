<?php

/* HotelesBackendBundle:Oferta:status.html.twig */
class __TwigTemplate_d46cf07580878386fbb52e57fa205224 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 26
        if (($this->getContext($context, "status") == "0")) {
            // line 27
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta pendiente"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "1")) {
            // line 29
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta superior al PVP"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "2")) {
            // line 31
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta inferior al minimo"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "10")) {
            // line 33
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraoferta realizada"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "11")) {
            // line 35
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraoferta rechazada"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "12")) {
            // line 37
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraoferta caducada"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "13")) {
            // line 39
            echo "    <span class=\"label label-success\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraoferta pagada"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "14")) {
            // line 41
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta aceptada por otro hotel"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "20")) {
            // line 43
            echo "    <span class=\"label label-success\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta pagada"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "21")) {
            // line 45
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta NO pagada"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "22")) {
            // line 47
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta rechazada"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "23")) {
            // line 49
            echo "    <span class=\"label label-primary\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta caducada"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "24")) {
            // line 51
            echo "    <span class=\"label label-success \">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta aceptada"), "html", null, true);
            echo "</span>
";
        } elseif (($this->getContext($context, "status") == "25")) {
            // line 53
            echo "    <span class=\"label label-success \">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Oferta aceptada por otro hotel"), "html", null, true);
            echo "</span>
";
        } else {
            // line 55
            echo "    <span class=\"label label-info \">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado no reconocido"), "html", null, true);
            echo "</span>    
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Oferta:status.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 55,  97 => 53,  85 => 49,  79 => 47,  55 => 39,  49 => 37,  43 => 35,  37 => 33,  25 => 29,  19 => 27,  17 => 26,  470 => 179,  463 => 175,  456 => 171,  449 => 167,  442 => 163,  433 => 157,  414 => 141,  396 => 125,  379 => 121,  375 => 120,  365 => 118,  356 => 116,  348 => 115,  339 => 114,  336 => 113,  332 => 108,  330 => 107,  325 => 105,  321 => 104,  317 => 103,  311 => 102,  307 => 101,  303 => 100,  299 => 99,  295 => 98,  291 => 97,  285 => 96,  279 => 95,  275 => 94,  271 => 93,  264 => 92,  261 => 90,  244 => 89,  236 => 84,  232 => 83,  228 => 82,  224 => 81,  220 => 80,  216 => 79,  212 => 78,  208 => 77,  204 => 76,  200 => 75,  196 => 74,  192 => 73,  188 => 72,  184 => 71,  180 => 70,  175 => 69,  166 => 61,  158 => 55,  143 => 53,  139 => 52,  134 => 51,  132 => 50,  127 => 48,  122 => 45,  120 => 44,  115 => 41,  113 => 40,  102 => 32,  94 => 26,  91 => 51,  83 => 21,  80 => 20,  73 => 45,  67 => 43,  61 => 41,  57 => 16,  45 => 8,  42 => 7,  36 => 5,  31 => 31,  28 => 3,);
    }
}
