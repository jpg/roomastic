<?php

/* HotelesBackendBundle:Extras:hotelStatus.html.twig */
class __TwigTemplate_af75579d22d63869adbe4059e35f8152 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 7
        if (($this->getContext($context, "status") == 0)) {
            // line 8
            echo "    <span class=\"fa-lg\">
        <i class=\"fa fa-2x fa-eye-slash\"></i>
    </span>
";
        } elseif (($this->getContext($context, "status") == 1)) {
            // line 12
            echo "    <span class=\"fa-lg\">
        <i class=\"fa fa-check\"></i>
    </span>
";
        } elseif (($this->getContext($context, "status") == 2)) {
            // line 16
            echo "    <span class=\"fa-stack fa-lg\">
        <i class=\"fa fa-user fa-stack-1x\"></i>
        <i class=\"fa fa-ban fa-stack-2x text-danger\"></i>
    </span>
";
        } else {
            // line 21
            echo "    <span class=\"fa-lg\">
        <i class=\"fa fa-question\"></i>
    </span>
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:hotelStatus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 21,  31 => 16,  25 => 12,  19 => 8,  17 => 7,  217 => 80,  212 => 77,  204 => 75,  202 => 74,  187 => 61,  171 => 58,  163 => 56,  161 => 55,  157 => 53,  155 => 52,  148 => 50,  142 => 49,  138 => 48,  134 => 47,  128 => 46,  125 => 45,  108 => 44,  101 => 40,  97 => 39,  93 => 38,  89 => 37,  85 => 36,  81 => 35,  77 => 34,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
