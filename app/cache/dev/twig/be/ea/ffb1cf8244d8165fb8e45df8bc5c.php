<?php

/* HotelesBackendBundle:User:index.html.twig */
class __TwigTemplate_beeaffb1cf8244d8165fb8e45df8bc5c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de usuarios"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de usuarios"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <!--  buscador + rtdos -->
                    <div class=\"adv-table\">
                        <div class=\"row\">
                            <div class=\"col-lg-6\">
                                <div id=\"editable-sample_length\" class=\"dataTables_length\">
                                    ";
        // line 22
        $this->env->loadTemplate("HotelesBackendBundle:Extras:select.html.twig")->display($context);
        // line 23
        echo "                                </div>
                            </div>
                            <div class=\"col-lg-6\">
                                ";
        // line 31
        echo "                            </div>
                        </div>
                        <!-- / buscador + rtdos -->
                        <!--  contentTabla -->
                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-advance table-hover\">
                            <thead>
                                <tr>
                                    <th>";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de usuario"), "html", null, true);
        echo "</th>
                                    <th class=\"hidden-phone\"> ";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Rol"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Último acceso"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acciones"), "html", null, true);
        echo "</th>
                                </tr>
                            </thead>
                            <tbody>

                                ";
        // line 48
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "pagination"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 49
            echo "                                    <tr>
                                        <td><a href=\"";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_show", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "getPublicUsername"), "html", null, true);
            echo "</a></td>
                                        <td>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "email"), "html", null, true);
            echo "</td>
                                        <td>
                                            ";
            // line 53
            if (($this->getAttribute($this->getContext($context, "user"), "isActive") == 1)) {
                // line 54
                echo "                                                <span class=\"label label-success label-mini\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Activo"), "html", null, true);
                echo "</span>
                                            ";
            } else {
                // line 56
                echo "                                                <span class=\"label label-danger label-mini\">";
                $this->env->loadTemplate("HotelesBackendBundle:User:status.html.twig")->display(array_merge($context, array("status" => $this->getAttribute($this->getContext($context, "user"), "status"))));
                echo "</span>
                                            ";
            }
            // line 58
            echo "                                        </td>
                                        <td>
                                            ";
            // line 60
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "user"), "roles"));
            foreach ($context['_seq'] as $context["_key"] => $context["rol"]) {
                // line 61
                echo "                                                ";
                if (($this->getContext($context, "rol") == "ROLE_ADMIN")) {
                    // line 62
                    echo "                                                    <span class=\"label label-inverse\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administrador"), "html", null, true);
                    echo "</span>    
                                                ";
                } elseif (($this->getContext($context, "rol") == "ROLE_EMPRESA")) {
                    // line 64
                    echo "                                                    <span class=\"label label-info\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
                    echo "</span>
                                                ";
                } elseif (($this->getContext($context, "rol") == "ROLE_HOTEL")) {
                    // line 66
                    echo "                                                    <span class=\"label label-primary\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
                    echo "</span>
                                                ";
                } elseif (($this->getContext($context, "rol") == "ROLE_WEB")) {
                    // line 68
                    echo "                                                    <span class=\"label label-warning\">";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuario Web"), "html", null, true);
                    echo "</span>
                                                ";
                }
                // line 70
                echo "                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rol'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 71
            echo "                                        </td>
                                        <td>";
            // line 72
            if ($this->getAttribute($this->getContext($context, "user"), "lastLogin")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "user"), "lastLogin"), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                                        <td>
                                            ";
            // line 74
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "user"), "roles"));
            foreach ($context['_seq'] as $context["_key"] => $context["rol"]) {
                // line 75
                echo "                                                ";
                if (($this->getContext($context, "rol") == "ROLE_ADMIN")) {
                    // line 76
                    echo "                                                    ";
                    // line 81
                    echo "                                                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_useradministrador_edit", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                    echo "\">
                                                        <button class=\"btn btn-primary btn-xs\" title=\"";
                    // line 82
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar"), "html", null, true);
                    echo "\"><i class=\"fa fa-pencil\"></i></button>
                                                    </a>
                                                    <a href=\"";
                    // line 84
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_deleteget", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                    echo "\" onClick=\"return confirm('";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Está seguro de borrar?"), "html", null, true);
                    echo "');\">
                                                        <button class=\"btn btn-primary btn-xs\" title=\"";
                    // line 85
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Borrar"), "html", null, true);
                    echo "\"><i class=\"fa fa-trash-o\"></i></button>
                                                    </a>
                                                    ";
                    // line 87
                    if (($this->getAttribute($this->getContext($context, "user"), "isActive") != true)) {
                        // line 88
                        echo "                                                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_enabled", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                        echo "\">
                                                            <button class=\"btn btn-primary btn-xs\" title=\"";
                        // line 89
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Activar"), "html", null, true);
                        echo "\"><i class=\"fa fa-check-circle\"></i></button>
                                                        </a>
                                                    ";
                    } else {
                        // line 92
                        echo "                                                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_disabled", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                        echo "\">
                                                            <button class=\"btn btn-primary btn-xs\" title=\"";
                        // line 93
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Desactivar"), "html", null, true);
                        echo "\"><i class=\"fa fa-minus-circle\"></i></button>
                                                        </a>
                                                    ";
                    }
                    // line 96
                    echo "                                                    ";
                    if (($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "username") != $this->getAttribute($this->getContext($context, "user"), "username"))) {
                        echo " 
                                                        ";
                        // line 97
                        if ((!$this->env->getExtension('security')->isGranted("ROLE_PREVIOUS_ADMIN"))) {
                            // line 98
                            echo "                                                            <a href=\"";
                            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing", array("_want_to_be_this_user" => $this->getAttribute($this->getContext($context, "user"), "username"))), "html", null, true);
                            echo "\" onClick=\"return confirm('";
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Está seguro de querer suplantar a ese usuario?"), "html", null, true);
                            echo "');\">
                                                                <button class=\"btn btn-primary btn-xs\" title=\"";
                            // line 99
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suplantar"), "html", null, true);
                            echo "\"><i class=\"fa  fa-male\"></i></button>
                                                            </a>
                                                        ";
                        }
                        // line 102
                        echo "                                                    ";
                    }
                    // line 103
                    echo "                                                ";
                } elseif (($this->getContext($context, "rol") == "ROLE_EMPRESA")) {
                    // line 104
                    echo "                                                    ";
                    // line 109
                    echo "                                                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa_edit", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                    echo "\">
                                                        <button class=\"btn btn-primary btn-xs\" title=\"";
                    // line 110
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar"), "html", null, true);
                    echo "\"><i class=\"fa fa-pencil\"></i></button>
                                                    </a>
                                                    <a href=\"";
                    // line 112
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_deleteget", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                    echo "\" onClick=\"return confirm('";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Está seguro de borrar?"), "html", null, true);
                    echo "');\">
                                                        <button class=\"btn btn-primary btn-xs\" title=\"";
                    // line 113
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Borrar"), "html", null, true);
                    echo "\"><i class=\"fa fa-trash-o\"></i></button>
                                                    </a>
                                                    ";
                    // line 115
                    if (($this->getAttribute($this->getContext($context, "user"), "isActive") != true)) {
                        // line 116
                        echo "                                                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_enabled", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                        echo "\">
                                                            <button class=\"btn btn-primary btn-xs\" title=\"";
                        // line 117
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Activar"), "html", null, true);
                        echo "\"><i class=\"fa fa-check-circle\"></i></button>
                                                        </a>
                                                    ";
                    } else {
                        // line 120
                        echo "                                                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_disabled", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                        echo "\">
                                                            <button class=\"btn btn-primary btn-xs\" title=\"";
                        // line 121
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Desactivar"), "html", null, true);
                        echo "\"><i class=\"fa fa-minus-circle\"></i></button>
                                                        </a>
                                                    ";
                    }
                    // line 124
                    echo "                                                    ";
                    if (($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "username") != $this->getAttribute($this->getContext($context, "user"), "username"))) {
                        echo " 
                                                        ";
                        // line 125
                        if ((!$this->env->getExtension('security')->isGranted("ROLE_PREVIOUS_ADMIN"))) {
                            // line 126
                            echo "                                                            <a href=\"";
                            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing", array("_want_to_be_this_user" => $this->getAttribute($this->getContext($context, "user"), "username"))), "html", null, true);
                            echo "\" onClick=\"return confirm('¿Está seguro de querer suplantar a ese usuario?');\">
                                                                <button class=\"btn btn-primary btn-xs\" title=\"";
                            // line 127
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suplantar"), "html", null, true);
                            echo "\"><i class=\"fa  fa-male\"></i></button>
                                                            </a>
                                                        ";
                        }
                        // line 130
                        echo "                                                    ";
                    }
                    // line 131
                    echo "                                                ";
                } elseif (($this->getContext($context, "rol") == "ROLE_HOTEL")) {
                    // line 132
                    echo "                                                    ";
                    // line 137
                    echo "                                                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_edit", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                    echo "\">
                                                        <button class=\"btn btn-primary btn-xs\" title=\"";
                    // line 138
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar"), "html", null, true);
                    echo "\"><i class=\"fa fa-pencil\"></i></button>
                                                    </a>
                                                    <a href=\"";
                    // line 140
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_deleteget", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                    echo "\" onClick=\"return confirm('";
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Está seguro de borrar?"), "html", null, true);
                    echo "');\">
                                                        <button class=\"btn btn-primary btn-xs\" title=\"";
                    // line 141
                    echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Borrar"), "html", null, true);
                    echo "\"><i class=\"fa fa-trash-o\"></i></button>
                                                    </a>
                                                    ";
                    // line 143
                    if (($this->getAttribute($this->getContext($context, "user"), "isActive") != true)) {
                        // line 144
                        echo "                                                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_enabled", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                        echo "\">
                                                            <button class=\"btn btn-primary btn-xs\" title=\"";
                        // line 145
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Activar"), "html", null, true);
                        echo "\"><i class=\"fa fa-check-circle\"></i></button>
                                                        </a>
                                                    ";
                    } else {
                        // line 148
                        echo "                                                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_disabled", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                        echo "\">
                                                            <button class=\"btn btn-primary btn-xs\" title=\"";
                        // line 149
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Desactivar"), "html", null, true);
                        echo "\"><i class=\"fa fa-minus-circle\"></i></button>
                                                        </a>
                                                    ";
                    }
                    // line 152
                    echo "                                                    ";
                    if (($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "user"), "username") != $this->getAttribute($this->getContext($context, "user"), "username"))) {
                        echo " 
                                                        ";
                        // line 153
                        if ((!$this->env->getExtension('security')->isGranted("ROLE_PREVIOUS_ADMIN"))) {
                            // line 154
                            echo "                                                            <a href=\"";
                            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_landing", array("_want_to_be_this_user" => $this->getAttribute($this->getContext($context, "user"), "username"))), "html", null, true);
                            echo "\" onClick=\"return confirm('";
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Está seguro de querer suplantar a ese usuario?"), "html", null, true);
                            echo "');\">
                                                                <button class=\"btn btn-primary btn-xs\" title=\"";
                            // line 155
                            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suplantar"), "html", null, true);
                            echo "\"><i class=\"fa  fa-male\"></i></button>
                                                            </a>
                                                        ";
                        }
                        // line 158
                        echo "                                                    ";
                    }
                    // line 159
                    echo "                                                ";
                } elseif (($this->getContext($context, "rol") == "ROLE_WEB")) {
                    // line 160
                    echo "
                                                    <a href=\"";
                    // line 161
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_show", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                    echo "\">
                                                        <button class=\"btn btn-success btn-xs\"><i class=\"fa fa-eye\"></i></button>
                                                    </a>  
                                                    ";
                    // line 164
                    if (($this->getAttribute($this->getContext($context, "user"), "isBanned") == false)) {
                        // line 165
                        echo "                                                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_ban", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                        echo "\">
                                                            <button class=\"btn btn-primary btn-xs\" title=\"";
                        // line 166
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Banear a este usuario"), "html", null, true);
                        echo "\">
                                                                <i class=\"fa fa-minus-circle\"></i>
                                                            </button>
                                                        </a>
                                                    ";
                    } else {
                        // line 171
                        echo "                                                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_usuario_unban", array("id" => $this->getAttribute($this->getContext($context, "user"), "id"))), "html", null, true);
                        echo "\">
                                                            <button class=\"btn btn-primary btn-xs\" title=\"";
                        // line 172
                        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Quitar baneo a este usuario"), "html", null, true);
                        echo "\">
                                                                <i class=\"fa fa-check-circle\"></i>
                                                            </button>
                                                        </a>
                                                    ";
                    }
                    // line 177
                    echo "
                                                ";
                }
                // line 179
                echo "

                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rol'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 182
            echo "                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 185
        echo "                            </tbody>
                        </table>
                    </div>

            </section>

            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"span6\">
                        ";
        // line 198
        echo "                    </div>
                    <div class=\"span6\">
                        <div class=\"dataTables_paginate paging_bootstrap pagination withoutbuttons\">
                            ";
        // line 201
        echo $this->env->getExtension('knp_pagination')->render($this->getContext($context, "pagination"), "HotelesBackendBundle:Paginacion:paginacion.html.twig");
        echo "
                        </div>
                    </div>                  
                </div>          
            </section>
            <!-- /btns -->

        </div><!-- /col -->
    </div> <!-- /row -->

";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:User:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  508 => 201,  503 => 198,  491 => 185,  475 => 182,  467 => 179,  463 => 177,  455 => 172,  450 => 171,  442 => 166,  437 => 165,  435 => 164,  429 => 161,  426 => 160,  423 => 159,  420 => 158,  414 => 155,  407 => 154,  405 => 153,  400 => 152,  394 => 149,  389 => 148,  383 => 145,  378 => 144,  376 => 143,  371 => 141,  365 => 140,  360 => 138,  355 => 137,  353 => 132,  350 => 131,  347 => 130,  341 => 127,  336 => 126,  334 => 125,  329 => 124,  323 => 121,  318 => 120,  312 => 117,  307 => 116,  305 => 115,  300 => 113,  294 => 112,  289 => 110,  284 => 109,  282 => 104,  279 => 103,  276 => 102,  270 => 99,  263 => 98,  261 => 97,  256 => 96,  250 => 93,  245 => 92,  239 => 89,  234 => 88,  232 => 87,  227 => 85,  221 => 84,  216 => 82,  211 => 81,  209 => 76,  206 => 75,  202 => 74,  195 => 72,  192 => 71,  186 => 70,  180 => 68,  174 => 66,  168 => 64,  162 => 62,  159 => 61,  155 => 60,  151 => 58,  145 => 56,  139 => 54,  137 => 53,  132 => 51,  126 => 50,  123 => 49,  106 => 48,  98 => 43,  94 => 42,  90 => 41,  86 => 40,  82 => 39,  78 => 38,  69 => 31,  64 => 23,  62 => 22,  51 => 14,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
