<?php

/* HotelesBackendBundle:Empresa:edit.html.twig */
class __TwigTemplate_7b648a14c4668d0dfcf76d4e82079f24 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar empresa"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">

            <!--tab nav start-->
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar empresa"), "html", null, true);
        echo "
                </header>
                <header class=\"panel-heading tab-bg-dark-navy-blue \">
                    <ul class=\"nav nav-tabs\">
                        <li class=\"active ";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "email")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombreempresa")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cif")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "direccionfacturacion")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefono")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "numerocuenta")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#general\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la empresa"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombrepersonacontacto")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "apellidospersonacontacto")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "emailpersonacontacto")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefonopersonacontacto")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cargopersonacontacto")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#api\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la persona de contacto"), "html", null, true);
        echo "</a>
                        </li>
                        ";
        // line 26
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 27
            echo "                            <li class=\"\">
                                <a data-toggle=\"tab\" href=\"#com\">";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comisiones"), "html", null, true);
            echo "</a>
                            </li>
                        ";
        }
        // line 31
        echo "                    </ul>
                </header>
                <div class=\"panel-body\">
                    <div class=\"tab-content\">
                        <div id=\"general\" class=\"tab-pane active\">
                            <form action=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_update", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "edit_form"));
        echo ">
                                ";
        // line 38
        echo "                                <div class=\"form-group  ";
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "email"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 41
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "email"));
        echo "
                                        ";
        // line 42
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "email"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 46
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Recibir aquí todos los emails"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 48
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "receive_mails"));
        echo "
                                        ";
        // line 49
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "receive_mails"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombreempresa"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la empresa"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 55
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombreempresa"));
        echo "
                                        ";
        // line 56
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "nombreempresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cif"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CIF"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 62
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cif"));
        echo "
                                        ";
        // line 63
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "cif"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "direccionfacturacion"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 67
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dirección de facturación"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 69
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "direccionfacturacion"));
        echo "
                                        ";
        // line 70
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "direccionfacturacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefono"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 76
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefono"));
        echo "
                                        ";
        // line 77
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "telefono"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "numerocuenta"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 81
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de cuenta"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 83
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "numerocuenta"));
        echo "
                                        ";
        // line 84
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "numerocuenta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                        </div>
                        <div id=\"api\" class=\"tab-pane\">
                            <div class=\"form-group ";
        // line 89
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombrepersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 90
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 92
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombrepersonacontacto"));
        echo "
                                    ";
        // line 93
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "nombrepersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 96
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "apellidospersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 97
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 99
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "apellidospersonacontacto"));
        echo "
                                    ";
        // line 100
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "apellidospersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 103
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "emailpersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 104
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 106
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "emailpersonacontacto"));
        echo "
                                    ";
        // line 107
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "emailpersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefonopersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 113
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "telefonopersonacontacto"));
        echo "
                                    ";
        // line 114
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "telefonopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 117
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cargopersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 118
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo de la persona contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 120
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "cargopersonacontacto"));
        echo "
                                    ";
        // line 121
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "cargopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                        </div>
                        ";
        // line 125
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 126
            echo "                            <div id=\"com\" class=\"tab-pane\">                                
                                <div class=\"form-group\">
                                    <label for=\"exampleInputEmail1\">";
            // line 128
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Porcentaje de comisión (campo enteros) , (campo decimales)."), "html", null, true);
            echo "</label>
                                    <div class=\"clearfix\"></div>
                                    <div class=\"col-sm-2-dcnt ";
            // line 130
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "comisionempresaentero"))), "html", null, true);
            echo "\">
                                        ";
            // line 131
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "comisionempresaentero"));
            echo "
                                        ";
            // line 132
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "comisionempresaentero"), array("attr" => array("class" => "form-control")));
            echo "                     
                                    </div>
                                    <div class=\"comaclear\" style=\"float:left\"> , </div>
                                    <div class=\"col-sm-2-dcnt ";
            // line 135
            echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "comisionempresadecimal"))), "html", null, true);
            echo "\">  
                                        ";
            // line 136
            echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "comisionempresadecimal"));
            echo "
                                        ";
            // line 137
            echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "comisionempresadecimal"), array("attr" => array("class" => "form-control")));
            echo "                                  
                                    </div>
                                    <div class=\"clearfix\"></div>
                                </div>

                            </div>
                            <div id=\"tpv\" class=\"tab-pane\">tpv</div>
                        ";
        }
        // line 145
        echo "                            ";
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "edit_form"));
        echo "
                    </div>
                </div>
            </section>

            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-info\">";
        // line 152
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    ";
        // line 154
        if (($this->getAttribute((isset($context["entity"]) ? $context["entity"] : null), "status", array(), "any", true, true) && ($this->getAttribute($this->getContext($context, "entity"), "status") == 1))) {
            // line 155
            echo "                        <form action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_reactivate", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
            echo "\" method=\"post\" class=\"btnDelete\">
                            ";
            // line 156
            echo $this->env->getExtension('form')->renderWidget($this->getContext($context, "reactivate_form"));
            echo "
                            <button type=\"submit\" class=\"btn btn-success\" onclick=\"return confirm('¿Está seguro de re-activar esta empresa?');\">";
            // line 157
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Reactivar empresa"), "html", null, true);
            echo "</button>
                        </form>
                    ";
        } else {
            // line 160
            echo "                        <form action=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_delete", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
            echo "\" method=\"post\" class=\"btnDelete\">
                            ";
            // line 161
            echo $this->env->getExtension('form')->renderWidget($this->getContext($context, "delete_form"));
            echo "
                            <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('¿Estás seguro que quieres suspender esta empresa?\\n' +
                                            'Realizar esta acción cancelará todas las ofertas activas sobre todos los hoteles de la empresa y los usuarios y no se podrán reactivar de ninguna forma.');\">";
            // line 163
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suspender"), "html", null, true);
            echo "</button>
                        </form>
                    ";
        }
        // line 166
        echo "                    ";
        // line 167
        echo "
                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Empresa:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  429 => 167,  427 => 166,  421 => 163,  416 => 161,  411 => 160,  405 => 157,  401 => 156,  396 => 155,  394 => 154,  389 => 152,  378 => 145,  367 => 137,  363 => 136,  359 => 135,  353 => 132,  349 => 131,  345 => 130,  340 => 128,  336 => 126,  334 => 125,  327 => 121,  323 => 120,  318 => 118,  314 => 117,  308 => 114,  304 => 113,  299 => 111,  295 => 110,  289 => 107,  285 => 106,  280 => 104,  276 => 103,  270 => 100,  266 => 99,  261 => 97,  257 => 96,  251 => 93,  247 => 92,  242 => 90,  238 => 89,  230 => 84,  226 => 83,  221 => 81,  217 => 80,  211 => 77,  207 => 76,  202 => 74,  198 => 73,  192 => 70,  188 => 69,  183 => 67,  179 => 66,  173 => 63,  169 => 62,  164 => 60,  160 => 59,  154 => 56,  150 => 55,  145 => 53,  141 => 52,  135 => 49,  131 => 48,  126 => 46,  122 => 45,  116 => 42,  112 => 41,  107 => 39,  102 => 38,  96 => 36,  89 => 31,  83 => 28,  80 => 27,  78 => 26,  73 => 24,  69 => 23,  64 => 21,  60 => 20,  53 => 16,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
