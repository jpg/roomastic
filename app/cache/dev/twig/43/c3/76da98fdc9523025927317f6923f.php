<?php

/* HotelesBackendBundle:UserHotel:index.html.twig */
class __TwigTemplate_43c376da98fdc9523025927317f6923f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de usuarios del hotel"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-12\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de usuarios del hotel"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <table class=\"table table-striped table-advance table-hover\">
                        <thead>
                            <tr>
                                <th></th>
                                <th>";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de usuario"), "html", null, true);
        echo "</th>
                                <th>";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</th>
                                <th>";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado"), "html", null, true);
        echo "</th>
                                <th>";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Administrador"), "html", null, true);
        echo "</th>
                                <th>";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
        echo "</th>
                                <th>";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Hotel"), "html", null, true);
        echo "</th>                                 
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                            ";
        // line 32
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "pagination"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 33
            echo "                                <tr>
                                    <td>
                                        ";
            // line 35
            $this->env->loadTemplate("HotelesBackendBundle:Extras:thumbs.html.twig")->display(array_merge($context, array("dato" => $this->getAttribute($this->getContext($context, "entity"), "imagen"))));
            // line 36
            echo "                                    </td>                                      
                                    <td><a href=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_edit", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "getPublicUsername"), "html", null, true);
            echo "</a></td>
                                    <td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "email"), "html", null, true);
            echo "</td>
                                    <td>
                                        ";
            // line 40
            $this->env->loadTemplate("HotelesBackendBundle:Extras:checked.html.twig")->display(array_merge($context, array("dato" => $this->getAttribute($this->getContext($context, "entity"), "enabled"))));
            // line 41
            echo "                                    </td>
                                    <td>";
            // line 42
            if ($this->getAttribute($this->getContext($context, "entity"), "administrador")) {
                echo " ";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si"), "html", null, true);
            } else {
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No"), "html", null, true);
            }
            echo "</td>
                                    <td>";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "entity"), "hotel"), "empresa"), "html", null, true);
            echo "</td>
                                    <td>";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "hotel"), "html", null, true);
            echo "</td>            
                                    <td>
                                        <a href=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_edit", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-xs\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar"), "html", null, true);
            echo "\"><i class=\"fa fa-pencil\"></i></a>
                                    </td>
                                </tr>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 50
        echo "                        </tbody>
                    </table>
                </div>
            </section>
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"span6\">
                        <a href=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userhotel_new"), "html", null, true);
        echo "\" class=\"btn btn-success\">
                            ";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear un nuevo usuario hotel"), "html", null, true);
        echo "
                        </a>
                    </div>
                    <div class=\"span6\">
                        <div class=\"dataTables_paginate paging_bootstrap pagination withbuttons\">
                            ";
        // line 63
        echo $this->env->getExtension('knp_pagination')->render($this->getContext($context, "pagination"), "HotelesBackendBundle:Paginacion:paginacion.html.twig");
        echo "
                        </div>
                    </div>                  
                </div>          
            </section>
        </div>
    </div>    

";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:UserHotel:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 63,  183 => 58,  179 => 57,  170 => 50,  150 => 46,  145 => 44,  141 => 43,  132 => 42,  129 => 41,  127 => 40,  122 => 38,  116 => 37,  113 => 36,  111 => 35,  107 => 33,  90 => 32,  81 => 26,  77 => 25,  73 => 24,  69 => 23,  65 => 22,  61 => 21,  51 => 14,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
