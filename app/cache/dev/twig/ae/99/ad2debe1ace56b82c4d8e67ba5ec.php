<?php

/* FOSUserBundle:Resetting:checkEmail.html.twig */
class __TwigTemplate_ae99ad2debe1ace56b82c4d8e67ba5ec extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("FOSUserBundle::layout.html.twig");

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FOSUserBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 4
        echo "
\t<form class=\"form-signin\">
        <h2 class=\"form-signin-heading no-icon\">";
        // line 6
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Aviso"), "html", null, true);
        echo "</h2>
        <div class=\"login-wrap\">
            <p>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comprueba tu e-mail.", array("%email%" => $this->getContext($context, "email")), "FOSUserBundle"), "html", null, true);
        echo "</p>
        </div>
    </form>

";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:checkEmail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 8,  33 => 6,  29 => 4,  26 => 3,);
    }
}
