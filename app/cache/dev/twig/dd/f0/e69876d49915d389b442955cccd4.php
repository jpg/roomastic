<?php

/* HotelesBackendBundle:Lugar:sacalugares.html.twig */
class __TwigTemplate_ddf0e69876d49915d389b442955cccd4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getContext($context, "obligatorio") == 0)) {
            // line 2
            echo "    <option>

    </option>
";
        }
        // line 6
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "lugares"));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["lugar"]) {
            // line 7
            echo "    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "lugar"), "id"), "html", null, true);
            echo "\">
        ";
            // line 8
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "lugar"), "zona"), "html", null, true);
            echo "
    </option>
";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 11
            echo "    <option selected value=\"\">
        ";
            // line 12
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No hay lugares"), "html", null, true);
            echo "
    </option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['lugar'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Lugar:sacalugares.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 12,  43 => 11,  35 => 8,  30 => 7,  25 => 6,  19 => 2,  17 => 1,);
    }
}
