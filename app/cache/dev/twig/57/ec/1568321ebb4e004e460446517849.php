<?php

/* HotelesBackendBundle:Paginacion:paginacion.html.twig */
class __TwigTemplate_57ec1568321ebb4e004e460446517849 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getContext($context, "pageCount") > 1)) {
            // line 2
            echo "
    <ul>

    ";
            // line 5
            if (array_key_exists("previous", $context)) {
                // line 6
                echo "        <li class=\"prev\">
            <a href=\"";
                // line 7
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getContext($context, "route"), twig_array_merge($this->getContext($context, "query"), array($this->getContext($context, "pageParameterName") => $this->getContext($context, "previous")))), "html", null, true);
                echo "\">&laquo;&nbsp;";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Previous"), "html", null, true);
                echo "</a>
        </li>
    ";
            } else {
                // line 10
                echo "        <li class=\"disabled prev\">
            <a href=\"#\" onclick=\"return false\">&laquo;&nbsp;";
                // line 11
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Previous"), "html", null, true);
                echo "</a>
        </li>
    ";
            }
            // line 14
            echo "
    ";
            // line 15
            if (($this->getContext($context, "startPage") > 1)) {
                // line 16
                echo "        <li>
            <a href=\"";
                // line 17
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getContext($context, "route"), twig_array_merge($this->getContext($context, "query"), array($this->getContext($context, "pageParameterName") => 1))), "html", null, true);
                echo "\">1</a>
        </li>
        ";
                // line 19
                if (($this->getContext($context, "startPage") == 3)) {
                    // line 20
                    echo "            <li>
                <a href=\"";
                    // line 21
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getContext($context, "route"), twig_array_merge($this->getContext($context, "query"), array($this->getContext($context, "pageParameterName") => 2))), "html", null, true);
                    echo "\">2</a>
            </li>
        ";
                } elseif (($this->getContext($context, "startPage") != 2)) {
                    // line 24
                    echo "        <li class=\"disabled\">
            <a href=\"#\" onclick=\"return false\">&hellip;</a>
            ";
                    // line 29
                    echo "        </li>
        ";
                }
                // line 31
                echo "    ";
            }
            // line 32
            echo "
    ";
            // line 33
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getContext($context, "pagesInRange"));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 34
                echo "        ";
                if (($this->getContext($context, "page") != $this->getContext($context, "current"))) {
                    // line 35
                    echo "            <li>
                <a href=\"";
                    // line 36
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getContext($context, "route"), twig_array_merge($this->getContext($context, "query"), array($this->getContext($context, "pageParameterName") => $this->getContext($context, "page")))), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getContext($context, "page"), "html", null, true);
                    echo "</a>
            </li>
        ";
                } else {
                    // line 39
                    echo "            <li class=\"active\">
                <a href=\"#\" onClick=\"return false\">";
                    // line 40
                    echo twig_escape_filter($this->env, $this->getContext($context, "page"), "html", null, true);
                    echo "</a>
            </li>
        ";
                }
                // line 43
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 45
            echo "
    ";
            // line 46
            if (($this->getContext($context, "pageCount") > $this->getContext($context, "endPage"))) {
                // line 47
                echo "        ";
                if (($this->getContext($context, "pageCount") > ($this->getContext($context, "endPage") + 1))) {
                    // line 48
                    echo "            ";
                    if (($this->getContext($context, "pageCount") > ($this->getContext($context, "endPage") + 2))) {
                        // line 49
                        echo "                <li class=\"disabled\">
                    <a href=\"#\" onclick=\"return false\">&hellip;</a>
                    ";
                        // line 54
                        echo "                </li>
            ";
                    } else {
                        // line 56
                        echo "                <li>
                    <a href=\"";
                        // line 57
                        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getContext($context, "route"), twig_array_merge($this->getContext($context, "query"), array($this->getContext($context, "pageParameterName") => ($this->getContext($context, "pageCount") - 1)))), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, ($this->getContext($context, "pageCount") - 1), "html", null, true);
                        echo "</a>
                </li>
            ";
                    }
                    // line 60
                    echo "        ";
                }
                // line 61
                echo "        <li>
            <a href=\"";
                // line 62
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getContext($context, "route"), twig_array_merge($this->getContext($context, "query"), array($this->getContext($context, "pageParameterName") => $this->getContext($context, "pageCount")))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getContext($context, "pageCount"), "html", null, true);
                echo "</a>
        </li>
    ";
            }
            // line 65
            echo "
    ";
            // line 66
            if (array_key_exists("next", $context)) {
                // line 67
                echo "        <li>
            <a href=\"";
                // line 68
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getContext($context, "route"), twig_array_merge($this->getContext($context, "query"), array($this->getContext($context, "pageParameterName") => $this->getContext($context, "next")))), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Next"), "html", null, true);
                echo "&nbsp;&raquo;</a>
        </li>
    ";
            } else {
                // line 71
                echo "        <li class=\"disabled\">
            <a href=\"#\" onClick=\"return false\">";
                // line 72
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Next"), "html", null, true);
                echo "&nbsp;&raquo;</a>
        </li>
    ";
            }
            // line 75
            echo "    </ul>

";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Paginacion:paginacion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  187 => 75,  181 => 72,  178 => 71,  170 => 68,  167 => 67,  165 => 66,  154 => 62,  148 => 60,  140 => 57,  133 => 54,  129 => 49,  121 => 46,  118 => 45,  111 => 43,  105 => 40,  102 => 39,  91 => 35,  88 => 34,  84 => 33,  81 => 32,  74 => 29,  70 => 24,  61 => 20,  59 => 19,  54 => 17,  49 => 15,  46 => 14,  40 => 11,  29 => 7,  26 => 6,  24 => 5,  37 => 10,  53 => 12,  50 => 11,  31 => 6,  25 => 4,  21 => 3,  19 => 2,  17 => 1,  508 => 201,  503 => 198,  491 => 185,  475 => 182,  467 => 179,  463 => 177,  455 => 172,  450 => 171,  442 => 166,  437 => 165,  435 => 164,  429 => 161,  426 => 160,  423 => 159,  420 => 158,  414 => 155,  407 => 154,  405 => 153,  400 => 152,  394 => 149,  389 => 148,  383 => 145,  378 => 144,  376 => 143,  371 => 141,  365 => 140,  360 => 138,  355 => 137,  353 => 132,  350 => 131,  347 => 130,  341 => 127,  336 => 126,  334 => 125,  329 => 124,  323 => 121,  318 => 120,  312 => 117,  307 => 116,  305 => 115,  300 => 113,  294 => 112,  289 => 110,  284 => 109,  282 => 104,  279 => 103,  276 => 102,  270 => 99,  263 => 98,  261 => 97,  256 => 96,  250 => 93,  245 => 92,  239 => 89,  234 => 88,  232 => 87,  227 => 85,  221 => 84,  216 => 82,  211 => 81,  209 => 76,  206 => 75,  202 => 74,  195 => 72,  192 => 71,  186 => 70,  180 => 68,  174 => 66,  168 => 64,  162 => 65,  159 => 61,  155 => 60,  151 => 61,  145 => 56,  139 => 54,  137 => 56,  132 => 51,  126 => 48,  123 => 47,  106 => 48,  98 => 43,  94 => 36,  90 => 41,  86 => 40,  82 => 39,  78 => 31,  69 => 31,  64 => 21,  62 => 22,  51 => 16,  44 => 9,  41 => 8,  35 => 9,  30 => 4,  27 => 6,);
    }
}
