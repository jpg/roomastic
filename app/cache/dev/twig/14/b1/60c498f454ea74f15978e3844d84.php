<?php

/* HotelesFrontendBundle:Frontend:index.mv.twig */
class __TwigTemplate_14b160c498f454ea74f15978e3844d84 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"content_home\">
        <!-- bocata -->
        <div class=\"bocata\">
            Haz tu oferta, ahorra<br>y disfruta de tu estancia
            <span class=\"triangle\"></span>
        </div>
        <!-- form ppal. -->

        <form class=\"form_portada clearfix\" id=\"form-destino-fecha\">

            <select id=\"select-beast\" class=\"selec_ciudad\" placeholder=\"\" name=\"ciudad\">
                <option value=\"\">Destino</option>
                ";
        // line 15
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["hotel"]) {
            // line 16
            echo "                    ";
            if (($this->getAttribute($this->getContext($context, "hotel"), "provincia") != "")) {
                // line 17
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "provincia"), "nombre"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "provincia"), "nombre"), "html", null, true);
                echo "</option>
                    ";
            }
            // line 19
            echo "                    ";
            if (($this->getAttribute($this->getContext($context, "hotel"), "municipio") != "")) {
                // line 20
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "municipio"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "municipio"), "provincia"), "nombre"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "municipio"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "municipio"), "provincia"), "nombre"), "html", null, true);
                echo "</option>
                    ";
            }
            // line 22
            echo "                    ";
            if (($this->getAttribute($this->getContext($context, "hotel"), "lugar") != "")) {
                // line 23
                echo "                        <option value=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "provincia"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "municipio"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "zona"), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "provincia"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "municipio"), "nombre"), "html", null, true);
                echo "/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "hotel"), "lugar"), "zona"), "html", null, true);
                echo "</option>
                    ";
            }
            // line 24
            echo "                                                                
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 25
        echo "                            
            </select>
            <div class=\"fecha_picker\">
                <input type=\"text\" id=\"fromDate\" name=\"fromDate\" class=\"input_mov ll-skin-lug\"/>
                <input type=\"text\" id=\"toDate\" name=\"toDate\"/>
            </div>

            <input type=\"submit\" id=\"buscar\" name=\"buscar\" class=\"input_mov\" >

        </form>

        <!-- vídeo youtube -->
        <a href=\"https://www.youtube.com/embed/-03y5GizzKQ\" class=\"video\"><span>Te explicamos cómo funciona<br>Roomastic</span> en este vídeo</a>
    </div>
    <!-- /content_home -->
";
    }

    // line 41
    public function block_javascripts($context, array $blocks = array())
    {
        // line 42
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    ";
        // line 43
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "a101ceb_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a101ceb_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/a101ceb_index_1.js");
            // line 44
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "a101ceb"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_a101ceb") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/a101ceb.js");
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:index.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 44,  127 => 43,  122 => 42,  119 => 41,  100 => 25,  93 => 24,  77 => 23,  74 => 22,  62 => 20,  59 => 19,  51 => 17,  48 => 16,  44 => 15,  30 => 3,  27 => 2,);
    }
}
