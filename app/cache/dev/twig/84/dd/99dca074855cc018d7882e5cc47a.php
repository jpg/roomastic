<?php

/* HotelesFrontendBundle:Frontend:listadoajax.html.twig */
class __TwigTemplate_84dd99dca074855cc018d7882e5cc47a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
    ";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 3
            echo "        <div class=\"single_hotel clearfix  stars-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "calificacion"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "filtroServicios"), "html", null, true);
            echo "\" id=\"hotel-summary-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">

            <div class=\"clearfix\">

                <img class=\"estrellas_hotel\" src=\"/bundles/hotelesfrontend/img/";
            // line 7
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "calificacion"), "html", null, true);
            echo "stars.png\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "calificacion"), "html", null, true);
            echo " estrellas\">

                <div class=\"cont_img\">
                    ";
            // line 10
            if ((twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "objetohotel"), "imagenes")) > 0)) {
                // line 11
                echo "                        <a href=\"\" title=\"\"><img height=\"152\" width=\"215\" src=\"/uploads/hoteles/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "imagenes"), 0, array(), "array"), "imagen"), "html", null, true);
                echo "\" alt=\"\"></a>
                        ";
            }
            // line 13
            echo "                </div>

                <div class=\"hotel_cont clearfix\">
                    <h2 class=\"tit_hotel\">";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "</h2>

                    <ul class=\"servicios clearfix\">
                        ";
            // line 19
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "piscina") == 1)) {
                // line 20
                echo "                            <li class=\"piscina\">Piscina</li>
                            ";
            }
            // line 22
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "spa") == 1)) {
                // line 23
                echo "                            <li class=\"spa\">Spa</li>
                            ";
            }
            // line 25
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "wiFi") == 1)) {
                // line 26
                echo "                            <li class=\"wi-fi\">Wi-fi</li>
                            ";
            }
            // line 28
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "accesoAdaptado") == 1)) {
                // line 29
                echo "                            <li class=\"acceso-adaptado\">Acceso adaptado</li>
                            ";
            }
            // line 31
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "aceptanPerros") == 1)) {
                // line 32
                echo "                            <li class=\"perros\">Aceptan perros</li>
                            ";
            }
            // line 34
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "aparcamiento") == 1)) {
                // line 35
                echo "                            <li class=\"parking\">Aparcamiento/Parking</li>
                            ";
            }
            // line 37
            echo "                            ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "businessCenter") == 1)) {
                // line 38
                echo "                            <li class=\"business-center\">Business center</li>
                            ";
            }
            // line 40
            echo "                    </ul>

                    <div class=\"botones_hotel\">
                        <a class=\"detalle\" href=\"#\" title=\"\">DETALLE</a>
                        <a class=\"abrir_map\" href=\"#\" title=\"\">mapa</a>
                        <input class=\"timbre\" name=\"hotel1\" type=\"checkbox\" data-hotel-id=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">
                        <!--<a class=\"timbre\" href=\"#\" title=\"\">toca el timbre</a>-->
                    </div><!-- botones_hotel -->

                </div><!-- hotel_cont -->

            </div><!-- clearfix -->


            <div class=\"desplegable clearfix slideFotosList\">
                <ul class=\"galeria clearfix pikame_listado\" >
                    ";
            // line 56
            if ((twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "objetohotel"), "imagenes")) > 0)) {
                // line 57
                echo "                        ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "objetohotel"), "imagenes"));
                foreach ($context['_seq'] as $context["_key"] => $context["imagen"]) {
                    // line 58
                    echo "                            <li>
                                <a href=\"/uploads/hoteles/";
                    // line 59
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "imagen"), "html", null, true);
                    echo "\" title=\"\"><img height=\"48\" width=\"70\" src=\"/uploads/hoteles/";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "imagen"), "imagen"), "html", null, true);
                    echo "\" alt=\"\"></a>
                            </li>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['imagen'], $context['_parent'], $context['loop']);
                $context = array_merge($_parent, array_intersect_key($context, $_parent));
                // line 62
                echo "                    ";
            }
            echo "                                        
                </ul>
                <div class=\"hotel_desc\">
                    <p>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "descripcion"), "html", null, true);
            echo "</p>
                    <p>";
            // line 66
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "caracteristicas"), "html", null, true);
            echo "</p>
                    <a class=\"mas_dets rosa\" href=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_detallehotel", array("nombrehotel" => $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "url"))), "html", null, true);
            echo "\" title=\"\">Ver todos los detalles del hotel</a>
                </div>
            </div><!-- desplegable -->

            <div class=\"desplegable_mapa clearfix\">

                <div class=\"mapa\" style=\"height:150px;\" data-hotel-id=\"";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\" data-longitud=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "longitud"), "html", null, true);
            echo "\" data-latitud=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "latitud"), "html", null, true);
            echo "\"></div>

            </div><!-- desplegable -->

        </div><!-- single_hotel -->

    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 80
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listadoajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 80,  177 => 73,  168 => 67,  164 => 66,  160 => 65,  153 => 62,  142 => 59,  139 => 58,  134 => 57,  132 => 56,  118 => 45,  111 => 40,  107 => 38,  104 => 37,  100 => 35,  97 => 34,  93 => 32,  90 => 31,  86 => 29,  83 => 28,  79 => 26,  76 => 25,  72 => 23,  69 => 22,  65 => 20,  63 => 19,  57 => 16,  52 => 13,  46 => 11,  44 => 10,  36 => 7,  24 => 3,  20 => 2,  17 => 1,);
    }
}
