<?php

/* HotelesFrontendBundle:Frontend:oferta.mv.twig */
class __TwigTemplate_ca099bc51deb1d8e1f7114146c89e654 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.mv.twig");

        $this->blocks = array(
            'javascripts' => array($this, 'block_javascripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'modal' => array($this, 'block_modal'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_javascripts($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("javascripts", $context, $blocks);
        echo "
    <script>
        if (typeof ROOMASTIC === 'undefined') {
            ROOMASTIC = {};
        }
        ROOMASTIC.googleID = '";
        // line 8
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "configuracion"), "googAppId"), "html", null, true);
        echo "';
        ROOMASTIC.googleapikey = '";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "configuracion"), "googApiKey"), "html", null, true);
        echo "';
    </script>
    ";
        // line 11
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "7fd4248_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/7fd4248_oferta_1.js");
            // line 17
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
            // asset "7fd4248_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/7fd4248_moment_2.js");
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
            // asset "7fd4248_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248_2") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/7fd4248_sweet-alert_3.js");
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
            // asset "7fd4248_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248_3") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/7fd4248_facebook_login_4.js");
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
            // asset "7fd4248_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248_4") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/7fd4248_google_login_5.js");
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        } else {
            // asset "7fd4248"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_7fd4248") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/7fd4248.js");
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
    ";
        }
        unset($context["asset_url"]);
    }

    // line 20
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 21
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    ";
        // line 22
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "b3922df_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b3922df_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/b3922df_minimal.mv_1.css");
            // line 25
            echo "       
    <link rel=\"stylesheet\" href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\">
    ";
            // asset "b3922df_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b3922df_1") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/b3922df_sweet-alert_2.css");
            // line 25
            echo "       
    <link rel=\"stylesheet\" href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\">
    ";
            // asset "b3922df_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b3922df_2") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/b3922df_oferta.mv_3.css");
            // line 25
            echo "       
    <link rel=\"stylesheet\" href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\">
    ";
        } else {
            // asset "b3922df"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_b3922df") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/b3922df.css");
            // line 25
            echo "       
    <link rel=\"stylesheet\" href=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\">
    ";
        }
        unset($context["asset_url"]);
    }

    // line 30
    public function block_modal($context, array $blocks = array())
    {
        // line 31
        echo "    <div class=\"modalregister\">
        <div class=\"header\">
            <a href=\"#\" class=\"close\">X</a>
        </div>
        <div class=\"body\">
            <div class=\"registro\">

                <form class=\"form-signin\" action=\"";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_security_check"), "html", null, true);
        echo "\" method=\"post\" id=\"formlogin\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">
                    <div class=\"errorlogin\"></div>

                    <h2 class=\"form-signin-heading\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Debes estar registrado para"), "html", null, true);
        echo "<br> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("hacer tu oferta"), "html", null, true);
        echo "</h2>


                    <div class=\"login-wrap \">
                        <p class=\"tit\">";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceso"), "html", null, true);
        echo "</p>

                        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->getContext($context, "csrf_token"), "html", null, true);
        echo "\" />
                        <input type=\"text\" class=\"form-control\" placeholder=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Usuario"), "html", null, true);
        echo "\" name=\"_username\" value=\"";
        if (array_key_exists("last_username", $context)) {
            echo " ";
            echo twig_escape_filter($this->env, $this->getContext($context, "last_username"), "html", null, true);
            echo " ";
        }
        echo "\" autofocus>
                        <input type=\"password\" class=\"form-control\" placeholder=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Contraseña"), "html", null, true);
        echo "\" name=\"_password\">

                        <button class=\"btn btn-lg btn-login btn-block\" type=\"submit\">";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acceder"), "html", null, true);
        echo "</button>
                        <p class=\"forgor_ps\">
                            <a href=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_user_resetting_request"), "html", null, true);
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado tu contraseña?"), "html", null, true);
        echo "</a>
                        </p>

                        <p>";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("O accede con tus redes sociales:"), "html", null, true);
        echo "</p>

                        <div class=\"login-social-link\">
                            <a href=\"#\" class=\"facebook fontello login-rrss\">
                                <i class=\"icon-facebook-1\"></i>
                                <!--Facebook-->
                            </a>

                            <a href=\"#\" class=\"google login-rrss\">
                                <i class=\"icon-gplus\"></i>
                                <!-- google + -->
                            </a>
                        </div>

                    </div><!-- /left -->

                    <div class=\"login-wrap \">
                        <p class=\"tit\">";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Registro"), "html", null, true);
        echo "</p>
                        <p class=\"alignleft\">";
        // line 74
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Puedes registrarte directamente en nuestra web:"), "html", null, true);
        echo "</p>

                        <button class=\"btn btn-lg btn-login btn-block showregister\" type=\"submit\">";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("REGISTRO EN LA WEB"), "html", null, true);
        echo "</button>

                        <p class=\"alignleft\">";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("O con tu cuenta de usuario en alguna de estas redes sociales:"), "html", null, true);
        echo "</p>


                        <div class=\"login-social-link\">

                            <a href=\"#\" class=\"facebook fontello login-rrss\">
                                <i class=\"icon-facebook-1\"></i>
                                <!--Facebook-->
                            </a>
                            <div id=\"fb-root\"></div>
                            <script>
        if (typeof ROOMASTIC === 'undefined') {
            ROOMASTIC = {};
        }
        ROOMASTIC.facebookAppId = '";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "configuracion"), "fbappid"), "html", null, true);
        echo "';
        ROOMASTIC.state = '";
        // line 93
        echo twig_escape_filter($this->env, $this->getContext($context, "csrf_token"), "html", null, true);
        echo "';
        (function () {
            var e = document.createElement('script');
            e.async = true;
            e.src = document.location.protocol +
                    '//connect.facebook.net/en_US/all.js';
            document.getElementById('fb-root').appendChild(e);
        }());
                            </script>

                            <a href=\"#\" class=\"google special login-rrss\">
                                <i class=\"icon-gplus\"></i>
                                <!-- google + -->

                                <!-- google + -->
                                <script type=\"text/javascript\">
                                    (function () {
                                        var po = document.createElement('script');
                                        po.type = 'text/javascript';
                                        po.async = true;
                                        po.src = 'https://plus.google.com/js/client:plusone.js?onload=googleReady';
                                        var s = document.getElementsByTagName('script')[0];
                                        s.parentNode.insertBefore(po, s);
                                    })();


                                </script>
                            </a>
                        </div>


                    </div><!-- /right -->
            </div>
            <div class=\"clearfix\"></div>

            <!-- Modal -->
            <div aria-hidden=\"true\" aria-labelledby=\"myModalLabel\" role=\"dialog\" tabindex=\"-1\" id=\"myModal\" class=\"modal fade\" style=\"display:none;\">
                <div class=\"modal-dialog\">
                    <div class=\"modal-content\">
                        <div class=\"modal-header\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                            <h4 class=\"modal-title\">";
        // line 134
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Has olvidado la contraseña?"), "html", null, true);
        echo "</h4>
                        </div>
                        <div class=\"modal-body\">
                            <p>";
        // line 137
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Escribe tu email y te reenviaremos una nueva contraseña"), "html", null, true);
        echo "</p>
                            <input type=\"text\" name=\"email\" placeholder=\"Email\" autocomplete=\"off\" class=\"form-control placeholder-no-fix\">

                        </div>
                        <div class=\"modal-footer\">
                            <button data-dismiss=\"modal\" class=\"btn btn-default\" type=\"button\">";
        // line 142
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cancelar"), "html", null, true);
        echo "</button>
                            <button class=\"btn btn-success\" type=\"button\">";
        // line 143
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar"), "html", null, true);
        echo "</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal -->

            </form><!-- form-signin -->


        </div>
        <!--/body -->


        <div class=\"bodyregister hide\">

            <div class=\"registro nuevo_reg\">


                <form class=\"form-signin\" action=\"";
        // line 162
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_registroajax"), "html", null, true);
        echo "\" id=\"formregister\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "formRegister"));
        echo ">

                    <h2 class=\"form-signin-heading\">";
        // line 164
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nuevo registro en la web"), "html", null, true);
        echo "</h2>
                    <div class=\"login-wrap\">
                        <div class=\"input_newreg\">

                            ";
        // line 168
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "nombre"), array("attr" => array("class" => "form-control width1", "placeholder" => $this->env->getExtension('translator')->trans("Nombre"))));
        echo "  

                            ";
        // line 170
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "apellidos"), array("attr" => array("class" => "form-control width2", "placeholder" => $this->env->getExtension('translator')->trans("Apellidos"))));
        echo "   

                            ";
        // line 172
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "direccioncompleta"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("Dirección completa (Campo no obligatorio)"))));
        echo "    

                            ";
        // line 174
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "dni"), array("attr" => array("class" => "form-control width1", "placeholder" => $this->env->getExtension('translator')->trans("DNI (Para dar veracidad a tu oferta)"))));
        echo "

                            ";
        // line 176
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "telefono"), array("attr" => array("class" => "form-control width1", "placeholder" => $this->env->getExtension('translator')->trans("Teléfono (Para contactarte en caso de cambios en la reserva)"))));
        echo "    

                            ";
        // line 178
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegister"), "email"), array("attr" => array("class" => "form-control width3", "placeholder" => $this->env->getExtension('translator')->trans("Email"))));
        echo "

                            ";
        // line 180
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "formRegister"), "password"), "first"), array("attr" => array("class" => "form-control width6", "placeholder" => $this->env->getExtension('translator')->trans("Contraseña"))));
        echo "

                            ";
        // line 182
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getAttribute($this->getContext($context, "formRegister"), "password"), "second"), array("attr" => array("class" => "form-control width7", "placeholder" => $this->env->getExtension('translator')->trans("Repite contraseña"))));
        echo "    


                            <div class=\"clearfix\"></div>

                            <button type=\"submit\" class=\"btn btn-lg btn-login btn-block w-es\" >";
        // line 187
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar datos y hacer oferta"), "html", null, true);
        echo "</button>
                            ";
        // line 188
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "formRegister"));
        echo "
                        </div>
                    </div>
                </form>  
            </div>

        </div>

        <div class=\"bodyregisterextra hide\">

            <div class=\"registro nuevo_reg\">


                <form class=\"form-signin\" action=\"";
        // line 201
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_registroextraajax"), "html", null, true);
        echo "\" id=\"formregisterextra\" method=\"post\"  ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "formRegisterextra"));
        echo ">
                    <h2 class=\"form-signin-heading\">";
        // line 202
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Completa tu registro para"), "html", null, true);
        echo "<br> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("hacer tu oferta"), "html", null, true);
        echo "</h2>
                    <div class=\"login-wrap\">
                        <div class=\"input_newreg\">

                            ";
        // line 206
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "nombre"), array("attr" => array("class" => "form-control width1 nombre", "placeholder" => $this->env->getExtension('translator')->trans("Nombre"))));
        echo "  

                            ";
        // line 208
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "apellidos"), array("attr" => array("class" => "form-control width2 apellidos", "placeholder" => $this->env->getExtension('translator')->trans("Apellidos"))));
        echo "   

                            ";
        // line 210
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "direccioncompleta"), array("attr" => array("class" => "form-control width3 direccion", "placeholder" => $this->env->getExtension('translator')->trans("Dirección completa (Campo no obligatorio)"))));
        echo "

                            ";
        // line 212
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "dni"), array("attr" => array("class" => "form-control width1 dni", "placeholder " => $this->env->getExtension('translator')->trans("DNI (Para dar veracidad a tu oferta)"))));
        echo "

                            ";
        // line 214
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "telefono"), array("attr" => array("class" => "form-control width1 telefono", "placeholder" => $this->env->getExtension('translator')->trans("Teléfono (Para contactarte en caso de cambios en la reserva)"))));
        echo "    

                            ";
        // line 216
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "formRegisterextra"), "email"), array("attr" => array("class" => "form-control width3 email", "placeholder" => $this->env->getExtension('translator')->trans("Email"))));
        echo "

                            <div class=\"clearfix\"></div>

                            <button type=\"submit\" class=\"btn btn-lg btn-login btn-block w-es\">";
        // line 220
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar datos y hacer oferta"), "html", null, true);
        echo "</button>

                            <div class=\"clearfix\"></div>

                        </div>
                    </div>
                    ";
        // line 226
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "formRegisterextra"));
        echo "
                </form>  

            </div>

        </div><!--/bodyregisterextra -->

    </div>
</div>
";
    }

    // line 237
    public function block_content($context, array $blocks = array())
    {
        // line 238
        echo "

    <div class=\"wpregistro\">
        <div class=\"oferta\">
            <form action=\"\" method=\"post\" id=\"formoferta\">
                ";
        // line 243
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "_token_oferta"));
        echo "


                <div id=\"tabsWithStyle\" class=\"style-tabs\">
                    <div class=\"clearfix\"></div>
                    <div id=\"tabs-1\">
                        <h2>¿Cuánto estás dispuesto a pagar?</h2>
                        <p>El precio debe ser por noche, habitación doble y en régimen de alojamiento.Los precios oficiales de los hoteles que has seleccionado rondan entre los <b>";
        // line 250
        echo twig_escape_filter($this->env, $this->getContext($context, "precio_hotel_min"), "html", null, true);
        echo " € y los ";
        echo twig_escape_filter($this->env, $this->getContext($context, "precio_hotel_max"), "html", null, true);
        echo "  €</b>
                        (Algunos hoteles ofrecen desayuno o media pensión incluido)</p>
                        ";
        // line 252
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "preciohabitacion"), array("attr" => array("type" => "number", "placeholder" => "Precio", "tabindex" => "0")));
        echo "
                    </div>

                    <div class=\"clearfix separadortabs\"></div>

                    <div id=\"tabs-2\">
                        <h2>Número de noches</h2>
                        <div class=\"fecha_picker\">
                            ";
        // line 260
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "fecha"), array("attr" => array("value" => ((twig_date_format_filter($this->env, $this->getContext($context, "fecha_inicio"), "d/m/y") . " - ") . twig_date_format_filter($this->env, $this->getContext($context, "fecha_fin"), "d/m/y")), "tabindex" => "1", "class" => "select_fecha hide", "placeholder" => "Fechas")));
        echo "

                            <input type=\"text\" id=\"fromDate\" name=\"fromDate\" class=\"input_mov ll-skin-lug\" placeholder=\"Entrada\" data-from-date=\"";
        // line 262
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getContext($context, "fecha_inicio"), "d-m-y"), "html", null, true);
        echo "\"/>
                            <input type=\"text\" id=\"toDate\" name=\"toDate\" placeholder=\"Salida\" data-to-date=\"";
        // line 263
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getContext($context, "fecha_fin"), "d-m-y"), "html", null, true);
        echo "\"/>
                        </div>
                        <p>Total número de noches ";
        // line 265
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "numnoches"), array("attr" => array("value" => $this->getContext($context, "num_noches"), "placeholder" => "3", "readonly" => "")));
        echo "</p>

                    </div>

                    <div class=\"clearfix separadortabs\"></div>

                    <div id=\"tabs-3\">
                        <h2>Selecciona el número de huéspedes</h2>
                        <p>
                            <label>Adultos</label>
                            ";
        // line 275
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "numadultos"), array("attr" => array("value" => "1", "placeholder" => "1", "tabindex" => "2", "class" => "select_adultos")));
        echo "
                        </p>
                        <p>
                            <label>Niños hasta 3 años</label>
                            ";
        // line 279
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "numninos"), array("attr" => array("value" => "0", "placeholder" => "0", "tabindex" => "3", "class" => "select_ninios")));
        echo "
                        </p>
                    </div>

                    <div class=\"clearfix separadortabs\"></div>

                    <div id=\"tabs-4\">
                        <h2>Selecciona el número de habitaciones</h2>
                        <p>
                            <label>Habitaciones</label>
                            ";
        // line 289
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "numhabitaciones"), array("attr" => array("value" => "1", "placeholder" => "1", "tabindex" => "4", "class" => "select_habs")));
        echo "
                        </p>

                    </div>

                    <div class=\"clearfix separadortabs\"></div>

                    <div id=\"tabs-5\">
                        <h2>Hoteles</h2>

                        <ul class=\"hoteles\">
                            ";
        // line 300
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["hotel"]) {
            // line 301
            echo "                                <li class=\"hotel_li\">
                                    <input type=\"checkbox\" class=\"checkbox-oferta\" name=\"hoteles[id][]\" id=\"";
            // line 302
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "id"), "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "id"), "html", null, true);
            echo "\" checked>
                                    <label for=\"Hotel Diamante Suites\">";
            // line 303
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "hotel"), "nombrehotel"), "html", null, true);
            echo "</label>
                                </li>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['hotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 305
        echo "                    
                        </ul>
                    </div>

                </div><!-- tabs -->

                <div class=\"clearfix separadortabs\"></div>


                <!-- Resumen oferta -->
                <div class=\"resumenOferta\">
                    <h3>Precio total de tu oferta</h3>
                    <div class=\"caja\">
                        <p>En base a la configuración que nos acabas de facilitar, este será el precio total que vas a ofertar a los hoteles seleccionados:</p>

                        ";
        // line 320
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "preciototaloferta"), array("attr" => array("value" => "", "placeholder" => "0", "readonly" => "")));
        echo "
                        <div class=\"clearfix\"></div>

                        <div class=\"acepto1 clearfix\">
                            <input type=\"checkbox\" id=\"acepto1\" class=\"checkbox-oferta\" name=\"acepto_condiciones\" required>
                            <label for=\"acepto1\">Acepto las condiciones de venta y de uso de esta web.</label>
                        </div>

                        <div class=\"acepto2 clearfix\">
                            <input type=\"checkbox\" id=\"acepto2\"  class=\"checkbox-oferta\" name=\"acepto_suscribirme\">
                            <label for=\"acepto2\">Acepto suscribirme al boletín de noticias RooMail</label>
                        </div>
                        <div class=\"validaoferta\">
                            ";
        // line 333
        if ($this->getAttribute($this->getContext($context, "app"), "user")) {
            // line 334
            echo "                                ";
            $context["title"] = "Ya puedes hacer tu oferta";
            // line 335
            echo "                            ";
        } else {
            // line 336
            echo "                                ";
            $context["title"] = "Todavia tienes que loguearte";
            // line 337
            echo "
                            ";
        }
        // line 339
        echo "                            <a class=\"btn_oferta\"  title=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
        echo "\" style=\"cursor: pointer;\" >hacer oferta <span class=\"decoration\"></span></a>                        
                        </div>
                    </div><!-- caja -->
                </div><!-- resumenOferta -->
            </form>
        </div>
    </div>
    <!-- /opciones -->
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:oferta.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  666 => 339,  662 => 337,  659 => 336,  656 => 335,  653 => 334,  651 => 333,  635 => 320,  618 => 305,  609 => 303,  603 => 302,  600 => 301,  596 => 300,  582 => 289,  569 => 279,  562 => 275,  549 => 265,  544 => 263,  540 => 262,  535 => 260,  524 => 252,  517 => 250,  507 => 243,  500 => 238,  497 => 237,  483 => 226,  474 => 220,  467 => 216,  462 => 214,  457 => 212,  452 => 210,  447 => 208,  442 => 206,  433 => 202,  427 => 201,  411 => 188,  407 => 187,  399 => 182,  394 => 180,  389 => 178,  384 => 176,  379 => 174,  374 => 172,  369 => 170,  364 => 168,  357 => 164,  350 => 162,  328 => 143,  324 => 142,  316 => 137,  310 => 134,  266 => 93,  262 => 92,  245 => 78,  240 => 76,  235 => 74,  231 => 73,  211 => 56,  203 => 53,  198 => 51,  193 => 49,  183 => 48,  179 => 47,  174 => 45,  165 => 41,  157 => 38,  148 => 31,  145 => 30,  137 => 26,  134 => 25,  127 => 26,  124 => 25,  118 => 26,  115 => 25,  109 => 26,  106 => 25,  102 => 22,  97 => 21,  94 => 20,  54 => 17,  50 => 11,  45 => 9,  41 => 8,  32 => 3,  29 => 2,);
    }
}
