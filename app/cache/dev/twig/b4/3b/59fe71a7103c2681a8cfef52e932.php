<?php

/* HotelesFrontendBundle:Frontend:contacto.html.twig */
class __TwigTemplate_b43b59fe71a7103c2681a8cfef52e932 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesFrontendBundle::layout.html.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesFrontendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "

    <div class=\"contenido\">

        <h2>";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Atención al cliente"), "html", null, true);
        echo "</h2>
        <div class=\"contacto\">
            <div class=\"left\">
                <p>Ponte en contacto con nosotros rellenando el siguiente formulario de contacto. En un breve plazo de tiempo nos pondremos en contacto contigo.</p>
                <p>Si lo prefieres, puedes ponerte en contacto con nosotros por teléfono de lunes a viernes de 9 a 19h.</p>
                <p class=\"rosa\"><span>911 610 156</span></p>
            </div>
            <div class=\"right\">

                ";
        // line 17
        if ($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "hasFlash", array(0 => "notice"), "method")) {
            // line 18
            echo "                    <div class=\"flash-notice\">
                        ";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "app"), "session"), "flash", array(0 => "notice"), "method"), "html", null, true);
            echo "
                    </div>
                ";
        }
        // line 22
        echo "
                <form action=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_contacto"), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">

                    <input class=\"bug\" type=\"text\" id=\"hoteles_backendbundle_contactotype_nombre\" name=\"hoteles_backendbundle_contactotype[nombre]\" required=\"required\" maxlength=\"255\" placeholder=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre"), "html", null, true);
        echo "\"><br><br>
                    <input type=\"text\" id=\"hoteles_backendbundle_contactotype_apellidos\" name=\"hoteles_backendbundle_contactotype[apellidos]\" required=\"required\" maxlength=\"255\" placeholder=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos"), "html", null, true);
        echo "\" class=\"longinput\"><br><br>
                    <input type=\"text\" id=\"hoteles_backendbundle_contactotype_email\" name=\"hoteles_backendbundle_contactotype[email]\" required=\"required\" maxlength=\"255\" placeholder=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "\" class=\"longinput\"><br><br>
                    <textarea id=\"hoteles_backendbundle_contactotype_texto\" name=\"hoteles_backendbundle_contactotype[texto]\" required=\"required\" placeholder=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comentarios"), "html", null, true);
        echo "\"></textarea><br><br>
                    ";
        // line 29
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "_token"));
        echo "
                    ";
        // line 31
        echo "                    <p>
                        <button type=\"submit\" class=\"btn_generic\">";
        // line 32
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Enviar petición"), "html", null, true);
        echo "</button>
                    </p>
                </form>
            </div>\t
            <div class=\"clearfix\"></div>
        </div>

    </div><!-- contenido -->

    ";
        // line 59
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:contacto.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 59,  91 => 32,  88 => 31,  84 => 29,  80 => 28,  76 => 27,  72 => 26,  68 => 25,  61 => 23,  58 => 22,  52 => 19,  49 => 18,  47 => 17,  35 => 8,  29 => 4,  26 => 3,);
    }
}
