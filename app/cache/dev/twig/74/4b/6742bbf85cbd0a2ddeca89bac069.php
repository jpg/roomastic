<?php

/* HotelesFrontendBundle:Frontend:listadoAJAXlateral.mv.twig */
class __TwigTemplate_744b6742bbf85cbd0a2ddeca89bac069 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
    ";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 3
            echo "        <li class=\"hotel_li hotel-selected\"  style=\"display: none;\" id=\"hotel-checkbox-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\" data-hotel-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">
            <input type=\"checkbox\" class=\"checkbox-lateral\" id=\"";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "\" checked>
            <label for=\"";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "</label>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 8
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listadoAJAXlateral.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 8,  35 => 5,  31 => 4,  146 => 62,  134 => 56,  129 => 53,  124 => 50,  121 => 49,  116 => 46,  113 => 45,  108 => 42,  105 => 41,  100 => 38,  97 => 37,  92 => 34,  89 => 33,  84 => 30,  81 => 29,  76 => 26,  74 => 25,  68 => 22,  61 => 17,  53 => 14,  49 => 13,  45 => 11,  39 => 9,  37 => 8,  24 => 3,  20 => 2,  17 => 1,);
    }
}
