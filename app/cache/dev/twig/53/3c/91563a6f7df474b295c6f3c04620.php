<?php

/* TwigBundle:Exception:traces.html.twig */
class __TwigTemplate_533c91563a6f7df474b295c6f3c04620 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"block\">
    ";
        // line 2
        if (($this->getContext($context, "count") > 0)) {
            // line 3
            echo "        <h2>
            <span><small>[";
            // line 4
            echo twig_escape_filter($this->env, (($this->getContext($context, "count") - $this->getContext($context, "position")) + 1), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, ($this->getContext($context, "count") + 1), "html", null, true);
            echo "]</small></span>
            ";
            // line 5
            echo $this->env->getExtension('code')->abbrClass($this->getAttribute($this->getContext($context, "exception"), "class"));
            echo ": ";
            echo $this->env->getExtension('code')->formatFileFromText(strtr(twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "exception"), "message")), array("
" => "<br />")));
            echo "&nbsp;
            ";
            // line 6
            ob_start();
            // line 7
            echo "            <a href=\"#\" onclick=\"toggle('traces_";
            echo twig_escape_filter($this->env, $this->getContext($context, "position"), "html", null, true);
            echo "', 'traces'); switchIcons('icon_traces_";
            echo twig_escape_filter($this->env, $this->getContext($context, "position"), "html", null, true);
            echo "_open', 'icon_traces_";
            echo twig_escape_filter($this->env, $this->getContext($context, "position"), "html", null, true);
            echo "_close'); return false;\">
                <img class=\"toggle\" id=\"icon_traces_";
            // line 8
            echo twig_escape_filter($this->env, $this->getContext($context, "position"), "html", null, true);
            echo "_close\" alt=\"-\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/images/blue_picto_less.gif"), "html", null, true);
            echo "\" style=\"visibility: ";
            echo (((0 == $this->getContext($context, "count"))) ? ("visible") : ("hidden"));
            echo "\" />
                <img class=\"toggle\" id=\"icon_traces_";
            // line 9
            echo twig_escape_filter($this->env, $this->getContext($context, "position"), "html", null, true);
            echo "_open\" alt=\"+\" src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/framework/images/blue_picto_more.gif"), "html", null, true);
            echo "\" style=\"visibility: ";
            echo (((0 == $this->getContext($context, "count"))) ? ("hidden") : ("visible"));
            echo "; margin-left: -18px\" />
            </a>
            ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 12
            echo "        </h2>
    ";
        } else {
            // line 14
            echo "        <h2>Stack Trace</h2>
    ";
        }
        // line 16
        echo "
    <a id=\"traces_link_";
        // line 17
        echo twig_escape_filter($this->env, $this->getContext($context, "position"), "html", null, true);
        echo "\"></a>
    <ol class=\"traces list_exception\" id=\"traces_";
        // line 18
        echo twig_escape_filter($this->env, $this->getContext($context, "position"), "html", null, true);
        echo "\" style=\"display: ";
        echo (((0 == $this->getContext($context, "count"))) ? ("block") : ("none"));
        echo "\">
        ";
        // line 19
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getContext($context, "exception"), "trace"));
        foreach ($context['_seq'] as $context["i"] => $context["trace"]) {
            // line 20
            echo "            <li>
                ";
            // line 21
            $this->env->loadTemplate("TwigBundle:Exception:trace.html.twig")->display(array("prefix" => $this->getContext($context, "position"), "i" => $this->getContext($context, "i"), "trace" => $this->getContext($context, "trace")));
            // line 22
            echo "            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['trace'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 24
        echo "    </ol>
</div>
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:traces.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 21,  88 => 19,  78 => 17,  75 => 16,  71 => 14,  40 => 7,  38 => 6,  31 => 5,  22 => 3,  20 => 2,  224 => 96,  200 => 83,  195 => 80,  177 => 71,  158 => 61,  156 => 60,  146 => 55,  135 => 50,  131 => 48,  126 => 46,  120 => 45,  113 => 43,  103 => 36,  47 => 19,  39 => 15,  17 => 1,  86 => 6,  46 => 14,  37 => 8,  33 => 7,  24 => 6,  19 => 1,  44 => 8,  30 => 4,  27 => 3,  1000 => 291,  995 => 290,  993 => 289,  990 => 288,  974 => 284,  952 => 283,  950 => 282,  947 => 281,  935 => 276,  931 => 275,  926 => 274,  924 => 273,  921 => 272,  912 => 266,  906 => 264,  903 => 263,  898 => 262,  896 => 261,  893 => 260,  886 => 255,  877 => 253,  873 => 252,  870 => 251,  867 => 250,  865 => 249,  862 => 248,  854 => 244,  852 => 243,  849 => 242,  842 => 237,  839 => 236,  831 => 231,  827 => 230,  823 => 229,  820 => 228,  818 => 227,  815 => 226,  807 => 222,  805 => 221,  802 => 220,  794 => 214,  792 => 213,  789 => 212,  781 => 208,  778 => 207,  776 => 206,  773 => 205,  752 => 201,  749 => 200,  746 => 199,  743 => 198,  741 => 197,  738 => 196,  730 => 190,  727 => 189,  725 => 188,  722 => 187,  715 => 184,  712 => 183,  709 => 182,  701 => 178,  698 => 177,  696 => 176,  693 => 175,  677 => 171,  674 => 170,  672 => 169,  669 => 168,  661 => 164,  658 => 163,  645 => 157,  642 => 156,  640 => 155,  637 => 154,  629 => 150,  626 => 149,  624 => 148,  621 => 147,  613 => 143,  611 => 142,  608 => 141,  597 => 136,  595 => 135,  592 => 134,  584 => 130,  581 => 129,  579 => 128,  577 => 127,  574 => 126,  567 => 121,  559 => 120,  554 => 119,  548 => 117,  545 => 116,  543 => 115,  532 => 108,  530 => 104,  525 => 103,  519 => 101,  516 => 100,  514 => 99,  511 => 98,  502 => 92,  498 => 91,  494 => 90,  490 => 89,  485 => 88,  479 => 86,  476 => 85,  471 => 83,  455 => 79,  453 => 78,  450 => 77,  434 => 73,  432 => 72,  429 => 71,  419 => 65,  416 => 64,  413 => 63,  405 => 60,  400 => 59,  397 => 58,  388 => 55,  386 => 54,  378 => 53,  366 => 49,  361 => 48,  352 => 46,  349 => 45,  347 => 44,  344 => 43,  335 => 39,  323 => 37,  319 => 35,  304 => 33,  300 => 32,  295 => 31,  292 => 30,  287 => 29,  285 => 28,  282 => 27,  272 => 23,  267 => 21,  259 => 17,  256 => 16,  250 => 14,  248 => 13,  237 => 7,  233 => 6,  228 => 5,  226 => 4,  223 => 3,  219 => 288,  216 => 287,  214 => 281,  209 => 272,  206 => 271,  201 => 260,  196 => 248,  191 => 242,  188 => 77,  185 => 76,  180 => 235,  178 => 226,  175 => 225,  173 => 220,  170 => 219,  167 => 217,  162 => 63,  160 => 205,  155 => 196,  152 => 195,  149 => 193,  144 => 186,  142 => 54,  132 => 168,  129 => 47,  119 => 153,  114 => 146,  112 => 141,  107 => 134,  104 => 24,  99 => 34,  92 => 20,  89 => 97,  87 => 83,  84 => 82,  82 => 18,  77 => 28,  74 => 27,  72 => 43,  69 => 42,  67 => 12,  64 => 26,  62 => 24,  59 => 23,  57 => 9,  52 => 3,  284 => 128,  270 => 22,  261 => 122,  257 => 120,  253 => 15,  251 => 117,  225 => 93,  222 => 92,  218 => 90,  215 => 90,  208 => 70,  204 => 84,  190 => 78,  186 => 34,  171 => 67,  159 => 13,  153 => 59,  147 => 187,  141 => 157,  139 => 181,  136 => 91,  122 => 154,  117 => 44,  108 => 71,  81 => 47,  79 => 40,  70 => 26,  66 => 39,  63 => 38,  61 => 23,  49 => 8,  36 => 10,  25 => 4,  666 => 339,  662 => 337,  659 => 336,  656 => 162,  653 => 161,  651 => 333,  635 => 320,  618 => 305,  609 => 303,  603 => 302,  600 => 137,  596 => 300,  582 => 289,  569 => 279,  562 => 275,  549 => 265,  544 => 263,  540 => 114,  535 => 260,  524 => 252,  517 => 250,  507 => 243,  500 => 238,  497 => 237,  483 => 226,  474 => 84,  467 => 216,  462 => 214,  457 => 212,  452 => 210,  447 => 208,  442 => 206,  433 => 202,  427 => 201,  411 => 188,  407 => 61,  399 => 182,  394 => 57,  389 => 178,  384 => 176,  379 => 174,  374 => 51,  369 => 170,  364 => 168,  357 => 47,  350 => 162,  328 => 143,  324 => 142,  316 => 137,  310 => 134,  266 => 125,  262 => 92,  245 => 12,  240 => 76,  235 => 74,  231 => 73,  211 => 88,  203 => 269,  198 => 259,  193 => 79,  183 => 236,  179 => 72,  174 => 24,  165 => 212,  157 => 204,  148 => 31,  145 => 30,  137 => 51,  134 => 174,  127 => 161,  124 => 160,  118 => 26,  115 => 25,  109 => 140,  106 => 70,  102 => 126,  97 => 22,  94 => 31,  54 => 11,  50 => 11,  45 => 13,  41 => 7,  32 => 11,  29 => 6,);
    }
}
