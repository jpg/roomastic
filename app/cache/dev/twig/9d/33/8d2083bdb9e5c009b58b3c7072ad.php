<?php

/* HotelesFrontendBundle:Frontend:listadoAJAXlateral.html.twig */
class __TwigTemplate_9d338d2083bdb9e5c009b58b3c7072ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
    ";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 3
            echo "        <li class=\"hotel_li hotel-selected\"  style=\"display: none;\" id=\"hotel-checkbox-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\" data-hotel-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">
            <input type=\"checkbox\" class=\"checkbox-lateral\" id=\"";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "\" checked>
            <label for=\"";
            // line 5
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "</label>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 8
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listadoAJAXlateral.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 5,  31 => 4,  194 => 80,  177 => 73,  168 => 67,  164 => 66,  160 => 65,  153 => 62,  142 => 59,  139 => 58,  134 => 57,  132 => 56,  118 => 45,  111 => 40,  107 => 38,  104 => 37,  100 => 35,  97 => 34,  93 => 32,  90 => 31,  86 => 29,  83 => 28,  79 => 26,  76 => 25,  72 => 23,  69 => 22,  65 => 20,  63 => 19,  57 => 16,  52 => 13,  46 => 8,  44 => 10,  36 => 7,  24 => 3,  20 => 2,  17 => 1,);
    }
}
