<?php

/* HotelesBackendBundle:Extras:preload.html.twig */
class __TwigTemplate_873cd73695a3ab05468e197e0f6595fc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"fade\"></div>
<div id=\"modal\">
    ";
        // line 3
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "2be4f30_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2be4f30_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/images/2be4f30_loading_1.gif");
            // line 4
            echo "    <img id=\"loader\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" alt=\"Cargando...\" />
    ";
        } else {
            // asset "2be4f30"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_2be4f30") : $this->env->getExtension('assets')->getAssetUrl("_controller/images/2be4f30.gif");
            echo "    <img id=\"loader\" src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" alt=\"Cargando...\" />
    ";
        }
        unset($context["asset_url"]);
        // line 5
        echo "  
</div>";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:preload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 5,  25 => 4,  21 => 3,  717 => 356,  691 => 354,  687 => 351,  646 => 315,  640 => 314,  634 => 313,  628 => 312,  622 => 311,  609 => 300,  606 => 298,  604 => 297,  601 => 296,  576 => 289,  567 => 283,  563 => 282,  559 => 281,  555 => 280,  551 => 279,  542 => 274,  521 => 271,  518 => 270,  500 => 269,  497 => 268,  495 => 267,  483 => 258,  476 => 253,  471 => 250,  468 => 249,  463 => 246,  460 => 245,  455 => 242,  452 => 241,  447 => 238,  444 => 237,  439 => 234,  436 => 233,  431 => 230,  428 => 229,  423 => 226,  421 => 225,  415 => 222,  407 => 216,  404 => 215,  395 => 212,  391 => 211,  388 => 210,  383 => 209,  381 => 208,  376 => 206,  366 => 201,  354 => 197,  337 => 196,  321 => 187,  317 => 186,  312 => 184,  308 => 183,  302 => 180,  298 => 179,  284 => 167,  275 => 164,  271 => 163,  264 => 162,  260 => 161,  188 => 92,  183 => 89,  170 => 87,  167 => 86,  163 => 85,  153 => 81,  150 => 80,  147 => 79,  133 => 78,  130 => 77,  127 => 76,  124 => 75,  121 => 74,  103 => 73,  101 => 72,  89 => 63,  73 => 49,  71 => 48,  63 => 42,  58 => 37,  54 => 36,  17 => 1,);
    }
}
