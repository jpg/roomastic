<?php

/* HotelesBackendBundle:Empresa:new.html.twig */
class __TwigTemplate_1d3e97bb1ddcafc68237d729f170127c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear empresa"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">

            <!--tab nav start-->
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear empresa"), "html", null, true);
        echo "
                </header>
                <header class=\"panel-heading tab-bg-dark-navy-blue \">
                    <ul class=\"nav nav-tabs\">
                        <li class=\"active ";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "email")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombreempresa")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cif")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "direccionfacturacion")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefono")), 5 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "numerocuenta")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#general\">";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la empresa"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombrepersonacontacto")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "apellidospersonacontacto")), 2 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "emailpersonacontacto")), 3 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefonopersonacontacto")), 4 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cargopersonacontacto")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#api\">";
        // line 24
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Datos de la persona de contacto"), "html", null, true);
        echo "</a>
                        </li>
                        <li class=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->errorgroup(array(0 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionempresaentero")), 1 => $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionempresadecimal")))), "html", null, true);
        echo "\">
                            <a data-toggle=\"tab\" href=\"#comisiones\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Comisiones"), "html", null, true);
        echo "</a>
                        </li>
                    </ul>
                </header>
                <div class=\"panel-body\">
                    <div class=\"tab-content\">
                        <div id=\"general\" class=\"tab-pane active\">
                            <form action=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_empresa_create"), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "form"));
        echo ">
                                ";
        // line 36
        echo "                                <div class=\"form-group  ";
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "email"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 39
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "email"));
        echo "
                                        ";
        // line 40
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "email"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Recibir aquí todos los emails"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 46
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "receive_mails"));
        echo "
                                        ";
        // line 47
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "receive_mails"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombreempresa"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la empresa"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 53
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombreempresa"));
        echo "
                                        ";
        // line 54
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "nombreempresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 57
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cif"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("CIF"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 60
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cif"));
        echo "
                                        ";
        // line 61
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "cif"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "direccionfacturacion"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 65
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Dirección de facturación"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 67
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "direccionfacturacion"));
        echo "
                                        ";
        // line 68
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "direccionfacturacion"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefono"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 72
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 74
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefono"));
        echo "
                                        ";
        // line 75
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "telefono"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                                <div class=\"form-group  ";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "numerocuenta"))), "html", null, true);
        echo "\">
                                    <label for=\"exampleInputEmail1\">";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Número de cuenta"), "html", null, true);
        echo "</label>
                                    <div class=\"\">
                                        ";
        // line 81
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "numerocuenta"));
        echo "
                                        ";
        // line 82
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "numerocuenta"), array("attr" => array("class" => "form-control")));
        echo "                        
                                    </div>
                                </div>
                        </div>
                        <div id=\"api\" class=\"tab-pane\">
                            <div class=\"form-group ";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombrepersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 88
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 90
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "nombrepersonacontacto"));
        echo "
                                    ";
        // line 91
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "nombrepersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 94
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "apellidospersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 95
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 97
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "apellidospersonacontacto"));
        echo "
                                    ";
        // line 98
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "apellidospersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "emailpersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 104
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "emailpersonacontacto"));
        echo "
                                    ";
        // line 105
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "emailpersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 108
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefonopersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 109
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Teléfono de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 111
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "telefonopersonacontacto"));
        echo "
                                    ";
        // line 112
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "telefonopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                            <div class=\"form-group ";
        // line 115
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cargopersonacontacto"))), "html", null, true);
        echo "\">
                                <label for=\"exampleInputEmail1\">";
        // line 116
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cargo de la persona de contacto"), "html", null, true);
        echo "</label>
                                <div class=\"\">
                                    ";
        // line 118
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "cargopersonacontacto"));
        echo "
                                    ";
        // line 119
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "cargopersonacontacto"), array("attr" => array("class" => "form-control")));
        echo "                        
                                </div>
                            </div>
                        </div>
                        <div id=\"comisiones\" class=\"tab-pane\">

                             <div class=\"form-group\">
                                <label for=\"exampleInputEmail1\">";
        // line 126
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Porcentaje de comisión (campo enteros) , (campo decimales)."), "html", null, true);
        echo "</label>
                                <div class=\"clearfix\"></div>
                                <div class=\"col-sm-2-dcnt ";
        // line 128
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionempresaentero"))), "html", null, true);
        echo "\">
                                    ";
        // line 129
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionempresaentero"));
        echo "
                                    ";
        // line 130
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "comisionempresaentero"), array("attr" => array("class" => "form-control")));
        echo "                     
                                </div>
                                <div class=\"comaclear\" style=\"float:left\"> , </div>
                                <div class=\"col-sm-2-dcnt ";
        // line 133
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionempresadecimal"))), "html", null, true);
        echo "\">  
                                    ";
        // line 134
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "form"), "comisionempresadecimal"));
        echo "
                                    ";
        // line 135
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "form"), "comisionempresadecimal"), array("attr" => array("class" => "form-control")));
        echo "                                  
                                </div>
                                <div class=\"clearfix\"></div>
                            </div>
                        </div>
                        <div id=\"tpv\" class=\"tab-pane\">";
        // line 140
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("TPV"), "html", null, true);
        echo "</div>
                    </div>
                </div>
            </section>
            ";
        // line 144
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "form"));
        echo "
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-success\">";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    ";
        // line 150
        echo "                </div>
            </section>

            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->










    ";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Empresa:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  386 => 150,  381 => 147,  375 => 144,  368 => 140,  360 => 135,  356 => 134,  352 => 133,  346 => 130,  342 => 129,  338 => 128,  333 => 126,  323 => 119,  319 => 118,  314 => 116,  310 => 115,  304 => 112,  300 => 111,  295 => 109,  291 => 108,  285 => 105,  281 => 104,  276 => 102,  272 => 101,  266 => 98,  262 => 97,  257 => 95,  253 => 94,  247 => 91,  243 => 90,  238 => 88,  234 => 87,  226 => 82,  222 => 81,  217 => 79,  213 => 78,  207 => 75,  203 => 74,  198 => 72,  194 => 71,  188 => 68,  184 => 67,  179 => 65,  175 => 64,  169 => 61,  165 => 60,  160 => 58,  156 => 57,  150 => 54,  146 => 53,  141 => 51,  137 => 50,  131 => 47,  127 => 46,  122 => 44,  118 => 43,  112 => 40,  108 => 39,  103 => 37,  98 => 36,  92 => 34,  82 => 27,  78 => 26,  73 => 24,  69 => 23,  64 => 21,  60 => 20,  53 => 16,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
