<?php

/* HotelesFrontendBundle:Frontend:listadoajax.mv.twig */
class __TwigTemplate_65288fee61047fbb9e9022dba1c0c116 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div>
    ";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "hoteles"));
        foreach ($context['_seq'] as $context["_key"] => $context["objetohotel"]) {
            // line 3
            echo "        <div class=\"single_hotel clearfix  stars-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "calificacion"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "filtroServicios"), "html", null, true);
            echo "\" id=\"hotel-summary-";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">

            <div class=\"clearfix\">

                <div class=\"cont_img\">
                    ";
            // line 8
            if ((twig_length_filter($this->env, $this->getAttribute($this->getContext($context, "objetohotel"), "imagenes")) > 0)) {
                // line 9
                echo "                        <a href=\"\" title=\"\"><img height=\"152\" width=\"215\" src=\"/uploads/hoteles/";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "imagenes"), 0, array(), "array"), "imagen"), "html", null, true);
                echo "\" alt=\"\"></a>
                        ";
            }
            // line 11
            echo "                </div>
                <ul class=\"estrellas left\">
                    ";
            // line 13
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "calificacion")));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 14
                echo "                        <li class=\"star\">
                        </li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_merge($_parent, array_intersect_key($context, $_parent));
            // line 17
            echo "                    <div class=\"clearfix\"></div>
                </ul>

                <div class=\"dates left\">

                    <h2 class=\"name tit_hotel\"><a >";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "nombrehotel"), "html", null, true);
            echo "</a></h2>
                    <!-- servicios -->
                    <ul class=\"servicios\">
                        ";
            // line 25
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "piscina") == 1)) {
                // line 26
                echo "                            <li class=\"piscina\">Piscina
                            </li>
                        ";
            }
            // line 29
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "spa") == 1)) {
                // line 30
                echo "                            <li class=\"spa\">Spa
                            </li>
                        ";
            }
            // line 33
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "wiFi") == 1)) {
                // line 34
                echo "                            <li class=\"wi-fi\">Wi-fi
                            </li>
                        ";
            }
            // line 37
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "accesoAdaptado") == 1)) {
                // line 38
                echo "                            <li class=\"acceso-adaptado\">Acceso adaptado
                            </li>
                        ";
            }
            // line 41
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "aceptanPerros") == 1)) {
                // line 42
                echo "                            <li class=\"perros\">Aceptan perros
                            </li>
                        ";
            }
            // line 45
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "aparcamiento") == 1)) {
                // line 46
                echo "                            <li class=\"parking\">Aparcamiento/Parking
                            </li>
                        ";
            }
            // line 49
            echo "                        ";
            if (($this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "businessCenter") == 1)) {
                // line 50
                echo "                            <li class=\"business-center\">Business center
                            </li>
                        ";
            }
            // line 53
            echo "                        <div class=\"clearfix\"></div>
                    </ul>
                    <!-- icheckbox_timbre -->
                    <input class=\"icheckbox_timbre timbre\" name=\"hotel1\" type=\"checkbox\" data-hotel-id=\"";
            // line 56
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getContext($context, "objetohotel"), "hotel"), "id"), "html", null, true);
            echo "\">
                </div> 
                <div class=\"clearfix\"></div><!-- hotel_cont -->
            </div><!-- clearfix -->
        </div><!-- single_hotel -->
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['objetohotel'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 62
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle:Frontend:listadoajax.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 62,  134 => 56,  129 => 53,  124 => 50,  121 => 49,  116 => 46,  113 => 45,  108 => 42,  105 => 41,  100 => 38,  97 => 37,  92 => 34,  89 => 33,  84 => 30,  81 => 29,  76 => 26,  74 => 25,  68 => 22,  61 => 17,  53 => 14,  49 => 13,  45 => 11,  39 => 9,  37 => 8,  24 => 3,  20 => 2,  17 => 1,);
    }
}
