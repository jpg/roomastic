<?php

/* HotelesBackendBundle:Hotel:index.html.twig */
class __TwigTemplate_4793186c581fb7882362335c981d5ab4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Listado de hoteles"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "

    <div class=\"row\">
        <div class=\"col-lg-12\">
            <section class=\"panel\">
                <header class=\"panel-heading\">
                    ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de hoteles"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <!--  buscador + rtdos -->
                    <div class=\"adv-table\">
                        <div class=\"row\">
                            <div class=\"col-lg-6\">
                                <div id=\"editable-sample_length\" class=\"dataTables_length\">
                                    ";
        // line 23
        $this->env->loadTemplate("HotelesBackendBundle:Extras:select.html.twig")->display($context);
        // line 24
        echo "                                </div>
                            </div>
                            <div class=\"col-lg-6\">                                
                            </div>
                        </div>
                        <!-- / buscador + rtdos -->
                        <!--  contentTabla -->
                        <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-advance table-hover\" id=\"hidden-table-info\">
                            <thead>
                                <tr>
                                    <th>";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre de hotel"), "html", null, true);
        echo "</th>            
                                    <th>";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email de la persona de contacto"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 37
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Pvp oficial"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 38
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cantidad mínima aceptada"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Estado"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acciones"), "html", null, true);
        echo "</th>
                                </tr>
                            </thead>
                            <tbody>
                                ";
        // line 44
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "pagination"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 45
            echo "                                    <tr>
                                        <td><a href=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_edit", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "nombrehotel"), "html", null, true);
            echo "</a></td >        
                                        <td>";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "Empresa"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "Emailpersonacontacto"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "Pvpoficialenteros"), "html", null, true);
            echo ",";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "Pvpoficialdecimales"), "html", null, true);
            echo "</td>
                                        <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "Cantidadminimaaceptadaenteros"), "html", null, true);
            echo ",";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "Cantidadminimaaceptadadecimales"), "html", null, true);
            echo "</td>
                                        <td>
                                            ";
            // line 52
            $this->env->loadTemplate("HotelesBackendBundle:Extras:hotelStatus.html.twig")->display(array_merge($context, array("status" => $this->getAttribute($this->getContext($context, "entity"), "status"))));
            // line 53
            echo "                                        </td>
                                        <td>
                                            ";
            // line 55
            if ($this->getContext($context, "show_admin")) {
                // line 56
                echo "                                            <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_edit", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
                echo "\" class=\"btn btn-primary btn-xs\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar"), "html", null, true);
                echo "\"><i class=\"fa fa-pencil\"></i></a>
                                            ";
            }
            // line 58
            echo "                                        </td>
                                    </tr>
                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 61
        echo "                            </tbody>
                        </table>
                        <!--  /contentTabla -->

                    </div> <!-- /addtable -->
                </div><!-- /panelbody -->

            </section>

            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <div class=\"span6\">
                        ";
        // line 74
        if (($this->env->getExtension('security')->isGranted("ROLE_ADMIN") || $this->env->getExtension('security')->isGranted("ROLE_EMPRESA"))) {
            // line 75
            echo "                            <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_hotel_new"), "html", null, true);
            echo "\" class=\"btn btn-success\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear un nuevo hotel"), "html", null, true);
            echo "</a>
                        ";
        }
        // line 77
        echo "                    </div>
                    <div class=\"span6\">
                        <div class=\"dataTables_paginate paging_bootstrap pagination withbuttons\">
                            ";
        // line 80
        echo $this->env->getExtension('knp_pagination')->render($this->getContext($context, "pagination"), "HotelesBackendBundle:Paginacion:paginacion.html.twig");
        echo "
                        </div>
                    </div>                  
                </div>          
            </section>
            <!-- /btns -->

        </div><!-- /col -->

    </div><!-- /row -->

";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Hotel:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  217 => 80,  212 => 77,  204 => 75,  202 => 74,  187 => 61,  171 => 58,  163 => 56,  161 => 55,  157 => 53,  155 => 52,  148 => 50,  142 => 49,  138 => 48,  134 => 47,  128 => 46,  125 => 45,  108 => 44,  101 => 40,  97 => 39,  93 => 38,  89 => 37,  85 => 36,  81 => 35,  77 => 34,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
