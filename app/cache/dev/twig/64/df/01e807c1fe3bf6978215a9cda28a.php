<?php

/* HotelesBackendBundle:Incidencia:index.html.twig */
class __TwigTemplate_64df01e807c1fe3bf6978215a9cda28a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de incidencias"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "

<div class=\"row\">
    <div class=\"col-lg-12\">
      <section class=\"panel\">
          <header class=\"panel-heading\">
             ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Lista de incidencias"), "html", null, true);
        echo "
          </header>
          <div class=\"panel-body\">
                <!--  buscador + rtdos -->
                <div class=\"adv-table\">
                    <div class=\"row\">
                        <div class=\"col-lg-6\">
                            <div id=\"editable-sample_length\" class=\"dataTables_length\">
                                ";
        // line 23
        $this->env->loadTemplate("HotelesBackendBundle:Extras:select.html.twig")->display($context);
        // line 24
        echo "                            </div>
                        </div>
                        <div class=\"col-lg-6\">
                            ";
        // line 32
        echo "                        </div>
                    </div>
                    <!-- / buscador + rtdos -->
                    <!--  contentTabla -->
                    <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"table table-striped table-advance table-hover\" id=\"hidden-table-info\">

                          <thead>
                              <tr>
                                    <th>";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("De ..."), "html", null, true);
        echo "</th>
                                    <th>";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Para ..."), "html", null, true);
        echo "</th>
                                    <th>";
        // line 42
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Asunto"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 43
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 44
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Atendido"), "html", null, true);
        echo "</th>
                                    <th>";
        // line 45
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Acciones"), "html", null, true);
        echo "</th>

                              </tr>
                          </thead>
                          <tbody>
                            ";
        // line 50
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "pagination"));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
            // line 51
            echo "                                <tr>
                                    <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "usuariofrom"), "html", null, true);
            echo "</td>
                                    <td>";
            // line 53
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "usuarioto"), "html", null, true);
            echo "</td>
                                    <td>";
            // line 54
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "asunto"), "html", null, true);
            echo "</td>
                                    <td>";
            // line 55
            if ($this->getAttribute($this->getContext($context, "entity"), "fecha")) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "fecha"), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                                    <td>
                                        ";
            // line 57
            $this->env->loadTemplate("HotelesBackendBundle:Extras:checked.html.twig")->display(array_merge($context, array("dato" => $this->getAttribute($this->getContext($context, "entity"), "atendido"))));
            // line 58
            echo "                                    </td>
                                    <td>
                                        ";
            // line 63
            echo "                                        ";
            if (($this->getAttribute($this->getContext($context, "entity"), "atendido") != 1)) {
                // line 64
                echo "                                         <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia_atender", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
                echo "\" class=\"btn btn-primary btn-xs\" title=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Atendido"), "html", null, true);
                echo "\"><i class=\"fa fa-check\"></i></a>
                                         ";
            }
            // line 66
            echo "                                         <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia_edit", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
            echo "\" class=\"btn btn-primary btn-xs\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar"), "html", null, true);
            echo "\"><i class=\"fa fa-pencil\"></i></a>
   
                                    </td>
                                </tr>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 71
        echo "                          </tbody>
                    </table>
                    <!--  /contentTabla -->
                   
                </div> <!-- /addtable -->
            </div><!-- /panelbody -->

      </section>

      <!-- btns -->
      <section class=\"panel\">
            <div class=\"panel-body\">
                <div class=\"span6\">
                    <a href=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_incidencia_new"), "html", null, true);
        echo "\" class=\"btn btn-success\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Crear una nueva entrada"), "html", null, true);
        echo "</a>
                </div>
                <div class=\"span6\">
                  <div class=\"dataTables_paginate paging_bootstrap pagination withbuttons\">
                    ";
        // line 88
        echo $this->env->getExtension('knp_pagination')->render($this->getContext($context, "pagination"), "HotelesBackendBundle:Paginacion:paginacion.html.twig");
        echo "
                  </div>
                </div>                  
            </div>          
      </section>
      <!-- /btns -->
      

    </div><!-- /col -->

</div><!-- /row -->

";
        // line 155
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Incidencia:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 155,  210 => 88,  201 => 84,  186 => 71,  164 => 66,  156 => 64,  153 => 63,  149 => 58,  147 => 57,  140 => 55,  136 => 54,  132 => 53,  128 => 52,  125 => 51,  108 => 50,  100 => 45,  96 => 44,  92 => 43,  88 => 42,  84 => 41,  80 => 40,  70 => 32,  65 => 24,  63 => 23,  52 => 15,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
