<?php

/* HotelesBackendBundle:Extras:select.html.twig */
class __TwigTemplate_c88d55a91fbd73244f80a5c87f2f496e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["marcado"] = "50";
        // line 2
        if ($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "request"), "query"), "get", array(0 => "show"), "method")) {
            // line 3
            echo "    ";
            $context["marcado"] = $this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "request"), "query"), "get", array(0 => "show"), "method");
        }
        // line 5
        $context["valores"] = array(0 => "25", 1 => "50", 2 => "75");
        // line 6
        echo "<label>
    <select size=\"1\" name=\"editable-sample_length\" aria-controls=\"editable-sample\" class=\"form-control xsmall\" id=\"numSelect\">
        ";
        // line 8
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "valores"));
        foreach ($context['_seq'] as $context["_key"] => $context["v"]) {
            // line 9
            echo "            <option value=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "v"), "html", null, true);
            echo "\" ";
            if (($this->getContext($context, "v") == $this->getContext($context, "marcado"))) {
                echo "selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->getContext($context, "v"), "html", null, true);
            echo "</option>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
        // line 11
        echo "    </select> 
    ";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Resultados por página"), "html", null, true);
        echo "
</label>
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Extras:select.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 12,  50 => 11,  31 => 8,  25 => 5,  21 => 3,  19 => 2,  17 => 1,  508 => 201,  503 => 198,  491 => 185,  475 => 182,  467 => 179,  463 => 177,  455 => 172,  450 => 171,  442 => 166,  437 => 165,  435 => 164,  429 => 161,  426 => 160,  423 => 159,  420 => 158,  414 => 155,  407 => 154,  405 => 153,  400 => 152,  394 => 149,  389 => 148,  383 => 145,  378 => 144,  376 => 143,  371 => 141,  365 => 140,  360 => 138,  355 => 137,  353 => 132,  350 => 131,  347 => 130,  341 => 127,  336 => 126,  334 => 125,  329 => 124,  323 => 121,  318 => 120,  312 => 117,  307 => 116,  305 => 115,  300 => 113,  294 => 112,  289 => 110,  284 => 109,  282 => 104,  279 => 103,  276 => 102,  270 => 99,  263 => 98,  261 => 97,  256 => 96,  250 => 93,  245 => 92,  239 => 89,  234 => 88,  232 => 87,  227 => 85,  221 => 84,  216 => 82,  211 => 81,  209 => 76,  206 => 75,  202 => 74,  195 => 72,  192 => 71,  186 => 70,  180 => 68,  174 => 66,  168 => 64,  162 => 62,  159 => 61,  155 => 60,  151 => 58,  145 => 56,  139 => 54,  137 => 53,  132 => 51,  126 => 50,  123 => 49,  106 => 48,  98 => 43,  94 => 42,  90 => 41,  86 => 40,  82 => 39,  78 => 38,  69 => 31,  64 => 23,  62 => 22,  51 => 14,  44 => 9,  41 => 8,  35 => 9,  30 => 4,  27 => 6,);
    }
}
