<?php

/* HotelesBackendBundle:Contacto:edit.html.twig */
class __TwigTemplate_e89f44a2173c9c61e4b474a584ec8980 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar contacto"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "
    <div class=\"row\">
        <div class=\"col-lg-6\">
            <section class=\"panel\"> 
                <header class=\"panel-heading\">
                    ";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar contacto"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <form role=\"form\" action=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb_update", array("id" => $this->getAttribute($this->getContext($context, "contacto"), "id"))), "html", null, true);
        echo "\" method=\"post\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "edit_form"));
        echo ">
                        ";
        // line 19
        echo "                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 22
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombre"));
        echo "
                                ";
        // line 23
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "nombre"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Apellidos"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 29
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "apellidos"));
        echo "
                                ";
        // line 30
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "apellidos"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 36
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "email"));
        echo "
                                ";
        // line 37
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Texto"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 43
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "texto"));
        echo "
                                ";
        // line 44
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "texto"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 48
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Fecha"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 50
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "fecha"));
        echo "
                                ";
        // line 51
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "fecha"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>                        
                        <div class=\"form-group\">
                            <label for=\"exampleInputEmail1\">";
        // line 55
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Atendido"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 57
        echo $this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "atendido"));
        echo "
                                ";
        // line 58
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "atendido"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        ";
        // line 61
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "edit_form"));
        echo "
                </div>
            </section>

            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-info\">";
        // line 68
        echo "Guardar y marcar como atendido";
        echo "</button>   
                    </form><!-- / form edit -->
                    <a href=\"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
               <form action=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_contactoweb_delete", array("id" => $this->getAttribute($this->getContext($context, "contacto"), "id"))), "html", null, true);
        echo "\" method=\"post\" class=\"btnDelete\">
                        ";
        // line 72
        echo $this->env->getExtension('form')->renderWidget($this->getContext($context, "delete_form"));
        echo "
                        <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('";
        // line 73
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("¿Está seguro de borrar?"), "html", null, true);
        echo "');\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Borrar"), "html", null, true);
        echo "</button>
                    </form>
                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->



    ";
        // line 109
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Contacto:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 109,  190 => 73,  186 => 72,  182 => 71,  176 => 70,  171 => 68,  161 => 61,  155 => 58,  151 => 57,  146 => 55,  139 => 51,  135 => 50,  130 => 48,  123 => 44,  119 => 43,  114 => 41,  107 => 37,  103 => 36,  98 => 34,  91 => 30,  87 => 29,  82 => 27,  75 => 23,  71 => 22,  66 => 20,  63 => 19,  57 => 17,  51 => 14,  44 => 9,  41 => 8,  35 => 5,  30 => 4,  27 => 3,);
    }
}
