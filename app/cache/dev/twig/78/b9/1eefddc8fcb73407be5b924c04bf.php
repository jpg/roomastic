<?php

/* HotelesFrontendBundle::layout.mv.twig */
class __TwigTemplate_78b91eefddc8fcb73407be5b924c04bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'seotitle' => array($this, 'block_seotitle'),
            'seodescripcion' => array($this, 'block_seodescripcion'),
            'seokeywords' => array($this, 'block_seokeywords'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'modal' => array($this, 'block_modal'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7]>      <html class=\"no-js lt-ie9 lt-ie8 lt-ie7\"> <![endif]-->
<!--[if IE 7]>         <html class=\"no-js lt-ie9 lt-ie8\"> <![endif]-->
<!--[if IE 8]>         <html class=\"no-js lt-ie9\"> <![endif]-->
<!--[if gt IE 8]><!--> <html class=\"no-js\"> <!--<![endif]-->
    <head>

        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge,chrome=1\">
        <title>";
        // line 10
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        <meta name=\"title\" content=\"";
        // line 12
        $this->displayBlock('seotitle', $context, $blocks);
        echo "\">
        <meta name=\"description\" content=\"";
        // line 13
        $this->displayBlock('seodescripcion', $context, $blocks);
        echo "\">
        <meta name=\"keywords\" content=\"";
        // line 14
        $this->displayBlock('seokeywords', $context, $blocks);
        echo "\">
        <!--<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">-->
        <!-- favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"/bundles/hotelesfrontend/img/favicon.ico\" />
        <link rel=\"apple-touch-icon\" href=\"/bundles/hotelesfrontend/img/apple-touch-icon.png\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"/bundles/hotelesfrontend/img/apple-touch-icon-72x72.png\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"/bundles/hotelesfrontend/img/apple-touch-icon-114x114.png\">
        <!-- meta para mv -->
        <meta name=\"viewport\" content=\"width=700, target-densityDpi=device-dpi\">
        ";
        // line 23
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 38
        echo "
        <script src=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("bundles/fosjsrouting/js/router.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("fos_js_routing_js", array("callback" => "fos.Router.setData")), "html", null, true);
        echo "\"></script>
        <!--[if lt IE 9]>
            <script src=\"js/vendor/html5shiv/dist/html5shiv.min.js\"></script>
        <![endif]-->

    </head> 
    ";
        // line 46
        $context["rrss"] = $this->env->getExtension('twig_extension')->getRRSS();
        // line 47
        echo "    <script>
        if (typeof ROOMASTIC === 'undefined') {
            ROOMASTIC = {};
        }
        ROOMASTIC.version_mv = true;
    </script>
    <body class=\"portada\">
        <!--[if lt IE 7]>
            <p class=\"browsehappy\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- aviso cookie -->

        <!-- wrapper -->
        <div class=\"wrapper\">
            <!-- aviso cookie -->
            <div id=\"cookie-bar\" class=\"fixed\"></div>
            <!-- aviso cookie -->
            <!-- header -->
            <div class=\"headerpg\">
                <h1><a href=\"index\">Roomastic</a></h1>
            </div>
            <div class=\"container\">        
                ";
        // line 70
        $this->displayBlock('content', $context, $blocks);
        // line 71
        echo "             
            </div><!-- container -->

            <!-- footer -->
            <div class=\"footer\">
                <ul>
                    <li>
                        <a href=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath($this->getAttribute($this->getAttribute($this->getAttribute($this->getContext($context, "app"), "request"), "attributes"), "get", array(0 => "_route"), "method"), twig_array_merge($this->env->getExtension('twig_extension')->getCurrentRouteParams(), array("version" => "full"))), "html", null, true);
        echo "\" class=\"web\">VERSIÓN COMPLETA WEB</a>
                    </li>
                    <li class=\"last\"><a href=\"";
        // line 80
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("HotelesFrontendBundle_condicionesLegales"), "html", null, true);
        echo "\" class=\"legal\">AVISO LEGAL Y CONDICIONES </a></li>
                    <div class=\"clearfix\"></div>    
                </ul>
                <p>© Copyright. Todos los derechos reservados.</p>

            </div>
            <!-- /footer -->
        </div> 
        <!-- /wrapper -->
        ";
        // line 89
        $this->displayBlock('modal', $context, $blocks);
        // line 91
        echo "
        ";
        // line 92
        $this->displayBlock('javascripts', $context, $blocks);
        // line 157
        echo "    </body>
</html>
";
    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
        echo "Roomastic";
    }

    // line 12
    public function block_seotitle($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["seo"]) ? $context["seo"] : null), "seotitulo", array(), "any", true, true)) ? ($this->getAttribute($this->getContext($context, "seo"), "seotitulo")) : ("")), "html", null, true);
    }

    // line 13
    public function block_seodescripcion($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["seo"]) ? $context["seo"] : null), "seodescripcion", array(), "any", true, true)) ? ($this->getAttribute($this->getContext($context, "seo"), "seodescripcion")) : ("")), "html", null, true);
    }

    // line 14
    public function block_seokeywords($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["seo"]) ? $context["seo"] : null), "seokeywords", array(), "any", true, true)) ? ($this->getAttribute($this->getContext($context, "seo"), "seokeywords")) : ("")), "html", null, true);
    }

    // line 23
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 24
        echo "            <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

            <!--<link href=\"http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css\" rel=\"stylesheet\">-->
            <link href=\"https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css\" rel=\"stylesheet\">
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/daterangepicker-bs3.css\">
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/selectize.default.css\">
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/owl.carousel.css\">
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/base.css\"  />
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/jquery.fancybox.css\">
            <link rel=\"stylesheet\" href=\"/bundles/hotelesfrontend/css/plugins/sweet-alert.css\">
            ";
        // line 34
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "bfb8d30_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_bfb8d30_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/bfb8d30_movil_1.css");
            // line 35
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" />
            ";
        } else {
            // asset "bfb8d30"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_bfb8d30") : $this->env->getExtension('assets')->getAssetUrl("_controller/css/bfb8d30.css");
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\" />
            ";
        }
        unset($context["asset_url"]);
        // line 37
        echo "        ";
    }

    // line 70
    public function block_content($context, array $blocks = array())
    {
        // line 71
        echo "                ";
    }

    // line 89
    public function block_modal($context, array $blocks = array())
    {
        // line 90
        echo "        ";
    }

    // line 92
    public function block_javascripts($context, array $blocks = array())
    {
        // line 93
        echo "            <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js\"></script>
            <script>window.jQuery || document.write('<script src=\"/bundles/hotelesfrontend/js/vendor/jquery/dist/jquery.min.js\"><\\/script>')</script>

            <script src=\"/bundles/hotelesfrontend/js/plugins.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.backstretch.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/moment.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/daterangepicker.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/selectize.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/icheck.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/gmap3.min.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/owl.carousel.min.js\"></script>
            <script src=\"https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js\"></script>
            <!-- slide fotos detalle hotel -->
            <script type=\"text/javascript\" src=\"/bundles/hotelesfrontend/js/plugins/jquery.jcarousel.min.js\"></script>
            <script type=\"text/javascript\" src=\"/bundles/hotelesfrontend/js/plugins/jquery.pikachoose.js\"></script>
            <!-- fancybox -->
            <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.js\"></script>
            <script src=\"/bundles/hotelesfrontend/js/plugins/source/jquery.fancybox.pack.js\"></script>
            <!-- api maps -->
            <script src=\"https://maps.googleapis.com/maps/api/js?v=3.exp\"></script>
            <!-- cookies -->
            <script src=\"/bundles/hotelesfrontend/js/plugins/jquery.cookiebar.js\"></script>
            <script src=\"//code.jquery.com/ui/1.11.1/jquery-ui.js\"></script>
            <script type=\"text/javascript\">
                ";
        // line 117
        if (($this->getAttribute($this->getContext($context, "app"), "debug") == 1)) {
            // line 118
            echo "                    var env = 'dev';
                ";
        } else {
            // line 120
            echo "                    var env = 'prod';
                ";
        }
        // line 122
        echo "            </script>


            ";
        // line 125
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "592cb5a_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_592cb5a_0") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/592cb5a_roomastic_1.js");
            // line 126
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
            ";
        } else {
            // asset "592cb5a"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('routing')->getPath("_assetic_592cb5a") : $this->env->getExtension('assets')->getAssetUrl("_controller/js/592cb5a.js");
            echo "            <script src=\"";
            echo twig_escape_filter($this->env, $this->getContext($context, "asset_url"), "html", null, true);
            echo "\"></script>
            ";
        }
        unset($context["asset_url"]);
        // line 128
        echo "
            <!-- script slide foto hotel detalle -->
            <script type=\"text/javascript\">
                    jQuery(document).ready(function () {
                        var a = function (self) {
                            self.anchor.fancybox();
                        };
                        jQuery(\"#pikame\").PikaChoose({buildFinished: a});
                    });
            </script> 


            <script>
                (function (b, o, i, l, e, r) {
                    b.GoogleAnalyticsObject = l;
                    b[l] || (b[l] =
                            function () {
                                (b[l].q = b[l].q || []).push(arguments)
                            });
                    b[l].l = +new Date;
                    e = o.createElement(i);
                    r = o.getElementsByTagName(i)[0];
                    e.src = '//www.google-analytics.com/analytics.js';
                    r.parentNode.insertBefore(e, r)
                }(window, document, 'script', 'ga'));
                ga('create', 'UA-53266623-1');
                ga('send', 'pageview');
            </script>
        ";
    }

    public function getTemplateName()
    {
        return "HotelesFrontendBundle::layout.mv.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  284 => 128,  270 => 126,  266 => 125,  261 => 122,  257 => 120,  253 => 118,  251 => 117,  225 => 93,  222 => 92,  218 => 90,  211 => 71,  208 => 70,  204 => 37,  190 => 35,  174 => 24,  171 => 23,  165 => 14,  159 => 13,  153 => 12,  147 => 10,  141 => 157,  139 => 92,  136 => 91,  117 => 78,  108 => 71,  106 => 70,  81 => 47,  79 => 46,  66 => 39,  63 => 38,  61 => 23,  49 => 14,  45 => 13,  41 => 12,  25 => 1,  479 => 224,  476 => 222,  473 => 221,  462 => 216,  457 => 213,  452 => 210,  449 => 209,  444 => 206,  441 => 205,  436 => 202,  433 => 201,  428 => 198,  425 => 197,  420 => 194,  417 => 193,  412 => 190,  409 => 189,  404 => 186,  402 => 185,  396 => 182,  389 => 177,  381 => 174,  377 => 173,  373 => 171,  365 => 168,  358 => 167,  356 => 166,  347 => 163,  343 => 162,  334 => 156,  322 => 146,  311 => 143,  307 => 142,  300 => 141,  296 => 140,  227 => 74,  223 => 73,  215 => 89,  202 => 66,  199 => 65,  195 => 64,  186 => 34,  151 => 27,  148 => 26,  134 => 89,  131 => 24,  128 => 23,  125 => 22,  122 => 80,  104 => 20,  102 => 19,  97 => 16,  94 => 15,  86 => 12,  83 => 11,  76 => 12,  70 => 40,  65 => 10,  62 => 9,  40 => 6,  36 => 10,  31 => 3,  28 => 2,);
    }
}
