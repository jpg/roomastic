<?php

/* HotelesBackendBundle:UserEmpresa:edit.html.twig */
class __TwigTemplate_8d903b7f73ec0c7cda60a54b7b8a138d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("HotelesBackendBundle::layout.html.twig");

        $this->blocks = array(
            'titulo' => array($this, 'block_titulo'),
            'body' => array($this, 'block_body'),
            'jsextras' => array($this, 'block_jsextras'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "HotelesBackendBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_titulo($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("titulo", $context, $blocks);
        echo "
    ";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar usuarios de empresa"), "html", null, true);
        echo "
";
    }

    // line 8
    public function block_body($context, array $blocks = array())
    {
        // line 9
        echo "

    <div class=\"row\">
        <div class=\"col-lg-6\">
            <section class=\"panel\"> 
                <header class=\"panel-heading\">
                    ";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar usuarios de empresa"), "html", null, true);
        echo "
                </header>
                <div class=\"panel-body\">
                    <form action=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa_update", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
        echo "\" method=\"post\" id=\"formulariosubida\" ";
        echo $this->env->getExtension('form')->renderEnctype($this->getContext($context, "edit_form"));
        echo ">
                        ";
        // line 20
        echo "
                        <div class=\"form-group ";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "empresa"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Empresa"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 24
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "empresa"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 27
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "email"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Email"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 30
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "email"), array("attr" => array("class" => "form-control")));
        echo "                        
                            </div>
                        </div>
                        <div class=\"form-group ";
        // line 33
        echo twig_escape_filter($this->env, $this->env->getExtension('twig_extension')->error($this->env->getExtension('form')->renderErrors($this->getAttribute($this->getContext($context, "edit_form"), "nombrecompleto"))), "html", null, true);
        echo "\">
                            <label for=\"exampleInputEmail1\">";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Nombre completo"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                ";
        // line 36
        echo $this->env->getExtension('form')->renderWidget($this->getAttribute($this->getContext($context, "edit_form"), "nombrecompleto"), array("attr" => array("class" => "form-control")));
        echo "                    
                            </div>
                        </div>

                        <div class=\"form-group \">
                            <label for=\"exampleInputEmail1\">";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Imagen de usuario"), "html", null, true);
        echo "</label>
                            <div class=\"\">
                                <input type=\"file\" name=\"archivo\" id=\"inputsubida\">
                            </div>
                            <br>
                            <input type=\"hidden\" name=\"imagensubidanombre\" id=\"imagensubidanombre\" value=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "entity"), "imagen"), "html", null, true);
        echo "\">
                            ";
        // line 47
        if ((array_key_exists("imagendevuelta", $context) && ($this->getContext($context, "imagendevuelta") != ""))) {
            // line 48
            echo "                                <img src=\"/uploads/user/";
            echo twig_escape_filter($this->env, $this->getContext($context, "imagendevuelta"), "html", null, true);
            echo "\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                            ";
        } else {
            // line 50
            echo "                                <img src=\"\" widtH=\"200\" height=\"200\" id=\"imagenusuario\">
                            ";
        }
        // line 52
        echo "                            <iframe src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user")), "html", null, true);
        echo "\" height=\"0\" width=\"1300\" style=\"border:0px;\" name=\"inter\"></iframe>
                        </div>     
                        ";
        // line 54
        echo $this->env->getExtension('form')->renderRest($this->getContext($context, "edit_form"));
        echo "
                </div>
            </section>
            <!-- btns -->
            <section class=\"panel\">
                <div class=\"panel-body\">
                    <button type=\"submit\" class=\"btn btn-info\">";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar"), "html", null, true);
        echo "</button>   
                    </form><!-- / form edit -->
                    <a href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa"), "html", null, true);
        echo "\" class=\"btn btn-primary\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Regresar a la lista"), "html", null, true);
        echo "</a>
                    ";
        // line 63
        if ($this->env->getExtension('security')->isGranted("ROLE_ADMIN")) {
            // line 64
            echo "                        ";
            if ($this->getAttribute($this->getContext($context, "entity"), "isSuspended", array(), "method")) {
                // line 65
                echo "                            <form action=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa_reactivate", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
                echo "\" method=\"post\" class=\"btnDelete\">
                                ";
                // line 66
                echo $this->env->getExtension('form')->renderWidget($this->getContext($context, "reactivate_form"));
                echo "
                                <button type=\"submit\" class=\"btn btn-success\" onclick=\"return confirm('¿Está seguro de reactivar este usuario?');\">";
                // line 67
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Reactivar "), "html", null, true);
                echo "</button>
                            </form>
                        ";
            } else {
                // line 70
                echo "                            <form action=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("admin_userempresa_delete", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id"))), "html", null, true);
                echo "\" method=\"post\" class=\"btnDelete\">
                                ";
                // line 71
                echo $this->env->getExtension('form')->renderWidget($this->getContext($context, "delete_form"));
                echo "
                                <button type=\"submit\" class=\"btn btn-danger\" onclick=\"return confirm('¿Está seguro de borrar?');\">";
                // line 72
                echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Suspender"), "html", null, true);
                echo "</button>
                            </form>
                        ";
            }
            // line 75
            echo "                    ";
        }
        // line 76
        echo "                </div>
            </section>
            <!-- /btns -->
        </div><!-- /col -->
    </div><!-- /row -->




    ";
        // line 86
        echo "    ";
        // line 87
        echo "    ";
        // line 88
        echo "    ";
        // line 89
        echo "    ";
        // line 90
        echo "

    <a class=\"\" data-toggle=\"modal\" href=\"#myModal\" id=\"clickmodal\"></a>


    <div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" style=\"width: 923px;margin: 36px auto;height: 508px!important;\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                    <h4 class=\"modal-title\">";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Editar imagen"), "html", null, true);
        echo "</h4>
                </div>

                <div style=\"width:100%; height:auto; padding-top: 17px;\" >
                    <img src=\"/bundles/hotelesbackend/img/logo2.png\" id=\"target\" width=\"200\" alt=\"[Jcrop Example]\" />
                </div>

                <div class=\"clearfix\"></div>

                <div class=\"modal-footer\">
                    <button data-dismiss=\"modal\" id=\"closeimage\" class=\"btn btn-default\" type=\"button\">";
        // line 110
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Cerrar"), "html", null, true);
        echo "</button>
                    <button class=\"btn btn-success\" id=\"successimage\" data-id=\"\" data-url=\"\" data-img=\"\" type=\"button\">";
        // line 111
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Guardar cambios"), "html", null, true);
        echo "</button>
                </div>

            </div>
        </div>
    </div>
";
    }

    // line 119
    public function block_jsextras($context, array $blocks = array())
    {
        // line 120
        echo "
    ";
        // line 121
        $context["urlcrea"] = $this->env->getExtension('routing')->getPath("admin_userempresa_update", array("id" => $this->getAttribute($this->getContext($context, "entity"), "id")));
        // line 122
        echo "        ";
        $context["urlsubidaimagen"] = $this->env->getExtension('routing')->getPath("admin_imageupload", array("tiposubida" => "user"));
        // line 123
        echo "    ";
        $context["tipo"] = "user";
        // line 124
        echo "    ";
        $context["crop1"] = "100";
        // line 125
        echo "    ";
        $context["crop2"] = "100";
        // line 126
        echo "    ";
        $context["crop3"] = "100";
        // line 127
        echo "    ";
        $context["crop4"] = "100";
        // line 128
        echo "    ";
        $context["crop5"] = "1";
        // line 129
        echo "    ";
        $context["crop6"] = "1";
        // line 130
        echo "
    ";
        // line 131
        $this->env->loadTemplate("HotelesBackendBundle:Crop:crop.html.twig")->display(array_merge($context, array("especifico" => "user")));
        // line 132
        echo "
";
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:UserEmpresa:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  299 => 132,  297 => 131,  294 => 130,  291 => 129,  288 => 128,  285 => 127,  282 => 126,  279 => 125,  276 => 124,  273 => 123,  270 => 122,  268 => 121,  265 => 120,  262 => 119,  251 => 111,  247 => 110,  234 => 100,  222 => 90,  220 => 89,  218 => 88,  216 => 87,  214 => 86,  203 => 76,  200 => 75,  194 => 72,  190 => 71,  185 => 70,  179 => 67,  175 => 66,  170 => 65,  167 => 64,  165 => 63,  159 => 62,  154 => 60,  145 => 54,  139 => 52,  135 => 50,  129 => 48,  127 => 47,  123 => 46,  115 => 41,  107 => 36,  102 => 34,  98 => 33,  92 => 30,  87 => 28,  83 => 27,  77 => 24,  72 => 22,  68 => 21,  65 => 20,  59 => 18,  53 => 15,  45 => 9,  42 => 8,  36 => 5,  31 => 4,  28 => 3,);
    }
}
