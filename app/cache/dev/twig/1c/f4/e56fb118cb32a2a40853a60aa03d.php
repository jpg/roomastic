<?php

/* HotelesBackendBundle:Contacto:atendido.html.twig */
class __TwigTemplate_1cf4e56fb118cb32a2a40853a60aa03d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 5
        if (($this->getContext($context, "atendido") == 1)) {
            // line 6
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("Si"), "html", null, true);
            echo "
";
        } elseif (($this->getContext($context, "atendido") == 0)) {
            // line 8
            echo "    ";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans("No"), "html", null, true);
            echo "    
";
        }
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Contacto:atendido.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 8,  19 => 6,  17 => 5,  193 => 67,  171 => 62,  166 => 57,  160 => 56,  156 => 55,  152 => 54,  148 => 53,  144 => 52,  138 => 51,  135 => 50,  118 => 49,  112 => 46,  108 => 45,  104 => 44,  100 => 43,  96 => 42,  92 => 41,  88 => 40,  84 => 39,  71 => 29,  65 => 25,  63 => 24,  52 => 16,  44 => 10,  41 => 9,  35 => 5,  30 => 4,  27 => 3,);
    }
}
