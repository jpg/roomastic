<?php

/* HotelesBackendBundle:Lugar:sacamunicipios.html.twig */
class __TwigTemplate_80a8bc28640605a66931143555ef1f78 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (($this->getContext($context, "obligatorio") == 0)) {
            // line 2
            echo "    <option></option>
";
        }
        // line 4
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getContext($context, "municipios"));
        foreach ($context['_seq'] as $context["_key"] => $context["municipio"]) {
            // line 5
            echo "    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "municipio"), "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "municipio"), "nombre"), "html", null, true);
            echo "</option>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['municipio'], $context['_parent'], $context['loop']);
        $context = array_merge($_parent, array_intersect_key($context, $_parent));
    }

    public function getTemplateName()
    {
        return "HotelesBackendBundle:Lugar:sacamunicipios.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 5,  23 => 4,  19 => 2,  17 => 1,);
    }
}
