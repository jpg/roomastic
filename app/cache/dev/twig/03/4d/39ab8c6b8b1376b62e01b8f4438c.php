<?php

/* FOSUserBundle::form.html.twig */
class __TwigTemplate_034d39ab8c6b8b1376b62e01b8f4438c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field_label' => array($this, 'block_field_label'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('field_label', $context, $blocks);
    }

    public function block_field_label($context, array $blocks = array())
    {
        // line 3
        ob_start();
        // line 4
        echo "    <label for=\"";
        echo twig_escape_filter($this->env, $this->getContext($context, "id"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getContext($context, "id"), array(), "FOSUserBundle"), "html", null, true);
        echo "</label>
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "FOSUserBundle::form.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  27 => 3,  21 => 2,  66 => 18,  62 => 17,  58 => 16,  54 => 15,  49 => 14,  45 => 12,  39 => 10,  33 => 7,  19 => 2,  17 => 1,  96 => 42,  93 => 41,  86 => 43,  84 => 41,  80 => 39,  74 => 38,  65 => 35,  60 => 34,  55 => 33,  51 => 32,  18 => 1,  29 => 4,  26 => 4,);
    }
}
