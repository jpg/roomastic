<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appprodUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appprodUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = urldecode($pathinfo);

        // TooltypAuthGoogleBundle_login
        if ($pathinfo === '/login/google') {
            return array (  '_controller' => 'Tooltyp\\AuthGoogleBundle\\Controller\\DefaultController::indexAction',  '_route' => 'TooltypAuthGoogleBundle_login',);
        }

        // TooltypAuthTwitterBundle_homepage
        if ($pathinfo === '/login/twitter') {
            return array (  '_controller' => 'Tooltyp\\AuthTwitterBundle\\Controller\\DefaultController::indexAction',  '_route' => 'TooltypAuthTwitterBundle_homepage',);
        }

        // TooltypAuthFaceBundle_login
        if ($pathinfo === '/login/facebook') {
            return array (  '_controller' => 'Tooltyp\\AuthFaceBundle\\Controller\\DefaultController::indexAction',  '_route' => 'TooltypAuthFaceBundle_login',);
        }

        // hoteles_backend_notificacion_updatecounters
        if ($pathinfo === '/notificaciones/update') {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_hoteles_backend_notificacion_updatecounters;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NotificacionController::updateCountersAction',  '_route' => 'hoteles_backend_notificacion_updatecounters',);
        }
        not_hoteles_backend_notificacion_updatecounters:

        // admin_userhotel
        if (rtrim($pathinfo, '/') === '/admin/userhotel') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_userhotel');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::indexAction',  '_route' => 'admin_userhotel',);
        }

        // admin_userhotel_show
        if (0 === strpos($pathinfo, '/admin/userhotel') && preg_match('#^/admin/userhotel/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::showAction',)), array('_route' => 'admin_userhotel_show'));
        }

        // admin_userhotel_new
        if ($pathinfo === '/admin/userhotel/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::newAction',  '_route' => 'admin_userhotel_new',);
        }

        // admin_userhotel_create
        if ($pathinfo === '/admin/userhotel/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_userhotel_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::createAction',  '_route' => 'admin_userhotel_create',);
        }
        not_admin_userhotel_create:

        // admin_userhotel_edit
        if (0 === strpos($pathinfo, '/admin/userhotel') && preg_match('#^/admin/userhotel/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::editAction',)), array('_route' => 'admin_userhotel_edit'));
        }

        // admin_userhotel_update
        if (0 === strpos($pathinfo, '/admin/userhotel') && preg_match('#^/admin/userhotel/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_userhotel_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::updateAction',)), array('_route' => 'admin_userhotel_update'));
        }
        not_admin_userhotel_update:

        // admin_userhotel_delete
        if (0 === strpos($pathinfo, '/admin/userhotel') && preg_match('#^/admin/userhotel/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_userhotel_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::deleteAction',)), array('_route' => 'admin_userhotel_delete'));
        }
        not_admin_userhotel_delete:

        // admin_userhotel_reactivate
        if (0 === strpos($pathinfo, '/admin/userhotel') && preg_match('#^/admin/userhotel/(?P<id>[^/]+?)/reactivate$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_userhotel_reactivate;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserHotelController::reactivateUserAction',)), array('_route' => 'admin_userhotel_reactivate'));
        }
        not_admin_userhotel_reactivate:

        // admin_homeconfiguracion
        if (rtrim($pathinfo, '/') === '/admin/homeconfiguracion') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_homeconfiguracion');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::indexAction',  '_route' => 'admin_homeconfiguracion',);
        }

        // admin_ordenahome
        if (0 === strpos($pathinfo, '/admin/homeconfiguracion/ordena') && preg_match('#^/admin/homeconfiguracion/ordena/(?P<orden>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::ordenaAction',)), array('_route' => 'admin_ordenahome'));
        }

        // admin_homeconfiguracion_show
        if (0 === strpos($pathinfo, '/admin/homeconfiguracion') && preg_match('#^/admin/homeconfiguracion/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::showAction',)), array('_route' => 'admin_homeconfiguracion_show'));
        }

        // admin_homeconfiguracion_new
        if ($pathinfo === '/admin/homeconfiguracion/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::newAction',  '_route' => 'admin_homeconfiguracion_new',);
        }

        // admin_homeconfiguracion_create
        if ($pathinfo === '/admin/homeconfiguracion/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_homeconfiguracion_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::createAction',  '_route' => 'admin_homeconfiguracion_create',);
        }
        not_admin_homeconfiguracion_create:

        // admin_homeconfiguracion_edit
        if (0 === strpos($pathinfo, '/admin/homeconfiguracion') && preg_match('#^/admin/homeconfiguracion/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::editAction',)), array('_route' => 'admin_homeconfiguracion_edit'));
        }

        // admin_homeconfiguracion_update
        if (0 === strpos($pathinfo, '/admin/homeconfiguracion') && preg_match('#^/admin/homeconfiguracion/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_homeconfiguracion_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::updateAction',)), array('_route' => 'admin_homeconfiguracion_update'));
        }
        not_admin_homeconfiguracion_update:

        // admin_homeconfiguracion_delete
        if (0 === strpos($pathinfo, '/admin/homeconfiguracion') && preg_match('#^/admin/homeconfiguracion/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_homeconfiguracion_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::deleteAction',)), array('_route' => 'admin_homeconfiguracion_delete'));
        }
        not_admin_homeconfiguracion_delete:

        // admin_homeconfiguracion_deleteget
        if (0 === strpos($pathinfo, '/admin/homeconfiguracion') && preg_match('#^/admin/homeconfiguracion/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_admin_homeconfiguracion_deleteget;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HomeController::deletegetAction',)), array('_route' => 'admin_homeconfiguracion_deleteget'));
        }
        not_admin_homeconfiguracion_deleteget:

        // admin_newsletter
        if (0 === strpos($pathinfo, '/admin/newsletter') && preg_match('#^/admin/newsletter(?:/(?P<email>[^/]+?))?$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  'email' => NULL,  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::indexAction',)), array('_route' => 'admin_newsletter'));
        }

        // admin_newsletter_show
        if (0 === strpos($pathinfo, '/admin/newsletter') && preg_match('#^/admin/newsletter/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::showAction',)), array('_route' => 'admin_newsletter_show'));
        }

        // admin_newsletter_new
        if ($pathinfo === '/admin/newsletter/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::newAction',  '_route' => 'admin_newsletter_new',);
        }

        // admin_newsletter_create
        if ($pathinfo === '/admin/newsletter/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_newsletter_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::createAction',  '_route' => 'admin_newsletter_create',);
        }
        not_admin_newsletter_create:

        // admin_newsletter_edit
        if (0 === strpos($pathinfo, '/admin/newsletter') && preg_match('#^/admin/newsletter/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::editAction',)), array('_route' => 'admin_newsletter_edit'));
        }

        // admin_newsletter_update
        if (0 === strpos($pathinfo, '/admin/newsletter') && preg_match('#^/admin/newsletter/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_newsletter_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::updateAction',)), array('_route' => 'admin_newsletter_update'));
        }
        not_admin_newsletter_update:

        // admin_newsletter_delete
        if (0 === strpos($pathinfo, '/admin/newsletter') && preg_match('#^/admin/newsletter/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_newsletter_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::deleteAction',)), array('_route' => 'admin_newsletter_delete'));
        }
        not_admin_newsletter_delete:

        // admin_newsletter_activate
        if (0 === strpos($pathinfo, '/admin/newsletter') && preg_match('#^/admin/newsletter/(?P<id>[^/]+?)/activate$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_newsletter_activate;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\NewsletterController::activateAction',)), array('_route' => 'admin_newsletter_activate'));
        }
        not_admin_newsletter_activate:

        // hoteles_backend_passwordreset_afterreset
        if ($pathinfo === '/password-cambiada') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PasswordResetController::afterResetAction',  '_route' => 'hoteles_backend_passwordreset_afterreset',);
        }

        // admin_privacidad
        if (rtrim($pathinfo, '/') === '/admin/privacidad') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_privacidad');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::indexAction',  '_route' => 'admin_privacidad',);
        }

        // admin_privacidad_show
        if (0 === strpos($pathinfo, '/admin/privacidad') && preg_match('#^/admin/privacidad/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::showAction',)), array('_route' => 'admin_privacidad_show'));
        }

        // admin_privacidad_new
        if ($pathinfo === '/admin/privacidad/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::newAction',  '_route' => 'admin_privacidad_new',);
        }

        // admin_privacidad_create
        if ($pathinfo === '/admin/privacidad/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_privacidad_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::createAction',  '_route' => 'admin_privacidad_create',);
        }
        not_admin_privacidad_create:

        // admin_privacidad_edit
        if (0 === strpos($pathinfo, '/admin/privacidad') && preg_match('#^/admin/privacidad/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::editAction',)), array('_route' => 'admin_privacidad_edit'));
        }

        // admin_privacidad_update
        if (0 === strpos($pathinfo, '/admin/privacidad') && preg_match('#^/admin/privacidad/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_privacidad_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::updateAction',)), array('_route' => 'admin_privacidad_update'));
        }
        not_admin_privacidad_update:

        // admin_privacidad_delete
        if (0 === strpos($pathinfo, '/admin/privacidad') && preg_match('#^/admin/privacidad/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_privacidad_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PrivacidadController::deleteAction',)), array('_route' => 'admin_privacidad_delete'));
        }
        not_admin_privacidad_delete:

        // admin_imageupload
        if (0 === strpos($pathinfo, '/admin/imageupload') && preg_match('#^/admin/imageupload/(?P<tiposubida>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImageuploadController::indexAction',)), array('_route' => 'admin_imageupload'));
        }

        // admin_imagecrop
        if (0 === strpos($pathinfo, '/admin/cropimagen') && preg_match('#^/admin/cropimagen/(?P<imagen>[^/]+?)/(?P<posx>[^/]+?)/(?P<posy>[^/]+?)/(?P<width>[^/]+?)/(?P<height>[^/]+?)/(?P<carpeta>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImageuploadController::cropAction',)), array('_route' => 'admin_imagecrop'));
        }

        // hoteles_backend_imageupload_deletetempfile
        if (0 === strpos($pathinfo, '/admin/deletetemp') && preg_match('#^/admin/deletetemp/(?P<imagen>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImageuploadController::deleteTempFileAction',)), array('_route' => 'hoteles_backend_imageupload_deletetempfile'));
        }

        // admin_seo
        if (rtrim($pathinfo, '/') === '/admin/seo') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_seo');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::indexAction',  '_route' => 'admin_seo',);
        }

        // admin_seo_show
        if (0 === strpos($pathinfo, '/admin/seo') && preg_match('#^/admin/seo/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::showAction',)), array('_route' => 'admin_seo_show'));
        }

        // admin_seo_new
        if ($pathinfo === '/admin/seo/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::newAction',  '_route' => 'admin_seo_new',);
        }

        // admin_seo_create
        if ($pathinfo === '/admin/seo/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_seo_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::createAction',  '_route' => 'admin_seo_create',);
        }
        not_admin_seo_create:

        // admin_seo_edit
        if (0 === strpos($pathinfo, '/admin/seo') && preg_match('#^/admin/seo/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::editAction',)), array('_route' => 'admin_seo_edit'));
        }

        // admin_seo_update
        if (0 === strpos($pathinfo, '/admin/seo') && preg_match('#^/admin/seo/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_seo_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::updateAction',)), array('_route' => 'admin_seo_update'));
        }
        not_admin_seo_update:

        // admin_seo_delete
        if (0 === strpos($pathinfo, '/admin/seo') && preg_match('#^/admin/seo/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_seo_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\SeoController::deleteAction',)), array('_route' => 'admin_seo_delete'));
        }
        not_admin_seo_delete:

        // admin_lugares
        if (rtrim($pathinfo, '/') === '/admin/lugares') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_lugares');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::indexAction',  '_route' => 'admin_lugares',);
        }

        // admin_lugaresfiltramunicipio
        if (0 === strpos($pathinfo, '/admin/lugares/filtramunicipios') && preg_match('#^/admin/lugares/filtramunicipios/(?P<idprovincia>[^/]+?)/(?P<obligatorio>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::sacamunicipiosAction',)), array('_route' => 'admin_lugaresfiltramunicipio'));
        }

        // admin_lugaresfiltralugares
        if (0 === strpos($pathinfo, '/admin/lugares/filtrazonas') && preg_match('#^/admin/lugares/filtrazonas/(?P<idprovincia>[^/]+?)/(?P<idmunicipio>[^/]+?)/(?P<obligatorio>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::sacalugaresAction',)), array('_route' => 'admin_lugaresfiltralugares'));
        }

        // admin_lugares_show
        if (0 === strpos($pathinfo, '/admin/lugares') && preg_match('#^/admin/lugares/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::showAction',)), array('_route' => 'admin_lugares_show'));
        }

        // admin_lugares_new
        if ($pathinfo === '/admin/lugares/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::newAction',  '_route' => 'admin_lugares_new',);
        }

        // admin_lugares_create
        if ($pathinfo === '/admin/lugares/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_lugares_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::createAction',  '_route' => 'admin_lugares_create',);
        }
        not_admin_lugares_create:

        // admin_lugares_edit
        if (0 === strpos($pathinfo, '/admin/lugares') && preg_match('#^/admin/lugares/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::editAction',)), array('_route' => 'admin_lugares_edit'));
        }

        // admin_lugares_update
        if (0 === strpos($pathinfo, '/admin/lugares') && preg_match('#^/admin/lugares/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_lugares_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::updateAction',)), array('_route' => 'admin_lugares_update'));
        }
        not_admin_lugares_update:

        // admin_lugares_delete
        if (0 === strpos($pathinfo, '/admin/lugares') && preg_match('#^/admin/lugares/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_lugares_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LugarController::deleteAction',)), array('_route' => 'admin_lugares_delete'));
        }
        not_admin_lugares_delete:

        // admin_cookies
        if (rtrim($pathinfo, '/') === '/admin/cookies') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_cookies');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::indexAction',  '_route' => 'admin_cookies',);
        }

        // admin_cookies_show
        if (0 === strpos($pathinfo, '/admin/cookies') && preg_match('#^/admin/cookies/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::showAction',)), array('_route' => 'admin_cookies_show'));
        }

        // admin_cookies_new
        if ($pathinfo === '/admin/cookies/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::newAction',  '_route' => 'admin_cookies_new',);
        }

        // admin_cookies_create
        if ($pathinfo === '/admin/cookies/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_cookies_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::createAction',  '_route' => 'admin_cookies_create',);
        }
        not_admin_cookies_create:

        // admin_cookies_edit
        if (0 === strpos($pathinfo, '/admin/cookies') && preg_match('#^/admin/cookies/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::editAction',)), array('_route' => 'admin_cookies_edit'));
        }

        // admin_cookies_update
        if (0 === strpos($pathinfo, '/admin/cookies') && preg_match('#^/admin/cookies/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_cookies_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::updateAction',)), array('_route' => 'admin_cookies_update'));
        }
        not_admin_cookies_update:

        // admin_cookies_delete
        if (0 === strpos($pathinfo, '/admin/cookies') && preg_match('#^/admin/cookies/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_cookies_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CookiesController::deleteAction',)), array('_route' => 'admin_cookies_delete'));
        }
        not_admin_cookies_delete:

        // admin_faqs
        if (rtrim($pathinfo, '/') === '/admin/faqs') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_faqs');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::indexAction',  '_route' => 'admin_faqs',);
        }

        // admin_ordena
        if (0 === strpos($pathinfo, '/admin/faqs/ordena') && preg_match('#^/admin/faqs/ordena/(?P<orden>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::ordenaAction',)), array('_route' => 'admin_ordena'));
        }

        // admin_faqs_show
        if (0 === strpos($pathinfo, '/admin/faqs') && preg_match('#^/admin/faqs/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::showAction',)), array('_route' => 'admin_faqs_show'));
        }

        // admin_faqs_new
        if ($pathinfo === '/admin/faqs/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::newAction',  '_route' => 'admin_faqs_new',);
        }

        // admin_faqs_create
        if ($pathinfo === '/admin/faqs/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_faqs_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::createAction',  '_route' => 'admin_faqs_create',);
        }
        not_admin_faqs_create:

        // admin_faqs_edit
        if (0 === strpos($pathinfo, '/admin/faqs') && preg_match('#^/admin/faqs/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::editAction',)), array('_route' => 'admin_faqs_edit'));
        }

        // admin_faqs_update
        if (0 === strpos($pathinfo, '/admin/faqs') && preg_match('#^/admin/faqs/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_faqs_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::updateAction',)), array('_route' => 'admin_faqs_update'));
        }
        not_admin_faqs_update:

        // admin_faqs_delete
        if (0 === strpos($pathinfo, '/admin/faqs') && preg_match('#^/admin/faqs/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_faqs_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\FaqsController::deleteAction',)), array('_route' => 'admin_faqs_delete'));
        }
        not_admin_faqs_delete:

        // admin_landing
        if (rtrim($pathinfo, '/') === '/admin/dashboard') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_landing');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\LandingController::indexAction',  '_route' => 'admin_landing',);
        }

        // admin_condicioneslegales
        if (rtrim($pathinfo, '/') === '/admin/condicioneslegales') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_condicioneslegales');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::indexAction',  '_route' => 'admin_condicioneslegales',);
        }

        // admin_condicioneslegales_show
        if (0 === strpos($pathinfo, '/admin/condicioneslegales') && preg_match('#^/admin/condicioneslegales/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::showAction',)), array('_route' => 'admin_condicioneslegales_show'));
        }

        // admin_condicioneslegales_new
        if ($pathinfo === '/admin/condicioneslegales/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::newAction',  '_route' => 'admin_condicioneslegales_new',);
        }

        // admin_condicioneslegales_create
        if ($pathinfo === '/admin/condicioneslegales/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_condicioneslegales_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::createAction',  '_route' => 'admin_condicioneslegales_create',);
        }
        not_admin_condicioneslegales_create:

        // admin_condicioneslegales_edit
        if (0 === strpos($pathinfo, '/admin/condicioneslegales') && preg_match('#^/admin/condicioneslegales/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::editAction',)), array('_route' => 'admin_condicioneslegales_edit'));
        }

        // admin_condicioneslegales_update
        if (0 === strpos($pathinfo, '/admin/condicioneslegales') && preg_match('#^/admin/condicioneslegales/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_condicioneslegales_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::updateAction',)), array('_route' => 'admin_condicioneslegales_update'));
        }
        not_admin_condicioneslegales_update:

        // admin_condicioneslegales_delete
        if (0 === strpos($pathinfo, '/admin/condicioneslegales') && preg_match('#^/admin/condicioneslegales/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_condicioneslegales_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CondicioneslegalesController::deleteAction',)), array('_route' => 'admin_condicioneslegales_delete'));
        }
        not_admin_condicioneslegales_delete:

        // admin_usuario
        if (rtrim($pathinfo, '/') === '/admin/usuario') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_usuario');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::indexAction',  '_route' => 'admin_usuario',);
        }

        // admin_usuario_show
        if (0 === strpos($pathinfo, '/admin/usuario') && preg_match('#^/admin/usuario/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::showAction',)), array('_route' => 'admin_usuario_show'));
        }

        // admin_usuario_new
        if ($pathinfo === '/admin/usuario/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::newAction',  '_route' => 'admin_usuario_new',);
        }

        // admin_usuario_create
        if ($pathinfo === '/admin/usuario/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_usuario_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::createAction',  '_route' => 'admin_usuario_create',);
        }
        not_admin_usuario_create:

        // admin_usuario_edit
        if (0 === strpos($pathinfo, '/admin/usuario') && preg_match('#^/admin/usuario/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::editAction',)), array('_route' => 'admin_usuario_edit'));
        }

        // admin_usuario_update
        if (0 === strpos($pathinfo, '/admin/usuario') && preg_match('#^/admin/usuario/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_usuario_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::updateAction',)), array('_route' => 'admin_usuario_update'));
        }
        not_admin_usuario_update:

        // admin_usuario_delete
        if (0 === strpos($pathinfo, '/admin/usuario') && preg_match('#^/admin/usuario/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_usuario_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::deleteAction',)), array('_route' => 'admin_usuario_delete'));
        }
        not_admin_usuario_delete:

        // admin_usuario_deleteget
        if (0 === strpos($pathinfo, '/admin/usuario') && preg_match('#^/admin/usuario/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_admin_usuario_deleteget;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::deletegetAction',)), array('_route' => 'admin_usuario_deleteget'));
        }
        not_admin_usuario_deleteget:

        // admin_usuario_enabled
        if (0 === strpos($pathinfo, '/admin/usuario') && preg_match('#^/admin/usuario/(?P<id>[^/]+?)/enabled$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::enabledAction',)), array('_route' => 'admin_usuario_enabled'));
        }

        // admin_usuario_disabled
        if (0 === strpos($pathinfo, '/admin/usuario') && preg_match('#^/admin/usuario/(?P<id>[^/]+?)/disabled$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::disabledAction',)), array('_route' => 'admin_usuario_disabled'));
        }

        // admin_usuario_ban
        if (0 === strpos($pathinfo, '/admin/usuario') && preg_match('#^/admin/usuario/(?P<id>[^/]+?)/ban$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::banAction',)), array('_route' => 'admin_usuario_ban'));
        }

        // admin_usuario_unban
        if (0 === strpos($pathinfo, '/admin/usuario') && preg_match('#^/admin/usuario/(?P<id>[^/]+?)/unban$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserController::unbanAction',)), array('_route' => 'admin_usuario_unban'));
        }

        // admin_configuracion
        if (rtrim($pathinfo, '/') === '/admin/configuracion') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_configuracion');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::indexAction',  '_route' => 'admin_configuracion',);
        }

        // admin_configuracion_show
        if (0 === strpos($pathinfo, '/admin/configuracion') && preg_match('#^/admin/configuracion/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::showAction',)), array('_route' => 'admin_configuracion_show'));
        }

        // admin_configuracion_new
        if ($pathinfo === '/admin/configuracion/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::newAction',  '_route' => 'admin_configuracion_new',);
        }

        // admin_configuracion_create
        if ($pathinfo === '/admin/configuracion/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_configuracion_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::createAction',  '_route' => 'admin_configuracion_create',);
        }
        not_admin_configuracion_create:

        // admin_configuracion_edit
        if (0 === strpos($pathinfo, '/admin/configuracion') && preg_match('#^/admin/configuracion/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::editAction',)), array('_route' => 'admin_configuracion_edit'));
        }

        // admin_configuracion_update
        if (0 === strpos($pathinfo, '/admin/configuracion') && preg_match('#^/admin/configuracion/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_configuracion_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::updateAction',)), array('_route' => 'admin_configuracion_update'));
        }
        not_admin_configuracion_update:

        // admin_configuracion_delete
        if (0 === strpos($pathinfo, '/admin/configuracion') && preg_match('#^/admin/configuracion/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_configuracion_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ConfiguracionController::deleteAction',)), array('_route' => 'admin_configuracion_delete'));
        }
        not_admin_configuracion_delete:

        // admin_publicatuhotel
        if (rtrim($pathinfo, '/') === '/admin/publicatuhotel') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_publicatuhotel');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::indexAction',  '_route' => 'admin_publicatuhotel',);
        }

        // admin_publicatuhotel_show
        if (0 === strpos($pathinfo, '/admin/publicatuhotel') && preg_match('#^/admin/publicatuhotel/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::showAction',)), array('_route' => 'admin_publicatuhotel_show'));
        }

        // admin_publicatuhotel_new
        if ($pathinfo === '/admin/publicatuhotel/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::newAction',  '_route' => 'admin_publicatuhotel_new',);
        }

        // admin_publicatuhotel_create
        if ($pathinfo === '/admin/publicatuhotel/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_publicatuhotel_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::createAction',  '_route' => 'admin_publicatuhotel_create',);
        }
        not_admin_publicatuhotel_create:

        // admin_publicatuhotel_edit
        if (0 === strpos($pathinfo, '/admin/publicatuhotel') && preg_match('#^/admin/publicatuhotel/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::editAction',)), array('_route' => 'admin_publicatuhotel_edit'));
        }

        // admin_publicatuhotel_update
        if (0 === strpos($pathinfo, '/admin/publicatuhotel') && preg_match('#^/admin/publicatuhotel/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_publicatuhotel_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::updateAction',)), array('_route' => 'admin_publicatuhotel_update'));
        }
        not_admin_publicatuhotel_update:

        // admin_publicatuhotel_delete
        if (0 === strpos($pathinfo, '/admin/publicatuhotel') && preg_match('#^/admin/publicatuhotel/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_publicatuhotel_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\PublicatuhotelController::deleteAction',)), array('_route' => 'admin_publicatuhotel_delete'));
        }
        not_admin_publicatuhotel_delete:

        // admin_imagenhotel
        if (rtrim($pathinfo, '/') === '/admin/imagenhotel') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_imagenhotel');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::indexAction',  '_route' => 'admin_imagenhotel',);
        }

        // admin_imagenhotel_show
        if (0 === strpos($pathinfo, '/admin/imagenhotel') && preg_match('#^/admin/imagenhotel/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::showAction',)), array('_route' => 'admin_imagenhotel_show'));
        }

        // admin_imagenhotel_new
        if ($pathinfo === '/admin/imagenhotel/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::newAction',  '_route' => 'admin_imagenhotel_new',);
        }

        // admin_imagenhotel_create
        if ($pathinfo === '/admin/imagenhotel/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_imagenhotel_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::createAction',  '_route' => 'admin_imagenhotel_create',);
        }
        not_admin_imagenhotel_create:

        // admin_imagenhotel_edit
        if (0 === strpos($pathinfo, '/admin/imagenhotel') && preg_match('#^/admin/imagenhotel/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::editAction',)), array('_route' => 'admin_imagenhotel_edit'));
        }

        // admin_imagenhotel_update
        if (0 === strpos($pathinfo, '/admin/imagenhotel') && preg_match('#^/admin/imagenhotel/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_imagenhotel_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::updateAction',)), array('_route' => 'admin_imagenhotel_update'));
        }
        not_admin_imagenhotel_update:

        // admin_imagenhotel_delete
        if (0 === strpos($pathinfo, '/admin/imagenhotel') && preg_match('#^/admin/imagenhotel/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_imagenhotel_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ImagenhotelController::deleteAction',)), array('_route' => 'admin_imagenhotel_delete'));
        }
        not_admin_imagenhotel_delete:

        // admin_userempresa
        if (rtrim($pathinfo, '/') === '/admin/userempresa') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_userempresa');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::indexAction',  '_route' => 'admin_userempresa',);
        }

        // admin_userempresa_show
        if (0 === strpos($pathinfo, '/admin/userempresa') && preg_match('#^/admin/userempresa/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::showAction',)), array('_route' => 'admin_userempresa_show'));
        }

        // admin_userempresa_new
        if ($pathinfo === '/admin/userempresa/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::newAction',  '_route' => 'admin_userempresa_new',);
        }

        // admin_userempresa_create
        if ($pathinfo === '/admin/userempresa/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_userempresa_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::createAction',  '_route' => 'admin_userempresa_create',);
        }
        not_admin_userempresa_create:

        // admin_userempresa_edit
        if (0 === strpos($pathinfo, '/admin/userempresa') && preg_match('#^/admin/userempresa/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::editAction',)), array('_route' => 'admin_userempresa_edit'));
        }

        // admin_userempresa_update
        if (0 === strpos($pathinfo, '/admin/userempresa') && preg_match('#^/admin/userempresa/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_userempresa_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::updateAction',)), array('_route' => 'admin_userempresa_update'));
        }
        not_admin_userempresa_update:

        // admin_userempresa_delete
        if (0 === strpos($pathinfo, '/admin/userempresa') && preg_match('#^/admin/userempresa/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_userempresa_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::deleteAction',)), array('_route' => 'admin_userempresa_delete'));
        }
        not_admin_userempresa_delete:

        // admin_userempresa_reactivate
        if (0 === strpos($pathinfo, '/admin/userempresa') && preg_match('#^/admin/userempresa/(?P<id>[^/]+?)/reactivate$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_userempresa_reactivate;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserEmpresaController::reactivateUserAction',)), array('_route' => 'admin_userempresa_reactivate'));
        }
        not_admin_userempresa_reactivate:

        // hoteles_backend_export_exportnewsletter
        if ($pathinfo === '/admin/export/newsletter') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ExportController::exportNewsletter',  '_route' => 'hoteles_backend_export_exportnewsletter',);
        }

        // admin_empresa
        if (rtrim($pathinfo, '/') === '/admin/empresa') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_empresa');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::indexAction',  '_route' => 'admin_empresa',);
        }

        // admin_empresa_show
        if (0 === strpos($pathinfo, '/admin/empresa') && preg_match('#^/admin/empresa/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::showAction',)), array('_route' => 'admin_empresa_show'));
        }

        // admin_empresa_new
        if ($pathinfo === '/admin/empresa/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::newAction',  '_route' => 'admin_empresa_new',);
        }

        // admin_empresa_create
        if ($pathinfo === '/admin/empresa/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_empresa_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::createAction',  '_route' => 'admin_empresa_create',);
        }
        not_admin_empresa_create:

        // admin_empresa_edit
        if (0 === strpos($pathinfo, '/admin/empresa') && preg_match('#^/admin/empresa/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::editAction',)), array('_route' => 'admin_empresa_edit'));
        }

        // admin_empresa_update
        if (0 === strpos($pathinfo, '/admin/empresa') && preg_match('#^/admin/empresa/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_empresa_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::updateAction',)), array('_route' => 'admin_empresa_update'));
        }
        not_admin_empresa_update:

        // admin_empresa_delete
        if (0 === strpos($pathinfo, '/admin/empresa') && preg_match('#^/admin/empresa/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_empresa_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::deleteAction',)), array('_route' => 'admin_empresa_delete'));
        }
        not_admin_empresa_delete:

        // admin_empresa_reactivate
        if (0 === strpos($pathinfo, '/admin/empresa') && preg_match('#^/admin/empresa/(?P<id>[^/]+?)/reactivate$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_empresa_reactivate;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\EmpresaController::reactivateAction',)), array('_route' => 'admin_empresa_reactivate'));
        }
        not_admin_empresa_reactivate:

        // admin_useradministrador
        if (rtrim($pathinfo, '/') === '/admin/useradministrador') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_useradministrador');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::indexAction',  '_route' => 'admin_useradministrador',);
        }

        // admin_useradministrador_show
        if (0 === strpos($pathinfo, '/admin/useradministrador') && preg_match('#^/admin/useradministrador/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::showAction',)), array('_route' => 'admin_useradministrador_show'));
        }

        // admin_useradministrador_new
        if ($pathinfo === '/admin/useradministrador/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::newAction',  '_route' => 'admin_useradministrador_new',);
        }

        // admin_useradministrador_create
        if ($pathinfo === '/admin/useradministrador/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_useradministrador_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::createAction',  '_route' => 'admin_useradministrador_create',);
        }
        not_admin_useradministrador_create:

        // admin_useradministrador_edit
        if (0 === strpos($pathinfo, '/admin/useradministrador') && preg_match('#^/admin/useradministrador/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::editAction',)), array('_route' => 'admin_useradministrador_edit'));
        }

        // admin_useradministrador_update
        if (0 === strpos($pathinfo, '/admin/useradministrador') && preg_match('#^/admin/useradministrador/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_useradministrador_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::updateAction',)), array('_route' => 'admin_useradministrador_update'));
        }
        not_admin_useradministrador_update:

        // admin_useradministrador_delete
        if (0 === strpos($pathinfo, '/admin/useradministrador') && preg_match('#^/admin/useradministrador/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_useradministrador_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\UserAdministradorController::deleteAction',)), array('_route' => 'admin_useradministrador_delete'));
        }
        not_admin_useradministrador_delete:

        // admin_incidencia
        if (rtrim($pathinfo, '/') === '/admin/incidencia') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_incidencia');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::indexAction',  '_route' => 'admin_incidencia',);
        }

        // admin_incidencia_show
        if (0 === strpos($pathinfo, '/admin/incidencia') && preg_match('#^/admin/incidencia/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::showAction',)), array('_route' => 'admin_incidencia_show'));
        }

        // admin_incidencia_new
        if ($pathinfo === '/admin/incidencia/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::newAction',  '_route' => 'admin_incidencia_new',);
        }

        // admin_incidencia_create
        if ($pathinfo === '/admin/incidencia/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_incidencia_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::createAction',  '_route' => 'admin_incidencia_create',);
        }
        not_admin_incidencia_create:

        // admin_incidencia_edit
        if (0 === strpos($pathinfo, '/admin/incidencia') && preg_match('#^/admin/incidencia/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::editAction',)), array('_route' => 'admin_incidencia_edit'));
        }

        // admin_incidencia_update
        if (0 === strpos($pathinfo, '/admin/incidencia') && preg_match('#^/admin/incidencia/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_incidencia_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::updateAction',)), array('_route' => 'admin_incidencia_update'));
        }
        not_admin_incidencia_update:

        // admin_incidencia_delete
        if (0 === strpos($pathinfo, '/admin/incidencia') && preg_match('#^/admin/incidencia/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_incidencia_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::deleteAction',)), array('_route' => 'admin_incidencia_delete'));
        }
        not_admin_incidencia_delete:

        // admin_incidencia_atender
        if (0 === strpos($pathinfo, '/admin/incidencia') && preg_match('#^/admin/incidencia/(?P<id>[^/]+?)/atender$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_admin_incidencia_atender;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\IncidenciaController::atenderAction',)), array('_route' => 'admin_incidencia_atender'));
        }
        not_admin_incidencia_atender:

        // admin_hotel
        if (rtrim($pathinfo, '/') === '/admin/hotel') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_hotel');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::indexAction',  '_route' => 'admin_hotel',);
        }

        // admin_hotel_show
        if (0 === strpos($pathinfo, '/admin/hotel') && preg_match('#^/admin/hotel/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::showAction',)), array('_route' => 'admin_hotel_show'));
        }

        // admin_hotel_new
        if ($pathinfo === '/admin/hotel/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::newAction',  '_route' => 'admin_hotel_new',);
        }

        // admin_hotel_create
        if ($pathinfo === '/admin/hotel/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_hotel_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::createAction',  '_route' => 'admin_hotel_create',);
        }
        not_admin_hotel_create:

        // admin_hotel_edit
        if (0 === strpos($pathinfo, '/admin/hotel') && preg_match('#^/admin/hotel/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::editAction',)), array('_route' => 'admin_hotel_edit'));
        }

        // admin_hotel_update
        if (0 === strpos($pathinfo, '/admin/hotel') && preg_match('#^/admin/hotel/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_hotel_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::updateAction',)), array('_route' => 'admin_hotel_update'));
        }
        not_admin_hotel_update:

        // admin_hotel_delete
        if (0 === strpos($pathinfo, '/admin/hotel') && preg_match('#^/admin/hotel/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_hotel_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::deleteAction',)), array('_route' => 'admin_hotel_delete'));
        }
        not_admin_hotel_delete:

        // admin_hotel_reactivate
        if (0 === strpos($pathinfo, '/admin/hotel') && preg_match('#^/admin/hotel/(?P<id>[^/]+?)/reactivate$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_hotel_reactivate;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::reactivateHotelAction',)), array('_route' => 'admin_hotel_reactivate'));
        }
        not_admin_hotel_reactivate:

        // admin_hotel_check_slug
        if (0 === strpos($pathinfo, '/admin/hotel/check-hotel-slug') && preg_match('#^/admin/hotel/check\\-hotel\\-slug/(?P<slug>[^/]+?)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_admin_hotel_check_slug;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\HotelController::checkHotelSlugAction',)), array('_route' => 'admin_hotel_check_slug'));
        }
        not_admin_hotel_check_slug:

        // admin_quienessomos
        if (rtrim($pathinfo, '/') === '/admin/quienessomos') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_quienessomos');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::indexAction',  '_route' => 'admin_quienessomos',);
        }

        // admin_quienessomos_show
        if (0 === strpos($pathinfo, '/admin/quienessomos') && preg_match('#^/admin/quienessomos/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::showAction',)), array('_route' => 'admin_quienessomos_show'));
        }

        // admin_quienessomos_new
        if ($pathinfo === '/admin/quienessomos/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::newAction',  '_route' => 'admin_quienessomos_new',);
        }

        // admin_quienessomos_create
        if ($pathinfo === '/admin/quienessomos/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_quienessomos_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::createAction',  '_route' => 'admin_quienessomos_create',);
        }
        not_admin_quienessomos_create:

        // admin_quienessomos_edit
        if (0 === strpos($pathinfo, '/admin/quienessomos') && preg_match('#^/admin/quienessomos/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::editAction',)), array('_route' => 'admin_quienessomos_edit'));
        }

        // admin_quienessomos_update
        if (0 === strpos($pathinfo, '/admin/quienessomos') && preg_match('#^/admin/quienessomos/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_quienessomos_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::updateAction',)), array('_route' => 'admin_quienessomos_update'));
        }
        not_admin_quienessomos_update:

        // admin_quienessomos_delete
        if (0 === strpos($pathinfo, '/admin/quienessomos') && preg_match('#^/admin/quienessomos/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_quienessomos_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\QuienessomosController::deleteAction',)), array('_route' => 'admin_quienessomos_delete'));
        }
        not_admin_quienessomos_delete:

        // hoteles_backend_crontask_fixture
        if ($pathinfo === '/crontasks/fixture') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\CronTaskController::fixtureAction',  '_route' => 'hoteles_backend_crontask_fixture',);
        }

        // admin_ofertas
        if (rtrim($pathinfo, '/') === '/admin/ofertas') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_ofertas');
            }
            return array (  'filtro' => 'todos',  'id_hotel' => NULL,  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::indexAction',  '_route' => 'admin_ofertas',);
        }

        // admin_ofertas_filtradas
        if (0 === strpos($pathinfo, '/admin/ofertas/filtro') && preg_match('#^/admin/ofertas/filtro(?:/(?P<filtro>[^/]+?)(?:/(?P<id_hotel>[^/]+?))?)?$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  'filtro' => 'todos',  'id_hotel' => NULL,  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::indexAction',)), array('_route' => 'admin_ofertas_filtradas'));
        }

        // admin_ofertas_show
        if (0 === strpos($pathinfo, '/admin/ofertas') && preg_match('#^/admin/ofertas/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::showAction',)), array('_route' => 'admin_ofertas_show'));
        }

        // admin_ofertas_new
        if ($pathinfo === '/admin/ofertas/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::newAction',  '_route' => 'admin_ofertas_new',);
        }

        // admin_ofertas_create
        if ($pathinfo === '/admin/ofertas/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_ofertas_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::createAction',  '_route' => 'admin_ofertas_create',);
        }
        not_admin_ofertas_create:

        // admin_ofertas_edit
        if (0 === strpos($pathinfo, '/admin/ofertas') && preg_match('#^/admin/ofertas/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::editAction',)), array('_route' => 'admin_ofertas_edit'));
        }

        // admin_ofertas_update
        if (0 === strpos($pathinfo, '/admin/ofertas') && preg_match('#^/admin/ofertas/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_ofertas_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::updateAction',)), array('_route' => 'admin_ofertas_update'));
        }
        not_admin_ofertas_update:

        // admin_ofertas_delete
        if (0 === strpos($pathinfo, '/admin/ofertas') && preg_match('#^/admin/ofertas/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_ofertas_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::deleteAction',)), array('_route' => 'admin_ofertas_delete'));
        }
        not_admin_ofertas_delete:

        // acepta_oferta
        if (0 === strpos($pathinfo, '/admin/ofertas/aceptaoferta') && preg_match('#^/admin/ofertas/aceptaoferta/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::aceptaofertaAction',)), array('_route' => 'acepta_oferta'));
        }

        // hoteles_backend_oferta_aceptarofertadesdeemail
        if (0 === strpos($pathinfo, '/admin/ofertas/acepta-oferta') && preg_match('#^/admin/ofertas/acepta\\-oferta/(?P<idofertaunico>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::aceptarOfertaDesdeEmailAction',)), array('_route' => 'hoteles_backend_oferta_aceptarofertadesdeemail'));
        }

        // hoteles_backend_oferta_rechazarofertadesdeemail
        if (0 === strpos($pathinfo, '/admin/ofertas/rechazar-oferta') && preg_match('#^/admin/ofertas/rechazar\\-oferta/(?P<idofertaunico>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::rechazarOfertaDesdeEmailAction',)), array('_route' => 'hoteles_backend_oferta_rechazarofertadesdeemail'));
        }

        // declina_oferta
        if (0 === strpos($pathinfo, '/admin/ofertas/declinaoferta') && preg_match('#^/admin/ofertas/declinaoferta/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::declinaofertaAction',)), array('_route' => 'declina_oferta'));
        }

        // admin_ofertas_contraoferta
        if (0 === strpos($pathinfo, '/admin/ofertas/contraoferta') && preg_match('#^/admin/ofertas/contraoferta/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::contraofertaAction',)), array('_route' => 'admin_ofertas_contraoferta'));
        }

        // hoteles_backend_oferta_ofertadetails
        if (0 === strpos($pathinfo, '/admin/ofertas/oferta-details') && preg_match('#^/admin/ofertas/oferta\\-details/(?P<id>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\OfertaController::ofertaDetailsAction',)), array('_route' => 'hoteles_backend_oferta_ofertadetails'));
        }

        // admin_contactoweb
        if (rtrim($pathinfo, '/') === '/admin/contactoweb') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'admin_contactoweb');
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::indexAction',  '_route' => 'admin_contactoweb',);
        }

        // admin_contactoweb_show
        if (0 === strpos($pathinfo, '/admin/contactoweb') && preg_match('#^/admin/contactoweb/(?P<id>[^/]+?)/show$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::showAction',)), array('_route' => 'admin_contactoweb_show'));
        }

        // admin_contactoweb_new
        if ($pathinfo === '/admin/contactoweb/new') {
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::newAction',  '_route' => 'admin_contactoweb_new',);
        }

        // admin_contactoweb_create
        if ($pathinfo === '/admin/contactoweb/create') {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_contactoweb_create;
            }
            return array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::createAction',  '_route' => 'admin_contactoweb_create',);
        }
        not_admin_contactoweb_create:

        // admin_contactoweb_edit
        if (0 === strpos($pathinfo, '/admin/contactoweb') && preg_match('#^/admin/contactoweb/(?P<id>[^/]+?)/edit$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::editAction',)), array('_route' => 'admin_contactoweb_edit'));
        }

        // admin_contactoweb_update
        if (0 === strpos($pathinfo, '/admin/contactoweb') && preg_match('#^/admin/contactoweb/(?P<id>[^/]+?)/update$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_contactoweb_update;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::updateAction',)), array('_route' => 'admin_contactoweb_update'));
        }
        not_admin_contactoweb_update:

        // admin_contactoweb_delete
        if (0 === strpos($pathinfo, '/admin/contactoweb') && preg_match('#^/admin/contactoweb/(?P<id>[^/]+?)/delete$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_admin_contactoweb_delete;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\ContactoController::deleteAction',)), array('_route' => 'admin_contactoweb_delete'));
        }
        not_admin_contactoweb_delete:

        // HotelesFrontendBundle_homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'HotelesFrontendBundle_homepage');
            }
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::indexAction',  '_route' => 'HotelesFrontendBundle_homepage',);
        }

        // HotelesFrontendBundle_compruebalogin
        if ($pathinfo === '/compruebalogin') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::compruebaloginAction',  '_route' => 'HotelesFrontendBundle_compruebalogin',);
        }

        // HotelesFrontendBundle_getUserData
        if ($pathinfo === '/getuserdata') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::getUserDataAction',  '_route' => 'HotelesFrontendBundle_getUserData',);
        }

        // HotelesFrontendBundle_logout
        if ($pathinfo === '/logoutuser') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::logoutAction',  '_route' => 'HotelesFrontendBundle_logout',);
        }

        // HotelesFrontendBundle_compruebaloginuserrss
        if ($pathinfo === '/compruebaloginuserrss') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::compruebaloginuserrssAction',  '_route' => 'HotelesFrontendBundle_compruebaloginuserrss',);
        }

        // HotelesFrontendBundle_listadoprovincia
        if (0 === strpos($pathinfo, '/resultados') && preg_match('#^/resultados/(?P<provincia>[^/]+?)/(?P<fechain>[^/]+?)/(?P<fechaout>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciaAction',)), array('_route' => 'HotelesFrontendBundle_listadoprovincia'));
        }

        // HotelesFrontendBundle_listadoprovinciamunicipio
        if (0 === strpos($pathinfo, '/resultados') && preg_match('#^/resultados/(?P<provincia>[^/]+?)/(?P<municipio>[^/]+?)/(?P<fechain>[^/]+?)/(?P<fechaout>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciamunicipioAction',)), array('_route' => 'HotelesFrontendBundle_listadoprovinciamunicipio'));
        }

        // HotelesFrontendBundle_listadoprovinciamunicipiolugar
        if (0 === strpos($pathinfo, '/resultados') && preg_match('#^/resultados/(?P<provincia>[^/]+?)/(?P<municipio>[^/]+?)/(?P<zona>[^/]+?)/(?P<fechain>[^/]+?)/(?P<fechaout>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciamunicipiolugarAction',)), array('_route' => 'HotelesFrontendBundle_listadoprovinciamunicipiolugar'));
        }

        // HotelesFrontendBundle_listadoprovinciaajax
        if (0 === strpos($pathinfo, '/listadoajax') && preg_match('#^/listadoajax/(?P<provincia>[^/]+?)/(?P<fechain>[^/]+?)/(?P<fechaout>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciaajaxAction',)), array('_route' => 'HotelesFrontendBundle_listadoprovinciaajax'));
        }

        // HotelesFrontendBundle_listadoprovinciamunicipioajax
        if (0 === strpos($pathinfo, '/listadoajax') && preg_match('#^/listadoajax/(?P<provincia>[^/]+?)/(?P<municipio>[^/]+?)/(?P<fechain>[^/]+?)/(?P<fechaout>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciamunicipioajaxAction',)), array('_route' => 'HotelesFrontendBundle_listadoprovinciamunicipioajax'));
        }

        // HotelesFrontendBundle_listadoprovinciamunicipiolugarajax
        if (0 === strpos($pathinfo, '/listadoajax') && preg_match('#^/listadoajax/(?P<provincia>[^/]+?)/(?P<municipio>[^/]+?)/(?P<zona>[^/]+?)/(?P<fechain>[^/]+?)/(?P<fechaout>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\ListadoController::listadoprovinciamunicipiolugarajaxAction',)), array('_route' => 'HotelesFrontendBundle_listadoprovinciamunicipiolugarajax'));
        }

        // HotelesFrontendBundle_detallehotel
        if (0 === strpos($pathinfo, '/hotel') && preg_match('#^/hotel/(?P<nombrehotel>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::detallehotelAction',)), array('_route' => 'HotelesFrontendBundle_detallehotel'));
        }

        // HotelesFrontendBundle_oferta
        if ($pathinfo === '/oferta') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::ofertaAction',  '_route' => 'HotelesFrontendBundle_oferta',);
        }

        // HotelesFrontendBundle_registroajax
        if ($pathinfo === '/registroajax') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::registroajaxAction',  '_route' => 'HotelesFrontendBundle_registroajax',);
        }

        // HotelesFrontendBundle_registroextraajax
        if ($pathinfo === '/registroextraajax') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::registroextraajaxAction',  '_route' => 'HotelesFrontendBundle_registroextraajax',);
        }

        // HotelesFrontendBundle_ofertarealizada
        if ($pathinfo === '/oferta-realizada') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::ofertarealizadaAction',  '_route' => 'HotelesFrontendBundle_ofertarealizada',);
        }

        // HotelesFrontendBundle_usuariobaneado
        if ($pathinfo === '/usuario-baneado') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\FrontendController::usuarioBaneadoAction',  '_route' => 'HotelesFrontendBundle_usuariobaneado',);
        }

        // HotelesFrontendBundle_quienesSomos
        if ($pathinfo === '/quienes-somos') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::quienesSomosAction',  '_route' => 'HotelesFrontendBundle_quienesSomos',);
        }

        // HotelesFrontendBundle_condicionesLegales
        if ($pathinfo === '/condiciones-legales') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::condicionesLegalesAction',  '_route' => 'HotelesFrontendBundle_condicionesLegales',);
        }

        // HotelesFrontendBundle_privacidad
        if ($pathinfo === '/condiciones-particulares-y-privacidad') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::privacidadAction',  '_route' => 'HotelesFrontendBundle_privacidad',);
        }

        // HotelesFrontendBundle_cookies
        if ($pathinfo === '/cookies') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::cookiesAction',  '_route' => 'HotelesFrontendBundle_cookies',);
        }

        // HotelesFrontendBundle_faqs
        if ($pathinfo === '/faqs') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::faqsAction',  '_route' => 'HotelesFrontendBundle_faqs',);
        }

        // HotelesFrontendBundle_publicaTuHotel
        if ($pathinfo === '/publica-tu-hotel') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::publicaTuHotelAction',  '_route' => 'HotelesFrontendBundle_publicaTuHotel',);
        }

        // HotelesFrontendBundle_contacto
        if ($pathinfo === '/contacto') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::contactoAction',  '_route' => 'HotelesFrontendBundle_contacto',);
        }

        // HotelesFrontendBundle_mapaweb
        if ($pathinfo === '/mapaweb') {
            return array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::mapawebAction',  '_route' => 'HotelesFrontendBundle_mapaweb',);
        }

        // hoteles_frontend_tpv_done
        if (0 === strpos($pathinfo, '/tpv/done') && preg_match('#^/tpv/done/(?P<firma>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\TPVController::doneAction',)), array('_route' => 'hoteles_frontend_tpv_done'));
        }

        // hoteles_frontend_tpv_processpayment
        if (0 === strpos($pathinfo, '/tpv/processpayment') && preg_match('#^/tpv/processpayment/(?P<firma>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\TPVController::processpaymentAction',)), array('_route' => 'hoteles_frontend_tpv_processpayment'));
        }

        // hoteles_frontend_tpv_success
        if (0 === strpos($pathinfo, '/tpv/success') && preg_match('#^/tpv/success/(?P<firma>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\TPVController::successAction',)), array('_route' => 'hoteles_frontend_tpv_success'));
        }

        // hoteles_frontend_tpv_fail
        if (0 === strpos($pathinfo, '/tpv/fail') && preg_match('#^/tpv/fail/(?P<firma>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\TPVController::failAction',)), array('_route' => 'hoteles_frontend_tpv_fail'));
        }

        // hoteles_frontend_tpv_pay
        if (0 === strpos($pathinfo, '/tpv/pay') && preg_match('#^/tpv/pay/(?P<firma>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\TPVController::payAction',)), array('_route' => 'hoteles_frontend_tpv_pay'));
        }

        // prueba_de_render
        if (0 === strpos($pathinfo, '/render') && preg_match('#^/render/(?P<templateName>[^/]+?)$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\FrontendBundle\\Controller\\WebController::renderAction',)), array('_route' => 'prueba_de_render'));
        }

        // fos_user_security_login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
        }

        // fos_user_security_check
        if ($pathinfo === '/login_check') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
        }

        // fos_user_security_logout
        if ($pathinfo === '/logout') {
            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
        }

        // fos_user_profile_show
        if ($pathinfo === '/profile') {
            return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::urlRedirectAction',  'path' => '/password-cambiada',  'permanent' => true,  '_route' => 'fos_user_profile_show',);
        }

        // fos_user_profile_edit
        if ($pathinfo === '/profile/edit') {
            return array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::urlRedirectAction',  'path' => '/password-cambiada',  'permanent' => true,  '_route' => 'fos_user_profile_edit',);
        }

        if (0 === strpos($pathinfo, '/resetting')) {
            // fos_user_resetting_request
            if ($pathinfo === '/resetting/request') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_request;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',  '_route' => 'fos_user_resetting_request',);
            }
            not_fos_user_resetting_request:

            // fos_user_resetting_send_email
            if ($pathinfo === '/resetting/send-email') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_fos_user_resetting_send_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',  '_route' => 'fos_user_resetting_send_email',);
            }
            not_fos_user_resetting_send_email:

            // fos_user_resetting_check_email
            if ($pathinfo === '/resetting/check-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_resetting_check_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',  '_route' => 'fos_user_resetting_check_email',);
            }
            not_fos_user_resetting_check_email:

            // fos_user_resetting_reset
            if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_resetting_reset;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',)), array('_route' => 'fos_user_resetting_reset'));
            }
            not_fos_user_resetting_reset:

        }

        if (0 === strpos($pathinfo, '/register')) {
            // fos_user_registration_register
            if (rtrim($pathinfo, '/') === '/register') {
                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',  '_route' => 'fos_user_registration_register',);
            }

            // fos_user_registration_check_email
            if ($pathinfo === '/register/check-email') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_check_email;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',  '_route' => 'fos_user_registration_check_email',);
            }
            not_fos_user_registration_check_email:

            // fos_user_registration_confirm
            if (0 === strpos($pathinfo, '/register/confirm') && preg_match('#^/register/confirm/(?P<token>[^/]+?)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_confirm;
                }
                return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',)), array('_route' => 'fos_user_registration_confirm'));
            }
            not_fos_user_registration_confirm:

            // fos_user_registration_confirmed
            if ($pathinfo === '/register/confirmed') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_fos_user_registration_confirmed;
                }
                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',  '_route' => 'fos_user_registration_confirmed',);
            }
            not_fos_user_registration_confirmed:

        }

        // fos_js_routing_js
        if (0 === strpos($pathinfo, '/js/routing') && preg_match('#^/js/routing(?:\\.(?P<_format>js|json))?$#s', $pathinfo, $matches)) {
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'fos_js_routing.controller:indexAction',  '_format' => 'js',)), array('_route' => 'fos_js_routing_js'));
        }

        // remove_trailing_slash
        if (preg_match('#^/(?P<url>.*/)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_remove_trailing_slash;
            }
            return array_merge($this->mergeDefaults($matches, array (  '_controller' => 'Hoteles\\BackendBundle\\Controller\\RedirectingController::removeTrailingSlashAction',)), array('_route' => 'remove_trailing_slash'));
        }
        not_remove_trailing_slash:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
